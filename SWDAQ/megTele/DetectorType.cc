// $Id: DetectorType.cc 184 2013-03-25 16:54:49Z galli $

#include "DetectorType.hh"

using namespace std;

DetectorType::DetectorType(int detTypeId, 
               double pitch_phi, double pitch_z, 
               double externalSize_u, double externalSize_w, 
               double activeSize_u, double activeSize_w, 
                           int nstrips_phi, int nstrips_z) {
  assert(detTypeId>0 && detTypeId<7);
  _detTypeId = detTypeId;

  _pitch[0] = pitch_phi;
  _pitch[1] = pitch_z;

  _externalSize[0] = externalSize_u;
  _externalSize[1] = externalSize_w;

  _activeSize[0] = activeSize_u;
  _activeSize[1] = activeSize_w;

  _nstrips[0] = nstrips_phi;
  _nstrips[1] = nstrips_z;
}

