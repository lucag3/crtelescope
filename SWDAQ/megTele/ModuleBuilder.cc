// $Id: ModuleBuilder.cc 184 2013-03-25 16:54:49Z galli $

#include <fstream>
#include <iostream>

#include "ModuleBuilder.hh"

#include "DetectorType.hh"
#include "ModuleType.hh"

using namespace std;

ModuleBuilder::ModuleBuilder() {

  BuildDetectorList("/root/Desktop/megup/prototypes/DCHPrototype/online/megTele/detectorListConfig.dat");
  BuildModuleList("/root/Desktop/megup/prototypes/DCHPrototype/online/megTele/moduleListConfig.dat");

  /*
  // do some testing
  cout << "Testing module type D4AB..." << endl;

  ModuleType* mod = _modules["D4AB"];
  vector<DetectorType*> dets = mod->_detectors;
  cout << "No. detectors: " << dets.size() << endl;
  for (int i=0;i<dets.size();i++) cout << dets[i]->GetDetTypeId() << " ";
  cout << endl;
  //for (int i=0;i<mod->_detStrip1.size();i++) {
    //    cout << i << " " 
    //   << mod->_detStrip1[i].chip << " " 
    //   << mod->_detStrip1[i].chan << " " 
    //   << mod->_detStrip1[i].det << " " 
    //   << mod->_detStrip1[i].strip << endl; 
    //}

  int num_strips=0;
  vector<double> zpos1 = mod->GetZPositions(0,110,num_strips);
  cout << "Getting positions for chip 0, chan 110..." << endl;
  cout << "(0,110) -> num_strips: " << num_strips << endl << endl;

  cout << "Getting positions for chip 3, chan 0..." << endl;
  zpos1 = mod->GetZPositions(3,0,num_strips);
  cout << "(3,0) -> num_strips: " << num_strips << endl;
  cout << "   z1: " << zpos1[0] << endl;
  if (num_strips>1) 
    cout << "   z2: " << zpos1[1] << endl;
  cout << endl;

  cout << "Getting positions for chip 4, chan 110..." << endl;
  zpos1 = mod->GetZPositions(4,110,num_strips);
  cout << "(4,110) -> num_strips: " << num_strips << endl;
  cout << "   z1: " << zpos1[0] << ", z2: " << zpos1[1] << endl;
  */
}

void ModuleBuilder::BuildModuleList(string filename) {

  ifstream moduleFile(filename.c_str());
  assert(moduleFile.good());
  
  // read header line
  string headerLine;
  getline(moduleFile,headerLine);

  string id;
  int num_det;
  int detTypeId;
  vector<DetectorType*> detectors;

  while (moduleFile.good()) {

    detectors.clear();
    
    moduleFile >> id >> num_det;

    for (int i=0;i<num_det;i++) {
      moduleFile >> detTypeId;
      detectors.push_back(_detectors[detTypeId-1]);
    }

    //    for (int i=0;i<detectors.size();i++) cout << "     " << detectors[i]->GetDetTypeId() << " ";

    _modules[id] = (new ModuleType(id,detectors));
    
  }
  //cout << "Number of modules: " << _modules.size() << endl;
}


void ModuleBuilder::BuildDetectorList(string filename) {
  
  ifstream detFile(filename.c_str());
  assert(detFile.good());
  
  // read header line
  string headerLine;
  getline(detFile,headerLine);

  int id;
  double pitch_phi;
  double pitch_z;
  double externalSize_phi;
  double externalSize_z;
  double activeSize_phi;
  double activeSize_z;
  int nstrips_phi;
  int nstrips_z;

  while (detFile.good()) {
    
    detFile >> id >> pitch_phi >> pitch_z >> externalSize_phi >> externalSize_z >> nstrips_phi >> nstrips_z;

    //    cout << "BuildDetectorList: id: " << id << ", pitch_phi: " << pitch_phi << ", pitch_z: " <<  pitch_z << endl;

    // convert from microns to cm
    pitch_phi /= 10000.;
    pitch_z /= 10000.;

    // convert from mm to cm
    externalSize_phi /= 10.;
    externalSize_z /= 10.;

    //dead zone on detector borders is 700 microns on each edge
    activeSize_phi = externalSize_phi - 2*(700.)/10000.;
    activeSize_z = externalSize_z - 2*(700.)/10000.;

    // valid range for id is 1-6 
    assert(id>0 && id<7);

    //    _detectors[id-1] = new DetectorType(id,pitch_phi, pitch_z, externalSize_phi, externalSize_z,
    //                                activeSize_phi, activeSize_z, nstrips_phi, nstrips_z);

    _detectors.push_back(new DetectorType(id,pitch_phi, pitch_z, externalSize_phi, externalSize_z,
                                          activeSize_phi, activeSize_z, nstrips_phi, nstrips_z));
    
  }
  /*
  cout << "Number of detectors: " << _detectors.size() << endl;
  for (int i=0;i<_detectors.size();i++) {
    cout << "Det " << i+1 
         << ", phi pitch: " <<  _detectors[i]->GetPitch(0) 
         << ", z pitch: " <<  _detectors[i]->GetPitch(1) << endl;
  }
  */
}

bool ModuleBuilder::ValidModuleTypeId(string moduleTypeId) {
  return (moduleTypeId == "D4AF" ||
          moduleTypeId == "D4AB" ||
          moduleTypeId == "D4BF" ||
          moduleTypeId == "D4BB" ||
          moduleTypeId == "D5AF" ||
          moduleTypeId == "D5AB" ||
          moduleTypeId == "D5BF" ||       
          moduleTypeId == "D5BB" );
}

ModuleType* ModuleBuilder::GetModuleType(string moduleTypeId) {
  assert(ValidModuleTypeId(moduleTypeId));
  return _modules[moduleTypeId];
}

DetectorType* ModuleBuilder::GetDetectorType(int detTypeId) {
  assert(detTypeId>0 && detTypeId<7); 
  return _detectors[detTypeId-1];
}
