// $Id: ModuleBuilder.hh 235 2013-05-28 13:40:42Z galli $

#ifndef MODULEBUILDER
#define MODULEBUILDER

#include <assert.h>
#include <string>
#include <vector>
#include <map>

using namespace std;

class DetectorType;
class ModuleType;

// create lists of Detector and Module types

class ModuleBuilder {

private:
  
  vector<DetectorType*> _detectors;
  map<string,ModuleType*> _modules;

public:

  ModuleBuilder();
  // ~ModuleBuilder();
  
  void BuildDetectorList(string filename);

  void BuildModuleList(string filename);

  DetectorType* GetDetectorType(int detTypeId);
  ModuleType* GetModuleType(string moduleTypeId);
  bool ValidModuleTypeId(string moduleTypeId);
};
/*
ModuleBuilder::~ModuleBuilder(){
  _detectors.clear();
  _modules.clear();
}
*/
#endif
