// "$Id: StripInfo.hh 184 2013-03-25 16:54:49Z galli $"

#ifndef STRIPINFO
#define STRIPINFO

struct ChipChan {
  int chip;
  int chan;

  ChipChan() {
    chip = -1;
    chan = -1;
  }
  ChipChan(int chip1, int chan1) {
    chip = chip1;
    chan = chan1;
  }
};

struct DetStrip {
  int det;
  int strip;

  DetStrip() {
    det = 0;
    strip = 0;
  }
  DetStrip(int det1, int strip1) {
    det = det1;
    strip = strip1;
  }
};


struct StripInfo {
  int chip;
  int chan;
  int det;
  int strip;
  

  StripInfo(ChipChan cc, DetStrip ds) {
    chip = cc.chip;
    chan = cc.chan;
    det = ds.det;
    strip = ds.strip;
  }

  StripInfo(int chip1, int chan1, int det1, int strip1) {
    chip = chip1;
    chan = chan1;
    det = det1;
    strip = strip1;
  }
};

bool operator < (ChipChan cc1, ChipChan cc2);
bool operator < (DetStrip ds1, DetStrip ds2);

#endif
