// $Id: ModuleType.hh 184 2013-03-25 16:54:49Z galli $

#ifndef MODULETYPE
#define MODULETYPE

#include <assert.h>
#include <string>
#include <vector>
#include <map>
#include "StripInfo.hh"

using namespace std;

class DetectorType;

// define half-module properties

class ModuleType {

  // private:
public:
  
  string _moduleTypeId;  //"D4AF", "D5BB", etc.
  vector<DetectorType*> _detectors;

  // bonding map stuff
  vector<StripInfo*> _detStrip1;
  vector<StripInfo*> _detStrip2;

  map<ChipChan,DetStrip> _detStrMap1;
  map<ChipChan,DetStrip> _detStrMap2;

  map<DetStrip,ChipChan> _chipChanMap;

  double _phiPitch;
  double _zPitch;

  double _nChipsPhi;

  double _zStripLength;
  double _zStripCenter;
  double _phiStripLength;
  double _phiStripCenter;

  //  vector<int> _detTest;

  //public:

  ModuleType(string moduleTypeId, vector<DetectorType*> detectors);


  inline string GetModuleTypeId() {return _moduleTypeId;}

  inline vector<DetectorType*> GetDetectors() {return _detectors;}

  void ReadBondingMap();

  vector<double> GetZPositions(int chip, int chan, int& num_strips); 
  void GetZPositions(int chip, int chan, int& num_strips, double*); 

  double GetZPosition(int det, int strip);

  bool GetPhiPosition(int chip, int chan, double *phiPos);

  void CalculateModuleDimensions(double* dim);

  ChipChan GetChipChan(DetStrip ds);
  DetStrip GetDetStripFromZ(double zpos);

  ChipChan GetChipChanFromZ(double zpos);

  double GetZStripLength() {return _zStripLength; }
  double GetPhiStripLength() {return _phiStripLength; }

  double GetZStripCenter() {return _zStripCenter; }
  double GetPhiStripCenter() {return _phiStripCenter; }

  
};
#endif
