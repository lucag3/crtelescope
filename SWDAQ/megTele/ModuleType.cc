// $Id: ModuleType.cc 235 2013-05-28 13:40:42Z galli $

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>

#include "ModuleType.hh"
#include "DetectorType.hh"
#include "StripInfo.hh"

using namespace std;

ModuleType::ModuleType(string moduleTypeId, vector<DetectorType*> detectors) : 
  _moduleTypeId(moduleTypeId),_detectors(detectors) {

  ReadBondingMap();

  // the pitch is the same for all detectors within a module, store here for use later
  _phiPitch = _detectors[0]->GetPitch(0);
  _zPitch = _detectors[0]->GetPitch(1);

  // save the length of the phi and z strips 
  _zStripLength = _detectors[0]->GetExternalSize(0); //the phi dimension of a detector
  _zStripCenter = _zStripLength/2.;

  _phiStripLength=0;
  // add up detector dimensions along z, but skip over 1st (wedge) detector
  for (int i=1;i<_detectors.size();i++)
    _phiStripLength += _detectors[i]->GetExternalSize(1);

  // add in gaps between detectors
  _phiStripLength += (double) (_detectors.size()-2)*0.0075;
  _phiStripCenter = _phiStripLength/2.;

  // calc number of chips on phi side
  _nChipsPhi = _detectors[0]->GetNStrips(0)/128;

}

void ModuleType::ReadBondingMap() {

  string filename = "/root/Desktop/megup/prototypes/DCHPrototype/online/megTele/maps/" + _moduleTypeId + ".map";


  ifstream datafile(filename.c_str());
  assert (datafile.good());

  //  cout << "ReadBondingMap: filename: " << filename << endl;

  string line;
  int count=0;

  while(datafile.good()) {
    getline(datafile,line);

    istringstream ss(line);
    vector<string> tokens;
    string buf;

    while(ss >> buf) {
      tokens.push_back(buf);
    }
    
    if (tokens.size()>3) {
      int chip = atoi(tokens[2].substr(2,1).c_str());
      int chan = atoi(tokens[2].substr(4,3).c_str());
      int det1 = atoi(tokens[3].substr(2,1).c_str());
      int str1 = atoi(tokens[3].substr(4,3).c_str());

      int det2=0;
      int str2=0;

      if (tokens.size()>4) {
        det2 = atoi(tokens[4].substr(2,1).c_str());
        str2 = atoi(tokens[4].substr(4,3).c_str());
      }
        
      _detStrMap1[ChipChan(chip,chan)] = DetStrip(det1,str1);
      _detStrMap2[ChipChan(chip,chan)] = DetStrip(det2,str2);

      _chipChanMap[DetStrip(det1,str1)] = ChipChan(chip,chan);
      _chipChanMap[DetStrip(det2,str2)] = ChipChan(chip,chan);
      count++;
    }
    else {
      //cout << "Size: " << tokens.size() << endl;
    }
  }

}

vector<double> ModuleType::GetZPositions(int chip, int chan, int& num_strips) {

  vector<double> pos;
  num_strips = 0;

  DetStrip ds1 = _detStrMap1[ChipChan(chip,chan)];

  int det1 = ds1.det;
  int str1 = ds1.strip;

  if (det1 > 1) { //not in wedge detector

    //    cout << "ds1: chip, chan, det, str " << chip << " " <<  chan << " " << ds1.det << " " << ds1.strip << endl; 
    // store position
    num_strips++;
    pos.push_back(GetZPosition(det1, str1));
  }
  // see if 2nd strip present
  DetStrip ds2 = _detStrMap2[ChipChan(chip,chan)];

  if (ds2.det > 1 ) {// 2nd strip present and not in wedge
    //    cout << "ds2: chip, chan, det, str " << chip << " " <<  chan << " " << ds2.det << " " << ds2.strip << endl; 
    num_strips++;
    pos.push_back(GetZPosition(ds2.det,ds2.strip));
  }

  return pos;
}

double ModuleType::GetZPosition(int det, int strip) {

  // calculate number of detectors to skip over

  double d_skip=0; // length of skipped detectors and gaps
  double gap=0;

  for (int i=0; i<(det-2); i++) {
    d_skip += _detectors[i+1]->GetExternalSize(1); // start counting from detector 2
    gap += 0.0075; //gap between glued detectors
  }
  
  d_skip += gap;
  //cout << "d_skip: " << d_skip << endl;

  // the dead zone extends from the edge of the detector to the center of strip 1
  double dead_zone = 0.0700; 

  //double detid = _detectors[det-1]->GetDetTypeId();
  double pitch = _detectors[det-1]->GetPitch(1);
  //cout << "GetZPosition: DetId: " << detid << ", Pitch: " << pitch << endl;

  //double d_strip = pitch*double(strip) - pitch/2.;

  double d_strip = pitch*(strip-1); 

  double pos = d_skip + dead_zone + d_strip ; 
  return pos;
}

bool ModuleType::GetPhiPosition(int chip, int chan, double *phiPos) {

  // the phi position is returned through the arg list (phiPos)
  // the return value is true for valid chip, chan values, false otherwis

  if ( (chip > (_nChipsPhi-1)) || (chan > 127) ) {
    return false;
  }
   
  else {
    // the phi position is the same for all module types, simply the strip# * pitch - pitch/2;

    int strip = 128*chip + chan + 1; //chip/chan start from 0, strip starts from 1
    
    *phiPos = _phiPitch*double(strip) - _phiPitch/2.;
    
    return true;
  }
}

void ModuleType::CalculateModuleDimensions(double* dim) {
  
  int nDet = _detectors.size();
  
  double gap = (nDet-1)*0.0075;  //gap between detectors is 75 microns

  double xdim=0; // x is width of module (aligned with wafer "u")
  double ydim=0; // y is length of module (aligned with wafer "v")
  
  // sum up y dimensions. all detectors should have same width, but take max x dim for safety

  for (int i=0; i<nDet; i++) {
    DetectorType* detType = _detectors[i];
    
    ydim += detType->GetExternalSize(1);
    if (detType->GetExternalSize(0) > xdim) {
      xdim = detType->GetExternalSize(0);
    }
  }

  dim[0] = xdim;
  dim[1] = ydim + gap;

}

void ModuleType::GetZPositions(int chip, int chan, int& nstr, double* zpos) {

  vector<double> zvec = GetZPositions(chip,chan,nstr);
  for (int i=0; i<nstr; i++) {
    zpos[i] = zvec[i];
  }
}

ChipChan ModuleType::GetChipChan(DetStrip ds) {
  return _chipChanMap[ds];
}

DetStrip ModuleType::GetDetStripFromZ(double zpos) {
  
  int det = 2;
  int strip = 0;
  double zdet(0);

  //cout << "GetDetStripFromZ: zpos = " << zpos << endl;

  for (int idet=3; idet<= _detectors.size(); idet++) {


    // get zpos of edge of this detector
    double z0 = GetZPosition(idet,1) - 0.0700;
    //    cout << "idet=" << idet << ", z0 = " << z0 << endl;
    if (zpos>= z0) {
      det++;
      zdet = z0;
    }
  }
  
  // check to see if z is in inactive area
  double p = _detectors[det-1]->GetPitch(1);
  double activeSize = _detectors[det-1]->GetActiveSize(1);

  if ((zpos-zdet) < (0.0700 - p/2.) || (zpos-zdet) > (0.0700 + activeSize + p/2.)) {
    return DetStrip(det,0);
  }

  // calculate the strip closest to the z pos
  double stripFloat = (zpos - zdet - 0.0700)/p + 1.;

  // now round to nearest integer strip
  strip = (int) floor( stripFloat + 0.5 );

  return DetStrip(det,strip);

}

ChipChan ModuleType::GetChipChanFromZ(double zpos) {

  DetStrip detStrip = GetDetStripFromZ(zpos);
  ChipChan cc = GetChipChan(detStrip);

  return GetChipChan(detStrip);
}
