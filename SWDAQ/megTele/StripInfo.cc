// "$Id: StripInfo.cc 184 2013-03-25 16:54:49Z galli $"

#include "StripInfo.hh"

bool operator < (ChipChan cc1, ChipChan cc2) {
  return (1000*cc1.chip + cc1.chan) < (1000*cc2.chip + cc2.chan);
}

bool operator < (DetStrip ds1, DetStrip ds2) {
  return (1000*ds1.det+ds1.strip) < (1000*ds2.det+ds2.strip);
}
