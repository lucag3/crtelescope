// $Id: DetectorType.hh 184 2013-03-25 16:54:49Z galli $

#ifndef DETECTORTYPE
#define DETECTORTYPE

#include <assert.h>


// define parameters for silicon detectors

class DetectorType {

private:
  
  int _detTypeId; //from 1 to 6
  double _pitch[2]; //in cm, phi=0, z=1
  int _nstrips[2];
  double _externalSize[2];
  double _activeSize[2];

public:

  DetectorType(int detTypeId, 
               double pitch_phi, double pitch_z, 
               double externalSize_u, double externalSize_w, 
               double activeSize_u, double activeSize_w, 
               int nstrips_phi, int nstrips_z);

  inline int GetDetTypeId() {return _detTypeId;}
  inline double GetPitch(int side) {assert(side>-1 && side<2); return _pitch[side];}
  inline double GetExternalSize(int coord) {assert(coord>-1 && coord<2); return _externalSize[coord];}
  inline double GetActiveSize(int coord) {assert(coord>-1 && coord<2); return _activeSize[coord];}
  inline double GetNStrips(int side) {assert(side>-1 && side<2); return _nstrips[side];}

};
#endif
