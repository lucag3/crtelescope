/********************************************************************

  Name:         ace.h
  Created by:   Stefan Ritt

  Contents:     Header file for xsa.c

  $Id: ace.h 152 2013-02-26 10:16:42Z renga $

\********************************************************************/

#include "mvmestd.h"

/*------------------------------------------------------------------*/

#define ACE_SUCCESS      1
#define ACE_DEVICE_BUSY  2
#define ACE_NO_CF        3
#define ACE_NOT_READY    4

typedef struct {
   MVME_INTERFACE *vme;
   mvme_addr_t base_addr;
} ACE;

#ifndef O_RDONLY
#define O_RDONLY       0x0000  /* open for reading only */
#define O_WRONLY       0x0001  /* open for writing only */
#define O_RDWR         0x0002  /* open for reading and writing */
#define O_APPEND       0x0008  /* writes done at eof */

#define O_CREAT        0x0100  /* create and open file */
#define O_TRUNC        0x0200  /* open and truncate */
#define O_EXCL         0x0400  /* open only if file doesn't already exist */
#endif /* O_RDONLY */

/*---- Function prototypes -----------------------------------------*/

/* make functions callable from a C++ program */
#ifdef __cplusplus
extern "C" {
#endif

int ace_init(MVME_INTERFACE *vme, int slot, ACE *ace);
int ace_write_sectors(ACE *ace, int sector, int n_sectors, unsigned char *buf);
int ace_read_sectors(ACE *ace, int sector, int n_sectors, unsigned char *buf);
int ace_exit(ACE *ace);

int ace_open(ACE *ace, const char *pathname, int flags);
int ace_filesize(int fd);
int ace_close(int fd);
int ace_dir(ACE *ace);
int ace_write(int fd, void *buf, unsigned int count);
int ace_read(int fd, void *buf, unsigned int count);
int ace_flush(int fd);
int ace_upload(ACE *ace, char *file_name);
int ace_download(ACE *ace, char *file_name);

#ifdef __cplusplus
}
#endif

/*------------------------------------------------------------------*/
