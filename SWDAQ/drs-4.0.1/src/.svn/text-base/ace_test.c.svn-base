// $Id$
/********************************************************************

  Name:         ace_test.c
  Created by:   Stefan Ritt

  Contents:     Test program for ACE driver

\********************************************************************/

#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "mvmestd.h"

#ifdef _MSC_VER
#   include <windows.h>
#   include <direct.h>
#   include <io.h>
#   define SLEEP(x) Sleep(x)
#else
#   include <unistd.h>
#   include <sys/time.h>
#   define SLEEP(x) usleep(x*1000)
#endif

#include "ace.h"

char *buffer1, *buffer2;

#define FILE_TEST

int main()
{
   int fd, fh, i, n, size, n_error;
   ACE ace;
   struct stat st;

#ifdef CF_VIA_USB
   if (ace_init(NULL, 1, &ace) != ACE_SUCCESS)   // second physical device
      return -1;
#else
   MVME_INTERFACE *vme;

   if (mvme_open(&vme, 0) != MVME_SUCCESS)
      return 0;

   if (ace_init(vme, 13, &ace) != ACE_SUCCESS)  // VME slot 12
      return 0;
#endif

#ifdef SECTOR_TEST
   /* write and readback sector */
   size = 20*512;
   buffer1 = malloc(size);
   buffer2 = malloc(size);
   for (i=0 ; i<size ; i++)
      buffer1[i] = i;
   i = ace_write_sectors(&ace, 1000, size/512, buffer1);
   ace_read_sectors(&ace, 1000, size/512, buffer2);

   for (i=n_error=0 ; i<size ; i++)
      if (buffer1[i] != buffer2[i])
         n_error++;

   if (n_error == 0)
      printf("\nVerification successfully completed\n");
   else
      printf("\nVerification revealed %d errors\n", n_error);

   free(buffer1);
   free(buffer2);
   return 0;
#endif

#ifdef FILE_TEST

   /* read file and allocate buffers */
   fh = open("c:\\meg\\online\\VPC\\drs2\\2vp30\\cflash\\drs2\\rev0\\rev0.ace", O_RDONLY | O_BINARY);
   fstat(fh, &st);
   size = st.st_size;
   buffer1 = malloc(size);
   buffer2 = malloc(size);
   assert(buffer1);
   assert(buffer2);
   n = read(fh, buffer1, size);
   close(fh);

   /* download file */
   printf("Downloading file\n");
   fd = ace_open(&ace, "rev0.ace", O_RDWR | O_CREAT);
   n = ace_write(fd, buffer1, size);
   ace_close(fd);

   /* verify file */
   printf("\nRead back file\n");
   fd = ace_open(&ace, "rev0.ace", O_RDWR);
   n = ace_read(fd, buffer2, size);
   ace_close(fd);

   /* compare buffers */
   for (i=n_error=0 ; i<size ; i++)
      if (buffer1[i] != buffer2[i])
         n_error++;

   if (n_error == 0)
      printf("\nVerification successfully completed\n");
   else
      printf("\nVerification revealed %d errors\n", n_error);

   free(buffer1);
   free(buffer2);

#endif

#ifndef CF_VIA_USB
   mvme_close(vme);
#endif

   return 1;
}
