/********************************************************************\

  Name:         drs_exam.cpp
  Created by:   Stefan Ritt

  Contents:     Simple example application to read out a DRS4
                evaluation board

  $Id: drs_exam_multich.cpp 265 2013-10-18 16:27:44Z renga $

\********************************************************************/

#include <math.h>

#ifdef _MSC_VER

#include <windows.h>

#elif defined(OS_LINUX)

#define O_BINARY 0

#include <unistd.h>
#include <ctype.h>
#include <sys/ioctl.h>
#include <errno.h>

#define DIR_SEPARATOR '/'

#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "strlcpy.h"
#include "DRS.h"

/*------------------------------------------------------------------*/

int main()
{
   int i, j, nBoards;
   DRS *drs;
   DRSBoard **b = new DRSBoard*[2];
   float time_array[4][1024];
   float wave_array[32][1024];
   short swaveform[32][1024];
   FILE  *f;

   /* do initial scan */
   drs = new DRS();

   /* show any found board(s) */
   nBoards = drs->GetNumberOfBoards();

   if (nBoards == 0) {
     printf("No DRS4 evaluation board found\n");
     return 0;
   }
   
   for (i=0 ; i<nBoards ; i++) {

     b[i] = drs->GetBoard(i);
     printf("Found DRS4 evaluation board, serial #%d, firmware revision %d\n", 
             b[i]->GetBoardSerialNumber(), b[i]->GetFirmwareVersion());
     
     /* initialize board */
     b[i]->Init();
     
     /* set sampling frequency */
     b[i]->SetFrequency(1.601, true);

     /* enable transparent mode needed for analog trigger */
     b[i]->SetTranspMode(1);

     /* set input range to -0.5V ... +0.5V */
     //b[i]->SetInputRange(0.);

     /* use following line to set range to 0..1V */
     //b->SetInputRange(0.5);
     
     /* Trigger switch */
     //first value --> hardware trigger enable
     //second value --> software trigger
     b[i]->EnableTrigger(1,1);

   }

   /* open file to save waveforms */
   f = fopen("data.txt", "w");
   if (f == NULL) {
     perror("ERROR: Cannot open file \"data.txt\"");
     return 1;
   }
   
   bool found = false;
   j=0;

   while(j<10000){

     found = false;
     
     /* start board (activate domino wave) */
     b[0]->StartDomino();
     b[1]->StartDomino();

     /* wait for trigger */
     while (b[0]->IsBusy());

     for (i=0 ; i<nBoards ; i++) {

       /* read all waveforms */
       b[i]->TransferWaves();
       
       for(int ichip=0;ichip<4;ichip++){

         //b[i]->GetTime(ichip, b[i]->GetTriggerCell(0), time_array[ichip]);

         for(int ichannel=0;ichannel<4;ichannel++){

           b[i]->GetWave(ichip,2*ichannel, wave_array[i+2*ichannel+2*4*ichip],true,b[i]->GetTriggerCell(ichip),b[i]->GetStopWSR(ichip));
          
           b[i]->GetWave(ichip,2*ichannel, swaveform[i+2*ichannel+2*4*ichip], 0, 0);
           
           double thr = 0;
           
           for (int jj=100 ; jj<b[i]->GetChannelDepth() ; jj++){
             
             if(i==1 && fabs(swaveform[i+2*ichannel+2*4*ichip][jj]) > thr && ichip==0 && ichannel < 2) { found = true; break; }
             
           }
           
         }
         
       }
       
     }

     if(found){
       
       j++;
       
       printf("%d event found\n",j);

       for(int ich=0;ich<32;ich++){
         
         for (int jj=0 ; jj<1024 ; jj++)
           // fprintf(f, "%d %4d\n", jj, swaveform[ich][jj]);
         fprintf(f, "%d %f\n", jj, wave_array[ich][jj]);
         
       }
       
     }
     
   }
   
   fclose(f);
   
   /* delete DRS object -> close USB connection */
   delete drs;
}
