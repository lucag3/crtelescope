/********************************************************************

  Name:         ace.c
  Created by:   Stefan Ritt

  Contents:     Interface to System ACE on VPC board

  $Id: ace.c 152 2013-02-26 10:16:42Z renga $

\********************************************************************/

#ifndef EXCLUDE_VME

#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>
#include <sys/stat.h>
#include <fcntl.h>

#ifdef _MSC_VER
#   include <windows.h>
#   include <direct.h>
#   include <io.h>
#   define SLEEP(x) Sleep(x)
#   define DIR_SEPARATOR '\\'
#else
#   include <ctype.h>
#   include <unistd.h>
#   include <sys/time.h>
#   include <errno.h>
#   define SLEEP(x) usleep(x*1000)
#   define DIR_SEPARATOR '/'
#   define O_BINARY 0
#endif

#include "ace.h"

/* Swapping is necessary on Windows and Linux PCs using the Struck VME
   interface. It might not be needed for PPC embedded CPUs. In this case
   NO_SWAP should be defined */
#ifdef NO_SWAP
#define WORD_SWAP(x)
#define DWORD_SWAP(x)
#else
#define WORD_SWAP(x) { unsigned char _tmp;                               \
                       _tmp= *((unsigned char *)(x));                    \
                       *((unsigned char *)(x)) = *(((unsigned char *)(x))+1);     \
                       *(((unsigned char *)(x))+1) = _tmp; }
#define DWORD_SWAP(x) { unsigned char _tmp;                              \
                       _tmp= *((unsigned char *)(x));                    \
                       *((unsigned char *)(x)) = *(((unsigned char *)(x))+3);     \
                       *(((unsigned char *)(x))+3) = _tmp;               \
                       _tmp= *(((unsigned char *)(x))+1);                \
                       *(((unsigned char *)(x))+1) = *(((unsigned char *)(x))+2); \
                       *(((unsigned char *)(x))+2) = _tmp; }
#endif /* NO_SWAP */

#define ACE_TIMEOUT 10000

/*---- VME addresses -----------------------------------------------*/

/* assuming following DIP Switch settings:

   SW1-1: 1 (off)       use geographical addressing (1=left, 21=right)
   SW1-2: 1 (off)       \
   SW1-3: 1 (off)        >  VME_WINSIZE = 8MB, subwindow = 1MB
   SW1-4: 0 (on)        /
   SW1-5: 0 (on)        reserverd
   SW1-6: 0 (on)        reserverd
   SW1-7: 0 (on)        reserverd
   SW1-8: 0 (on)       \
                        |
   SW2-1: 0 (on)        |
   SW2-2: 0 (on)        |
   SW2-3: 0 (on)        |
   SW2-4: 0 (on)        > VME_ADDR_OFFSET = 0
   SW2-5: 0 (on)        |
   SW2-6: 0 (on)        |
   SW2-7: 0 (on)        |
   SW2-8: 0 (on)       /

   which gives 
     VME base address = SlotNo * VME_WINSIZE + VME_ADDR_OFFSET
                      = SlotNo * 0x80'0000
*/
#define GEVPC_BASE_ADDR           0x00000000
#define GEVPC_WINSIZE               0x800000
#define GEVPC_ACE_OFFSET             0x10000
#define GEVPC_USER_FPGA   (GEVPC_WINSIZE*2/8)

/*---- Register addresses ------------------------------------------*/

#define ACE_BMR_OFFSET           0x00  /* Bus mode (BUSMODEREG)     */
#define ACE_SR_OFFSET            0x04  /* Status (STATUSREG)        */
#define ACE_ER_OFFSET            0x08  /* Error (ERRORREG)          */
#define ACE_CLR_OFFSET           0x0C  /* Config LBA (CFGLBAREG)    */
#define ACE_MLR_OFFSET           0x10  /* MPU LBA (MPULBAREG)       */
#define ACE_SCCR_OFFSET          0x14  /* Sector cnt (SECCNTCMDREG) */
#define ACE_VR_OFFSET            0x16  /* Version (VERSIONREG)      */
#define ACE_CR_OFFSET            0x18  /* Control (CONTROLREG)      */
#define ACE_FSR_OFFSET           0x1C  /* FAT status (FATSTATREG)   */
#define ACE_DBR_OFFSET           0x40  /* Data buffer (DATABUFREG)  */

/* status register bits */
#define ACE_SR_CFGLOCK          (1<<0)
#define ACE_SR_MPULOCK          (1<<1)
#define ACE_SR_CFGERROR         (1<<2)
#define ACE_SR_CFCERROR         (1<<3)
#define ACE_SR_CFDETECT         (1<<4)
#define ACE_SR_DATABUFRDY       (1<<5)
#define ACE_SR_DATABUFMODE      (1<<6)
#define ACE_SR_CFGDONE          (1<<7)
#define ACE_SR_RDYFORCFCMD      (1<<8)
#define ACE_SR_CFGMODEPIN       (1<<9)
#define ACE_SR_CFBSY           (1<<17)
#define ACE_SR_CFRDY           (1<<18)
#define ACE_SR_CFDWF           (1<<19)
#define ACE_SR_CFDSC           (1<<20)
#define ACE_SR_CFDRQ           (1<<21)
#define ACE_SR_CFCORR          (1<<22)
#define ACE_SR_CFERR           (1<<23)

/* control register bits */
#define ACE_CR_FORCELOCKREQ     (1<<0)
#define ACE_CR_LOCKREQ          (1<<1)
#define ACE_CR_FORCECFGADDR     (1<<2)
#define ACE_CR_FORCECFGMODE     (1<<3)
#define ACE_CR_CFGMODE          (1<<4)
#define ACE_CR_CFGSTART         (1<<5)
#define ACE_CR_CFGSEL           (1<<6)
#define ACE_CR_CFGRESET         (1<<7)
#define ACE_CR_DATABUFRDYIRQ    (1<<8)
#define ACE_CR_ERRORIRQ         (1<<9)
#define ACE_CR_CFGDONEIRQ      (1<<10)
#define ACE_CR_RESETIRQ        (1<<11)
#define ACE_CR_CFGPROG         (1<<12)

/* sector count & command register bits */
#define ACE_SCCR_RESET        (1 << 8)
#define ACE_SCCR_IDENTIFY     (2 << 8)
#define ACE_SCCR_READ         (3 << 8)
#define ACE_SCCR_WRITE        (4 << 8)
#define ACE_SCCR_ABORT        (6 << 8)

/*---- ACE read/write routines for 32bit and 16bit access ----------*/

#ifndef CF_VIA_USB 

void ace_write32(ACE *ace, int offset, unsigned int data)
{
   mvme_set_dmode(ace->vme, MVME_DMODE_D32);
   DWORD_SWAP(&data);
   mvme_write_value(ace->vme, ace->base_addr + offset, data);
}

unsigned int ace_read32(ACE *ace, int offset)
{
   unsigned int data;

   mvme_set_dmode(ace->vme, MVME_DMODE_D32);
   data = mvme_read_value(ace->vme, ace->base_addr + offset);
   DWORD_SWAP(&data);
   return data;
}

void ace_write16(ACE *ace, int offset, unsigned short data)
{
   mvme_set_dmode(ace->vme, MVME_DMODE_D16);
   WORD_SWAP(&data);
   mvme_write_value(ace->vme, ace->base_addr + offset, data);
}

unsigned short ace_read16(ACE *ace, int offset)
{
   unsigned short data;

   mvme_set_dmode(ace->vme, MVME_DMODE_D16);
   data = mvme_read_value(ace->vme, ace->base_addr + offset);
   WORD_SWAP(&data);
   return data;
}

#endif /* CF_VIA_USB */

/*------------------------------------------------------------------*/

typedef struct {
   ACE            *ace;
   unsigned char  flags;
   unsigned char  attributes;
   unsigned int   file_size;
   unsigned int   first_cluster;
   unsigned int   first_cluster_sector;
   unsigned int   file_pointer;
   unsigned short cluster_pointer;
   unsigned char  *cluster_buffer;
   unsigned int   cluster_dirty;
   unsigned int   dir_sector;
   unsigned int   dirent_offset;
   unsigned int   dir_dirty;
} ACE_FD;

ACE_FD ace_fd[32];

typedef struct {
   char           file_name[8];
   char           extension[3];
   unsigned char  attributes;
   unsigned char  reserved;
   unsigned char  create_time_fine;
   unsigned short create_time;
   unsigned short create_date;
   unsigned short last_access_date;
   unsigned short ea_index;
   unsigned short modified_time;
   unsigned short modified_date;
   unsigned short first_cluster;
   unsigned int   file_size;
} DIR_ENTRY;

unsigned short *fat_table = NULL;
unsigned short *fat_dirty = NULL;
unsigned int sectors_per_fat, sectors_per_cluster, bytes_per_cluster, 
   boot_sector, reserved_sectors, total_sectors;

/*---- low level ACE routins ---------------------------------------*/

int ace_init(MVME_INTERFACE *vme, int slot, ACE *ace)
{
#ifdef CF_VIA_USB
   HANDLE hDevice;
   char drive[80];

   /* simulate ACE by accessing the CF card through a USB interface */
   sprintf(drive, "\\\\.\\PhysicalDrive%d", slot);
   hDevice = CreateFile(drive,                    // drive to open
                        GENERIC_READ | GENERIC_WRITE,  // access mode
                        FILE_SHARE_READ |         // share mode
                        FILE_SHARE_WRITE,
                        NULL,                     // default security attributes
                        OPEN_EXISTING,            // disposition
                        0,                        // file attributes
                        NULL);                    // do not copy file attributes

   if (hDevice == INVALID_HANDLE_VALUE)
      return ACE_NO_CF;

   ace->base_addr = (int)hDevice;
#else
   unsigned int cr_bits;

   /* store handle to VME interface */
   ace->vme = vme;

   /* calculate address of System ACE */
   ace->base_addr = GEVPC_BASE_ADDR + slot*GEVPC_WINSIZE + GEVPC_ACE_OFFSET;

   /* read registers */
   cr_bits = ace_read32(ace, ACE_CR_OFFSET);

   /* check if CompactFlash card is detected */
   if (!(ace_read32(ace, ACE_SR_OFFSET) & ACE_SR_CFDETECT))
      return ACE_NO_CF;

   /* disable interrupts */
   cr_bits |= ACE_CR_RESETIRQ;
   cr_bits &= ~(ACE_CR_DATABUFRDYIRQ | ACE_CR_ERRORIRQ | ACE_CR_CFGDONEIRQ);
   ace_write32(ace, ACE_CR_OFFSET, cr_bits);

   /* Reset the configuration controller if it is locked */
   if (ace_read32(ace, ACE_SR_OFFSET) & ACE_SR_CFGLOCK) {
      cr_bits |= ACE_CR_CFGRESET;
      ace_write32(ace, ACE_CR_OFFSET, cr_bits);
   }

   /* Force the MPU lock. The lock will occur immediately. */
   cr_bits |= (ACE_CR_LOCKREQ | ACE_CR_FORCELOCKREQ);
   ace_write32(ace, ACE_CR_OFFSET, cr_bits);
   
   /* See if the lock was granted */
   if (!(ace_read32(ace, ACE_SR_OFFSET) & ACE_SR_MPULOCK)) {
      
      /* Lock was not granted, so remove request and return error */
      cr_bits &= ~(ACE_CR_LOCKREQ | ACE_CR_FORCELOCKREQ);
      ace_write32(ace, ACE_CR_OFFSET, cr_bits);

      return ACE_DEVICE_BUSY;
   }

   /* check if ready for command */
   if (!(ace_read32(ace, ACE_SR_OFFSET) & ACE_SR_RDYFORCFCMD)) {
      /* abort any pending command */
      ace_write32(ace, ACE_SCCR_OFFSET, ACE_SCCR_ABORT);

      if (!(ace_read32(ace, ACE_SR_OFFSET) & ACE_SR_RDYFORCFCMD))
         return ACE_NOT_READY;
   }

   /* Release lock a possible reset */
   cr_bits &= ~(ACE_CR_CFGRESET | ACE_CR_LOCKREQ | ACE_CR_FORCELOCKREQ);
   ace_write32(ace, ACE_CR_OFFSET, cr_bits);

#endif /* CF_VIA_USB */

   /* clear any previously read FAT */
   if (fat_table) {
      free(fat_table);
      free(fat_dirty);
      fat_table = fat_dirty = NULL;
   }

   return ACE_SUCCESS;
}

/*------------------------------------------------------------------*/

int ace_exit(ACE *ace)
{
#ifdef CF_VIA_USB
   CloseHandle((HANDLE)ace->base_addr);
#else
#endif /* CF_VIA_USB */

   /* clear any previously read FAT */
   if (fat_table) {
      free(fat_table);
      free(fat_dirty);
      fat_table = fat_dirty = NULL;
   }

   return ACE_SUCCESS;
}

/*------------------------------------------------------------------*/

void print_sector(int sector, unsigned char buf[512])
{
   int i, j;
   unsigned char c;

   printf("Sector %d (0x%X):\n", sector, sector);
   for (i = 0; i < 32; i++) {
      printf("%03X ", i * 16);
      for (j = 0; j < 16; j++)
         printf("%02X ", buf[i * 16 + j]);
      printf(" ");

      for (j = 0; j < 16; j++) {
         c = buf[i * 16 + j];
         printf("%c", (c >= 32 && c < 128) ? buf[i * 16 + j] : '.');
      }
      printf("\n");
   }

   printf("\n");
}

int ace_read_sectors(ACE *ace, int sector, int n_sectors, unsigned char *buf)
{
#ifdef CF_VIA_USB
   unsigned int n;

   SetFilePointer((HANDLE)ace->base_addr, sector*512, NULL, FILE_BEGIN);
   ReadFile((HANDLE)ace->base_addr, buf, n_sectors*512, &n, NULL);
   if (n == n_sectors*512)
      return ACE_SUCCESS;
   else
      return ACE_DEVICE_BUSY;
#else
   unsigned int i, index, n, timeout, *pdata;
   unsigned int cr_bits, sr_bits, er_bits;

   /* Request and wait for the lock */
   cr_bits = ace_read32(ace, ACE_CR_OFFSET);
   cr_bits |= ACE_CR_LOCKREQ;
   ace_write32(ace, ACE_CR_OFFSET, cr_bits);
   
   for (timeout = 0 ; timeout < ACE_TIMEOUT ; timeout++) {
      sr_bits = ace_read32(ace, ACE_SR_OFFSET);
      if (sr_bits & ACE_SR_MPULOCK)
         break;
   }
   if (timeout == ACE_TIMEOUT)
      return ACE_NOT_READY;

   pdata = (unsigned int *)buf;

   for (index = 0 ; (int)index < n_sectors ; index++) {
      /* See if the CF is ready for a command.
         Increased timeout since after writing many sectors,
         the CF controller can be busy for quite long */
      for (timeout = 0 ; timeout < ACE_TIMEOUT*10 ; timeout++) {
         sr_bits = ace_read32(ace, ACE_SR_OFFSET);
         if (sr_bits & ACE_SR_RDYFORCFCMD)
            break;
      }
      if (timeout == ACE_TIMEOUT*10)
         return ACE_NOT_READY;

      /* Write the sector ID (LBA) */
      ace_write32(ace, ACE_MLR_OFFSET, sector+index);

      /* Send a read command of one sector to the controller */
      ace_write16(ace, ACE_SCCR_OFFSET, ACE_SCCR_READ | 1);

      /* Check for error */
      er_bits = ace_read32(ace, ACE_ER_OFFSET);
      if (er_bits)
         return er_bits;

      for (n = 0 ; n < 512 ;) {
         /* wait for CF data buffer ready */
         for (timeout = 0 ; timeout < ACE_TIMEOUT ; timeout++) {
            if (ace_read32(ace, ACE_SR_OFFSET) & ACE_SR_DATABUFRDY)
               break;
         }
         if (timeout == ACE_TIMEOUT)
            return ACE_NOT_READY;

         /* read 32 bytes */
         for (i=0 ; i<32 ; i+= 4,n+=4)
            *pdata++ = ace_read32(ace, ACE_DBR_OFFSET);
      }
   }

   /* Remove lock */
   cr_bits &= ~(ACE_CR_LOCKREQ);
   ace_write32(ace, ACE_CR_OFFSET, cr_bits);
 
   return ACE_SUCCESS;

#endif /* CF_VIA_USB */
}

/*------------------------------------------------------------------*/

int ace_write_sectors(ACE *ace, int sector, int n_sectors, unsigned char *buf)
{
#ifdef CF_VIA_USB
   unsigned int n;

   SetFilePointer((HANDLE)ace->base_addr, sector*512, NULL, FILE_BEGIN);
   WriteFile((HANDLE)ace->base_addr, buf, n_sectors*512, &n, NULL);
   if (n == n_sectors*512)
      return ACE_SUCCESS;
   else
      return ACE_DEVICE_BUSY;
#else
   unsigned int i, index, n, timeout, *pdata;
   unsigned int cr_bits, sr_bits, er_bits;

   /* Request and wait for the lock */
   cr_bits = ace_read32(ace, ACE_CR_OFFSET);
   cr_bits |= ACE_CR_LOCKREQ;
   ace_write32(ace, ACE_CR_OFFSET, cr_bits);
   
   for (timeout = 0 ; timeout < ACE_TIMEOUT ; timeout++) {
      sr_bits = ace_read32(ace, ACE_SR_OFFSET);
      if (sr_bits & ACE_SR_MPULOCK)
         break;
   }
   if (timeout == ACE_TIMEOUT) {
      printf("ACE timeout\n");
      return ACE_NOT_READY;
   }

   pdata = (unsigned int *)buf;

   for (index = 0 ; (int)index < n_sectors ; index++) {

      /* See if the CF is ready for a command */
      for (timeout = 0 ; timeout < ACE_TIMEOUT ; timeout++) {
         sr_bits = ace_read32(ace, ACE_SR_OFFSET);
         if (sr_bits & ACE_SR_RDYFORCFCMD)
            break;
      }
      if (timeout == ACE_TIMEOUT) {
         printf("ACE timeout\n");
         return ACE_NOT_READY;
      }

      /* Write the sector ID (LBA) */
      ace_write32(ace, ACE_MLR_OFFSET, sector+index);

      /* Send a write command for one sector to the controller */
      ace_write16(ace, ACE_SCCR_OFFSET, ACE_SCCR_WRITE | 1);

      /* Check for error */
      er_bits = ace_read32(ace, ACE_ER_OFFSET);
      if (er_bits) {
         printf("ACE error %d\n", er_bits);
         return er_bits;
      }

      for (n = 0 ; n < 512 ;) {
         /* wait for CF data buffer ready */
         for (timeout = 0 ; timeout < ACE_TIMEOUT ; timeout++) {
            if (ace_read32(ace, ACE_SR_OFFSET) & ACE_SR_DATABUFRDY)
               break;
         }
         if (timeout == ACE_TIMEOUT) {
            printf("ACE timeout\n");
            return ACE_NOT_READY;
         }

         /* write 32 bytes */
         for (i=0 ; i<32 ; i+= 4,n+=4)
            ace_write32(ace, ACE_DBR_OFFSET, *pdata++);
      }

      /* wait for CF data buffer ready */
      for (timeout = 0 ; timeout < ACE_TIMEOUT ; timeout++) {
         if (ace_read32(ace, ACE_SR_OFFSET) & ACE_SR_DATABUFRDY)
            break;
      }
      if (timeout == ACE_TIMEOUT) {
         printf("ACE timeout\n");
         return ACE_NOT_READY;
      }
   }

   /* Remove lock */
   cr_bits &= ~(ACE_CR_LOCKREQ);
   ace_write32(ace, ACE_CR_OFFSET, cr_bits);
 
   return ACE_SUCCESS;
#endif /* CF_VIA_USB */
}

/*---- high level file access routines -----------------------------*/

int ace_open(ACE *ace, const char *pathname, int flags)
{
   unsigned char sec[512];
   char basename[8], extname[3];
   unsigned int i, n, next;
   int first_free_index;
   unsigned int root_sector, number_of_fats, hidden_sectors, 
      root_entries, first_cluster_sector, first_free_root_sector;
   DIR_ENTRY *pdirent;
   time_t now;
   struct tm *ts;

   /* read MBR */
   if (ace_read_sectors(ace, 0, 1, sec) != ACE_SUCCESS)
      return -1;

   /* get LBA of first sector in partition */
   boot_sector = *((unsigned int *) &sec[0x1C6]);

   /* read FAT boot sector */
   if (ace_read_sectors(ace, boot_sector, 1, sec) != ACE_SUCCESS)
      return -1;

   /* check for FAT16 type */
   if (memcmp(sec+0x36, "FAT16   ", 8) != 0) {
      printf("CF card is not formatted in FAT16\n");
      return -1;
   }

   /* check for 512 bytes per sector */
   if (*((unsigned short *) &sec[0x0B]) != 512)
      return -1;

   /* allocate new file descriptor */
   for (n=0 ; n<sizeof(ace_fd)/sizeof(ACE_FD) ; n++)
      if (ace_fd[n].ace == NULL)
         break;

   if (n == sizeof(ace_fd)/sizeof(ACE_FD))
      return -1;

   /* calculate cluster and sector numbers */
   reserved_sectors = *((unsigned short *) &sec[0x0E]);
   number_of_fats = sec[0x10];
   root_entries = *((unsigned short *) &sec[0x11]);
   sectors_per_fat = *((unsigned short *) &sec[0x16]);
   hidden_sectors = *((unsigned int *) &sec[0x1C]);
   sectors_per_cluster = sec[0x0D];
   bytes_per_cluster = sectors_per_cluster * 512;

   root_sector = boot_sector + reserved_sectors + number_of_fats*sectors_per_fat;
   first_cluster_sector = root_sector + root_entries / 16;
   total_sectors = *((unsigned short *) &sec[0x13]);
   if (total_sectors == 0)
      total_sectors = *((unsigned int *) &sec[0x20]);

   if (reserved_sectors > 1) {
      printf("CF card was formatted under Windows and cannot be read by ACE controller.\n");
      printf("Please reformat it with mkdosfs.exe\n");
      return -1;
   }

   /* read FAT when called for the first time */
   if (fat_table == NULL) {
      fat_table = (unsigned short *)malloc(512 * sectors_per_fat);
      fat_dirty = (unsigned short *)malloc(sizeof(short) * sectors_per_fat);
      assert(fat_table);
      assert(fat_dirty);
      ace_read_sectors(ace, boot_sector + reserved_sectors, sectors_per_fat, (unsigned char *) fat_table);
      memset(fat_dirty, 0, sizeof(short) * sectors_per_fat);
   }

   /* set up FD entry */
   ace_fd[n].ace = ace;
   ace_fd[n].flags = flags;
   ace_fd[n].cluster_dirty = 0;
   ace_fd[n].dir_dirty = 0;

   first_cluster_sector -= 2 * sectors_per_cluster; /* first clusters is always #2 */
   ace_fd[n].first_cluster_sector = first_cluster_sector;

   /* split pathname */
   memset(basename, ' ', sizeof(basename));
   for (i=0 ; i<sizeof(basename) ; i++)
      basename[i] = toupper(pathname[i]);
   for (i=0 ; i<sizeof(basename) ; i++)
      if (basename[i] == '.')
         break;
   for ( ; i<sizeof(basename) ; i++)
      basename[i] = ' ';

   memset(extname, ' ', sizeof(extname));
   if (strchr(pathname, '.'))
      strncpy(extname, strchr(pathname, '.')+1, sizeof(extname));
   for (i=0 ; i<sizeof(extname) ; i++)
      extname[i] = toupper(extname[i]);

   /* read root directory sector */
   ace_read_sectors(ace, root_sector, 1, sec);
   pdirent = (DIR_ENTRY *)sec;
   first_free_root_sector = root_sector;

   for (i=0,first_free_index=-1 ; i<root_entries ; i++) {
      
      if (((unsigned char)pdirent->file_name[0] == 0 ||      // end of used directory
           (unsigned char)pdirent->file_name[0] == 0xE5) &&  // erased file
           first_free_index == -1) {
         first_free_root_sector = root_sector+i/16;
         first_free_index = i % 16;
      }

      if ((unsigned char)pdirent->file_name[0] == 0)
         break;

      if (memcmp(pdirent->file_name, basename, sizeof(basename)) == 0 &&
         memcmp(pdirent->extension, extname, sizeof(extname)) == 0) {

            /* file found */
            ace_fd[n].attributes = pdirent->attributes;
            ace_fd[n].file_size = pdirent->file_size;
            ace_fd[n].first_cluster = pdirent->first_cluster;
            ace_fd[n].file_pointer = 0;
            ace_fd[n].cluster_pointer = pdirent->first_cluster;
            ace_fd[n].cluster_buffer = (unsigned char *)malloc(bytes_per_cluster);
            assert(ace_fd[n].cluster_buffer);
            ace_fd[n].dir_sector = root_sector+i/16;
            ace_fd[n].dirent_offset = i%16;

            if (flags & O_APPEND) {
               /* find last cluster */
               next = ace_fd[n].cluster_pointer;
               if (next < 0xFFF8) {
                  while (next < 0xFFF8) {
                     i = next;
                     next = fat_table[i];
                  }
                  ace_fd[n].cluster_pointer = i;
               }
               ace_fd[n].file_pointer = ace_fd[n].file_size;
            }

            /* read first cluster */
            ace_read_sectors(ace, 
                             ace_fd[n].first_cluster_sector +
                             ace_fd[n].cluster_pointer * sectors_per_cluster,
                             sectors_per_cluster,
                             ace_fd[n].cluster_buffer);
            
            /*
            for (j=0 ; j<sectors_per_cluster ; j++)
               print_sector(ace_fd[n].first_cluster_sector +
                            ace_fd[n].cluster_pointer * sectors_per_cluster + j,
                            ace_fd[n].cluster_buffer + j*512);
            */

            /* delete file if called with O_TRUNC */
            if ((flags & O_TRUNC) && 
                ((flags & O_WRONLY) || (flags & O_RDWR))) {
               ace_fd[n].file_size = 0;
               ace_fd[n].cluster_dirty = 1;
               ace_fd[n].dir_dirty = 1;

               next = ace_fd[n].cluster_pointer;
               while (next < 0xFFF8) {
                  i = next;
                  next = fat_table[i];
                  fat_table[i] = 0;
                  fat_dirty[i/256] = 1;
               }
               i = ace_fd[n].cluster_pointer;
               fat_table[i] = 0xFFFF;
               fat_dirty[i/256] = 1;
            }

            return n;
         }

      pdirent++;
      if (((i+1) % 16) == 0) {
         ace_read_sectors(ace, root_sector+(i+1)/16, 1, sec);
         pdirent = (DIR_ENTRY *)sec;
      }
   }

   /* file not found, so clear file descriptor */
   if (!(flags & O_CREAT)) {
      ace_close(n);
      return -1;
   }

   /* create new file */
   ace_read_sectors(ace, first_free_root_sector, 1, sec);
   pdirent = ((DIR_ENTRY *)sec)+first_free_index;
   ace_fd[n].dir_sector = first_free_root_sector;
   ace_fd[n].dirent_offset = first_free_index;
   ace_fd[n].attributes = 0;
   ace_fd[n].file_size = 0;
   ace_fd[n].file_pointer = 0;
   ace_fd[n].cluster_buffer = (unsigned char *)malloc(bytes_per_cluster);
   assert(ace_fd[n].cluster_buffer);

   pdirent->attributes = 0x20; /* archive */
   for (i=0 ; i<8 ; i++)
      pdirent->file_name[i] = toupper(basename[i]);
   for (i=0 ; i<3 ; i++)
      pdirent->extension[i] = toupper(extname[i]);

   /* get currnt time */
   time(&now);
   ts = localtime(&now);

   pdirent->file_size = 0;
   pdirent->create_date = ((ts->tm_year-80) << 9) | ((ts->tm_mon+1) << 5) | (ts->tm_mday);
   pdirent->create_time = (ts->tm_hour << 11) | (ts->tm_min << 5) | (ts->tm_sec/2);
   pdirent->create_time_fine = 0;
   pdirent->last_access_date = pdirent->create_date;
   pdirent->modified_date = pdirent->create_date;
   pdirent->modified_time = pdirent->create_time;
   pdirent->ea_index = 0;
   pdirent->reserved = 0;

   /* find first free sector */
   for (i=2 ; i<sectors_per_fat*256 ; i++)
      if (fat_table[i] == 0)
         break;

   if (i == sectors_per_fat*256) {
      /* disk full */
      ace_close(n);
      return -1;
   }

   pdirent->first_cluster = i;
   fat_table[i] = 0xFFFF;
   ace_fd[n].first_cluster = i;
   ace_fd[n].cluster_pointer = i;

   /* write directory and FAT */
   ace_write_sectors(ace, first_free_root_sector, 1, sec);
   fat_dirty[i/256] = 1;

   return n;
}

/*------------------------------------------------------------------*/

int ace_close(int fd)
{
   if (fd < 0 || fd >= (int)(sizeof(ace_fd)/sizeof(ACE_FD)))
      return -1;

   ace_flush(fd);

   free(ace_fd[fd].cluster_buffer);
   memset(&ace_fd[fd], 0, sizeof(ACE_FD));
   return 0;
}

/*------------------------------------------------------------------*/

int ace_filesize(int fd)
{
   if (fd < 0 || fd >= (int)(sizeof(ace_fd)/sizeof(ACE_FD)))
      return -1;

   return ace_fd[fd].file_size;
}

/*------------------------------------------------------------------*/

int ace_dir(ACE *ace)
{
   unsigned char sec[512];
   char str[80], base_name[80], ext_name[80];
   unsigned int i, j, n, d, m, y, h;
   int first_free_index;
   unsigned int root_sector, number_of_fats, hidden_sectors, 
      root_entries, first_cluster_sector, first_free_root_sector;
   DIR_ENTRY *pdirent;

   /* read MBR */
   if (ace_read_sectors(ace, 0, 1, sec) != ACE_SUCCESS)
      return -1;

   /* get LBA of first sector in partition */
   boot_sector = *((unsigned int *) &sec[0x1C6]);

   /* read FAT boot sector */
   if (ace_read_sectors(ace, boot_sector, 1, sec) != ACE_SUCCESS)
      return -1;

   /* check for FAT16 type */
   if (memcmp(sec+0x36, "FAT16   ", 8) != 0) {
      printf("CF card is not formatted in FAT16\n");
      return -1;
   }

   /* check for 512 bytes per sector */
   if (*((unsigned short *) &sec[0x0B]) != 512)
      return -1;

   /* calculate cluster and sector numbers */
   reserved_sectors = *((unsigned short *) &sec[0x0E]);
   number_of_fats = sec[0x10];
   root_entries = *((unsigned short *) &sec[0x11]);
   sectors_per_fat = *((unsigned short *) &sec[0x16]);
   hidden_sectors = *((unsigned int *) &sec[0x1C]);
   sectors_per_cluster = sec[0x0D];
   bytes_per_cluster = sectors_per_cluster * 512;

   if (reserved_sectors > 1)
      printf("Warning: CF card was formatted under Windows and cannot be read by ACE.\n");

   root_sector = boot_sector + reserved_sectors + number_of_fats*sectors_per_fat;
   first_cluster_sector = root_sector + root_entries / 16;
   total_sectors = *((unsigned short *) &sec[0x13]);
   if (total_sectors == 0)
      total_sectors = *((unsigned int *) &sec[0x20]);

   /* read FAT when called for the first time */
   if (fat_table == NULL) {
      fat_table = (unsigned short *)malloc(512 * sectors_per_fat);
      fat_dirty = (unsigned short *)malloc(sizeof(short) * sectors_per_fat);
      assert(fat_table);
      assert(fat_dirty);
      ace_read_sectors(ace, boot_sector + reserved_sectors, sectors_per_fat, (unsigned char *) fat_table);
      memset(fat_dirty, 0, sizeof(short) * sectors_per_fat);
   }

   first_cluster_sector -= 2 * sectors_per_cluster; /* first clusters is always #2 */

   /* read root directory sector */
   ace_read_sectors(ace, root_sector, 1, sec);
   pdirent = (DIR_ENTRY *)sec;
   first_free_root_sector = root_sector;

   for (i=n=0,first_free_index=-1 ; i<root_entries ; i++) {
      
      if (pdirent->file_name[0] == 0)
         break;

      if ((unsigned char)pdirent->file_name[0] != 0xE5) {

         /* check if volume */
         if (pdirent->attributes & 0x02) {
         
            /* don't show hidden files */
         
         } else if (pdirent->attributes & 0x08) {
         
            /* don't show volume labels
            memset(str, 0, sizeof(str));
            strncpy(str, pdirent->file_name, 8);
            strncpy(str+8, pdirent->extension, 3);
            printf("Volume is %s\n", str);
            */

         } else {

            /* normal file */
            n++;

            d = pdirent->modified_date & 31;
            m = (pdirent->modified_date >> 5) & 15;
            y = (pdirent->modified_date >> 9) + 1980;
            printf("%02d.%02d.%04d", d, m, y);

            m = (pdirent->modified_time >> 5) & 63;
            h = (pdirent->modified_time >> 11);
            printf("  %02d:%02d", h, m);

            str[0] = 0;
            if (pdirent->file_size >= 1000000) {
               sprintf(str, "%d'", pdirent->file_size / 1000000);
               sprintf(str+strlen(str), "%03d'", (pdirent->file_size / 1000) % 1000);
               sprintf(str+strlen(str), "%03d", pdirent->file_size % 1000);
            } else if (pdirent->file_size >= 1000) {
               sprintf(str+strlen(str), "%d'", pdirent->file_size / 1000);
               sprintf(str+strlen(str), "%03d", pdirent->file_size % 1000);
            } else
               sprintf(str+strlen(str), "%d", pdirent->file_size);

            for (j=0 ; j<20-strlen(str) ; j++)
               printf(" ");
            printf("%s", str);
            strncpy(base_name, pdirent->file_name, 8);
            base_name[8] = 0;
            if (strchr(base_name, ' '))
               *strchr(base_name, ' ') = 0;
            strncpy(ext_name, pdirent->file_name+8, 3);
            ext_name[3] = 0;
            if (strchr(ext_name, ' '))
               *strchr(ext_name, ' ') = 0;
            printf(" %s.%s\n", base_name, ext_name);
         }
      }

      pdirent++;
      if (((i+1) % 16) == 0) {
         ace_read_sectors(ace, root_sector+(i+1)/16, 1, sec);
         pdirent = (DIR_ENTRY *)sec;
      }
   }

   return n;
}

/*------------------------------------------------------------------*/

int ace_read(int fd, void *buf, unsigned int count)
{
   unsigned char *pdest;
   unsigned int i, i_cluster, n_read, cl_per_dot;

   if (fd < 0 || fd >= (int)(sizeof(ace_fd)/sizeof(ACE_FD)))
      return -1;

   pdest = (unsigned char *)buf;
   n_read = 0;
   i_cluster = ace_fd[fd].file_pointer % bytes_per_cluster;
   i = count / bytes_per_cluster;
   cl_per_dot = i / 40 + 1;
   printf("[");
   for (i=0 ; i<count/bytes_per_cluster/cl_per_dot ; i++)
      printf("-");
   printf("]\r[");
   fflush(stdout);

   for (i=0 ; i<count ; i++) {
      
      /* end of file reached ? */
      if (ace_fd[fd].file_pointer == ace_fd[fd].file_size)
         return n_read;
      *pdest++ = ace_fd[fd].cluster_buffer[i_cluster];

      i_cluster++;
      ace_fd[fd].file_pointer++;
      n_read++;

      /* if end of cluster, read new one */
      if (i_cluster == bytes_per_cluster) {

         if (((i / bytes_per_cluster) % cl_per_dot) == 0) {
            printf("=");
            fflush(stdout);
         }

         /* get next cluster from FAT */
         ace_fd[fd].cluster_pointer = fat_table[ace_fd[fd].cluster_pointer];

         /* check if last cluster */
         if (ace_fd[fd].cluster_pointer >= 0xFFF8)
            return n_read;

         ace_read_sectors(ace_fd[fd].ace, 
                          ace_fd[fd].first_cluster_sector +
                          ace_fd[fd].cluster_pointer * sectors_per_cluster, 
                          sectors_per_cluster,
                          ace_fd[fd].cluster_buffer);

         /*
         for (j=0 ; j<sectors_per_cluster ; j++) {
            print_sector(ace_fd[fd].first_cluster_sector +
                         ace_fd[fd].cluster_pointer * sectors_per_cluster + j,
                         ace_fd[fd].cluster_buffer + j*512);
         }
         */

         i_cluster = 0;
      }
   }

   return n_read;
}

/*------------------------------------------------------------------*/

int ace_write(int fd, void *buf, unsigned int count)
{
   unsigned char *psrc;
   unsigned int i, j, i_cluster, n_written, next, cl_per_dot;

   if (fd < 0 || fd >= (int)(sizeof(ace_fd)/sizeof(ACE_FD)))
      return -1;

   if (!(ace_fd[fd].flags & O_WRONLY) &&
       !(ace_fd[fd].flags & O_RDWR))
      return -1;

   psrc = (unsigned char *)buf;
   n_written = 0;
   i_cluster = ace_fd[fd].file_pointer % bytes_per_cluster;
   i = count / bytes_per_cluster;
   cl_per_dot = i / 40 + 1;
   printf("[");
   for (i=0 ; i<count/bytes_per_cluster/cl_per_dot ; i++)
      printf("-");
   printf("]\r[");
   fflush(stdout);

   for (i=0 ; i<count ; i++) {
      
      ace_fd[fd].cluster_buffer[i_cluster] = *psrc++;

      i_cluster++;
      n_written++;
      ace_fd[fd].file_pointer++;
      if (ace_fd[fd].file_pointer > ace_fd[fd].file_size)
         ace_fd[fd].file_size = ace_fd[fd].file_pointer;
      ace_fd[fd].cluster_dirty = 1;
      ace_fd[fd].dir_dirty = 1;

      /* if end of cluster, write it */
      if (i_cluster == bytes_per_cluster) {

         if (((i / bytes_per_cluster) % cl_per_dot) == 0) {
            printf("=");
            fflush(stdout);
         }

         ace_write_sectors(ace_fd[fd].ace, 
                           ace_fd[fd].first_cluster_sector +
                           ace_fd[fd].cluster_pointer * sectors_per_cluster, 
                           sectors_per_cluster,
                           ace_fd[fd].cluster_buffer);
         i_cluster = 0;
         ace_fd[fd].cluster_dirty = 0;
         memset(ace_fd[fd].cluster_buffer, 0, sectors_per_cluster * 512);

         /* get next file cluster from FAT */
         next = fat_table[ace_fd[fd].cluster_pointer];

         /* if EOF, get next free cluster from FAT */
         if (next >= 0xFFF8) {
            for (j=2 ; j<sectors_per_fat*256 ; j++)
               if (fat_table[j] == 0)
                  break;

            if (j == sectors_per_fat*256) {
               /* disk full */
               ace_close(fd);
               return n_written;
            }

            fat_table[ace_fd[fd].cluster_pointer] = j;
            fat_table[j] = 0xFFFF;
            ace_fd[fd].cluster_pointer = j;
            fat_dirty[j/256] = 1;
         } else
            /* just move cluster pointer */
            ace_fd[fd].cluster_pointer = next;
      }
   }

   return n_written;
}

/*------------------------------------------------------------------*/

int ace_flush(int fd)
{
   int i;
   unsigned char sec[512], *cluster_buffer;
   DIR_ENTRY *pdirent;
   time_t now;
   struct tm *ts;

   if (fd < 0 || fd >= (int)(sizeof(ace_fd)/sizeof(ACE_FD)))
      return -1;

   if (!(ace_fd[fd].flags & O_RDWR) &&
       !(ace_fd[fd].flags & O_WRONLY))
      return -1;

   /* write cluster buffer until end of file */
   if (ace_fd[fd].cluster_dirty) {
      if (ace_fd[fd].file_pointer < ace_fd[fd].file_size) {
         /* combine partial custer data with cluster from disk */
         cluster_buffer = (unsigned char*)malloc(sectors_per_cluster * 512);
         assert(cluster_buffer);
         ace_read_sectors(ace_fd[fd].ace, 
                          ace_fd[fd].first_cluster_sector +
                          ace_fd[fd].cluster_pointer * sectors_per_cluster,
                          (ace_fd[fd].file_pointer % bytes_per_cluster)/512+1,
                          cluster_buffer);
         i = ace_fd[fd].file_pointer % (sectors_per_cluster * 512);
         memcpy(ace_fd[fd].cluster_buffer+i, cluster_buffer+i, sectors_per_cluster * 512 - i);
         free(cluster_buffer);
      }
      ace_write_sectors(ace_fd[fd].ace, 
                        ace_fd[fd].first_cluster_sector +
                        ace_fd[fd].cluster_pointer * sectors_per_cluster,
                        (ace_fd[fd].file_pointer % bytes_per_cluster)/512+1,
                        ace_fd[fd].cluster_buffer);
      ace_fd[fd].cluster_dirty = 0;
   }

   /* update directory entry */
   if (ace_fd[fd].dir_dirty) {
      ace_read_sectors(ace_fd[fd].ace, ace_fd[fd].dir_sector, 1, sec);
      pdirent = ((DIR_ENTRY *)sec)+ace_fd[fd].dirent_offset;
      pdirent->file_size = ace_fd[fd].file_size;

      /* get currnt time */
      time(&now);
      ts = localtime(&now);

      pdirent->modified_date = ((ts->tm_year-80) << 9) | ((ts->tm_mon+1) << 5) | (ts->tm_mday);
      pdirent->modified_time = (ts->tm_hour << 11) | (ts->tm_min << 5) | (ts->tm_sec/2);
      ace_write_sectors(ace_fd[fd].ace, ace_fd[fd].dir_sector, 1, sec);
   }

   /* flush modified FAT sectors */
   for (i=0 ; i<(int)sectors_per_fat ; i++)
      if (fat_dirty[i]) {
         /* primary FAT */
         ace_write_sectors(ace_fd[fd].ace, boot_sector+reserved_sectors+i, 1, (unsigned char*) &fat_table[i*256]);

         /* secondary FAT */
         ace_write_sectors(ace_fd[fd].ace, boot_sector+reserved_sectors+sectors_per_fat+i, 1, (unsigned char*) &fat_table[i*256]);
         fat_dirty[i] = 0;
      }

   return 0;
}

/*------------------------------------------------------------------*/

int ace_upload(ACE *ace, char *file_name)
{
   int i, fh, fd, n, size, retry, n_error;
   struct stat st;
   char str[256], *buffer1, *buffer2;

   /* read file and allocate buffers */
   if (strchr(file_name, '\r'))
      *strchr(file_name, '\r') = 0;
   if (strchr(file_name, '\n'))
      *strchr(file_name, '\n') = 0;
   fh = open(file_name, O_RDONLY | O_BINARY);
   if (fh < 0) {
      printf("Cannot open file \"%s\": %s\n", file_name, strerror(errno));
      return -1;
   }

   fstat(fh, &st);
   size = st.st_size;
   buffer1 = (char *)malloc(size);
   buffer2 = (char *)malloc(size);
   assert(buffer1);
   assert(buffer2);
   n = read(fh, buffer1, size);
   close(fh);

   /* strip any directory from file name */
   if (strrchr(file_name, DIR_SEPARATOR))
      strcpy(str, strrchr(file_name, DIR_SEPARATOR) + 1);
   else
      strcpy(str, file_name);

   for (retry = 0 ; retry < 10 ; retry++) {
      /* upload file */
      printf("Uploading file \"%s\":\n", str);
      fflush(stdout);
      fd = ace_open(ace, str, O_RDWR | O_CREAT);
      if (fd >= 0) {
         n = ace_write(fd, buffer1, size);
         ace_close(fd);

         /* verify file */
         printf("\nRead back file:\n");
         fflush(stdout);
         fd = ace_open(ace, str, O_RDWR);
         if (fd < 0) {
            fd = ace_open(ace, str, O_RDWR);
            printf("\nCannot read back file\n");
            break;
         }
         memset(buffer2, 0, size);
         n = ace_read(fd, buffer2, size);
         ace_close(fd);

         if (n != size) {
            printf("\nOnly could read back %d bytes\n", n);
            break;
         }
         /* compare buffers */
         for (i=n_error=0 ; i<size ; i++)
            if (buffer1[i] != buffer2[i])
               n_error++;

         if (n_error == 0) {
            printf("\nVerification successfully completed\n");
            break;
         } else
            printf("\nVerification revealed %d errors, retrying...\n", n_error);
      } else {
         printf("Cannot open file \"%s\" on CF card\n", str);
      }
   }

   free(buffer1);
   free(buffer2);

   return 0;
}

/*------------------------------------------------------------------*/

int ace_download(ACE *ace, char *file_name)
{
   int fh, fd, n, size;
   char str[256], *buffer;

   /* read file and allocate buffers */
   if (strchr(file_name, '\r'))
      *strchr(file_name, '\r') = 0;
   if (strchr(file_name, '\n'))
      *strchr(file_name, '\n') = 0;
   fh = open(file_name, O_CREAT | O_RDWR | O_BINARY);
   if (fh < 0) {
      printf("Cannot open file \"%s\": %s\n", file_name, strerror(errno));
      return -1;
   }

   /* strip any directory from file name */
   if (strrchr(file_name, DIR_SEPARATOR))
      strcpy(str, strrchr(file_name, DIR_SEPARATOR) + 1);
   else
      strcpy(str, file_name);

   /* download file */
   printf("Reading file:\n");
   fflush(stdout);
   fd = ace_open(ace, str, O_RDWR);
   if (fd < 0) {
      fd = ace_open(ace, str, O_RDWR);
      printf("\nCannot find file \"%s\"\n", str);
      return 0;
   }

   size = ace_filesize(fd);
   assert(size > 0);
   buffer = (char *)malloc(size);
   assert(buffer);

   n = ace_read(fd, buffer, size);
   ace_close(fd);
   if (n != size)
      printf("\nCould only read %d out of %d bytes\n", n, size);
   else
      printf("\nSuccessfully read %d bytes\n", n);

   /* write buffer */
   n = write(fh, buffer, size);
   if (n < size)
      printf("Could only write %d bytes to disk file\n", n);

   close(fh);
   free(buffer);

   return n;
}

/*------------------------------------------------------------------*/

#endif // EXCLUDE_VME
