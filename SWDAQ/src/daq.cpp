// $Id: daq.cpp 288 2017-02-07 21:10:34Z galli $
#include <iostream>
using namespace std;
#include <vector>
#include <sys/types.h>
#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include "TString.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TH1F.h"
#include "TGraph.h"
#include "TClonesArray.h"
#include "TControlBar.h"
#include "TApplication.h"
#include "TROOT.h"
#include "TRint.h"
#include "TVector3.h"
#include "../megTele/StripInfo.hh"
#include "../megTele/DetectorType.hh"
#include "../megTele/ModuleType.hh"
#include "../megTele/ModuleBuilder.hh"
// include drivers for VME
#include <fcntl.h>
#include "sis1100_var.h"
#include "sis3100_vme_calls.h"
// custom class
#include "../include/common.h"
#include "../include/AToM.h"
#include "../include/Terasic.h"
#include "../include/Server.h"
#include "../include/UDP.h"
#include "../include/Type1.h"
#include "../include/MoveStrip.h"
#include "../include/V1729.h"
//DRS classes
#if defined(HAVE_DRS)
#include "DRS.h"
#endif

//telescope set up
Terasic *terasic[8];
Server *server;
UDP *udp;
AToM *atom[8][9];
Type1 *type1;
V1729 *caendigi;
// canvas definition
TCanvas *eventdisplay;
//
#if defined(HAVE_DRS)
//DRS set up
int nBoards;
int nDRSchip;
int nDRSchannel;
DRS *drs;
DRSBoard **boards;
float **DRStimes;
float **DRSwaves;
#endif

TClonesArray wf("TGraph",50);
//function to define the rec structure
void DefineRec(TTree *);
//single run daq procedure
void initTelescope();
void initDRS();  
void readDRS();  
void removeDRSBusy();
void singlerun(Int_t );

// run submit controller
int main() 
{  
  // create a run parameters member
  runpar = new RunParameters();
  /* define the connections with:
     - Server
     - Terasic
     - DRS   */
  runpar->Print();
  // initialize telescope
  length=sizeof(struct sockaddr_in);
  fromlen = sizeof(struct sockaddr_in);
  initTelescope();
  // check is DRS is used
#if defined(HAVE_DRS)
  if(runpar->Getisdrs()==1) {
    initDRS();
  }//endi if isDRS
#endif
  // check if CAEN V1729 is used
  if(runpar->Getiscaen()==1) {
    // allocate the pointer to V1729
    caendigi = new V1729(0xc0000000);
    // reset the board
    caendigi->Reset();
    // initialize the board
    caendigi->Init();
  }// end if isCAEN
  // define vectors for telescope hits
  vboard = new vector<int>;
  vhitchip = new vector<int>;
  vchnum = new vector<int>;
  vTOT = new vector<int>;
  vTimStamp = new vector<int>;
  vpos = new vector<double>;
  vcenter = new vector<double>;
  vlength = new vector<double>;
  // usefull variables
  //  Int_t maxnrun = 200;
  Int_t firstrun = 5100;
  Int_t nrun = runpar->Getnrun();
  //open a canvas for event display
  //  TApplication *app = new TApplication("app",0,NULL);
  eventdisplay = new TCanvas("c","Event Display",800,600);
  eventdisplay->Divide(1,2);
  //check for the first not existing run number
  //  FILE *frun;
  while(fopen(Form("/home/data/run%d.root", firstrun),"r") != false) {
  //    fclose(frun);
    firstrun++;
  }
  //  control that a free number is available
  //  if(firstrun==maxnrun) {
  //    cout<<"Maximum number if run exceeded!!!"<<endl;
  //    return -1; 
  //  }
  // this is the last run!!
  Int_t lastrun = firstrun + nrun;
  //start with the run loop
  for(Int_t irun = firstrun; irun<lastrun; irun++)
    singlerun(irun);
  return 1;
}


void DefineRec(TTree *rec) {
  // Define Tree to store output
  /* Data structure: 
     Run number
     Event number
     Trigger type
     Number of hits
     Terasic Board Number
     Chip number
     Channel number
     Threshold
     Time over threshold
     DC-DRS waveforms
  */
  rec->Branch("run"   ,&run      ,"run/I");  
  rec->Branch("eve"   ,&eve      ,"eve/I");
  rec->Branch("typ"   ,&typ      ,"typ/I");
  rec->Branch("nhit"  ,&nhit     ,"nhit/I");
  rec->Branch("boa"   ,board     ,"boa[nhit]/I"); 
  rec->Branch("chi"   ,hitchip   ,"chi[nhit]/I"); 
  rec->Branch("cha"   ,chnum     ,"cha[nhit]/I");
  rec->Branch("tot "  ,TOT       ,"tot[nhit]/I");
  rec->Branch("tst "  ,TimStamp  ,"tst[nhit]/I");
  rec->Branch("pos "  ,posstrip  ,"pos[nhit]/D");
  rec->Branch("len "  ,lenstrip  ,"len[nhit]/D");
  rec->Branch("cen "  ,censtrip  ,"cen[nhit]/D");
  // DRS waveforms if it is used
  if(runpar->Getisdrs()==1){
    rec->Branch("DCWaveform","TClonesArray",&wf);
  }// end if isdrs
  if(runpar->Getiscaen()==1){
    rec->Branch("wfm"    ,wfm      ,"wfm[5120]/I");
    rec->Branch("endcell",&endcell ,"endcell/I"); 
  }// end if iscaen
  if(runpar->Getisvme()) {
    rec->Branch("triwfm",triwfm ,"triwfm[128]/I"); 
    rec->Branch("triadd",&triadd ,"triadd/I"); 
  }
}

void initTelescope(){
  //
  if(runpar->Getnterasic() == 0) {
    cout << "No Terasic Board to initialize" << endl;
  }
  else {
    cout<<"Start Terasic initialization"<<endl;
  }
  // set server
  server = new Server();
  server->SetSockServer();
  server->SetServer(); 
  // configure all the AToM chips 
  for(int iter = 0; iter<runpar->Getnterasic(); iter++) {    
  // create the AToM chips 
    for(int ichip = 0; ichip<nchip; ichip++) {
      atom[iter][ichip] = new AToM(ichip,iter);
    }
    terasic[iter] = new Terasic(iter,atom[iter]); 
    terasic[iter]->SetSockClient();
    TString ip = Form("10.0.0.%d",iter+11); 
    TString port = "8888";
    terasic[iter]->SetClient(ip,port);
  }
  udp = new UDP(server, terasic);
    for( int iter = 0; iter<runpar->Getnterasic(); iter++) {
    // set the busy to 1 for all the Terasic boards to prevent any trigger to be 
    // fired to AToM
    udp->SendCommand(iter,regsetbusy,0);
    // now set all the bitmasks on terasic boards
    udp->SendCommand(iter,regbitmask, bitmask);
    // and the trigger latency
    udp->SendCommand(iter,regtrglate, trglate);
    // issue a Master Reset in broadcast, to p and n side
    udp->SendCommand(iter,ctrl_p, (0xf<<8) | MasterRes);
    udp->SendCommand(iter,ctrl_n, (0xf<<8) | MasterRes);
    //
    // set all the AToM chips to running condition
    for(int ichip = 0; ichip<nchip; ichip++) {
      AToM *chiptoconfig = terasic[iter]->GetAToM(ichip);
      // set the configuration register for all the chip 
      int temp = 0;
      do {
        if(ichip<5) { // then it is n-sided chip
          // first load the control register word on the register
          udp->SendCommand(iter, data0_n, chiptoconfig->GetConfigreg());
          // then send the command to the AToM
          udp->SendCommand(iter, ctrl_n, (chiptoconfig->GetChipId()<<8) | WCtrlReg);
          // finally read back the configuration register and check that it good
          //first issue an AToM read register command
          udp->SendCommand(iter, ctrl_n, (chiptoconfig->GetChipId()<<8) | ReadReg);
          // then read the FIFO and cross check the data
          udp->ReadData(iter,chiptoconfig->GetFIFOreg(), 
                        terasic[iter]->GetModType());
        }
        else {  // it is a p-sided chip
          // first load the control register word on the register
          udp->SendCommand(iter, data0_p, chiptoconfig->GetConfigreg());
          // then send the command to the AToM
          udp->SendCommand(iter, ctrl_p, (chiptoconfig->GetChipId()<<8) | WCtrlReg);
          // finally read back the configuration register and check that it good
          //first issue an AToM read register command
          udp->SendCommand(iter, ctrl_p, (chiptoconfig->GetChipId()<<8) | ReadReg);
          // then read the FIFO and cross check the data
          udp->ReadData(iter,chiptoconfig->GetFIFOreg(),
                        terasic[iter]->GetModType());         
        }
      } while (((ReadCalDAC != DefCalDac) || 
                (ReadCalThr != chiptoconfig->GetThr()) || 
                (ReadRiseTime != DefRiseTime) ||
                (ReadJitter != DefMaxJitterTime) || 
                (Chip != chiptoconfig->GetChipId())) && temp++<100);
      if (temp>=10) printf("Timeout in configuring chip %d\n", ichip);    
        // masks    
      if(ichip<5) { // then it is n-sided chip
        // first load the masks registers
        udp->SendCommand(iter,data0_n,chiptoconfig->GetMask0());
        udp->SendCommand(iter,data1_n,chiptoconfig->GetMask1());
        udp->SendCommand(iter,data2_n,chiptoconfig->GetMask2());
        udp->SendCommand(iter,data3_n,chiptoconfig->GetMask3());
        udp->SendCommand(iter,data4_n,chiptoconfig->GetMask4());
        udp->SendCommand(iter,data5_n,chiptoconfig->GetMask5());
        udp->SendCommand(iter,data6_n,chiptoconfig->GetMask6());
        udp->SendCommand(iter,data7_n,chiptoconfig->GetMask7());
        // then send the command
        udp->SendCommand(iter,ctrl_n,(chiptoconfig->GetChipId()<<8) | WCalMask);
      }
      else { //it is p-sided chip
        // first load the masks registers
        udp->SendCommand(iter,data0_p,chiptoconfig->GetMask0());
        udp->SendCommand(iter,data1_p,chiptoconfig->GetMask1());
        udp->SendCommand(iter,data2_p,chiptoconfig->GetMask2());
        udp->SendCommand(iter,data3_p,chiptoconfig->GetMask3());
        udp->SendCommand(iter,data4_p,chiptoconfig->GetMask4());
        udp->SendCommand(iter,data5_p,chiptoconfig->GetMask5());
        udp->SendCommand(iter,data6_p,chiptoconfig->GetMask6());
        udp->SendCommand(iter,data7_p,chiptoconfig->GetMask7());
        // then send the command
        udp->SendCommand(iter,ctrl_p,(chiptoconfig->GetChipId()<<8) | WCalMask);
      }
    } // end for ichip for chip calibration and mask set up
    cout<<"Terasic "<<iter+1<<" with ip address "<<terasic[iter]->GetIp()<<" configured!"<<endl;
  }// end for nterasic
  if(runpar->Getisvme()) {
    cout<<" NOW open VME connection and configure Type1 board... "<<endl;
    /* open VME */
    if ((crate = open("/dev/sis1100_00remote", O_RDWR, 0)) < 0) { 
      printf("error on opening VME environment 0x%08x  \n",crate);
      return_code++;
    }
    printf("    crate = 0x%08x  \n",crate);
    
    /* reset del VME */
    return_code = vmesysreset(crate);
    if (return_code) {
      printf("error:   return_code = 0x%08x  \n",return_code);
    }
    cout<<"... VME opened!"<<endl;
    //now configure the Type1 board
    type1 = new Type1(0x69);
    // loop on used channels to set the baseline value
    type1->RoboConf();
    for(int icha = 0; icha<2; icha++) {
      type1->SetDAC(50,icha);
    }
    type1->SetThresholds();
    //end of VME config 
  }//endif isVME
  // finally read the survey data file
  FILE *surfil = fopen("survey.dat","r");
  for(int imod = 0; imod<4; imod++) {
    for(int iread = 0; iread<3; iread++) {
      if(!iread)
        fscanf(surfil,"%*s");
      else
        fscanf(surfil,"%lf %lf %lf", survey[imod][iread-1],
               survey[imod][iread-1]+1,survey[imod][iread-1]+2);
    }//end for iread
  }//end for imod
  return;
}// end initTelescope

#if defined(HAVE_DRS)
void initDRS() {  
  //
  nDRSchip = 4;
  nDRSchannel = 4;
  //
  float freq = runpar->Getdrsfreq();
  //
  drs = new DRS(crate);
  nBoards = drs->GetNumberOfBoards();
  //
  cout << "Found " << nBoards << " boards" << endl;
  //
  boards = new DRSBoard*[nBoards];
  DRStimes = new float*[nBoards];
  DRSwaves = new float*[nBoards*nDRSchip*nDRSchannel];
  //
  /* show any found board(s) */
  for (int i=0 ; i<nBoards ; i++) {
    boards[i] = drs->GetBoard(i);
    printf("Found DRS4 evaluation board, serial #%d, firmware revision %d\n",  boards[i]->GetBoardSerialNumber(), boards[i]->GetFirmwareVersion());
    /* initialize board */
    boards[i]->Init(); 
    /* set sampling frequency */
    boards[i]->SetFrequency(freq, true);
    printf("\tFrequency for board 0 set to %f\n",freq);
    /* enable transparent mode needed for analog trigger */
    boards[i]->SetTranspMode(1);
    /* set input range to -0.5V ... +0.5V */
    boards[i]->SetInputRange(0.45);
    /* use following line to enable external hardware trigger (Lemo) */
    boards[i]->EnableTrigger(1,1);
    //boards[i]->SetTriggerDelayNs(450);
    DRStimes[i] = new float[1024];
  }// end for nboards

  for(int j=0;j<nBoards*nDRSchip*nDRSchannel;j++) DRSwaves[j] = new float[1024];

}//end init DRS
 
void readDRS() {
  int status;
  //
  /* read all waveforms */
  for(int i=0;i<nBoards;i++){
    //
    boards[i]->IsVoltageCalibrationValid();
    /*
    if(!boards[i]->IsVoltageCalibrationValid()) {
      cout << "Calibration of board " << i << " not valid -- board not read" << endl; 
      continue;
    }
    */
    //
    boards[i]->TransferWaves();
    //
    //boards[i]->GetTime(0,boards[i]->GetTriggerCell(0),DRStimes[i]);
    for(Int_t bb=0;bb<1024;bb++) DRStimes[i][bb] = bb;
    //
    for(int ichip=0;ichip<nDRSchip;ichip++) {
      //
      for(int ichannel=0;ichannel<nDRSchannel;ichannel++) {
        //
        int idx = i*nDRSchip*nDRSchannel + ichip*nDRSchannel + ichannel;
        //
        status = boards[i]->GetWave(ichip,2*ichannel,DRSwaves[idx],true,boards[i]->GetTriggerCell(ichip),boards[i]->GetStopWSR(ichip));
        //short swaveform[1024];
        //status = boards[i]->GetWave(ichip,2*ichannel,swaveform,0,0);
        //for(Int_t bb=0;bb<1024;bb++) DRSwaves[idx][bb] = swaveform[bb];
        //
        if(status!=kSuccess) cout << "Error in GetWave with code " << status << endl;
        //
        new(wf[idx]) TGraph(1024,DRStimes[i],DRSwaves[idx]);
        /*
        TApplication *app = new TApplication("app",0,NULL);

        TCanvas *c1 = new TCanvas("c1");
        wf[idx]->Draw("ALP");
        c1->Update();
        c1->WaitPrimitive();

        app->Run(true);
        */
        //
      }// end for nDRSchannels
    }// end for nDRSchip
  }// end for nBoards
}//end Read DRS

void removeDRSBusy(){
  //restart the domino
  for(int i=0;i<nBoards;i++) boards[i]->StartDomino(); //is it enough??? to be checked
}
#endif //HAVE_DRS

void singlerun(Int_t irun) {
  bool result; // false if there is a error in telescope read out
  TFile *filout = new TFile(Form("/home/data/run%d.root",irun),"RECREATE");
  rec = new TTree("rec","rec");  
  run = irun;
  DefineRec(rec);
  int ieve = 0;
  // first the begin of run:
  // reset event counter in trigger board
  if(runpar->Getisvme())  type1->Sync();
  // send clear readout and sync to all AToM chips
  for(int iter = 0; iter<runpar->Getnterasic(); iter++) {
    // first n-side 
    udp->SendCommand(iter, ctrl_n, ClearReadout);
    udp->SendCommand(iter, ctrl_n, SyncComm);
    // then p-side
    udp->SendCommand(iter, ctrl_p, ClearReadout);
    udp->SendCommand(iter, ctrl_p, SyncComm);
    // then remove the busy from the boards
    udp->SendCommand(iter, regremovebusy, 0);
  }
  // begin of run for DRS....
  if(runpar->Getisdrs()==1) {
    for (int i=0 ; i<nBoards ; i++) boards[i]->StartDomino();
  }
  //
  //here the events!!!
  int status = 1;
  do {    
    // enable the CAEN digitizer to receive a trigger
    if(runpar->Getiscaen()) {
      status = caendigi->PollEvent();
      while (status%2) {
        printf("WARNING! The CAEN interrupt register has not been cleared, content = %x\n",status); 
        caendigi->Acknowledge();
        status = caendigi->PollEvent();
      }
      caendigi->StartDAQ();
    }
    // clear the busy and start DAQ!
    if(runpar->Getisvme()) type1->GoRun();
    //POLL EVENT
    //on trigger Type1 board:
    // read bit #12 on RRUN register when 0 it means we had a trigger!
    u_int32_t iseve = 0;
    do {
      //POLL EVENT ON TYPE1
      /*      if(runpar->Getiscaen()) {
              iseve = caendigi->PollEvent();
              iseve&=0x1; 
              }*/
      if(runpar->Getisvme()) {
        iseve = type1->PollEvent(); //when iseve = 0 then we have a trigger
      }
    } while(iseve == 1);

    //poll on INTERRUPT on V1729
    if(runpar->Getiscaen()==1) {
      Int_t ic = 0;
      do { 
        status = caendigi->PollEvent();
      } while ( (++ic < 20000) && (status%2 == 0) ); 
        //      } while (status%2 == 0); 

      if (ic>=20000){ printf("Warning, CAEN empty for event %d\n",ieve);
        for(Int_t iwfm = 0; iwfm<5120; iwfm++) {
          wfm[iwfm] = 0;
          endcell = -1;
        }
      }
      else {      caendigi->Acknowledge();
        caendigi->ReadWfm(wfm,&endcell);
      }
      //      cout << "Endcell = " << endcell << flush << endl;
    }

    // Telescope read out
    for(int iter = 0; iter<runpar->Getnterasic(); iter++) {
      //      cout<<" Trigger! Reading Terasic "<<iter<<endl;
      // send the read out command to n side
      udp->SendCommand(iter, ctrl_n, ReadEvent);  
      // and then read the data, the FIFO address is common for all n-side 
      result = udp->ReadData(iter, terasic[iter]->GetAToM(0)->GetFIFOreg(), 
                             terasic[iter]->GetModType());      
      // send the read out command to p side
      udp->SendCommand(iter, ctrl_p, ReadEvent);
      // and then read the data, the FIFO address is common for all p-side 
      result &= udp->ReadData(iter, terasic[iter]->GetAToM(5)->GetFIFOreg(),
                              terasic[iter]->GetModType());
    }// end for iter
#if defined(HAVE_DRS)
    // DRS read out 
    if(runpar->Getisdrs()==1) {
      readDRS();
    }// end if isdrs
#endif
    // if isvme then Type1 read out
    type1->ReadRam(triwfm,&triadd);
    // Remove all busy
    // DRS Board
#if defined(HAVE_DRS)
    if(runpar->Getisdrs()==1) {
      removeDRSBusy();
    }
#endif
    //
    // Terasic boards
    for(int iter = 0; iter<runpar->Getnterasic(); iter++) {
      // then remove the busy from the boards
      udp->SendCommand(iter, regremovebusy, 0);
    }// end for iter
    //
    eve = ieve;
    // get the number of hits
    nhit = vboard->size();
    // now fill the vectors
    for (int ihit = 0; ihit<nhit; ihit++) {
      board[ihit] = vboard->at(ihit);
      hitchip[ihit] = vhitchip->at(ihit);
      chnum[ihit] = vchnum->at(ihit);
      TOT[ihit] = vTOT->at(ihit);
      TimStamp[ihit] = vTimStamp->at(ihit);
      posstrip[ihit] = vpos->at(ihit);
      censtrip[ihit] = vcenter->at(ihit);
      lenstrip[ihit] = vlength->at(ihit);
    }
    // print the run status
    if (eve>0 && !(eve%10)) printf(" event number %d, run number %d\n", eve, irun);
    //
    result = true;

    if(result) { // fill the rec file only if telescope read out is good
      rec->Fill();
    }
    // here event display every 10 events
    if (eve>0 && !(eve%10)) {      
      // first define the arrays, one per hit
      TVector3 *strip[nhit];
      for(int ihit = 0; ihit<nhit; ihit++) {
        strip[ihit] = new TVector3;
        // from Walsh to global coordinates using Walsh's center
        LocalToLocal(posstrip[ihit], censtrip[ihit], hitchip[ihit], strip[ihit]);
        // now refer the system of coordinate to the cross
        int crossid;
        if(strcmp(runpar->Getmodtyp(board[ihit]).Data(),"D4AB") &&
           strcmp(runpar->Getmodtyp(board[ihit]).Data(),"D4BB") &&
           strcmp(runpar->Getmodtyp(board[ihit]).Data(),"D5AB") &&
           strcmp(runpar->Getmodtyp(board[ihit]).Data(),"D5BB")) { //if it is backward...
          crossid = 0;
        }
        else { // it is foreward
          crossid = 1;
        }
        // now put the reference in the survey cross
        LocalToCross(strip[ihit],crossid);
        //for foreward module apply the head to head rotation
        if(crossid==1)
          RotateHeadToHead(strip[ihit]);
        // now apply the survey measurements and put in position
        int modid;
        if(strcmp(runpar->Getmodtyp(board[ihit]).Data(),"D4AB") && 
           strcmp(runpar->Getmodtyp(board[ihit]).Data(),"D4AF") && 
           strcmp(runpar->Getmodtyp(board[ihit]).Data(),"D4BB") && 
           strcmp(runpar->Getmodtyp(board[ihit]).Data(),"D4BF")) //it is layer4 module
          modid = (runpar->Getmask(board[ihit])-1)*2;
        else //it is a layer 5 module
          modid = (runpar->Getmask(board[ihit])-1)*2 + 1; 
        MaskInPlace(strip[ihit],runpar->Getmask(board[ihit]),
                    survey[modid][crossid],runpar->Getmodtyp(board[ihit]),
                    runpar->Getdistance());
      }// end for ihit
      //now event display!
      double tmp[3],xhit[nhit],yphi[nhit],yz[nhit],zhit[nhit];
      // extract the hit and put into vectors
      int ngoodphi = 0;
      int ngoodz = 0;
      for(int ihit = 0; ihit<nhit; ihit++) {
        strip[ihit]->GetXYZ(tmp);
        if(TOT[ihit] > 3 && TimStamp[ihit]>4 && TimStamp[ihit]<11) {
          if(hitchip[ihit]>4) { //hit on p-side
            xhit[ngoodphi] = tmp[0];
            yphi[ngoodphi] = tmp[2];
            ngoodphi++;
          }
          else { // hit on z-side
            yz[ngoodz] = tmp[1];
            zhit[ngoodz] = tmp[2];
            ngoodz++;
          }// end else
        }// end if TOT
      }//end for ihit
      //now define TGraphs and draw them
      TGraph *xy = new TGraph(ngoodphi,xhit,yphi);
      TGraph *zy = new TGraph(ngoodz,zhit,yz);
      eventdisplay->cd(1);
      xy->SetMarkerStyle(20);
      xy->SetMarkerSize(0.5);
      xy->GetXaxis()->SetTitle("x (mm)");
      xy->GetYaxis()->SetTitle("y (mm)");
      if (ngoodphi>0) xy->Draw("AP");
      eventdisplay->cd(2);
      zy->SetMarkerStyle(20);
      zy->SetMarkerSize(0.5);
      zy->GetXaxis()->SetTitle("z (mm)");
      zy->GetYaxis()->SetTitle("y (mm)");
      if (ngoodz) zy->Draw("AP");
      eventdisplay->Update();
      eventdisplay->WaitPrimitive();
      //      cout<<"now going to next event"<<endl;
    }//end every 10 events...
    // now delete all the arrays!
    vboard->erase(vboard->begin(),vboard->end());
    vhitchip->erase(vhitchip->begin(),vhitchip->end());
    vchnum->erase(vchnum->begin(),vchnum->end());
    vTOT->erase(vTOT->begin(),vTOT->end());
    vTimStamp->erase(vTimStamp->begin(),vTimStamp->end());
    vpos->erase(vpos->begin(),vpos->end());
    vcenter->erase(vcenter->begin(),vcenter->end());
    vlength->erase(vlength->begin(),vlength->end());
    if(runpar->Getisdrs()==1)
      wf.Delete();

    // and go for the next event
  } while(ieve++<runpar->Getneve());
  // now write the rec file
  rec->Write();
  // write the logbook file
  runpar->WriteLog(run);
  // delete the rec file for next run
  rec->Delete();
  // close the output file
  filout->Close();
  return;
}

