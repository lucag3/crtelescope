// $Id: frontend.cpp 190 2013-03-29 15:27:15Z galli $
#include <stdio.h>
/* UDP client in the internet domain */
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "driver.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TH1F.h"
#include "TControlBar.h"
#include "TApplication.h"

int argc = 4;

//int main(int argc, char *argv[])
int main()
{
  //  int argc 4;
  char* argv[5];
  int terid;
  printf("which terasic are you using?\n");
  scanf("%d", &terid);
  argv[1] = Form("10.0.0.%d", terid+10);
  argv[2] = "8888"; 
  argv[3] = "4444";
  
  //define fromlen size
  fromlen = sizeof(struct sockaddr_in);
  //       if (argc != 4) { printf("Usage: server port\n");
  //        exit(1);
  //            }
  sock_client= socket(AF_INET, SOCK_DGRAM, 0);
  sock_server= socket(AF_INET, SOCK_DGRAM, 0);
  if (sock_client < 0) error("socket");
  //
  //open UDP connection 
  client.sin_family = AF_INET;
  server.sin_family = AF_INET;
  server.sin_addr.s_addr=INADDR_ANY;
  //
  //get the IP address for client from arguments...
  hp = gethostbyname(argv[1]);
  if (hp==0) error("Unknown host");
  // ...and set to client
  bcopy((char *)hp->h_addr,
        (char *)&client.sin_addr,
        hp->h_length);
  //set client and server ports
  client.sin_port = htons(atoi(argv[2]));
  server.sin_port = htons(atoi(argv[3]));
  //
  length=sizeof(struct sockaddr_in);
  //
  // build the socket server   
  if (bind(sock_server,(struct sockaddr *)&server,length)<0)
    error("binding");
  //

  //enable printing
  enprint = true;

  //Set Default Control Register at Startup
  SetDefaultCR();
  int addr;
  int option = 10;
  while(option>=0) {
    printf("Options:\n");
    printf("[0] Set Bitmask                   \t");
    printf("[1] Set Control Register          \t");
    printf("[2] Master Reset\n");
    printf("[3] Read Register                 \t");
    printf("[4] Read Event                    \t");
    printf("[5] Clear Readout\n");
    printf("[6] Sync                          \t");
    printf("[7] Trigger Accept                \t");
    printf("[8] Calibration Strobe\n");
    printf("[9] Write Control Path Selection  \t");
    printf("[10] Write Cal and Channel Mask   \t");
    printf("[11] Read Data                    \n");
    printf("[12] Clear Busy                   \t");
    printf("[13] Clear FIFO                   \t");
    printf("[14] Clear Event Counter \n");
    printf("[15] Set respond to read          \t");
    printf("[97] Fix side                     \t");
    printf("[98] Unfix side                   \n");
    printf("[99] Test                         \t");
    printf("[-1] exit\n");
    scanf("%d",&option);
    bzero(buffer,256);
    if(option>0&&option<11) {
      if(!fixed) {
        printf("Which side?\n 0 = p side gpio0\n 1 = n side gpio0\n 2 = p side gpio1\n 3 = n side gpio1\n");
        scanf("%d", &side);
      }
      if ((option == 1 || option == 2 || option == 3 || option == 10)){
        printf("Which chip? [f for broadcast (only if possible!)]\n");
        scanf("%x", &chip);
      }
      //
    }
    switch(option) {
    case 0: SetBitmask();
      break;
    case 1: SetCRregister();
      break;
    case 2: MasterReset();
      break;
    case 3: RegRead();
      break;
    case 4: EventRead();
      break;
    case 5: ClearRead();
      break;
    case 6: Sync();
      break;
    case 7: TrigAccept();
      break;
    case 8: CalStrobe(); 
      break;
    case 9: SetCtrlPath();
      break;
    case 10: WriteCalMask();
      break;
    case 11:     printf("Type FIFO address\n0 = inctrl \n1 = gpio_0_p \n2 = gpio_0_n \n3 = gpio_1_p \n4 = gpio_1_n\n");
      scanf("%d", &addr);
      ReadData(addr,NULL);
      break;
    case 12: ClearBusy();
      break;
    case 13: ClearFIFO();
      break;
    case 14: ClearEveCou();
      break;
    case 15: SetMaster(0x20);
      break;
    case 97:    printf("Which side?\n 0 = p side gpio0\n 1 = n side gpio0\n 2 = p side gpio1\n 3 = n side gpio1\n");
        scanf("%d", &side);
        fixed = true;
      break;
    case 98: fixed = false;
      break;
    case 99: Test();
      break;
    default: break;
    } 
  }
  close(sock_client);
  close(sock_server);
  return 1;
   }
