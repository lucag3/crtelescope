// $Id: AToMLoop.h 281 2014-07-08 16:49:00Z galli $
#ifndef AToMLoop_h
#define AToMLoop_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>


// Header file for the classes stored in the TTree if any.

// Fixed size dimensions of array or collections stored in the TTree if any.

class AToMLoop {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   Int_t           eve;
   Int_t           typ;
   Int_t           boa;
   Int_t           sid;
   Int_t           chi;
   Int_t           nhit;
   Int_t           cha;
   UShort_t        jit;
   UShort_t        rtime;
   UShort_t        dac;
   UShort_t        thr;
   UShort_t        trl;
   Int_t           tot;
   Int_t           skctrl;
   Int_t           tst;
   Int_t           neverf;
   Int_t           nevthr;

   Int_t board, day, month, year;

   // List of branches
   TBranch        *b_eve;   //!
   TBranch        *b_typ;   //!
   TBranch        *b_boa;   //!
   TBranch        *b_sid;   //!
   TBranch        *b_chi;   //!
   TBranch        *b_nhit;   //!
   TBranch        *b_cha;   //!
   TBranch        *b_jit;   //!
   TBranch        *b_rtime;   //!
   TBranch        *b_dac;   //!
   TBranch        *b_thr;   //!
   TBranch        *b_trl;   //!
   TBranch        *b_tot;   //!
   TBranch        *b_skctrl;   //!
   TBranch        *b_tst;   //!
   TBranch        *b_neverf;   //!
   TBranch        *b_nevthr;   //!

   AToMLoop() {;};
   AToMLoop(Int_t board, Int_t day, Int_t month, Int_t year, TTree *tree=0);
   virtual ~AToMLoop();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef AToMLoop_cxx
AToMLoop::AToMLoop(Int_t iboard, Int_t iday, Int_t imonth, Int_t iyear, TTree *tree) : fChain(0) 
{
  board = iboard;
  day   = iday;
  month = imonth;
  year = iyear;
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {

#ifdef SINGLE_TREE
      // The following code should be used if you want this class to access
      // a single tree instead of a chain
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("data/board0-std4-sideP.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("data/board0-std4-sideP.root");
      }
      f->GetObject("rec",tree);

#else // SINGLE_TREE

      // The following code should be used if you want this class to access a chain
      // of trees.
      TChain * chain = new TChain("rec","");
      chain->Add(Form("/home/data/board%d-side0_%02d%02d%02d.root/rec", board,day,month,year));
      chain->Add(Form("/home/data/board%d-side1_%02d%02d%02d.root/rec", board,day,month,year));
      tree = chain;
#endif // SINGLE_TREE

   }
   Init(tree);
}

AToMLoop::~AToMLoop()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t AToMLoop::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t AToMLoop::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void AToMLoop::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("eve", &eve, &b_eve);
   fChain->SetBranchAddress("typ", &typ, &b_typ);
   fChain->SetBranchAddress("boa", &boa, &b_boa);
   fChain->SetBranchAddress("sid", &sid, &b_sid);
   fChain->SetBranchAddress("chi", &chi, &b_chi);
   fChain->SetBranchAddress("nhit", &nhit, &b_nhit);
   fChain->SetBranchAddress("cha", &cha, &b_cha);
   fChain->SetBranchAddress("jit", &jit, &b_jit);
   fChain->SetBranchAddress("rtime", &rtime, &b_rtime);
   fChain->SetBranchAddress("dac", &dac, &b_dac);
   fChain->SetBranchAddress("thr", &thr, &b_thr);
   fChain->SetBranchAddress("trl", &trl, &b_trl);
   fChain->SetBranchAddress("tot", &tot, &b_tot);
   fChain->SetBranchAddress("skctrl", &skctrl, &b_skctrl);
   fChain->SetBranchAddress("tst", &tst, &b_tst);
   fChain->SetBranchAddress("nevthr", &nevthr, &b_nevthr);
   fChain->SetBranchAddress("neverf", &neverf, &b_neverf);
   Notify();
}

Bool_t AToMLoop::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void AToMLoop::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t AToMLoop::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef AToMLoop_cxx
