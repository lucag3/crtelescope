// $Id: driver.h 190 2013-03-29 15:27:15Z galli $
#include "TTree.h"

//list of variables
int sock_client;
int sock_server;
int n, m, bitmask;
unsigned int length;
struct sockaddr_in client;
struct sockaddr_in server;
struct sockaddr_in from;
struct hostent *hp;
char buffer[256];
int chip, side, board, ClkCmdMask, MRdata;
socklen_t fromlen;
char* AToM_answ = "WRITE_ATOM_REG_DONE";char* AToM_answ2 = "READ_ATOM_DATA_DONE";

//list of Control Register parameter
int ThreshDac;
int CalDac;
int MaxJitterTime;
int RiseTime;
int SamplingRate;
int SkipControl;
int ReadDirection;
int RespondRead;
int ClkThru;
int DigTestPatrn;
int SelectClk;
int ClkStatus;
int CRdata;

//Control Register default parameters values
int DefThreshDac = 0x14;
int DefCalDac = 0x20;
int DefMaxJitterTime = 0x8;
int DefRiseTime = 0x3;
int DefSamplingRate = 0x0;
int DefSkipControl = 0x0;
int DefReadDirection = 0x0;
int DefRespondRead = 0x0;
int DefClkThru = 0x0;
int DefDigTestPatrn = 0xa;
int DefSelectClk = 0x0;
int DefClkStatus = 0x0;

unsigned short data[100000];
int datadim; 

bool fixed = false;

//list of variables for DAQ
int eve, typ;
short ReadCalThr, ReadCalDAC, ReadRiseTime, ReadJitter;
short CALDAC, CALTHR, RISETIME, JITTER, TRL;
int nhit;
int chnum, TOT, TimStamp, achip, SKCTRL;

//list of address

// p-side gpio0
#define data0_p_0 0x008
#define data1_p_0 0x010
#define data2_p_0 0x018
#define data3_p_0 0x020
#define data4_p_0 0x028
#define data5_p_0 0x030
#define data6_p_0 0x038
#define data7_p_0 0x040
#define ctrl_p_0  0x048

// n-side gpio0
#define data0_n_0 0x050
#define data1_n_0 0x058
#define data2_n_0 0x060
#define data3_n_0 0x068
#define data4_n_0 0x070
#define data5_n_0 0x078
#define data6_n_0 0x080
#define data7_n_0 0x088
#define ctrl_n_0  0x090

// p-side gpio1
#define data0_p_1 0x098
#define data1_p_1 0x0A0
#define data2_p_1 0x0A8
#define data3_p_1 0x0B0
#define data4_p_1 0x0B8
#define data5_p_1 0x0C0
#define data6_p_1 0x0C8
#define data7_p_1 0x0D0
#define ctrl_p_1  0x0D8

// n-side gpio1
#define data0_n_1 0x0E0
#define data1_n_1 0x0E8
#define data2_n_1 0x0F0
#define data3_n_1 0x0F8
#define data4_n_1 0x100
#define data5_n_1 0x108
#define data6_n_1 0x110
#define data7_n_1 0x118
#define ctrl_n_1  0x120

// bitmask address
#define bitmask_addr 0x400

// Data_Line_A
// CLK_THROUGH_MODE
#define ClkThrough_A 0x2B108814
// DEFAULT_MODE
#define Default 0x29108814

// Data_Line_B
// CLK_THROUGH_MODE
#define ClkThrough_B 0x2B108814
// DEFaULT_MODE
#define Default_B 0x29108814

#define ClearReadout  0x01
#define SyncComm      0x02
#define TrgAcc        0x03
#define ReadEvent     0x04
#define CalStrobeComm 0x05
#define WCtrlPath     0x1A
#define WCtrlReg      0x1B
#define WCalMask      0x1C
#define ReadReg       0x1D
#define MasterRes     0x1E

// list of function prototypes
void error(const char *);
void SetCtrlPath();
void WriteCalMask(int type);
void ClearRead();
void Sync();
void TrigAccept();
void CalStrobe();
void SetCRregister();
void MasterReset();
void RegRead();
void WriteReg(int chip,int side,int Command);
void WriteRegGlobal(int side,int Command);
void SetRegister (int side, int CRdata);
void EventRead();
void Test();
void ClearBusy();
void SetBusy();
void ClearEveCou();
void ClearFIFO();
void SendCalMaskString();
Int_t ReadData(int addr, TTree *rec);
void SetBitmask();
void SetCalParameter(short calibthr, short calibdac, short rtime, short jitter);
void SetTriggerLatency(int lat);
void ChipConfig(int nchip, int calibdac, int calibthr, int rtime, int grayjitter, int skctrl, int *msk);

//enables the printf fro event read...
bool enprint;

//list of Control Register parameter setting function
void SetClk();
void SetClkThru();
void SetReadDirection();
void SetRiseTime();
void SetSamplingRate();
void SetThreshDac();
void SetDigTestPatrn();
void SetRespndRead();
void SetSkipControl();
void SetMaxJitterTime();
void SetCalDac();
void SetDefaultCR();
Int_t BufferInterpreter(TTree *);
void RegInterpreter();
int GraySwitch(int i);
int GraySwitchBack(int i);


void SetDefaultCR(){
  //Set Control Register parameters to default values
  ThreshDac = DefThreshDac;
  CalDac = DefCalDac;
  MaxJitterTime = DefMaxJitterTime;
  RiseTime = DefRiseTime;
  SamplingRate = DefSamplingRate;
  SkipControl = DefSkipControl;
  ReadDirection = DefReadDirection;
  RespondRead = DefRespondRead;
  ClkThru = DefClkThru;
  DigTestPatrn = DefDigTestPatrn;
  SelectClk = DefSelectClk;
  SelectClk = 1;
  ClkStatus = DefClkStatus;
}

void SetCRregister(){

  
  int flag =1;  

  while (flag>=0){
    printf("Setting Control Register:\n");
    printf("[0] Set Default Control Register\n");
    printf("[1] Select Clk/Cmd line\n");
    printf("[2] Set TPE Pattern\n");
    printf("[3] Clock Through mode\n");
    printf("[4] Set Respond to Read \n");
    printf("[5] Set Read Direction\n");
    printf("[6] Set TOT Skip Control\n");
    printf("[7] Sampling Rate frequecy\n");
    printf("[8] Rise Time\n");
    printf("[9] Set Max Jitter Time\n");
    printf("[10] Voltage in Cinj\n");
    printf("[11] Threshold voltage\n");
    printf("[-1] Write Control Register\n");
    scanf("%d",&flag);
  
  
    switch(flag){
    case 0: SetDefaultCR();
      break;
    case 1: SetClk();
      break;
    case 2: SetDigTestPatrn();
      break;        
    case 3: SetClkThru();
      break;
    case 4: SetRespndRead();
      break;
    case 5: SetReadDirection();
      break;
    case 6: SetSkipControl();
      break;
    case 7: SetSamplingRate();
      break;
    case 8: SetRiseTime();
      break;
    case 9: SetMaxJitterTime();
      break;    
    case 10: SetCalDac();
      break;
    case 11: SetThreshDac();
      break;
    default: break; 
    }
  }
 
  CRdata = 0;
  CRdata = (ClkStatus << 31)|(SelectClk << 30)| (DigTestPatrn << 26) | (ClkThru << 25) | (RespondRead << 24)
    | (ReadDirection << 23) | (SkipControl << 21) | (SamplingRate << 19) | (RiseTime << 17) | (MaxJitterTime << 12)
    | (CalDac << 6) | (ThreshDac);
  //Write CRdata in the ctrl
  SetRegister(side, CRdata);
  //Write
  WriteReg(chip, side, WCtrlReg);
  //printf("\nCR data = %x \n", CRdata);
 
}


void SetClk(){
  printf("Select Clk/Cmd line \n1 = A [default]\n0 = B\n");
  scanf("%d", &SelectClk);
}

void SetDigTestPatrn(){
  printf("Set Digital Test Pattern Enable - 4 bits \n0000 = nowrite \n1010 = every other [default]\netc...\n");
  scanf("%d", &DigTestPatrn);   
}

void SetRespndRead(){
  printf("Set Respond to Read bit \n0 = no \n1 = yes [default]\n");
  scanf("%d", &RespondRead);        
}
    
void SetClkThru(){
  printf("Set clock through mode \n0 for off [default] \n1 for on\n");
  scanf("%d", &ClkThru);
}

void SetReadDirection(){
  printf("Set Read Direction \n0 = left [default] \n1 = right \n");
  scanf("%d", &ReadDirection);
}
    
void SetSkipControl(){
  printf("Set Skip Control \n00 = no skip [default] \n01 = every 2nd \n10 = every 3rd \n11 = every 4th\n");
  scanf("%d", &SkipControl);
}
    
    
void SetSamplingRate(){
  printf("Set sampling rate frequecy \n00 = 1/2 of imput clock freq \n01 = 1/3 \n10 = 1/4 [default] \n11 = no write\n");
  scanf("%d", &SamplingRate);
}

void SetRiseTime(){
  printf("Set nominal rise time \n00 = 100 ns [default] \n01 = 200 ns \n10 = 300 ns \n11 = 400 ns\n");
  scanf("%d", &RiseTime);
}

void SetMaxJitterTime(){
  printf("Set Jitter Time \nfive bits - LSB = 67ns \n01000 = 1 µs at 15 MHz [default]\n");
  scanf("%x", &MaxJitterTime);
}


void SetCalDac(){
  printf("Set voltage in Cinj \nsix bits - 10mV/bit \n100000 = 320 mV [default]\n");
  scanf("%x", &CalDac);
}

void SetThreshDac(){
  printf("Set Threshold voltage \nsix bits - 7.5(12.5) mV/bit \n010100 150(250) mV[default]\n");
  scanf("%x", &ThreshDac);
}




void error(const char *msg)
{
  perror(msg);
  exit(0);
}


void WriteReg(int chip,int side,int Command){
  switch(side) {
  case 0: sprintf(buffer,"! WRITE_ATOM_REG %x %x", (chip<<8)| Command, ctrl_p_0);
    break;   
  case 1: sprintf(buffer,"! WRITE_ATOM_REG %x %x", (chip<<8)| Command, ctrl_n_0);
    break;   
  case 2: sprintf(buffer,"! WRITE_ATOM_REG %x %x", (chip<<8)| Command, ctrl_p_1);
    break;   
  case 3: sprintf(buffer,"! WRITE_ATOM_REG %x %x", (chip<<8)| Command, ctrl_n_1);
    break;
  }
  //
  n=sendto(sock_client,buffer,
       strlen(buffer),0,(const struct sockaddr *)&client,length);
  //receive Terasic answer
  m = recvfrom(sock_server,buffer,1024,0,(struct sockaddr *)&from,&fromlen);
  //  
  if (strncmp(buffer,AToM_answ,19)){
    printf("communication error \n%s \n%s \n", buffer, AToM_answ);
  }
  
  //  m = recvfrom(sock_server,buffer,1024,0,(struct sockaddr *)&from,&fromlen);
  //    write(1,"Received a datagram: ",21);
  //    write(1,buffer,n);
}


void WriteRegGlobal(int side,int Command){
  switch(side) {
  case 0: sprintf(buffer,"! WRITE_ATOM_REG %x %x", Command, ctrl_p_0);
    break;   
  case 1: sprintf(buffer,"! WRITE_ATOM_REG %x %x", Command, ctrl_n_0);
    break;   
  case 2: sprintf(buffer,"! WRITE_ATOM_REG %x %x", Command, ctrl_p_1);
    break;   
  case 3: sprintf(buffer,"! WRITE_ATOM_REG %x %x", Command, ctrl_n_1);
    break;
  }
  //
  n=sendto(sock_client,buffer,
       strlen(buffer),0,(const struct sockaddr *)&client,length);
  //receive Terasic answer
  m = recvfrom(sock_server,buffer,1024,0,(struct sockaddr *)&from,&fromlen);
  //  
  if (strncmp(buffer,AToM_answ,19)){
    printf("communication error \n%s \n%s \n", buffer, AToM_answ);
  }
  
  //  m = recvfrom(sock_server,buffer,1024,0,(struct sockaddr *)&from,&fromlen);
  //    write(1,"Received a datagram: ",21);
  //    write(1,buffer,n);
}

void SetRegister (int side, int CRdata) {
  //Set Control Register Data word
  switch(side) {
  case 0: sprintf(buffer,"! WRITE_ATOM_REG %x %x", CRdata, data0_p_0);
    break;   
  case 1: sprintf(buffer,"! WRITE_ATOM_REG %x %x", CRdata, data0_n_0);
    break;   
  case 2: sprintf(buffer,"! WRITE_ATOM_REG %x %x", CRdata, data0_p_1);
    break;   
  case 3: sprintf(buffer,"! WRITE_ATOM_REG %x %x", CRdata, data0_n_1);
    break;
  }
  //Send packet
  n=sendto(sock_client,buffer,
       strlen(buffer),0,(const struct sockaddr *)&client,length);
  //receive Terasic answer
  m = recvfrom(sock_server,buffer,1024,0,(struct sockaddr *)&from,&fromlen);
 
  if (strncmp(buffer,AToM_answ,19)){
    printf("communication error \n%s \n%s \n", buffer, AToM_answ);
  }
}

void MasterReset() {
  //set the command word to master reset
  WriteReg(chip, side, MasterRes); 
}

void ClearRead() {
  //set the command word to clear readout
  WriteRegGlobal(side, ClearReadout); 
}

void Sync() {
  //set the command word to sync
  WriteRegGlobal(side, SyncComm); 
}

void TrigAccept() {
  // first of all clear of the busy
  ClearBusy();
  //set the command word to L1 Trigger Accept
  WriteRegGlobal(side, TrgAcc); 
}

void ClearBusy(){
  sprintf(buffer,"! WRITE_ATOM_REG %x %x", 0, 0x80000000);
  //
  n=sendto(sock_client,buffer,
       strlen(buffer),0,(const struct sockaddr *)&client,length);
  //receive Terasic answer
  m = recvfrom(sock_server,buffer,1024,0,(struct sockaddr *)&from,&fromlen);
  if(enprint)
    printf("%s \n", buffer);
 
  return;
}


void SetBusy(){
  sprintf(buffer,"! WRITE_ATOM_REG %x %x", 1, 0x80000000);
  //
  n=sendto(sock_client,buffer,
       strlen(buffer),0,(const struct sockaddr *)&client,length);
  //receive Terasic answer
  m = recvfrom(sock_server,buffer,1024,0,(struct sockaddr *)&from,&fromlen);
  if(enprint)
    printf("%s \n", buffer);
 
  return;
}

void CalStrobe() {
  // first clear the busy
  ClearBusy();
  //set the command word to Cal Strobe
  WriteRegGlobal(side, CalStrobeComm); 
}

void RegRead() {
  //set the command word
  WriteReg(chip, side, ReadReg);
  
}
//
void EventRead() {
  //set the command word
  WriteRegGlobal(side, ReadEvent);
}

void SendCalMaskString(){
  n=sendto(sock_client,buffer,
       strlen(buffer),0,(const struct sockaddr *)&client,length);
  //receive Terasic answer
  m = recvfrom(sock_server,buffer,1024,0,(struct sockaddr *)&from,&fromlen);
  //  
  if (strncmp(buffer,AToM_answ,19)){
    printf("communication error \n%s \n%s \n", buffer, AToM_answ);
  }
}

void SetBitmask(){
  bitmask = 0x0000000a;
  //write bitmask
  sprintf(buffer,"! WRITE_ATOM_REG %x %x", bitmask, bitmask_addr);
  n=sendto(sock_client,buffer,
       strlen(buffer),0,(const struct sockaddr *)&client,length);
  //receive Terasic answer
  m = recvfrom(sock_server,buffer,1024,0,(struct sockaddr *)&from,&fromlen);
  //  
  if (strncmp(buffer,AToM_answ,19)){
    printf("communication error \n%s \n%s \n", buffer, AToM_answ);
  
  }
}

void WriteCalMask(int type){
  // type == 0 --> all the masks on
  // type == 1 --> online mask 0,4,8 ... etc on
  // type == 2 --> online mask 1,5,9 ... etc on
  // type == 3 --> online mask 2,6,10... etc on
  // type == 4 --> online mask 3,7,11... etc on
  int CalChan; 
  int CalMask;   
  if(type==0) {
    CalMask = 0xffffffff;
    CalChan = 0xffffffff;
  } else if (type==1) {
    CalMask = 0x11111111;
    CalChan = 0x88888888;
  } else if (type==2) {
    CalMask = 0x22222222;
    CalChan = 0x44444444;
  } else if (type==3) {
    CalMask = 0x44444444;
    CalChan = 0x22222222;
  } else if (type==4) {
    CalMask = 0x88888888;
    CalChan = 0x11111111;
  }

  switch(side) {
  case 0:
    sprintf(buffer,"! WRITE_ATOM_REG %x %x", (CalMask), data0_p_0);
    SendCalMaskString();
    sprintf(buffer,"! WRITE_ATOM_REG %x %x", (CalMask), data1_p_0);
    SendCalMaskString();
    sprintf(buffer,"! WRITE_ATOM_REG %x %x", (CalMask), data2_p_0);
    SendCalMaskString();
    sprintf(buffer,"! WRITE_ATOM_REG %x %x", (CalMask), data3_p_0);
    SendCalMaskString();
    sprintf(buffer,"! WRITE_ATOM_REG %x %x", (CalChan), data4_p_0);
    SendCalMaskString();
    sprintf(buffer,"! WRITE_ATOM_REG %x %x", (CalChan), data5_p_0);
    SendCalMaskString();
    sprintf(buffer,"! WRITE_ATOM_REG %x %x", (CalChan), data6_p_0);
    SendCalMaskString();
    sprintf(buffer,"! WRITE_ATOM_REG %x %x", (CalChan), data7_p_0);
    SendCalMaskString();  
     break;   
  case 1: 
    sprintf(buffer,"! WRITE_ATOM_REG %x %x", (CalMask), data0_n_0);
    SendCalMaskString();
    sprintf(buffer,"! WRITE_ATOM_REG %x %x", (CalMask), data1_n_0);
    SendCalMaskString();
    sprintf(buffer,"! WRITE_ATOM_REG %x %x", (CalMask), data2_n_0);
    SendCalMaskString();
    sprintf(buffer,"! WRITE_ATOM_REG %x %x", (CalMask), data3_n_0);
    SendCalMaskString();
    sprintf(buffer,"! WRITE_ATOM_REG %x %x", (CalChan), data4_n_0);
    SendCalMaskString();
    sprintf(buffer,"! WRITE_ATOM_REG %x %x", (CalChan), data5_n_0);
    SendCalMaskString();
    sprintf(buffer,"! WRITE_ATOM_REG %x %x", (CalChan), data6_n_0);
    SendCalMaskString();
    sprintf(buffer,"! WRITE_ATOM_REG %x %x", (CalChan), data7_n_0);
    SendCalMaskString();
     break;   
  case 2: 
    sprintf(buffer,"! WRITE_ATOM_REG %x %x", (CalMask), data0_p_1);
    SendCalMaskString();
    sprintf(buffer,"! WRITE_ATOM_REG %x %x", (CalMask), data1_p_1);
    SendCalMaskString();
    sprintf(buffer,"! WRITE_ATOM_REG %x %x", (CalMask), data2_p_1);
    SendCalMaskString();
    sprintf(buffer,"! WRITE_ATOM_REG %x %x", (CalMask), data3_p_1);
    SendCalMaskString();
    sprintf(buffer,"! WRITE_ATOM_REG %x %x", (CalChan), data4_p_1);
    SendCalMaskString();
    sprintf(buffer,"! WRITE_ATOM_REG %x %x", (CalChan), data5_p_1);
    SendCalMaskString();
    sprintf(buffer,"! WRITE_ATOM_REG %x %x", (CalChan), data6_p_1);
    SendCalMaskString();
    sprintf(buffer,"! WRITE_ATOM_REG %x %x", (CalChan), data7_p_1);
    SendCalMaskString();
    break;   
  case 3: 
    sprintf(buffer,"! WRITE_ATOM_REG %x %x", (CalMask), data0_n_1);
    SendCalMaskString();
    sprintf(buffer,"! WRITE_ATOM_REG %x %x", (CalMask), data1_n_1);
    SendCalMaskString();
    sprintf(buffer,"! WRITE_ATOM_REG %x %x", (CalMask), data2_n_1);
    SendCalMaskString();
    sprintf(buffer,"! WRITE_ATOM_REG %x %x", (CalMask), data3_n_1);
    SendCalMaskString();
    sprintf(buffer,"! WRITE_ATOM_REG %x %x", (CalChan), data4_n_1);
    SendCalMaskString();
    sprintf(buffer,"! WRITE_ATOM_REG %x %x", (CalChan), data5_n_1);
    SendCalMaskString();
    sprintf(buffer,"! WRITE_ATOM_REG %x %x", (CalChan), data6_n_1);
    SendCalMaskString();
    sprintf(buffer,"! WRITE_ATOM_REG %x %x", (CalChan), data7_n_1);
    SendCalMaskString();
    break;
  }
  
  //than send the command
  WriteReg(chip, side, WCalMask);
}

//
void Test() {
  int i;
  for(i = 0; i<16;i++) {
    bzero(buffer,256);
    sprintf(buffer,"! WRITE_ATOM_REG %x 400",i);
    //      fgets(buffer,255,stdin);
    n=sendto(sock_client,buffer,
         strlen(buffer),0,(const struct sockaddr *)&client,length);
    if (n < 0) error("Sendto");
    //receive Terasic answer
    m = recvfrom(sock_server,buffer,1024,0,(struct sockaddr *)&from,&fromlen);
    if (strncmp(buffer,AToM_answ,19)){
      printf("communication error \n%s \n%s \n", buffer, AToM_answ);
    }
    usleep(100000);
  }
}


Int_t ReadData(int addr, TTree *rec){
  char bufferout[10][40000];
  int ibuf = 0, err = 0;
  sprintf(buffer,"! READ_ATOM_DATA %x", addr);
  //
  n=sendto(sock_client,buffer,
       strlen(buffer),0,(const struct sockaddr *)&client,length);
  //receive Terasic answer
  bool isevent;
  bool isfifo = false;
  datadim = 0;
  do{
    m = recvfrom(sock_server,bufferout[ibuf],1024,0,(struct sockaddr *)&from,&fromlen);
       //~ printf("%s \n", bufferout[ibuf]);

    if (strncmp(bufferout[ibuf],AToM_answ2,19) && strncmp(bufferout[ibuf],"FIFO",4)) {
      isfifo = true;
      int iloop = 0, iparity = 0;
      for(iloop = 0; iloop <m/8; iloop++) {
        for(iparity = 1; iparity>-1; iparity--) {
          char buftemp[4];
          buftemp[0] = bufferout[ibuf][((iloop*2)+iparity)*4];
          buftemp[1] = bufferout[ibuf][((iloop*2)+iparity)*4 + 1];
          buftemp[2] = bufferout[ibuf][((iloop*2)+iparity)*4 + 2];
          buftemp[3] = bufferout[ibuf][((iloop*2)+iparity)*4 + 3];
          data[iloop*2 + 1 - iparity + datadim] = strtoul(buftemp, NULL, 16); //converts a string into a unsigned long 
          //~ printf("data[%d] = %x\n", (iloop*2 + 1 - iparity + datadim),  data[iloop*2 + 1 -iparity + datadim]);
        }// end for iparity
      }// end for iloop
      datadim += m/4;
      //~ check buffer goodness
      
      if((addr>0) && (data[0]&0x20))
        isevent = true;
      else
        isevent = false;
    }//end if
  } while ( strncmp(bufferout[ibuf++],AToM_answ2,19));
  // 
  if(isfifo) {
    if(isevent)
      BufferInterpreter(rec);
    else if (addr>0)
      RegInterpreter();
    if(enprint)
      printf("\nEnd of communication\n");
  }
  return err;
}


void ClearFIFO(){
  
  sprintf(buffer,"! WRITE_ATOM_REG %X %x", 0, 0x40000000);
  //
  n=sendto(sock_client,buffer,
       strlen(buffer),0,(const struct sockaddr *)&client,length);
  //receive Terasic answer
  m = recvfrom(sock_server,buffer,1024,0,(struct sockaddr *)&from,&fromlen);
  if(enprint)
    printf("%s \n", buffer);
 
  return;
}

void ClearEveCou(){
  
  sprintf(buffer,"! WRITE_ATOM_REG %X %x", 0, 0x20000000);
  //
  n=sendto(sock_client,buffer,
       strlen(buffer),0,(const struct sockaddr *)&client,length);
  //receive Terasic answer
  m = recvfrom(sock_server,buffer,1024,0,(struct sockaddr *)&from,&fromlen);
  if(enprint)
    printf("%s \n", buffer);
 
  return;
}


void SetCtrlPath(){
    
  //first set clock and command mask
  ClkCmdMask = 0xff;
    
  switch(side) {
  case 0: sprintf(buffer,"! WRITE_ATOM_REG %x %x", ClkCmdMask, data0_p_0);
    break;   
  case 1: sprintf(buffer,"! WRITE_ATOM_REG %x %x", ClkCmdMask, data0_n_0);
    break;   
  case 2: sprintf(buffer,"! WRITE_ATOM_REG %x %x", ClkCmdMask, data0_p_1);
    break;   
  case 3: sprintf(buffer,"! WRITE_ATOM_REG %x %x", ClkCmdMask, data0_n_1);
    break;
  }
  //
  n=sendto(sock_client,buffer,
       strlen(buffer),0,(const struct sockaddr *)&client,length);
  //receive Terasic answer
  m = recvfrom(sock_server,buffer,1024,0,(struct sockaddr *)&from,&fromlen);
  //  
  if (strncmp(buffer,AToM_answ,19)){
    printf("communication error \n%s \n%s \n", buffer, AToM_answ);
  }
  
  //than send write control path selection command
  
  WriteReg(0, side, WCtrlPath);   
  
}

int BufferInterpreter(TTree *rec) {
  int j = 0;
  bool inchip = false;
  nhit = 0;

  Int_t errcode = 0;
  while(j<datadim) {
    if(!inchip && data[j]!=0) { //then it is an header!!!
      if(enprint) {
    printf("#########################\n");
    printf("HEADER: chip number %d, event counter %d, trigger time %d\n", (data[j]&0x1e)>>1, (data[j]&0x7c0)>>6, (data[j]&0xf800)>>11);
    printf("#########################\n");
      }
      j++;
      inchip = true;
    }
    else if(data[j]!=0) { //then it is an hit!!!
      //      chnum = data[j]&0x7f;
      //if (typ && ((chnum%4) != (4-mask))) {
      //        printf("WARNING! Hit on Ch.number %d while mask is %d\n",chnum,mask);
      //        errcode = 1;
      //        return (errcode);
      //      } 
      if(enprint)
    printf("HIT!   Ch Num %d, Time stamp %d, TOT %d\n",data[j]&0x7f, (data[j]&0xf80)>>7, (data[j]&0xf000)>>12);
      j++;
      nhit++;
    }
    else if(inchip){ //then it is a trailer!!!
      // trailer of one chip
      if(enprint) {
    printf("#########################\n");
    printf("END OF CHIP\n");
    printf("#########################\n");
      }
      j++;
      inchip = false;
    }
    else { //event trailer
      if(enprint) {
    printf("#########################\n");
    printf("END OF EVENT\n");
    printf("#########################\n");
      }
      j++;
    }
  }//end of while
  //now we know the number of hits!
  // create the arrays
  /*  chnum = (int*) malloc(nhit*sizeof(int));
  TOT = (int*) malloc(nhit*sizeof(int));
  TimStamp = (int*) malloc(nhit*sizeof(int));
  achip = (int*) malloc(nhit*sizeof(int));*/
  //loop again on data to fill the arrays
  j = 0;
  nhit = 0;
  inchip = false;
  int chipval = 0;
  while(j<datadim) {
    if(!inchip && data[j]!=0) { //then it is an header!!!
      chipval =  (data[j]&0x1e)>>1;
      inchip = true;
      j++;
    }
    else if(data[j]!=0) { //then it is an hit!!!
      /*      achip[nhit] =  chipval;
      chnum[nhit] =  data[j]&0x7f;
      TOT[nhit] =   (data[j]&0xf000)>>12;
      TimStamp[nhit] =  (data[j]&0xf80)>>7;*/
      achip =  chipval;
      chnum =  data[j]&0x7f;
      TOT =   (data[j]&0xf000)>>12;
      TOT = GraySwitchBack(TOT);
      TimStamp =  (data[j]&0xf80)>>7;
      TimStamp =  GraySwitchBack(TimStamp);
      if(rec!=NULL)
      //~ if(CALDAC != 0x20){
        //~ printf("eve = %d\n",eve);
        //~ printf("thr = %x\n",CALTHR);
        //~ printf("dac = %x\n",CALDAC);
        //~ printf("nhit = %d\n",nhit);
      //~ }


        rec->Fill();
      j++;
      nhit++;
    }
    else if(inchip){ //then it is a chip trailer!!!
      // trailer of one chip
      j++;
      inchip = false;
    }
    else { //event trailer
      j++;
    }
  }//end of while
  
  return (errcode);
}

void RegInterpreter() {
   //in case of read register...
  if(enprint) {
    printf("Start bit = %x\n", data[0]&1);
    printf("Chip address = %x\n", (data[0]&0x1e)>>1);
    printf("Event Status = %x\n", (data[0]&0x20)>>5);
    printf("Trigger Tag = %x\n", (data[0]&0x7c0)>>6);
    printf("Time Stamp = %x\n", (data[0]&0xf800)>>11);
    printf("Thresh DAC = %x\n", data[1]&0x3f);
    printf("Cal DAC = %x\n", (data[1]&0xfc0)>>6);
    printf("Resp to Read =  %x\n", (data[2]&0x100)>>8);
    printf("Readout dir = %x\n", (data[2]&0x80)>>7);
    printf("Sampling rate = %x\n", (data[2]&0x18)>>3);
    printf("Max Jitter time = %x\n", (data[2]&0x1)<<4 | (data[1]&0xf000)>>12);
  }
  ReadCalDAC = ((data[1]&0xfc0)>>6)&0x3f;
  ReadCalThr = (data[1]&0x3f);
  ReadJitter = ((data[1]&0xf000)>>12)|((data[2]&0x1)<<4);
  ReadRiseTime = (data[2]&0x6)>>1;
}

void SetCalParameter(short calibthr, short calibdac, short rtime, short jitter, short trl, short skctrl){
    CALTHR = calibthr;
    CALDAC = calibdac;
    JITTER = jitter;
    RISETIME = rtime;
    TRL = trl;
    SKCTRL = skctrl;
    }
    
int GraySwitch(int i){
switch(i){
case 0: return 0x0;
  break;
case 1: return 0x1;
  break;
case 2: return 0x3 ;
  break;
case 3: return 0x2;
  break;
case 4: return 0x6;
  break;
case 5: return 0x7 ;
  break;
case 6: return 0x5 ;
  break;
case 7: return 0x4 ;
  break;
case 8: return 0xc;
  break;
case 9: return 0xd;
  break;
case 10: return 0xf;
  break;
case 11: return 0xe;
  break;
case 12: return 0xa;
  break;
case 13: return 0xb;
  break;
case 14: return 0x9;
  break;
case 15: return 0x8;
  break;
case 16: return 0x18;
  break;
case 17: return 0x19;
  break;
case 18: return 0x1b;
  break;
case 19: return 0x1a;
  break;
case 20: return 0x1e;
  break;
case 21: return 0x1f;
  break;
case 22: return 0x1d;
  break;
case 23: return 0x1c;
  break;
case 24: return 0x14;
  break;
case 25: return 0x15;
  break;
case 26: return 0x17;
  break;
case 27: return 0x16;
  break;
case 28: return 0x12;
  break;
case 29: return 0x13;
  break;
case 30: return 0x11;
  break;
case 31: return 0x10;
  break;
}
 return 0;
}

int GraySwitchBack(int i){
switch(i){
case 0x0: return 0x0;
  break;
case 0x1: return 0x1;
  break;
case 0x3: return 0x2;
  break;
case 0x2: return 0x3;
  break;
case 0x6: return 0x4;
  break;
case 0x7: return 0x5;
  break;
case 0x5: return 0x6;
  break;
case 0x4: return 0x7;
  break;
case 0xc: return 0x8;
  break;
case 0xd: return 0x9;
  break;
case 0xf: return 0xa;
  break;
case 0xe: return 0xb;
  break;
case 0xa: return 0xc;
  break;
case 0xb: return 0xd;
  break;
case 0x9: return 0xe;
  break;
case 0x8: return 0xf;
  break;
}
 return 0;
}

//~ Modifica il valore iniziale del contatore che ritarda il trigger
//~ n=3711 -> 12.8 µs
//~ n=3705 -> 13.0 µs
//~ n=3717 -> 12.6 µs
//~ in generale 1 = 33.33 ns
void SetTriggerLatency(int lat){
  unsigned int rlat = 0;
  int iter = 0;
  do {
    sprintf(buffer,"! WRITE_ATOM_REG %x %x", lat, 0x800);
    
    n=sendto(sock_client,buffer,
             strlen(buffer),0,(const struct sockaddr *)&client,length);
    //receive Terasic answer
    m = recvfrom(sock_server,buffer,1024,0,(struct sockaddr *)&from,&fromlen);
    //  
    if (strncmp(buffer,AToM_answ,19)){
      printf("communication error \n%s \n%s \n", buffer, AToM_answ);
    }

    ReadData(0,NULL);
    rlat = ((unsigned int )data[1])<<16;
    rlat |= (unsigned int) data[0];
    rlat >>= 13;
    rlat &= 0xfff;
  } while (rlat != (lat%4096) && iter++<10);
  if (iter>=10) printf("error setting trg latency, required %d, set to %d\n",
                       lat,rlat);
}

void ChipConfig(int nchip, int calibdac, int calibthr, int rtime, int grayjitter, int skctrl, int *msk) {
  int ichip;
  //configure the chips
  for(ichip = 0; ichip<nchip; ichip++) {
    chip = ichip;
    int temp = 0;
    do {
      //Set Default Control Register at Startup
      SetDefaultCR();
      //set the configuration register
      if(ichip == 0) {
        RespondRead = 0x1;
      } else {    
        RespondRead = 0x0;
      }
      // load the correct values f
      CalDac = calibdac; 
      ThreshDac = calibthr;
      SamplingRate = 0;
      RiseTime = rtime;
      MaxJitterTime = grayjitter;
      SkipControl = skctrl;
      
      CRdata = (ClkStatus << 31)|(SelectClk << 30)| (DigTestPatrn << 26) | (ClkThru << 25) | (RespondRead << 24)
        | (ReadDirection << 23) | (SkipControl << 21) | (SamplingRate << 19) | (RiseTime << 17) | (MaxJitterTime << 12)
        | (CalDac << 6) | (ThreshDac);
      //Write CRdata in the ctrl
      SetRegister(side, CRdata);
      //Write
      WriteReg(chip, side, WCtrlReg);
      // read the config register of ichip
      RegRead(); 
      // read the FIFO
      ReadCalDAC = 65;
      ReadCalThr = 65; 
      ReadRiseTime = 65;
      ReadJitter = 65;
      ReadData(side+1,NULL);
      //control that the config resiter is well written
      //~ if (ReadCalDAC != 0x20) printf("readcaldac = %d\n", ReadCalDAC);
    } while (((ReadCalDAC != calibdac) || (ReadCalThr != calibthr) 
              || (ReadRiseTime != rtime)||(ReadJitter != grayjitter))
             && (temp++<30));
    if(temp>=30) printf("Timeout in configuring chip %d\n",ichip);
    memcpy(msk+ichip*10, data+3,64);
  } //end of the chip loop 
}
