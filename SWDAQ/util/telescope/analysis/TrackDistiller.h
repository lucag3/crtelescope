// "$Id: TrackDistiller.h 274 2013-11-13 14:05:31Z galli $"

//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Jul 23 14:39:46 2013 by ROOT version 5.28/00h
// from TTree rec/rec
// found on file: /home/data/run1490.root
//////////////////////////////////////////////////////////

#ifndef TrackDistiller_h
#define TrackDistiller_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TH2F.h>

// Header file for the classes stored in the TTree if any.
#include <TClonesArray.h>
#include <TObject.h>
#include <TNamed.h>
#include <TAttLine.h>
#include <TAttFill.h>
#include <TAttMarker.h>
#include <TGraph.h>
#include "/root/Desktop/megup/prototypes/DCHPrototype/online/include/MoveStrip.h"
#include <TCanvas.h>
#include <iostream>

const Int_t kMaxDCWaveform = 1;

class TrackDistiller {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   Int_t           frun;
   Int_t           run;
   Int_t           eve;
   Int_t           typ;
   Int_t           nhit;
   Int_t           boa[20000];   //[nhit]
   Int_t           chi[20000];   //[nhit]
   Int_t           cha[20000];   //[nhit]
   Int_t           tot[20000];   //[nhit]
   Int_t           tst[20000];   //[nhit]
   Double_t        pos[20000];   //[nhit]
   Double_t        len[20000];   //[nhit]
   Double_t        cen[20000];   //[nhit]
   /* Int_t           DCWaveform_; */
   /* UInt_t          DCWaveform_fUniqueID[kMaxDCWaveform];   //[DCWaveform_] */
   /* UInt_t          DCWaveform_fBits[kMaxDCWaveform];   //[DCWaveform_] */
   /* TString         DCWaveform_fName[kMaxDCWaveform]; */
   /* TString         DCWaveform_fTitle[kMaxDCWaveform]; */
   /* Short_t         DCWaveform_fLineColor[kMaxDCWaveform];   //[DCWaveform_] */
   /* Short_t         DCWaveform_fLineStyle[kMaxDCWaveform];   //[DCWaveform_] */
   /* Short_t         DCWaveform_fLineWidth[kMaxDCWaveform];   //[DCWaveform_] */
   /* Short_t         DCWaveform_fFillColor[kMaxDCWaveform];   //[DCWaveform_] */
   /* Short_t         DCWaveform_fFillStyle[kMaxDCWaveform];   //[DCWaveform_] */
   /* Short_t         DCWaveform_fMarkerColor[kMaxDCWaveform];   //[DCWaveform_] */
   /* Short_t         DCWaveform_fMarkerStyle[kMaxDCWaveform];   //[DCWaveform_] */
   /* Float_t         DCWaveform_fMarkerSize[kMaxDCWaveform];   //[DCWaveform_] */
   /* Int_t           DCWaveform_fNpoints[kMaxDCWaveform];   //[DCWaveform_] */
   /* Double_t       *DCWaveform_fX[kMaxDCWaveform];   //[DCWaveform_fNpoints] */
   /* Double_t       *DCWaveform_fY[kMaxDCWaveform];   //[DCWaveform_fNpoints] */
   /* Double_t        DCWaveform_fMinimum[kMaxDCWaveform];   //[DCWaveform_] */
   /* Double_t        DCWaveform_fMaximum[kMaxDCWaveform];   //[DCWaveform_] */
   // arrays for CAEN digi and Type1
   Int_t         wfm[5120];
   Int_t         endcell;
   Int_t         triwfm[128];
   Int_t         triadd;


   // List of branches
   TBranch        *b_run;   //!
   TBranch        *b_eve;   //!
   TBranch        *b_typ;   //!
   TBranch        *b_nhit;   //!
   TBranch        *b_boa;   //!
   TBranch        *b_chi;   //!
   TBranch        *b_cha;   //!
   TBranch        *b_tot ;   //!
   TBranch        *b_tst ;   //!
   TBranch        *b_pos ;   //!
   TBranch        *b_len ;   //!
   TBranch        *b_cen ;   //!
   /* TBranch        *b_DCWaveform_;   //! */
   /* TBranch        *b_DCWaveform_fUniqueID;   //! */
   /* TBranch        *b_DCWaveform_fBits;   //! */
   /* TBranch        *b_DCWaveform_fName;   //! */
   /* TBranch        *b_DCWaveform_fTitle;   //! */
   /* TBranch        *b_DCWaveform_fLineColor;   //! */
   /* TBranch        *b_DCWaveform_fLineStyle;   //! */
   /* TBranch        *b_DCWaveform_fLineWidth;   //! */
   /* TBranch        *b_DCWaveform_fFillColor;   //! */
   /* TBranch        *b_DCWaveform_fFillStyle;   //! */
   /* TBranch        *b_DCWaveform_fMarkerColor;   //! */
   /* TBranch        *b_DCWaveform_fMarkerStyle;   //! */
   /* TBranch        *b_DCWaveform_fMarkerSize;   //! */
   /* TBranch        *b_DCWaveform_fNpoints;   //! */
   /* TBranch        *b_DCWaveform_fX;   //! */
   /* TBranch        *b_DCWaveform_fY;   //! */
   /* TBranch        *b_DCWaveform_fMinimum;   //! */
   /* TBranch        *b_DCWaveform_fMaximum;   //! */
   // branches for CAEN digi and Type1
   TBranch        *b_wfm;   //!
   TBranch        *b_endcell;   //!
   TBranch        *b_triwfm;   //!
   TBranch        *b_triadd;   //!
   
   TrackDistiller(Int_t irun = 330, TTree *tree=0);
   virtual ~TrackDistiller();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop(Bool_t draw = false);
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef TrackDistiller_cxx
TrackDistiller::TrackDistiller(Int_t irun, TTree *tree)
{
  frun = irun;
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
     TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(Form("/home/data/run%d.root",irun));
     if (!f || !f->IsOpen()) {
       f = new TFile(Form("/home/data/run%d.root",irun));
     }
     tree = (TTree*)gDirectory->Get("rec");

   }
   Init(tree);
}

TrackDistiller::~TrackDistiller()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t TrackDistiller::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t TrackDistiller::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (!fChain->InheritsFrom(TChain::Class()))  return centry;
   TChain *chain = (TChain*)fChain;
   if (chain->GetTreeNumber() != fCurrent) {
      fCurrent = chain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void TrackDistiller::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set array pointer
  //   for(int i=0; i<kMaxDCWaveform; ++i) DCWaveform_fX[i] = 0;
  //   for(int i=0; i<kMaxDCWaveform; ++i) DCWaveform_fY[i] = 0;

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("run", &run, &b_run);
   fChain->SetBranchAddress("eve", &eve, &b_eve);
   fChain->SetBranchAddress("typ", &typ, &b_typ);
   fChain->SetBranchAddress("nhit", &nhit, &b_nhit);
   fChain->SetBranchAddress("boa", boa, &b_boa);
   fChain->SetBranchAddress("chi", chi, &b_chi);
   fChain->SetBranchAddress("cha", cha, &b_cha);
   fChain->SetBranchAddress("tot ", tot , &b_tot );
   fChain->SetBranchAddress("tst ", tst , &b_tst );
   fChain->SetBranchAddress("pos ", pos , &b_pos );
   fChain->SetBranchAddress("len ", len , &b_len );
   fChain->SetBranchAddress("cen ", cen , &b_cen );
   /* fChain->SetBranchAddress("DCWaveform", &DCWaveform_, &b_DCWaveform_); */
   /* fChain->SetBranchAddress("DCWaveform.fUniqueID", &DCWaveform_fUniqueID, &b_DCWaveform_fUniqueID); */
   /* fChain->SetBranchAddress("DCWaveform.fBits", &DCWaveform_fBits, &b_DCWaveform_fBits); */
   /* fChain->SetBranchAddress("DCWaveform.fName", &DCWaveform_fName, &b_DCWaveform_fName); */
   /* fChain->SetBranchAddress("DCWaveform.fTitle", &DCWaveform_fTitle, &b_DCWaveform_fTitle); */
   /* fChain->SetBranchAddress("DCWaveform.fLineColor", &DCWaveform_fLineColor, &b_DCWaveform_fLineColor); */
   /* fChain->SetBranchAddress("DCWaveform.fLineStyle", &DCWaveform_fLineStyle, &b_DCWaveform_fLineStyle); */
   /* fChain->SetBranchAddress("DCWaveform.fLineWidth", &DCWaveform_fLineWidth, &b_DCWaveform_fLineWidth); */
   /* fChain->SetBranchAddress("DCWaveform.fFillColor", &DCWaveform_fFillColor, &b_DCWaveform_fFillColor); */
   /* fChain->SetBranchAddress("DCWaveform.fFillStyle", &DCWaveform_fFillStyle, &b_DCWaveform_fFillStyle); */
   /* fChain->SetBranchAddress("DCWaveform.fMarkerColor", &DCWaveform_fMarkerColor, &b_DCWaveform_fMarkerColor); */
   /* fChain->SetBranchAddress("DCWaveform.fMarkerStyle", &DCWaveform_fMarkerStyle, &b_DCWaveform_fMarkerStyle); */
   /* fChain->SetBranchAddress("DCWaveform.fMarkerSize", &DCWaveform_fMarkerSize, &b_DCWaveform_fMarkerSize); */
   /* fChain->SetBranchAddress("DCWaveform.fNpoints", &DCWaveform_fNpoints, &b_DCWaveform_fNpoints); */
   /* fChain->SetBranchAddress("DCWaveform.fX", &DCWaveform_fX, &b_DCWaveform_fX); */
   /* fChain->SetBranchAddress("DCWaveform.fY", &DCWaveform_fY, &b_DCWaveform_fY); */
   /* fChain->SetBranchAddress("DCWaveform.fMinimum", &DCWaveform_fMinimum, &b_DCWaveform_fMinimum); */
   /* fChain->SetBranchAddress("DCWaveform.fMaximum", &DCWaveform_fMaximum, &b_DCWaveform_fMaximum); */
   // for CAEN digi and Type1
   fChain->SetBranchAddress("wfm", wfm, &b_wfm);
   fChain->SetBranchAddress("endcell", &endcell, &b_endcell);
   fChain->SetBranchAddress("triwfm", triwfm, &b_triwfm);
   fChain->SetBranchAddress("triadd", &triadd, &b_triadd);
   //
   Notify();
}

Bool_t TrackDistiller::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void TrackDistiller::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t TrackDistiller::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef TrackDistiller_cxx

Double_t GetTrigCharge(Int_t *wfm) {
  //define  histogram for the pedestal computation
  TH1F *histo = new TH1F("histo","histo",1024,0,1024);
  for(Int_t iloop = 0; iloop<128; iloop++) {
    //fill the histo
    histo->Fill(wfm[iloop]);
  }
  //pedestal as the bin with higher occupancy
  Double_t ped = histo->GetMaximumBin()-1.;
  //now evaluate charge
  Double_t cha = 0;
  for(Int_t icha = 95; icha<110; icha++)
    cha += (wfm[icha] - ped);
  histo->Delete();
  return cha;
}
