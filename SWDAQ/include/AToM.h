// $Id: AToM.h 88 2013-02-03 14:21:08Z galli $
class AToM {
 public:
  // AToM chip characteristics
  int                fchipid;      // chip number
  int                   fthr;      // discriminator threshold
  u_int32_t           fmask0;      // mask[31..0] 
  u_int32_t           fmask1;      // mask[63..32] 
  u_int32_t           fmask2;      // mask[95..64] 
  u_int32_t           fmask3;      // mask[127..96] 
  u_int32_t           fmask4;      // mask[159..128] 
  u_int32_t           fmask5;      // mask[191..160] 
  u_int32_t           fmask6;      // mask[223..192] 
  u_int32_t           fmask7;      // mask[255..224] 
  u_int32_t   frespondtoread;      // respond to read (1 per chip 0 and 0 for the others)
  u_int32_t       fconfigreg;      // configuration register
  int               fFIFOreg;      // address of the corresponding FIFO
  int           feventnumber;      // current event number

  // getters
  int                 GetChipId() { return fchipid; }
  int                    GetThr() { return fthr; }
  u_int32_t            GetMask0() { return fmask0; }
  u_int32_t            GetMask1() { return fmask1; }
  u_int32_t            GetMask2() { return fmask2; }
  u_int32_t            GetMask3() { return fmask3; }
  u_int32_t            GetMask4() { return fmask4; }
  u_int32_t            GetMask5() { return fmask5; }
  u_int32_t            GetMask6() { return fmask6; }
  u_int32_t            GetMask7() { return fmask7; }
  u_int32_t        GetConfigreg() { return fconfigreg; }
  int                GetFIFOreg() { return fFIFOreg; }
  int            GetEventNumber() { return feventnumber; }
  // setters
  void  SetEventNumber(int evenum) { feventnumber = evenum; }
  // Constructor
  AToM(int ichip, int iter) { 
    if(ichip<5) // n-sided chip
      fchipid = ichip;
    else // p-sided chip
      fchipid = ichip-5;
    
    fthr = runpar->fchiconf[iter][ichip][0];
    fmask0 = runpar->fchiconf[iter][ichip][1];
    fmask1 = runpar->fchiconf[iter][ichip][2];
    fmask2 = runpar->fchiconf[iter][ichip][3];
    fmask3 = runpar->fchiconf[iter][ichip][4];
    fmask4 = runpar->fchiconf[iter][ichip][5];
    fmask5 = runpar->fchiconf[iter][ichip][6];
    fmask6 = runpar->fchiconf[iter][ichip][7];
    fmask7 = runpar->fchiconf[iter][ichip][8];
    
    // configuration resgister setup
    // first set the respond to read parameter
    if(ichip == 0 || ichip == 5) {
      frespondtoread = 1;
    }
    else {
      frespondtoread = 0;    
    }
    // the build the confguration register word
    fconfigreg = (DefClkStatus << 31)|(DefSelectClk << 30)| (DefDigTestPatrn << 26) | (DefClkThru << 25) | 
      (frespondtoread << 24) | (DefReadDirection << 23) | (DefSkipControl << 21) | (DefSamplingRate << 19) | 
      (DefRiseTime << 17) | (DefMaxJitterTime << 12) | (DefCalDac << 6) | fthr;
    // associate the proper read out FIFO
    if(ichip<5) // then n-sided chip
      fFIFOreg = 2;
    else // then p-sided chip
      fFIFOreg = 1;
  }
};


