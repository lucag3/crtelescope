// $Id: MoveStrip.h 241 2013-05-30 14:06:26Z galli $
#include <TVector3.h>
//this function moves the center of the strip from the system
//of coordinates used by john to the system of coordinates
//of the mask
//only change of system NO translations
void LocalToLocal(double pos, double cen, int chipval, TVector3 *vec) {
  //check if it a p-side or n-side
  if(chipval<5) { //--> nside, so z-like strips
    vec->SetX(-cen*10.); //opposite direction with respect to Walsh coordinates
    vec->SetY(-0.3); // -300 microns (detector width)
    vec->SetZ(pos*10.);
  }//end chipval
  else { //then p-side, phi-like strips
    vec->SetX(-pos*10.);//opposite direction with respect to Walsh coordinates
    vec->SetY(0);
    vec->SetZ(cen*10.);
  } //end else
}
//this function refers all the strips to the reference cross on the module
void LocalToCross(TVector3 *vec, int cross) {
  //the traslation is common to all the modules!
  TVector3 tra;
  if(cross == 0) { //backward modules
    tra(0) = -0.5;
    tra(1) = 0.; //same plane
    tra(2) = -0.7;
  }
  else { //foreward modules
    tra(0) = 51.7; // it is the wafer width in x - the distance of the 
    // cross from the edge of the wafer - the distance between Walsh
    // reference and the other cross
    tra(1) = 0.;
    tra(2) = -0.7; // the same as in the other case
  }
  // now apply traslation
  *vec = *vec + tra;
}// end of LocalToCross
//
// this function rotates the semi-modules oposite to the reference cross in the mask
void RotateHeadToHead(TVector3 *vec) {
  //rotate by 180 deg along y axis, module cross reference
  //as center of rotation
  double tmp[3];
  vec->GetXYZ(tmp);
  vec->SetX(-tmp[0]);
  vec->SetZ(-tmp[2]);
}//end of RotateHeadToHead
//this function place the module in position w.r.t. the (0,0,0) i.e.
//the cross reference on mask1
void MaskInPlace(TVector3 *vec, int mask, double *survey, TString modtyp, double distance) {
  //load the translation vector
  TVector3 tra;
  if(mask==1) {
    tra(0) = survey[0];
    tra(1) = survey[1] - 245.15; //-245.15 to center the DUT
    tra(2) = survey[2];  
    // now apply traslation
    *vec = *vec + tra;
  }
  else { //mask = 2
    //apply the the traslation
    tra(0) = survey[0];
    tra(1) = survey[1];
    tra(2) = survey[2];  
    // now apply traslation
    *vec = *vec + tra;
    // rotate by 180 deg along x axis using the cross as reference
    double tmp[3];
    vec->GetXYZ(tmp);
    vec->SetY(-tmp[1]);
    vec->SetZ(-tmp[2]); // the same for all the modules
    //now apply the final translation to add the translation between the two crosses
    vec->GetXYZ(tmp);
    vec->SetY(tmp[1]+distance - 245.15);
    vec->SetZ(tmp[2]+613.1);  // DONS!! write the nominal distance between crosses wrote on technical design report
  }// end else mask == 2
}//end of Mask in place
