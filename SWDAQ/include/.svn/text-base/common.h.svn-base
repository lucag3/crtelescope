// $Id$
// standard END_OF_COMMUNICATION packets
TString End_Write = "WRITE_ATOM_REG_DONE";
TString  End_Read = "READ_ATOM_DATA_DONE";
//
//list of terasic register address
//
// p-side gpio0
#define data0_p 0x008
#define data1_p 0x010
#define data2_p 0x018
#define data3_p 0x020
#define data4_p 0x028
#define data5_p 0x030
#define data6_p 0x038
#define data7_p 0x040
#define ctrl_p  0x048
//
// n-side gpio0
#define data0_n 0x050
#define data1_n 0x058
#define data2_n 0x060
#define data3_n 0x068
#define data4_n 0x070
#define data5_n 0x078
#define data6_n 0x080
#define data7_n 0x088
#define ctrl_n  0x090
//
// bitmask register
#define regbitmask 0x400 
// bitmask value is 0x8000000a in order to:
// bit 1 enable line B on p-side
// bit 3 enable line B on n-side
// bit 4 enable data B
// bit 32 enable ext trigger
#define bitmask 0x8000000a 
//
//
// trigger latency register
#define regtrglate 0x800
#define trglate     3750
// busy regiters
#define regsetbusy    0x08000000
#define regremovebusy 0x80000000
//
// AToM registers and commands
#define ClearReadout  0x01
#define SyncComm      0x02
#define TrgAcc        0x03
#define ReadEvent     0x04
#define CalStrobeComm 0x05
#define WCtrlPath     0x1A
#define WCtrlReg      0x1B
#define WCalMask      0x1C
#define ReadReg       0x1D
#define MasterRes     0x1E
//
#define nchip         9
//Control Register default parameters values
int DefCalDac = 0x20;
int DefMaxJitterTime = 0x8;
int DefRiseTime = 0x3;
//int DefRiseTime = 0;
int DefSamplingRate = 0x0;
//int DefSkipControl = 0x3;
int DefSkipControl = 0;
int DefReadDirection = 0x0;
int DefRespondRead = 0x0;
int DefClkThru = 0x0;
int DefDigTestPatrn = 0xa;
int DefSelectClk = 0x0;
int DefClkStatus = 0x0;
// Type1 registers
#define MEG_MREG       0x000000
#define MEG_RMOD       (MEG_MREG | 0x0008)   /* Configuration register */
#define MEG1_RPTH      (MEG_MREG | 0x0010)   /* Pedestal threshold */
#define MEG1_RTTH      (MEG_MREG | 0x0014)   /* (Low) threshold for timing */
#define MEG1_RHTH      (MEG_MREG | 0x0018)   /* Channel threshold */
#define MEG1_RTRI      (MEG_MREG | 0x001C)   /* Board threshold */
#define MEG1_RMUL      (MEG_MREG | 0x000C)   /* Multiplicity threshold */
/* CPLD registers */
/* RoboClock control: the 16 bits should have 3 values Low, High and HighZ */
#define CPLD_ROBO01     0x200000
#define CPLD_ROBOHZ     0x400000
#define ROCLO_DAT_M4    0x0
#define ROCLO_HZ_M4     0x0
#define ROCLO_DAT_M3    0x0
#define ROCLO_HZ_M3     0x1
#define ROCLO_DAT_M2    0x1
#define ROCLO_HZ_M2     0x0
#define ROCLO_DAT_M1    0x0
#define ROCLO_HZ_M1     0x2
#define ROCLO_DAT_M0    0x0
#define ROCLO_HZ_M0     0x3
#define ROCLO_DAT_P1    0x1
#define ROCLO_HZ_P1     0x2
#define ROCLO_DAT_P2    0x2
#define ROCLO_HZ_P2     0x0
#define ROCLO_DAT_P3    0x2
#define ROCLO_HZ_P3     0x1
#define ROCLO_DAT_P4    0x3
#define ROCLO_HZ_P4     0x0
/* DAC control */
#define CPLD_DAC        0x800000
/* FPGA control: bit0 = Reset ; bit1 = Sync */
#define CPLD_MEGRESY   0x600000
/* JTAG */
#define JTAG_REG        0xC00000
//
// variables for V1729
#define NUM_WFM_BIN  2560
//VME variables
u_int32_t crate = -1, return_code = 0;
//
int data[100000];
int datadim;
//
//list of variables for DAQ
int eve, typ;
int debug = 0; 
int ReadCalThr, ReadCalDAC, ReadRiseTime, ReadJitter, Chip;
short CALDAC, CALTHR, RISETIME, JITTER, TRL;
double survey[4][2][3];
//variables for rec tree
int nhit;
int chnum[10000], TOT[10000], TimStamp[10000], hitchip[10000], board[10000];
double posstrip[10000], lenstrip[10000], censtrip[10000];
u_int32_t wfm[2*NUM_WFM_BIN]; // two channel in the same 32bit word
u_int32_t endcell;
u_int32_t triwfm[128],triadd;
vector<int> *vchnum, *vTOT, *vTimStamp, *vhitchip, *vboard;
vector<double> *vpos, *vcenter, *vlength;

// global rec tree
TTree *rec;
// Global variables
Int_t event;
Int_t run;
//variables for UDP
char *buffer = new char[256];
socklen_t fromlen, length;
struct sockaddr_in from;
//module builder definition
ModuleBuilder builder;
//
int StripFinder(int, int, double*, double*, ModuleType*, int);

class RunParameters {
 public:
  // board characteristics
  int               fnrun;
  int               fneve;
  TString           fversion;
  int               fnterasic;
  int               fpedthr;
  int               ftrithr;
  int               fisdrs;   
  float             fdrsfreq;
  int               fisvme;   
  u_int32_t         fchiconf[8][9][9];
  TString           fmodtyp[8];
  int               fmask[8];
  double            fdistance;
  int               fiscaen;
  // getters
  int        Getnrun()               { return fnrun;             } 
  int        Getneve()               { return fneve;             } 
  int    Getnterasic()               { return fnterasic;         }
  int      Getpedthr()               { return fpedthr;           }
  int      Gettrithr()               { return ftrithr;           }
  int       Getisdrs()               { return fisdrs;            }
  float   Getdrsfreq()               { return fdrsfreq;          }
  int       Getisvme()               { return fisvme;            }
  TString Getversion()               { return fversion;          }
  TString  Getmodtyp(int iboard)     { return fmodtyp[iboard];   }
  int        Getmask(int iboard)     { return fmask[iboard];     }
  double Getdistance()               { return fdistance;         }
  int      Getiscaen()               { return fiscaen;           }
  // setters
  void Setnrun     (int nrun      )    { fnrun      = nrun ;       } 
  void Setneve     (int neve      )    { fneve      = neve ;       } 
  void Setnterasic (int nterasic  )    { fnterasic  = nterasic ;   } 
  void Setpedthr   (int pedthr    )    { fpedthr    = pedthr ;     }   
  void Settrithr   (int trithr    )    { ftrithr    = trithr ;     }   
  void Setisdrs    (int isdrs     )    { fisdrs     = isdrs ;      } 
  void Setdrsfreq  (float drsfreq )    { fdrsfreq   = drsfreq ;    } 
  void Setisvme    (int isvme     )    { fisvme     = isvme ;      } 
  void Setiscaen   (int iscaen    )    { fiscaen    = iscaen ;     } 
  // save to file run configuration
  void WriteLog(int crun) {
    FILE *outfile;
    outfile = fopen(Form("/home/data/run%d.logbook",crun),"w");
    fprintf(outfile,"Run configuration:\n");
    fprintf(outfile,"Number of events:           %d\n", Getneve());
    fprintf(outfile,"Number of terasic boards    %d\n", Getnterasic());
    fprintf(outfile,"Type1 pedestal threshold    %d\n", Getpedthr()); 
    fprintf(outfile,"Type1 trigger threshold     %d\n", Gettrithr());     
    fprintf(outfile,"Module distance             %f\n", Getdistance());     
    fprintf(outfile,"Is DRS inside?              %d\n", Getisdrs());
    if(Getisdrs()) fprintf(outfile,"\tDRS frequency      %f\n", Getdrsfreq());
    fprintf(outfile,"Is VME used?                %d\n", Getisvme());
    fprintf(outfile,"Is CAEN V1729 inside?              %d\n", Getiscaen());
    // now print module type
    for(int iter = 0; iter<fnterasic; iter++)
      fprintf(outfile,"Terasic %d module %s\n", iter, fmodtyp[iter].Data());
    // now print mask number
    for(int iter = 0; iter<fnterasic; iter++)
      fprintf(outfile,"Terasic %d mask# %d\n", iter, fmask[iter]);
    // now print if we use the CAEN digitizer
    // now print thr and masks
    fprintf(outfile,"Terasic\tchip\tthr\tmask0\t\tmask1\t\tmask2\t\tmask3\t\tmask4\t\tmask5\t\tmask6\t\tmask7\n");
    for(int ichi = 0; ichi<nchip*fnterasic; ichi++) {
      fprintf(outfile,"%d\t%d\t%d\t%x\t%x\t%x\t%x\t%x\t%x\t%x\t%x\n",ichi/nchip, ichi%nchip, fchiconf[ichi/nchip][ichi%nchip][0], fchiconf[ichi/nchip][ichi%nchip][1], 
              fchiconf[ichi/nchip][ichi%nchip][2], fchiconf[ichi/nchip][ichi%nchip][3], fchiconf[ichi/nchip][ichi%nchip][4], fchiconf[ichi/nchip][ichi%nchip][5], 
              fchiconf[ichi/nchip][ichi%nchip][6], fchiconf[ichi/nchip][ichi%nchip][7], fchiconf[ichi/nchip][ichi%nchip][8] );
    }
    fprintf(outfile,"File Version               %s\n", Getversion().Data());
    fclose(outfile);
  }
  // print on screen run config
  void Print() {
    printf("Run configuration:\n");
    printf("Number of runs:              %d\n", Getnrun());
    printf("Number of events:            %d\n", Getneve());
    printf("Number of terasic boards     %d\n", Getnterasic());
    printf("Type1 pedestal threshold     %d\n", Getpedthr()); 
    printf("Type1 trigger threshold      %d\n", Gettrithr()); 
    printf("module distance              %f\n", Getdistance()); 
    printf("Is DRS inside?               %d\n", Getisdrs());
    if(Getisdrs()) printf("\tDRS frequency        %f\n", Getdrsfreq());
    printf("Is VME used?                 %d\n", Getisvme());
    printf("Is CAEN V1729 inside?        %d\n", Getiscaen());
    printf("version =                    %s\n", Getversion().Data());
    // now print module type
    for(int iter = 0; iter<fnterasic; iter++)
      printf("Terasic %d module %s\n", iter, fmodtyp[iter].Data());
    // now print mask number
    for(int iter = 0; iter<fnterasic; iter++)
      printf("Terasic %d mask# %d\n", iter, fmask[iter]);
  }
  //constructor
  RunParameters() {
    char modtyptmp[10][10];
    FILE *filparam = fopen("parameters/runpar.dat","r");
    fscanf(filparam,"%*s\t%d",&fnrun);
    fscanf(filparam,"%*s\t%d",&fneve);
    fscanf(filparam,"%*s\t%d",&fnterasic);
    fscanf(filparam,"%*s\t%d",&fpedthr);
    fscanf(filparam,"%*s\t%d",&ftrithr);
    fscanf(filparam,"%*s\t%lf",&fdistance);
    fscanf(filparam,"%*s\t%d",&fisdrs);
    fscanf(filparam,"%*s\t%f",&fdrsfreq);
    fscanf(filparam,"%*s\t%d",&fisvme);
    // read the module type from file
    fscanf(filparam,"%*s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s",modtyptmp[0],
           modtyptmp[1], modtyptmp[2], modtyptmp[3], modtyptmp[4], modtyptmp[5],
           modtyptmp[6], modtyptmp[7]);
    for(int iter = 0; iter<fnterasic; iter++)
      fmodtyp[iter] = modtyptmp[iter];
    // read the mask number from file
    fscanf(filparam,"%*s\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d",fmask,
           fmask+1, fmask+2, fmask+3, fmask+4, fmask+5,
           fmask+6, fmask+7);
    fscanf(filparam,"%*s\t%d",&fiscaen);
    fclose(filparam);    
    fversion = "1.0";
    FILE *chipconfig = fopen("parameters/chipconfig.dat","r");
    fscanf(chipconfig,"%*s\t%*s\t%*s\t%*s\t%*s\t%*s\t%*s\t%*s\t%*s\t%*s\t%*s");
    for(int ichi = 0; ichi<nchip*fnterasic; ichi++) {
      fscanf(chipconfig,"%*d\t%*d\t%d\t%x\t%x\t%x\t%x\t%x\t%x\t%x\t%x",fchiconf[ichi/nchip][ichi%nchip],
             fchiconf[ichi/nchip][ichi%nchip]+1, fchiconf[ichi/nchip][ichi%nchip]+2, fchiconf[ichi/nchip][ichi%nchip]+3,
             fchiconf[ichi/nchip][ichi%nchip]+4, fchiconf[ichi/nchip][ichi%nchip]+5, fchiconf[ichi/nchip][ichi%nchip]+6,
             fchiconf[ichi/nchip][ichi%nchip]+7,fchiconf[ichi/nchip][ichi%nchip]+8);
    }
  fclose(chipconfig);
  }
};
RunParameters *runpar;
// switch an integer to a gray code number
int GraySwitchBack(int i){
  int output;
  switch(i){
  case 0x0: output = 0x0;
    break;
  case 0x1: output = 0x1;
    break;
  case 0x3: output = 0x2;
    break;
  case 0x2: output = 0x3;
    break;
  case 0x6: output = 0x4;
    break;
  case 0x7: output = 0x5;
    break;
  case 0x5: output = 0x6;
    break;
  case 0x4: output = 0x7;
    break;
  case 0xc: output = 0x8;
    break;
  case 0xd: output = 0x9;
    break;
  case 0xf: output = 0xa;
    break;
  case 0xe: output = 0xb;
    break;
  case 0xa: output = 0xc;
    break;
  case 0xb: output = 0xd;
    break;
  case 0x9: output = 0xe;
    break;
  case 0x8: output = 0xf;
    break;
  case 0x18: output = 0x10;
    break;
  case 0x19: output = 0x11;
    break;
  case 0x1b: output = 0x12;
    break;
  case 0x1a: output = 0x13;
    break;
  case 0x1e: output = 0x14;
    break;
  case 0x1f: output = 0x15;
    break;
  case 0x1d: output = 0x16;
    break;
  case 0x1c: output = 0x17;
    break;
  case 0x14: output = 0x18;
    break;
  case 0x15: output = 0x19;
    break;
  case 0x17: output = 0x1a;
    break;
  case 0x16: output = 0x1b;
    break;
  case 0x12: output = 0x1c;
    break;
  case 0x13: output = 0x1d;
    break;
  case 0x11: output = 0x1e;
    break;
  case 0x10: output = 0x1f;
    break;
  }
  return output;
}// end grey code switch back
//this is to get hits from AToM data stream
bool BufferInterpreter(TTree *rec, int *eventnumber, int iboard, int side, ModuleType *mod) {
  int j = 0;
  bool inchip = false;
  nhit = 0;  
  int chipval = 0;
  //  ModuleBuilder builder;
  //  ModuleType *mod = builder.GetModuleType(runpar->Getmodtyp(iboard).Data());
  while(j<datadim) {
    if(!inchip && data[j]!=0) { //then it is an header!!!
      // get the chip number form the chip header
      chipval =  (data[j]&0x1e)>>1;
      if(chipval>5) { // this is a read out error!!!
        printf("error invalid chip number!!!!!\n");
        return false;
          }
      eventnumber[chipval] = (data[j]&0x7c0)>>6;
      inchip = true;
      j++;
    }
    else if(data[j]!=0) { //then it is an hit!!!
      int chnumtemp, tottemp, tsttemp, nstrip;
      double zpos[2], phipos;
      // get the channel number
      vboard->push_back(iboard);
      vhitchip->push_back(chipval + side*5);
      chnumtemp = data[j]&0x7f;
      vchnum->push_back(chnumtemp);
      tottemp = (data[j]&0xf000)>>12;
      tottemp = GraySwitchBack(tottemp);
      vTOT->push_back(tottemp);
      tsttemp = (data[j]&0xf80)>>7;
      tsttemp = GraySwitchBack(tsttemp);
      vTimStamp->push_back(tsttemp);
      j++; //to check the dimension of the read buffer
      //now check the strip positions and in case of ganging double the hit
      nstrip = StripFinder(chipval,chnumtemp,zpos,&phipos,mod, side);
             // now fill z or phi depending on the side
      // if side == 1 then p-side and phi position
      if(nstrip>0 && side == 1) {
        vpos->push_back(phipos);
        vlength->push_back(mod->GetPhiStripLength());
        vcenter->push_back(mod->GetPhiStripCenter());
      }//end if side == 1
      else if(nstrip>0 && side == 0) { // i.e. n-side
        vpos->push_back(zpos[0]);
        vlength->push_back(mod->GetZStripLength());
        vcenter->push_back(mod->GetZStripCenter());            
        if(nstrip==2) {
          //in case of ganging put another entry in the vector identical to 
          //the latter but with a different z
          vboard->push_back(iboard);
          vhitchip->push_back(chipval + side*5);
          vchnum->push_back(chnumtemp);
          vTOT->push_back(tottemp);
          vTimStamp->push_back(tsttemp);
          vpos->push_back(zpos[1]);
          vlength->push_back(mod->GetZStripLength());
          vcenter->push_back(mod->GetZStripCenter());          
        }// end if nstrip == 2
      }// end else n-side
      else if(nstrip==0) { //nstrip == 0
        vpos->push_back(-10000.);
        vcenter->push_back(-10000.);
        vlength->push_back(-10000.);
      }//end else nstrip > 0
    }// end of hit composition
    else if(inchip){ //then it is a chip trailer!!!
      // trailer of one chip
      j++;
      inchip = false;
    }
    else { //event trailer
      j++;
    }
  }//end of while
 return true;  
}

bool RegInterpreter() {
   //in case of read register...
  ReadCalDAC = ((data[1]&0xfc0)>>6)&0x3f;
  ReadCalThr = (data[1]&0x3f);
  ReadJitter = ((data[1]&0xf000)>>12)|((data[2]&0x1)<<4);
  ReadRiseTime = (data[2]&0x6)>>1;
  Chip = (data[0]&0x1e)>>1;
  return true;
}
//this function associates the strip number in strip position in local frame
/*int StripFinder(int chip, int chan, double *zpos, double *phipos, TString modtyp, int pside) {
  //module type definition
  ModuleType *mod = builder.GetModuleType(modtyp.Data());
  int nstr=0;
  if(pside == 0) {
    // extract Z positions
    mod->GetZPositions(chip, chan, nstr, zpos);
    // extract Phi position and check if the chip exists
  }
  else {
    bool goodphi;
    goodphi = mod->GetPhiPosition(chip, chan, phipos);
    if(!goodphi)
      nstr = 0;
    else 
      nstr = 1;
  }
  return nstr;
  }*/// end of StripFinder
int StripFinder(int chip, int chan, double *zpos, double *phipos, ModuleType *mod, int pside) {
  int nstr=0;
  if(pside == 0) {
    // extract Z positions
    mod->GetZPositions(chip, chan, nstr, zpos);
    // extract Phi position and check if the chip exists
  }
  else {
    bool goodphi;
    goodphi = mod->GetPhiPosition(chip, chan, phipos);
    if(!goodphi)
      nstr = 0;
    else 
      nstr = 1;
  }
  return nstr;
  }// end of StripFinder
//
