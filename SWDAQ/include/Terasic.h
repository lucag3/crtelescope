// $Id: Terasic.h 235 2013-05-28 13:40:42Z galli $
class Terasic {
 public:
  // board characteristics
  int               fboardid;      // board id
  TString                fip;      // ip address
  TString              fport;      // port address 
  int           fsock_client;      // client socket
  struct sockaddr_in fclient;      // client
  struct hostent        *fhp;      // host entity
  AToM          *AToMChip[9];      // AToM chips belonging to Terasic [0-4] n-side, [5-8] p-side
  int           feventnumber;      // current event number
  ModuleType           *fmod;      // module type for map strips

  // getters
  int                GetBoardId() { return fboardid; }
  TString                 GetIp() { return fip; }
  TString               GetPort() { return fport; }
  int             GetSockClient() { return fsock_client; }
  struct sockaddr_in* GetClient() { return &fclient; }
  struct hostent*         GetHp() { return fhp; }
  AToM*        GetAToM(int ichip) { return AToMChip[ichip]; }
  int            GetEventNumber() { return feventnumber; }
  ModuleType*        GetModType() { return fmod; }
  // setters
  void       SetEventNumber(int evenum) { feventnumber = evenum; }
  void                SetIp(TString ip) { fip = ip; return; } 
  void            SetPort(TString port) { fport = port; return; } 
  void                  SetSockClient() { 
    fsock_client = socket(AF_INET, SOCK_DGRAM, 0);    
    if (fsock_client < 0) cout<<"Error in building socket client for IP = "<<GetIp()<<endl;
  }
  void  SetClient(TString ip, TString  port) {
    // first set ip address and port
    SetIp(ip);
    SetPort(port);
    //then open UDP connection 
    fclient.sin_family = AF_INET;
    //get the IP address for client 
    fhp = gethostbyname(ip.Data());
    if (fhp==0) cout<<"Unknown host"<<endl;
    // ...and set to client
    bcopy((char *)fhp->h_addr,
          (char *)&fclient.sin_addr,
          fhp->h_length);
    //set client and server ports
    fclient.sin_port = htons(atoi(port.Data()));
  }
  // Constructor
  Terasic(int board, AToM *chip[8]) { 
    fboardid = board; 
    fmod = builder.GetModuleType(runpar->Getmodtyp(board).Data());
    for(int ichip = 0; ichip<nchip; ichip++) {
      AToMChip[ichip] = chip[ichip];
    }
  } ;
};


