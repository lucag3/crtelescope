// $Id: UDP.h 235 2013-05-28 13:40:42Z galli $
class UDP {
 public:
  // board characteristics
  Server         *fserver;   // pointer to the server
  Terasic    *fterasic[8];   // pointer to to the nterasic clients

  // getters
  Server*             GetServer() { return fserver; }
  Terasic*   GetTerasic(int iter) { return fterasic[iter]; }
  // send a Command via UDP 
  void SendCommand(int iter, u_int32_t reg, u_int32_t data) {
    int n,m;
    sprintf(buffer,"! WRITE_ATOM_REG %x %x", data, reg);
    //    
    n=sendto(fterasic[iter]->GetSockClient(),buffer,
             strlen(buffer),0,(const struct sockaddr *)fterasic[iter]->GetClient(),length);
    //receive Terasic answer
    m = 0;
    m = recvfrom(fserver->GetSockServer(),buffer,1024,0,(struct sockaddr *)&from,&fromlen);
    //
    if (strncmp(buffer,End_Write.Data(),19)){
      printf("communication error \n%s \n%s \n", buffer, End_Write.Data());
      //    }
      return;
    }    
  }// end Send Command
  // Read data from Terasic to Server
  bool ReadData(int iter, u_int32_t addr, ModuleType *mod){
    char bufferout[10][40000]={{NULL}};
    int ibuf = 0;
    int n, m;
    int eventnumber[5];
    bool result = true;
    //
    sprintf(buffer,"! READ_ATOM_DATA %x", addr);
    //
    //send the command asling to read the FIFO or the control register
    n=sendto(fterasic[iter]->GetSockClient(),buffer,
             strlen(buffer),0,(const struct sockaddr *)fterasic[iter]->GetClient(),length);
    //receive Terasic answer
    bool isevent;
    bool isfifo = false;
    datadim = 0;
    do{
      m = recvfrom(fserver->GetSockServer(),bufferout[ibuf],1024,0,(struct sockaddr *)&from,&fromlen);
      //check if I'm reading a FIFO! 
      if (strncmp(bufferout[ibuf],End_Read.Data(),19) && strncmp(bufferout[ibuf],"FIFO",4)) {
        isfifo = true;
        int iloop = 0, iparity = 0;
        // unpack the UDP data into a vector of insigned integer 
        for(iloop = 0; iloop <m/8; iloop++) {
          for(iparity = 1; iparity>-1; iparity--) {
            char buftemp[4];
            buftemp[0] = bufferout[ibuf][((iloop*2)+iparity)*4];
            buftemp[1] = bufferout[ibuf][((iloop*2)+iparity)*4 + 1];
            buftemp[2] = bufferout[ibuf][((iloop*2)+iparity)*4 + 2];
            buftemp[3] = bufferout[ibuf][((iloop*2)+iparity)*4 + 3];
            data[iloop*2 + 1 - iparity + datadim] = strtoul(buftemp, NULL, 16); //converts a string into a unsigned long 
          }// end for iparity
        }// end for iloop
        //evaluate the number of read data
        datadim += m/4;
        //~ check buffer goodness               
        if(data[0]&0x20)
          isevent = true; // the I read the chnnels fired in a event!
        else
          isevent = false; // then I'm reding the configuration register!
      }//end if
    } while ( strncmp(bufferout[ibuf++],End_Read.Data(),19)); // then the read out is finished
    // 
    if(addr == 0) { //then it is read out of terasic configuration register
      printf("%x%x\n",data[0], data[1]);
    }
    else if(isfifo) {
      if(isevent) {
        if(addr == 2) { //then n side!
          // interprete the buffer
          result = BufferInterpreter(rec,eventnumber,iter,0,mod); // here 0 means n side
          // fill the chip event number
          // first in case of n side
          for(int ichip = 0; ichip<5; ichip++) {
            fterasic[iter]->GetAToM(ichip)->SetEventNumber(eventnumber[ichip]);
          } // end for ichip
        } // end if addr==2
        // first in case of n side
        else if(addr == 1) {
          // interprete the buffer
          result = BufferInterpreter(rec,eventnumber,iter,1,mod); // here 1 means p side
          for(int ichip = 0; ichip<4; ichip++) {
            fterasic[iter]->GetAToM(ichip+5)->SetEventNumber(eventnumber[ichip]);
          } //end for ichip
        } // end else if addr==1
      } // end if isfifo
      else { // it is a configuration register read out
        result = RegInterpreter();               
      }// end else is register
    }
    return result;
  }//end ReadData
  // Constructor
  UDP(Server *server, Terasic *terasic[8]) { 
    fserver = server;
    for(int iter = 0; iter<runpar->Getnterasic(); iter++) {
      fterasic[iter] = terasic[iter];
    }
  } ;
};


