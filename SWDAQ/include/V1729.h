// $Id: V1729.h 253 2013-06-19 06:34:14Z galli $
class V1729 {
 public:
  // board characteristics
  u_int32_t         faddress;      // board address
  u_int32_t         fpostrg;       // trigger position
  int               fncha;         // number of channels

  // getters
  u_int32_t      GetAddress()  { return faddress; }
  u_int32_t       GetPosTrg()  { return fpostrg; }
  int               GetNCha()  { return fncha; }
  // this routine reset the board
  void Reset() {
    u_int32_t uno = 0x1;
    int return_code;
    // access the reset register writing a dummy number
    return_code = vme_A32D32_write( crate, faddress | 0x0800, uno); 
    if(return_code) 
      cout<<"Problems in board resetting.....V1729"<<endl;
    return;
  }// end of Reset
  //
  // Board Initialization
  void Init(u_int32_t postrg_lsb = 0x50, u_int32_t postrg_msb = 0x00, u_int32_t freq = 1, int ncha = 4) {
    // Let's reset and configure V1729
    printf("Resetting V1729 waveform digitizer...\n");
    // V1729 initialization sequence
    u_int32_t datum;
    // PRETIRG: leave as it is
    // POSTTRIG: leave as it is
    //          LSB
    vme_A32D32_write(crate, faddress | 0x1A00, postrg_lsb); 
    //          MSB
    vme_A32D32_write(crate, faddress | 0x1B00, postrg_msb); 
    // store postrg value in the class
    fpostrg = (postrg_msb<<8) | postrg_lsb;
    //
    // Trigger mask (in case of trigger on input channel)
    datum = 1;
    vme_A32D32_write(crate, faddress | 0x1E00, datum);    
    // Trigger threshold (in case of trigger on input channel)
    datum = 3000;
    vme_A32D32_write(crate, faddress | 0x0900, datum);    
    // Trigger type: 0x1 trigger on input, 0x2 ext trigger
    datum = 0x2;
    vme_A32D32_write(crate, faddress | 0x1D00, datum);
    // NUM OF COL TO READ: default read all = 128
    // Channel Mask: A bit for each enabled channel
    int idx = 4;
    datum = 0x0;
    while (idx > 0) {
      datum |= (1<< (--idx));
    }
    vme_A32D32_write(crate, faddress | 0x2300, datum);
    // Set trigger threshold
    datum = 1500;
    vme_A32D32_write(crate, faddress | 0x0A00, datum);
    // FP FREQUENCY: 1 = 2 GHz, 2 = 1 GHz
    vme_A32D32_write(crate, faddress | 0x8100, freq);
    //end of V1729 config
    //finally set the number of channels
    fncha = ncha;
    return;
  }// end Init
  void ReadWfm(u_int32_t *datum, u_int32_t *endcella) {
    /* Module memory mapping:
       each waveform has 2565 points (at maximum)
       the waveforms bins start from address 
       3*NCH to 2564(NCH-1)
       the channels are read in parallel, this means that
       we read first of all the first bin of each
       WFM, then the second and so on....
       the internal addressing is performed by V1729 itself,
       we always read the same address
    */
    //BLT read
    uint32_t vmeadd = faddress | 0x0D00;
    uint32_t first_sample[4];
    uint32_t vernier[4];
    uint32_t reset_baseline[4];
    uint32_t gotlw;

    //  int size = 4 * NCH * 2564; // size in byte to be trasfered
    // read first_sample
    //read end cell position
    vme_A32D32_read(crate, faddress | 0x2000, endcella); 
    // and mask the word
    (*endcella) &= 0xFF;
    //first values read with single word access and NOT block transfer
    for(int iloop = 0; iloop< 2 ; iloop++) {
     vme_A32D32_read(crate, vmeadd, first_sample + iloop); 
    }
    // read vernier
    for(int iloop = 0; iloop< 2 ; iloop++) {
      vme_A32D32_read(crate, vmeadd, vernier + iloop); 
    }
    // read reset_baseline
    for(int iloop = 0; iloop< 2 ; iloop++) {
      vme_A32D32_read(crate, vmeadd, reset_baseline + iloop); 
    }
    //now read the wfms with block transfer
    //    for (int iloop = 0; iloop<5120; iloop++) {
    //      vme_A32D32_read(crate, vmeadd, datum + iloop); 
    //      }
    //
    if (vme_A32BLT32FIFO_read(crate, vmeadd, datum, 5120, &gotlw))
      printf("readout error from CAEN\n");

    if (gotlw != 5120)
      printf("readout error from CAEN, %d lwords returned\n",gotlw);

    return;
  }// end of Read Event
  // this function enables the channel for internal trigger
  void StartDAQ() {
    // just write a random number in this register....
    vme_A32D32_write(crate, faddress | 0x1700, 1);    
  }
  u_int32_t PollEvent() {
    u_int32_t datum;
    // check interrupt status
    vme_A32D32_read(crate, faddress | 0x8000, &datum);
    return (datum);
  }
  // Acknowledge()
  void Acknowledge() {
    // release the interrupt 
    vme_A32D32_write(crate, faddress | 0x8000, 0); 
  }
  //
  // Constructor
  V1729(u_int32_t address) { 
    faddress = address; 
  }
};


