// $Id: Type1.h 272 2013-11-09 13:47:05Z galli $
class Type1 {
 public:
  // board characteristics
  u_int32_t         fboardid;      // board id

  // getters
  u_int32_t      GetBoardId() { return fboardid; }
  // set thresholds
  // this routine writes all the needed threshold on the type1 registers
  void SetThresholds() {
    u_int32_t vmeadd, wdata;
    return_code = 0;
    /* Pedestal Threshold */    
    wdata = runpar->Getpedthr();             
    vmeadd = (GetBoardId()<<24) | MEG1_RPTH ;
    return_code = vme_A32D32_write(crate, vmeadd, wdata ) ;
    //
    /* Bar sum Threshold */    
    wdata = runpar->Gettrithr();             
    vmeadd = (GetBoardId()<<24) | MEG1_RHTH ;
    return_code |= vme_A32D32_write(crate, vmeadd, wdata ) ;
    //
    /* Bar sum Threshold */    
    wdata = runpar->Gettrithr()*2;             
    vmeadd = (GetBoardId()<<24) | MEG1_RTRI ;
    return_code |= vme_A32D32_write(crate, vmeadd, wdata ) ;
  }
  /*--------------------------------------------------------------------*/
  /* RoboClock registers configuration: got from TRG.c, check there!    */ 
  /*--------------------------------------------------------------------*/
  void RoboConf()
  {
    u_int32_t wdata, vmeadd;
    //
    return_code = 0;
    wdata = 0x5 ;
    vmeadd = (GetBoardId()<<24) | CPLD_ROBO01;
    return_code |= vme_A32D32_write(crate, vmeadd, wdata ) ;
    if (return_code) {
      printf("roboconf error:   return_code = 0x%08x  \n",return_code);
    }

    //    
    wdata = 0xffe ;                      /*  x5 */
    vmeadd = (GetBoardId()<<24) | CPLD_ROBOHZ ;
    return_code |=  vme_A32D32_write(crate, vmeadd, wdata ) ;
    if (return_code) {
      printf("robocinf error:   return_code = 0x%08x  \n",return_code);
    }
  }
  /*--------------------------------------------------------------------*/
  /* DAC setting: code taken from TRG.c, check there!                   */
  /*--------------------------------------------------------------------*/
  void SetDAC(int dac, int cha)
  {
    //
    int i;
    u_int32_t wdata, wmask, dac_data, bdata;
    u_int32_t vmeadd;
    //   
    return_code = 0;
    //     
    vmeadd = (GetBoardId()<<24) | CPLD_DAC;
    dac_data = dac<<4;
    //
    /* initialize DAC */
    wdata = 0x3ffff ;
    return_code |= vme_A32D32_write(crate, vmeadd, wdata ) ;
    //
    /* SYNC low */
    wmask = 0xffff & (~(1<<cha));
    return_code |= vme_A32D32_write(crate, vmeadd, wmask ) ;
    if (return_code) {
      printf("set dac error:   return_code = 0x%08x  \n",return_code);
    }
    //
    /* insert data at the trailing edge of SCLK */
    for (i=0;i<16;i++) {
      //         
      /* extract data for id_board DAC input */
      bdata = (dac_data >> (15-i)) & 1;
      //         
      /* set data with SCLK high */
      wdata = (bdata<<17) | 0x10000 | wmask;
      return_code |= vme_A32D32_write(crate, vmeadd, wdata ) ;
      if (return_code) {
        printf("set dac error:   return_code = 0x%08x  \n",return_code);
      }
      //         
      /* again with SCLK low */
      wdata = (bdata<<17) | wmask;
      return_code |= vme_A32D32_write(crate, vmeadd, wdata ) ;
      //         
      /* again with SCLK high */
      wdata = (bdata<<17) | 0x10000 | wmask;
      return_code |= vme_A32D32_write(crate, vmeadd, wdata ) ;
      if (return_code) {
        printf("set dac error:   return_code = 0x%08x  \n",return_code);
      }
      //
    } //end for i = 0-->16
    
    /* return to initial DAC register configuration */
    wdata = 0x3ffff ;
    return_code |= vme_A32D32_write(crate, vmeadd, wdata ) ;
    if (return_code) {
      printf("set dac error:   return_code = 0x%08x  \n",return_code);
    }
    //      
    /* END OF DAC CONFIGURATION PHASE */
  }//end SetDAC
  // set the Type1 board in run mode
  int GoRun() {
    u_int32_t wdata, vmeadd;
    int return_code;
    //
    return_code = 0;
    // first remove the busy 4 times
    for(int i = 0; i<4; i++) {
      wdata = 0x17 ;
      vmeadd = (GetBoardId()<<24) | MEG_RMOD;
      return_code |= vme_A32D32_write(crate, vmeadd, wdata ) ;
    }
    // then runmode
    wdata = 0x1 ;
    vmeadd = (GetBoardId()<<24) | MEG_MREG;
    return_code |= vme_A32D32_write(crate, vmeadd, wdata ) ;
    //
    return return_code;
  }
  // check the RRUN searching for a trigger!
  int PollEvent() {
    u_int32_t data, vmeadd;
    int return_code;
    //
    return_code = 0;
    vmeadd = (GetBoardId()<<24) | MEG_MREG;
    return_code |= vme_A32D32_read(crate, vmeadd, &data ) ;
    return ((data&0x1000)>>12);
  }
  //reset teh Type1 i.e. event counter cleared
  void Sync() {
    u_int32_t wdata,vmeadd,return_code;
    wdata = 0x2 ;             /* sync */
    vmeadd = (GetBoardId()<<24) | CPLD_MEGRESY ;
    return_code =  vme_A32D32_write(crate, vmeadd, wdata ) ;
    //
    wdata = 0x0 ;             /* again at zero */
    vmeadd = (GetBoardId()<<24) | CPLD_MEGRESY ;
    return_code =  vme_A32D32_write(crate, vmeadd, wdata ) ;
    //
  }
  // read the rams of the type1 board
  void ReadRam(u_int32_t *data, u_int32_t *addr) {

    u_int32_t get_lwords ;
    
    u_int32_t vmeadd = (GetBoardId()<<24) | 0x090000; //memory address
    // this is the block transfer read out
    vme_A32BLT32_read(crate, vmeadd, data, 128, &get_lwords) ;

    vmeadd = (GetBoardId()<<24) | 0x0A0000;           //latch address
    vme_A32D32_read(crate, vmeadd, addr) ;

    //and then clear the busy
    //first read rmod register
    //first read the rrun register to get the indr status
    u_int32_t rmod;
    vmeadd = (GetBoardId()<<24) | 0x8;                
    vme_A32D32_read(crate, vmeadd, &rmod) ;
    // and then issue the clr busy
    u_int32_t wdata;
    wdata = rmod | 0x10;
    vme_A32D32_write(crate, vmeadd, wdata) ;
    
    return;
  }
  // Constructor
  Type1(int board) { 
    fboardid = board; 
  }

};


