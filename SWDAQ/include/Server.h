// $Id: Server.h 88 2013-02-03 14:21:08Z galli $
class Server {
 public:
  // board characteristics
  TString              fport;                  // port address 
  int           fsock_server;                  // server socket
  struct sockaddr_in fserver;                  // server

  // getters
  TString              GetPort() { return fport; }
  int            GetSockServer() { return fsock_server; }
  struct sockaddr_in GetServer() { return fserver; }
  // setters
  void           SetSockServer() { 
    fsock_server = socket(AF_INET, SOCK_DGRAM, 0);    
    if (fsock_server < 0) cout<<"Error in building socket server "<<endl;
  }
  void               SetServer() {
    //then open UDP connection 
    fserver.sin_family = AF_INET;
    fserver.sin_addr.s_addr = INADDR_ANY;
    //set server port
    fserver.sin_port = htons(atoi(fport.Data()));
    if (bind(fsock_server,(struct sockaddr *)&fserver,length)<0)
      cout<<"error in binding"<<endl;
  }
  // Constructor
  Server() {
    fport = "4444";
  } ;

};


