//  C:\USERS\USER\DESKTOP\VLOGATOM.v
//  Verilog created by Xilinx's StateCAD 5.03
//  Thu Aug 09 15:35:38 2012

//  This Verilog code (for use with Synopsys) was generated using: 
//  one-hot state assignment with boolean code format.
//  Minimization is enabled,  implied else is enabled, 
//  and outputs are speed optimized.

`timescale 1s/1s

module shell_vlogatom(CLK,atom_data,chip_addr0,chip_addr1,chip_addr2,
	chip_addr3,chip_addr4,cmd_code0,cmd_code1,cmd_code2,cmd_code3,cmd_code4,data0
	,data1,data2,data3,data4,data5,data6,data7,data8,data9,data10,data11,data12,
	data13,data14,data15,data16,data17,data18,data19,data20,data21,data22,data23,
	data24,data25,data26,data27,data28,data29,data30,data31,data_10,data_11,
	data_12,data_13,data_14,data_15,data_16,data_17,data_18,data_19,data_20,
	data_21,data_22,data_23,data_24,data_25,data_26,data_27,data_28,data_29,
	data_30,data_31,data_32,data_33,data_34,data_35,data_36,data_37,data_38,
	data_39,data_40,data_41,data_42,data_43,data_44,data_45,data_46,data_47,
	data_48,data_49,data_50,data_51,data_52,data_53,data_54,data_55,data_56,
	data_57,data_58,data_59,data_60,data_61,data_62,data_63,data_64,data_65,
	data_66,data_67,data_68,data_69,data_70,data_71,data_72,data_73,data_74,
	data_75,data_76,data_77,data_78,data_79,data_110,data_111,data_112,data_113,
	data_114,data_115,data_116,data_117,data_118,data_119,data_120,data_121,
	data_122,data_123,data_124,data_125,data_126,data_127,data_128,data_129,
	data_130,data_131,data_210,data_211,data_212,data_213,data_214,data_215,
	data_216,data_217,data_218,data_219,data_220,data_221,data_222,data_223,
	data_224,data_225,data_226,data_227,data_228,data_229,data_230,data_231,
	data_310,data_311,data_312,data_313,data_314,data_315,data_316,data_317,
	data_318,data_319,data_320,data_321,data_322,data_323,data_324,data_325,
	data_326,data_327,data_328,data_329,data_330,data_331,data_410,data_411,
	data_412,data_413,data_414,data_415,data_416,data_417,data_418,data_419,
	data_420,data_421,data_422,data_423,data_424,data_425,data_426,data_427,
	data_428,data_429,data_430,data_431,data_510,data_511,data_512,data_513,
	data_514,data_515,data_516,data_517,data_518,data_519,data_520,data_521,
	data_522,data_523,data_524,data_525,data_526,data_527,data_528,data_529,
	data_530,data_531,data_610,data_611,data_612,data_613,data_614,data_615,
	data_616,data_617,data_618,data_619,data_620,data_621,data_622,data_623,
	data_624,data_625,data_626,data_627,data_628,data_629,data_630,data_631,
	data_710,data_711,data_712,data_713,data_714,data_715,data_716,data_717,
	data_718,data_719,data_720,data_721,data_722,data_723,data_724,data_725,
	data_726,data_727,data_728,data_729,data_730,data_731,eve_cou0,eve_cou1,
	eve_cou2,eve_cou3,eve_cou4,eve_cou5,eve_cou6,eve_cou7,eve_cou8,eve_cou9,
	eve_cou10,eve_cou11,eve_cou12,eve_cou13,eve_cou14,eve_cou15,eve_cou16,
	eve_cou17,eve_cou18,eve_cou19,eve_cou20,eve_cou21,eve_cou22,eve_cou23,
	eve_cou24,eve_cou25,eve_cou26,eve_cou27,eve_cou28,eve_cou29,eve_cou30,
	eve_cou31,RESET,cmd,data_reg_out0,data_reg_out1,data_reg_out2,data_reg_out3,
	data_reg_out4,data_reg_out5,data_reg_out6,data_reg_out7,data_reg_out8,
	data_reg_out9,data_reg_out10,data_reg_out11,data_reg_out12,data_reg_out13,
	data_reg_out14,data_reg_out15,data_reg_out16,data_reg_out17,data_reg_out18,
	data_reg_out19,data_reg_out20,data_reg_out21,data_reg_out22,data_reg_out23,
	data_reg_out24,data_reg_out25,data_reg_out26,data_reg_out27,data_reg_out28,
	data_reg_out29,data_reg_out30,data_reg_out31,FIFO_we,mux_out,reset_code);

	input CLK;
	input atom_data,chip_addr0,chip_addr1,chip_addr2,chip_addr3,chip_addr4,
		cmd_code0,cmd_code1,cmd_code2,cmd_code3,cmd_code4,data0,data1,data2,data3,
		data4,data5,data6,data7,data8,data9,data10,data11,data12,data13,data14,data15
		,data16,data17,data18,data19,data20,data21,data22,data23,data24,data25,data26
		,data27,data28,data29,data30,data31,data_10,data_11,data_12,data_13,data_14,
		data_15,data_16,data_17,data_18,data_19,data_20,data_21,data_22,data_23,
		data_24,data_25,data_26,data_27,data_28,data_29,data_30,data_31,data_32,
		data_33,data_34,data_35,data_36,data_37,data_38,data_39,data_40,data_41,
		data_42,data_43,data_44,data_45,data_46,data_47,data_48,data_49,data_50,
		data_51,data_52,data_53,data_54,data_55,data_56,data_57,data_58,data_59,
		data_60,data_61,data_62,data_63,data_64,data_65,data_66,data_67,data_68,
		data_69,data_70,data_71,data_72,data_73,data_74,data_75,data_76,data_77,
		data_78,data_79,data_110,data_111,data_112,data_113,data_114,data_115,
		data_116,data_117,data_118,data_119,data_120,data_121,data_122,data_123,
		data_124,data_125,data_126,data_127,data_128,data_129,data_130,data_131,
		data_210,data_211,data_212,data_213,data_214,data_215,data_216,data_217,
		data_218,data_219,data_220,data_221,data_222,data_223,data_224,data_225,
		data_226,data_227,data_228,data_229,data_230,data_231,data_310,data_311,
		data_312,data_313,data_314,data_315,data_316,data_317,data_318,data_319,
		data_320,data_321,data_322,data_323,data_324,data_325,data_326,data_327,
		data_328,data_329,data_330,data_331,data_410,data_411,data_412,data_413,
		data_414,data_415,data_416,data_417,data_418,data_419,data_420,data_421,
		data_422,data_423,data_424,data_425,data_426,data_427,data_428,data_429,
		data_430,data_431,data_510,data_511,data_512,data_513,data_514,data_515,
		data_516,data_517,data_518,data_519,data_520,data_521,data_522,data_523,
		data_524,data_525,data_526,data_527,data_528,data_529,data_530,data_531,
		data_610,data_611,data_612,data_613,data_614,data_615,data_616,data_617,
		data_618,data_619,data_620,data_621,data_622,data_623,data_624,data_625,
		data_626,data_627,data_628,data_629,data_630,data_631,data_710,data_711,
		data_712,data_713,data_714,data_715,data_716,data_717,data_718,data_719,
		data_720,data_721,data_722,data_723,data_724,data_725,data_726,data_727,
		data_728,data_729,data_730,data_731,eve_cou0,eve_cou1,eve_cou2,eve_cou3,
		eve_cou4,eve_cou5,eve_cou6,eve_cou7,eve_cou8,eve_cou9,eve_cou10,eve_cou11,
		eve_cou12,eve_cou13,eve_cou14,eve_cou15,eve_cou16,eve_cou17,eve_cou18,
		eve_cou19,eve_cou20,eve_cou21,eve_cou22,eve_cou23,eve_cou24,eve_cou25,
		eve_cou26,eve_cou27,eve_cou28,eve_cou29,eve_cou30,eve_cou31,RESET;
	output cmd,data_reg_out0,data_reg_out1,data_reg_out2,data_reg_out3,
		data_reg_out4,data_reg_out5,data_reg_out6,data_reg_out7,data_reg_out8,
		data_reg_out9,data_reg_out10,data_reg_out11,data_reg_out12,data_reg_out13,
		data_reg_out14,data_reg_out15,data_reg_out16,data_reg_out17,data_reg_out18,
		data_reg_out19,data_reg_out20,data_reg_out21,data_reg_out22,data_reg_out23,
		data_reg_out24,data_reg_out25,data_reg_out26,data_reg_out27,data_reg_out28,
		data_reg_out29,data_reg_out30,data_reg_out31,FIFO_we,mux_out,reset_code;

	reg [31:0] BP_data_reg_out;
	reg [5:0] cnt;
	reg [31:0] data_reg;
	reg [31:0] data_reg_out;
	reg [4:0] shift_reg;
	reg [1:0] temp_cmd;
	reg BP_cmd,next_BP_cmd,BP_data_reg_out0,next_BP_data_reg_out0,
		BP_data_reg_out1,next_BP_data_reg_out1,BP_data_reg_out2,next_BP_data_reg_out2
		,BP_data_reg_out3,next_BP_data_reg_out3,BP_data_reg_out4,
		next_BP_data_reg_out4,BP_data_reg_out5,next_BP_data_reg_out5,BP_data_reg_out6
		,next_BP_data_reg_out6,BP_data_reg_out7,next_BP_data_reg_out7,
		BP_data_reg_out8,next_BP_data_reg_out8,BP_data_reg_out9,next_BP_data_reg_out9
		,BP_data_reg_out10,next_BP_data_reg_out10,BP_data_reg_out11,
		next_BP_data_reg_out11,BP_data_reg_out12,next_BP_data_reg_out12,
		BP_data_reg_out13,next_BP_data_reg_out13,BP_data_reg_out14,
		next_BP_data_reg_out14,BP_data_reg_out15,next_BP_data_reg_out15,
		BP_data_reg_out16,next_BP_data_reg_out16,BP_data_reg_out17,
		next_BP_data_reg_out17,BP_data_reg_out18,next_BP_data_reg_out18,
		BP_data_reg_out19,next_BP_data_reg_out19,BP_data_reg_out20,
		next_BP_data_reg_out20,BP_data_reg_out21,next_BP_data_reg_out21,
		BP_data_reg_out22,next_BP_data_reg_out22,BP_data_reg_out23,
		next_BP_data_reg_out23,BP_data_reg_out24,next_BP_data_reg_out24,
		BP_data_reg_out25,next_BP_data_reg_out25,BP_data_reg_out26,
		next_BP_data_reg_out26,BP_data_reg_out27,next_BP_data_reg_out27,
		BP_data_reg_out28,next_BP_data_reg_out28,BP_data_reg_out29,
		next_BP_data_reg_out29,BP_data_reg_out30,next_BP_data_reg_out30,
		BP_data_reg_out31,next_BP_data_reg_out31,BP_FIFO_we,next_BP_FIFO_we,
		BP_mux_out,next_BP_mux_out,BP_reset_code,next_BP_reset_code,cnt0,next_cnt0,
		cnt1,next_cnt1,cnt2,next_cnt2,cnt3,next_cnt3,cnt4,next_cnt4,cnt5,next_cnt5,
		data_reg0,next_data_reg0,data_reg1,next_data_reg1,data_reg2,next_data_reg2,
		data_reg3,next_data_reg3,data_reg4,next_data_reg4,data_reg5,next_data_reg5,
		data_reg6,next_data_reg6,data_reg7,next_data_reg7,data_reg8,next_data_reg8,
		data_reg9,next_data_reg9,data_reg10,next_data_reg10,data_reg11,
		next_data_reg11,data_reg12,next_data_reg12,data_reg13,next_data_reg13,
		data_reg14,next_data_reg14,data_reg15,next_data_reg15,data_reg16,
		next_data_reg16,data_reg17,next_data_reg17,data_reg18,next_data_reg18,
		data_reg19,next_data_reg19,data_reg20,next_data_reg20,data_reg21,
		next_data_reg21,data_reg22,next_data_reg22,data_reg23,next_data_reg23,
		data_reg24,next_data_reg24,data_reg25,next_data_reg25,data_reg26,
		next_data_reg26,data_reg27,next_data_reg27,data_reg28,next_data_reg28,
		data_reg29,next_data_reg29,data_reg30,next_data_reg30,data_reg31,
		next_data_reg31,shift_reg0,next_shift_reg0,shift_reg1,next_shift_reg1,
		shift_reg2,next_shift_reg2,shift_reg3,next_shift_reg3,shift_reg4,
		next_shift_reg4,temp_cmd0,next_temp_cmd0,temp_cmd1,next_temp_cmd1;
	reg cmd,data_reg_out0,data_reg_out1,data_reg_out2,data_reg_out3,
		data_reg_out4,data_reg_out5,data_reg_out6,data_reg_out7,data_reg_out8,
		data_reg_out9,data_reg_out10,data_reg_out11,data_reg_out12,data_reg_out13,
		data_reg_out14,data_reg_out15,data_reg_out16,data_reg_out17,data_reg_out18,
		data_reg_out19,data_reg_out20,data_reg_out21,data_reg_out22,data_reg_out23,
		data_reg_out24,data_reg_out25,data_reg_out26,data_reg_out27,data_reg_out28,
		data_reg_out29,data_reg_out30,data_reg_out31,FIFO_we,mux_out,reset_code;
	reg read_event,next_read_event,read_registers,next_read_registers,
		rescmd_sync,next_rescmd_sync,sbit_clr,next_sbit_clr,sbit_cstr,next_sbit_cstr,
		sbit_mr,next_sbit_mr,sbit_rege,next_sbit_rege,sbit_reve,next_sbit_reve,
		sbit_sync,next_sbit_sync,sbit_tra,next_sbit_tra,sbit_wccm,next_sbit_wccm,
		sbit_wcps,next_sbit_wcps,sbit_wcr,next_sbit_wcr,send_add_rege,
		next_send_add_rege,send_addr_mr,next_send_addr_mr,send_addr_wccm,
		next_send_addr_wccm,send_addr_wcps,next_send_addr_wcps,send_addr_wcr,
		next_send_addr_wcr,send_cmd_clr,next_send_cmd_clr,send_cmd_cstr,
		next_send_cmd_cstr,send_cmd_mr,next_send_cmd_mr,send_cmd_reve,
		next_send_cmd_reve,send_cmd_sync,next_send_cmd_sync,send_cmd_tra,
		next_send_cmd_tra,send_cmd_wccm,next_send_cmd_wccm,send_cmd_wcps,
		next_send_cmd_wcps,send_cmd_wcr,next_send_cmd_wcr,send_code_rege,
		next_send_code_rege,send_data1_wccm,next_send_data1_wccm,send_data2_wccm,
		next_send_data2_wccm,send_data3_wccm,next_send_data3_wccm,send_data4_wccm,
		next_send_data4_wccm,send_data5_wccm,next_send_data5_wccm,send_data6_wccm,
		next_send_data6_wccm,send_data7_wccm,next_send_data7_wccm,send_data_tra,
		next_send_data_tra,send_data_wccm,next_send_data_wccm,send_data_wcps,
		next_send_data_wcps,send_data_wcr,next_send_data_wcr,wait_sbit_rege,
		next_wait_sbit_rege,wait_sbit_reve,next_wait_sbit_reve,wait_sequence,
		next_wait_sequence,zbit_clr,next_zbit_clr,zbit_cstr,next_zbit_cstr,zbit_mr,
		next_zbit_mr,zbit_rege,next_zbit_rege,zbit_reve,next_zbit_reve,zbit_sync,
		next_zbit_sync,zbit_tra,next_zbit_tra,zbit_wccm,next_zbit_wccm,zbit_wcps,
		next_zbit_wcps,zbit_wcr,next_zbit_wcr;

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) read_event = 0;
		else read_event = next_read_event;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) read_registers = 0;
		else read_registers = next_read_registers;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) rescmd_sync = 0;
		else rescmd_sync = next_rescmd_sync;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) sbit_clr = 0;
		else sbit_clr = next_sbit_clr;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) sbit_cstr = 0;
		else sbit_cstr = next_sbit_cstr;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) sbit_mr = 0;
		else sbit_mr = next_sbit_mr;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) sbit_rege = 0;
		else sbit_rege = next_sbit_rege;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) sbit_reve = 0;
		else sbit_reve = next_sbit_reve;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) sbit_sync = 0;
		else sbit_sync = next_sbit_sync;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) sbit_tra = 0;
		else sbit_tra = next_sbit_tra;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) sbit_wccm = 0;
		else sbit_wccm = next_sbit_wccm;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) sbit_wcps = 0;
		else sbit_wcps = next_sbit_wcps;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) sbit_wcr = 0;
		else sbit_wcr = next_sbit_wcr;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) send_add_rege = 0;
		else send_add_rege = next_send_add_rege;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) send_addr_mr = 0;
		else send_addr_mr = next_send_addr_mr;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) send_addr_wccm = 0;
		else send_addr_wccm = next_send_addr_wccm;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) send_addr_wcps = 0;
		else send_addr_wcps = next_send_addr_wcps;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) send_addr_wcr = 0;
		else send_addr_wcr = next_send_addr_wcr;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) send_cmd_clr = 0;
		else send_cmd_clr = next_send_cmd_clr;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) send_cmd_cstr = 0;
		else send_cmd_cstr = next_send_cmd_cstr;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) send_cmd_mr = 0;
		else send_cmd_mr = next_send_cmd_mr;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) send_cmd_reve = 0;
		else send_cmd_reve = next_send_cmd_reve;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) send_cmd_sync = 0;
		else send_cmd_sync = next_send_cmd_sync;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) send_cmd_tra = 0;
		else send_cmd_tra = next_send_cmd_tra;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) send_cmd_wccm = 0;
		else send_cmd_wccm = next_send_cmd_wccm;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) send_cmd_wcps = 0;
		else send_cmd_wcps = next_send_cmd_wcps;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) send_cmd_wcr = 0;
		else send_cmd_wcr = next_send_cmd_wcr;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) send_code_rege = 0;
		else send_code_rege = next_send_code_rege;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) send_data1_wccm = 0;
		else send_data1_wccm = next_send_data1_wccm;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) send_data2_wccm = 0;
		else send_data2_wccm = next_send_data2_wccm;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) send_data3_wccm = 0;
		else send_data3_wccm = next_send_data3_wccm;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) send_data4_wccm = 0;
		else send_data4_wccm = next_send_data4_wccm;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) send_data5_wccm = 0;
		else send_data5_wccm = next_send_data5_wccm;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) send_data6_wccm = 0;
		else send_data6_wccm = next_send_data6_wccm;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) send_data7_wccm = 0;
		else send_data7_wccm = next_send_data7_wccm;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) send_data_tra = 0;
		else send_data_tra = next_send_data_tra;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) send_data_wccm = 0;
		else send_data_wccm = next_send_data_wccm;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) send_data_wcps = 0;
		else send_data_wcps = next_send_data_wcps;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) send_data_wcr = 0;
		else send_data_wcr = next_send_data_wcr;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) wait_sbit_rege = 0;
		else wait_sbit_rege = next_wait_sbit_rege;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) wait_sbit_reve = 0;
		else wait_sbit_reve = next_wait_sbit_reve;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) wait_sequence = 1;
		else wait_sequence = next_wait_sequence;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) zbit_clr = 0;
		else zbit_clr = next_zbit_clr;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) zbit_cstr = 0;
		else zbit_cstr = next_zbit_cstr;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) zbit_mr = 0;
		else zbit_mr = next_zbit_mr;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) zbit_rege = 0;
		else zbit_rege = next_zbit_rege;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) zbit_reve = 0;
		else zbit_reve = next_zbit_reve;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) zbit_sync = 0;
		else zbit_sync = next_zbit_sync;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) zbit_tra = 0;
		else zbit_tra = next_zbit_tra;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) zbit_wccm = 0;
		else zbit_wccm = next_zbit_wccm;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) zbit_wcps = 0;
		else zbit_wcps = next_zbit_wcps;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) zbit_wcr = 0;
		else zbit_wcr = next_zbit_wcr;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) BP_data_reg_out31 = 0;
		else BP_data_reg_out31 = next_BP_data_reg_out31;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) BP_data_reg_out30 = 0;
		else BP_data_reg_out30 = next_BP_data_reg_out30;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) BP_data_reg_out29 = 0;
		else BP_data_reg_out29 = next_BP_data_reg_out29;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) BP_data_reg_out28 = 0;
		else BP_data_reg_out28 = next_BP_data_reg_out28;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) BP_data_reg_out27 = 0;
		else BP_data_reg_out27 = next_BP_data_reg_out27;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) BP_data_reg_out26 = 0;
		else BP_data_reg_out26 = next_BP_data_reg_out26;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) BP_data_reg_out25 = 0;
		else BP_data_reg_out25 = next_BP_data_reg_out25;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) BP_data_reg_out24 = 0;
		else BP_data_reg_out24 = next_BP_data_reg_out24;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) BP_data_reg_out23 = 0;
		else BP_data_reg_out23 = next_BP_data_reg_out23;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) BP_data_reg_out22 = 0;
		else BP_data_reg_out22 = next_BP_data_reg_out22;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) BP_data_reg_out21 = 0;
		else BP_data_reg_out21 = next_BP_data_reg_out21;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) BP_data_reg_out20 = 0;
		else BP_data_reg_out20 = next_BP_data_reg_out20;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) BP_data_reg_out19 = 0;
		else BP_data_reg_out19 = next_BP_data_reg_out19;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) BP_data_reg_out18 = 0;
		else BP_data_reg_out18 = next_BP_data_reg_out18;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) BP_data_reg_out17 = 0;
		else BP_data_reg_out17 = next_BP_data_reg_out17;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) BP_data_reg_out16 = 0;
		else BP_data_reg_out16 = next_BP_data_reg_out16;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) BP_data_reg_out15 = 0;
		else BP_data_reg_out15 = next_BP_data_reg_out15;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) BP_data_reg_out14 = 0;
		else BP_data_reg_out14 = next_BP_data_reg_out14;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) BP_data_reg_out13 = 0;
		else BP_data_reg_out13 = next_BP_data_reg_out13;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) BP_data_reg_out12 = 0;
		else BP_data_reg_out12 = next_BP_data_reg_out12;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) BP_data_reg_out11 = 0;
		else BP_data_reg_out11 = next_BP_data_reg_out11;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) BP_data_reg_out10 = 0;
		else BP_data_reg_out10 = next_BP_data_reg_out10;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) BP_data_reg_out9 = 0;
		else BP_data_reg_out9 = next_BP_data_reg_out9;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) BP_data_reg_out8 = 0;
		else BP_data_reg_out8 = next_BP_data_reg_out8;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) BP_data_reg_out7 = 0;
		else BP_data_reg_out7 = next_BP_data_reg_out7;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) BP_data_reg_out6 = 0;
		else BP_data_reg_out6 = next_BP_data_reg_out6;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) BP_data_reg_out5 = 0;
		else BP_data_reg_out5 = next_BP_data_reg_out5;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) BP_data_reg_out4 = 0;
		else BP_data_reg_out4 = next_BP_data_reg_out4;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) BP_data_reg_out3 = 0;
		else BP_data_reg_out3 = next_BP_data_reg_out3;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) BP_data_reg_out2 = 0;
		else BP_data_reg_out2 = next_BP_data_reg_out2;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) BP_data_reg_out1 = 0;
		else BP_data_reg_out1 = next_BP_data_reg_out1;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) BP_data_reg_out0 = 0;
		else BP_data_reg_out0 = next_BP_data_reg_out0;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) cnt5 = 0;
		else cnt5 = next_cnt5;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) cnt4 = 0;
		else cnt4 = next_cnt4;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) cnt3 = 0;
		else cnt3 = next_cnt3;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) cnt2 = 0;
		else cnt2 = next_cnt2;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) cnt1 = 0;
		else cnt1 = next_cnt1;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) cnt0 = 0;
		else cnt0 = next_cnt0;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) data_reg31 = 0;
		else data_reg31 = next_data_reg31;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) data_reg30 = 0;
		else data_reg30 = next_data_reg30;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) data_reg29 = 0;
		else data_reg29 = next_data_reg29;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) data_reg28 = 0;
		else data_reg28 = next_data_reg28;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) data_reg27 = 0;
		else data_reg27 = next_data_reg27;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) data_reg26 = 0;
		else data_reg26 = next_data_reg26;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) data_reg25 = 0;
		else data_reg25 = next_data_reg25;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) data_reg24 = 0;
		else data_reg24 = next_data_reg24;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) data_reg23 = 0;
		else data_reg23 = next_data_reg23;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) data_reg22 = 0;
		else data_reg22 = next_data_reg22;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) data_reg21 = 0;
		else data_reg21 = next_data_reg21;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) data_reg20 = 0;
		else data_reg20 = next_data_reg20;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) data_reg19 = 0;
		else data_reg19 = next_data_reg19;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) data_reg18 = 0;
		else data_reg18 = next_data_reg18;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) data_reg17 = 0;
		else data_reg17 = next_data_reg17;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) data_reg16 = 0;
		else data_reg16 = next_data_reg16;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) data_reg15 = 0;
		else data_reg15 = next_data_reg15;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) data_reg14 = 0;
		else data_reg14 = next_data_reg14;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) data_reg13 = 0;
		else data_reg13 = next_data_reg13;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) data_reg12 = 0;
		else data_reg12 = next_data_reg12;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) data_reg11 = 0;
		else data_reg11 = next_data_reg11;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) data_reg10 = 0;
		else data_reg10 = next_data_reg10;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) data_reg9 = 0;
		else data_reg9 = next_data_reg9;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) data_reg8 = 0;
		else data_reg8 = next_data_reg8;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) data_reg7 = 0;
		else data_reg7 = next_data_reg7;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) data_reg6 = 0;
		else data_reg6 = next_data_reg6;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) data_reg5 = 0;
		else data_reg5 = next_data_reg5;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) data_reg4 = 0;
		else data_reg4 = next_data_reg4;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) data_reg3 = 0;
		else data_reg3 = next_data_reg3;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) data_reg2 = 0;
		else data_reg2 = next_data_reg2;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) data_reg1 = 0;
		else data_reg1 = next_data_reg1;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) data_reg0 = 0;
		else data_reg0 = next_data_reg0;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) shift_reg4 = 0;
		else shift_reg4 = next_shift_reg4;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) shift_reg3 = 0;
		else shift_reg3 = next_shift_reg3;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) shift_reg2 = 0;
		else shift_reg2 = next_shift_reg2;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) shift_reg1 = 0;
		else shift_reg1 = next_shift_reg1;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) shift_reg0 = 0;
		else shift_reg0 = next_shift_reg0;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) temp_cmd1 = 0;
		else temp_cmd1 = next_temp_cmd1;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) temp_cmd0 = 0;
		else temp_cmd0 = next_temp_cmd0;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) BP_cmd = 0;
		else BP_cmd = next_BP_cmd;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) BP_FIFO_we = 0;
		else BP_FIFO_we = next_BP_FIFO_we;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) BP_mux_out = 0;
		else BP_mux_out = next_BP_mux_out;
	end

	always @(posedge CLK or posedge RESET)
	begin
		if ( RESET ) BP_reset_code = 0;
		else BP_reset_code = next_BP_reset_code;
	end

	always @ (atom_data or BP_cmd or BP_data_reg_out0 or BP_data_reg_out1 or 
		BP_data_reg_out2 or BP_data_reg_out3 or BP_data_reg_out4 or BP_data_reg_out5 
		or BP_data_reg_out6 or BP_data_reg_out7 or BP_data_reg_out8 or 
		BP_data_reg_out9 or BP_data_reg_out10 or BP_data_reg_out11 or 
		BP_data_reg_out12 or BP_data_reg_out13 or BP_data_reg_out14 or 
		BP_data_reg_out15 or BP_data_reg_out16 or BP_data_reg_out17 or 
		BP_data_reg_out18 or BP_data_reg_out19 or BP_data_reg_out20 or 
		BP_data_reg_out21 or BP_data_reg_out22 or BP_data_reg_out23 or 
		BP_data_reg_out24 or BP_data_reg_out25 or BP_data_reg_out26 or 
		BP_data_reg_out27 or BP_data_reg_out28 or BP_data_reg_out29 or 
		BP_data_reg_out30 or BP_data_reg_out31 or BP_FIFO_we or BP_mux_out or 
		BP_reset_code or chip_addr0 or chip_addr1 or chip_addr2 or chip_addr3 or 
		chip_addr4 or cmd_code0 or cmd_code1 or cmd_code2 or cmd_code3 or cmd_code4 
		or cnt0 or cnt1 or cnt2 or cnt3 or cnt4 or cnt5 or data0 or data1 or data2 or
		 data3 or data4 or data5 or data6 or data7 or data8 or data9 or data10 or 
		data11 or data12 or data13 or data14 or data15 or data16 or data17 or data18 
		or data19 or data20 or data21 or data22 or data23 or data24 or data25 or 
		data26 or data27 or data28 or data29 or data30 or data31 or data_10 or 
		data_11 or data_12 or data_13 or data_14 or data_15 or data_16 or data_17 or 
		data_18 or data_19 or data_20 or data_21 or data_22 or data_23 or data_24 or 
		data_25 or data_26 or data_27 or data_28 or data_29 or data_30 or data_31 or 
		data_32 or data_33 or data_34 or data_35 or data_36 or data_37 or data_38 or 
		data_39 or data_40 or data_41 or data_42 or data_43 or data_44 or data_45 or 
		data_46 or data_47 or data_48 or data_49 or data_50 or data_51 or data_52 or 
		data_53 or data_54 or data_55 or data_56 or data_57 or data_58 or data_59 or 
		data_60 or data_61 or data_62 or data_63 or data_64 or data_65 or data_66 or 
		data_67 or data_68 or data_69 or data_70 or data_71 or data_72 or data_73 or 
		data_74 or data_75 or data_76 or data_77 or data_78 or data_79 or data_110 or
		 data_111 or data_112 or data_113 or data_114 or data_115 or data_116 or 
		data_117 or data_118 or data_119 or data_120 or data_121 or data_122 or 
		data_123 or data_124 or data_125 or data_126 or data_127 or data_128 or 
		data_129 or data_130 or data_131 or data_210 or data_211 or data_212 or 
		data_213 or data_214 or data_215 or data_216 or data_217 or data_218 or 
		data_219 or data_220 or data_221 or data_222 or data_223 or data_224 or 
		data_225 or data_226 or data_227 or data_228 or data_229 or data_230 or 
		data_231 or data_310 or data_311 or data_312 or data_313 or data_314 or 
		data_315 or data_316 or data_317 or data_318 or data_319 or data_320 or 
		data_321 or data_322 or data_323 or data_324 or data_325 or data_326 or 
		data_327 or data_328 or data_329 or data_330 or data_331 or data_410 or 
		data_411 or data_412 or data_413 or data_414 or data_415 or data_416 or 
		data_417 or data_418 or data_419 or data_420 or data_421 or data_422 or 
		data_423 or data_424 or data_425 or data_426 or data_427 or data_428 or 
		data_429 or data_430 or data_431 or data_510 or data_511 or data_512 or 
		data_513 or data_514 or data_515 or data_516 or data_517 or data_518 or 
		data_519 or data_520 or data_521 or data_522 or data_523 or data_524 or 
		data_525 or data_526 or data_527 or data_528 or data_529 or data_530 or 
		data_531 or data_610 or data_611 or data_612 or data_613 or data_614 or 
		data_615 or data_616 or data_617 or data_618 or data_619 or data_620 or 
		data_621 or data_622 or data_623 or data_624 or data_625 or data_626 or 
		data_627 or data_628 or data_629 or data_630 or data_631 or data_710 or 
		data_711 or data_712 or data_713 or data_714 or data_715 or data_716 or 
		data_717 or data_718 or data_719 or data_720 or data_721 or data_722 or 
		data_723 or data_724 or data_725 or data_726 or data_727 or data_728 or 
		data_729 or data_730 or data_731 or data_reg0 or data_reg1 or data_reg2 or 
		data_reg3 or data_reg4 or data_reg5 or data_reg6 or data_reg7 or data_reg8 or
		 data_reg9 or data_reg10 or data_reg11 or data_reg12 or data_reg13 or 
		data_reg14 or data_reg15 or data_reg16 or data_reg17 or data_reg18 or 
		data_reg19 or data_reg20 or data_reg21 or data_reg22 or data_reg23 or 
		data_reg24 or data_reg25 or data_reg26 or data_reg27 or data_reg28 or 
		data_reg29 or data_reg30 or data_reg31 or eve_cou0 or eve_cou1 or eve_cou2 or
		 eve_cou3 or eve_cou4 or eve_cou5 or eve_cou6 or eve_cou7 or eve_cou8 or 
		eve_cou9 or eve_cou10 or eve_cou11 or eve_cou12 or eve_cou13 or eve_cou14 or 
		eve_cou15 or eve_cou16 or eve_cou17 or eve_cou18 or eve_cou19 or eve_cou20 or
		 eve_cou21 or eve_cou22 or eve_cou23 or eve_cou24 or eve_cou25 or eve_cou26 
		or eve_cou27 or eve_cou28 or eve_cou29 or eve_cou30 or eve_cou31 or 
		read_event or read_registers or rescmd_sync or sbit_clr or sbit_cstr or 
		sbit_mr or sbit_rege or sbit_reve or sbit_sync or sbit_tra or sbit_wccm or 
		sbit_wcps or sbit_wcr or send_add_rege or send_addr_mr or send_addr_wccm or 
		send_addr_wcps or send_addr_wcr or send_cmd_clr or send_cmd_cstr or 
		send_cmd_mr or send_cmd_reve or send_cmd_sync or send_cmd_tra or 
		send_cmd_wccm or send_cmd_wcps or send_cmd_wcr or send_code_rege or 
		send_data1_wccm or send_data2_wccm or send_data3_wccm or send_data4_wccm or 
		send_data5_wccm or send_data6_wccm or send_data7_wccm or send_data_tra or 
		send_data_wccm or send_data_wcps or send_data_wcr or shift_reg0 or shift_reg1
		 or shift_reg2 or shift_reg3 or shift_reg4 or temp_cmd0 or temp_cmd1 or 
		wait_sbit_rege or wait_sbit_reve or wait_sequence or zbit_clr or zbit_cstr or
		 zbit_mr or zbit_rege or zbit_reve or zbit_sync or zbit_tra or zbit_wccm or 
		zbit_wcps or zbit_wcr or BP_data_reg_out or cnt or data_reg or shift_reg or 
		temp_cmd)
	begin

		if ( ~cnt5 & read_event | cnt4 & read_event | cnt3 & read_event | cnt2 & 
			read_event | cnt1 & read_event | cnt0 & read_event | data_reg0 & read_event |
			 data_reg1 & read_event | data_reg2 & read_event | data_reg3 & read_event | 
			data_reg4 & read_event | data_reg5 & read_event | data_reg6 & read_event | 
			data_reg7 & read_event | data_reg8 & read_event | data_reg9 & read_event | 
			data_reg10 & read_event | data_reg11 & read_event | data_reg12 & read_event |
			 data_reg13 & read_event | data_reg14 & read_event | data_reg15 & read_event 
			| data_reg16 & read_event | data_reg17 & read_event | data_reg18 & read_event
			 | data_reg19 & read_event | data_reg20 & read_event | data_reg21 & 
			read_event | data_reg22 & read_event | data_reg23 & read_event | data_reg24 &
			 read_event | data_reg25 & read_event | data_reg26 & read_event | data_reg27 
			& read_event | data_reg28 & read_event | data_reg29 & read_event | data_reg30
			 & read_event | data_reg31 & read_event | atom_data & wait_sbit_reve ) 
			next_read_event=1;
		else next_read_event=0;

		if ( ~cnt5 & read_registers | cnt4 & read_registers | cnt3 & read_registers
			 | cnt2 & read_registers | cnt1 & read_registers | cnt0 & read_registers | 
			data_reg0 & read_registers | data_reg1 & read_registers | data_reg2 & 
			read_registers | data_reg3 & read_registers | data_reg4 & read_registers | 
			data_reg5 & read_registers | data_reg6 & read_registers | data_reg7 & 
			read_registers | data_reg8 & read_registers | data_reg9 & read_registers | 
			data_reg10 & read_registers | data_reg11 & read_registers | data_reg12 & 
			read_registers | data_reg13 & read_registers | data_reg14 & read_registers | 
			data_reg15 & read_registers | data_reg16 & read_registers | data_reg17 & 
			read_registers | data_reg18 & read_registers | data_reg19 & read_registers | 
			data_reg20 & read_registers | data_reg21 & read_registers | data_reg22 & 
			read_registers | data_reg23 & read_registers | data_reg24 & read_registers | 
			data_reg25 & read_registers | data_reg26 & read_registers | data_reg27 & 
			read_registers | data_reg28 & read_registers | data_reg29 & read_registers | 
			data_reg30 & read_registers | data_reg31 & read_registers | atom_data & 
			wait_sbit_rege ) next_read_registers=1;
		else next_read_registers=0;

		if ( ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & ~cnt5 & send_cmd_sync ) 
			next_rescmd_sync=1;
		else next_rescmd_sync=0;

		if ( zbit_clr ) next_sbit_clr=1;
		else next_sbit_clr=0;

		if ( zbit_cstr ) next_sbit_cstr=1;
		else next_sbit_cstr=0;

		if ( zbit_mr ) next_sbit_mr=1;
		else next_sbit_mr=0;

		if ( zbit_rege ) next_sbit_rege=1;
		else next_sbit_rege=0;

		if ( zbit_reve ) next_sbit_reve=1;
		else next_sbit_reve=0;

		if ( zbit_sync ) next_sbit_sync=1;
		else next_sbit_sync=0;

		if ( zbit_tra ) next_sbit_tra=1;
		else next_sbit_tra=0;

		if ( zbit_wccm ) next_sbit_wccm=1;
		else next_sbit_wccm=0;

		if ( zbit_wcps ) next_sbit_wcps=1;
		else next_sbit_wcps=0;

		if ( zbit_wcr ) next_sbit_wcr=1;
		else next_sbit_wcr=0;

		if ( cnt0 & send_add_rege | cnt1 & send_add_rege | cnt2 & send_add_rege | 
			cnt3 & send_add_rege | cnt4 & send_add_rege | cnt5 & send_add_rege | ~cnt0 & 
			~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & ~cnt5 & send_code_rege ) 
			next_send_add_rege=1;
		else next_send_add_rege=0;

		if ( cnt0 & send_addr_mr | cnt1 & send_addr_mr | cnt2 & send_addr_mr | cnt3
			 & send_addr_mr | cnt4 & send_addr_mr | cnt5 & send_addr_mr | ~cnt0 & ~cnt1 &
			 ~cnt2 & ~cnt3 & ~cnt4 & ~cnt5 & send_cmd_mr ) next_send_addr_mr=1;
		else next_send_addr_mr=0;

		if ( cnt0 & send_addr_wccm | cnt1 & send_addr_wccm | cnt2 & send_addr_wccm 
			| cnt3 & send_addr_wccm | cnt4 & send_addr_wccm | cnt5 & send_addr_wccm | 
			~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & ~cnt5 & send_cmd_wccm ) 
			next_send_addr_wccm=1;
		else next_send_addr_wccm=0;

		if ( cnt0 & send_addr_wcps | cnt1 & send_addr_wcps | cnt2 & send_addr_wcps 
			| cnt3 & send_addr_wcps | cnt4 & send_addr_wcps | cnt5 & send_addr_wcps | 
			~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & ~cnt5 & send_cmd_wcps ) 
			next_send_addr_wcps=1;
		else next_send_addr_wcps=0;

		if ( cnt0 & send_addr_wcr | cnt1 & send_addr_wcr | cnt2 & send_addr_wcr | 
			cnt3 & send_addr_wcr | cnt4 & send_addr_wcr | cnt5 & send_addr_wcr | ~cnt0 & 
			~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & ~cnt5 & send_cmd_wcr ) next_send_addr_wcr=1;
		else next_send_addr_wcr=0;

		if ( sbit_clr | cnt0 & send_cmd_clr | cnt1 & send_cmd_clr | cnt2 & 
			send_cmd_clr | cnt3 & send_cmd_clr | cnt4 & send_cmd_clr | cnt5 & 
			send_cmd_clr ) next_send_cmd_clr=1;
		else next_send_cmd_clr=0;

		if ( sbit_cstr | cnt0 & send_cmd_cstr | cnt1 & send_cmd_cstr | cnt2 & 
			send_cmd_cstr | cnt3 & send_cmd_cstr | cnt4 & send_cmd_cstr | cnt5 & 
			send_cmd_cstr ) next_send_cmd_cstr=1;
		else next_send_cmd_cstr=0;

		if ( sbit_mr | cnt0 & send_cmd_mr | cnt1 & send_cmd_mr | cnt2 & send_cmd_mr
			 | cnt3 & send_cmd_mr | cnt4 & send_cmd_mr | cnt5 & send_cmd_mr ) 
			next_send_cmd_mr=1;
		else next_send_cmd_mr=0;

		if ( sbit_reve | cnt0 & send_cmd_reve | cnt1 & send_cmd_reve | cnt2 & 
			send_cmd_reve | cnt3 & send_cmd_reve | cnt4 & send_cmd_reve | cnt5 & 
			send_cmd_reve ) next_send_cmd_reve=1;
		else next_send_cmd_reve=0;

		if ( sbit_sync | cnt0 & send_cmd_sync | cnt1 & send_cmd_sync | cnt2 & 
			send_cmd_sync | cnt3 & send_cmd_sync | cnt4 & send_cmd_sync | cnt5 & 
			send_cmd_sync ) next_send_cmd_sync=1;
		else next_send_cmd_sync=0;

		if ( sbit_tra | cnt0 & send_cmd_tra | cnt1 & send_cmd_tra | cnt2 & 
			send_cmd_tra | cnt3 & send_cmd_tra | cnt4 & send_cmd_tra | cnt5 & 
			send_cmd_tra ) next_send_cmd_tra=1;
		else next_send_cmd_tra=0;

		if ( sbit_wccm | cnt0 & send_cmd_wccm | cnt1 & send_cmd_wccm | cnt2 & 
			send_cmd_wccm | cnt3 & send_cmd_wccm | cnt4 & send_cmd_wccm | cnt5 & 
			send_cmd_wccm ) next_send_cmd_wccm=1;
		else next_send_cmd_wccm=0;

		if ( sbit_wcps | cnt0 & send_cmd_wcps | cnt1 & send_cmd_wcps | cnt2 & 
			send_cmd_wcps | cnt3 & send_cmd_wcps | cnt4 & send_cmd_wcps | cnt5 & 
			send_cmd_wcps ) next_send_cmd_wcps=1;
		else next_send_cmd_wcps=0;

		if ( sbit_wcr | cnt0 & send_cmd_wcr | cnt1 & send_cmd_wcr | cnt2 & 
			send_cmd_wcr | cnt3 & send_cmd_wcr | cnt4 & send_cmd_wcr | cnt5 & 
			send_cmd_wcr ) next_send_cmd_wcr=1;
		else next_send_cmd_wcr=0;

		if ( sbit_rege | cnt0 & send_code_rege | cnt1 & send_code_rege | cnt2 & 
			send_code_rege | cnt3 & send_code_rege | cnt4 & send_code_rege | cnt5 & 
			send_code_rege ) next_send_code_rege=1;
		else next_send_code_rege=0;

		if ( cnt0 & send_data1_wccm | cnt1 & send_data1_wccm | cnt2 & 
			send_data1_wccm | cnt3 & send_data1_wccm | cnt4 & send_data1_wccm | cnt5 & 
			send_data1_wccm | ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & ~cnt5 & 
			send_data_wccm ) next_send_data1_wccm=1;
		else next_send_data1_wccm=0;

		if ( ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & ~cnt5 & send_data1_wccm | cnt0
			 & send_data2_wccm | cnt1 & send_data2_wccm | cnt2 & send_data2_wccm | cnt3 &
			 send_data2_wccm | cnt4 & send_data2_wccm | cnt5 & send_data2_wccm ) 
			next_send_data2_wccm=1;
		else next_send_data2_wccm=0;

		if ( ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & ~cnt5 & send_data2_wccm | cnt0
			 & send_data3_wccm | cnt1 & send_data3_wccm | cnt2 & send_data3_wccm | cnt3 &
			 send_data3_wccm | cnt4 & send_data3_wccm | cnt5 & send_data3_wccm ) 
			next_send_data3_wccm=1;
		else next_send_data3_wccm=0;

		if ( ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & ~cnt5 & send_data3_wccm | cnt0
			 & send_data4_wccm | cnt1 & send_data4_wccm | cnt2 & send_data4_wccm | cnt3 &
			 send_data4_wccm | cnt4 & send_data4_wccm | cnt5 & send_data4_wccm ) 
			next_send_data4_wccm=1;
		else next_send_data4_wccm=0;

		if ( ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & ~cnt5 & send_data4_wccm | cnt0
			 & send_data5_wccm | cnt1 & send_data5_wccm | cnt2 & send_data5_wccm | cnt3 &
			 send_data5_wccm | cnt4 & send_data5_wccm | cnt5 & send_data5_wccm ) 
			next_send_data5_wccm=1;
		else next_send_data5_wccm=0;

		if ( ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & ~cnt5 & send_data5_wccm | cnt0
			 & send_data6_wccm | cnt1 & send_data6_wccm | cnt2 & send_data6_wccm | cnt3 &
			 send_data6_wccm | cnt4 & send_data6_wccm | cnt5 & send_data6_wccm ) 
			next_send_data6_wccm=1;
		else next_send_data6_wccm=0;

		if ( ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & ~cnt5 & send_data6_wccm | cnt0
			 & send_data7_wccm | cnt1 & send_data7_wccm | cnt2 & send_data7_wccm | cnt3 &
			 send_data7_wccm | cnt4 & send_data7_wccm | cnt5 & send_data7_wccm ) 
			next_send_data7_wccm=1;
		else next_send_data7_wccm=0;

		if ( ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & ~cnt5 & send_cmd_tra | cnt0 & 
			send_data_tra | cnt1 & send_data_tra | cnt2 & send_data_tra | cnt3 & 
			send_data_tra | cnt4 & send_data_tra | cnt5 & send_data_tra ) 
			next_send_data_tra=1;
		else next_send_data_tra=0;

		if ( ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & ~cnt5 & send_addr_wccm | cnt0 
			& send_data_wccm | cnt1 & send_data_wccm | cnt2 & send_data_wccm | cnt3 & 
			send_data_wccm | cnt4 & send_data_wccm | cnt5 & send_data_wccm ) 
			next_send_data_wccm=1;
		else next_send_data_wccm=0;

		if ( ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & ~cnt5 & send_addr_wcps | cnt0 
			& send_data_wcps | cnt1 & send_data_wcps | cnt2 & send_data_wcps | cnt3 & 
			send_data_wcps | cnt4 & send_data_wcps | cnt5 & send_data_wcps ) 
			next_send_data_wcps=1;
		else next_send_data_wcps=0;

		if ( ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & ~cnt5 & send_addr_wcr | cnt0 &
			 send_data_wcr | cnt1 & send_data_wcr | cnt2 & send_data_wcr | cnt3 & 
			send_data_wcr | cnt4 & send_data_wcr | cnt5 & send_data_wcr ) 
			next_send_data_wcr=1;
		else next_send_data_wcr=0;

		if ( ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & ~cnt5 & send_add_rege | 
			~atom_data & wait_sbit_rege ) next_wait_sbit_rege=1;
		else next_wait_sbit_rege=0;

		if ( ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & ~cnt5 & send_cmd_reve | 
			~atom_data & wait_sbit_reve ) next_wait_sbit_reve=1;
		else next_wait_sbit_reve=0;

		if ( ~data_reg0 & ~data_reg1 & ~data_reg2 & ~data_reg3 & ~data_reg4 & 
			~data_reg5 & ~data_reg6 & ~data_reg7 & ~data_reg8 & ~data_reg9 & ~data_reg10 
			& ~data_reg11 & ~data_reg12 & ~data_reg13 & ~data_reg14 & ~data_reg15 & 
			~data_reg16 & ~data_reg17 & ~data_reg18 & ~data_reg19 & ~data_reg20 & 
			~data_reg21 & ~data_reg22 & ~data_reg23 & ~data_reg24 & ~data_reg25 & 
			~data_reg26 & ~data_reg27 & ~data_reg28 & ~data_reg29 & ~data_reg30 & 
			~data_reg31 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_event | 
			~data_reg0 & ~data_reg1 & ~data_reg2 & ~data_reg3 & ~data_reg4 & ~data_reg5 &
			 ~data_reg6 & ~data_reg7 & ~data_reg8 & ~data_reg9 & ~data_reg10 & 
			~data_reg11 & ~data_reg12 & ~data_reg13 & ~data_reg14 & ~data_reg15 & 
			~data_reg16 & ~data_reg17 & ~data_reg18 & ~data_reg19 & ~data_reg20 & 
			~data_reg21 & ~data_reg22 & ~data_reg23 & ~data_reg24 & ~data_reg25 & 
			~data_reg26 & ~data_reg27 & ~data_reg28 & ~data_reg29 & ~data_reg30 & 
			~data_reg31 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_registers |
			 rescmd_sync | ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & ~cnt5 & send_addr_mr |
			 ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & ~cnt5 & send_cmd_clr | ~cnt0 & ~cnt1
			 & ~cnt2 & ~cnt3 & ~cnt4 & ~cnt5 & send_cmd_cstr | ~cnt0 & ~cnt1 & ~cnt2 & 
			~cnt3 & ~cnt4 & ~cnt5 & send_data7_wccm | ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & 
			~cnt4 & ~cnt5 & send_data_tra | ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & ~cnt5
			 & send_data_wcps | ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & ~cnt5 & 
			send_data_wcr | ~cmd_code3 & cmd_code4 & wait_sequence | ~cmd_code4 & 
			cmd_code3 & wait_sequence | ~cmd_code2 & ~cmd_code1 & ~cmd_code0 & 
			wait_sequence | ~cmd_code4 & cmd_code2 & cmd_code1 & wait_sequence | 
			~cmd_code3 & cmd_code2 & cmd_code1 & wait_sequence | ~cmd_code2 & ~cmd_code1 
			& cmd_code4 & wait_sequence | ~cmd_code2 & ~cmd_code1 & cmd_code3 & 
			wait_sequence | cmd_code0 & cmd_code1 & cmd_code2 & wait_sequence ) 
			next_wait_sequence=1;
		else next_wait_sequence=0;

		if ( cmd_code0 & ~cmd_code1 & ~cmd_code2 & ~cmd_code3 & ~cmd_code4 & 
			wait_sequence ) next_zbit_clr=1;
		else next_zbit_clr=0;

		if ( cmd_code0 & ~cmd_code1 & cmd_code2 & ~cmd_code3 & ~cmd_code4 & 
			wait_sequence ) next_zbit_cstr=1;
		else next_zbit_cstr=0;

		if ( ~cmd_code0 & cmd_code1 & cmd_code2 & cmd_code3 & cmd_code4 & 
			wait_sequence ) next_zbit_mr=1;
		else next_zbit_mr=0;

		if ( cmd_code0 & ~cmd_code1 & cmd_code2 & cmd_code3 & cmd_code4 & 
			wait_sequence ) next_zbit_rege=1;
		else next_zbit_rege=0;

		if ( ~cmd_code0 & ~cmd_code1 & cmd_code2 & ~cmd_code3 & ~cmd_code4 & 
			wait_sequence ) next_zbit_reve=1;
		else next_zbit_reve=0;

		if ( ~cmd_code0 & cmd_code1 & ~cmd_code2 & ~cmd_code3 & ~cmd_code4 & 
			wait_sequence ) next_zbit_sync=1;
		else next_zbit_sync=0;

		if ( cmd_code0 & cmd_code1 & ~cmd_code2 & ~cmd_code3 & ~cmd_code4 & 
			wait_sequence ) next_zbit_tra=1;
		else next_zbit_tra=0;

		if ( ~cmd_code0 & ~cmd_code1 & cmd_code2 & cmd_code3 & cmd_code4 & 
			wait_sequence ) next_zbit_wccm=1;
		else next_zbit_wccm=0;

		if ( ~cmd_code0 & cmd_code1 & ~cmd_code2 & cmd_code3 & cmd_code4 & 
			wait_sequence ) next_zbit_wcps=1;
		else next_zbit_wcps=0;

		if ( cmd_code0 & cmd_code1 & ~cmd_code2 & cmd_code3 & cmd_code4 & 
			wait_sequence ) next_zbit_wcr=1;
		else next_zbit_wcr=0;


		if ( BP_cmd & read_event | BP_cmd & read_registers | ~temp_cmd1 & 
			~temp_cmd0 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & ~cnt5 & send_cmd_sync | 
			BP_cmd & rescmd_sync | zbit_clr | zbit_cstr | zbit_mr | zbit_rege | zbit_reve
			 | ~temp_cmd1 & ~temp_cmd0 & zbit_sync | zbit_tra | zbit_wccm | zbit_wcps | 
			zbit_wcr | ~shift_reg4 & ~shift_reg3 & ~shift_reg2 & ~shift_reg1 & 
			~shift_reg0 & cnt5 & send_add_rege | ~shift_reg4 & ~shift_reg3 & ~shift_reg2 
			& ~shift_reg1 & ~shift_reg0 & cnt4 & send_add_rege | ~shift_reg4 & 
			~shift_reg3 & ~shift_reg2 & ~shift_reg1 & ~shift_reg0 & cnt3 & send_add_rege 
			| ~shift_reg4 & ~shift_reg3 & ~shift_reg2 & ~shift_reg1 & ~shift_reg0 & cnt2 
			& send_add_rege | ~shift_reg4 & ~shift_reg3 & ~shift_reg2 & ~shift_reg1 & 
			~shift_reg0 & cnt1 & send_add_rege | ~shift_reg4 & ~shift_reg3 & ~shift_reg2 
			& ~shift_reg1 & ~shift_reg0 & cnt0 & send_add_rege | BP_cmd & ~cnt0 & ~cnt1 &
			 ~cnt2 & ~cnt3 & ~cnt4 & ~cnt5 & send_add_rege | ~shift_reg4 & ~shift_reg3 & 
			~shift_reg2 & ~shift_reg1 & ~shift_reg0 & cnt5 & send_addr_mr | ~shift_reg4 &
			 ~shift_reg3 & ~shift_reg2 & ~shift_reg1 & ~shift_reg0 & cnt4 & send_addr_mr 
			| ~shift_reg4 & ~shift_reg3 & ~shift_reg2 & ~shift_reg1 & ~shift_reg0 & cnt3 
			& send_addr_mr | ~shift_reg4 & ~shift_reg3 & ~shift_reg2 & ~shift_reg1 & 
			~shift_reg0 & cnt2 & send_addr_mr | ~shift_reg4 & ~shift_reg3 & ~shift_reg2 &
			 ~shift_reg1 & ~shift_reg0 & cnt1 & send_addr_mr | ~shift_reg4 & ~shift_reg3 
			& ~shift_reg2 & ~shift_reg1 & ~shift_reg0 & cnt0 & send_addr_mr | BP_cmd & 
			~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & ~cnt5 & send_addr_mr | ~shift_reg4 & 
			~shift_reg3 & ~shift_reg2 & ~shift_reg1 & ~shift_reg0 & cnt5 & send_addr_wccm
			 | ~shift_reg4 & ~shift_reg3 & ~shift_reg2 & ~shift_reg1 & ~shift_reg0 & cnt4
			 & send_addr_wccm | ~shift_reg4 & ~shift_reg3 & ~shift_reg2 & ~shift_reg1 & 
			~shift_reg0 & cnt3 & send_addr_wccm | ~shift_reg4 & ~shift_reg3 & ~shift_reg2
			 & ~shift_reg1 & ~shift_reg0 & cnt2 & send_addr_wccm | ~shift_reg4 & 
			~shift_reg3 & ~shift_reg2 & ~shift_reg1 & ~shift_reg0 & cnt1 & send_addr_wccm
			 | ~shift_reg4 & ~shift_reg3 & ~shift_reg2 & ~shift_reg1 & ~shift_reg0 & cnt0
			 & send_addr_wccm | ~shift_reg4 & ~shift_reg3 & ~shift_reg2 & ~shift_reg1 & 
			~shift_reg0 & cnt5 & send_addr_wcr | ~shift_reg4 & ~shift_reg3 & ~shift_reg2 
			& ~shift_reg1 & ~shift_reg0 & cnt4 & send_addr_wcr | ~shift_reg4 & 
			~shift_reg3 & ~shift_reg2 & ~shift_reg1 & ~shift_reg0 & cnt3 & send_addr_wcr 
			| ~shift_reg4 & ~shift_reg3 & ~shift_reg2 & ~shift_reg1 & ~shift_reg0 & cnt2 
			& send_addr_wcr | ~shift_reg4 & ~shift_reg3 & ~shift_reg2 & ~shift_reg1 & 
			~shift_reg0 & cnt1 & send_addr_wcr | ~shift_reg4 & ~shift_reg3 & ~shift_reg2 
			& ~shift_reg1 & ~shift_reg0 & cnt0 & send_addr_wcr | ~shift_reg4 & 
			~shift_reg3 & ~shift_reg2 & ~shift_reg1 & ~shift_reg0 & sbit_clr | 
			~shift_reg4 & ~shift_reg3 & ~shift_reg2 & ~shift_reg1 & ~shift_reg0 & cnt5 & 
			send_cmd_clr | ~shift_reg4 & ~shift_reg3 & ~shift_reg2 & ~shift_reg1 & 
			~shift_reg0 & cnt4 & send_cmd_clr | ~shift_reg4 & ~shift_reg3 & ~shift_reg2 &
			 ~shift_reg1 & ~shift_reg0 & cnt3 & send_cmd_clr | ~shift_reg4 & ~shift_reg3 
			& ~shift_reg2 & ~shift_reg1 & ~shift_reg0 & cnt2 & send_cmd_clr | ~shift_reg4
			 & ~shift_reg3 & ~shift_reg2 & ~shift_reg1 & ~shift_reg0 & cnt1 & 
			send_cmd_clr | ~shift_reg4 & ~shift_reg3 & ~shift_reg2 & ~shift_reg1 & 
			~shift_reg0 & cnt0 & send_cmd_clr | BP_cmd & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & 
			~cnt4 & ~cnt5 & send_cmd_clr | ~shift_reg4 & ~shift_reg3 & ~shift_reg2 & 
			~shift_reg1 & ~shift_reg0 & sbit_cstr | ~shift_reg4 & ~shift_reg3 & 
			~shift_reg2 & ~shift_reg1 & ~shift_reg0 & cnt5 & send_cmd_cstr | ~shift_reg4 
			& ~shift_reg3 & ~shift_reg2 & ~shift_reg1 & ~shift_reg0 & cnt4 & 
			send_cmd_cstr | ~shift_reg4 & ~shift_reg3 & ~shift_reg2 & ~shift_reg1 & 
			~shift_reg0 & cnt3 & send_cmd_cstr | ~shift_reg4 & ~shift_reg3 & ~shift_reg2 
			& ~shift_reg1 & ~shift_reg0 & cnt2 & send_cmd_cstr | ~shift_reg4 & 
			~shift_reg3 & ~shift_reg2 & ~shift_reg1 & ~shift_reg0 & cnt1 & send_cmd_cstr 
			| ~shift_reg4 & ~shift_reg3 & ~shift_reg2 & ~shift_reg1 & ~shift_reg0 & cnt0 
			& send_cmd_cstr | BP_cmd & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & ~cnt5 & 
			send_cmd_cstr | ~shift_reg4 & ~shift_reg3 & ~shift_reg2 & ~shift_reg1 & 
			~shift_reg0 & sbit_mr | ~shift_reg4 & ~shift_reg3 & ~shift_reg2 & ~shift_reg1
			 & ~shift_reg0 & send_cmd_mr | ~shift_reg4 & ~shift_reg3 & ~shift_reg2 & 
			~shift_reg1 & ~shift_reg0 & sbit_reve | ~shift_reg4 & ~shift_reg3 & 
			~shift_reg2 & ~shift_reg1 & ~shift_reg0 & cnt5 & send_cmd_reve | ~shift_reg4 
			& ~shift_reg3 & ~shift_reg2 & ~shift_reg1 & ~shift_reg0 & cnt4 & 
			send_cmd_reve | ~shift_reg4 & ~shift_reg3 & ~shift_reg2 & ~shift_reg1 & 
			~shift_reg0 & cnt3 & send_cmd_reve | ~shift_reg4 & ~shift_reg3 & ~shift_reg2 
			& ~shift_reg1 & ~shift_reg0 & cnt2 & send_cmd_reve | ~shift_reg4 & 
			~shift_reg3 & ~shift_reg2 & ~shift_reg1 & ~shift_reg0 & cnt1 & send_cmd_reve 
			| ~shift_reg4 & ~shift_reg3 & ~shift_reg2 & ~shift_reg1 & ~shift_reg0 & cnt0 
			& send_cmd_reve | BP_cmd & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & ~cnt5 & 
			send_cmd_reve | ~shift_reg4 & ~shift_reg3 & ~shift_reg2 & ~shift_reg1 & 
			~shift_reg0 & sbit_sync | ~shift_reg4 & ~shift_reg3 & ~shift_reg2 & 
			~shift_reg1 & ~shift_reg0 & cnt5 & send_cmd_sync | ~shift_reg4 & ~shift_reg3 
			& ~shift_reg2 & ~shift_reg1 & ~shift_reg0 & cnt4 & send_cmd_sync | 
			~shift_reg4 & ~shift_reg3 & ~shift_reg2 & ~shift_reg1 & ~shift_reg0 & cnt3 & 
			send_cmd_sync | ~shift_reg4 & ~shift_reg3 & ~shift_reg2 & ~shift_reg1 & 
			~shift_reg0 & cnt2 & send_cmd_sync | ~shift_reg4 & ~shift_reg3 & ~shift_reg2 
			& ~shift_reg1 & ~shift_reg0 & cnt1 & send_cmd_sync | ~shift_reg4 & 
			~shift_reg3 & ~shift_reg2 & ~shift_reg1 & ~shift_reg0 & cnt0 & send_cmd_sync 
			| ~shift_reg4 & ~shift_reg3 & ~shift_reg2 & ~shift_reg1 & ~shift_reg0 & 
			sbit_tra | ~shift_reg4 & ~shift_reg3 & ~shift_reg2 & ~shift_reg1 & 
			~shift_reg0 & cnt5 & send_cmd_tra | ~shift_reg4 & ~shift_reg3 & ~shift_reg2 &
			 ~shift_reg1 & ~shift_reg0 & cnt4 & send_cmd_tra | ~shift_reg4 & ~shift_reg3 
			& ~shift_reg2 & ~shift_reg1 & ~shift_reg0 & cnt3 & send_cmd_tra | ~shift_reg4
			 & ~shift_reg3 & ~shift_reg2 & ~shift_reg1 & ~shift_reg0 & cnt2 & 
			send_cmd_tra | ~shift_reg4 & ~shift_reg3 & ~shift_reg2 & ~shift_reg1 & 
			~shift_reg0 & cnt1 & send_cmd_tra | ~shift_reg4 & ~shift_reg3 & ~shift_reg2 &
			 ~shift_reg1 & ~shift_reg0 & cnt0 & send_cmd_tra | ~shift_reg4 & ~shift_reg3 
			& ~shift_reg2 & ~shift_reg1 & ~shift_reg0 & sbit_wccm | ~shift_reg4 & 
			~shift_reg3 & ~shift_reg2 & ~shift_reg1 & ~shift_reg0 & send_cmd_wccm | 
			~shift_reg4 & ~shift_reg3 & ~shift_reg2 & ~shift_reg1 & ~shift_reg0 & 
			sbit_wcps | ~shift_reg4 & ~shift_reg3 & ~shift_reg2 & ~shift_reg1 & 
			~shift_reg0 & cnt5 & send_cmd_wcps | ~shift_reg4 & ~shift_reg3 & ~shift_reg2 
			& ~shift_reg1 & ~shift_reg0 & cnt4 & send_cmd_wcps | ~shift_reg4 & 
			~shift_reg3 & ~shift_reg2 & ~shift_reg1 & ~shift_reg0 & cnt3 & send_cmd_wcps 
			| ~shift_reg4 & ~shift_reg3 & ~shift_reg2 & ~shift_reg1 & ~shift_reg0 & cnt2 
			& send_cmd_wcps | ~shift_reg4 & ~shift_reg3 & ~shift_reg2 & ~shift_reg1 & 
			~shift_reg0 & cnt1 & send_cmd_wcps | ~shift_reg4 & ~shift_reg3 & ~shift_reg2 
			& ~shift_reg1 & ~shift_reg0 & cnt0 & send_cmd_wcps | ~shift_reg4 & 
			~shift_reg3 & ~shift_reg2 & ~shift_reg1 & ~shift_reg0 & sbit_wcr | 
			~shift_reg4 & ~shift_reg3 & ~shift_reg2 & ~shift_reg1 & ~shift_reg0 & 
			send_cmd_wcr | ~shift_reg4 & ~shift_reg3 & ~shift_reg2 & ~shift_reg1 & 
			~shift_reg0 & sbit_rege | ~shift_reg4 & ~shift_reg3 & ~shift_reg2 & 
			~shift_reg1 & ~shift_reg0 & send_code_rege | ~data_reg31 & ~data_reg30 & 
			~data_reg29 & ~data_reg28 & ~data_reg27 & ~data_reg26 & ~data_reg25 & 
			~data_reg24 & ~data_reg23 & ~data_reg22 & ~data_reg21 & ~data_reg20 & 
			~data_reg19 & ~data_reg18 & ~data_reg17 & ~data_reg16 & ~data_reg15 & 
			~data_reg14 & ~data_reg13 & ~data_reg12 & ~data_reg11 & ~data_reg10 & 
			~data_reg9 & ~data_reg8 & ~data_reg7 & ~data_reg6 & ~data_reg5 & ~data_reg4 &
			 ~data_reg3 & ~data_reg2 & ~data_reg1 & ~data_reg0 & send_data1_wccm | 
			~data_reg31 & ~data_reg30 & ~data_reg29 & ~data_reg28 & ~data_reg27 & 
			~data_reg26 & ~data_reg25 & ~data_reg24 & ~data_reg23 & ~data_reg22 & 
			~data_reg21 & ~data_reg20 & ~data_reg19 & ~data_reg18 & ~data_reg17 & 
			~data_reg16 & ~data_reg15 & ~data_reg14 & ~data_reg13 & ~data_reg12 & 
			~data_reg11 & ~data_reg10 & ~data_reg9 & ~data_reg8 & ~data_reg7 & ~data_reg6
			 & ~data_reg5 & ~data_reg4 & ~data_reg3 & ~data_reg2 & ~data_reg1 & 
			~data_reg0 & send_data2_wccm | ~data_reg31 & ~data_reg30 & ~data_reg29 & 
			~data_reg28 & ~data_reg27 & ~data_reg26 & ~data_reg25 & ~data_reg24 & 
			~data_reg23 & ~data_reg22 & ~data_reg21 & ~data_reg20 & ~data_reg19 & 
			~data_reg18 & ~data_reg17 & ~data_reg16 & ~data_reg15 & ~data_reg14 & 
			~data_reg13 & ~data_reg12 & ~data_reg11 & ~data_reg10 & ~data_reg9 & 
			~data_reg8 & ~data_reg7 & ~data_reg6 & ~data_reg5 & ~data_reg4 & ~data_reg3 &
			 ~data_reg2 & ~data_reg1 & ~data_reg0 & send_data3_wccm | ~data_reg31 & 
			~data_reg30 & ~data_reg29 & ~data_reg28 & ~data_reg27 & ~data_reg26 & 
			~data_reg25 & ~data_reg24 & ~data_reg23 & ~data_reg22 & ~data_reg21 & 
			~data_reg20 & ~data_reg19 & ~data_reg18 & ~data_reg17 & ~data_reg16 & 
			~data_reg15 & ~data_reg14 & ~data_reg13 & ~data_reg12 & ~data_reg11 & 
			~data_reg10 & ~data_reg9 & ~data_reg8 & ~data_reg7 & ~data_reg6 & ~data_reg5 
			& ~data_reg4 & ~data_reg3 & ~data_reg2 & ~data_reg1 & ~data_reg0 & 
			send_data4_wccm | ~data_reg31 & ~data_reg30 & ~data_reg29 & ~data_reg28 & 
			~data_reg27 & ~data_reg26 & ~data_reg25 & ~data_reg24 & ~data_reg23 & 
			~data_reg22 & ~data_reg21 & ~data_reg20 & ~data_reg19 & ~data_reg18 & 
			~data_reg17 & ~data_reg16 & ~data_reg15 & ~data_reg14 & ~data_reg13 & 
			~data_reg12 & ~data_reg11 & ~data_reg10 & ~data_reg9 & ~data_reg8 & 
			~data_reg7 & ~data_reg6 & ~data_reg5 & ~data_reg4 & ~data_reg3 & ~data_reg2 &
			 ~data_reg1 & ~data_reg0 & send_data5_wccm | ~data_reg31 & ~data_reg30 & 
			~data_reg29 & ~data_reg28 & ~data_reg27 & ~data_reg26 & ~data_reg25 & 
			~data_reg24 & ~data_reg23 & ~data_reg22 & ~data_reg21 & ~data_reg20 & 
			~data_reg19 & ~data_reg18 & ~data_reg17 & ~data_reg16 & ~data_reg15 & 
			~data_reg14 & ~data_reg13 & ~data_reg12 & ~data_reg11 & ~data_reg10 & 
			~data_reg9 & ~data_reg8 & ~data_reg7 & ~data_reg6 & ~data_reg5 & ~data_reg4 &
			 ~data_reg3 & ~data_reg2 & ~data_reg1 & ~data_reg0 & send_data6_wccm | 
			~data_reg31 & ~data_reg30 & ~data_reg29 & ~data_reg28 & ~data_reg27 & 
			~data_reg26 & ~data_reg25 & ~data_reg24 & ~data_reg23 & ~data_reg22 & 
			~data_reg21 & ~data_reg20 & ~data_reg19 & ~data_reg18 & ~data_reg17 & 
			~data_reg16 & ~data_reg15 & ~data_reg14 & ~data_reg13 & ~data_reg12 & 
			~data_reg11 & ~data_reg10 & ~data_reg9 & ~data_reg8 & ~data_reg7 & ~data_reg6
			 & ~data_reg5 & ~data_reg4 & ~data_reg3 & ~data_reg2 & ~data_reg1 & 
			~data_reg0 & cnt5 & send_data7_wccm | ~data_reg31 & ~data_reg30 & ~data_reg29
			 & ~data_reg28 & ~data_reg27 & ~data_reg26 & ~data_reg25 & ~data_reg24 & 
			~data_reg23 & ~data_reg22 & ~data_reg21 & ~data_reg20 & ~data_reg19 & 
			~data_reg18 & ~data_reg17 & ~data_reg16 & ~data_reg15 & ~data_reg14 & 
			~data_reg13 & ~data_reg12 & ~data_reg11 & ~data_reg10 & ~data_reg9 & 
			~data_reg8 & ~data_reg7 & ~data_reg6 & ~data_reg5 & ~data_reg4 & ~data_reg3 &
			 ~data_reg2 & ~data_reg1 & ~data_reg0 & cnt4 & send_data7_wccm | ~data_reg31 
			& ~data_reg30 & ~data_reg29 & ~data_reg28 & ~data_reg27 & ~data_reg26 & 
			~data_reg25 & ~data_reg24 & ~data_reg23 & ~data_reg22 & ~data_reg21 & 
			~data_reg20 & ~data_reg19 & ~data_reg18 & ~data_reg17 & ~data_reg16 & 
			~data_reg15 & ~data_reg14 & ~data_reg13 & ~data_reg12 & ~data_reg11 & 
			~data_reg10 & ~data_reg9 & ~data_reg8 & ~data_reg7 & ~data_reg6 & ~data_reg5 
			& ~data_reg4 & ~data_reg3 & ~data_reg2 & ~data_reg1 & ~data_reg0 & cnt3 & 
			send_data7_wccm | ~data_reg31 & ~data_reg30 & ~data_reg29 & ~data_reg28 & 
			~data_reg27 & ~data_reg26 & ~data_reg25 & ~data_reg24 & ~data_reg23 & 
			~data_reg22 & ~data_reg21 & ~data_reg20 & ~data_reg19 & ~data_reg18 & 
			~data_reg17 & ~data_reg16 & ~data_reg15 & ~data_reg14 & ~data_reg13 & 
			~data_reg12 & ~data_reg11 & ~data_reg10 & ~data_reg9 & ~data_reg8 & 
			~data_reg7 & ~data_reg6 & ~data_reg5 & ~data_reg4 & ~data_reg3 & ~data_reg2 &
			 ~data_reg1 & ~data_reg0 & cnt2 & send_data7_wccm | ~data_reg31 & ~data_reg30
			 & ~data_reg29 & ~data_reg28 & ~data_reg27 & ~data_reg26 & ~data_reg25 & 
			~data_reg24 & ~data_reg23 & ~data_reg22 & ~data_reg21 & ~data_reg20 & 
			~data_reg19 & ~data_reg18 & ~data_reg17 & ~data_reg16 & ~data_reg15 & 
			~data_reg14 & ~data_reg13 & ~data_reg12 & ~data_reg11 & ~data_reg10 & 
			~data_reg9 & ~data_reg8 & ~data_reg7 & ~data_reg6 & ~data_reg5 & ~data_reg4 &
			 ~data_reg3 & ~data_reg2 & ~data_reg1 & ~data_reg0 & cnt1 & send_data7_wccm |
			 ~data_reg31 & ~data_reg30 & ~data_reg29 & ~data_reg28 & ~data_reg27 & 
			~data_reg26 & ~data_reg25 & ~data_reg24 & ~data_reg23 & ~data_reg22 & 
			~data_reg21 & ~data_reg20 & ~data_reg19 & ~data_reg18 & ~data_reg17 & 
			~data_reg16 & ~data_reg15 & ~data_reg14 & ~data_reg13 & ~data_reg12 & 
			~data_reg11 & ~data_reg10 & ~data_reg9 & ~data_reg8 & ~data_reg7 & ~data_reg6
			 & ~data_reg5 & ~data_reg4 & ~data_reg3 & ~data_reg2 & ~data_reg1 & 
			~data_reg0 & cnt0 & send_data7_wccm | BP_cmd & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 
			& ~cnt4 & ~cnt5 & send_data7_wccm | ~data_reg31 & ~data_reg30 & ~data_reg29 &
			 ~data_reg28 & ~data_reg27 & ~data_reg26 & ~data_reg25 & ~data_reg24 & 
			~data_reg23 & ~data_reg22 & ~data_reg21 & ~data_reg20 & ~data_reg19 & 
			~data_reg18 & ~data_reg17 & ~data_reg16 & ~data_reg15 & ~data_reg14 & 
			~data_reg13 & ~data_reg12 & ~data_reg11 & ~data_reg10 & ~data_reg9 & 
			~data_reg8 & ~data_reg7 & ~data_reg6 & ~data_reg5 & ~data_reg4 & ~data_reg3 &
			 ~data_reg2 & ~data_reg1 & ~data_reg0 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4
			 & ~cnt5 & send_cmd_tra | ~data_reg31 & ~data_reg30 & ~data_reg29 & 
			~data_reg28 & ~data_reg27 & ~data_reg26 & ~data_reg25 & ~data_reg24 & 
			~data_reg23 & ~data_reg22 & ~data_reg21 & ~data_reg20 & ~data_reg19 & 
			~data_reg18 & ~data_reg17 & ~data_reg16 & ~data_reg15 & ~data_reg14 & 
			~data_reg13 & ~data_reg12 & ~data_reg11 & ~data_reg10 & ~data_reg9 & 
			~data_reg8 & ~data_reg7 & ~data_reg6 & ~data_reg5 & ~data_reg4 & ~data_reg3 &
			 ~data_reg2 & ~data_reg1 & ~data_reg0 & cnt5 & send_data_tra | ~data_reg31 & 
			~data_reg30 & ~data_reg29 & ~data_reg28 & ~data_reg27 & ~data_reg26 & 
			~data_reg25 & ~data_reg24 & ~data_reg23 & ~data_reg22 & ~data_reg21 & 
			~data_reg20 & ~data_reg19 & ~data_reg18 & ~data_reg17 & ~data_reg16 & 
			~data_reg15 & ~data_reg14 & ~data_reg13 & ~data_reg12 & ~data_reg11 & 
			~data_reg10 & ~data_reg9 & ~data_reg8 & ~data_reg7 & ~data_reg6 & ~data_reg5 
			& ~data_reg4 & ~data_reg3 & ~data_reg2 & ~data_reg1 & ~data_reg0 & cnt4 & 
			send_data_tra | ~data_reg31 & ~data_reg30 & ~data_reg29 & ~data_reg28 & 
			~data_reg27 & ~data_reg26 & ~data_reg25 & ~data_reg24 & ~data_reg23 & 
			~data_reg22 & ~data_reg21 & ~data_reg20 & ~data_reg19 & ~data_reg18 & 
			~data_reg17 & ~data_reg16 & ~data_reg15 & ~data_reg14 & ~data_reg13 & 
			~data_reg12 & ~data_reg11 & ~data_reg10 & ~data_reg9 & ~data_reg8 & 
			~data_reg7 & ~data_reg6 & ~data_reg5 & ~data_reg4 & ~data_reg3 & ~data_reg2 &
			 ~data_reg1 & ~data_reg0 & cnt3 & send_data_tra | ~data_reg31 & ~data_reg30 &
			 ~data_reg29 & ~data_reg28 & ~data_reg27 & ~data_reg26 & ~data_reg25 & 
			~data_reg24 & ~data_reg23 & ~data_reg22 & ~data_reg21 & ~data_reg20 & 
			~data_reg19 & ~data_reg18 & ~data_reg17 & ~data_reg16 & ~data_reg15 & 
			~data_reg14 & ~data_reg13 & ~data_reg12 & ~data_reg11 & ~data_reg10 & 
			~data_reg9 & ~data_reg8 & ~data_reg7 & ~data_reg6 & ~data_reg5 & ~data_reg4 &
			 ~data_reg3 & ~data_reg2 & ~data_reg1 & ~data_reg0 & cnt2 & send_data_tra | 
			~data_reg31 & ~data_reg30 & ~data_reg29 & ~data_reg28 & ~data_reg27 & 
			~data_reg26 & ~data_reg25 & ~data_reg24 & ~data_reg23 & ~data_reg22 & 
			~data_reg21 & ~data_reg20 & ~data_reg19 & ~data_reg18 & ~data_reg17 & 
			~data_reg16 & ~data_reg15 & ~data_reg14 & ~data_reg13 & ~data_reg12 & 
			~data_reg11 & ~data_reg10 & ~data_reg9 & ~data_reg8 & ~data_reg7 & ~data_reg6
			 & ~data_reg5 & ~data_reg4 & ~data_reg3 & ~data_reg2 & ~data_reg1 & 
			~data_reg0 & cnt1 & send_data_tra | ~data_reg31 & ~data_reg30 & ~data_reg29 &
			 ~data_reg28 & ~data_reg27 & ~data_reg26 & ~data_reg25 & ~data_reg24 & 
			~data_reg23 & ~data_reg22 & ~data_reg21 & ~data_reg20 & ~data_reg19 & 
			~data_reg18 & ~data_reg17 & ~data_reg16 & ~data_reg15 & ~data_reg14 & 
			~data_reg13 & ~data_reg12 & ~data_reg11 & ~data_reg10 & ~data_reg9 & 
			~data_reg8 & ~data_reg7 & ~data_reg6 & ~data_reg5 & ~data_reg4 & ~data_reg3 &
			 ~data_reg2 & ~data_reg1 & ~data_reg0 & cnt0 & send_data_tra | BP_cmd & ~cnt0
			 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & ~cnt5 & send_data_tra | ~data_reg31 & 
			~data_reg30 & ~data_reg29 & ~data_reg28 & ~data_reg27 & ~data_reg26 & 
			~data_reg25 & ~data_reg24 & ~data_reg23 & ~data_reg22 & ~data_reg21 & 
			~data_reg20 & ~data_reg19 & ~data_reg18 & ~data_reg17 & ~data_reg16 & 
			~data_reg15 & ~data_reg14 & ~data_reg13 & ~data_reg12 & ~data_reg11 & 
			~data_reg10 & ~data_reg9 & ~data_reg8 & ~data_reg7 & ~data_reg6 & ~data_reg5 
			& ~data_reg4 & ~data_reg3 & ~data_reg2 & ~data_reg1 & ~data_reg0 & ~cnt0 & 
			~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & ~cnt5 & send_addr_wccm | ~data_reg31 & 
			~data_reg30 & ~data_reg29 & ~data_reg28 & ~data_reg27 & ~data_reg26 & 
			~data_reg25 & ~data_reg24 & ~data_reg23 & ~data_reg22 & ~data_reg21 & 
			~data_reg20 & ~data_reg19 & ~data_reg18 & ~data_reg17 & ~data_reg16 & 
			~data_reg15 & ~data_reg14 & ~data_reg13 & ~data_reg12 & ~data_reg11 & 
			~data_reg10 & ~data_reg9 & ~data_reg8 & ~data_reg7 & ~data_reg6 & ~data_reg5 
			& ~data_reg4 & ~data_reg3 & ~data_reg2 & ~data_reg1 & ~data_reg0 & 
			send_data_wccm | ~data_reg31 & ~data_reg30 & ~data_reg29 & ~data_reg28 & 
			~data_reg27 & ~data_reg26 & ~data_reg25 & ~data_reg24 & ~data_reg23 & 
			~data_reg22 & ~data_reg21 & ~data_reg20 & ~data_reg19 & ~data_reg18 & 
			~data_reg17 & ~data_reg16 & ~data_reg15 & ~data_reg14 & ~data_reg13 & 
			~data_reg12 & ~data_reg11 & ~data_reg10 & ~data_reg9 & ~data_reg8 & 
			~data_reg7 & ~data_reg6 & ~data_reg5 & ~data_reg4 & ~data_reg3 & ~data_reg2 &
			 ~data_reg1 & ~data_reg0 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & ~cnt5 & 
			send_addr_wcps | ~data_reg31 & ~data_reg30 & ~data_reg29 & ~data_reg28 & 
			~data_reg27 & ~data_reg26 & ~data_reg25 & ~data_reg24 & ~data_reg23 & 
			~data_reg22 & ~data_reg21 & ~data_reg20 & ~data_reg19 & ~data_reg18 & 
			~data_reg17 & ~data_reg16 & ~data_reg15 & ~data_reg14 & ~data_reg13 & 
			~data_reg12 & ~data_reg11 & ~data_reg10 & ~data_reg9 & ~data_reg8 & 
			~data_reg7 & ~data_reg6 & ~data_reg5 & ~data_reg4 & ~data_reg3 & ~data_reg2 &
			 ~data_reg1 & ~data_reg0 & cnt5 & send_data_wcps | ~data_reg31 & ~data_reg30 
			& ~data_reg29 & ~data_reg28 & ~data_reg27 & ~data_reg26 & ~data_reg25 & 
			~data_reg24 & ~data_reg23 & ~data_reg22 & ~data_reg21 & ~data_reg20 & 
			~data_reg19 & ~data_reg18 & ~data_reg17 & ~data_reg16 & ~data_reg15 & 
			~data_reg14 & ~data_reg13 & ~data_reg12 & ~data_reg11 & ~data_reg10 & 
			~data_reg9 & ~data_reg8 & ~data_reg7 & ~data_reg6 & ~data_reg5 & ~data_reg4 &
			 ~data_reg3 & ~data_reg2 & ~data_reg1 & ~data_reg0 & cnt4 & send_data_wcps | 
			~data_reg31 & ~data_reg30 & ~data_reg29 & ~data_reg28 & ~data_reg27 & 
			~data_reg26 & ~data_reg25 & ~data_reg24 & ~data_reg23 & ~data_reg22 & 
			~data_reg21 & ~data_reg20 & ~data_reg19 & ~data_reg18 & ~data_reg17 & 
			~data_reg16 & ~data_reg15 & ~data_reg14 & ~data_reg13 & ~data_reg12 & 
			~data_reg11 & ~data_reg10 & ~data_reg9 & ~data_reg8 & ~data_reg7 & ~data_reg6
			 & ~data_reg5 & ~data_reg4 & ~data_reg3 & ~data_reg2 & ~data_reg1 & 
			~data_reg0 & cnt3 & send_data_wcps | ~data_reg31 & ~data_reg30 & ~data_reg29 
			& ~data_reg28 & ~data_reg27 & ~data_reg26 & ~data_reg25 & ~data_reg24 & 
			~data_reg23 & ~data_reg22 & ~data_reg21 & ~data_reg20 & ~data_reg19 & 
			~data_reg18 & ~data_reg17 & ~data_reg16 & ~data_reg15 & ~data_reg14 & 
			~data_reg13 & ~data_reg12 & ~data_reg11 & ~data_reg10 & ~data_reg9 & 
			~data_reg8 & ~data_reg7 & ~data_reg6 & ~data_reg5 & ~data_reg4 & ~data_reg3 &
			 ~data_reg2 & ~data_reg1 & ~data_reg0 & cnt2 & send_data_wcps | ~data_reg31 &
			 ~data_reg30 & ~data_reg29 & ~data_reg28 & ~data_reg27 & ~data_reg26 & 
			~data_reg25 & ~data_reg24 & ~data_reg23 & ~data_reg22 & ~data_reg21 & 
			~data_reg20 & ~data_reg19 & ~data_reg18 & ~data_reg17 & ~data_reg16 & 
			~data_reg15 & ~data_reg14 & ~data_reg13 & ~data_reg12 & ~data_reg11 & 
			~data_reg10 & ~data_reg9 & ~data_reg8 & ~data_reg7 & ~data_reg6 & ~data_reg5 
			& ~data_reg4 & ~data_reg3 & ~data_reg2 & ~data_reg1 & ~data_reg0 & cnt1 & 
			send_data_wcps | ~data_reg31 & ~data_reg30 & ~data_reg29 & ~data_reg28 & 
			~data_reg27 & ~data_reg26 & ~data_reg25 & ~data_reg24 & ~data_reg23 & 
			~data_reg22 & ~data_reg21 & ~data_reg20 & ~data_reg19 & ~data_reg18 & 
			~data_reg17 & ~data_reg16 & ~data_reg15 & ~data_reg14 & ~data_reg13 & 
			~data_reg12 & ~data_reg11 & ~data_reg10 & ~data_reg9 & ~data_reg8 & 
			~data_reg7 & ~data_reg6 & ~data_reg5 & ~data_reg4 & ~data_reg3 & ~data_reg2 &
			 ~data_reg1 & ~data_reg0 & cnt0 & send_data_wcps | BP_cmd & ~cnt0 & ~cnt1 & 
			~cnt2 & ~cnt3 & ~cnt4 & ~cnt5 & send_data_wcps | ~data_reg31 & ~data_reg30 & 
			~data_reg29 & ~data_reg28 & ~data_reg27 & ~data_reg26 & ~data_reg25 & 
			~data_reg24 & ~data_reg23 & ~data_reg22 & ~data_reg21 & ~data_reg20 & 
			~data_reg19 & ~data_reg18 & ~data_reg17 & ~data_reg16 & ~data_reg15 & 
			~data_reg14 & ~data_reg13 & ~data_reg12 & ~data_reg11 & ~data_reg10 & 
			~data_reg9 & ~data_reg8 & ~data_reg7 & ~data_reg6 & ~data_reg5 & ~data_reg4 &
			 ~data_reg3 & ~data_reg2 & ~data_reg1 & ~data_reg0 & ~cnt0 & ~cnt1 & ~cnt2 & 
			~cnt3 & ~cnt4 & ~cnt5 & send_addr_wcr | ~data_reg31 & ~data_reg30 & 
			~data_reg29 & ~data_reg28 & ~data_reg27 & ~data_reg26 & ~data_reg25 & 
			~data_reg24 & ~data_reg23 & ~data_reg22 & ~data_reg21 & ~data_reg20 & 
			~data_reg19 & ~data_reg18 & ~data_reg17 & ~data_reg16 & ~data_reg15 & 
			~data_reg14 & ~data_reg13 & ~data_reg12 & ~data_reg11 & ~data_reg10 & 
			~data_reg9 & ~data_reg8 & ~data_reg7 & ~data_reg6 & ~data_reg5 & ~data_reg4 &
			 ~data_reg3 & ~data_reg2 & ~data_reg1 & ~data_reg0 & cnt5 & send_data_wcr | 
			~data_reg31 & ~data_reg30 & ~data_reg29 & ~data_reg28 & ~data_reg27 & 
			~data_reg26 & ~data_reg25 & ~data_reg24 & ~data_reg23 & ~data_reg22 & 
			~data_reg21 & ~data_reg20 & ~data_reg19 & ~data_reg18 & ~data_reg17 & 
			~data_reg16 & ~data_reg15 & ~data_reg14 & ~data_reg13 & ~data_reg12 & 
			~data_reg11 & ~data_reg10 & ~data_reg9 & ~data_reg8 & ~data_reg7 & ~data_reg6
			 & ~data_reg5 & ~data_reg4 & ~data_reg3 & ~data_reg2 & ~data_reg1 & 
			~data_reg0 & cnt4 & send_data_wcr | ~data_reg31 & ~data_reg30 & ~data_reg29 &
			 ~data_reg28 & ~data_reg27 & ~data_reg26 & ~data_reg25 & ~data_reg24 & 
			~data_reg23 & ~data_reg22 & ~data_reg21 & ~data_reg20 & ~data_reg19 & 
			~data_reg18 & ~data_reg17 & ~data_reg16 & ~data_reg15 & ~data_reg14 & 
			~data_reg13 & ~data_reg12 & ~data_reg11 & ~data_reg10 & ~data_reg9 & 
			~data_reg8 & ~data_reg7 & ~data_reg6 & ~data_reg5 & ~data_reg4 & ~data_reg3 &
			 ~data_reg2 & ~data_reg1 & ~data_reg0 & cnt3 & send_data_wcr | ~data_reg31 & 
			~data_reg30 & ~data_reg29 & ~data_reg28 & ~data_reg27 & ~data_reg26 & 
			~data_reg25 & ~data_reg24 & ~data_reg23 & ~data_reg22 & ~data_reg21 & 
			~data_reg20 & ~data_reg19 & ~data_reg18 & ~data_reg17 & ~data_reg16 & 
			~data_reg15 & ~data_reg14 & ~data_reg13 & ~data_reg12 & ~data_reg11 & 
			~data_reg10 & ~data_reg9 & ~data_reg8 & ~data_reg7 & ~data_reg6 & ~data_reg5 
			& ~data_reg4 & ~data_reg3 & ~data_reg2 & ~data_reg1 & ~data_reg0 & cnt2 & 
			send_data_wcr | ~data_reg31 & ~data_reg30 & ~data_reg29 & ~data_reg28 & 
			~data_reg27 & ~data_reg26 & ~data_reg25 & ~data_reg24 & ~data_reg23 & 
			~data_reg22 & ~data_reg21 & ~data_reg20 & ~data_reg19 & ~data_reg18 & 
			~data_reg17 & ~data_reg16 & ~data_reg15 & ~data_reg14 & ~data_reg13 & 
			~data_reg12 & ~data_reg11 & ~data_reg10 & ~data_reg9 & ~data_reg8 & 
			~data_reg7 & ~data_reg6 & ~data_reg5 & ~data_reg4 & ~data_reg3 & ~data_reg2 &
			 ~data_reg1 & ~data_reg0 & cnt1 & send_data_wcr | ~data_reg31 & ~data_reg30 &
			 ~data_reg29 & ~data_reg28 & ~data_reg27 & ~data_reg26 & ~data_reg25 & 
			~data_reg24 & ~data_reg23 & ~data_reg22 & ~data_reg21 & ~data_reg20 & 
			~data_reg19 & ~data_reg18 & ~data_reg17 & ~data_reg16 & ~data_reg15 & 
			~data_reg14 & ~data_reg13 & ~data_reg12 & ~data_reg11 & ~data_reg10 & 
			~data_reg9 & ~data_reg8 & ~data_reg7 & ~data_reg6 & ~data_reg5 & ~data_reg4 &
			 ~data_reg3 & ~data_reg2 & ~data_reg1 & ~data_reg0 & cnt0 & send_data_wcr | 
			BP_cmd & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & ~cnt5 & send_data_wcr | 
			BP_cmd & wait_sbit_rege | BP_cmd & wait_sbit_reve | BP_cmd & cmd_code0 & 
			cmd_code1 & cmd_code2 & wait_sequence | BP_cmd & ~cmd_code2 & ~cmd_code1 & 
			cmd_code3 & wait_sequence | BP_cmd & ~cmd_code2 & ~cmd_code1 & cmd_code4 & 
			wait_sequence | BP_cmd & ~cmd_code3 & cmd_code2 & cmd_code1 & wait_sequence |
			 BP_cmd & ~cmd_code4 & cmd_code2 & cmd_code1 & wait_sequence | BP_cmd & 
			~cmd_code2 & ~cmd_code1 & ~cmd_code0 & wait_sequence | BP_cmd & ~cmd_code4 & 
			cmd_code3 & wait_sequence | BP_cmd & ~cmd_code3 & cmd_code4 & wait_sequence |
			 ~temp_cmd1 & ~temp_cmd0 & ~cmd_code0 & cmd_code1 & ~cmd_code2 & ~cmd_code3 &
			 ~cmd_code4 & wait_sequence ) next_BP_cmd=1;
		else next_BP_cmd=0;

		BP_data_reg_out= ( {32{read_event}}  & ( {32{cnt0}} | {32{cnt1}} | {32{cnt2
			}} | {32{cnt3}} | {32{cnt4}} | {32{~cnt5}}  ) & ( {BP_data_reg_out31,
			BP_data_reg_out30,BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,
			BP_data_reg_out26,BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,
			BP_data_reg_out22,BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,
			BP_data_reg_out18,BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,
			BP_data_reg_out14,BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,
			BP_data_reg_out10,BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,
			BP_data_reg_out6,BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,
			BP_data_reg_out2,BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{read_event}}
			  & ( {32{data_reg31}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3
			}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg30}} & {32{~cnt0}} & {32{~cnt1}} 
			& {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg29}} & 
			{32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{
			cnt5}} | {32{data_reg28}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~
			cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg27}} & {32{~cnt0}} & {32{~
			cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{
			data_reg26}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~
			cnt4}} & {32{cnt5}} | {32{data_reg25}} & {32{~cnt0}} & {32{~cnt1}} & {32{~
			cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg24}} & {32{~
			cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} |
			 {32{data_reg23}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & 
			{32{~cnt4}} & {32{cnt5}} | {32{data_reg22}} & {32{~cnt0}} & {32{~cnt1}} & 
			{32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg21}} & 
			{32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{
			cnt5}} | {32{data_reg20}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~
			cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg19}} & {32{~cnt0}} & {32{~
			cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{
			data_reg18}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~
			cnt4}} & {32{cnt5}} | {32{data_reg17}} & {32{~cnt0}} & {32{~cnt1}} & {32{~
			cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg16}} & {32{~
			cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} |
			 {32{data_reg15}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & 
			{32{~cnt4}} & {32{cnt5}} | {32{data_reg14}} & {32{~cnt0}} & {32{~cnt1}} & 
			{32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg13}} & 
			{32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{
			cnt5}} | {32{data_reg12}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~
			cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg11}} & {32{~cnt0}} & {32{~
			cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{
			data_reg10}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~
			cnt4}} & {32{cnt5}} | {32{data_reg9}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2
			}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg8}} & {32{~cnt0}} &
			 {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{
			data_reg7}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~
			cnt4}} & {32{cnt5}} | {32{data_reg6}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2
			}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg5}} & {32{~cnt0}} &
			 {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{
			data_reg4}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~
			cnt4}} & {32{cnt5}} | {32{data_reg3}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2
			}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg2}} & {32{~cnt0}} &
			 {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{
			data_reg1}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~
			cnt4}} & {32{cnt5}} | {32{data_reg0}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2
			}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}}  ) & ( {data_reg31,data_reg30,
			data_reg29,data_reg28,data_reg27,data_reg26,data_reg25,data_reg24,data_reg23,
			data_reg22,data_reg21,data_reg20,data_reg19,data_reg18,data_reg17,data_reg16,
			data_reg15,data_reg14,data_reg13,data_reg12,data_reg11,data_reg10,data_reg9,
			data_reg8,data_reg7,data_reg6,data_reg5,data_reg4,data_reg3,data_reg2,
			data_reg1,data_reg0}  ) ) | ( {32{read_event}}  & ( {32{~data_reg0}} & {32{~
			data_reg1}} & {32{~data_reg2}} & {32{~data_reg3}} & {32{~data_reg4}} & {32{~
			data_reg5}} & {32{~data_reg6}} & {32{~data_reg7}} & {32{~data_reg8}} & {32{~
			data_reg9}} & {32{~data_reg10}} & {32{~data_reg11}} & {32{~data_reg12}} & 
			{32{~data_reg13}} & {32{~data_reg14}} & {32{~data_reg15}} & {32{~data_reg16}}
			 & {32{~data_reg17}} & {32{~data_reg18}} & {32{~data_reg19}} & {32{~
			data_reg20}} & {32{~data_reg21}} & {32{~data_reg22}} & {32{~data_reg23}} & 
			{32{~data_reg24}} & {32{~data_reg25}} & {32{~data_reg26}} & {32{~data_reg27}}
			 & {32{~data_reg28}} & {32{~data_reg29}} & {32{~data_reg30}} & {32{~
			data_reg31}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~
			cnt4}} & {32{cnt5}}  ) & ( {BP_data_reg_out31,BP_data_reg_out30,
			BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,
			BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,
			BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,
			BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,
			BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,
			BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,
			BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,
			BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{read_registers}}  & ( {32{
			cnt0}} | {32{cnt1}} | {32{cnt2}} | {32{cnt3}} | {32{cnt4}} | {32{~cnt5}}  ) &
			 ( {BP_data_reg_out31,BP_data_reg_out30,BP_data_reg_out29,BP_data_reg_out28,
			BP_data_reg_out27,BP_data_reg_out26,BP_data_reg_out25,BP_data_reg_out24,
			BP_data_reg_out23,BP_data_reg_out22,BP_data_reg_out21,BP_data_reg_out20,
			BP_data_reg_out19,BP_data_reg_out18,BP_data_reg_out17,BP_data_reg_out16,
			BP_data_reg_out15,BP_data_reg_out14,BP_data_reg_out13,BP_data_reg_out12,
			BP_data_reg_out11,BP_data_reg_out10,BP_data_reg_out9,BP_data_reg_out8,
			BP_data_reg_out7,BP_data_reg_out6,BP_data_reg_out5,BP_data_reg_out4,
			BP_data_reg_out3,BP_data_reg_out2,BP_data_reg_out1,BP_data_reg_out0}  ) ) | (
			 {32{read_registers}}  & ( {32{data_reg31}} & {32{~cnt0}} & {32{~cnt1}} & 
			{32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg30}} & 
			{32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{
			cnt5}} | {32{data_reg29}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~
			cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg28}} & {32{~cnt0}} & {32{~
			cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{
			data_reg27}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~
			cnt4}} & {32{cnt5}} | {32{data_reg26}} & {32{~cnt0}} & {32{~cnt1}} & {32{~
			cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg25}} & {32{~
			cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} |
			 {32{data_reg24}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & 
			{32{~cnt4}} & {32{cnt5}} | {32{data_reg23}} & {32{~cnt0}} & {32{~cnt1}} & 
			{32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg22}} & 
			{32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{
			cnt5}} | {32{data_reg21}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~
			cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg20}} & {32{~cnt0}} & {32{~
			cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{
			data_reg19}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~
			cnt4}} & {32{cnt5}} | {32{data_reg18}} & {32{~cnt0}} & {32{~cnt1}} & {32{~
			cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg17}} & {32{~
			cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} |
			 {32{data_reg16}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & 
			{32{~cnt4}} & {32{cnt5}} | {32{data_reg15}} & {32{~cnt0}} & {32{~cnt1}} & 
			{32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg14}} & 
			{32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{
			cnt5}} | {32{data_reg13}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~
			cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg12}} & {32{~cnt0}} & {32{~
			cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{
			data_reg11}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~
			cnt4}} & {32{cnt5}} | {32{data_reg10}} & {32{~cnt0}} & {32{~cnt1}} & {32{~
			cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg9}} & {32{~cnt0
			}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | 
			{32{data_reg8}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & 
			{32{~cnt4}} & {32{cnt5}} | {32{data_reg7}} & {32{~cnt0}} & {32{~cnt1}} & {32{
			~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg6}} & {32{~
			cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} |
			 {32{data_reg5}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & 
			{32{~cnt4}} & {32{cnt5}} | {32{data_reg4}} & {32{~cnt0}} & {32{~cnt1}} & {32{
			~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg3}} & {32{~
			cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} |
			 {32{data_reg2}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & 
			{32{~cnt4}} & {32{cnt5}} | {32{data_reg1}} & {32{~cnt0}} & {32{~cnt1}} & {32{
			~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg0}} & {32{~
			cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}}  
			) & ( {BP_data_reg_out31,BP_data_reg_out30,BP_data_reg_out29,
			BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,BP_data_reg_out25,
			BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,BP_data_reg_out21,
			BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,BP_data_reg_out17,
			BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,BP_data_reg_out13,
			BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,BP_data_reg_out9,
			BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,BP_data_reg_out5,
			BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,BP_data_reg_out1,
			BP_data_reg_out0}  ) ) | ( {32{read_registers}}  & ( {32{~data_reg0}} & {32{~
			data_reg1}} & {32{~data_reg2}} & {32{~data_reg3}} & {32{~data_reg4}} & {32{~
			data_reg5}} & {32{~data_reg6}} & {32{~data_reg7}} & {32{~data_reg8}} & {32{~
			data_reg9}} & {32{~data_reg10}} & {32{~data_reg11}} & {32{~data_reg12}} & 
			{32{~data_reg13}} & {32{~data_reg14}} & {32{~data_reg15}} & {32{~data_reg16}}
			 & {32{~data_reg17}} & {32{~data_reg18}} & {32{~data_reg19}} & {32{~
			data_reg20}} & {32{~data_reg21}} & {32{~data_reg22}} & {32{~data_reg23}} & 
			{32{~data_reg24}} & {32{~data_reg25}} & {32{~data_reg26}} & {32{~data_reg27}}
			 & {32{~data_reg28}} & {32{~data_reg29}} & {32{~data_reg30}} & {32{~
			data_reg31}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~
			cnt4}} & {32{cnt5}}  ) & ( {BP_data_reg_out31,BP_data_reg_out30,
			BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,
			BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,
			BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,
			BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,
			BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,
			BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,
			BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,
			BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{rescmd_sync}}  & ( 
			32'hffffffff ) & ( {BP_data_reg_out31,BP_data_reg_out30,BP_data_reg_out29,
			BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,BP_data_reg_out25,
			BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,BP_data_reg_out21,
			BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,BP_data_reg_out17,
			BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,BP_data_reg_out13,
			BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,BP_data_reg_out9,
			BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,BP_data_reg_out5,
			BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,BP_data_reg_out1,
			BP_data_reg_out0}  ) ) | ( {32{sbit_clr}}  & ( 32'hffffffff ) & ( {
			BP_data_reg_out31,BP_data_reg_out30,BP_data_reg_out29,BP_data_reg_out28,
			BP_data_reg_out27,BP_data_reg_out26,BP_data_reg_out25,BP_data_reg_out24,
			BP_data_reg_out23,BP_data_reg_out22,BP_data_reg_out21,BP_data_reg_out20,
			BP_data_reg_out19,BP_data_reg_out18,BP_data_reg_out17,BP_data_reg_out16,
			BP_data_reg_out15,BP_data_reg_out14,BP_data_reg_out13,BP_data_reg_out12,
			BP_data_reg_out11,BP_data_reg_out10,BP_data_reg_out9,BP_data_reg_out8,
			BP_data_reg_out7,BP_data_reg_out6,BP_data_reg_out5,BP_data_reg_out4,
			BP_data_reg_out3,BP_data_reg_out2,BP_data_reg_out1,BP_data_reg_out0}  ) ) | (
			 {32{sbit_cstr}}  & ( 32'hffffffff ) & ( {BP_data_reg_out31,BP_data_reg_out30
			,BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,
			BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,
			BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,
			BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,
			BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,
			BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,
			BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,
			BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{sbit_mr}}  & ( 32'hffffffff )
			 & ( {BP_data_reg_out31,BP_data_reg_out30,BP_data_reg_out29,BP_data_reg_out28
			,BP_data_reg_out27,BP_data_reg_out26,BP_data_reg_out25,BP_data_reg_out24,
			BP_data_reg_out23,BP_data_reg_out22,BP_data_reg_out21,BP_data_reg_out20,
			BP_data_reg_out19,BP_data_reg_out18,BP_data_reg_out17,BP_data_reg_out16,
			BP_data_reg_out15,BP_data_reg_out14,BP_data_reg_out13,BP_data_reg_out12,
			BP_data_reg_out11,BP_data_reg_out10,BP_data_reg_out9,BP_data_reg_out8,
			BP_data_reg_out7,BP_data_reg_out6,BP_data_reg_out5,BP_data_reg_out4,
			BP_data_reg_out3,BP_data_reg_out2,BP_data_reg_out1,BP_data_reg_out0}  ) ) | (
			 {32{sbit_rege}}  & ( 32'hffffffff ) & ( {BP_data_reg_out31,BP_data_reg_out30
			,BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,
			BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,
			BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,
			BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,
			BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,
			BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,
			BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,
			BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{sbit_reve}}  & ( 32'hffffffff
			 ) & ( {BP_data_reg_out31,BP_data_reg_out30,BP_data_reg_out29,
			BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,BP_data_reg_out25,
			BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,BP_data_reg_out21,
			BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,BP_data_reg_out17,
			BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,BP_data_reg_out13,
			BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,BP_data_reg_out9,
			BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,BP_data_reg_out5,
			BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,BP_data_reg_out1,
			BP_data_reg_out0}  ) ) | ( {32{sbit_sync}}  & ( 32'hffffffff ) & ( {
			BP_data_reg_out31,BP_data_reg_out30,BP_data_reg_out29,BP_data_reg_out28,
			BP_data_reg_out27,BP_data_reg_out26,BP_data_reg_out25,BP_data_reg_out24,
			BP_data_reg_out23,BP_data_reg_out22,BP_data_reg_out21,BP_data_reg_out20,
			BP_data_reg_out19,BP_data_reg_out18,BP_data_reg_out17,BP_data_reg_out16,
			BP_data_reg_out15,BP_data_reg_out14,BP_data_reg_out13,BP_data_reg_out12,
			BP_data_reg_out11,BP_data_reg_out10,BP_data_reg_out9,BP_data_reg_out8,
			BP_data_reg_out7,BP_data_reg_out6,BP_data_reg_out5,BP_data_reg_out4,
			BP_data_reg_out3,BP_data_reg_out2,BP_data_reg_out1,BP_data_reg_out0}  ) ) | (
			 {32{sbit_tra}}  & ( 32'hffffffff ) & ( {BP_data_reg_out31,BP_data_reg_out30,
			BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,
			BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,
			BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,
			BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,
			BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,
			BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,
			BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,
			BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{sbit_wccm}}  & ( 32'hffffffff
			 ) & ( {BP_data_reg_out31,BP_data_reg_out30,BP_data_reg_out29,
			BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,BP_data_reg_out25,
			BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,BP_data_reg_out21,
			BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,BP_data_reg_out17,
			BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,BP_data_reg_out13,
			BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,BP_data_reg_out9,
			BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,BP_data_reg_out5,
			BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,BP_data_reg_out1,
			BP_data_reg_out0}  ) ) | ( {32{sbit_wcps}}  & ( 32'hffffffff ) & ( {
			BP_data_reg_out31,BP_data_reg_out30,BP_data_reg_out29,BP_data_reg_out28,
			BP_data_reg_out27,BP_data_reg_out26,BP_data_reg_out25,BP_data_reg_out24,
			BP_data_reg_out23,BP_data_reg_out22,BP_data_reg_out21,BP_data_reg_out20,
			BP_data_reg_out19,BP_data_reg_out18,BP_data_reg_out17,BP_data_reg_out16,
			BP_data_reg_out15,BP_data_reg_out14,BP_data_reg_out13,BP_data_reg_out12,
			BP_data_reg_out11,BP_data_reg_out10,BP_data_reg_out9,BP_data_reg_out8,
			BP_data_reg_out7,BP_data_reg_out6,BP_data_reg_out5,BP_data_reg_out4,
			BP_data_reg_out3,BP_data_reg_out2,BP_data_reg_out1,BP_data_reg_out0}  ) ) | (
			 {32{sbit_wcr}}  & ( 32'hffffffff ) & ( {BP_data_reg_out31,BP_data_reg_out30,
			BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,
			BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,
			BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,
			BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,
			BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,
			BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,
			BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,
			BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{send_add_rege}}  & ( {32{~
			cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{~cnt5}} 
			 ) & ( {BP_data_reg_out31,BP_data_reg_out30,BP_data_reg_out29,
			BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,BP_data_reg_out25,
			BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,BP_data_reg_out21,
			BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,BP_data_reg_out17,
			BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,BP_data_reg_out13,
			BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,BP_data_reg_out9,
			BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,BP_data_reg_out5,
			BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,BP_data_reg_out1,
			BP_data_reg_out0}  ) ) | ( {32{send_add_rege}}  & ( {32{cnt5}} | {32{cnt4}} |
			 {32{cnt3}} | {32{cnt2}} | {32{cnt1}} | {32{cnt0}}  ) & ( {BP_data_reg_out31,
			BP_data_reg_out30,BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,
			BP_data_reg_out26,BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,
			BP_data_reg_out22,BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,
			BP_data_reg_out18,BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,
			BP_data_reg_out14,BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,
			BP_data_reg_out10,BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,
			BP_data_reg_out6,BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,
			BP_data_reg_out2,BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{send_addr_mr
			}}  & ( {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} &
			 {32{~cnt5}}  ) & ( {BP_data_reg_out31,BP_data_reg_out30,BP_data_reg_out29,
			BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,BP_data_reg_out25,
			BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,BP_data_reg_out21,
			BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,BP_data_reg_out17,
			BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,BP_data_reg_out13,
			BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,BP_data_reg_out9,
			BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,BP_data_reg_out5,
			BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,BP_data_reg_out1,
			BP_data_reg_out0}  ) ) | ( {32{send_addr_mr}}  & ( {32{cnt5}} | {32{cnt4}} | 
			{32{cnt3}} | {32{cnt2}} | {32{cnt1}} | {32{cnt0}}  ) & ( {BP_data_reg_out31,
			BP_data_reg_out30,BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,
			BP_data_reg_out26,BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,
			BP_data_reg_out22,BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,
			BP_data_reg_out18,BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,
			BP_data_reg_out14,BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,
			BP_data_reg_out10,BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,
			BP_data_reg_out6,BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,
			BP_data_reg_out2,BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{
			send_addr_wccm}}  & ( {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} &
			 {32{~cnt4}} & {32{~cnt5}}  ) & ( {BP_data_reg_out31,BP_data_reg_out30,
			BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,
			BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,
			BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,
			BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,
			BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,
			BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,
			BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,
			BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{send_addr_wccm}}  & ( {32{
			cnt5}} | {32{cnt4}} | {32{cnt3}} | {32{cnt2}} | {32{cnt1}} | {32{cnt0}}  ) & 
			( {BP_data_reg_out31,BP_data_reg_out30,BP_data_reg_out29,BP_data_reg_out28,
			BP_data_reg_out27,BP_data_reg_out26,BP_data_reg_out25,BP_data_reg_out24,
			BP_data_reg_out23,BP_data_reg_out22,BP_data_reg_out21,BP_data_reg_out20,
			BP_data_reg_out19,BP_data_reg_out18,BP_data_reg_out17,BP_data_reg_out16,
			BP_data_reg_out15,BP_data_reg_out14,BP_data_reg_out13,BP_data_reg_out12,
			BP_data_reg_out11,BP_data_reg_out10,BP_data_reg_out9,BP_data_reg_out8,
			BP_data_reg_out7,BP_data_reg_out6,BP_data_reg_out5,BP_data_reg_out4,
			BP_data_reg_out3,BP_data_reg_out2,BP_data_reg_out1,BP_data_reg_out0}  ) ) | (
			 {32{send_addr_wcps}}  & ( {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~
			cnt3}} & {32{~cnt4}} & {32{~cnt5}}  ) & ( {BP_data_reg_out31,
			BP_data_reg_out30,BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,
			BP_data_reg_out26,BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,
			BP_data_reg_out22,BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,
			BP_data_reg_out18,BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,
			BP_data_reg_out14,BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,
			BP_data_reg_out10,BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,
			BP_data_reg_out6,BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,
			BP_data_reg_out2,BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{
			send_addr_wcps}}  & ( {32{cnt5}} | {32{cnt4}} | {32{cnt3}} | {32{cnt2}} | 
			{32{cnt1}} | {32{cnt0}}  ) & ( {BP_data_reg_out31,BP_data_reg_out30,
			BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,
			BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,
			BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,
			BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,
			BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,
			BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,
			BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,
			BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{send_addr_wcr}}  & ( {32{~
			cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{~cnt5}} 
			 ) & ( {BP_data_reg_out31,BP_data_reg_out30,BP_data_reg_out29,
			BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,BP_data_reg_out25,
			BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,BP_data_reg_out21,
			BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,BP_data_reg_out17,
			BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,BP_data_reg_out13,
			BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,BP_data_reg_out9,
			BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,BP_data_reg_out5,
			BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,BP_data_reg_out1,
			BP_data_reg_out0}  ) ) | ( {32{send_addr_wcr}}  & ( {32{cnt5}} | {32{cnt4}} |
			 {32{cnt3}} | {32{cnt2}} | {32{cnt1}} | {32{cnt0}}  ) & ( {BP_data_reg_out31,
			BP_data_reg_out30,BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,
			BP_data_reg_out26,BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,
			BP_data_reg_out22,BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,
			BP_data_reg_out18,BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,
			BP_data_reg_out14,BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,
			BP_data_reg_out10,BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,
			BP_data_reg_out6,BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,
			BP_data_reg_out2,BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{send_cmd_clr
			}}  & ( {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} &
			 {32{~cnt5}}  ) & ( {BP_data_reg_out31,BP_data_reg_out30,BP_data_reg_out29,
			BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,BP_data_reg_out25,
			BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,BP_data_reg_out21,
			BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,BP_data_reg_out17,
			BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,BP_data_reg_out13,
			BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,BP_data_reg_out9,
			BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,BP_data_reg_out5,
			BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,BP_data_reg_out1,
			BP_data_reg_out0}  ) ) | ( {32{send_cmd_clr}}  & ( {32{cnt5}} | {32{cnt4}} | 
			{32{cnt3}} | {32{cnt2}} | {32{cnt1}} | {32{cnt0}}  ) & ( {BP_data_reg_out31,
			BP_data_reg_out30,BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,
			BP_data_reg_out26,BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,
			BP_data_reg_out22,BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,
			BP_data_reg_out18,BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,
			BP_data_reg_out14,BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,
			BP_data_reg_out10,BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,
			BP_data_reg_out6,BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,
			BP_data_reg_out2,BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{
			send_cmd_cstr}}  & ( {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & 
			{32{~cnt4}} & {32{~cnt5}}  ) & ( {BP_data_reg_out31,BP_data_reg_out30,
			BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,
			BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,
			BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,
			BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,
			BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,
			BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,
			BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,
			BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{send_cmd_cstr}}  & ( {32{cnt5
			}} | {32{cnt4}} | {32{cnt3}} | {32{cnt2}} | {32{cnt1}} | {32{cnt0}}  ) & ( {
			BP_data_reg_out31,BP_data_reg_out30,BP_data_reg_out29,BP_data_reg_out28,
			BP_data_reg_out27,BP_data_reg_out26,BP_data_reg_out25,BP_data_reg_out24,
			BP_data_reg_out23,BP_data_reg_out22,BP_data_reg_out21,BP_data_reg_out20,
			BP_data_reg_out19,BP_data_reg_out18,BP_data_reg_out17,BP_data_reg_out16,
			BP_data_reg_out15,BP_data_reg_out14,BP_data_reg_out13,BP_data_reg_out12,
			BP_data_reg_out11,BP_data_reg_out10,BP_data_reg_out9,BP_data_reg_out8,
			BP_data_reg_out7,BP_data_reg_out6,BP_data_reg_out5,BP_data_reg_out4,
			BP_data_reg_out3,BP_data_reg_out2,BP_data_reg_out1,BP_data_reg_out0}  ) ) | (
			 {32{send_cmd_mr}}  & ( {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}}
			 & {32{~cnt4}} & {32{~cnt5}}  ) & ( {BP_data_reg_out31,BP_data_reg_out30,
			BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,
			BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,
			BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,
			BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,
			BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,
			BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,
			BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,
			BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{send_cmd_mr}}  & ( {32{cnt5}}
			 | {32{cnt4}} | {32{cnt3}} | {32{cnt2}} | {32{cnt1}} | {32{cnt0}}  ) & ( {
			BP_data_reg_out31,BP_data_reg_out30,BP_data_reg_out29,BP_data_reg_out28,
			BP_data_reg_out27,BP_data_reg_out26,BP_data_reg_out25,BP_data_reg_out24,
			BP_data_reg_out23,BP_data_reg_out22,BP_data_reg_out21,BP_data_reg_out20,
			BP_data_reg_out19,BP_data_reg_out18,BP_data_reg_out17,BP_data_reg_out16,
			BP_data_reg_out15,BP_data_reg_out14,BP_data_reg_out13,BP_data_reg_out12,
			BP_data_reg_out11,BP_data_reg_out10,BP_data_reg_out9,BP_data_reg_out8,
			BP_data_reg_out7,BP_data_reg_out6,BP_data_reg_out5,BP_data_reg_out4,
			BP_data_reg_out3,BP_data_reg_out2,BP_data_reg_out1,BP_data_reg_out0}  ) ) | (
			 {32{send_cmd_reve}}  & ( {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3
			}} & {32{~cnt4}} & {32{~cnt5}}  ) & ( {BP_data_reg_out31,BP_data_reg_out30,
			BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,
			BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,
			BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,
			BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,
			BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,
			BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,
			BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,
			BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{send_cmd_reve}}  & ( {32{cnt5
			}} | {32{cnt4}} | {32{cnt3}} | {32{cnt2}} | {32{cnt1}} | {32{cnt0}}  ) & ( {
			BP_data_reg_out31,BP_data_reg_out30,BP_data_reg_out29,BP_data_reg_out28,
			BP_data_reg_out27,BP_data_reg_out26,BP_data_reg_out25,BP_data_reg_out24,
			BP_data_reg_out23,BP_data_reg_out22,BP_data_reg_out21,BP_data_reg_out20,
			BP_data_reg_out19,BP_data_reg_out18,BP_data_reg_out17,BP_data_reg_out16,
			BP_data_reg_out15,BP_data_reg_out14,BP_data_reg_out13,BP_data_reg_out12,
			BP_data_reg_out11,BP_data_reg_out10,BP_data_reg_out9,BP_data_reg_out8,
			BP_data_reg_out7,BP_data_reg_out6,BP_data_reg_out5,BP_data_reg_out4,
			BP_data_reg_out3,BP_data_reg_out2,BP_data_reg_out1,BP_data_reg_out0}  ) ) | (
			 {32{send_cmd_sync}}  & ( {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3
			}} & {32{~cnt4}} & {32{~cnt5}}  ) & ( {BP_data_reg_out31,BP_data_reg_out30,
			BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,
			BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,
			BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,
			BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,
			BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,
			BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,
			BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,
			BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{send_cmd_sync}}  & ( {32{cnt5
			}} | {32{cnt4}} | {32{cnt3}} | {32{cnt2}} | {32{cnt1}} | {32{cnt0}}  ) & ( {
			BP_data_reg_out31,BP_data_reg_out30,BP_data_reg_out29,BP_data_reg_out28,
			BP_data_reg_out27,BP_data_reg_out26,BP_data_reg_out25,BP_data_reg_out24,
			BP_data_reg_out23,BP_data_reg_out22,BP_data_reg_out21,BP_data_reg_out20,
			BP_data_reg_out19,BP_data_reg_out18,BP_data_reg_out17,BP_data_reg_out16,
			BP_data_reg_out15,BP_data_reg_out14,BP_data_reg_out13,BP_data_reg_out12,
			BP_data_reg_out11,BP_data_reg_out10,BP_data_reg_out9,BP_data_reg_out8,
			BP_data_reg_out7,BP_data_reg_out6,BP_data_reg_out5,BP_data_reg_out4,
			BP_data_reg_out3,BP_data_reg_out2,BP_data_reg_out1,BP_data_reg_out0}  ) ) | (
			 {32{send_cmd_tra}}  & ( {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3
			}} & {32{~cnt4}} & {32{~cnt5}}  ) & ( {BP_data_reg_out31,BP_data_reg_out30,
			BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,
			BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,
			BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,
			BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,
			BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,
			BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,
			BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,
			BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{send_cmd_tra}}  & ( {32{cnt5
			}} | {32{cnt4}} | {32{cnt3}} | {32{cnt2}} | {32{cnt1}} | {32{cnt0}}  ) & ( {
			BP_data_reg_out31,BP_data_reg_out30,BP_data_reg_out29,BP_data_reg_out28,
			BP_data_reg_out27,BP_data_reg_out26,BP_data_reg_out25,BP_data_reg_out24,
			BP_data_reg_out23,BP_data_reg_out22,BP_data_reg_out21,BP_data_reg_out20,
			BP_data_reg_out19,BP_data_reg_out18,BP_data_reg_out17,BP_data_reg_out16,
			BP_data_reg_out15,BP_data_reg_out14,BP_data_reg_out13,BP_data_reg_out12,
			BP_data_reg_out11,BP_data_reg_out10,BP_data_reg_out9,BP_data_reg_out8,
			BP_data_reg_out7,BP_data_reg_out6,BP_data_reg_out5,BP_data_reg_out4,
			BP_data_reg_out3,BP_data_reg_out2,BP_data_reg_out1,BP_data_reg_out0}  ) ) | (
			 {32{send_cmd_wccm}}  & ( {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3
			}} & {32{~cnt4}} & {32{~cnt5}}  ) & ( {BP_data_reg_out31,BP_data_reg_out30,
			BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,
			BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,
			BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,
			BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,
			BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,
			BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,
			BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,
			BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{send_cmd_wccm}}  & ( {32{cnt5
			}} | {32{cnt4}} | {32{cnt3}} | {32{cnt2}} | {32{cnt1}} | {32{cnt0}}  ) & ( {
			BP_data_reg_out31,BP_data_reg_out30,BP_data_reg_out29,BP_data_reg_out28,
			BP_data_reg_out27,BP_data_reg_out26,BP_data_reg_out25,BP_data_reg_out24,
			BP_data_reg_out23,BP_data_reg_out22,BP_data_reg_out21,BP_data_reg_out20,
			BP_data_reg_out19,BP_data_reg_out18,BP_data_reg_out17,BP_data_reg_out16,
			BP_data_reg_out15,BP_data_reg_out14,BP_data_reg_out13,BP_data_reg_out12,
			BP_data_reg_out11,BP_data_reg_out10,BP_data_reg_out9,BP_data_reg_out8,
			BP_data_reg_out7,BP_data_reg_out6,BP_data_reg_out5,BP_data_reg_out4,
			BP_data_reg_out3,BP_data_reg_out2,BP_data_reg_out1,BP_data_reg_out0}  ) ) | (
			 {32{send_cmd_wcps}}  & ( {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3
			}} & {32{~cnt4}} & {32{~cnt5}}  ) & ( {BP_data_reg_out31,BP_data_reg_out30,
			BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,
			BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,
			BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,
			BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,
			BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,
			BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,
			BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,
			BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{send_cmd_wcps}}  & ( {32{cnt5
			}} | {32{cnt4}} | {32{cnt3}} | {32{cnt2}} | {32{cnt1}} | {32{cnt0}}  ) & ( {
			BP_data_reg_out31,BP_data_reg_out30,BP_data_reg_out29,BP_data_reg_out28,
			BP_data_reg_out27,BP_data_reg_out26,BP_data_reg_out25,BP_data_reg_out24,
			BP_data_reg_out23,BP_data_reg_out22,BP_data_reg_out21,BP_data_reg_out20,
			BP_data_reg_out19,BP_data_reg_out18,BP_data_reg_out17,BP_data_reg_out16,
			BP_data_reg_out15,BP_data_reg_out14,BP_data_reg_out13,BP_data_reg_out12,
			BP_data_reg_out11,BP_data_reg_out10,BP_data_reg_out9,BP_data_reg_out8,
			BP_data_reg_out7,BP_data_reg_out6,BP_data_reg_out5,BP_data_reg_out4,
			BP_data_reg_out3,BP_data_reg_out2,BP_data_reg_out1,BP_data_reg_out0}  ) ) | (
			 {32{send_cmd_wcr}}  & ( {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3
			}} & {32{~cnt4}} & {32{~cnt5}}  ) & ( {BP_data_reg_out31,BP_data_reg_out30,
			BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,
			BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,
			BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,
			BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,
			BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,
			BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,
			BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,
			BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{send_cmd_wcr}}  & ( {32{cnt5
			}} | {32{cnt4}} | {32{cnt3}} | {32{cnt2}} | {32{cnt1}} | {32{cnt0}}  ) & ( {
			BP_data_reg_out31,BP_data_reg_out30,BP_data_reg_out29,BP_data_reg_out28,
			BP_data_reg_out27,BP_data_reg_out26,BP_data_reg_out25,BP_data_reg_out24,
			BP_data_reg_out23,BP_data_reg_out22,BP_data_reg_out21,BP_data_reg_out20,
			BP_data_reg_out19,BP_data_reg_out18,BP_data_reg_out17,BP_data_reg_out16,
			BP_data_reg_out15,BP_data_reg_out14,BP_data_reg_out13,BP_data_reg_out12,
			BP_data_reg_out11,BP_data_reg_out10,BP_data_reg_out9,BP_data_reg_out8,
			BP_data_reg_out7,BP_data_reg_out6,BP_data_reg_out5,BP_data_reg_out4,
			BP_data_reg_out3,BP_data_reg_out2,BP_data_reg_out1,BP_data_reg_out0}  ) ) | (
			 {32{send_code_rege}}  & ( {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~
			cnt3}} & {32{~cnt4}} & {32{~cnt5}}  ) & ( {BP_data_reg_out31,
			BP_data_reg_out30,BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,
			BP_data_reg_out26,BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,
			BP_data_reg_out22,BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,
			BP_data_reg_out18,BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,
			BP_data_reg_out14,BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,
			BP_data_reg_out10,BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,
			BP_data_reg_out6,BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,
			BP_data_reg_out2,BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{
			send_code_rege}}  & ( {32{cnt5}} | {32{cnt4}} | {32{cnt3}} | {32{cnt2}} | 
			{32{cnt1}} | {32{cnt0}}  ) & ( {BP_data_reg_out31,BP_data_reg_out30,
			BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,
			BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,
			BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,
			BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,
			BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,
			BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,
			BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,
			BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{send_data1_wccm}}  & ( {32{~
			cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{~cnt5}} 
			 ) & ( {BP_data_reg_out31,BP_data_reg_out30,BP_data_reg_out29,
			BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,BP_data_reg_out25,
			BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,BP_data_reg_out21,
			BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,BP_data_reg_out17,
			BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,BP_data_reg_out13,
			BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,BP_data_reg_out9,
			BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,BP_data_reg_out5,
			BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,BP_data_reg_out1,
			BP_data_reg_out0}  ) ) | ( {32{send_data1_wccm}}  & ( {32{cnt5}} | {32{cnt4}}
			 | {32{cnt3}} | {32{cnt2}} | {32{cnt1}} | {32{cnt0}}  ) & ( {
			BP_data_reg_out31,BP_data_reg_out30,BP_data_reg_out29,BP_data_reg_out28,
			BP_data_reg_out27,BP_data_reg_out26,BP_data_reg_out25,BP_data_reg_out24,
			BP_data_reg_out23,BP_data_reg_out22,BP_data_reg_out21,BP_data_reg_out20,
			BP_data_reg_out19,BP_data_reg_out18,BP_data_reg_out17,BP_data_reg_out16,
			BP_data_reg_out15,BP_data_reg_out14,BP_data_reg_out13,BP_data_reg_out12,
			BP_data_reg_out11,BP_data_reg_out10,BP_data_reg_out9,BP_data_reg_out8,
			BP_data_reg_out7,BP_data_reg_out6,BP_data_reg_out5,BP_data_reg_out4,
			BP_data_reg_out3,BP_data_reg_out2,BP_data_reg_out1,BP_data_reg_out0}  ) ) | (
			 {32{send_data2_wccm}}  & ( {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~
			cnt3}} & {32{~cnt4}} & {32{~cnt5}}  ) & ( {BP_data_reg_out31,
			BP_data_reg_out30,BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,
			BP_data_reg_out26,BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,
			BP_data_reg_out22,BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,
			BP_data_reg_out18,BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,
			BP_data_reg_out14,BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,
			BP_data_reg_out10,BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,
			BP_data_reg_out6,BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,
			BP_data_reg_out2,BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{
			send_data2_wccm}}  & ( {32{cnt5}} | {32{cnt4}} | {32{cnt3}} | {32{cnt2}} | 
			{32{cnt1}} | {32{cnt0}}  ) & ( {BP_data_reg_out31,BP_data_reg_out30,
			BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,
			BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,
			BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,
			BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,
			BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,
			BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,
			BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,
			BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{send_data3_wccm}}  & ( {32{~
			cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{~cnt5}} 
			 ) & ( {BP_data_reg_out31,BP_data_reg_out30,BP_data_reg_out29,
			BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,BP_data_reg_out25,
			BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,BP_data_reg_out21,
			BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,BP_data_reg_out17,
			BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,BP_data_reg_out13,
			BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,BP_data_reg_out9,
			BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,BP_data_reg_out5,
			BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,BP_data_reg_out1,
			BP_data_reg_out0}  ) ) | ( {32{send_data3_wccm}}  & ( {32{cnt5}} | {32{cnt4}}
			 | {32{cnt3}} | {32{cnt2}} | {32{cnt1}} | {32{cnt0}}  ) & ( {
			BP_data_reg_out31,BP_data_reg_out30,BP_data_reg_out29,BP_data_reg_out28,
			BP_data_reg_out27,BP_data_reg_out26,BP_data_reg_out25,BP_data_reg_out24,
			BP_data_reg_out23,BP_data_reg_out22,BP_data_reg_out21,BP_data_reg_out20,
			BP_data_reg_out19,BP_data_reg_out18,BP_data_reg_out17,BP_data_reg_out16,
			BP_data_reg_out15,BP_data_reg_out14,BP_data_reg_out13,BP_data_reg_out12,
			BP_data_reg_out11,BP_data_reg_out10,BP_data_reg_out9,BP_data_reg_out8,
			BP_data_reg_out7,BP_data_reg_out6,BP_data_reg_out5,BP_data_reg_out4,
			BP_data_reg_out3,BP_data_reg_out2,BP_data_reg_out1,BP_data_reg_out0}  ) ) | (
			 {32{send_data4_wccm}}  & ( {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~
			cnt3}} & {32{~cnt4}} & {32{~cnt5}}  ) & ( {BP_data_reg_out31,
			BP_data_reg_out30,BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,
			BP_data_reg_out26,BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,
			BP_data_reg_out22,BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,
			BP_data_reg_out18,BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,
			BP_data_reg_out14,BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,
			BP_data_reg_out10,BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,
			BP_data_reg_out6,BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,
			BP_data_reg_out2,BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{
			send_data4_wccm}}  & ( {32{cnt5}} | {32{cnt4}} | {32{cnt3}} | {32{cnt2}} | 
			{32{cnt1}} | {32{cnt0}}  ) & ( {BP_data_reg_out31,BP_data_reg_out30,
			BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,
			BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,
			BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,
			BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,
			BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,
			BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,
			BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,
			BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{send_data5_wccm}}  & ( {32{~
			cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{~cnt5}} 
			 ) & ( {BP_data_reg_out31,BP_data_reg_out30,BP_data_reg_out29,
			BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,BP_data_reg_out25,
			BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,BP_data_reg_out21,
			BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,BP_data_reg_out17,
			BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,BP_data_reg_out13,
			BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,BP_data_reg_out9,
			BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,BP_data_reg_out5,
			BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,BP_data_reg_out1,
			BP_data_reg_out0}  ) ) | ( {32{send_data5_wccm}}  & ( {32{cnt5}} | {32{cnt4}}
			 | {32{cnt3}} | {32{cnt2}} | {32{cnt1}} | {32{cnt0}}  ) & ( {
			BP_data_reg_out31,BP_data_reg_out30,BP_data_reg_out29,BP_data_reg_out28,
			BP_data_reg_out27,BP_data_reg_out26,BP_data_reg_out25,BP_data_reg_out24,
			BP_data_reg_out23,BP_data_reg_out22,BP_data_reg_out21,BP_data_reg_out20,
			BP_data_reg_out19,BP_data_reg_out18,BP_data_reg_out17,BP_data_reg_out16,
			BP_data_reg_out15,BP_data_reg_out14,BP_data_reg_out13,BP_data_reg_out12,
			BP_data_reg_out11,BP_data_reg_out10,BP_data_reg_out9,BP_data_reg_out8,
			BP_data_reg_out7,BP_data_reg_out6,BP_data_reg_out5,BP_data_reg_out4,
			BP_data_reg_out3,BP_data_reg_out2,BP_data_reg_out1,BP_data_reg_out0}  ) ) | (
			 {32{send_data6_wccm}}  & ( {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~
			cnt3}} & {32{~cnt4}} & {32{~cnt5}}  ) & ( {BP_data_reg_out31,
			BP_data_reg_out30,BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,
			BP_data_reg_out26,BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,
			BP_data_reg_out22,BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,
			BP_data_reg_out18,BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,
			BP_data_reg_out14,BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,
			BP_data_reg_out10,BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,
			BP_data_reg_out6,BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,
			BP_data_reg_out2,BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{
			send_data6_wccm}}  & ( {32{cnt5}} | {32{cnt4}} | {32{cnt3}} | {32{cnt2}} | 
			{32{cnt1}} | {32{cnt0}}  ) & ( {BP_data_reg_out31,BP_data_reg_out30,
			BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,
			BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,
			BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,
			BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,
			BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,
			BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,
			BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,
			BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{send_data7_wccm}}  & ( {32{~
			cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{~cnt5}} 
			 ) & ( {BP_data_reg_out31,BP_data_reg_out30,BP_data_reg_out29,
			BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,BP_data_reg_out25,
			BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,BP_data_reg_out21,
			BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,BP_data_reg_out17,
			BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,BP_data_reg_out13,
			BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,BP_data_reg_out9,
			BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,BP_data_reg_out5,
			BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,BP_data_reg_out1,
			BP_data_reg_out0}  ) ) | ( {32{send_data7_wccm}}  & ( {32{cnt5}} | {32{cnt4}}
			 | {32{cnt3}} | {32{cnt2}} | {32{cnt1}} | {32{cnt0}}  ) & ( {
			BP_data_reg_out31,BP_data_reg_out30,BP_data_reg_out29,BP_data_reg_out28,
			BP_data_reg_out27,BP_data_reg_out26,BP_data_reg_out25,BP_data_reg_out24,
			BP_data_reg_out23,BP_data_reg_out22,BP_data_reg_out21,BP_data_reg_out20,
			BP_data_reg_out19,BP_data_reg_out18,BP_data_reg_out17,BP_data_reg_out16,
			BP_data_reg_out15,BP_data_reg_out14,BP_data_reg_out13,BP_data_reg_out12,
			BP_data_reg_out11,BP_data_reg_out10,BP_data_reg_out9,BP_data_reg_out8,
			BP_data_reg_out7,BP_data_reg_out6,BP_data_reg_out5,BP_data_reg_out4,
			BP_data_reg_out3,BP_data_reg_out2,BP_data_reg_out1,BP_data_reg_out0}  ) ) | (
			 {32{send_data_tra}}  & ( {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3
			}} & {32{~cnt4}} & {32{~cnt5}}  ) & ( {BP_data_reg_out31,BP_data_reg_out30,
			BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,
			BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,
			BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,
			BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,
			BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,
			BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,
			BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,
			BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{send_data_tra}}  & ( {32{cnt5
			}} | {32{cnt4}} | {32{cnt3}} | {32{cnt2}} | {32{cnt1}} | {32{cnt0}}  ) & ( {
			BP_data_reg_out31,BP_data_reg_out30,BP_data_reg_out29,BP_data_reg_out28,
			BP_data_reg_out27,BP_data_reg_out26,BP_data_reg_out25,BP_data_reg_out24,
			BP_data_reg_out23,BP_data_reg_out22,BP_data_reg_out21,BP_data_reg_out20,
			BP_data_reg_out19,BP_data_reg_out18,BP_data_reg_out17,BP_data_reg_out16,
			BP_data_reg_out15,BP_data_reg_out14,BP_data_reg_out13,BP_data_reg_out12,
			BP_data_reg_out11,BP_data_reg_out10,BP_data_reg_out9,BP_data_reg_out8,
			BP_data_reg_out7,BP_data_reg_out6,BP_data_reg_out5,BP_data_reg_out4,
			BP_data_reg_out3,BP_data_reg_out2,BP_data_reg_out1,BP_data_reg_out0}  ) ) | (
			 {32{send_data_wccm}}  & ( {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~
			cnt3}} & {32{~cnt4}} & {32{~cnt5}}  ) & ( {BP_data_reg_out31,
			BP_data_reg_out30,BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,
			BP_data_reg_out26,BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,
			BP_data_reg_out22,BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,
			BP_data_reg_out18,BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,
			BP_data_reg_out14,BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,
			BP_data_reg_out10,BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,
			BP_data_reg_out6,BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,
			BP_data_reg_out2,BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{
			send_data_wccm}}  & ( {32{cnt5}} | {32{cnt4}} | {32{cnt3}} | {32{cnt2}} | 
			{32{cnt1}} | {32{cnt0}}  ) & ( {BP_data_reg_out31,BP_data_reg_out30,
			BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,
			BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,
			BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,
			BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,
			BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,
			BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,
			BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,
			BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{send_data_wcps}}  & ( {32{~
			cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{~cnt5}} 
			 ) & ( {BP_data_reg_out31,BP_data_reg_out30,BP_data_reg_out29,
			BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,BP_data_reg_out25,
			BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,BP_data_reg_out21,
			BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,BP_data_reg_out17,
			BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,BP_data_reg_out13,
			BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,BP_data_reg_out9,
			BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,BP_data_reg_out5,
			BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,BP_data_reg_out1,
			BP_data_reg_out0}  ) ) | ( {32{send_data_wcps}}  & ( {32{cnt5}} | {32{cnt4}} 
			| {32{cnt3}} | {32{cnt2}} | {32{cnt1}} | {32{cnt0}}  ) & ( {BP_data_reg_out31
			,BP_data_reg_out30,BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,
			BP_data_reg_out26,BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,
			BP_data_reg_out22,BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,
			BP_data_reg_out18,BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,
			BP_data_reg_out14,BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,
			BP_data_reg_out10,BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,
			BP_data_reg_out6,BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,
			BP_data_reg_out2,BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{
			send_data_wcr}}  & ( {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & 
			{32{~cnt4}} & {32{~cnt5}}  ) & ( {BP_data_reg_out31,BP_data_reg_out30,
			BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,
			BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,
			BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,
			BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,
			BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,
			BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,
			BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,
			BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{send_data_wcr}}  & ( {32{cnt5
			}} | {32{cnt4}} | {32{cnt3}} | {32{cnt2}} | {32{cnt1}} | {32{cnt0}}  ) & ( {
			BP_data_reg_out31,BP_data_reg_out30,BP_data_reg_out29,BP_data_reg_out28,
			BP_data_reg_out27,BP_data_reg_out26,BP_data_reg_out25,BP_data_reg_out24,
			BP_data_reg_out23,BP_data_reg_out22,BP_data_reg_out21,BP_data_reg_out20,
			BP_data_reg_out19,BP_data_reg_out18,BP_data_reg_out17,BP_data_reg_out16,
			BP_data_reg_out15,BP_data_reg_out14,BP_data_reg_out13,BP_data_reg_out12,
			BP_data_reg_out11,BP_data_reg_out10,BP_data_reg_out9,BP_data_reg_out8,
			BP_data_reg_out7,BP_data_reg_out6,BP_data_reg_out5,BP_data_reg_out4,
			BP_data_reg_out3,BP_data_reg_out2,BP_data_reg_out1,BP_data_reg_out0}  ) ) | (
			 {32{wait_sbit_rege}}  & ( {32{atom_data}}  ) & ( {BP_data_reg_out31,
			BP_data_reg_out30,BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,
			BP_data_reg_out26,BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,
			BP_data_reg_out22,BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,
			BP_data_reg_out18,BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,
			BP_data_reg_out14,BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,
			BP_data_reg_out10,BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,
			BP_data_reg_out6,BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,
			BP_data_reg_out2,BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{
			wait_sbit_rege}}  & ( {32{~atom_data}}  ) & ( {BP_data_reg_out31,
			BP_data_reg_out30,BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,
			BP_data_reg_out26,BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,
			BP_data_reg_out22,BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,
			BP_data_reg_out18,BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,
			BP_data_reg_out14,BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,
			BP_data_reg_out10,BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,
			BP_data_reg_out6,BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,
			BP_data_reg_out2,BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{
			wait_sbit_reve}}  & ( {32{atom_data}}  ) & ( {BP_data_reg_out31,
			BP_data_reg_out30,BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,
			BP_data_reg_out26,BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,
			BP_data_reg_out22,BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,
			BP_data_reg_out18,BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,
			BP_data_reg_out14,BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,
			BP_data_reg_out10,BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,
			BP_data_reg_out6,BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,
			BP_data_reg_out2,BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{
			wait_sbit_reve}}  & ( {32{~atom_data}}  ) & ( {BP_data_reg_out31,
			BP_data_reg_out30,BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,
			BP_data_reg_out26,BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,
			BP_data_reg_out22,BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,
			BP_data_reg_out18,BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,
			BP_data_reg_out14,BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,
			BP_data_reg_out10,BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,
			BP_data_reg_out6,BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,
			BP_data_reg_out2,BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{
			wait_sequence}}  & ( {32{cmd_code0}} & {32{cmd_code1}} & {32{cmd_code2}} | 
			{32{~cmd_code2}} & {32{~cmd_code1}} & {32{cmd_code3}} | {32{~cmd_code2}} & 
			{32{~cmd_code1}} & {32{cmd_code4}} | {32{~cmd_code3}} & {32{cmd_code2}} & 
			{32{cmd_code1}} | {32{~cmd_code4}} & {32{cmd_code2}} & {32{cmd_code1}} | {32{
			~cmd_code2}} & {32{~cmd_code1}} & {32{~cmd_code0}} | {32{~cmd_code4}} & {32{
			cmd_code3}} | {32{~cmd_code3}} & {32{cmd_code4}}  ) & ( {BP_data_reg_out31,
			BP_data_reg_out30,BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,
			BP_data_reg_out26,BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,
			BP_data_reg_out22,BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,
			BP_data_reg_out18,BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,
			BP_data_reg_out14,BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,
			BP_data_reg_out10,BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,
			BP_data_reg_out6,BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,
			BP_data_reg_out2,BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{
			wait_sequence}}  & ( {32{~cmd_code0}} & {32{~cmd_code1}} & {32{cmd_code2}} & 
			{32{cmd_code3}} & {32{cmd_code4}}  ) & ( {BP_data_reg_out31,BP_data_reg_out30
			,BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,
			BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,
			BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,
			BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,
			BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,
			BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,
			BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,
			BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{wait_sequence}}  & ( {32{~
			cmd_code0}} & {32{cmd_code1}} & {32{cmd_code2}} & {32{cmd_code3}} & {32{
			cmd_code4}}  ) & ( {BP_data_reg_out31,BP_data_reg_out30,BP_data_reg_out29,
			BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,BP_data_reg_out25,
			BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,BP_data_reg_out21,
			BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,BP_data_reg_out17,
			BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,BP_data_reg_out13,
			BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,BP_data_reg_out9,
			BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,BP_data_reg_out5,
			BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,BP_data_reg_out1,
			BP_data_reg_out0}  ) ) | ( {32{wait_sequence}}  & ( {32{~cmd_code0}} & {32{
			cmd_code1}} & {32{~cmd_code2}} & {32{cmd_code3}} & {32{cmd_code4}}  ) & ( {
			BP_data_reg_out31,BP_data_reg_out30,BP_data_reg_out29,BP_data_reg_out28,
			BP_data_reg_out27,BP_data_reg_out26,BP_data_reg_out25,BP_data_reg_out24,
			BP_data_reg_out23,BP_data_reg_out22,BP_data_reg_out21,BP_data_reg_out20,
			BP_data_reg_out19,BP_data_reg_out18,BP_data_reg_out17,BP_data_reg_out16,
			BP_data_reg_out15,BP_data_reg_out14,BP_data_reg_out13,BP_data_reg_out12,
			BP_data_reg_out11,BP_data_reg_out10,BP_data_reg_out9,BP_data_reg_out8,
			BP_data_reg_out7,BP_data_reg_out6,BP_data_reg_out5,BP_data_reg_out4,
			BP_data_reg_out3,BP_data_reg_out2,BP_data_reg_out1,BP_data_reg_out0}  ) ) | (
			 {32{wait_sequence}}  & ( {32{cmd_code0}} & {32{~cmd_code1}} & {32{cmd_code2
			}} & {32{~cmd_code3}} & {32{~cmd_code4}}  ) & ( {BP_data_reg_out31,
			BP_data_reg_out30,BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,
			BP_data_reg_out26,BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,
			BP_data_reg_out22,BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,
			BP_data_reg_out18,BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,
			BP_data_reg_out14,BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,
			BP_data_reg_out10,BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,
			BP_data_reg_out6,BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,
			BP_data_reg_out2,BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{
			wait_sequence}}  & ( {32{~cmd_code0}} & {32{cmd_code1}} & {32{~cmd_code2}} & 
			{32{~cmd_code3}} & {32{~cmd_code4}}  ) & ( {BP_data_reg_out31,
			BP_data_reg_out30,BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,
			BP_data_reg_out26,BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,
			BP_data_reg_out22,BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,
			BP_data_reg_out18,BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,
			BP_data_reg_out14,BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,
			BP_data_reg_out10,BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,
			BP_data_reg_out6,BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,
			BP_data_reg_out2,BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{
			wait_sequence}}  & ( {32{cmd_code0}} & {32{~cmd_code1}} & {32{~cmd_code2}} & 
			{32{~cmd_code3}} & {32{~cmd_code4}}  ) & ( {BP_data_reg_out31,
			BP_data_reg_out30,BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,
			BP_data_reg_out26,BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,
			BP_data_reg_out22,BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,
			BP_data_reg_out18,BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,
			BP_data_reg_out14,BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,
			BP_data_reg_out10,BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,
			BP_data_reg_out6,BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,
			BP_data_reg_out2,BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{
			wait_sequence}}  & ( {32{cmd_code0}} & {32{cmd_code1}} & {32{~cmd_code2}} & 
			{32{~cmd_code3}} & {32{~cmd_code4}}  ) & ( {BP_data_reg_out31,
			BP_data_reg_out30,BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,
			BP_data_reg_out26,BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,
			BP_data_reg_out22,BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,
			BP_data_reg_out18,BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,
			BP_data_reg_out14,BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,
			BP_data_reg_out10,BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,
			BP_data_reg_out6,BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,
			BP_data_reg_out2,BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{
			wait_sequence}}  & ( {32{cmd_code0}} & {32{cmd_code1}} & {32{~cmd_code2}} & 
			{32{cmd_code3}} & {32{cmd_code4}}  ) & ( {BP_data_reg_out31,BP_data_reg_out30
			,BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,
			BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,
			BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,
			BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,
			BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,
			BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,
			BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,
			BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{wait_sequence}}  & ( {32{
			cmd_code0}} & {32{~cmd_code1}} & {32{cmd_code2}} & {32{cmd_code3}} & {32{
			cmd_code4}}  ) & ( {BP_data_reg_out31,BP_data_reg_out30,BP_data_reg_out29,
			BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,BP_data_reg_out25,
			BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,BP_data_reg_out21,
			BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,BP_data_reg_out17,
			BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,BP_data_reg_out13,
			BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,BP_data_reg_out9,
			BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,BP_data_reg_out5,
			BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,BP_data_reg_out1,
			BP_data_reg_out0}  ) ) | ( {32{wait_sequence}}  & ( {32{~cmd_code0}} & {32{~
			cmd_code1}} & {32{cmd_code2}} & {32{~cmd_code3}} & {32{~cmd_code4}}  ) & ( {
			BP_data_reg_out31,BP_data_reg_out30,BP_data_reg_out29,BP_data_reg_out28,
			BP_data_reg_out27,BP_data_reg_out26,BP_data_reg_out25,BP_data_reg_out24,
			BP_data_reg_out23,BP_data_reg_out22,BP_data_reg_out21,BP_data_reg_out20,
			BP_data_reg_out19,BP_data_reg_out18,BP_data_reg_out17,BP_data_reg_out16,
			BP_data_reg_out15,BP_data_reg_out14,BP_data_reg_out13,BP_data_reg_out12,
			BP_data_reg_out11,BP_data_reg_out10,BP_data_reg_out9,BP_data_reg_out8,
			BP_data_reg_out7,BP_data_reg_out6,BP_data_reg_out5,BP_data_reg_out4,
			BP_data_reg_out3,BP_data_reg_out2,BP_data_reg_out1,BP_data_reg_out0}  ) ) | (
			 {32{zbit_clr}}  & ( 32'hffffffff ) & ( {BP_data_reg_out31,BP_data_reg_out30,
			BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,
			BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,
			BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,
			BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,
			BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,
			BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,
			BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,
			BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{zbit_cstr}}  & ( 32'hffffffff
			 ) & ( {BP_data_reg_out31,BP_data_reg_out30,BP_data_reg_out29,
			BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,BP_data_reg_out25,
			BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,BP_data_reg_out21,
			BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,BP_data_reg_out17,
			BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,BP_data_reg_out13,
			BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,BP_data_reg_out9,
			BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,BP_data_reg_out5,
			BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,BP_data_reg_out1,
			BP_data_reg_out0}  ) ) | ( {32{zbit_mr}}  & ( 32'hffffffff ) & ( {
			BP_data_reg_out31,BP_data_reg_out30,BP_data_reg_out29,BP_data_reg_out28,
			BP_data_reg_out27,BP_data_reg_out26,BP_data_reg_out25,BP_data_reg_out24,
			BP_data_reg_out23,BP_data_reg_out22,BP_data_reg_out21,BP_data_reg_out20,
			BP_data_reg_out19,BP_data_reg_out18,BP_data_reg_out17,BP_data_reg_out16,
			BP_data_reg_out15,BP_data_reg_out14,BP_data_reg_out13,BP_data_reg_out12,
			BP_data_reg_out11,BP_data_reg_out10,BP_data_reg_out9,BP_data_reg_out8,
			BP_data_reg_out7,BP_data_reg_out6,BP_data_reg_out5,BP_data_reg_out4,
			BP_data_reg_out3,BP_data_reg_out2,BP_data_reg_out1,BP_data_reg_out0}  ) ) | (
			 {32{zbit_rege}}  & ( 32'hffffffff ) & ( {BP_data_reg_out31,BP_data_reg_out30
			,BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,
			BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,
			BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,
			BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,
			BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,
			BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,
			BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,
			BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{zbit_reve}}  & ( 32'hffffffff
			 ) & ( {BP_data_reg_out31,BP_data_reg_out30,BP_data_reg_out29,
			BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,BP_data_reg_out25,
			BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,BP_data_reg_out21,
			BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,BP_data_reg_out17,
			BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,BP_data_reg_out13,
			BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,BP_data_reg_out9,
			BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,BP_data_reg_out5,
			BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,BP_data_reg_out1,
			BP_data_reg_out0}  ) ) | ( {32{zbit_sync}}  & ( 32'hffffffff ) & ( {
			BP_data_reg_out31,BP_data_reg_out30,BP_data_reg_out29,BP_data_reg_out28,
			BP_data_reg_out27,BP_data_reg_out26,BP_data_reg_out25,BP_data_reg_out24,
			BP_data_reg_out23,BP_data_reg_out22,BP_data_reg_out21,BP_data_reg_out20,
			BP_data_reg_out19,BP_data_reg_out18,BP_data_reg_out17,BP_data_reg_out16,
			BP_data_reg_out15,BP_data_reg_out14,BP_data_reg_out13,BP_data_reg_out12,
			BP_data_reg_out11,BP_data_reg_out10,BP_data_reg_out9,BP_data_reg_out8,
			BP_data_reg_out7,BP_data_reg_out6,BP_data_reg_out5,BP_data_reg_out4,
			BP_data_reg_out3,BP_data_reg_out2,BP_data_reg_out1,BP_data_reg_out0}  ) ) | (
			 {32{zbit_tra}}  & ( 32'hffffffff ) & ( {BP_data_reg_out31,BP_data_reg_out30,
			BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,
			BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,
			BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,
			BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,
			BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,
			BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,
			BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,
			BP_data_reg_out1,BP_data_reg_out0}  ) ) | ( {32{zbit_wccm}}  & ( 32'hffffffff
			 ) & ( {BP_data_reg_out31,BP_data_reg_out30,BP_data_reg_out29,
			BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,BP_data_reg_out25,
			BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,BP_data_reg_out21,
			BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,BP_data_reg_out17,
			BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,BP_data_reg_out13,
			BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,BP_data_reg_out9,
			BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,BP_data_reg_out5,
			BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,BP_data_reg_out1,
			BP_data_reg_out0}  ) ) | ( {32{zbit_wcps}}  & ( 32'hffffffff ) & ( {
			BP_data_reg_out31,BP_data_reg_out30,BP_data_reg_out29,BP_data_reg_out28,
			BP_data_reg_out27,BP_data_reg_out26,BP_data_reg_out25,BP_data_reg_out24,
			BP_data_reg_out23,BP_data_reg_out22,BP_data_reg_out21,BP_data_reg_out20,
			BP_data_reg_out19,BP_data_reg_out18,BP_data_reg_out17,BP_data_reg_out16,
			BP_data_reg_out15,BP_data_reg_out14,BP_data_reg_out13,BP_data_reg_out12,
			BP_data_reg_out11,BP_data_reg_out10,BP_data_reg_out9,BP_data_reg_out8,
			BP_data_reg_out7,BP_data_reg_out6,BP_data_reg_out5,BP_data_reg_out4,
			BP_data_reg_out3,BP_data_reg_out2,BP_data_reg_out1,BP_data_reg_out0}  ) ) | (
			 {32{zbit_wcr}}  & ( 32'hffffffff ) & ( {BP_data_reg_out31,BP_data_reg_out30,
			BP_data_reg_out29,BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,
			BP_data_reg_out25,BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,
			BP_data_reg_out21,BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,
			BP_data_reg_out17,BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,
			BP_data_reg_out13,BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,
			BP_data_reg_out9,BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,
			BP_data_reg_out5,BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,
			BP_data_reg_out1,BP_data_reg_out0}  ) );

		if ( data_reg0 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_event 
			| data_reg1 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_event | 
			data_reg2 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_event | 
			data_reg3 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_event | 
			data_reg4 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_event | 
			data_reg5 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_event | 
			data_reg6 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_event | 
			data_reg7 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_event | 
			data_reg8 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_event | 
			data_reg9 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_event | 
			data_reg10 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_event | 
			data_reg11 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_event | 
			data_reg12 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_event | 
			data_reg13 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_event | 
			data_reg14 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_event | 
			data_reg15 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_event | 
			data_reg16 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_event | 
			data_reg17 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_event | 
			data_reg18 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_event | 
			data_reg19 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_event | 
			data_reg20 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_event | 
			data_reg21 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_event | 
			data_reg22 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_event | 
			data_reg23 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_event | 
			data_reg24 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_event | 
			data_reg25 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_event | 
			data_reg26 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_event | 
			data_reg27 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_event | 
			data_reg28 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_event | 
			data_reg29 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_event | 
			data_reg30 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_event | 
			data_reg31 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_event | 
			data_reg0 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_registers | 
			data_reg1 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_registers | 
			data_reg2 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_registers | 
			data_reg3 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_registers | 
			data_reg4 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_registers | 
			data_reg5 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_registers | 
			data_reg6 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_registers | 
			data_reg7 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_registers | 
			data_reg8 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_registers | 
			data_reg9 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_registers | 
			data_reg10 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_registers | 
			data_reg11 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_registers | 
			data_reg12 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_registers | 
			data_reg13 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_registers | 
			data_reg14 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_registers | 
			data_reg15 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_registers | 
			data_reg16 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_registers | 
			data_reg17 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_registers | 
			data_reg18 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_registers | 
			data_reg19 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_registers | 
			data_reg20 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_registers | 
			data_reg21 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_registers | 
			data_reg22 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_registers | 
			data_reg23 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_registers | 
			data_reg24 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_registers | 
			data_reg25 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_registers | 
			data_reg26 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_registers | 
			data_reg27 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_registers | 
			data_reg28 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_registers | 
			data_reg29 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_registers | 
			data_reg30 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_registers | 
			data_reg31 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & cnt5 & read_registers | 
			BP_FIFO_we & rescmd_sync | BP_FIFO_we & sbit_clr | BP_FIFO_we & sbit_cstr | 
			BP_FIFO_we & sbit_mr | BP_FIFO_we & sbit_rege | BP_FIFO_we & sbit_reve | 
			BP_FIFO_we & sbit_sync | BP_FIFO_we & sbit_tra | BP_FIFO_we & sbit_wccm | 
			BP_FIFO_we & sbit_wcps | BP_FIFO_we & sbit_wcr | BP_FIFO_we & send_add_rege |
			 BP_FIFO_we & send_addr_mr | BP_FIFO_we & send_addr_wccm | BP_FIFO_we & 
			send_addr_wcps | BP_FIFO_we & send_addr_wcr | BP_FIFO_we & send_cmd_clr | 
			BP_FIFO_we & send_cmd_cstr | BP_FIFO_we & send_cmd_mr | BP_FIFO_we & 
			send_cmd_reve | BP_FIFO_we & send_cmd_sync | BP_FIFO_we & send_cmd_tra | 
			BP_FIFO_we & send_cmd_wccm | BP_FIFO_we & send_cmd_wcps | BP_FIFO_we & 
			send_cmd_wcr | BP_FIFO_we & send_code_rege | BP_FIFO_we & send_data1_wccm | 
			BP_FIFO_we & send_data2_wccm | BP_FIFO_we & send_data3_wccm | BP_FIFO_we & 
			send_data4_wccm | BP_FIFO_we & send_data5_wccm | BP_FIFO_we & send_data6_wccm
			 | BP_FIFO_we & send_data7_wccm | BP_FIFO_we & send_data_tra | BP_FIFO_we & 
			send_data_wccm | BP_FIFO_we & send_data_wcps | BP_FIFO_we & send_data_wcr | 
			BP_FIFO_we & wait_sbit_rege | BP_FIFO_we & wait_sbit_reve | BP_FIFO_we & 
			~cmd_code4 & cmd_code3 & wait_sequence | BP_FIFO_we & ~cmd_code3 & cmd_code4 
			& wait_sequence | BP_FIFO_we & ~cmd_code0 & ~cmd_code1 & cmd_code3 & 
			wait_sequence | BP_FIFO_we & cmd_code1 & cmd_code2 & wait_sequence | 
			BP_FIFO_we & ~cmd_code0 & ~cmd_code2 & cmd_code3 & wait_sequence | BP_FIFO_we
			 & ~cmd_code1 & ~cmd_code2 & wait_sequence | BP_FIFO_we & cmd_code1 & 
			~cmd_code4 & wait_sequence | BP_FIFO_we & cmd_code0 & cmd_code1 & cmd_code3 &
			 wait_sequence | BP_FIFO_we & cmd_code0 & cmd_code2 & cmd_code3 & 
			wait_sequence | BP_FIFO_we & cmd_code2 & ~cmd_code4 & wait_sequence | 
			BP_FIFO_we & zbit_clr | BP_FIFO_we & zbit_cstr | BP_FIFO_we & zbit_mr | 
			BP_FIFO_we & zbit_rege | BP_FIFO_we & zbit_reve | BP_FIFO_we & zbit_sync | 
			BP_FIFO_we & zbit_tra | BP_FIFO_we & zbit_wccm | BP_FIFO_we & zbit_wcps | 
			BP_FIFO_we & zbit_wcr ) next_BP_FIFO_we=1;
		else next_BP_FIFO_we=0;

		if ( BP_mux_out & read_event | BP_mux_out & read_registers | BP_mux_out & 
			rescmd_sync | BP_mux_out & sbit_clr | BP_mux_out & sbit_cstr | BP_mux_out & 
			sbit_mr | BP_mux_out & sbit_rege | BP_mux_out & sbit_reve | BP_mux_out & 
			sbit_sync | BP_mux_out & sbit_tra | BP_mux_out & sbit_wccm | BP_mux_out & 
			sbit_wcps | BP_mux_out & sbit_wcr | BP_mux_out & send_add_rege | BP_mux_out &
			 send_addr_mr | BP_mux_out & send_addr_wccm | BP_mux_out & send_addr_wcps | 
			BP_mux_out & send_addr_wcr | BP_mux_out & send_cmd_clr | BP_mux_out & 
			send_cmd_cstr | BP_mux_out & send_cmd_mr | BP_mux_out & send_cmd_reve | 
			BP_mux_out & send_cmd_sync | BP_mux_out & send_cmd_tra | BP_mux_out & 
			send_cmd_wccm | BP_mux_out & send_cmd_wcps | BP_mux_out & send_cmd_wcr | 
			BP_mux_out & send_code_rege | BP_mux_out & send_data1_wccm | BP_mux_out & 
			send_data2_wccm | BP_mux_out & send_data3_wccm | BP_mux_out & send_data4_wccm
			 | BP_mux_out & send_data5_wccm | BP_mux_out & send_data6_wccm | BP_mux_out &
			 send_data7_wccm | BP_mux_out & send_data_tra | BP_mux_out & send_data_wccm |
			 BP_mux_out & send_data_wcps | ~data31 & ~data30 & ~data29 & ~data28 & 
			~data27 & ~data26 & ~data25 & ~data24 & ~data23 & ~data22 & ~data21 & ~data20
			 & ~data19 & ~data18 & ~data17 & ~data16 & ~data15 & ~data14 & ~data13 & 
			~data12 & ~data11 & ~data10 & ~data9 & ~data8 & ~data7 & ~data6 & ~data5 & 
			data4 & data3 & data2 & data1 & ~data0 & ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & 
			~cnt4 & ~cnt5 & send_data_wcr | BP_mux_out & cnt5 & send_data_wcr | 
			BP_mux_out & cnt4 & send_data_wcr | BP_mux_out & cnt3 & send_data_wcr | 
			BP_mux_out & cnt2 & send_data_wcr | BP_mux_out & cnt1 & send_data_wcr | 
			BP_mux_out & cnt0 & send_data_wcr | BP_mux_out & wait_sbit_rege | BP_mux_out 
			& wait_sbit_reve | BP_mux_out & ~cmd_code4 & cmd_code3 & wait_sequence | 
			BP_mux_out & ~cmd_code3 & cmd_code4 & wait_sequence | BP_mux_out & ~cmd_code0
			 & ~cmd_code1 & cmd_code3 & wait_sequence | BP_mux_out & cmd_code1 & 
			cmd_code2 & wait_sequence | BP_mux_out & ~cmd_code0 & ~cmd_code2 & cmd_code3 
			& wait_sequence | BP_mux_out & ~cmd_code1 & ~cmd_code2 & wait_sequence | 
			BP_mux_out & cmd_code1 & ~cmd_code4 & wait_sequence | BP_mux_out & cmd_code0 
			& cmd_code1 & cmd_code3 & wait_sequence | BP_mux_out & cmd_code0 & cmd_code2 
			& cmd_code3 & wait_sequence | BP_mux_out & cmd_code2 & ~cmd_code4 & 
			wait_sequence | BP_mux_out & zbit_clr | BP_mux_out & zbit_cstr | BP_mux_out &
			 zbit_mr | BP_mux_out & zbit_rege | BP_mux_out & zbit_reve | BP_mux_out & 
			zbit_sync | BP_mux_out & zbit_tra | BP_mux_out & zbit_wccm | BP_mux_out & 
			zbit_wcps | BP_mux_out & zbit_wcr ) next_BP_mux_out=1;
		else next_BP_mux_out=0;

		if ( BP_reset_code & read_event | BP_reset_code & read_registers | 
			BP_reset_code & rescmd_sync | BP_reset_code & sbit_clr | BP_reset_code & 
			sbit_cstr | BP_reset_code & sbit_mr | BP_reset_code & sbit_rege | 
			BP_reset_code & sbit_reve | BP_reset_code & sbit_sync | BP_reset_code & 
			sbit_tra | BP_reset_code & sbit_wccm | BP_reset_code & sbit_wcps | 
			BP_reset_code & sbit_wcr | BP_reset_code & send_add_rege | BP_reset_code & 
			send_addr_mr | BP_reset_code & send_addr_wccm | BP_reset_code & 
			send_addr_wcps | BP_reset_code & send_addr_wcr | cnt0 & send_cmd_clr | cnt1 &
			 send_cmd_clr | cnt2 & send_cmd_clr | cnt3 & send_cmd_clr | cnt4 & 
			send_cmd_clr | cnt5 & send_cmd_clr | cnt0 & send_cmd_cstr | cnt1 & 
			send_cmd_cstr | cnt2 & send_cmd_cstr | cnt3 & send_cmd_cstr | cnt4 & 
			send_cmd_cstr | cnt5 & send_cmd_cstr | cnt0 & send_cmd_mr | cnt1 & 
			send_cmd_mr | cnt2 & send_cmd_mr | cnt3 & send_cmd_mr | cnt4 & send_cmd_mr | 
			cnt5 & send_cmd_mr | cnt0 & send_cmd_reve | cnt1 & send_cmd_reve | cnt2 & 
			send_cmd_reve | cnt3 & send_cmd_reve | cnt4 & send_cmd_reve | cnt5 & 
			send_cmd_reve | ~cnt0 & ~cnt1 & ~cnt2 & ~cnt3 & ~cnt4 & ~cnt5 & send_cmd_sync
			 | cnt0 & send_cmd_tra | cnt1 & send_cmd_tra | cnt2 & send_cmd_tra | cnt3 & 
			send_cmd_tra | cnt4 & send_cmd_tra | cnt5 & send_cmd_tra | cnt0 & 
			send_cmd_wccm | cnt1 & send_cmd_wccm | cnt2 & send_cmd_wccm | cnt3 & 
			send_cmd_wccm | cnt4 & send_cmd_wccm | cnt5 & send_cmd_wccm | cnt0 & 
			send_cmd_wcps | cnt1 & send_cmd_wcps | cnt2 & send_cmd_wcps | cnt3 & 
			send_cmd_wcps | cnt4 & send_cmd_wcps | cnt5 & send_cmd_wcps | cnt0 & 
			send_cmd_wcr | cnt1 & send_cmd_wcr | cnt2 & send_cmd_wcr | cnt3 & 
			send_cmd_wcr | cnt4 & send_cmd_wcr | cnt5 & send_cmd_wcr | cnt0 & 
			send_code_rege | cnt1 & send_code_rege | cnt2 & send_code_rege | cnt3 & 
			send_code_rege | cnt4 & send_code_rege | cnt5 & send_code_rege | 
			BP_reset_code & send_data1_wccm | BP_reset_code & send_data2_wccm | 
			BP_reset_code & send_data3_wccm | BP_reset_code & send_data4_wccm | 
			BP_reset_code & send_data5_wccm | BP_reset_code & send_data6_wccm | 
			BP_reset_code & send_data7_wccm | BP_reset_code & send_data_tra | 
			BP_reset_code & send_data_wccm | BP_reset_code & send_data_wcps | 
			BP_reset_code & send_data_wcr | BP_reset_code & wait_sbit_rege | 
			BP_reset_code & wait_sbit_reve | BP_reset_code & ~cmd_code4 & cmd_code3 & 
			wait_sequence | BP_reset_code & ~cmd_code3 & cmd_code4 & wait_sequence | 
			BP_reset_code & ~cmd_code0 & ~cmd_code1 & cmd_code3 & wait_sequence | 
			BP_reset_code & cmd_code1 & cmd_code2 & wait_sequence | BP_reset_code & 
			~cmd_code0 & ~cmd_code2 & cmd_code3 & wait_sequence | BP_reset_code & 
			~cmd_code1 & ~cmd_code2 & wait_sequence | BP_reset_code & cmd_code1 & 
			~cmd_code4 & wait_sequence | BP_reset_code & cmd_code0 & cmd_code1 & 
			cmd_code3 & wait_sequence | BP_reset_code & cmd_code0 & cmd_code2 & cmd_code3
			 & wait_sequence | BP_reset_code & cmd_code2 & ~cmd_code4 & wait_sequence | 
			BP_reset_code & zbit_clr | BP_reset_code & zbit_cstr | BP_reset_code & 
			zbit_mr | BP_reset_code & zbit_rege | BP_reset_code & zbit_reve | 
			BP_reset_code & zbit_sync | BP_reset_code & zbit_tra | BP_reset_code & 
			zbit_wccm | BP_reset_code & zbit_wcps | BP_reset_code & zbit_wcr ) 
			next_BP_reset_code=1;
		else next_BP_reset_code=0;

		cnt= ( {6{read_event}}  & ( {6{cnt0}} | {6{cnt1}} | {6{cnt2}} | {6{cnt3}} |
			 {6{cnt4}} | {6{~cnt5}}  ) & ( {cnt5,cnt4,cnt3,cnt2,cnt1,cnt0}  + 'h1 ) ) | (
			 {6{read_event}}  & ( {6{data_reg31}} & {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} 
			& {6{~cnt3}} & {6{~cnt4}} & {6{cnt5}} | {6{data_reg30}} & {6{~cnt0}} & {6{~
			cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} & {6{cnt5}} | {6{data_reg29}} &
			 {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} & {6{cnt5}} |
			 {6{data_reg28}} & {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~
			cnt4}} & {6{cnt5}} | {6{data_reg27}} & {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} &
			 {6{~cnt3}} & {6{~cnt4}} & {6{cnt5}} | {6{data_reg26}} & {6{~cnt0}} & {6{~
			cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} & {6{cnt5}} | {6{data_reg25}} &
			 {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} & {6{cnt5}} |
			 {6{data_reg24}} & {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~
			cnt4}} & {6{cnt5}} | {6{data_reg23}} & {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} &
			 {6{~cnt3}} & {6{~cnt4}} & {6{cnt5}} | {6{data_reg22}} & {6{~cnt0}} & {6{~
			cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} & {6{cnt5}} | {6{data_reg21}} &
			 {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} & {6{cnt5}} |
			 {6{data_reg20}} & {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~
			cnt4}} & {6{cnt5}} | {6{data_reg19}} & {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} &
			 {6{~cnt3}} & {6{~cnt4}} & {6{cnt5}} | {6{data_reg18}} & {6{~cnt0}} & {6{~
			cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} & {6{cnt5}} | {6{data_reg17}} &
			 {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} & {6{cnt5}} |
			 {6{data_reg16}} & {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~
			cnt4}} & {6{cnt5}} | {6{data_reg15}} & {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} &
			 {6{~cnt3}} & {6{~cnt4}} & {6{cnt5}} | {6{data_reg14}} & {6{~cnt0}} & {6{~
			cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} & {6{cnt5}} | {6{data_reg13}} &
			 {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} & {6{cnt5}} |
			 {6{data_reg12}} & {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~
			cnt4}} & {6{cnt5}} | {6{data_reg11}} & {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} &
			 {6{~cnt3}} & {6{~cnt4}} & {6{cnt5}} | {6{data_reg10}} & {6{~cnt0}} & {6{~
			cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} & {6{cnt5}} | {6{data_reg9}} & 
			{6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} & {6{cnt5}} | 
			{6{data_reg8}} & {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~cnt4
			}} & {6{cnt5}} | {6{data_reg7}} & {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~
			cnt3}} & {6{~cnt4}} & {6{cnt5}} | {6{data_reg6}} & {6{~cnt0}} & {6{~cnt1}} & 
			{6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} & {6{cnt5}} | {6{data_reg5}} & {6{~cnt0
			}} & {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} & {6{cnt5}} | {6{
			data_reg4}} & {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} 
			& {6{cnt5}} | {6{data_reg3}} & {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~
			cnt3}} & {6{~cnt4}} & {6{cnt5}} | {6{data_reg2}} & {6{~cnt0}} & {6{~cnt1}} & 
			{6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} & {6{cnt5}} | {6{data_reg1}} & {6{~cnt0
			}} & {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} & {6{cnt5}} | {6{
			data_reg0}} & {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} 
			& {6{cnt5}}  ) & ( 'h0 ) ) | ( {6{read_event}}  & ( {6{~data_reg0}} & {6{~
			data_reg1}} & {6{~data_reg2}} & {6{~data_reg3}} & {6{~data_reg4}} & {6{~
			data_reg5}} & {6{~data_reg6}} & {6{~data_reg7}} & {6{~data_reg8}} & {6{~
			data_reg9}} & {6{~data_reg10}} & {6{~data_reg11}} & {6{~data_reg12}} & {6{~
			data_reg13}} & {6{~data_reg14}} & {6{~data_reg15}} & {6{~data_reg16}} & {6{~
			data_reg17}} & {6{~data_reg18}} & {6{~data_reg19}} & {6{~data_reg20}} & {6{~
			data_reg21}} & {6{~data_reg22}} & {6{~data_reg23}} & {6{~data_reg24}} & {6{~
			data_reg25}} & {6{~data_reg26}} & {6{~data_reg27}} & {6{~data_reg28}} & {6{~
			data_reg29}} & {6{~data_reg30}} & {6{~data_reg31}} & {6{~cnt0}} & {6{~cnt1}} 
			& {6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} & {6{cnt5}}  ) & ( {cnt5,cnt4,cnt3,
			cnt2,cnt1,cnt0}  ) ) | ( {6{read_registers}}  & ( {6{cnt0}} | {6{cnt1}} | {6{
			cnt2}} | {6{cnt3}} | {6{cnt4}} | {6{~cnt5}}  ) & ( {cnt5,cnt4,cnt3,cnt2,cnt1,
			cnt0}  + 'h1 ) ) | ( {6{read_registers}}  & ( {6{data_reg31}} & {6{~cnt0}} & 
			{6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} & {6{cnt5}} | {6{data_reg30
			}} & {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} & {6{cnt5
			}} | {6{data_reg29}} & {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & 
			{6{~cnt4}} & {6{cnt5}} | {6{data_reg28}} & {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2
			}} & {6{~cnt3}} & {6{~cnt4}} & {6{cnt5}} | {6{data_reg27}} & {6{~cnt0}} & {6{
			~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} & {6{cnt5}} | {6{data_reg26}} 
			& {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} & {6{cnt5}} 
			| {6{data_reg25}} & {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~
			cnt4}} & {6{cnt5}} | {6{data_reg24}} & {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} &
			 {6{~cnt3}} & {6{~cnt4}} & {6{cnt5}} | {6{data_reg23}} & {6{~cnt0}} & {6{~
			cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} & {6{cnt5}} | {6{data_reg22}} &
			 {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} & {6{cnt5}} |
			 {6{data_reg21}} & {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~
			cnt4}} & {6{cnt5}} | {6{data_reg20}} & {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} &
			 {6{~cnt3}} & {6{~cnt4}} & {6{cnt5}} | {6{data_reg19}} & {6{~cnt0}} & {6{~
			cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} & {6{cnt5}} | {6{data_reg18}} &
			 {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} & {6{cnt5}} |
			 {6{data_reg17}} & {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~
			cnt4}} & {6{cnt5}} | {6{data_reg16}} & {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} &
			 {6{~cnt3}} & {6{~cnt4}} & {6{cnt5}} | {6{data_reg15}} & {6{~cnt0}} & {6{~
			cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} & {6{cnt5}} | {6{data_reg14}} &
			 {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} & {6{cnt5}} |
			 {6{data_reg13}} & {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~
			cnt4}} & {6{cnt5}} | {6{data_reg12}} & {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} &
			 {6{~cnt3}} & {6{~cnt4}} & {6{cnt5}} | {6{data_reg11}} & {6{~cnt0}} & {6{~
			cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} & {6{cnt5}} | {6{data_reg10}} &
			 {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} & {6{cnt5}} |
			 {6{data_reg9}} & {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~
			cnt4}} & {6{cnt5}} | {6{data_reg8}} & {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} & 
			{6{~cnt3}} & {6{~cnt4}} & {6{cnt5}} | {6{data_reg7}} & {6{~cnt0}} & {6{~cnt1
			}} & {6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} & {6{cnt5}} | {6{data_reg6}} & {6{~
			cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} & {6{cnt5}} | {6{
			data_reg5}} & {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} 
			& {6{cnt5}} | {6{data_reg4}} & {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~
			cnt3}} & {6{~cnt4}} & {6{cnt5}} | {6{data_reg3}} & {6{~cnt0}} & {6{~cnt1}} & 
			{6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} & {6{cnt5}} | {6{data_reg2}} & {6{~cnt0
			}} & {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} & {6{cnt5}} | {6{
			data_reg1}} & {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} 
			& {6{cnt5}} | {6{data_reg0}} & {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~
			cnt3}} & {6{~cnt4}} & {6{cnt5}}  ) & ( 'h0 ) ) | ( {6{read_registers}}  & ( 
			{6{~data_reg0}} & {6{~data_reg1}} & {6{~data_reg2}} & {6{~data_reg3}} & {6{~
			data_reg4}} & {6{~data_reg5}} & {6{~data_reg6}} & {6{~data_reg7}} & {6{~
			data_reg8}} & {6{~data_reg9}} & {6{~data_reg10}} & {6{~data_reg11}} & {6{~
			data_reg12}} & {6{~data_reg13}} & {6{~data_reg14}} & {6{~data_reg15}} & {6{~
			data_reg16}} & {6{~data_reg17}} & {6{~data_reg18}} & {6{~data_reg19}} & {6{~
			data_reg20}} & {6{~data_reg21}} & {6{~data_reg22}} & {6{~data_reg23}} & {6{~
			data_reg24}} & {6{~data_reg25}} & {6{~data_reg26}} & {6{~data_reg27}} & {6{~
			data_reg28}} & {6{~data_reg29}} & {6{~data_reg30}} & {6{~data_reg31}} & {6{~
			cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} & {6{cnt5}}  ) & (
			 {cnt5,cnt4,cnt3,cnt2,cnt1,cnt0}  ) ) | ( {6{rescmd_sync}}  & ( 6'h3f ) & ( {
			cnt5,cnt4,cnt3,cnt2,cnt1,cnt0}  ) ) | ( {6{sbit_clr}}  & ( 6'h3f ) & ( 'h5 ) 
			) | ( {6{sbit_cstr}}  & ( 6'h3f ) & ( 'h5 ) ) | ( {6{sbit_mr}}  & ( 6'h3f ) &
			 ( 'h5 ) ) | ( {6{sbit_rege}}  & ( 6'h3f ) & ( 'h5 ) ) | ( {6{sbit_reve}}  & 
			( 6'h3f ) & ( 'h5 ) ) | ( {6{sbit_sync}}  & ( 6'h3f ) & ( 'h5 ) ) | ( {6{
			sbit_tra}}  & ( 6'h3f ) & ( 'h5 ) ) | ( {6{sbit_wccm}}  & ( 6'h3f ) & ( 'h5 )
			 ) | ( {6{sbit_wcps}}  & ( 6'h3f ) & ( 'h5 ) ) | ( {6{sbit_wcr}}  & ( 6'h3f )
			 & ( 'h5 ) ) | ( {6{send_add_rege}}  & ( {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}}
			 & {6{~cnt3}} & {6{~cnt4}} & {6{~cnt5}}  ) & ( {cnt5,cnt4,cnt3,cnt2,cnt1,cnt0
			}  ) ) | ( {6{send_add_rege}}  & ( {6{cnt5}} | {6{cnt4}} | {6{cnt3}} | {6{
			cnt2}} | {6{cnt1}} | {6{cnt0}}  ) & ( {cnt5,cnt4,cnt3,cnt2,cnt1,cnt0}  - 'h1 
			) ) | ( {6{send_addr_mr}}  & ( {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~
			cnt3}} & {6{~cnt4}} & {6{~cnt5}}  ) & ( {cnt5,cnt4,cnt3,cnt2,cnt1,cnt0}  ) ) 
			| ( {6{send_addr_mr}}  & ( {6{cnt5}} | {6{cnt4}} | {6{cnt3}} | {6{cnt2}} | 
			{6{cnt1}} | {6{cnt0}}  ) & ( {cnt5,cnt4,cnt3,cnt2,cnt1,cnt0}  - 'h1 ) ) | ( 
			{6{send_addr_wccm}}  & ( {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & 
			{6{~cnt4}} & {6{~cnt5}}  ) & ( 'h20 ) ) | ( {6{send_addr_wccm}}  & ( {6{cnt5
			}} | {6{cnt4}} | {6{cnt3}} | {6{cnt2}} | {6{cnt1}} | {6{cnt0}}  ) & ( {cnt5,
			cnt4,cnt3,cnt2,cnt1,cnt0}  - 'h1 ) ) | ( {6{send_addr_wcps}}  & ( {6{~cnt0}} 
			& {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} & {6{~cnt5}}  ) & ( 'h10 
			) ) | ( {6{send_addr_wcps}}  & ( {6{cnt5}} | {6{cnt4}} | {6{cnt3}} | {6{cnt2
			}} | {6{cnt1}} | {6{cnt0}}  ) & ( {cnt5,cnt4,cnt3,cnt2,cnt1,cnt0}  - 'h1 ) ) 
			| ( {6{send_addr_wcr}}  & ( {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}}
			 & {6{~cnt4}} & {6{~cnt5}}  ) & ( 'h20 ) ) | ( {6{send_addr_wcr}}  & ( {6{
			cnt5}} | {6{cnt4}} | {6{cnt3}} | {6{cnt2}} | {6{cnt1}} | {6{cnt0}}  ) & ( {
			cnt5,cnt4,cnt3,cnt2,cnt1,cnt0}  - 'h1 ) ) | ( {6{send_cmd_clr}}  & ( {6{~cnt0
			}} & {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} & {6{~cnt5}}  ) & ( {
			cnt5,cnt4,cnt3,cnt2,cnt1,cnt0}  ) ) | ( {6{send_cmd_clr}}  & ( {6{cnt5}} | 
			{6{cnt4}} | {6{cnt3}} | {6{cnt2}} | {6{cnt1}} | {6{cnt0}}  ) & ( {cnt5,cnt4,
			cnt3,cnt2,cnt1,cnt0}  - 'h1 ) ) | ( {6{send_cmd_cstr}}  & ( {6{~cnt0}} & {6{~
			cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} & {6{~cnt5}}  ) & ( {cnt5,cnt4,
			cnt3,cnt2,cnt1,cnt0}  ) ) | ( {6{send_cmd_cstr}}  & ( {6{cnt5}} | {6{cnt4}} |
			 {6{cnt3}} | {6{cnt2}} | {6{cnt1}} | {6{cnt0}}  ) & ( {cnt5,cnt4,cnt3,cnt2,
			cnt1,cnt0}  - 'h1 ) ) | ( {6{send_cmd_mr}}  & ( {6{~cnt0}} & {6{~cnt1}} & {6{
			~cnt2}} & {6{~cnt3}} & {6{~cnt4}} & {6{~cnt5}}  ) & ( 'h5 ) ) | ( {6{
			send_cmd_mr}}  & ( {6{cnt5}} | {6{cnt4}} | {6{cnt3}} | {6{cnt2}} | {6{cnt1}} 
			| {6{cnt0}}  ) & ( {cnt5,cnt4,cnt3,cnt2,cnt1,cnt0}  - 'h1 ) ) | ( {6{
			send_cmd_reve}}  & ( {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~
			cnt4}} & {6{~cnt5}}  ) & ( {cnt5,cnt4,cnt3,cnt2,cnt1,cnt0}  ) ) | ( {6{
			send_cmd_reve}}  & ( {6{cnt5}} | {6{cnt4}} | {6{cnt3}} | {6{cnt2}} | {6{cnt1
			}} | {6{cnt0}}  ) & ( {cnt5,cnt4,cnt3,cnt2,cnt1,cnt0}  - 'h1 ) ) | ( {6{
			send_cmd_sync}}  & ( {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~
			cnt4}} & {6{~cnt5}}  ) & ( {cnt5,cnt4,cnt3,cnt2,cnt1,cnt0}  ) ) | ( {6{
			send_cmd_sync}}  & ( {6{cnt5}} | {6{cnt4}} | {6{cnt3}} | {6{cnt2}} | {6{cnt1
			}} | {6{cnt0}}  ) & ( {cnt5,cnt4,cnt3,cnt2,cnt1,cnt0}  - 'h1 ) ) | ( {6{
			send_cmd_tra}}  & ( {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~
			cnt4}} & {6{~cnt5}}  ) & ( 'h5 ) ) | ( {6{send_cmd_tra}}  & ( {6{cnt5}} | {6{
			cnt4}} | {6{cnt3}} | {6{cnt2}} | {6{cnt1}} | {6{cnt0}}  ) & ( {cnt5,cnt4,cnt3
			,cnt2,cnt1,cnt0}  - 'h1 ) ) | ( {6{send_cmd_wccm}}  & ( {6{~cnt0}} & {6{~cnt1
			}} & {6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} & {6{~cnt5}}  ) & ( 'h5 ) ) | ( {6{
			send_cmd_wccm}}  & ( {6{cnt5}} | {6{cnt4}} | {6{cnt3}} | {6{cnt2}} | {6{cnt1
			}} | {6{cnt0}}  ) & ( {cnt5,cnt4,cnt3,cnt2,cnt1,cnt0}  - 'h1 ) ) | ( {6{
			send_cmd_wcps}}  & ( {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~
			cnt4}} & {6{~cnt5}}  ) & ( 'h5 ) ) | ( {6{send_cmd_wcps}}  & ( {6{cnt5}} | 
			{6{cnt4}} | {6{cnt3}} | {6{cnt2}} | {6{cnt1}} | {6{cnt0}}  ) & ( {cnt5,cnt4,
			cnt3,cnt2,cnt1,cnt0}  - 'h1 ) ) | ( {6{send_cmd_wcr}}  & ( {6{~cnt0}} & {6{~
			cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} & {6{~cnt5}}  ) & ( 'h5 ) ) | (
			 {6{send_cmd_wcr}}  & ( {6{cnt5}} | {6{cnt4}} | {6{cnt3}} | {6{cnt2}} | {6{
			cnt1}} | {6{cnt0}}  ) & ( {cnt5,cnt4,cnt3,cnt2,cnt1,cnt0}  - 'h1 ) ) | ( {6{
			send_code_rege}}  & ( {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{
			~cnt4}} & {6{~cnt5}}  ) & ( 'h5 ) ) | ( {6{send_code_rege}}  & ( {6{cnt5}} | 
			{6{cnt4}} | {6{cnt3}} | {6{cnt2}} | {6{cnt1}} | {6{cnt0}}  ) & ( {cnt5,cnt4,
			cnt3,cnt2,cnt1,cnt0}  - 'h1 ) ) | ( {6{send_data1_wccm}}  & ( {6{~cnt0}} & 
			{6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} & {6{~cnt5}}  ) & ( 'h20 ) 
			) | ( {6{send_data1_wccm}}  & ( {6{cnt5}} | {6{cnt4}} | {6{cnt3}} | {6{cnt2}}
			 | {6{cnt1}} | {6{cnt0}}  ) & ( {cnt5,cnt4,cnt3,cnt2,cnt1,cnt0}  - 'h1 ) ) | 
			( {6{send_data2_wccm}}  & ( {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}}
			 & {6{~cnt4}} & {6{~cnt5}}  ) & ( 'h20 ) ) | ( {6{send_data2_wccm}}  & ( {6{
			cnt5}} | {6{cnt4}} | {6{cnt3}} | {6{cnt2}} | {6{cnt1}} | {6{cnt0}}  ) & ( {
			cnt5,cnt4,cnt3,cnt2,cnt1,cnt0}  - 'h1 ) ) | ( {6{send_data3_wccm}}  & ( {6{~
			cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} & {6{~cnt5}}  ) & 
			( 'h20 ) ) | ( {6{send_data3_wccm}}  & ( {6{cnt5}} | {6{cnt4}} | {6{cnt3}} | 
			{6{cnt2}} | {6{cnt1}} | {6{cnt0}}  ) & ( {cnt5,cnt4,cnt3,cnt2,cnt1,cnt0}  - 
			'h1 ) ) | ( {6{send_data4_wccm}}  & ( {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} & 
			{6{~cnt3}} & {6{~cnt4}} & {6{~cnt5}}  ) & ( 'h20 ) ) | ( {6{send_data4_wccm}}
			  & ( {6{cnt5}} | {6{cnt4}} | {6{cnt3}} | {6{cnt2}} | {6{cnt1}} | {6{cnt0}}  
			) & ( {cnt5,cnt4,cnt3,cnt2,cnt1,cnt0}  - 'h1 ) ) | ( {6{send_data5_wccm}}  & 
			( {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} & {6{~cnt5}}
			  ) & ( 'h20 ) ) | ( {6{send_data5_wccm}}  & ( {6{cnt5}} | {6{cnt4}} | {6{
			cnt3}} | {6{cnt2}} | {6{cnt1}} | {6{cnt0}}  ) & ( {cnt5,cnt4,cnt3,cnt2,cnt1,
			cnt0}  - 'h1 ) ) | ( {6{send_data6_wccm}}  & ( {6{~cnt0}} & {6{~cnt1}} & {6{~
			cnt2}} & {6{~cnt3}} & {6{~cnt4}} & {6{~cnt5}}  ) & ( 'h20 ) ) | ( {6{
			send_data6_wccm}}  & ( {6{cnt5}} | {6{cnt4}} | {6{cnt3}} | {6{cnt2}} | {6{
			cnt1}} | {6{cnt0}}  ) & ( {cnt5,cnt4,cnt3,cnt2,cnt1,cnt0}  - 'h1 ) ) | ( {6{
			send_data7_wccm}}  & ( {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & 
			{6{~cnt4}} & {6{~cnt5}}  ) & ( {cnt5,cnt4,cnt3,cnt2,cnt1,cnt0}  ) ) | ( {6{
			send_data7_wccm}}  & ( {6{cnt5}} | {6{cnt4}} | {6{cnt3}} | {6{cnt2}} | {6{
			cnt1}} | {6{cnt0}}  ) & ( {cnt5,cnt4,cnt3,cnt2,cnt1,cnt0}  - 'h1 ) ) | ( {6{
			send_data_tra}}  & ( {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~
			cnt4}} & {6{~cnt5}}  ) & ( {cnt5,cnt4,cnt3,cnt2,cnt1,cnt0}  ) ) | ( {6{
			send_data_tra}}  & ( {6{cnt5}} | {6{cnt4}} | {6{cnt3}} | {6{cnt2}} | {6{cnt1
			}} | {6{cnt0}}  ) & ( {cnt5,cnt4,cnt3,cnt2,cnt1,cnt0}  - 'h1 ) ) | ( {6{
			send_data_wccm}}  & ( {6{~cnt0}} & {6{~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{
			~cnt4}} & {6{~cnt5}}  ) & ( 'h20 ) ) | ( {6{send_data_wccm}}  & ( {6{cnt5}} |
			 {6{cnt4}} | {6{cnt3}} | {6{cnt2}} | {6{cnt1}} | {6{cnt0}}  ) & ( {cnt5,cnt4,
			cnt3,cnt2,cnt1,cnt0}  - 'h1 ) ) | ( {6{send_data_wcps}}  & ( {6{~cnt0}} & {6{
			~cnt1}} & {6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} & {6{~cnt5}}  ) & ( {cnt5,cnt4
			,cnt3,cnt2,cnt1,cnt0}  ) ) | ( {6{send_data_wcps}}  & ( {6{cnt5}} | {6{cnt4}}
			 | {6{cnt3}} | {6{cnt2}} | {6{cnt1}} | {6{cnt0}}  ) & ( {cnt5,cnt4,cnt3,cnt2,
			cnt1,cnt0}  - 'h1 ) ) | ( {6{send_data_wcr}}  & ( {6{~cnt0}} & {6{~cnt1}} & 
			{6{~cnt2}} & {6{~cnt3}} & {6{~cnt4}} & {6{~cnt5}}  ) & ( {cnt5,cnt4,cnt3,cnt2
			,cnt1,cnt0}  ) ) | ( {6{send_data_wcr}}  & ( {6{cnt5}} | {6{cnt4}} | {6{cnt3
			}} | {6{cnt2}} | {6{cnt1}} | {6{cnt0}}  ) & ( {cnt5,cnt4,cnt3,cnt2,cnt1,cnt0}
			  - 'h1 ) ) | ( {6{wait_sbit_rege}}  & ( {6{atom_data}}  ) & ( 'h1 ) ) | ( 
			{6{wait_sbit_rege}}  & ( {6{~atom_data}}  ) & ( {cnt5,cnt4,cnt3,cnt2,cnt1,
			cnt0}  ) ) | ( {6{wait_sbit_reve}}  & ( {6{atom_data}}  ) & ( 'h1 ) ) | ( {6{
			wait_sbit_reve}}  & ( {6{~atom_data}}  ) & ( {cnt5,cnt4,cnt3,cnt2,cnt1,cnt0} 
			 ) ) | ( {6{wait_sequence}}  & ( {6{cmd_code0}} & {6{cmd_code1}} & {6{
			cmd_code2}} | {6{~cmd_code2}} & {6{~cmd_code1}} & {6{cmd_code3}} | {6{~
			cmd_code2}} & {6{~cmd_code1}} & {6{cmd_code4}} | {6{~cmd_code3}} & {6{
			cmd_code2}} & {6{cmd_code1}} | {6{~cmd_code4}} & {6{cmd_code2}} & {6{
			cmd_code1}} | {6{~cmd_code2}} & {6{~cmd_code1}} & {6{~cmd_code0}} | {6{~
			cmd_code4}} & {6{cmd_code3}} | {6{~cmd_code3}} & {6{cmd_code4}}  ) & ( {cnt5,
			cnt4,cnt3,cnt2,cnt1,cnt0}  ) ) | ( {6{wait_sequence}}  & ( {6{~cmd_code0}} & 
			{6{~cmd_code1}} & {6{cmd_code2}} & {6{cmd_code3}} & {6{cmd_code4}}  ) & ( {
			cnt5,cnt4,cnt3,cnt2,cnt1,cnt0}  ) ) | ( {6{wait_sequence}}  & ( {6{~cmd_code0
			}} & {6{cmd_code1}} & {6{cmd_code2}} & {6{cmd_code3}} & {6{cmd_code4}}  ) & (
			 {cnt5,cnt4,cnt3,cnt2,cnt1,cnt0}  ) ) | ( {6{wait_sequence}}  & ( {6{~
			cmd_code0}} & {6{cmd_code1}} & {6{~cmd_code2}} & {6{cmd_code3}} & {6{
			cmd_code4}}  ) & ( {cnt5,cnt4,cnt3,cnt2,cnt1,cnt0}  ) ) | ( {6{wait_sequence
			}}  & ( {6{cmd_code0}} & {6{~cmd_code1}} & {6{cmd_code2}} & {6{~cmd_code3}} &
			 {6{~cmd_code4}}  ) & ( {cnt5,cnt4,cnt3,cnt2,cnt1,cnt0}  ) ) | ( {6{
			wait_sequence}}  & ( {6{~cmd_code0}} & {6{cmd_code1}} & {6{~cmd_code2}} & {6{
			~cmd_code3}} & {6{~cmd_code4}}  ) & ( {cnt5,cnt4,cnt3,cnt2,cnt1,cnt0}  ) ) | 
			( {6{wait_sequence}}  & ( {6{cmd_code0}} & {6{~cmd_code1}} & {6{~cmd_code2}} 
			& {6{~cmd_code3}} & {6{~cmd_code4}}  ) & ( {cnt5,cnt4,cnt3,cnt2,cnt1,cnt0}  )
			 ) | ( {6{wait_sequence}}  & ( {6{cmd_code0}} & {6{cmd_code1}} & {6{~
			cmd_code2}} & {6{~cmd_code3}} & {6{~cmd_code4}}  ) & ( {cnt5,cnt4,cnt3,cnt2,
			cnt1,cnt0}  ) ) | ( {6{wait_sequence}}  & ( {6{cmd_code0}} & {6{cmd_code1}} &
			 {6{~cmd_code2}} & {6{cmd_code3}} & {6{cmd_code4}}  ) & ( {cnt5,cnt4,cnt3,
			cnt2,cnt1,cnt0}  ) ) | ( {6{wait_sequence}}  & ( {6{cmd_code0}} & {6{~
			cmd_code1}} & {6{cmd_code2}} & {6{cmd_code3}} & {6{cmd_code4}}  ) & ( {cnt5,
			cnt4,cnt3,cnt2,cnt1,cnt0}  ) ) | ( {6{wait_sequence}}  & ( {6{~cmd_code0}} & 
			{6{~cmd_code1}} & {6{cmd_code2}} & {6{~cmd_code3}} & {6{~cmd_code4}}  ) & ( {
			cnt5,cnt4,cnt3,cnt2,cnt1,cnt0}  ) ) | ( {6{zbit_clr}}  & ( 6'h3f ) & ( {cnt5,
			cnt4,cnt3,cnt2,cnt1,cnt0}  ) ) | ( {6{zbit_cstr}}  & ( 6'h3f ) & ( {cnt5,cnt4
			,cnt3,cnt2,cnt1,cnt0}  ) ) | ( {6{zbit_mr}}  & ( 6'h3f ) & ( {cnt5,cnt4,cnt3,
			cnt2,cnt1,cnt0}  ) ) | ( {6{zbit_rege}}  & ( 6'h3f ) & ( {cnt5,cnt4,cnt3,cnt2
			,cnt1,cnt0}  ) ) | ( {6{zbit_reve}}  & ( 6'h3f ) & ( {cnt5,cnt4,cnt3,cnt2,
			cnt1,cnt0}  ) ) | ( {6{zbit_sync}}  & ( 6'h3f ) & ( {cnt5,cnt4,cnt3,cnt2,cnt1
			,cnt0}  ) ) | ( {6{zbit_tra}}  & ( 6'h3f ) & ( {cnt5,cnt4,cnt3,cnt2,cnt1,cnt0
			}  ) ) | ( {6{zbit_wccm}}  & ( 6'h3f ) & ( {cnt5,cnt4,cnt3,cnt2,cnt1,cnt0}  )
			 ) | ( {6{zbit_wcps}}  & ( 6'h3f ) & ( {cnt5,cnt4,cnt3,cnt2,cnt1,cnt0}  ) ) |
			 ( {6{zbit_wcr}}  & ( 6'h3f ) & ( {cnt5,cnt4,cnt3,cnt2,cnt1,cnt0}  ) );

		data_reg= ( {32{read_event}}  & ( {32{cnt0}} | {32{cnt1}} | {32{cnt2}} | 
			{32{cnt3}} | {32{cnt4}} | {32{~cnt5}}  ) & ( {data_reg30,data_reg29,
			data_reg28,data_reg27,data_reg26,data_reg25,data_reg24,data_reg23,data_reg22,
			data_reg21,data_reg20,data_reg19,data_reg18,data_reg17,data_reg16,data_reg15,
			data_reg14,data_reg13,data_reg12,data_reg11,data_reg10,data_reg9,data_reg8,
			data_reg7,data_reg6,data_reg5,data_reg4,data_reg3,data_reg2,data_reg1,
			data_reg0,atom_data}  ) ) | ( {32{read_event}}  & ( {32{data_reg31}} & {32{~
			cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} |
			 {32{data_reg30}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & 
			{32{~cnt4}} & {32{cnt5}} | {32{data_reg29}} & {32{~cnt0}} & {32{~cnt1}} & 
			{32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg28}} & 
			{32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{
			cnt5}} | {32{data_reg27}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~
			cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg26}} & {32{~cnt0}} & {32{~
			cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{
			data_reg25}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~
			cnt4}} & {32{cnt5}} | {32{data_reg24}} & {32{~cnt0}} & {32{~cnt1}} & {32{~
			cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg23}} & {32{~
			cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} |
			 {32{data_reg22}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & 
			{32{~cnt4}} & {32{cnt5}} | {32{data_reg21}} & {32{~cnt0}} & {32{~cnt1}} & 
			{32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg20}} & 
			{32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{
			cnt5}} | {32{data_reg19}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~
			cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg18}} & {32{~cnt0}} & {32{~
			cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{
			data_reg17}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~
			cnt4}} & {32{cnt5}} | {32{data_reg16}} & {32{~cnt0}} & {32{~cnt1}} & {32{~
			cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg15}} & {32{~
			cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} |
			 {32{data_reg14}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & 
			{32{~cnt4}} & {32{cnt5}} | {32{data_reg13}} & {32{~cnt0}} & {32{~cnt1}} & 
			{32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg12}} & 
			{32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{
			cnt5}} | {32{data_reg11}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~
			cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg10}} & {32{~cnt0}} & {32{~
			cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg9
			}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & 
			{32{cnt5}} | {32{data_reg8}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{
			~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg7}} & {32{~cnt0}} & {32{~
			cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg6
			}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & 
			{32{cnt5}} | {32{data_reg5}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{
			~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg4}} & {32{~cnt0}} & {32{~
			cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg3
			}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & 
			{32{cnt5}} | {32{data_reg2}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{
			~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg1}} & {32{~cnt0}} & {32{~
			cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg0
			}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & 
			{32{cnt5}}  ) & ( {data_reg31,data_reg30,data_reg29,data_reg28,data_reg27,
			data_reg26,data_reg25,data_reg24,data_reg23,data_reg22,data_reg21,data_reg20,
			data_reg19,data_reg18,data_reg17,data_reg16,data_reg15,data_reg14,data_reg13,
			data_reg12,data_reg11,data_reg10,data_reg9,data_reg8,data_reg7,data_reg6,
			data_reg5,data_reg4,data_reg3,data_reg2,data_reg1,data_reg0}  ) ) | ( {32{
			read_event}}  & ( {32{~data_reg0}} & {32{~data_reg1}} & {32{~data_reg2}} & 
			{32{~data_reg3}} & {32{~data_reg4}} & {32{~data_reg5}} & {32{~data_reg6}} & 
			{32{~data_reg7}} & {32{~data_reg8}} & {32{~data_reg9}} & {32{~data_reg10}} & 
			{32{~data_reg11}} & {32{~data_reg12}} & {32{~data_reg13}} & {32{~data_reg14}}
			 & {32{~data_reg15}} & {32{~data_reg16}} & {32{~data_reg17}} & {32{~
			data_reg18}} & {32{~data_reg19}} & {32{~data_reg20}} & {32{~data_reg21}} & 
			{32{~data_reg22}} & {32{~data_reg23}} & {32{~data_reg24}} & {32{~data_reg25}}
			 & {32{~data_reg26}} & {32{~data_reg27}} & {32{~data_reg28}} & {32{~
			data_reg29}} & {32{~data_reg30}} & {32{~data_reg31}} & {32{~cnt0}} & {32{~
			cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}}  ) & ( {
			data_reg31,data_reg30,data_reg29,data_reg28,data_reg27,data_reg26,data_reg25,
			data_reg24,data_reg23,data_reg22,data_reg21,data_reg20,data_reg19,data_reg18,
			data_reg17,data_reg16,data_reg15,data_reg14,data_reg13,data_reg12,data_reg11,
			data_reg10,data_reg9,data_reg8,data_reg7,data_reg6,data_reg5,data_reg4,
			data_reg3,data_reg2,data_reg1,data_reg0}  ) ) | ( {32{read_registers}}  & ( 
			{32{cnt0}} | {32{cnt1}} | {32{cnt2}} | {32{cnt3}} | {32{cnt4}} | {32{~cnt5}} 
			 ) & ( {data_reg30,data_reg29,data_reg28,data_reg27,data_reg26,data_reg25,
			data_reg24,data_reg23,data_reg22,data_reg21,data_reg20,data_reg19,data_reg18,
			data_reg17,data_reg16,data_reg15,data_reg14,data_reg13,data_reg12,data_reg11,
			data_reg10,data_reg9,data_reg8,data_reg7,data_reg6,data_reg5,data_reg4,
			data_reg3,data_reg2,data_reg1,data_reg0,atom_data}  ) ) | ( {32{
			read_registers}}  & ( {32{data_reg31}} & {32{~cnt0}} & {32{~cnt1}} & {32{~
			cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg30}} & {32{~
			cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} |
			 {32{data_reg29}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & 
			{32{~cnt4}} & {32{cnt5}} | {32{data_reg28}} & {32{~cnt0}} & {32{~cnt1}} & 
			{32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg27}} & 
			{32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{
			cnt5}} | {32{data_reg26}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~
			cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg25}} & {32{~cnt0}} & {32{~
			cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{
			data_reg24}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~
			cnt4}} & {32{cnt5}} | {32{data_reg23}} & {32{~cnt0}} & {32{~cnt1}} & {32{~
			cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg22}} & {32{~
			cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} |
			 {32{data_reg21}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & 
			{32{~cnt4}} & {32{cnt5}} | {32{data_reg20}} & {32{~cnt0}} & {32{~cnt1}} & 
			{32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg19}} & 
			{32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{
			cnt5}} | {32{data_reg18}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~
			cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg17}} & {32{~cnt0}} & {32{~
			cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{
			data_reg16}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~
			cnt4}} & {32{cnt5}} | {32{data_reg15}} & {32{~cnt0}} & {32{~cnt1}} & {32{~
			cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg14}} & {32{~
			cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} |
			 {32{data_reg13}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & 
			{32{~cnt4}} & {32{cnt5}} | {32{data_reg12}} & {32{~cnt0}} & {32{~cnt1}} & 
			{32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg11}} & 
			{32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{
			cnt5}} | {32{data_reg10}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~
			cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg9}} & {32{~cnt0}} & {32{~cnt1
			}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg8}} &
			 {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{
			cnt5}} | {32{data_reg7}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~
			cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg6}} & {32{~cnt0}} & {32{~cnt1
			}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg5}} &
			 {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{
			cnt5}} | {32{data_reg4}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~
			cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg3}} & {32{~cnt0}} & {32{~cnt1
			}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg2}} &
			 {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{
			cnt5}} | {32{data_reg1}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~
			cnt3}} & {32{~cnt4}} & {32{cnt5}} | {32{data_reg0}} & {32{~cnt0}} & {32{~cnt1
			}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{cnt5}}  ) & ( {data_reg31,
			data_reg30,data_reg29,data_reg28,data_reg27,data_reg26,data_reg25,data_reg24,
			data_reg23,data_reg22,data_reg21,data_reg20,data_reg19,data_reg18,data_reg17,
			data_reg16,data_reg15,data_reg14,data_reg13,data_reg12,data_reg11,data_reg10,
			data_reg9,data_reg8,data_reg7,data_reg6,data_reg5,data_reg4,data_reg3,
			data_reg2,data_reg1,data_reg0}  ) ) | ( {32{read_registers}}  & ( {32{~
			data_reg0}} & {32{~data_reg1}} & {32{~data_reg2}} & {32{~data_reg3}} & {32{~
			data_reg4}} & {32{~data_reg5}} & {32{~data_reg6}} & {32{~data_reg7}} & {32{~
			data_reg8}} & {32{~data_reg9}} & {32{~data_reg10}} & {32{~data_reg11}} & {32{
			~data_reg12}} & {32{~data_reg13}} & {32{~data_reg14}} & {32{~data_reg15}} & 
			{32{~data_reg16}} & {32{~data_reg17}} & {32{~data_reg18}} & {32{~data_reg19}}
			 & {32{~data_reg20}} & {32{~data_reg21}} & {32{~data_reg22}} & {32{~
			data_reg23}} & {32{~data_reg24}} & {32{~data_reg25}} & {32{~data_reg26}} & 
			{32{~data_reg27}} & {32{~data_reg28}} & {32{~data_reg29}} & {32{~data_reg30}}
			 & {32{~data_reg31}} & {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} 
			& {32{~cnt4}} & {32{cnt5}}  ) & ( {data_reg31,data_reg30,data_reg29,
			data_reg28,data_reg27,data_reg26,data_reg25,data_reg24,data_reg23,data_reg22,
			data_reg21,data_reg20,data_reg19,data_reg18,data_reg17,data_reg16,data_reg15,
			data_reg14,data_reg13,data_reg12,data_reg11,data_reg10,data_reg9,data_reg8,
			data_reg7,data_reg6,data_reg5,data_reg4,data_reg3,data_reg2,data_reg1,
			data_reg0}  ) ) | ( {32{rescmd_sync}}  & ( 32'hffffffff ) & ( {data_reg31,
			data_reg30,data_reg29,data_reg28,data_reg27,data_reg26,data_reg25,data_reg24,
			data_reg23,data_reg22,data_reg21,data_reg20,data_reg19,data_reg18,data_reg17,
			data_reg16,data_reg15,data_reg14,data_reg13,data_reg12,data_reg11,data_reg10,
			data_reg9,data_reg8,data_reg7,data_reg6,data_reg5,data_reg4,data_reg3,
			data_reg2,data_reg1,data_reg0}  ) ) | ( {32{sbit_clr}}  & ( 32'hffffffff ) & 
			( {data_reg31,data_reg30,data_reg29,data_reg28,data_reg27,data_reg26,
			data_reg25,data_reg24,data_reg23,data_reg22,data_reg21,data_reg20,data_reg19,
			data_reg18,data_reg17,data_reg16,data_reg15,data_reg14,data_reg13,data_reg12,
			data_reg11,data_reg10,data_reg9,data_reg8,data_reg7,data_reg6,data_reg5,
			data_reg4,data_reg3,data_reg2,data_reg1,data_reg0}  ) ) | ( {32{sbit_cstr}}  
			& ( 32'hffffffff ) & ( {data_reg31,data_reg30,data_reg29,data_reg28,
			data_reg27,data_reg26,data_reg25,data_reg24,data_reg23,data_reg22,data_reg21,
			data_reg20,data_reg19,data_reg18,data_reg17,data_reg16,data_reg15,data_reg14,
			data_reg13,data_reg12,data_reg11,data_reg10,data_reg9,data_reg8,data_reg7,
			data_reg6,data_reg5,data_reg4,data_reg3,data_reg2,data_reg1,data_reg0}  ) ) |
			 ( {32{sbit_mr}}  & ( 32'hffffffff ) & ( {data_reg31,data_reg30,data_reg29,
			data_reg28,data_reg27,data_reg26,data_reg25,data_reg24,data_reg23,data_reg22,
			data_reg21,data_reg20,data_reg19,data_reg18,data_reg17,data_reg16,data_reg15,
			data_reg14,data_reg13,data_reg12,data_reg11,data_reg10,data_reg9,data_reg8,
			data_reg7,data_reg6,data_reg5,data_reg4,data_reg3,data_reg2,data_reg1,
			data_reg0}  ) ) | ( {32{sbit_rege}}  & ( 32'hffffffff ) & ( {data_reg31,
			data_reg30,data_reg29,data_reg28,data_reg27,data_reg26,data_reg25,data_reg24,
			data_reg23,data_reg22,data_reg21,data_reg20,data_reg19,data_reg18,data_reg17,
			data_reg16,data_reg15,data_reg14,data_reg13,data_reg12,data_reg11,data_reg10,
			data_reg9,data_reg8,data_reg7,data_reg6,data_reg5,data_reg4,data_reg3,
			data_reg2,data_reg1,data_reg0}  ) ) | ( {32{sbit_reve}}  & ( 32'hffffffff ) &
			 ( {data_reg31,data_reg30,data_reg29,data_reg28,data_reg27,data_reg26,
			data_reg25,data_reg24,data_reg23,data_reg22,data_reg21,data_reg20,data_reg19,
			data_reg18,data_reg17,data_reg16,data_reg15,data_reg14,data_reg13,data_reg12,
			data_reg11,data_reg10,data_reg9,data_reg8,data_reg7,data_reg6,data_reg5,
			data_reg4,data_reg3,data_reg2,data_reg1,data_reg0}  ) ) | ( {32{sbit_sync}}  
			& ( 32'hffffffff ) & ( {data_reg31,data_reg30,data_reg29,data_reg28,
			data_reg27,data_reg26,data_reg25,data_reg24,data_reg23,data_reg22,data_reg21,
			data_reg20,data_reg19,data_reg18,data_reg17,data_reg16,data_reg15,data_reg14,
			data_reg13,data_reg12,data_reg11,data_reg10,data_reg9,data_reg8,data_reg7,
			data_reg6,data_reg5,data_reg4,data_reg3,data_reg2,data_reg1,data_reg0}  ) ) |
			 ( {32{sbit_tra}}  & ( 32'hffffffff ) & ( {data_reg31,data_reg30,data_reg29,
			data_reg28,data_reg27,data_reg26,data_reg25,data_reg24,data_reg23,data_reg22,
			data_reg21,data_reg20,data_reg19,data_reg18,data_reg17,data_reg16,data_reg15,
			data_reg14,data_reg13,data_reg12,data_reg11,data_reg10,data_reg9,data_reg8,
			data_reg7,data_reg6,data_reg5,data_reg4,data_reg3,data_reg2,data_reg1,
			data_reg0}  ) ) | ( {32{sbit_wccm}}  & ( 32'hffffffff ) & ( {data_reg31,
			data_reg30,data_reg29,data_reg28,data_reg27,data_reg26,data_reg25,data_reg24,
			data_reg23,data_reg22,data_reg21,data_reg20,data_reg19,data_reg18,data_reg17,
			data_reg16,data_reg15,data_reg14,data_reg13,data_reg12,data_reg11,data_reg10,
			data_reg9,data_reg8,data_reg7,data_reg6,data_reg5,data_reg4,data_reg3,
			data_reg2,data_reg1,data_reg0}  ) ) | ( {32{sbit_wcps}}  & ( 32'hffffffff ) &
			 ( {data_reg31,data_reg30,data_reg29,data_reg28,data_reg27,data_reg26,
			data_reg25,data_reg24,data_reg23,data_reg22,data_reg21,data_reg20,data_reg19,
			data_reg18,data_reg17,data_reg16,data_reg15,data_reg14,data_reg13,data_reg12,
			data_reg11,data_reg10,data_reg9,data_reg8,data_reg7,data_reg6,data_reg5,
			data_reg4,data_reg3,data_reg2,data_reg1,data_reg0}  ) ) | ( {32{sbit_wcr}}  &
			 ( 32'hffffffff ) & ( {data_reg31,data_reg30,data_reg29,data_reg28,data_reg27
			,data_reg26,data_reg25,data_reg24,data_reg23,data_reg22,data_reg21,data_reg20
			,data_reg19,data_reg18,data_reg17,data_reg16,data_reg15,data_reg14,data_reg13
			,data_reg12,data_reg11,data_reg10,data_reg9,data_reg8,data_reg7,data_reg6,
			data_reg5,data_reg4,data_reg3,data_reg2,data_reg1,data_reg0}  ) ) | ( {32{
			send_add_rege}}  & ( {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & 
			{32{~cnt4}} & {32{~cnt5}}  ) & ( {data_reg31,data_reg30,data_reg29,data_reg28
			,data_reg27,data_reg26,data_reg25,data_reg24,data_reg23,data_reg22,data_reg21
			,data_reg20,data_reg19,data_reg18,data_reg17,data_reg16,data_reg15,data_reg14
			,data_reg13,data_reg12,data_reg11,data_reg10,data_reg9,data_reg8,data_reg7,
			data_reg6,data_reg5,data_reg4,data_reg3,data_reg2,data_reg1,data_reg0}  ) ) |
			 ( {32{send_add_rege}}  & ( {32{cnt5}} | {32{cnt4}} | {32{cnt3}} | {32{cnt2}}
			 | {32{cnt1}} | {32{cnt0}}  ) & ( {data_reg31,data_reg30,data_reg29,
			data_reg28,data_reg27,data_reg26,data_reg25,data_reg24,data_reg23,data_reg22,
			data_reg21,data_reg20,data_reg19,data_reg18,data_reg17,data_reg16,data_reg15,
			data_reg14,data_reg13,data_reg12,data_reg11,data_reg10,data_reg9,data_reg8,
			data_reg7,data_reg6,data_reg5,data_reg4,data_reg3,data_reg2,data_reg1,
			data_reg0}  ) ) | ( {32{send_addr_mr}}  & ( {32{~cnt0}} & {32{~cnt1}} & {32{~
			cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{~cnt5}}  ) & ( {data_reg31,
			data_reg30,data_reg29,data_reg28,data_reg27,data_reg26,data_reg25,data_reg24,
			data_reg23,data_reg22,data_reg21,data_reg20,data_reg19,data_reg18,data_reg17,
			data_reg16,data_reg15,data_reg14,data_reg13,data_reg12,data_reg11,data_reg10,
			data_reg9,data_reg8,data_reg7,data_reg6,data_reg5,data_reg4,data_reg3,
			data_reg2,data_reg1,data_reg0}  ) ) | ( {32{send_addr_mr}}  & ( {32{cnt5}} | 
			{32{cnt4}} | {32{cnt3}} | {32{cnt2}} | {32{cnt1}} | {32{cnt0}}  ) & ( {
			data_reg31,data_reg30,data_reg29,data_reg28,data_reg27,data_reg26,data_reg25,
			data_reg24,data_reg23,data_reg22,data_reg21,data_reg20,data_reg19,data_reg18,
			data_reg17,data_reg16,data_reg15,data_reg14,data_reg13,data_reg12,data_reg11,
			data_reg10,data_reg9,data_reg8,data_reg7,data_reg6,data_reg5,data_reg4,
			data_reg3,data_reg2,data_reg1,data_reg0}  ) ) | ( {32{send_addr_wccm}}  & ( 
			{32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{~
			cnt5}}  ) & ( {data31,data30,data29,data28,data27,data26,data25,data24,data23
			,data22,data21,data20,data19,data18,data17,data16,data15,data14,data13,data12
			,data11,data10,data9,data8,data7,data6,data5,data4,data3,data2,data1,data0}  
			) ) | ( {32{send_addr_wccm}}  & ( {32{cnt5}} | {32{cnt4}} | {32{cnt3}} | {32{
			cnt2}} | {32{cnt1}} | {32{cnt0}}  ) & ( {data_reg31,data_reg30,data_reg29,
			data_reg28,data_reg27,data_reg26,data_reg25,data_reg24,data_reg23,data_reg22,
			data_reg21,data_reg20,data_reg19,data_reg18,data_reg17,data_reg16,data_reg15,
			data_reg14,data_reg13,data_reg12,data_reg11,data_reg10,data_reg9,data_reg8,
			data_reg7,data_reg6,data_reg5,data_reg4,data_reg3,data_reg2,data_reg1,
			data_reg0}  ) ) | ( {32{send_addr_wcps}}  & ( {32{~cnt0}} & {32{~cnt1}} & 
			{32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{~cnt5}}  ) & ( {data31,data30,
			data29,data28,data27,data26,data25,data24,data23,data22,data21,data20,data19,
			data18,data17,data16,data15,data14,data13,data12,data11,data10,data9,data8,
			data7,data6,data5,data4,data3,data2,data1,data0}  ) ) | ( {32{send_addr_wcps
			}}  & ( {32{cnt5}} | {32{cnt4}} | {32{cnt3}} | {32{cnt2}} | {32{cnt1}} | {32{
			cnt0}}  ) & ( {data_reg31,data_reg30,data_reg29,data_reg28,data_reg27,
			data_reg26,data_reg25,data_reg24,data_reg23,data_reg22,data_reg21,data_reg20,
			data_reg19,data_reg18,data_reg17,data_reg16,data_reg15,data_reg14,data_reg13,
			data_reg12,data_reg11,data_reg10,data_reg9,data_reg8,data_reg7,data_reg6,
			data_reg5,data_reg4,data_reg3,data_reg2,data_reg1,data_reg0}  ) ) | ( {32{
			send_addr_wcr}}  & ( {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & 
			{32{~cnt4}} & {32{~cnt5}}  ) & ( {data31,data30,data29,data28,data27,data26,
			data25,data24,data23,data22,data21,data20,data19,data18,data17,data16,data15,
			data14,data13,data12,data11,data10,data9,data8,data7,data6,data5,data4,data3,
			data2,data1,data0}  ) ) | ( {32{send_addr_wcr}}  & ( {32{cnt5}} | {32{cnt4}} 
			| {32{cnt3}} | {32{cnt2}} | {32{cnt1}} | {32{cnt0}}  ) & ( {data_reg31,
			data_reg30,data_reg29,data_reg28,data_reg27,data_reg26,data_reg25,data_reg24,
			data_reg23,data_reg22,data_reg21,data_reg20,data_reg19,data_reg18,data_reg17,
			data_reg16,data_reg15,data_reg14,data_reg13,data_reg12,data_reg11,data_reg10,
			data_reg9,data_reg8,data_reg7,data_reg6,data_reg5,data_reg4,data_reg3,
			data_reg2,data_reg1,data_reg0}  ) ) | ( {32{send_cmd_clr}}  & ( {32{~cnt0}} &
			 {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{~cnt5}}  ) & ( {
			data_reg31,data_reg30,data_reg29,data_reg28,data_reg27,data_reg26,data_reg25,
			data_reg24,data_reg23,data_reg22,data_reg21,data_reg20,data_reg19,data_reg18,
			data_reg17,data_reg16,data_reg15,data_reg14,data_reg13,data_reg12,data_reg11,
			data_reg10,data_reg9,data_reg8,data_reg7,data_reg6,data_reg5,data_reg4,
			data_reg3,data_reg2,data_reg1,data_reg0}  ) ) | ( {32{send_cmd_clr}}  & ( 
			{32{cnt5}} | {32{cnt4}} | {32{cnt3}} | {32{cnt2}} | {32{cnt1}} | {32{cnt0}}  
			) & ( {data_reg31,data_reg30,data_reg29,data_reg28,data_reg27,data_reg26,
			data_reg25,data_reg24,data_reg23,data_reg22,data_reg21,data_reg20,data_reg19,
			data_reg18,data_reg17,data_reg16,data_reg15,data_reg14,data_reg13,data_reg12,
			data_reg11,data_reg10,data_reg9,data_reg8,data_reg7,data_reg6,data_reg5,
			data_reg4,data_reg3,data_reg2,data_reg1,data_reg0}  ) ) | ( {32{send_cmd_cstr
			}}  & ( {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} &
			 {32{~cnt5}}  ) & ( {data_reg31,data_reg30,data_reg29,data_reg28,data_reg27,
			data_reg26,data_reg25,data_reg24,data_reg23,data_reg22,data_reg21,data_reg20,
			data_reg19,data_reg18,data_reg17,data_reg16,data_reg15,data_reg14,data_reg13,
			data_reg12,data_reg11,data_reg10,data_reg9,data_reg8,data_reg7,data_reg6,
			data_reg5,data_reg4,data_reg3,data_reg2,data_reg1,data_reg0}  ) ) | ( {32{
			send_cmd_cstr}}  & ( {32{cnt5}} | {32{cnt4}} | {32{cnt3}} | {32{cnt2}} | {32{
			cnt1}} | {32{cnt0}}  ) & ( {data_reg31,data_reg30,data_reg29,data_reg28,
			data_reg27,data_reg26,data_reg25,data_reg24,data_reg23,data_reg22,data_reg21,
			data_reg20,data_reg19,data_reg18,data_reg17,data_reg16,data_reg15,data_reg14,
			data_reg13,data_reg12,data_reg11,data_reg10,data_reg9,data_reg8,data_reg7,
			data_reg6,data_reg5,data_reg4,data_reg3,data_reg2,data_reg1,data_reg0}  ) ) |
			 ( {32{send_cmd_mr}}  & ( {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3
			}} & {32{~cnt4}} & {32{~cnt5}}  ) & ( {data_reg31,data_reg30,data_reg29,
			data_reg28,data_reg27,data_reg26,data_reg25,data_reg24,data_reg23,data_reg22,
			data_reg21,data_reg20,data_reg19,data_reg18,data_reg17,data_reg16,data_reg15,
			data_reg14,data_reg13,data_reg12,data_reg11,data_reg10,data_reg9,data_reg8,
			data_reg7,data_reg6,data_reg5,data_reg4,data_reg3,data_reg2,data_reg1,
			data_reg0}  ) ) | ( {32{send_cmd_mr}}  & ( {32{cnt5}} | {32{cnt4}} | {32{cnt3
			}} | {32{cnt2}} | {32{cnt1}} | {32{cnt0}}  ) & ( {data_reg31,data_reg30,
			data_reg29,data_reg28,data_reg27,data_reg26,data_reg25,data_reg24,data_reg23,
			data_reg22,data_reg21,data_reg20,data_reg19,data_reg18,data_reg17,data_reg16,
			data_reg15,data_reg14,data_reg13,data_reg12,data_reg11,data_reg10,data_reg9,
			data_reg8,data_reg7,data_reg6,data_reg5,data_reg4,data_reg3,data_reg2,
			data_reg1,data_reg0}  ) ) | ( {32{send_cmd_reve}}  & ( {32{~cnt0}} & {32{~
			cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{~cnt5}}  ) & ( {
			data_reg31,data_reg30,data_reg29,data_reg28,data_reg27,data_reg26,data_reg25,
			data_reg24,data_reg23,data_reg22,data_reg21,data_reg20,data_reg19,data_reg18,
			data_reg17,data_reg16,data_reg15,data_reg14,data_reg13,data_reg12,data_reg11,
			data_reg10,data_reg9,data_reg8,data_reg7,data_reg6,data_reg5,data_reg4,
			data_reg3,data_reg2,data_reg1,data_reg0}  ) ) | ( {32{send_cmd_reve}}  & ( 
			{32{cnt5}} | {32{cnt4}} | {32{cnt3}} | {32{cnt2}} | {32{cnt1}} | {32{cnt0}}  
			) & ( {data_reg31,data_reg30,data_reg29,data_reg28,data_reg27,data_reg26,
			data_reg25,data_reg24,data_reg23,data_reg22,data_reg21,data_reg20,data_reg19,
			data_reg18,data_reg17,data_reg16,data_reg15,data_reg14,data_reg13,data_reg12,
			data_reg11,data_reg10,data_reg9,data_reg8,data_reg7,data_reg6,data_reg5,
			data_reg4,data_reg3,data_reg2,data_reg1,data_reg0}  ) ) | ( {32{send_cmd_sync
			}}  & ( {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} &
			 {32{~cnt5}}  ) & ( {data_reg31,data_reg30,data_reg29,data_reg28,data_reg27,
			data_reg26,data_reg25,data_reg24,data_reg23,data_reg22,data_reg21,data_reg20,
			data_reg19,data_reg18,data_reg17,data_reg16,data_reg15,data_reg14,data_reg13,
			data_reg12,data_reg11,data_reg10,data_reg9,data_reg8,data_reg7,data_reg6,
			data_reg5,data_reg4,data_reg3,data_reg2,data_reg1,data_reg0}  ) ) | ( {32{
			send_cmd_sync}}  & ( {32{cnt5}} | {32{cnt4}} | {32{cnt3}} | {32{cnt2}} | {32{
			cnt1}} | {32{cnt0}}  ) & ( {data_reg31,data_reg30,data_reg29,data_reg28,
			data_reg27,data_reg26,data_reg25,data_reg24,data_reg23,data_reg22,data_reg21,
			data_reg20,data_reg19,data_reg18,data_reg17,data_reg16,data_reg15,data_reg14,
			data_reg13,data_reg12,data_reg11,data_reg10,data_reg9,data_reg8,data_reg7,
			data_reg6,data_reg5,data_reg4,data_reg3,data_reg2,data_reg1,data_reg0}  ) ) |
			 ( {32{send_cmd_tra}}  & ( {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~
			cnt3}} & {32{~cnt4}} & {32{~cnt5}}  ) & ( {eve_cou31,eve_cou30,eve_cou29,
			eve_cou28,eve_cou27,eve_cou26,eve_cou25,eve_cou24,eve_cou23,eve_cou22,
			eve_cou21,eve_cou20,eve_cou19,eve_cou18,eve_cou17,eve_cou16,eve_cou15,
			eve_cou14,eve_cou13,eve_cou12,eve_cou11,eve_cou10,eve_cou9,eve_cou8,eve_cou7,
			eve_cou6,eve_cou5,eve_cou4,eve_cou3,eve_cou2,eve_cou1,eve_cou0}  ) ) | ( {32{
			send_cmd_tra}}  & ( {32{cnt5}} | {32{cnt4}} | {32{cnt3}} | {32{cnt2}} | {32{
			cnt1}} | {32{cnt0}}  ) & ( {data_reg31,data_reg30,data_reg29,data_reg28,
			data_reg27,data_reg26,data_reg25,data_reg24,data_reg23,data_reg22,data_reg21,
			data_reg20,data_reg19,data_reg18,data_reg17,data_reg16,data_reg15,data_reg14,
			data_reg13,data_reg12,data_reg11,data_reg10,data_reg9,data_reg8,data_reg7,
			data_reg6,data_reg5,data_reg4,data_reg3,data_reg2,data_reg1,data_reg0}  ) ) |
			 ( {32{send_cmd_wccm}}  & ( {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~
			cnt3}} & {32{~cnt4}} & {32{~cnt5}}  ) & ( {data_reg31,data_reg30,data_reg29,
			data_reg28,data_reg27,data_reg26,data_reg25,data_reg24,data_reg23,data_reg22,
			data_reg21,data_reg20,data_reg19,data_reg18,data_reg17,data_reg16,data_reg15,
			data_reg14,data_reg13,data_reg12,data_reg11,data_reg10,data_reg9,data_reg8,
			data_reg7,data_reg6,data_reg5,data_reg4,data_reg3,data_reg2,data_reg1,
			data_reg0}  ) ) | ( {32{send_cmd_wccm}}  & ( {32{cnt5}} | {32{cnt4}} | {32{
			cnt3}} | {32{cnt2}} | {32{cnt1}} | {32{cnt0}}  ) & ( {data_reg31,data_reg30,
			data_reg29,data_reg28,data_reg27,data_reg26,data_reg25,data_reg24,data_reg23,
			data_reg22,data_reg21,data_reg20,data_reg19,data_reg18,data_reg17,data_reg16,
			data_reg15,data_reg14,data_reg13,data_reg12,data_reg11,data_reg10,data_reg9,
			data_reg8,data_reg7,data_reg6,data_reg5,data_reg4,data_reg3,data_reg2,
			data_reg1,data_reg0}  ) ) | ( {32{send_cmd_wcps}}  & ( {32{~cnt0}} & {32{~
			cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{~cnt5}}  ) & ( {
			data_reg31,data_reg30,data_reg29,data_reg28,data_reg27,data_reg26,data_reg25,
			data_reg24,data_reg23,data_reg22,data_reg21,data_reg20,data_reg19,data_reg18,
			data_reg17,data_reg16,data_reg15,data_reg14,data_reg13,data_reg12,data_reg11,
			data_reg10,data_reg9,data_reg8,data_reg7,data_reg6,data_reg5,data_reg4,
			data_reg3,data_reg2,data_reg1,data_reg0}  ) ) | ( {32{send_cmd_wcps}}  & ( 
			{32{cnt5}} | {32{cnt4}} | {32{cnt3}} | {32{cnt2}} | {32{cnt1}} | {32{cnt0}}  
			) & ( {data_reg31,data_reg30,data_reg29,data_reg28,data_reg27,data_reg26,
			data_reg25,data_reg24,data_reg23,data_reg22,data_reg21,data_reg20,data_reg19,
			data_reg18,data_reg17,data_reg16,data_reg15,data_reg14,data_reg13,data_reg12,
			data_reg11,data_reg10,data_reg9,data_reg8,data_reg7,data_reg6,data_reg5,
			data_reg4,data_reg3,data_reg2,data_reg1,data_reg0}  ) ) | ( {32{send_cmd_wcr
			}}  & ( {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} &
			 {32{~cnt5}}  ) & ( {data_reg31,data_reg30,data_reg29,data_reg28,data_reg27,
			data_reg26,data_reg25,data_reg24,data_reg23,data_reg22,data_reg21,data_reg20,
			data_reg19,data_reg18,data_reg17,data_reg16,data_reg15,data_reg14,data_reg13,
			data_reg12,data_reg11,data_reg10,data_reg9,data_reg8,data_reg7,data_reg6,
			data_reg5,data_reg4,data_reg3,data_reg2,data_reg1,data_reg0}  ) ) | ( {32{
			send_cmd_wcr}}  & ( {32{cnt5}} | {32{cnt4}} | {32{cnt3}} | {32{cnt2}} | {32{
			cnt1}} | {32{cnt0}}  ) & ( {data_reg31,data_reg30,data_reg29,data_reg28,
			data_reg27,data_reg26,data_reg25,data_reg24,data_reg23,data_reg22,data_reg21,
			data_reg20,data_reg19,data_reg18,data_reg17,data_reg16,data_reg15,data_reg14,
			data_reg13,data_reg12,data_reg11,data_reg10,data_reg9,data_reg8,data_reg7,
			data_reg6,data_reg5,data_reg4,data_reg3,data_reg2,data_reg1,data_reg0}  ) ) |
			 ( {32{send_code_rege}}  & ( {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~
			cnt3}} & {32{~cnt4}} & {32{~cnt5}}  ) & ( {data_reg31,data_reg30,data_reg29,
			data_reg28,data_reg27,data_reg26,data_reg25,data_reg24,data_reg23,data_reg22,
			data_reg21,data_reg20,data_reg19,data_reg18,data_reg17,data_reg16,data_reg15,
			data_reg14,data_reg13,data_reg12,data_reg11,data_reg10,data_reg9,data_reg8,
			data_reg7,data_reg6,data_reg5,data_reg4,data_reg3,data_reg2,data_reg1,
			data_reg0}  ) ) | ( {32{send_code_rege}}  & ( {32{cnt5}} | {32{cnt4}} | {32{
			cnt3}} | {32{cnt2}} | {32{cnt1}} | {32{cnt0}}  ) & ( {data_reg31,data_reg30,
			data_reg29,data_reg28,data_reg27,data_reg26,data_reg25,data_reg24,data_reg23,
			data_reg22,data_reg21,data_reg20,data_reg19,data_reg18,data_reg17,data_reg16,
			data_reg15,data_reg14,data_reg13,data_reg12,data_reg11,data_reg10,data_reg9,
			data_reg8,data_reg7,data_reg6,data_reg5,data_reg4,data_reg3,data_reg2,
			data_reg1,data_reg0}  ) ) | ( {32{send_data1_wccm}}  & ( {32{~cnt0}} & {32{~
			cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{~cnt5}}  ) & ( {
			data_231,data_230,data_229,data_228,data_227,data_226,data_225,data_224,
			data_223,data_222,data_221,data_220,data_219,data_218,data_217,data_216,
			data_215,data_214,data_213,data_212,data_211,data_210,data_29,data_28,data_27
			,data_26,data_25,data_24,data_23,data_22,data_21,data_20}  ) ) | ( {32{
			send_data1_wccm}}  & ( {32{cnt5}} | {32{cnt4}} | {32{cnt3}} | {32{cnt2}} | 
			{32{cnt1}} | {32{cnt0}}  ) & ( {1'h0,data_reg31,data_reg30,data_reg29,
			data_reg28,data_reg27,data_reg26,data_reg25,data_reg24,data_reg23,data_reg22,
			data_reg21,data_reg20,data_reg19,data_reg18,data_reg17,data_reg16,data_reg15,
			data_reg14,data_reg13,data_reg12,data_reg11,data_reg10,data_reg9,data_reg8,
			data_reg7,data_reg6,data_reg5,data_reg4,data_reg3,data_reg2,data_reg1}  ) ) |
			 ( {32{send_data2_wccm}}  & ( {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~
			cnt3}} & {32{~cnt4}} & {32{~cnt5}}  ) & ( {data_331,data_330,data_329,
			data_328,data_327,data_326,data_325,data_324,data_323,data_322,data_321,
			data_320,data_319,data_318,data_317,data_316,data_315,data_314,data_313,
			data_312,data_311,data_310,data_39,data_38,data_37,data_36,data_35,data_34,
			data_33,data_32,data_31,data_30}  ) ) | ( {32{send_data2_wccm}}  & ( {32{cnt5
			}} | {32{cnt4}} | {32{cnt3}} | {32{cnt2}} | {32{cnt1}} | {32{cnt0}}  ) & ( {
			1'h0,data_reg31,data_reg30,data_reg29,data_reg28,data_reg27,data_reg26,
			data_reg25,data_reg24,data_reg23,data_reg22,data_reg21,data_reg20,data_reg19,
			data_reg18,data_reg17,data_reg16,data_reg15,data_reg14,data_reg13,data_reg12,
			data_reg11,data_reg10,data_reg9,data_reg8,data_reg7,data_reg6,data_reg5,
			data_reg4,data_reg3,data_reg2,data_reg1}  ) ) | ( {32{send_data3_wccm}}  & ( 
			{32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{~
			cnt5}}  ) & ( {data_431,data_430,data_429,data_428,data_427,data_426,data_425
			,data_424,data_423,data_422,data_421,data_420,data_419,data_418,data_417,
			data_416,data_415,data_414,data_413,data_412,data_411,data_410,data_49,
			data_48,data_47,data_46,data_45,data_44,data_43,data_42,data_41,data_40}  ) )
			 | ( {32{send_data3_wccm}}  & ( {32{cnt5}} | {32{cnt4}} | {32{cnt3}} | {32{
			cnt2}} | {32{cnt1}} | {32{cnt0}}  ) & ( {1'h0,data_reg31,data_reg30,
			data_reg29,data_reg28,data_reg27,data_reg26,data_reg25,data_reg24,data_reg23,
			data_reg22,data_reg21,data_reg20,data_reg19,data_reg18,data_reg17,data_reg16,
			data_reg15,data_reg14,data_reg13,data_reg12,data_reg11,data_reg10,data_reg9,
			data_reg8,data_reg7,data_reg6,data_reg5,data_reg4,data_reg3,data_reg2,
			data_reg1}  ) ) | ( {32{send_data4_wccm}}  & ( {32{~cnt0}} & {32{~cnt1}} & 
			{32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{~cnt5}}  ) & ( {data_531,
			data_530,data_529,data_528,data_527,data_526,data_525,data_524,data_523,
			data_522,data_521,data_520,data_519,data_518,data_517,data_516,data_515,
			data_514,data_513,data_512,data_511,data_510,data_59,data_58,data_57,data_56,
			data_55,data_54,data_53,data_52,data_51,data_50}  ) ) | ( {32{send_data4_wccm
			}}  & ( {32{cnt5}} | {32{cnt4}} | {32{cnt3}} | {32{cnt2}} | {32{cnt1}} | {32{
			cnt0}}  ) & ( {1'h0,data_reg31,data_reg30,data_reg29,data_reg28,data_reg27,
			data_reg26,data_reg25,data_reg24,data_reg23,data_reg22,data_reg21,data_reg20,
			data_reg19,data_reg18,data_reg17,data_reg16,data_reg15,data_reg14,data_reg13,
			data_reg12,data_reg11,data_reg10,data_reg9,data_reg8,data_reg7,data_reg6,
			data_reg5,data_reg4,data_reg3,data_reg2,data_reg1}  ) ) | ( {32{
			send_data5_wccm}}  & ( {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} 
			& {32{~cnt4}} & {32{~cnt5}}  ) & ( {data_631,data_630,data_629,data_628,
			data_627,data_626,data_625,data_624,data_623,data_622,data_621,data_620,
			data_619,data_618,data_617,data_616,data_615,data_614,data_613,data_612,
			data_611,data_610,data_69,data_68,data_67,data_66,data_65,data_64,data_63,
			data_62,data_61,data_60}  ) ) | ( {32{send_data5_wccm}}  & ( {32{cnt5}} | 
			{32{cnt4}} | {32{cnt3}} | {32{cnt2}} | {32{cnt1}} | {32{cnt0}}  ) & ( {1'h0,
			data_reg31,data_reg30,data_reg29,data_reg28,data_reg27,data_reg26,data_reg25,
			data_reg24,data_reg23,data_reg22,data_reg21,data_reg20,data_reg19,data_reg18,
			data_reg17,data_reg16,data_reg15,data_reg14,data_reg13,data_reg12,data_reg11,
			data_reg10,data_reg9,data_reg8,data_reg7,data_reg6,data_reg5,data_reg4,
			data_reg3,data_reg2,data_reg1}  ) ) | ( {32{send_data6_wccm}}  & ( {32{~cnt0
			}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{~cnt5}}  ) &
			 ( {data_731,data_730,data_729,data_728,data_727,data_726,data_725,data_724,
			data_723,data_722,data_721,data_720,data_719,data_718,data_717,data_716,
			data_715,data_714,data_713,data_712,data_711,data_710,data_79,data_78,data_77
			,data_76,data_75,data_74,data_73,data_72,data_71,data_70}  ) ) | ( {32{
			send_data6_wccm}}  & ( {32{cnt5}} | {32{cnt4}} | {32{cnt3}} | {32{cnt2}} | 
			{32{cnt1}} | {32{cnt0}}  ) & ( {1'h0,data_reg31,data_reg30,data_reg29,
			data_reg28,data_reg27,data_reg26,data_reg25,data_reg24,data_reg23,data_reg22,
			data_reg21,data_reg20,data_reg19,data_reg18,data_reg17,data_reg16,data_reg15,
			data_reg14,data_reg13,data_reg12,data_reg11,data_reg10,data_reg9,data_reg8,
			data_reg7,data_reg6,data_reg5,data_reg4,data_reg3,data_reg2,data_reg1}  ) ) |
			 ( {32{send_data7_wccm}}  & ( {32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~
			cnt3}} & {32{~cnt4}} & {32{~cnt5}}  ) & ( {data_reg31,data_reg30,data_reg29,
			data_reg28,data_reg27,data_reg26,data_reg25,data_reg24,data_reg23,data_reg22,
			data_reg21,data_reg20,data_reg19,data_reg18,data_reg17,data_reg16,data_reg15,
			data_reg14,data_reg13,data_reg12,data_reg11,data_reg10,data_reg9,data_reg8,
			data_reg7,data_reg6,data_reg5,data_reg4,data_reg3,data_reg2,data_reg1,
			data_reg0}  ) ) | ( {32{send_data7_wccm}}  & ( {32{cnt5}} | {32{cnt4}} | {32{
			cnt3}} | {32{cnt2}} | {32{cnt1}} | {32{cnt0}}  ) & ( {1'h0,data_reg31,
			data_reg30,data_reg29,data_reg28,data_reg27,data_reg26,data_reg25,data_reg24,
			data_reg23,data_reg22,data_reg21,data_reg20,data_reg19,data_reg18,data_reg17,
			data_reg16,data_reg15,data_reg14,data_reg13,data_reg12,data_reg11,data_reg10,
			data_reg9,data_reg8,data_reg7,data_reg6,data_reg5,data_reg4,data_reg3,
			data_reg2,data_reg1}  ) ) | ( {32{send_data_tra}}  & ( {32{~cnt0}} & {32{~
			cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{~cnt5}}  ) & ( {
			data_reg31,data_reg30,data_reg29,data_reg28,data_reg27,data_reg26,data_reg25,
			data_reg24,data_reg23,data_reg22,data_reg21,data_reg20,data_reg19,data_reg18,
			data_reg17,data_reg16,data_reg15,data_reg14,data_reg13,data_reg12,data_reg11,
			data_reg10,data_reg9,data_reg8,data_reg7,data_reg6,data_reg5,data_reg4,
			data_reg3,data_reg2,data_reg1,data_reg0}  ) ) | ( {32{send_data_tra}}  & ( 
			{32{cnt5}} | {32{cnt4}} | {32{cnt3}} | {32{cnt2}} | {32{cnt1}} | {32{cnt0}}  
			) & ( {1'h0,data_reg31,data_reg30,data_reg29,data_reg28,data_reg27,data_reg26
			,data_reg25,data_reg24,data_reg23,data_reg22,data_reg21,data_reg20,data_reg19
			,data_reg18,data_reg17,data_reg16,data_reg15,data_reg14,data_reg13,data_reg12
			,data_reg11,data_reg10,data_reg9,data_reg8,data_reg7,data_reg6,data_reg5,
			data_reg4,data_reg3,data_reg2,data_reg1}  ) ) | ( {32{send_data_wccm}}  & ( 
			{32{~cnt0}} & {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{~
			cnt5}}  ) & ( {data_131,data_130,data_129,data_128,data_127,data_126,data_125
			,data_124,data_123,data_122,data_121,data_120,data_119,data_118,data_117,
			data_116,data_115,data_114,data_113,data_112,data_111,data_110,data_19,
			data_18,data_17,data_16,data_15,data_14,data_13,data_12,data_11,data_10}  ) )
			 | ( {32{send_data_wccm}}  & ( {32{cnt5}} | {32{cnt4}} | {32{cnt3}} | {32{
			cnt2}} | {32{cnt1}} | {32{cnt0}}  ) & ( {1'h0,data_reg31,data_reg30,
			data_reg29,data_reg28,data_reg27,data_reg26,data_reg25,data_reg24,data_reg23,
			data_reg22,data_reg21,data_reg20,data_reg19,data_reg18,data_reg17,data_reg16,
			data_reg15,data_reg14,data_reg13,data_reg12,data_reg11,data_reg10,data_reg9,
			data_reg8,data_reg7,data_reg6,data_reg5,data_reg4,data_reg3,data_reg2,
			data_reg1}  ) ) | ( {32{send_data_wcps}}  & ( {32{~cnt0}} & {32{~cnt1}} & 
			{32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{~cnt5}}  ) & ( {data_reg31,
			data_reg30,data_reg29,data_reg28,data_reg27,data_reg26,data_reg25,data_reg24,
			data_reg23,data_reg22,data_reg21,data_reg20,data_reg19,data_reg18,data_reg17,
			data_reg16,data_reg15,data_reg14,data_reg13,data_reg12,data_reg11,data_reg10,
			data_reg9,data_reg8,data_reg7,data_reg6,data_reg5,data_reg4,data_reg3,
			data_reg2,data_reg1,data_reg0}  ) ) | ( {32{send_data_wcps}}  & ( {32{cnt5}} 
			| {32{cnt4}} | {32{cnt3}} | {32{cnt2}} | {32{cnt1}} | {32{cnt0}}  ) & ( {1'h0
			,data_reg31,data_reg30,data_reg29,data_reg28,data_reg27,data_reg26,data_reg25
			,data_reg24,data_reg23,data_reg22,data_reg21,data_reg20,data_reg19,data_reg18
			,data_reg17,data_reg16,data_reg15,data_reg14,data_reg13,data_reg12,data_reg11
			,data_reg10,data_reg9,data_reg8,data_reg7,data_reg6,data_reg5,data_reg4,
			data_reg3,data_reg2,data_reg1}  ) ) | ( {32{send_data_wcr}}  & ( {32{~cnt0}} 
			& {32{~cnt1}} & {32{~cnt2}} & {32{~cnt3}} & {32{~cnt4}} & {32{~cnt5}}  ) & ( 
			{data_reg31,data_reg30,data_reg29,data_reg28,data_reg27,data_reg26,data_reg25
			,data_reg24,data_reg23,data_reg22,data_reg21,data_reg20,data_reg19,data_reg18
			,data_reg17,data_reg16,data_reg15,data_reg14,data_reg13,data_reg12,data_reg11
			,data_reg10,data_reg9,data_reg8,data_reg7,data_reg6,data_reg5,data_reg4,
			data_reg3,data_reg2,data_reg1,data_reg0}  ) ) | ( {32{send_data_wcr}}  & ( 
			{32{cnt5}} | {32{cnt4}} | {32{cnt3}} | {32{cnt2}} | {32{cnt1}} | {32{cnt0}}  
			) & ( {1'h0,data_reg31,data_reg30,data_reg29,data_reg28,data_reg27,data_reg26
			,data_reg25,data_reg24,data_reg23,data_reg22,data_reg21,data_reg20,data_reg19
			,data_reg18,data_reg17,data_reg16,data_reg15,data_reg14,data_reg13,data_reg12
			,data_reg11,data_reg10,data_reg9,data_reg8,data_reg7,data_reg6,data_reg5,
			data_reg4,data_reg3,data_reg2,data_reg1}  ) ) | ( {32{wait_sbit_rege}}  & ( 
			{32{atom_data}}  ) & ( 'h1 ) ) | ( {32{wait_sbit_rege}}  & ( {32{~atom_data}}
			  ) & ( {data_reg31,data_reg30,data_reg29,data_reg28,data_reg27,data_reg26,
			data_reg25,data_reg24,data_reg23,data_reg22,data_reg21,data_reg20,data_reg19,
			data_reg18,data_reg17,data_reg16,data_reg15,data_reg14,data_reg13,data_reg12,
			data_reg11,data_reg10,data_reg9,data_reg8,data_reg7,data_reg6,data_reg5,
			data_reg4,data_reg3,data_reg2,data_reg1,data_reg0}  ) ) | ( {32{
			wait_sbit_reve}}  & ( {32{atom_data}}  ) & ( 'h1 ) ) | ( {32{wait_sbit_reve}}
			  & ( {32{~atom_data}}  ) & ( {data_reg31,data_reg30,data_reg29,data_reg28,
			data_reg27,data_reg26,data_reg25,data_reg24,data_reg23,data_reg22,data_reg21,
			data_reg20,data_reg19,data_reg18,data_reg17,data_reg16,data_reg15,data_reg14,
			data_reg13,data_reg12,data_reg11,data_reg10,data_reg9,data_reg8,data_reg7,
			data_reg6,data_reg5,data_reg4,data_reg3,data_reg2,data_reg1,data_reg0}  ) ) |
			 ( {32{wait_sequence}}  & ( {32{cmd_code0}} & {32{cmd_code1}} & {32{cmd_code2
			}} | {32{~cmd_code2}} & {32{~cmd_code1}} & {32{cmd_code3}} | {32{~cmd_code2}}
			 & {32{~cmd_code1}} & {32{cmd_code4}} | {32{~cmd_code3}} & {32{cmd_code2}} & 
			{32{cmd_code1}} | {32{~cmd_code4}} & {32{cmd_code2}} & {32{cmd_code1}} | {32{
			~cmd_code2}} & {32{~cmd_code1}} & {32{~cmd_code0}} | {32{~cmd_code4}} & {32{
			cmd_code3}} | {32{~cmd_code3}} & {32{cmd_code4}}  ) & ( {data_reg31,
			data_reg30,data_reg29,data_reg28,data_reg27,data_reg26,data_reg25,data_reg24,
			data_reg23,data_reg22,data_reg21,data_reg20,data_reg19,data_reg18,data_reg17,
			data_reg16,data_reg15,data_reg14,data_reg13,data_reg12,data_reg11,data_reg10,
			data_reg9,data_reg8,data_reg7,data_reg6,data_reg5,data_reg4,data_reg3,
			data_reg2,data_reg1,data_reg0}  ) ) | ( {32{wait_sequence}}  & ( {32{~
			cmd_code0}} & {32{~cmd_code1}} & {32{cmd_code2}} & {32{cmd_code3}} & {32{
			cmd_code4}}  ) & ( {data_reg31,data_reg30,data_reg29,data_reg28,data_reg27,
			data_reg26,data_reg25,data_reg24,data_reg23,data_reg22,data_reg21,data_reg20,
			data_reg19,data_reg18,data_reg17,data_reg16,data_reg15,data_reg14,data_reg13,
			data_reg12,data_reg11,data_reg10,data_reg9,data_reg8,data_reg7,data_reg6,
			data_reg5,data_reg4,data_reg3,data_reg2,data_reg1,data_reg0}  ) ) | ( {32{
			wait_sequence}}  & ( {32{~cmd_code0}} & {32{cmd_code1}} & {32{cmd_code2}} & 
			{32{cmd_code3}} & {32{cmd_code4}}  ) & ( {data_reg31,data_reg30,data_reg29,
			data_reg28,data_reg27,data_reg26,data_reg25,data_reg24,data_reg23,data_reg22,
			data_reg21,data_reg20,data_reg19,data_reg18,data_reg17,data_reg16,data_reg15,
			data_reg14,data_reg13,data_reg12,data_reg11,data_reg10,data_reg9,data_reg8,
			data_reg7,data_reg6,data_reg5,data_reg4,data_reg3,data_reg2,data_reg1,
			data_reg0}  ) ) | ( {32{wait_sequence}}  & ( {32{~cmd_code0}} & {32{cmd_code1
			}} & {32{~cmd_code2}} & {32{cmd_code3}} & {32{cmd_code4}}  ) & ( {data_reg31,
			data_reg30,data_reg29,data_reg28,data_reg27,data_reg26,data_reg25,data_reg24,
			data_reg23,data_reg22,data_reg21,data_reg20,data_reg19,data_reg18,data_reg17,
			data_reg16,data_reg15,data_reg14,data_reg13,data_reg12,data_reg11,data_reg10,
			data_reg9,data_reg8,data_reg7,data_reg6,data_reg5,data_reg4,data_reg3,
			data_reg2,data_reg1,data_reg0}  ) ) | ( {32{wait_sequence}}  & ( {32{
			cmd_code0}} & {32{~cmd_code1}} & {32{cmd_code2}} & {32{~cmd_code3}} & {32{~
			cmd_code4}}  ) & ( {data_reg31,data_reg30,data_reg29,data_reg28,data_reg27,
			data_reg26,data_reg25,data_reg24,data_reg23,data_reg22,data_reg21,data_reg20,
			data_reg19,data_reg18,data_reg17,data_reg16,data_reg15,data_reg14,data_reg13,
			data_reg12,data_reg11,data_reg10,data_reg9,data_reg8,data_reg7,data_reg6,
			data_reg5,data_reg4,data_reg3,data_reg2,data_reg1,data_reg0}  ) ) | ( {32{
			wait_sequence}}  & ( {32{~cmd_code0}} & {32{cmd_code1}} & {32{~cmd_code2}} & 
			{32{~cmd_code3}} & {32{~cmd_code4}}  ) & ( {data_reg31,data_reg30,data_reg29,
			data_reg28,data_reg27,data_reg26,data_reg25,data_reg24,data_reg23,data_reg22,
			data_reg21,data_reg20,data_reg19,data_reg18,data_reg17,data_reg16,data_reg15,
			data_reg14,data_reg13,data_reg12,data_reg11,data_reg10,data_reg9,data_reg8,
			data_reg7,data_reg6,data_reg5,data_reg4,data_reg3,data_reg2,data_reg1,
			data_reg0}  ) ) | ( {32{wait_sequence}}  & ( {32{cmd_code0}} & {32{~cmd_code1
			}} & {32{~cmd_code2}} & {32{~cmd_code3}} & {32{~cmd_code4}}  ) & ( {
			data_reg31,data_reg30,data_reg29,data_reg28,data_reg27,data_reg26,data_reg25,
			data_reg24,data_reg23,data_reg22,data_reg21,data_reg20,data_reg19,data_reg18,
			data_reg17,data_reg16,data_reg15,data_reg14,data_reg13,data_reg12,data_reg11,
			data_reg10,data_reg9,data_reg8,data_reg7,data_reg6,data_reg5,data_reg4,
			data_reg3,data_reg2,data_reg1,data_reg0}  ) ) | ( {32{wait_sequence}}  & ( 
			{32{cmd_code0}} & {32{cmd_code1}} & {32{~cmd_code2}} & {32{~cmd_code3}} & 
			{32{~cmd_code4}}  ) & ( {data_reg31,data_reg30,data_reg29,data_reg28,
			data_reg27,data_reg26,data_reg25,data_reg24,data_reg23,data_reg22,data_reg21,
			data_reg20,data_reg19,data_reg18,data_reg17,data_reg16,data_reg15,data_reg14,
			data_reg13,data_reg12,data_reg11,data_reg10,data_reg9,data_reg8,data_reg7,
			data_reg6,data_reg5,data_reg4,data_reg3,data_reg2,data_reg1,data_reg0}  ) ) |
			 ( {32{wait_sequence}}  & ( {32{cmd_code0}} & {32{cmd_code1}} & {32{~
			cmd_code2}} & {32{cmd_code3}} & {32{cmd_code4}}  ) & ( {data_reg31,data_reg30
			,data_reg29,data_reg28,data_reg27,data_reg26,data_reg25,data_reg24,data_reg23
			,data_reg22,data_reg21,data_reg20,data_reg19,data_reg18,data_reg17,data_reg16
			,data_reg15,data_reg14,data_reg13,data_reg12,data_reg11,data_reg10,data_reg9,
			data_reg8,data_reg7,data_reg6,data_reg5,data_reg4,data_reg3,data_reg2,
			data_reg1,data_reg0}  ) ) | ( {32{wait_sequence}}  & ( {32{cmd_code0}} & {32{
			~cmd_code1}} & {32{cmd_code2}} & {32{cmd_code3}} & {32{cmd_code4}}  ) & ( {
			data_reg31,data_reg30,data_reg29,data_reg28,data_reg27,data_reg26,data_reg25,
			data_reg24,data_reg23,data_reg22,data_reg21,data_reg20,data_reg19,data_reg18,
			data_reg17,data_reg16,data_reg15,data_reg14,data_reg13,data_reg12,data_reg11,
			data_reg10,data_reg9,data_reg8,data_reg7,data_reg6,data_reg5,data_reg4,
			data_reg3,data_reg2,data_reg1,data_reg0}  ) ) | ( {32{wait_sequence}}  & ( 
			{32{~cmd_code0}} & {32{~cmd_code1}} & {32{cmd_code2}} & {32{~cmd_code3}} & 
			{32{~cmd_code4}}  ) & ( {data_reg31,data_reg30,data_reg29,data_reg28,
			data_reg27,data_reg26,data_reg25,data_reg24,data_reg23,data_reg22,data_reg21,
			data_reg20,data_reg19,data_reg18,data_reg17,data_reg16,data_reg15,data_reg14,
			data_reg13,data_reg12,data_reg11,data_reg10,data_reg9,data_reg8,data_reg7,
			data_reg6,data_reg5,data_reg4,data_reg3,data_reg2,data_reg1,data_reg0}  ) ) |
			 ( {32{zbit_clr}}  & ( 32'hffffffff ) & ( {data_reg31,data_reg30,data_reg29,
			data_reg28,data_reg27,data_reg26,data_reg25,data_reg24,data_reg23,data_reg22,
			data_reg21,data_reg20,data_reg19,data_reg18,data_reg17,data_reg16,data_reg15,
			data_reg14,data_reg13,data_reg12,data_reg11,data_reg10,data_reg9,data_reg8,
			data_reg7,data_reg6,data_reg5,data_reg4,data_reg3,data_reg2,data_reg1,
			data_reg0}  ) ) | ( {32{zbit_cstr}}  & ( 32'hffffffff ) & ( {data_reg31,
			data_reg30,data_reg29,data_reg28,data_reg27,data_reg26,data_reg25,data_reg24,
			data_reg23,data_reg22,data_reg21,data_reg20,data_reg19,data_reg18,data_reg17,
			data_reg16,data_reg15,data_reg14,data_reg13,data_reg12,data_reg11,data_reg10,
			data_reg9,data_reg8,data_reg7,data_reg6,data_reg5,data_reg4,data_reg3,
			data_reg2,data_reg1,data_reg0}  ) ) | ( {32{zbit_mr}}  & ( 32'hffffffff ) & (
			 {data_reg31,data_reg30,data_reg29,data_reg28,data_reg27,data_reg26,
			data_reg25,data_reg24,data_reg23,data_reg22,data_reg21,data_reg20,data_reg19,
			data_reg18,data_reg17,data_reg16,data_reg15,data_reg14,data_reg13,data_reg12,
			data_reg11,data_reg10,data_reg9,data_reg8,data_reg7,data_reg6,data_reg5,
			data_reg4,data_reg3,data_reg2,data_reg1,data_reg0}  ) ) | ( {32{zbit_rege}}  
			& ( 32'hffffffff ) & ( {data_reg31,data_reg30,data_reg29,data_reg28,
			data_reg27,data_reg26,data_reg25,data_reg24,data_reg23,data_reg22,data_reg21,
			data_reg20,data_reg19,data_reg18,data_reg17,data_reg16,data_reg15,data_reg14,
			data_reg13,data_reg12,data_reg11,data_reg10,data_reg9,data_reg8,data_reg7,
			data_reg6,data_reg5,data_reg4,data_reg3,data_reg2,data_reg1,data_reg0}  ) ) |
			 ( {32{zbit_reve}}  & ( 32'hffffffff ) & ( {data_reg31,data_reg30,data_reg29,
			data_reg28,data_reg27,data_reg26,data_reg25,data_reg24,data_reg23,data_reg22,
			data_reg21,data_reg20,data_reg19,data_reg18,data_reg17,data_reg16,data_reg15,
			data_reg14,data_reg13,data_reg12,data_reg11,data_reg10,data_reg9,data_reg8,
			data_reg7,data_reg6,data_reg5,data_reg4,data_reg3,data_reg2,data_reg1,
			data_reg0}  ) ) | ( {32{zbit_sync}}  & ( 32'hffffffff ) & ( {data_reg31,
			data_reg30,data_reg29,data_reg28,data_reg27,data_reg26,data_reg25,data_reg24,
			data_reg23,data_reg22,data_reg21,data_reg20,data_reg19,data_reg18,data_reg17,
			data_reg16,data_reg15,data_reg14,data_reg13,data_reg12,data_reg11,data_reg10,
			data_reg9,data_reg8,data_reg7,data_reg6,data_reg5,data_reg4,data_reg3,
			data_reg2,data_reg1,data_reg0}  ) ) | ( {32{zbit_tra}}  & ( 32'hffffffff ) & 
			( {data_reg31,data_reg30,data_reg29,data_reg28,data_reg27,data_reg26,
			data_reg25,data_reg24,data_reg23,data_reg22,data_reg21,data_reg20,data_reg19,
			data_reg18,data_reg17,data_reg16,data_reg15,data_reg14,data_reg13,data_reg12,
			data_reg11,data_reg10,data_reg9,data_reg8,data_reg7,data_reg6,data_reg5,
			data_reg4,data_reg3,data_reg2,data_reg1,data_reg0}  ) ) | ( {32{zbit_wccm}}  
			& ( 32'hffffffff ) & ( {data_reg31,data_reg30,data_reg29,data_reg28,
			data_reg27,data_reg26,data_reg25,data_reg24,data_reg23,data_reg22,data_reg21,
			data_reg20,data_reg19,data_reg18,data_reg17,data_reg16,data_reg15,data_reg14,
			data_reg13,data_reg12,data_reg11,data_reg10,data_reg9,data_reg8,data_reg7,
			data_reg6,data_reg5,data_reg4,data_reg3,data_reg2,data_reg1,data_reg0}  ) ) |
			 ( {32{zbit_wcps}}  & ( 32'hffffffff ) & ( {data_reg31,data_reg30,data_reg29,
			data_reg28,data_reg27,data_reg26,data_reg25,data_reg24,data_reg23,data_reg22,
			data_reg21,data_reg20,data_reg19,data_reg18,data_reg17,data_reg16,data_reg15,
			data_reg14,data_reg13,data_reg12,data_reg11,data_reg10,data_reg9,data_reg8,
			data_reg7,data_reg6,data_reg5,data_reg4,data_reg3,data_reg2,data_reg1,
			data_reg0}  ) ) | ( {32{zbit_wcr}}  & ( 32'hffffffff ) & ( {data_reg31,
			data_reg30,data_reg29,data_reg28,data_reg27,data_reg26,data_reg25,data_reg24,
			data_reg23,data_reg22,data_reg21,data_reg20,data_reg19,data_reg18,data_reg17,
			data_reg16,data_reg15,data_reg14,data_reg13,data_reg12,data_reg11,data_reg10,
			data_reg9,data_reg8,data_reg7,data_reg6,data_reg5,data_reg4,data_reg3,
			data_reg2,data_reg1,data_reg0}  ) );

		shift_reg= ( {5{read_event}}  & ( {5{cnt0}} | {5{cnt1}} | {5{cnt2}} | {5{
			cnt3}} | {5{cnt4}} | {5{~cnt5}}  ) & ( {shift_reg4,shift_reg3,shift_reg2,
			shift_reg1,shift_reg0}  ) ) | ( {5{read_event}}  & ( {5{data_reg31}} & {5{~
			cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{cnt5}} | {5{
			data_reg30}} & {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}}
			 & {5{cnt5}} | {5{data_reg29}} & {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~
			cnt3}} & {5{~cnt4}} & {5{cnt5}} | {5{data_reg28}} & {5{~cnt0}} & {5{~cnt1}} &
			 {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{cnt5}} | {5{data_reg27}} & {5{~
			cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{cnt5}} | {5{
			data_reg26}} & {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}}
			 & {5{cnt5}} | {5{data_reg25}} & {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~
			cnt3}} & {5{~cnt4}} & {5{cnt5}} | {5{data_reg24}} & {5{~cnt0}} & {5{~cnt1}} &
			 {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{cnt5}} | {5{data_reg23}} & {5{~
			cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{cnt5}} | {5{
			data_reg22}} & {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}}
			 & {5{cnt5}} | {5{data_reg21}} & {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~
			cnt3}} & {5{~cnt4}} & {5{cnt5}} | {5{data_reg20}} & {5{~cnt0}} & {5{~cnt1}} &
			 {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{cnt5}} | {5{data_reg19}} & {5{~
			cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{cnt5}} | {5{
			data_reg18}} & {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}}
			 & {5{cnt5}} | {5{data_reg17}} & {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~
			cnt3}} & {5{~cnt4}} & {5{cnt5}} | {5{data_reg16}} & {5{~cnt0}} & {5{~cnt1}} &
			 {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{cnt5}} | {5{data_reg15}} & {5{~
			cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{cnt5}} | {5{
			data_reg14}} & {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}}
			 & {5{cnt5}} | {5{data_reg13}} & {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~
			cnt3}} & {5{~cnt4}} & {5{cnt5}} | {5{data_reg12}} & {5{~cnt0}} & {5{~cnt1}} &
			 {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{cnt5}} | {5{data_reg11}} & {5{~
			cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{cnt5}} | {5{
			data_reg10}} & {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}}
			 & {5{cnt5}} | {5{data_reg9}} & {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~
			cnt3}} & {5{~cnt4}} & {5{cnt5}} | {5{data_reg8}} & {5{~cnt0}} & {5{~cnt1}} & 
			{5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{cnt5}} | {5{data_reg7}} & {5{~cnt0
			}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{cnt5}} | {5{
			data_reg6}} & {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} 
			& {5{cnt5}} | {5{data_reg5}} & {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~
			cnt3}} & {5{~cnt4}} & {5{cnt5}} | {5{data_reg4}} & {5{~cnt0}} & {5{~cnt1}} & 
			{5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{cnt5}} | {5{data_reg3}} & {5{~cnt0
			}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{cnt5}} | {5{
			data_reg2}} & {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} 
			& {5{cnt5}} | {5{data_reg1}} & {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~
			cnt3}} & {5{~cnt4}} & {5{cnt5}} | {5{data_reg0}} & {5{~cnt0}} & {5{~cnt1}} & 
			{5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{cnt5}}  ) & ( {shift_reg4,
			shift_reg3,shift_reg2,shift_reg1,shift_reg0}  ) ) | ( {5{read_event}}  & ( 
			{5{~data_reg0}} & {5{~data_reg1}} & {5{~data_reg2}} & {5{~data_reg3}} & {5{~
			data_reg4}} & {5{~data_reg5}} & {5{~data_reg6}} & {5{~data_reg7}} & {5{~
			data_reg8}} & {5{~data_reg9}} & {5{~data_reg10}} & {5{~data_reg11}} & {5{~
			data_reg12}} & {5{~data_reg13}} & {5{~data_reg14}} & {5{~data_reg15}} & {5{~
			data_reg16}} & {5{~data_reg17}} & {5{~data_reg18}} & {5{~data_reg19}} & {5{~
			data_reg20}} & {5{~data_reg21}} & {5{~data_reg22}} & {5{~data_reg23}} & {5{~
			data_reg24}} & {5{~data_reg25}} & {5{~data_reg26}} & {5{~data_reg27}} & {5{~
			data_reg28}} & {5{~data_reg29}} & {5{~data_reg30}} & {5{~data_reg31}} & {5{~
			cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{cnt5}}  ) & (
			 {shift_reg4,shift_reg3,shift_reg2,shift_reg1,shift_reg0}  ) ) | ( {5{
			read_registers}}  & ( {5{cnt0}} | {5{cnt1}} | {5{cnt2}} | {5{cnt3}} | {5{cnt4
			}} | {5{~cnt5}}  ) & ( {shift_reg4,shift_reg3,shift_reg2,shift_reg1,
			shift_reg0}  ) ) | ( {5{read_registers}}  & ( {5{data_reg31}} & {5{~cnt0}} & 
			{5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{cnt5}} | {5{data_reg30
			}} & {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{cnt5
			}} | {5{data_reg29}} & {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & 
			{5{~cnt4}} & {5{cnt5}} | {5{data_reg28}} & {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2
			}} & {5{~cnt3}} & {5{~cnt4}} & {5{cnt5}} | {5{data_reg27}} & {5{~cnt0}} & {5{
			~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{cnt5}} | {5{data_reg26}} 
			& {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{cnt5}} 
			| {5{data_reg25}} & {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~
			cnt4}} & {5{cnt5}} | {5{data_reg24}} & {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} &
			 {5{~cnt3}} & {5{~cnt4}} & {5{cnt5}} | {5{data_reg23}} & {5{~cnt0}} & {5{~
			cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{cnt5}} | {5{data_reg22}} &
			 {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{cnt5}} |
			 {5{data_reg21}} & {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~
			cnt4}} & {5{cnt5}} | {5{data_reg20}} & {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} &
			 {5{~cnt3}} & {5{~cnt4}} & {5{cnt5}} | {5{data_reg19}} & {5{~cnt0}} & {5{~
			cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{cnt5}} | {5{data_reg18}} &
			 {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{cnt5}} |
			 {5{data_reg17}} & {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~
			cnt4}} & {5{cnt5}} | {5{data_reg16}} & {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} &
			 {5{~cnt3}} & {5{~cnt4}} & {5{cnt5}} | {5{data_reg15}} & {5{~cnt0}} & {5{~
			cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{cnt5}} | {5{data_reg14}} &
			 {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{cnt5}} |
			 {5{data_reg13}} & {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~
			cnt4}} & {5{cnt5}} | {5{data_reg12}} & {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} &
			 {5{~cnt3}} & {5{~cnt4}} & {5{cnt5}} | {5{data_reg11}} & {5{~cnt0}} & {5{~
			cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{cnt5}} | {5{data_reg10}} &
			 {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{cnt5}} |
			 {5{data_reg9}} & {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~
			cnt4}} & {5{cnt5}} | {5{data_reg8}} & {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} & 
			{5{~cnt3}} & {5{~cnt4}} & {5{cnt5}} | {5{data_reg7}} & {5{~cnt0}} & {5{~cnt1
			}} & {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{cnt5}} | {5{data_reg6}} & {5{~
			cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{cnt5}} | {5{
			data_reg5}} & {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} 
			& {5{cnt5}} | {5{data_reg4}} & {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~
			cnt3}} & {5{~cnt4}} & {5{cnt5}} | {5{data_reg3}} & {5{~cnt0}} & {5{~cnt1}} & 
			{5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{cnt5}} | {5{data_reg2}} & {5{~cnt0
			}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{cnt5}} | {5{
			data_reg1}} & {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} 
			& {5{cnt5}} | {5{data_reg0}} & {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~
			cnt3}} & {5{~cnt4}} & {5{cnt5}}  ) & ( {shift_reg4,shift_reg3,shift_reg2,
			shift_reg1,shift_reg0}  ) ) | ( {5{read_registers}}  & ( {5{~data_reg0}} & 
			{5{~data_reg1}} & {5{~data_reg2}} & {5{~data_reg3}} & {5{~data_reg4}} & {5{~
			data_reg5}} & {5{~data_reg6}} & {5{~data_reg7}} & {5{~data_reg8}} & {5{~
			data_reg9}} & {5{~data_reg10}} & {5{~data_reg11}} & {5{~data_reg12}} & {5{~
			data_reg13}} & {5{~data_reg14}} & {5{~data_reg15}} & {5{~data_reg16}} & {5{~
			data_reg17}} & {5{~data_reg18}} & {5{~data_reg19}} & {5{~data_reg20}} & {5{~
			data_reg21}} & {5{~data_reg22}} & {5{~data_reg23}} & {5{~data_reg24}} & {5{~
			data_reg25}} & {5{~data_reg26}} & {5{~data_reg27}} & {5{~data_reg28}} & {5{~
			data_reg29}} & {5{~data_reg30}} & {5{~data_reg31}} & {5{~cnt0}} & {5{~cnt1}} 
			& {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{cnt5}}  ) & ( {shift_reg4,
			shift_reg3,shift_reg2,shift_reg1,shift_reg0}  ) ) | ( {5{rescmd_sync}}  & ( 
			5'h1f ) & ( {shift_reg4,shift_reg3,shift_reg2,shift_reg1,shift_reg0}  ) ) | (
			 {5{sbit_clr}}  & ( 5'h1f ) & ( {cmd_code4,cmd_code3,cmd_code2,cmd_code1,
			cmd_code0}  ) ) | ( {5{sbit_cstr}}  & ( 5'h1f ) & ( {shift_reg4,shift_reg3,
			shift_reg2,shift_reg1,shift_reg0}  ) ) | ( {5{sbit_mr}}  & ( 5'h1f ) & ( {
			shift_reg4,shift_reg3,shift_reg2,shift_reg1,shift_reg0}  ) ) | ( {5{sbit_rege
			}}  & ( 5'h1f ) & ( {shift_reg4,shift_reg3,shift_reg2,shift_reg1,shift_reg0} 
			 ) ) | ( {5{sbit_reve}}  & ( 5'h1f ) & ( {shift_reg4,shift_reg3,shift_reg2,
			shift_reg1,shift_reg0}  ) ) | ( {5{sbit_sync}}  & ( 5'h1f ) & ( {shift_reg4,
			shift_reg3,shift_reg2,shift_reg1,shift_reg0}  ) ) | ( {5{sbit_tra}}  & ( 
			5'h1f ) & ( {shift_reg4,shift_reg3,shift_reg2,shift_reg1,shift_reg0}  ) ) | (
			 {5{sbit_wccm}}  & ( 5'h1f ) & ( {shift_reg4,shift_reg3,shift_reg2,shift_reg1
			,shift_reg0}  ) ) | ( {5{sbit_wcps}}  & ( 5'h1f ) & ( {shift_reg4,shift_reg3,
			shift_reg2,shift_reg1,shift_reg0}  ) ) | ( {5{sbit_wcr}}  & ( 5'h1f ) & ( {
			shift_reg4,shift_reg3,shift_reg2,shift_reg1,shift_reg0}  ) ) | ( {5{
			send_add_rege}}  & ( {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~
			cnt4}} & {5{~cnt5}}  ) & ( {shift_reg4,shift_reg3,shift_reg2,shift_reg1,
			shift_reg0}  ) ) | ( {5{send_add_rege}}  & ( {5{cnt5}} | {5{cnt4}} | {5{cnt3
			}} | {5{cnt2}} | {5{cnt1}} | {5{cnt0}}  ) & ( {1'h0,shift_reg4,shift_reg3,
			shift_reg2,shift_reg1}  ) ) | ( {5{send_addr_mr}}  & ( {5{~cnt0}} & {5{~cnt1
			}} & {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{~cnt5}}  ) & ( {shift_reg4,
			shift_reg3,shift_reg2,shift_reg1,shift_reg0}  ) ) | ( {5{send_addr_mr}}  & ( 
			{5{cnt5}} | {5{cnt4}} | {5{cnt3}} | {5{cnt2}} | {5{cnt1}} | {5{cnt0}}  ) & ( 
			{1'h0,shift_reg4,shift_reg3,shift_reg2,shift_reg1}  ) ) | ( {5{send_addr_wccm
			}}  & ( {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{~
			cnt5}}  ) & ( {shift_reg4,shift_reg3,shift_reg2,shift_reg1,shift_reg0}  ) ) |
			 ( {5{send_addr_wccm}}  & ( {5{cnt5}} | {5{cnt4}} | {5{cnt3}} | {5{cnt2}} | 
			{5{cnt1}} | {5{cnt0}}  ) & ( {1'h0,shift_reg4,shift_reg3,shift_reg2,
			shift_reg1}  ) ) | ( {5{send_addr_wcps}}  & ( {5{~cnt0}} & {5{~cnt1}} & {5{~
			cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{~cnt5}}  ) & ( {shift_reg4,shift_reg3,
			shift_reg2,shift_reg1,shift_reg0}  ) ) | ( {5{send_addr_wcps}}  & ( {5{cnt5}}
			 | {5{cnt4}} | {5{cnt3}} | {5{cnt2}} | {5{cnt1}} | {5{cnt0}}  ) & ( {1'h0,
			shift_reg4,shift_reg3,shift_reg2,shift_reg1}  ) ) | ( {5{send_addr_wcr}}  & (
			 {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{~cnt5}} 
			 ) & ( {shift_reg4,shift_reg3,shift_reg2,shift_reg1,shift_reg0}  ) ) | ( {5{
			send_addr_wcr}}  & ( {5{cnt5}} | {5{cnt4}} | {5{cnt3}} | {5{cnt2}} | {5{cnt1
			}} | {5{cnt0}}  ) & ( {1'h0,shift_reg4,shift_reg3,shift_reg2,shift_reg1}  ) )
			 | ( {5{send_cmd_clr}}  & ( {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}}
			 & {5{~cnt4}} & {5{~cnt5}}  ) & ( {shift_reg4,shift_reg3,shift_reg2,
			shift_reg1,shift_reg0}  ) ) | ( {5{send_cmd_clr}}  & ( {5{cnt5}} | {5{cnt4}} 
			| {5{cnt3}} | {5{cnt2}} | {5{cnt1}} | {5{cnt0}}  ) & ( {1'h0,shift_reg4,
			shift_reg3,shift_reg2,shift_reg1}  ) ) | ( {5{send_cmd_cstr}}  & ( {5{~cnt0}}
			 & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{~cnt5}}  ) & ( {
			shift_reg4,shift_reg3,shift_reg2,shift_reg1,shift_reg0}  ) ) | ( {5{
			send_cmd_cstr}}  & ( {5{cnt5}} | {5{cnt4}} | {5{cnt3}} | {5{cnt2}} | {5{cnt1
			}} | {5{cnt0}}  ) & ( {1'h0,shift_reg4,shift_reg3,shift_reg2,shift_reg1}  ) )
			 | ( {5{send_cmd_mr}}  & ( {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} 
			& {5{~cnt4}} & {5{~cnt5}}  ) & ( {chip_addr4,chip_addr3,chip_addr2,chip_addr1
			,chip_addr0}  ) ) | ( {5{send_cmd_mr}}  & ( {5{cnt5}} | {5{cnt4}} | {5{cnt3}}
			 | {5{cnt2}} | {5{cnt1}} | {5{cnt0}}  ) & ( {1'h0,shift_reg4,shift_reg3,
			shift_reg2,shift_reg1}  ) ) | ( {5{send_cmd_reve}}  & ( {5{~cnt0}} & {5{~cnt1
			}} & {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{~cnt5}}  ) & ( {shift_reg4,
			shift_reg3,shift_reg2,shift_reg1,shift_reg0}  ) ) | ( {5{send_cmd_reve}}  & (
			 {5{cnt5}} | {5{cnt4}} | {5{cnt3}} | {5{cnt2}} | {5{cnt1}} | {5{cnt0}}  ) & (
			 {1'h0,shift_reg4,shift_reg3,shift_reg2,shift_reg1}  ) ) | ( {5{send_cmd_sync
			}}  & ( {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{~
			cnt5}}  ) & ( {shift_reg4,shift_reg3,shift_reg2,shift_reg1,shift_reg0}  ) ) |
			 ( {5{send_cmd_sync}}  & ( {5{cnt5}} | {5{cnt4}} | {5{cnt3}} | {5{cnt2}} | 
			{5{cnt1}} | {5{cnt0}}  ) & ( {1'h0,shift_reg4,shift_reg3,shift_reg2,
			shift_reg1}  ) ) | ( {5{send_cmd_tra}}  & ( {5{~cnt0}} & {5{~cnt1}} & {5{~
			cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{~cnt5}}  ) & ( {shift_reg4,shift_reg3,
			shift_reg2,shift_reg1,shift_reg0}  ) ) | ( {5{send_cmd_tra}}  & ( {5{cnt5}} |
			 {5{cnt4}} | {5{cnt3}} | {5{cnt2}} | {5{cnt1}} | {5{cnt0}}  ) & ( {1'h0,
			shift_reg4,shift_reg3,shift_reg2,shift_reg1}  ) ) | ( {5{send_cmd_wccm}}  & (
			 {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{~cnt5}} 
			 ) & ( {chip_addr4,chip_addr3,chip_addr2,chip_addr1,chip_addr0}  ) ) | ( {5{
			send_cmd_wccm}}  & ( {5{cnt5}} | {5{cnt4}} | {5{cnt3}} | {5{cnt2}} | {5{cnt1
			}} | {5{cnt0}}  ) & ( {1'h0,shift_reg4,shift_reg3,shift_reg2,shift_reg1}  ) )
			 | ( {5{send_cmd_wcps}}  & ( {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3
			}} & {5{~cnt4}} & {5{~cnt5}}  ) & ( {shift_reg4,shift_reg3,shift_reg2,
			shift_reg1,shift_reg0}  ) ) | ( {5{send_cmd_wcps}}  & ( {5{cnt5}} | {5{cnt4}}
			 | {5{cnt3}} | {5{cnt2}} | {5{cnt1}} | {5{cnt0}}  ) & ( {1'h0,shift_reg4,
			shift_reg3,shift_reg2,shift_reg1}  ) ) | ( {5{send_cmd_wcr}}  & ( {5{~cnt0}} 
			& {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{~cnt5}}  ) & ( {
			chip_addr4,chip_addr3,chip_addr2,chip_addr1,chip_addr0}  ) ) | ( {5{
			send_cmd_wcr}}  & ( {5{cnt5}} | {5{cnt4}} | {5{cnt3}} | {5{cnt2}} | {5{cnt1}}
			 | {5{cnt0}}  ) & ( {1'h0,shift_reg4,shift_reg3,shift_reg2,shift_reg1}  ) ) |
			 ( {5{send_code_rege}}  & ( {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}}
			 & {5{~cnt4}} & {5{~cnt5}}  ) & ( {chip_addr4,chip_addr3,chip_addr2,
			chip_addr1,chip_addr0}  ) ) | ( {5{send_code_rege}}  & ( {5{cnt5}} | {5{cnt4
			}} | {5{cnt3}} | {5{cnt2}} | {5{cnt1}} | {5{cnt0}}  ) & ( {1'h0,shift_reg4,
			shift_reg3,shift_reg2,shift_reg1}  ) ) | ( {5{send_data1_wccm}}  & ( {5{~cnt0
			}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{~cnt5}}  ) & ( {
			shift_reg4,shift_reg3,shift_reg2,shift_reg1,shift_reg0}  ) ) | ( {5{
			send_data1_wccm}}  & ( {5{cnt5}} | {5{cnt4}} | {5{cnt3}} | {5{cnt2}} | {5{
			cnt1}} | {5{cnt0}}  ) & ( {shift_reg4,shift_reg3,shift_reg2,shift_reg1,
			shift_reg0}  ) ) | ( {5{send_data2_wccm}}  & ( {5{~cnt0}} & {5{~cnt1}} & {5{~
			cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{~cnt5}}  ) & ( {shift_reg4,shift_reg3,
			shift_reg2,shift_reg1,shift_reg0}  ) ) | ( {5{send_data2_wccm}}  & ( {5{cnt5
			}} | {5{cnt4}} | {5{cnt3}} | {5{cnt2}} | {5{cnt1}} | {5{cnt0}}  ) & ( {
			shift_reg4,shift_reg3,shift_reg2,shift_reg1,shift_reg0}  ) ) | ( {5{
			send_data3_wccm}}  & ( {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & 
			{5{~cnt4}} & {5{~cnt5}}  ) & ( {shift_reg4,shift_reg3,shift_reg2,shift_reg1,
			shift_reg0}  ) ) | ( {5{send_data3_wccm}}  & ( {5{cnt5}} | {5{cnt4}} | {5{
			cnt3}} | {5{cnt2}} | {5{cnt1}} | {5{cnt0}}  ) & ( {shift_reg4,shift_reg3,
			shift_reg2,shift_reg1,shift_reg0}  ) ) | ( {5{send_data4_wccm}}  & ( {5{~cnt0
			}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{~cnt5}}  ) & ( {
			shift_reg4,shift_reg3,shift_reg2,shift_reg1,shift_reg0}  ) ) | ( {5{
			send_data4_wccm}}  & ( {5{cnt5}} | {5{cnt4}} | {5{cnt3}} | {5{cnt2}} | {5{
			cnt1}} | {5{cnt0}}  ) & ( {shift_reg4,shift_reg3,shift_reg2,shift_reg1,
			shift_reg0}  ) ) | ( {5{send_data5_wccm}}  & ( {5{~cnt0}} & {5{~cnt1}} & {5{~
			cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{~cnt5}}  ) & ( {shift_reg4,shift_reg3,
			shift_reg2,shift_reg1,shift_reg0}  ) ) | ( {5{send_data5_wccm}}  & ( {5{cnt5
			}} | {5{cnt4}} | {5{cnt3}} | {5{cnt2}} | {5{cnt1}} | {5{cnt0}}  ) & ( {
			shift_reg4,shift_reg3,shift_reg2,shift_reg1,shift_reg0}  ) ) | ( {5{
			send_data6_wccm}}  & ( {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & 
			{5{~cnt4}} & {5{~cnt5}}  ) & ( {shift_reg4,shift_reg3,shift_reg2,shift_reg1,
			shift_reg0}  ) ) | ( {5{send_data6_wccm}}  & ( {5{cnt5}} | {5{cnt4}} | {5{
			cnt3}} | {5{cnt2}} | {5{cnt1}} | {5{cnt0}}  ) & ( {shift_reg4,shift_reg3,
			shift_reg2,shift_reg1,shift_reg0}  ) ) | ( {5{send_data7_wccm}}  & ( {5{~cnt0
			}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{~cnt5}}  ) & ( {
			shift_reg4,shift_reg3,shift_reg2,shift_reg1,shift_reg0}  ) ) | ( {5{
			send_data7_wccm}}  & ( {5{cnt5}} | {5{cnt4}} | {5{cnt3}} | {5{cnt2}} | {5{
			cnt1}} | {5{cnt0}}  ) & ( {shift_reg4,shift_reg3,shift_reg2,shift_reg1,
			shift_reg0}  ) ) | ( {5{send_data_tra}}  & ( {5{~cnt0}} & {5{~cnt1}} & {5{~
			cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{~cnt5}}  ) & ( {shift_reg4,shift_reg3,
			shift_reg2,shift_reg1,shift_reg0}  ) ) | ( {5{send_data_tra}}  & ( {5{cnt5}} 
			| {5{cnt4}} | {5{cnt3}} | {5{cnt2}} | {5{cnt1}} | {5{cnt0}}  ) & ( {
			shift_reg4,shift_reg3,shift_reg2,shift_reg1,shift_reg0}  ) ) | ( {5{
			send_data_wccm}}  & ( {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{
			~cnt4}} & {5{~cnt5}}  ) & ( {shift_reg4,shift_reg3,shift_reg2,shift_reg1,
			shift_reg0}  ) ) | ( {5{send_data_wccm}}  & ( {5{cnt5}} | {5{cnt4}} | {5{cnt3
			}} | {5{cnt2}} | {5{cnt1}} | {5{cnt0}}  ) & ( {shift_reg4,shift_reg3,
			shift_reg2,shift_reg1,shift_reg0}  ) ) | ( {5{send_data_wcps}}  & ( {5{~cnt0
			}} & {5{~cnt1}} & {5{~cnt2}} & {5{~cnt3}} & {5{~cnt4}} & {5{~cnt5}}  ) & ( {
			shift_reg4,shift_reg3,shift_reg2,shift_reg1,shift_reg0}  ) ) | ( {5{
			send_data_wcps}}  & ( {5{cnt5}} | {5{cnt4}} | {5{cnt3}} | {5{cnt2}} | {5{cnt1
			}} | {5{cnt0}}  ) & ( {shift_reg4,shift_reg3,shift_reg2,shift_reg1,shift_reg0
			}  ) ) | ( {5{send_data_wcr}}  & ( {5{~cnt0}} & {5{~cnt1}} & {5{~cnt2}} & {5{
			~cnt3}} & {5{~cnt4}} & {5{~cnt5}}  ) & ( {shift_reg4,shift_reg3,shift_reg2,
			shift_reg1,shift_reg0}  ) ) | ( {5{send_data_wcr}}  & ( {5{cnt5}} | {5{cnt4}}
			 | {5{cnt3}} | {5{cnt2}} | {5{cnt1}} | {5{cnt0}}  ) & ( {shift_reg4,
			shift_reg3,shift_reg2,shift_reg1,shift_reg0}  ) ) | ( {5{wait_sbit_rege}}  & 
			( {5{atom_data}}  ) & ( {shift_reg4,shift_reg3,shift_reg2,shift_reg1,
			shift_reg0}  ) ) | ( {5{wait_sbit_rege}}  & ( {5{~atom_data}}  ) & ( {
			shift_reg4,shift_reg3,shift_reg2,shift_reg1,shift_reg0}  ) ) | ( {5{
			wait_sbit_reve}}  & ( {5{atom_data}}  ) & ( {shift_reg4,shift_reg3,shift_reg2
			,shift_reg1,shift_reg0}  ) ) | ( {5{wait_sbit_reve}}  & ( {5{~atom_data}}  ) 
			& ( {shift_reg4,shift_reg3,shift_reg2,shift_reg1,shift_reg0}  ) ) | ( {5{
			wait_sequence}}  & ( {5{cmd_code0}} & {5{cmd_code1}} & {5{cmd_code2}} | {5{~
			cmd_code2}} & {5{~cmd_code1}} & {5{cmd_code3}} | {5{~cmd_code2}} & {5{~
			cmd_code1}} & {5{cmd_code4}} | {5{~cmd_code3}} & {5{cmd_code2}} & {5{
			cmd_code1}} | {5{~cmd_code4}} & {5{cmd_code2}} & {5{cmd_code1}} | {5{~
			cmd_code2}} & {5{~cmd_code1}} & {5{~cmd_code0}} | {5{~cmd_code4}} & {5{
			cmd_code3}} | {5{~cmd_code3}} & {5{cmd_code4}}  ) & ( {shift_reg4,shift_reg3,
			shift_reg2,shift_reg1,shift_reg0}  ) ) | ( {5{wait_sequence}}  & ( {5{~
			cmd_code0}} & {5{~cmd_code1}} & {5{cmd_code2}} & {5{cmd_code3}} & {5{
			cmd_code4}}  ) & ( {cmd_code4,cmd_code3,cmd_code2,cmd_code1,cmd_code0}  ) ) |
			 ( {5{wait_sequence}}  & ( {5{~cmd_code0}} & {5{cmd_code1}} & {5{cmd_code2}} 
			& {5{cmd_code3}} & {5{cmd_code4}}  ) & ( {cmd_code4,cmd_code3,cmd_code2,
			cmd_code1,cmd_code0}  ) ) | ( {5{wait_sequence}}  & ( {5{~cmd_code0}} & {5{
			cmd_code1}} & {5{~cmd_code2}} & {5{cmd_code3}} & {5{cmd_code4}}  ) & ( {
			cmd_code4,cmd_code3,cmd_code2,cmd_code1,cmd_code0}  ) ) | ( {5{wait_sequence
			}}  & ( {5{cmd_code0}} & {5{~cmd_code1}} & {5{cmd_code2}} & {5{~cmd_code3}} &
			 {5{~cmd_code4}}  ) & ( {cmd_code4,cmd_code3,cmd_code2,cmd_code1,cmd_code0}  
			) ) | ( {5{wait_sequence}}  & ( {5{~cmd_code0}} & {5{cmd_code1}} & {5{~
			cmd_code2}} & {5{~cmd_code3}} & {5{~cmd_code4}}  ) & ( 'h15 ) ) | ( {5{
			wait_sequence}}  & ( {5{cmd_code0}} & {5{~cmd_code1}} & {5{~cmd_code2}} & {5{
			~cmd_code3}} & {5{~cmd_code4}}  ) & ( {cmd_code4,cmd_code3,cmd_code2,
			cmd_code1,cmd_code0}  ) ) | ( {5{wait_sequence}}  & ( {5{cmd_code0}} & {5{
			cmd_code1}} & {5{~cmd_code2}} & {5{~cmd_code3}} & {5{~cmd_code4}}  ) & ( {
			cmd_code4,cmd_code3,cmd_code2,cmd_code1,cmd_code0}  ) ) | ( {5{wait_sequence
			}}  & ( {5{cmd_code0}} & {5{cmd_code1}} & {5{~cmd_code2}} & {5{cmd_code3}} & 
			{5{cmd_code4}}  ) & ( {cmd_code4,cmd_code3,cmd_code2,cmd_code1,cmd_code0}  ) 
			) | ( {5{wait_sequence}}  & ( {5{cmd_code0}} & {5{~cmd_code1}} & {5{cmd_code2
			}} & {5{cmd_code3}} & {5{cmd_code4}}  ) & ( {cmd_code4,cmd_code3,cmd_code2,
			cmd_code1,cmd_code0}  ) ) | ( {5{wait_sequence}}  & ( {5{~cmd_code0}} & {5{~
			cmd_code1}} & {5{cmd_code2}} & {5{~cmd_code3}} & {5{~cmd_code4}}  ) & ( {
			cmd_code4,cmd_code3,cmd_code2,cmd_code1,cmd_code0}  ) ) | ( {5{zbit_clr}}  & 
			( 5'h1f ) & ( {shift_reg4,shift_reg3,shift_reg2,shift_reg1,shift_reg0}  ) ) |
			 ( {5{zbit_cstr}}  & ( 5'h1f ) & ( {shift_reg4,shift_reg3,shift_reg2,
			shift_reg1,shift_reg0}  ) ) | ( {5{zbit_mr}}  & ( 5'h1f ) & ( {shift_reg4,
			shift_reg3,shift_reg2,shift_reg1,shift_reg0}  ) ) | ( {5{zbit_rege}}  & ( 
			5'h1f ) & ( {shift_reg4,shift_reg3,shift_reg2,shift_reg1,shift_reg0}  ) ) | (
			 {5{zbit_reve}}  & ( 5'h1f ) & ( {shift_reg4,shift_reg3,shift_reg2,shift_reg1
			,shift_reg0}  ) ) | ( {5{zbit_sync}}  & ( 5'h1f ) & ( {shift_reg4,shift_reg3,
			shift_reg2,shift_reg1,shift_reg0}  ) ) | ( {5{zbit_tra}}  & ( 5'h1f ) & ( {
			shift_reg4,shift_reg3,shift_reg2,shift_reg1,shift_reg0}  ) ) | ( {5{zbit_wccm
			}}  & ( 5'h1f ) & ( {shift_reg4,shift_reg3,shift_reg2,shift_reg1,shift_reg0} 
			 ) ) | ( {5{zbit_wcps}}  & ( 5'h1f ) & ( {shift_reg4,shift_reg3,shift_reg2,
			shift_reg1,shift_reg0}  ) ) | ( {5{zbit_wcr}}  & ( 5'h1f ) & ( {shift_reg4,
			shift_reg3,shift_reg2,shift_reg1,shift_reg0}  ) );

		temp_cmd= ( {2{read_event}}  & ( {2{cnt0}} | {2{cnt1}} | {2{cnt2}} | {2{
			cnt3}} | {2{cnt4}} | {2{~cnt5}}  ) & ( {temp_cmd1,temp_cmd0}  ) ) | ( {2{
			read_event}}  & ( {2{data_reg31}} & {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & 
			{2{~cnt3}} & {2{~cnt4}} & {2{cnt5}} | {2{data_reg30}} & {2{~cnt0}} & {2{~cnt1
			}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{cnt5}} | {2{data_reg29}} & {2{
			~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{cnt5}} | {2{
			data_reg28}} & {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}}
			 & {2{cnt5}} | {2{data_reg27}} & {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~
			cnt3}} & {2{~cnt4}} & {2{cnt5}} | {2{data_reg26}} & {2{~cnt0}} & {2{~cnt1}} &
			 {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{cnt5}} | {2{data_reg25}} & {2{~
			cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{cnt5}} | {2{
			data_reg24}} & {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}}
			 & {2{cnt5}} | {2{data_reg23}} & {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~
			cnt3}} & {2{~cnt4}} & {2{cnt5}} | {2{data_reg22}} & {2{~cnt0}} & {2{~cnt1}} &
			 {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{cnt5}} | {2{data_reg21}} & {2{~
			cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{cnt5}} | {2{
			data_reg20}} & {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}}
			 & {2{cnt5}} | {2{data_reg19}} & {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~
			cnt3}} & {2{~cnt4}} & {2{cnt5}} | {2{data_reg18}} & {2{~cnt0}} & {2{~cnt1}} &
			 {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{cnt5}} | {2{data_reg17}} & {2{~
			cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{cnt5}} | {2{
			data_reg16}} & {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}}
			 & {2{cnt5}} | {2{data_reg15}} & {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~
			cnt3}} & {2{~cnt4}} & {2{cnt5}} | {2{data_reg14}} & {2{~cnt0}} & {2{~cnt1}} &
			 {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{cnt5}} | {2{data_reg13}} & {2{~
			cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{cnt5}} | {2{
			data_reg12}} & {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}}
			 & {2{cnt5}} | {2{data_reg11}} & {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~
			cnt3}} & {2{~cnt4}} & {2{cnt5}} | {2{data_reg10}} & {2{~cnt0}} & {2{~cnt1}} &
			 {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{cnt5}} | {2{data_reg9}} & {2{~cnt0
			}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{cnt5}} | {2{
			data_reg8}} & {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} 
			& {2{cnt5}} | {2{data_reg7}} & {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~
			cnt3}} & {2{~cnt4}} & {2{cnt5}} | {2{data_reg6}} & {2{~cnt0}} & {2{~cnt1}} & 
			{2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{cnt5}} | {2{data_reg5}} & {2{~cnt0
			}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{cnt5}} | {2{
			data_reg4}} & {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} 
			& {2{cnt5}} | {2{data_reg3}} & {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~
			cnt3}} & {2{~cnt4}} & {2{cnt5}} | {2{data_reg2}} & {2{~cnt0}} & {2{~cnt1}} & 
			{2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{cnt5}} | {2{data_reg1}} & {2{~cnt0
			}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{cnt5}} | {2{
			data_reg0}} & {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} 
			& {2{cnt5}}  ) & ( {temp_cmd1,temp_cmd0}  ) ) | ( {2{read_event}}  & ( {2{~
			data_reg0}} & {2{~data_reg1}} & {2{~data_reg2}} & {2{~data_reg3}} & {2{~
			data_reg4}} & {2{~data_reg5}} & {2{~data_reg6}} & {2{~data_reg7}} & {2{~
			data_reg8}} & {2{~data_reg9}} & {2{~data_reg10}} & {2{~data_reg11}} & {2{~
			data_reg12}} & {2{~data_reg13}} & {2{~data_reg14}} & {2{~data_reg15}} & {2{~
			data_reg16}} & {2{~data_reg17}} & {2{~data_reg18}} & {2{~data_reg19}} & {2{~
			data_reg20}} & {2{~data_reg21}} & {2{~data_reg22}} & {2{~data_reg23}} & {2{~
			data_reg24}} & {2{~data_reg25}} & {2{~data_reg26}} & {2{~data_reg27}} & {2{~
			data_reg28}} & {2{~data_reg29}} & {2{~data_reg30}} & {2{~data_reg31}} & {2{~
			cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{cnt5}}  ) & (
			 {temp_cmd1,temp_cmd0}  ) ) | ( {2{read_registers}}  & ( {2{cnt0}} | {2{cnt1
			}} | {2{cnt2}} | {2{cnt3}} | {2{cnt4}} | {2{~cnt5}}  ) & ( {temp_cmd1,
			temp_cmd0}  ) ) | ( {2{read_registers}}  & ( {2{data_reg31}} & {2{~cnt0}} & 
			{2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{cnt5}} | {2{data_reg30
			}} & {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{cnt5
			}} | {2{data_reg29}} & {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & 
			{2{~cnt4}} & {2{cnt5}} | {2{data_reg28}} & {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2
			}} & {2{~cnt3}} & {2{~cnt4}} & {2{cnt5}} | {2{data_reg27}} & {2{~cnt0}} & {2{
			~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{cnt5}} | {2{data_reg26}} 
			& {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{cnt5}} 
			| {2{data_reg25}} & {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~
			cnt4}} & {2{cnt5}} | {2{data_reg24}} & {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} &
			 {2{~cnt3}} & {2{~cnt4}} & {2{cnt5}} | {2{data_reg23}} & {2{~cnt0}} & {2{~
			cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{cnt5}} | {2{data_reg22}} &
			 {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{cnt5}} |
			 {2{data_reg21}} & {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~
			cnt4}} & {2{cnt5}} | {2{data_reg20}} & {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} &
			 {2{~cnt3}} & {2{~cnt4}} & {2{cnt5}} | {2{data_reg19}} & {2{~cnt0}} & {2{~
			cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{cnt5}} | {2{data_reg18}} &
			 {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{cnt5}} |
			 {2{data_reg17}} & {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~
			cnt4}} & {2{cnt5}} | {2{data_reg16}} & {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} &
			 {2{~cnt3}} & {2{~cnt4}} & {2{cnt5}} | {2{data_reg15}} & {2{~cnt0}} & {2{~
			cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{cnt5}} | {2{data_reg14}} &
			 {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{cnt5}} |
			 {2{data_reg13}} & {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~
			cnt4}} & {2{cnt5}} | {2{data_reg12}} & {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} &
			 {2{~cnt3}} & {2{~cnt4}} & {2{cnt5}} | {2{data_reg11}} & {2{~cnt0}} & {2{~
			cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{cnt5}} | {2{data_reg10}} &
			 {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{cnt5}} |
			 {2{data_reg9}} & {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~
			cnt4}} & {2{cnt5}} | {2{data_reg8}} & {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & 
			{2{~cnt3}} & {2{~cnt4}} & {2{cnt5}} | {2{data_reg7}} & {2{~cnt0}} & {2{~cnt1
			}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{cnt5}} | {2{data_reg6}} & {2{~
			cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{cnt5}} | {2{
			data_reg5}} & {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} 
			& {2{cnt5}} | {2{data_reg4}} & {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~
			cnt3}} & {2{~cnt4}} & {2{cnt5}} | {2{data_reg3}} & {2{~cnt0}} & {2{~cnt1}} & 
			{2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{cnt5}} | {2{data_reg2}} & {2{~cnt0
			}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{cnt5}} | {2{
			data_reg1}} & {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} 
			& {2{cnt5}} | {2{data_reg0}} & {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~
			cnt3}} & {2{~cnt4}} & {2{cnt5}}  ) & ( {temp_cmd1,temp_cmd0}  ) ) | ( {2{
			read_registers}}  & ( {2{~data_reg0}} & {2{~data_reg1}} & {2{~data_reg2}} & 
			{2{~data_reg3}} & {2{~data_reg4}} & {2{~data_reg5}} & {2{~data_reg6}} & {2{~
			data_reg7}} & {2{~data_reg8}} & {2{~data_reg9}} & {2{~data_reg10}} & {2{~
			data_reg11}} & {2{~data_reg12}} & {2{~data_reg13}} & {2{~data_reg14}} & {2{~
			data_reg15}} & {2{~data_reg16}} & {2{~data_reg17}} & {2{~data_reg18}} & {2{~
			data_reg19}} & {2{~data_reg20}} & {2{~data_reg21}} & {2{~data_reg22}} & {2{~
			data_reg23}} & {2{~data_reg24}} & {2{~data_reg25}} & {2{~data_reg26}} & {2{~
			data_reg27}} & {2{~data_reg28}} & {2{~data_reg29}} & {2{~data_reg30}} & {2{~
			data_reg31}} & {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}}
			 & {2{cnt5}}  ) & ( {temp_cmd1,temp_cmd0}  ) ) | ( {2{rescmd_sync}}  & ( 2'h3
			 ) & ( {temp_cmd1,temp_cmd0}  ) ) | ( {2{sbit_clr}}  & ( 2'h3 ) & ( {
			temp_cmd1,temp_cmd0}  ) ) | ( {2{sbit_cstr}}  & ( 2'h3 ) & ( {temp_cmd1,
			temp_cmd0}  ) ) | ( {2{sbit_mr}}  & ( 2'h3 ) & ( {temp_cmd1,temp_cmd0}  ) ) |
			 ( {2{sbit_rege}}  & ( 2'h3 ) & ( {temp_cmd1,temp_cmd0}  ) ) | ( {2{sbit_reve
			}}  & ( 2'h3 ) & ( {temp_cmd1,temp_cmd0}  ) ) | ( {2{sbit_sync}}  & ( 2'h3 ) 
			& ( {temp_cmd1,temp_cmd0}  ) ) | ( {2{sbit_tra}}  & ( 2'h3 ) & ( {temp_cmd1,
			temp_cmd0}  ) ) | ( {2{sbit_wccm}}  & ( 2'h3 ) & ( {temp_cmd1,temp_cmd0}  ) )
			 | ( {2{sbit_wcps}}  & ( 2'h3 ) & ( {temp_cmd1,temp_cmd0}  ) ) | ( {2{
			sbit_wcr}}  & ( 2'h3 ) & ( {temp_cmd1,temp_cmd0}  ) ) | ( {2{send_add_rege}} 
			 & ( {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{~
			cnt5}}  ) & ( {temp_cmd1,temp_cmd0}  ) ) | ( {2{send_add_rege}}  & ( {2{cnt5
			}} | {2{cnt4}} | {2{cnt3}} | {2{cnt2}} | {2{cnt1}} | {2{cnt0}}  ) & ( {
			temp_cmd1,temp_cmd0}  ) ) | ( {2{send_addr_mr}}  & ( {2{~cnt0}} & {2{~cnt1}} 
			& {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{~cnt5}}  ) & ( {temp_cmd1,
			temp_cmd0}  ) ) | ( {2{send_addr_mr}}  & ( {2{cnt5}} | {2{cnt4}} | {2{cnt3}} 
			| {2{cnt2}} | {2{cnt1}} | {2{cnt0}}  ) & ( {temp_cmd1,temp_cmd0}  ) ) | ( {2{
			send_addr_wccm}}  & ( {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{
			~cnt4}} & {2{~cnt5}}  ) & ( {temp_cmd1,temp_cmd0}  ) ) | ( {2{send_addr_wccm
			}}  & ( {2{cnt5}} | {2{cnt4}} | {2{cnt3}} | {2{cnt2}} | {2{cnt1}} | {2{cnt0}}
			  ) & ( {temp_cmd1,temp_cmd0}  ) ) | ( {2{send_addr_wcps}}  & ( {2{~cnt0}} & 
			{2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{~cnt5}}  ) & ( {
			temp_cmd1,temp_cmd0}  ) ) | ( {2{send_addr_wcps}}  & ( {2{cnt5}} | {2{cnt4}} 
			| {2{cnt3}} | {2{cnt2}} | {2{cnt1}} | {2{cnt0}}  ) & ( {temp_cmd1,temp_cmd0} 
			 ) ) | ( {2{send_addr_wcr}}  & ( {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~
			cnt3}} & {2{~cnt4}} & {2{~cnt5}}  ) & ( {temp_cmd1,temp_cmd0}  ) ) | ( {2{
			send_addr_wcr}}  & ( {2{cnt5}} | {2{cnt4}} | {2{cnt3}} | {2{cnt2}} | {2{cnt1
			}} | {2{cnt0}}  ) & ( {temp_cmd1,temp_cmd0}  ) ) | ( {2{send_cmd_clr}}  & ( 
			{2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{~cnt5}}  
			) & ( {temp_cmd1,temp_cmd0}  ) ) | ( {2{send_cmd_clr}}  & ( {2{cnt5}} | {2{
			cnt4}} | {2{cnt3}} | {2{cnt2}} | {2{cnt1}} | {2{cnt0}}  ) & ( {temp_cmd1,
			temp_cmd0}  ) ) | ( {2{send_cmd_cstr}}  & ( {2{~cnt0}} & {2{~cnt1}} & {2{~
			cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{~cnt5}}  ) & ( {temp_cmd1,temp_cmd0}  )
			 ) | ( {2{send_cmd_cstr}}  & ( {2{cnt5}} | {2{cnt4}} | {2{cnt3}} | {2{cnt2}} 
			| {2{cnt1}} | {2{cnt0}}  ) & ( {temp_cmd1,temp_cmd0}  ) ) | ( {2{send_cmd_mr
			}}  & ( {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{~
			cnt5}}  ) & ( {temp_cmd1,temp_cmd0}  ) ) | ( {2{send_cmd_mr}}  & ( {2{cnt5}} 
			| {2{cnt4}} | {2{cnt3}} | {2{cnt2}} | {2{cnt1}} | {2{cnt0}}  ) & ( {temp_cmd1
			,temp_cmd0}  ) ) | ( {2{send_cmd_reve}}  & ( {2{~cnt0}} & {2{~cnt1}} & {2{~
			cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{~cnt5}}  ) & ( {temp_cmd1,temp_cmd0}  )
			 ) | ( {2{send_cmd_reve}}  & ( {2{cnt5}} | {2{cnt4}} | {2{cnt3}} | {2{cnt2}} 
			| {2{cnt1}} | {2{cnt0}}  ) & ( {temp_cmd1,temp_cmd0}  ) ) | ( {2{
			send_cmd_sync}}  & ( {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~
			cnt4}} & {2{~cnt5}}  ) & ( 'h0 ) ) | ( {2{send_cmd_sync}}  & ( {2{cnt5}} | 
			{2{cnt4}} | {2{cnt3}} | {2{cnt2}} | {2{cnt1}} | {2{cnt0}}  ) & ( {temp_cmd1,
			temp_cmd0}  ) ) | ( {2{send_cmd_tra}}  & ( {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2
			}} & {2{~cnt3}} & {2{~cnt4}} & {2{~cnt5}}  ) & ( {temp_cmd1,temp_cmd0}  ) ) |
			 ( {2{send_cmd_tra}}  & ( {2{cnt5}} | {2{cnt4}} | {2{cnt3}} | {2{cnt2}} | {2{
			cnt1}} | {2{cnt0}}  ) & ( {temp_cmd1,temp_cmd0}  ) ) | ( {2{send_cmd_wccm}}  
			& ( {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{~cnt5
			}}  ) & ( {temp_cmd1,temp_cmd0}  ) ) | ( {2{send_cmd_wccm}}  & ( {2{cnt5}} | 
			{2{cnt4}} | {2{cnt3}} | {2{cnt2}} | {2{cnt1}} | {2{cnt0}}  ) & ( {temp_cmd1,
			temp_cmd0}  ) ) | ( {2{send_cmd_wcps}}  & ( {2{~cnt0}} & {2{~cnt1}} & {2{~
			cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{~cnt5}}  ) & ( {temp_cmd1,temp_cmd0}  )
			 ) | ( {2{send_cmd_wcps}}  & ( {2{cnt5}} | {2{cnt4}} | {2{cnt3}} | {2{cnt2}} 
			| {2{cnt1}} | {2{cnt0}}  ) & ( {temp_cmd1,temp_cmd0}  ) ) | ( {2{send_cmd_wcr
			}}  & ( {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{~
			cnt5}}  ) & ( {temp_cmd1,temp_cmd0}  ) ) | ( {2{send_cmd_wcr}}  & ( {2{cnt5}}
			 | {2{cnt4}} | {2{cnt3}} | {2{cnt2}} | {2{cnt1}} | {2{cnt0}}  ) & ( {
			temp_cmd1,temp_cmd0}  ) ) | ( {2{send_code_rege}}  & ( {2{~cnt0}} & {2{~cnt1
			}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{~cnt5}}  ) & ( {temp_cmd1,
			temp_cmd0}  ) ) | ( {2{send_code_rege}}  & ( {2{cnt5}} | {2{cnt4}} | {2{cnt3
			}} | {2{cnt2}} | {2{cnt1}} | {2{cnt0}}  ) & ( {temp_cmd1,temp_cmd0}  ) ) | ( 
			{2{send_data1_wccm}}  & ( {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} &
			 {2{~cnt4}} & {2{~cnt5}}  ) & ( {temp_cmd1,temp_cmd0}  ) ) | ( {2{
			send_data1_wccm}}  & ( {2{cnt5}} | {2{cnt4}} | {2{cnt3}} | {2{cnt2}} | {2{
			cnt1}} | {2{cnt0}}  ) & ( {temp_cmd1,temp_cmd0}  ) ) | ( {2{send_data2_wccm}}
			  & ( {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{~
			cnt5}}  ) & ( {temp_cmd1,temp_cmd0}  ) ) | ( {2{send_data2_wccm}}  & ( {2{
			cnt5}} | {2{cnt4}} | {2{cnt3}} | {2{cnt2}} | {2{cnt1}} | {2{cnt0}}  ) & ( {
			temp_cmd1,temp_cmd0}  ) ) | ( {2{send_data3_wccm}}  & ( {2{~cnt0}} & {2{~cnt1
			}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{~cnt5}}  ) & ( {temp_cmd1,
			temp_cmd0}  ) ) | ( {2{send_data3_wccm}}  & ( {2{cnt5}} | {2{cnt4}} | {2{cnt3
			}} | {2{cnt2}} | {2{cnt1}} | {2{cnt0}}  ) & ( {temp_cmd1,temp_cmd0}  ) ) | ( 
			{2{send_data4_wccm}}  & ( {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} &
			 {2{~cnt4}} & {2{~cnt5}}  ) & ( {temp_cmd1,temp_cmd0}  ) ) | ( {2{
			send_data4_wccm}}  & ( {2{cnt5}} | {2{cnt4}} | {2{cnt3}} | {2{cnt2}} | {2{
			cnt1}} | {2{cnt0}}  ) & ( {temp_cmd1,temp_cmd0}  ) ) | ( {2{send_data5_wccm}}
			  & ( {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{~
			cnt5}}  ) & ( {temp_cmd1,temp_cmd0}  ) ) | ( {2{send_data5_wccm}}  & ( {2{
			cnt5}} | {2{cnt4}} | {2{cnt3}} | {2{cnt2}} | {2{cnt1}} | {2{cnt0}}  ) & ( {
			temp_cmd1,temp_cmd0}  ) ) | ( {2{send_data6_wccm}}  & ( {2{~cnt0}} & {2{~cnt1
			}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{~cnt5}}  ) & ( {temp_cmd1,
			temp_cmd0}  ) ) | ( {2{send_data6_wccm}}  & ( {2{cnt5}} | {2{cnt4}} | {2{cnt3
			}} | {2{cnt2}} | {2{cnt1}} | {2{cnt0}}  ) & ( {temp_cmd1,temp_cmd0}  ) ) | ( 
			{2{send_data7_wccm}}  & ( {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} &
			 {2{~cnt4}} & {2{~cnt5}}  ) & ( {temp_cmd1,temp_cmd0}  ) ) | ( {2{
			send_data7_wccm}}  & ( {2{cnt5}} | {2{cnt4}} | {2{cnt3}} | {2{cnt2}} | {2{
			cnt1}} | {2{cnt0}}  ) & ( {temp_cmd1,temp_cmd0}  ) ) | ( {2{send_data_tra}}  
			& ( {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{~cnt5
			}}  ) & ( {temp_cmd1,temp_cmd0}  ) ) | ( {2{send_data_tra}}  & ( {2{cnt5}} | 
			{2{cnt4}} | {2{cnt3}} | {2{cnt2}} | {2{cnt1}} | {2{cnt0}}  ) & ( {temp_cmd1,
			temp_cmd0}  ) ) | ( {2{send_data_wccm}}  & ( {2{~cnt0}} & {2{~cnt1}} & {2{~
			cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{~cnt5}}  ) & ( {temp_cmd1,temp_cmd0}  )
			 ) | ( {2{send_data_wccm}}  & ( {2{cnt5}} | {2{cnt4}} | {2{cnt3}} | {2{cnt2}}
			 | {2{cnt1}} | {2{cnt0}}  ) & ( {temp_cmd1,temp_cmd0}  ) ) | ( {2{
			send_data_wcps}}  & ( {2{~cnt0}} & {2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{
			~cnt4}} & {2{~cnt5}}  ) & ( {temp_cmd1,temp_cmd0}  ) ) | ( {2{send_data_wcps
			}}  & ( {2{cnt5}} | {2{cnt4}} | {2{cnt3}} | {2{cnt2}} | {2{cnt1}} | {2{cnt0}}
			  ) & ( {temp_cmd1,temp_cmd0}  ) ) | ( {2{send_data_wcr}}  & ( {2{~cnt0}} & 
			{2{~cnt1}} & {2{~cnt2}} & {2{~cnt3}} & {2{~cnt4}} & {2{~cnt5}}  ) & ( {
			temp_cmd1,temp_cmd0}  ) ) | ( {2{send_data_wcr}}  & ( {2{cnt5}} | {2{cnt4}} |
			 {2{cnt3}} | {2{cnt2}} | {2{cnt1}} | {2{cnt0}}  ) & ( {temp_cmd1,temp_cmd0}  
			) ) | ( {2{wait_sbit_rege}}  & ( {2{atom_data}}  ) & ( {temp_cmd1,temp_cmd0} 
			 ) ) | ( {2{wait_sbit_rege}}  & ( {2{~atom_data}}  ) & ( {temp_cmd1,temp_cmd0
			}  ) ) | ( {2{wait_sbit_reve}}  & ( {2{atom_data}}  ) & ( {temp_cmd1,
			temp_cmd0}  ) ) | ( {2{wait_sbit_reve}}  & ( {2{~atom_data}}  ) & ( {
			temp_cmd1,temp_cmd0}  ) ) | ( {2{wait_sequence}}  & ( {2{cmd_code0}} & {2{
			cmd_code1}} & {2{cmd_code2}} | {2{~cmd_code2}} & {2{~cmd_code1}} & {2{
			cmd_code3}} | {2{~cmd_code2}} & {2{~cmd_code1}} & {2{cmd_code4}} | {2{~
			cmd_code3}} & {2{cmd_code2}} & {2{cmd_code1}} | {2{~cmd_code4}} & {2{
			cmd_code2}} & {2{cmd_code1}} | {2{~cmd_code2}} & {2{~cmd_code1}} & {2{~
			cmd_code0}} | {2{~cmd_code4}} & {2{cmd_code3}} | {2{~cmd_code3}} & {2{
			cmd_code4}}  ) & ( {temp_cmd1,temp_cmd0}  ) ) | ( {2{wait_sequence}}  & ( {2{
			~cmd_code0}} & {2{~cmd_code1}} & {2{cmd_code2}} & {2{cmd_code3}} & {2{
			cmd_code4}}  ) & ( {temp_cmd1,temp_cmd0}  ) ) | ( {2{wait_sequence}}  & ( {2{
			~cmd_code0}} & {2{cmd_code1}} & {2{cmd_code2}} & {2{cmd_code3}} & {2{
			cmd_code4}}  ) & ( {temp_cmd1,temp_cmd0}  ) ) | ( {2{wait_sequence}}  & ( {2{
			~cmd_code0}} & {2{cmd_code1}} & {2{~cmd_code2}} & {2{cmd_code3}} & {2{
			cmd_code4}}  ) & ( {temp_cmd1,temp_cmd0}  ) ) | ( {2{wait_sequence}}  & ( {2{
			cmd_code0}} & {2{~cmd_code1}} & {2{cmd_code2}} & {2{~cmd_code3}} & {2{~
			cmd_code4}}  ) & ( {temp_cmd1,temp_cmd0}  ) ) | ( {2{wait_sequence}}  & ( {2{
			~cmd_code0}} & {2{cmd_code1}} & {2{~cmd_code2}} & {2{~cmd_code3}} & {2{~
			cmd_code4}}  ) & ( 'h0 ) ) | ( {2{wait_sequence}}  & ( {2{cmd_code0}} & {2{~
			cmd_code1}} & {2{~cmd_code2}} & {2{~cmd_code3}} & {2{~cmd_code4}}  ) & ( {
			temp_cmd1,temp_cmd0}  ) ) | ( {2{wait_sequence}}  & ( {2{cmd_code0}} & {2{
			cmd_code1}} & {2{~cmd_code2}} & {2{~cmd_code3}} & {2{~cmd_code4}}  ) & ( {
			temp_cmd1,temp_cmd0}  ) ) | ( {2{wait_sequence}}  & ( {2{cmd_code0}} & {2{
			cmd_code1}} & {2{~cmd_code2}} & {2{cmd_code3}} & {2{cmd_code4}}  ) & ( {
			temp_cmd1,temp_cmd0}  ) ) | ( {2{wait_sequence}}  & ( {2{cmd_code0}} & {2{~
			cmd_code1}} & {2{cmd_code2}} & {2{cmd_code3}} & {2{cmd_code4}}  ) & ( {
			temp_cmd1,temp_cmd0}  ) ) | ( {2{wait_sequence}}  & ( {2{~cmd_code0}} & {2{~
			cmd_code1}} & {2{cmd_code2}} & {2{~cmd_code3}} & {2{~cmd_code4}}  ) & ( {
			temp_cmd1,temp_cmd0}  ) ) | ( {2{zbit_clr}}  & ( 2'h3 ) & ( {temp_cmd1,
			temp_cmd0}  ) ) | ( {2{zbit_cstr}}  & ( 2'h3 ) & ( {temp_cmd1,temp_cmd0}  ) )
			 | ( {2{zbit_mr}}  & ( 2'h3 ) & ( {temp_cmd1,temp_cmd0}  ) ) | ( {2{zbit_rege
			}}  & ( 2'h3 ) & ( {temp_cmd1,temp_cmd0}  ) ) | ( {2{zbit_reve}}  & ( 2'h3 ) 
			& ( {temp_cmd1,temp_cmd0}  ) ) | ( {2{zbit_sync}}  & ( 2'h3 ) & ( 'h1 ) ) | (
			 {2{zbit_tra}}  & ( 2'h3 ) & ( {temp_cmd1,temp_cmd0}  ) ) | ( {2{zbit_wccm}} 
			 & ( 2'h3 ) & ( {temp_cmd1,temp_cmd0}  ) ) | ( {2{zbit_wcps}}  & ( 2'h3 ) & (
			 {temp_cmd1,temp_cmd0}  ) ) | ( {2{zbit_wcr}}  & ( 2'h3 ) & ( {temp_cmd1,
			temp_cmd0}  ) );

		next_BP_data_reg_out31 = BP_data_reg_out[31];
		next_BP_data_reg_out30 = BP_data_reg_out[30];
		next_BP_data_reg_out29 = BP_data_reg_out[29];
		next_BP_data_reg_out28 = BP_data_reg_out[28];
		next_BP_data_reg_out27 = BP_data_reg_out[27];
		next_BP_data_reg_out26 = BP_data_reg_out[26];
		next_BP_data_reg_out25 = BP_data_reg_out[25];
		next_BP_data_reg_out24 = BP_data_reg_out[24];
		next_BP_data_reg_out23 = BP_data_reg_out[23];
		next_BP_data_reg_out22 = BP_data_reg_out[22];
		next_BP_data_reg_out21 = BP_data_reg_out[21];
		next_BP_data_reg_out20 = BP_data_reg_out[20];
		next_BP_data_reg_out19 = BP_data_reg_out[19];
		next_BP_data_reg_out18 = BP_data_reg_out[18];
		next_BP_data_reg_out17 = BP_data_reg_out[17];
		next_BP_data_reg_out16 = BP_data_reg_out[16];
		next_BP_data_reg_out15 = BP_data_reg_out[15];
		next_BP_data_reg_out14 = BP_data_reg_out[14];
		next_BP_data_reg_out13 = BP_data_reg_out[13];
		next_BP_data_reg_out12 = BP_data_reg_out[12];
		next_BP_data_reg_out11 = BP_data_reg_out[11];
		next_BP_data_reg_out10 = BP_data_reg_out[10];
		next_BP_data_reg_out9 = BP_data_reg_out[9];
		next_BP_data_reg_out8 = BP_data_reg_out[8];
		next_BP_data_reg_out7 = BP_data_reg_out[7];
		next_BP_data_reg_out6 = BP_data_reg_out[6];
		next_BP_data_reg_out5 = BP_data_reg_out[5];
		next_BP_data_reg_out4 = BP_data_reg_out[4];
		next_BP_data_reg_out3 = BP_data_reg_out[3];
		next_BP_data_reg_out2 = BP_data_reg_out[2];
		next_BP_data_reg_out1 = BP_data_reg_out[1];
		next_BP_data_reg_out0 = BP_data_reg_out[0];
		next_cnt5 = cnt[5];
		next_cnt4 = cnt[4];
		next_cnt3 = cnt[3];
		next_cnt2 = cnt[2];
		next_cnt1 = cnt[1];
		next_cnt0 = cnt[0];
		next_data_reg31 = data_reg[31];
		next_data_reg30 = data_reg[30];
		next_data_reg29 = data_reg[29];
		next_data_reg28 = data_reg[28];
		next_data_reg27 = data_reg[27];
		next_data_reg26 = data_reg[26];
		next_data_reg25 = data_reg[25];
		next_data_reg24 = data_reg[24];
		next_data_reg23 = data_reg[23];
		next_data_reg22 = data_reg[22];
		next_data_reg21 = data_reg[21];
		next_data_reg20 = data_reg[20];
		next_data_reg19 = data_reg[19];
		next_data_reg18 = data_reg[18];
		next_data_reg17 = data_reg[17];
		next_data_reg16 = data_reg[16];
		next_data_reg15 = data_reg[15];
		next_data_reg14 = data_reg[14];
		next_data_reg13 = data_reg[13];
		next_data_reg12 = data_reg[12];
		next_data_reg11 = data_reg[11];
		next_data_reg10 = data_reg[10];
		next_data_reg9 = data_reg[9];
		next_data_reg8 = data_reg[8];
		next_data_reg7 = data_reg[7];
		next_data_reg6 = data_reg[6];
		next_data_reg5 = data_reg[5];
		next_data_reg4 = data_reg[4];
		next_data_reg3 = data_reg[3];
		next_data_reg2 = data_reg[2];
		next_data_reg1 = data_reg[1];
		next_data_reg0 = data_reg[0];
		next_shift_reg4 = shift_reg[4];
		next_shift_reg3 = shift_reg[3];
		next_shift_reg2 = shift_reg[2];
		next_shift_reg1 = shift_reg[1];
		next_shift_reg0 = shift_reg[0];
		next_temp_cmd1 = temp_cmd[1];
		next_temp_cmd0 = temp_cmd[0];
	end

	always @(BP_cmd)
	begin
		if ( BP_cmd )  cmd=1;
		else cmd=0;
	end

	always @(BP_FIFO_we)
	begin
		if ( BP_FIFO_we )  FIFO_we=1;
		else FIFO_we=0;
	end

	always @(BP_mux_out)
	begin
		if ( BP_mux_out )  mux_out=1;
		else mux_out=0;
	end

	always @(BP_reset_code)
	begin
		if ( BP_reset_code )  reset_code=1;
		else reset_code=0;
	end

	always @(BP_data_reg_out0 or BP_data_reg_out1 or BP_data_reg_out2 or 
		BP_data_reg_out3 or BP_data_reg_out4 or BP_data_reg_out5 or BP_data_reg_out6 
		or BP_data_reg_out7 or BP_data_reg_out8 or BP_data_reg_out9 or 
		BP_data_reg_out10 or BP_data_reg_out11 or BP_data_reg_out12 or 
		BP_data_reg_out13 or BP_data_reg_out14 or BP_data_reg_out15 or 
		BP_data_reg_out16 or BP_data_reg_out17 or BP_data_reg_out18 or 
		BP_data_reg_out19 or BP_data_reg_out20 or BP_data_reg_out21 or 
		BP_data_reg_out22 or BP_data_reg_out23 or BP_data_reg_out24 or 
		BP_data_reg_out25 or BP_data_reg_out26 or BP_data_reg_out27 or 
		BP_data_reg_out28 or BP_data_reg_out29 or BP_data_reg_out30 or 
		BP_data_reg_out31 or data_reg_out)
	begin
		data_reg_out= {BP_data_reg_out31,BP_data_reg_out30,BP_data_reg_out29,
			BP_data_reg_out28,BP_data_reg_out27,BP_data_reg_out26,BP_data_reg_out25,
			BP_data_reg_out24,BP_data_reg_out23,BP_data_reg_out22,BP_data_reg_out21,
			BP_data_reg_out20,BP_data_reg_out19,BP_data_reg_out18,BP_data_reg_out17,
			BP_data_reg_out16,BP_data_reg_out15,BP_data_reg_out14,BP_data_reg_out13,
			BP_data_reg_out12,BP_data_reg_out11,BP_data_reg_out10,BP_data_reg_out9,
			BP_data_reg_out8,BP_data_reg_out7,BP_data_reg_out6,BP_data_reg_out5,
			BP_data_reg_out4,BP_data_reg_out3,BP_data_reg_out2,BP_data_reg_out1,
			BP_data_reg_out0} ;
		data_reg_out0 = data_reg_out[0];
		data_reg_out1 = data_reg_out[1];
		data_reg_out2 = data_reg_out[2];
		data_reg_out3 = data_reg_out[3];
		data_reg_out4 = data_reg_out[4];
		data_reg_out5 = data_reg_out[5];
		data_reg_out6 = data_reg_out[6];
		data_reg_out7 = data_reg_out[7];
		data_reg_out8 = data_reg_out[8];
		data_reg_out9 = data_reg_out[9];
		data_reg_out10 = data_reg_out[10];
		data_reg_out11 = data_reg_out[11];
		data_reg_out12 = data_reg_out[12];
		data_reg_out13 = data_reg_out[13];
		data_reg_out14 = data_reg_out[14];
		data_reg_out15 = data_reg_out[15];
		data_reg_out16 = data_reg_out[16];
		data_reg_out17 = data_reg_out[17];
		data_reg_out18 = data_reg_out[18];
		data_reg_out19 = data_reg_out[19];
		data_reg_out20 = data_reg_out[20];
		data_reg_out21 = data_reg_out[21];
		data_reg_out22 = data_reg_out[22];
		data_reg_out23 = data_reg_out[23];
		data_reg_out24 = data_reg_out[24];
		data_reg_out25 = data_reg_out[25];
		data_reg_out26 = data_reg_out[26];
		data_reg_out27 = data_reg_out[27];
		data_reg_out28 = data_reg_out[28];
		data_reg_out29 = data_reg_out[29];
		data_reg_out30 = data_reg_out[30];
		data_reg_out31 = data_reg_out[31];
	end
endmodule

module vlogatom(chip_addr,cmd_code,data,data_1,data_2,data_3,data_4,data_5,
	data_6,data_7,data_reg_out,eve_cou,CLK,atom_data,RESET,cmd,FIFO_we,mux_out,
	reset_code);

	input [4:0] chip_addr;
	input [4:0] cmd_code;
	input [31:0] data;
	input [31:0] data_1;
	input [31:0] data_2;
	input [31:0] data_3;
	input [31:0] data_4;
	input [31:0] data_5;
	input [31:0] data_6;
	input [31:0] data_7;
	output [31:0] data_reg_out;
	input [31:0] eve_cou;
	input CLK;
	input atom_data,RESET;
	output cmd,FIFO_we,mux_out,reset_code;

	wire [4:0] chip_addr;
	wire [4:0] cmd_code;
	wire [31:0] data;
	wire [31:0] data_1;
	wire [31:0] data_2;
	wire [31:0] data_3;
	wire [31:0] data_4;
	wire [31:0] data_5;
	wire [31:0] data_6;
	wire [31:0] data_7;
	wire [31:0] data_reg_out;
	wire [31:0] eve_cou;
	wire CLK;
	wire atom_data,RESET;
	wire cmd,FIFO_we,mux_out,reset_code;

	shell_vlogatom part1(.CLK(CLK),.atom_data(atom_data),.chip_addr0(
		chip_addr[0]),.chip_addr1(chip_addr[1]),.chip_addr2(chip_addr[2]),.chip_addr3
		(chip_addr[3]),.chip_addr4(chip_addr[4]),.cmd_code0(cmd_code[0]),.cmd_code1(
		cmd_code[1]),.cmd_code2(cmd_code[2]),.cmd_code3(cmd_code[3]),.cmd_code4(
		cmd_code[4]),.data0(data[0]),.data1(data[1]),.data2(data[2]),.data3(data[3]),
		.data4(data[4]),.data5(data[5]),.data6(data[6]),.data7(data[7]),.data8(
		data[8]),.data9(data[9]),.data10(data[10]),.data11(data[11]),.data12(data[12]
		),.data13(data[13]),.data14(data[14]),.data15(data[15]),.data16(data[16]),
		.data17(data[17]),.data18(data[18]),.data19(data[19]),.data20(data[20]),
		.data21(data[21]),.data22(data[22]),.data23(data[23]),.data24(data[24]),
		.data25(data[25]),.data26(data[26]),.data27(data[27]),.data28(data[28]),
		.data29(data[29]),.data30(data[30]),.data31(data[31]),.data_10(data_1[0]),
		.data_11(data_1[1]),.data_12(data_1[2]),.data_13(data_1[3]),.data_14(
		data_1[4]),.data_15(data_1[5]),.data_16(data_1[6]),.data_17(data_1[7]),
		.data_18(data_1[8]),.data_19(data_1[9]),.data_20(data_2[0]),.data_21(
		data_2[1]),.data_22(data_2[2]),.data_23(data_2[3]),.data_24(data_2[4]),
		.data_25(data_2[5]),.data_26(data_2[6]),.data_27(data_2[7]),.data_28(
		data_2[8]),.data_29(data_2[9]),.data_30(data_3[0]),.data_31(data_3[1]),
		.data_32(data_3[2]),.data_33(data_3[3]),.data_34(data_3[4]),.data_35(
		data_3[5]),.data_36(data_3[6]),.data_37(data_3[7]),.data_38(data_3[8]),
		.data_39(data_3[9]),.data_40(data_4[0]),.data_41(data_4[1]),.data_42(
		data_4[2]),.data_43(data_4[3]),.data_44(data_4[4]),.data_45(data_4[5]),
		.data_46(data_4[6]),.data_47(data_4[7]),.data_48(data_4[8]),.data_49(
		data_4[9]),.data_50(data_5[0]),.data_51(data_5[1]),.data_52(data_5[2]),
		.data_53(data_5[3]),.data_54(data_5[4]),.data_55(data_5[5]),.data_56(
		data_5[6]),.data_57(data_5[7]),.data_58(data_5[8]),.data_59(data_5[9]),
		.data_60(data_6[0]),.data_61(data_6[1]),.data_62(data_6[2]),.data_63(
		data_6[3]),.data_64(data_6[4]),.data_65(data_6[5]),.data_66(data_6[6]),
		.data_67(data_6[7]),.data_68(data_6[8]),.data_69(data_6[9]),.data_70(
		data_7[0]),.data_71(data_7[1]),.data_72(data_7[2]),.data_73(data_7[3]),
		.data_74(data_7[4]),.data_75(data_7[5]),.data_76(data_7[6]),.data_77(
		data_7[7]),.data_78(data_7[8]),.data_79(data_7[9]),.data_110(data_1[10]),
		.data_111(data_1[11]),.data_112(data_1[12]),.data_113(data_1[13]),.data_114(
		data_1[14]),.data_115(data_1[15]),.data_116(data_1[16]),.data_117(data_1[17])
		,.data_118(data_1[18]),.data_119(data_1[19]),.data_120(data_1[20]),.data_121(
		data_1[21]),.data_122(data_1[22]),.data_123(data_1[23]),.data_124(data_1[24])
		,.data_125(data_1[25]),.data_126(data_1[26]),.data_127(data_1[27]),.data_128(
		data_1[28]),.data_129(data_1[29]),.data_130(data_1[30]),.data_131(data_1[31])
		,.data_210(data_2[10]),.data_211(data_2[11]),.data_212(data_2[12]),.data_213(
		data_2[13]),.data_214(data_2[14]),.data_215(data_2[15]),.data_216(data_2[16])
		,.data_217(data_2[17]),.data_218(data_2[18]),.data_219(data_2[19]),.data_220(
		data_2[20]),.data_221(data_2[21]),.data_222(data_2[22]),.data_223(data_2[23])
		,.data_224(data_2[24]),.data_225(data_2[25]),.data_226(data_2[26]),.data_227(
		data_2[27]),.data_228(data_2[28]),.data_229(data_2[29]),.data_230(data_2[30])
		,.data_231(data_2[31]),.data_310(data_3[10]),.data_311(data_3[11]),.data_312(
		data_3[12]),.data_313(data_3[13]),.data_314(data_3[14]),.data_315(data_3[15])
		,.data_316(data_3[16]),.data_317(data_3[17]),.data_318(data_3[18]),.data_319(
		data_3[19]),.data_320(data_3[20]),.data_321(data_3[21]),.data_322(data_3[22])
		,.data_323(data_3[23]),.data_324(data_3[24]),.data_325(data_3[25]),.data_326(
		data_3[26]),.data_327(data_3[27]),.data_328(data_3[28]),.data_329(data_3[29])
		,.data_330(data_3[30]),.data_331(data_3[31]),.data_410(data_4[10]),.data_411(
		data_4[11]),.data_412(data_4[12]),.data_413(data_4[13]),.data_414(data_4[14])
		,.data_415(data_4[15]),.data_416(data_4[16]),.data_417(data_4[17]),.data_418(
		data_4[18]),.data_419(data_4[19]),.data_420(data_4[20]),.data_421(data_4[21])
		,.data_422(data_4[22]),.data_423(data_4[23]),.data_424(data_4[24]),.data_425(
		data_4[25]),.data_426(data_4[26]),.data_427(data_4[27]),.data_428(data_4[28])
		,.data_429(data_4[29]),.data_430(data_4[30]),.data_431(data_4[31]),.data_510(
		data_5[10]),.data_511(data_5[11]),.data_512(data_5[12]),.data_513(data_5[13])
		,.data_514(data_5[14]),.data_515(data_5[15]),.data_516(data_5[16]),.data_517(
		data_5[17]),.data_518(data_5[18]),.data_519(data_5[19]),.data_520(data_5[20])
		,.data_521(data_5[21]),.data_522(data_5[22]),.data_523(data_5[23]),.data_524(
		data_5[24]),.data_525(data_5[25]),.data_526(data_5[26]),.data_527(data_5[27])
		,.data_528(data_5[28]),.data_529(data_5[29]),.data_530(data_5[30]),.data_531(
		data_5[31]),.data_610(data_6[10]),.data_611(data_6[11]),.data_612(data_6[12])
		,.data_613(data_6[13]),.data_614(data_6[14]),.data_615(data_6[15]),.data_616(
		data_6[16]),.data_617(data_6[17]),.data_618(data_6[18]),.data_619(data_6[19])
		,.data_620(data_6[20]),.data_621(data_6[21]),.data_622(data_6[22]),.data_623(
		data_6[23]),.data_624(data_6[24]),.data_625(data_6[25]),.data_626(data_6[26])
		,.data_627(data_6[27]),.data_628(data_6[28]),.data_629(data_6[29]),.data_630(
		data_6[30]),.data_631(data_6[31]),.data_710(data_7[10]),.data_711(data_7[11])
		,.data_712(data_7[12]),.data_713(data_7[13]),.data_714(data_7[14]),.data_715(
		data_7[15]),.data_716(data_7[16]),.data_717(data_7[17]),.data_718(data_7[18])
		,.data_719(data_7[19]),.data_720(data_7[20]),.data_721(data_7[21]),.data_722(
		data_7[22]),.data_723(data_7[23]),.data_724(data_7[24]),.data_725(data_7[25])
		,.data_726(data_7[26]),.data_727(data_7[27]),.data_728(data_7[28]),.data_729(
		data_7[29]),.data_730(data_7[30]),.data_731(data_7[31]),.eve_cou0(eve_cou[0])
		,.eve_cou1(eve_cou[1]),.eve_cou2(eve_cou[2]),.eve_cou3(eve_cou[3]),.eve_cou4(
		eve_cou[4]),.eve_cou5(eve_cou[5]),.eve_cou6(eve_cou[6]),.eve_cou7(eve_cou[7])
		,.eve_cou8(eve_cou[8]),.eve_cou9(eve_cou[9]),.eve_cou10(eve_cou[10]),
		.eve_cou11(eve_cou[11]),.eve_cou12(eve_cou[12]),.eve_cou13(eve_cou[13]),
		.eve_cou14(eve_cou[14]),.eve_cou15(eve_cou[15]),.eve_cou16(eve_cou[16]),
		.eve_cou17(eve_cou[17]),.eve_cou18(eve_cou[18]),.eve_cou19(eve_cou[19]),
		.eve_cou20(eve_cou[20]),.eve_cou21(eve_cou[21]),.eve_cou22(eve_cou[22]),
		.eve_cou23(eve_cou[23]),.eve_cou24(eve_cou[24]),.eve_cou25(eve_cou[25]),
		.eve_cou26(eve_cou[26]),.eve_cou27(eve_cou[27]),.eve_cou28(eve_cou[28]),
		.eve_cou29(eve_cou[29]),.eve_cou30(eve_cou[30]),.eve_cou31(eve_cou[31]),
		.RESET(RESET),.cmd(cmd),.data_reg_out0(data_reg_out[0]),.data_reg_out1(
		data_reg_out[1]),.data_reg_out2(data_reg_out[2]),.data_reg_out3(
		data_reg_out[3]),.data_reg_out4(data_reg_out[4]),.data_reg_out5(
		data_reg_out[5]),.data_reg_out6(data_reg_out[6]),.data_reg_out7(
		data_reg_out[7]),.data_reg_out8(data_reg_out[8]),.data_reg_out9(
		data_reg_out[9]),.data_reg_out10(data_reg_out[10]),.data_reg_out11(
		data_reg_out[11]),.data_reg_out12(data_reg_out[12]),.data_reg_out13(
		data_reg_out[13]),.data_reg_out14(data_reg_out[14]),.data_reg_out15(
		data_reg_out[15]),.data_reg_out16(data_reg_out[16]),.data_reg_out17(
		data_reg_out[17]),.data_reg_out18(data_reg_out[18]),.data_reg_out19(
		data_reg_out[19]),.data_reg_out20(data_reg_out[20]),.data_reg_out21(
		data_reg_out[21]),.data_reg_out22(data_reg_out[22]),.data_reg_out23(
		data_reg_out[23]),.data_reg_out24(data_reg_out[24]),.data_reg_out25(
		data_reg_out[25]),.data_reg_out26(data_reg_out[26]),.data_reg_out27(
		data_reg_out[27]),.data_reg_out28(data_reg_out[28]),.data_reg_out29(
		data_reg_out[29]),.data_reg_out30(data_reg_out[30]),.data_reg_out31(
		data_reg_out[31]),.FIFO_we(FIFO_we),.mux_out(mux_out),.reset_code(reset_code)
		);


endmodule
