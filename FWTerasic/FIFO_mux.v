//-----------------------------------------------------
// Design Name : mux_using_assign
// File Name   : mux_using_assign.v
// Function    : 2:1 Mux using Assign
// Coder       : Deepak Kumar Tala
//-----------------------------------------------------
module  FIFO_mux(
FIFO_p_0      , // Mux first input
FIFO_n_0      , // Mux Second input
FIFO_p_1      , // Mux third input
FIFO_n_1      , // Mux fourth input
sel           , // Select input
FIFO_data     , // Mux output
clk             // CLK
);
//-----------Input Ports---------------
input [31:0] FIFO_p_0; 
input [31:0] FIFO_n_0; 
input [31:0] FIFO_p_1; 
input [31:0] FIFO_n_1; 
input [2:0] sel; 
input clk; 
//-----------Output Ports---------------
output [31:0] FIFO_data;
//------------Internal Variables--------
reg  [31:0] FIFO_data;
//-------------Code Start-----------------
always@ (posedge clk) begin
	if(sel[2:0] == 3'b001) FIFO_data = FIFO_p_0;
	else if (sel[2:0] == 3'b010) FIFO_data = FIFO_n_0;
	else if (sel[2:0] == 3'b011 ) FIFO_data = FIFO_p_1;
	else if (sel[2:0] == 3'b100 ) FIFO_data = FIFO_n_1;
	else FIFO_data = 32'b0000000000000000000000000000000;
	end
endmodule //End Of Module mux
