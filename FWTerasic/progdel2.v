`timescale 1ns/1ns

module progdel2(din,clk,dout);

 //list of definitions
`define DLY_SIZE 16 

    input din;
    input clk;
    output dout;
//	 reg din;
	 reg dout;

	 reg [`DLY_SIZE-1:0] shifter;

	always@(posedge clk) begin
		shifter = {shifter[`DLY_SIZE-2:0],din};
		dout = shifter[`DLY_SIZE-1];
		end
		
endmodule
