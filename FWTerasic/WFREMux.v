//-----------------------------------------------------
// Design Name : mux_using_assign
// File Name   : mux_using_assign.v
// Function    : 2:1 Mux using Assign
// Coder       : Deepak Kumar Tala
//-----------------------------------------------------
module  wfre_mux(
wfre_p_0      , // Mux first input
wfre_n_0      , // Mux Second input
wfre_p_1      , // Mux third input
wfre_n_1      , // Mux fourth input
sel           , // Select input
wfre_out      , // Mux output
clk             // CLK
);
//-----------Input Ports---------------
input [1:0] wfre_p_0; 
input [1:0] wfre_n_0; 
input [1:0] wfre_p_1; 
input [1:0] wfre_n_1; 
input [1:0] sel; 
input clk; 
//-----------Output Ports---------------
output [1:0] wfre_out;
//------------Internal Variables--------
reg  [1:0] wfre_out;
//-------------Code Start-----------------
always@ (posedge clk) begin
	if(sel[1:0] == 2'b00) wfre_out = wfre_p_0;
	else if (sel[1:0] == 2'b01) wfre_out = wfre_n_0;
	else if (sel[1:0] == 2'b10 ) wfre_out = wfre_p_1;
	else if (sel[1:0] == 2'b11 ) wfre_out = wfre_n_1;
	end
endmodule //End Of Module mux