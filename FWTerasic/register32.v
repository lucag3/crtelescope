module register32(q,d,clk,clr,ena);

output[31:0] q;
input[31:0] d;
input clr, clk, ena;
reg[31:0] q;
always @ (posedge clk or posedge clr)
	if(clr)
		q = 0;
	else if(ena)
	q = d;
endmodule