//megafunction wizard: %Altera SOPC Builder%
//GENERATION: STANDARD
//VERSION: WM1.0


//Legal Notice: (C)2012 Altera Corporation. All rights reserved.  Your
//use of Altera Corporation's design tools, logic functions and other
//software and tools, and its AMPP partner logic functions, and any
//output files any of the foregoing (including device programming or
//simulation files), and any associated documentation or information are
//expressly subject to the terms and conditions of the Altera Program
//License Subscription Agreement or other applicable license agreement,
//including, without limitation, that your use is for the sole purpose
//of programming logic devices manufactured by Altera and sold by Altera
//or its authorized distributors.  Please refer to the applicable
//agreement for further details.

// synthesis translate_off
`timescale 1ns / 1ps
// synthesis translate_on

// turn off superfluous verilog processor warnings 
// altera message_level Level1 
// altera message_off 10034 10035 10036 10037 10230 10240 10030 

module DE2_70_SOPC_clock_0_in_arbitrator (
                                           // inputs:
                                            DE2_70_SOPC_clock_0_in_endofpacket,
                                            DE2_70_SOPC_clock_0_in_readdata,
                                            DE2_70_SOPC_clock_0_in_waitrequest,
                                            clk,
                                            cpu_data_master_address_to_slave,
                                            cpu_data_master_byteenable,
                                            cpu_data_master_latency_counter,
                                            cpu_data_master_read,
                                            cpu_data_master_write,
                                            cpu_data_master_writedata,
                                            reset_n,

                                           // outputs:
                                            DE2_70_SOPC_clock_0_in_address,
                                            DE2_70_SOPC_clock_0_in_byteenable,
                                            DE2_70_SOPC_clock_0_in_endofpacket_from_sa,
                                            DE2_70_SOPC_clock_0_in_nativeaddress,
                                            DE2_70_SOPC_clock_0_in_read,
                                            DE2_70_SOPC_clock_0_in_readdata_from_sa,
                                            DE2_70_SOPC_clock_0_in_reset_n,
                                            DE2_70_SOPC_clock_0_in_waitrequest_from_sa,
                                            DE2_70_SOPC_clock_0_in_write,
                                            DE2_70_SOPC_clock_0_in_writedata,
                                            cpu_data_master_granted_DE2_70_SOPC_clock_0_in,
                                            cpu_data_master_qualified_request_DE2_70_SOPC_clock_0_in,
                                            cpu_data_master_read_data_valid_DE2_70_SOPC_clock_0_in,
                                            cpu_data_master_requests_DE2_70_SOPC_clock_0_in,
                                            d1_DE2_70_SOPC_clock_0_in_end_xfer
                                         )
;

  output  [  3: 0] DE2_70_SOPC_clock_0_in_address;
  output  [  1: 0] DE2_70_SOPC_clock_0_in_byteenable;
  output           DE2_70_SOPC_clock_0_in_endofpacket_from_sa;
  output  [  2: 0] DE2_70_SOPC_clock_0_in_nativeaddress;
  output           DE2_70_SOPC_clock_0_in_read;
  output  [ 15: 0] DE2_70_SOPC_clock_0_in_readdata_from_sa;
  output           DE2_70_SOPC_clock_0_in_reset_n;
  output           DE2_70_SOPC_clock_0_in_waitrequest_from_sa;
  output           DE2_70_SOPC_clock_0_in_write;
  output  [ 15: 0] DE2_70_SOPC_clock_0_in_writedata;
  output           cpu_data_master_granted_DE2_70_SOPC_clock_0_in;
  output           cpu_data_master_qualified_request_DE2_70_SOPC_clock_0_in;
  output           cpu_data_master_read_data_valid_DE2_70_SOPC_clock_0_in;
  output           cpu_data_master_requests_DE2_70_SOPC_clock_0_in;
  output           d1_DE2_70_SOPC_clock_0_in_end_xfer;
  input            DE2_70_SOPC_clock_0_in_endofpacket;
  input   [ 15: 0] DE2_70_SOPC_clock_0_in_readdata;
  input            DE2_70_SOPC_clock_0_in_waitrequest;
  input            clk;
  input   [ 24: 0] cpu_data_master_address_to_slave;
  input   [  3: 0] cpu_data_master_byteenable;
  input   [  2: 0] cpu_data_master_latency_counter;
  input            cpu_data_master_read;
  input            cpu_data_master_write;
  input   [ 31: 0] cpu_data_master_writedata;
  input            reset_n;

  wire    [  3: 0] DE2_70_SOPC_clock_0_in_address;
  wire             DE2_70_SOPC_clock_0_in_allgrants;
  wire             DE2_70_SOPC_clock_0_in_allow_new_arb_cycle;
  wire             DE2_70_SOPC_clock_0_in_any_bursting_master_saved_grant;
  wire             DE2_70_SOPC_clock_0_in_any_continuerequest;
  wire             DE2_70_SOPC_clock_0_in_arb_counter_enable;
  reg              DE2_70_SOPC_clock_0_in_arb_share_counter;
  wire             DE2_70_SOPC_clock_0_in_arb_share_counter_next_value;
  wire             DE2_70_SOPC_clock_0_in_arb_share_set_values;
  wire             DE2_70_SOPC_clock_0_in_beginbursttransfer_internal;
  wire             DE2_70_SOPC_clock_0_in_begins_xfer;
  wire    [  1: 0] DE2_70_SOPC_clock_0_in_byteenable;
  wire             DE2_70_SOPC_clock_0_in_end_xfer;
  wire             DE2_70_SOPC_clock_0_in_endofpacket_from_sa;
  wire             DE2_70_SOPC_clock_0_in_firsttransfer;
  wire             DE2_70_SOPC_clock_0_in_grant_vector;
  wire             DE2_70_SOPC_clock_0_in_in_a_read_cycle;
  wire             DE2_70_SOPC_clock_0_in_in_a_write_cycle;
  wire             DE2_70_SOPC_clock_0_in_master_qreq_vector;
  wire    [  2: 0] DE2_70_SOPC_clock_0_in_nativeaddress;
  wire             DE2_70_SOPC_clock_0_in_non_bursting_master_requests;
  wire             DE2_70_SOPC_clock_0_in_read;
  wire    [ 15: 0] DE2_70_SOPC_clock_0_in_readdata_from_sa;
  reg              DE2_70_SOPC_clock_0_in_reg_firsttransfer;
  wire             DE2_70_SOPC_clock_0_in_reset_n;
  reg              DE2_70_SOPC_clock_0_in_slavearbiterlockenable;
  wire             DE2_70_SOPC_clock_0_in_slavearbiterlockenable2;
  wire             DE2_70_SOPC_clock_0_in_unreg_firsttransfer;
  wire             DE2_70_SOPC_clock_0_in_waitrequest_from_sa;
  wire             DE2_70_SOPC_clock_0_in_waits_for_read;
  wire             DE2_70_SOPC_clock_0_in_waits_for_write;
  wire             DE2_70_SOPC_clock_0_in_write;
  wire    [ 15: 0] DE2_70_SOPC_clock_0_in_writedata;
  wire             cpu_data_master_arbiterlock;
  wire             cpu_data_master_arbiterlock2;
  wire             cpu_data_master_continuerequest;
  wire             cpu_data_master_granted_DE2_70_SOPC_clock_0_in;
  wire             cpu_data_master_qualified_request_DE2_70_SOPC_clock_0_in;
  wire             cpu_data_master_read_data_valid_DE2_70_SOPC_clock_0_in;
  wire             cpu_data_master_requests_DE2_70_SOPC_clock_0_in;
  wire             cpu_data_master_saved_grant_DE2_70_SOPC_clock_0_in;
  reg              d1_DE2_70_SOPC_clock_0_in_end_xfer;
  reg              d1_reasons_to_wait;
  reg              enable_nonzero_assertions;
  wire             end_xfer_arb_share_counter_term_DE2_70_SOPC_clock_0_in;
  wire             in_a_read_cycle;
  wire             in_a_write_cycle;
  wire    [ 24: 0] shifted_address_to_DE2_70_SOPC_clock_0_in_from_cpu_data_master;
  wire             wait_for_DE2_70_SOPC_clock_0_in_counter;
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          d1_reasons_to_wait <= 0;
      else 
        d1_reasons_to_wait <= ~DE2_70_SOPC_clock_0_in_end_xfer;
    end


  assign DE2_70_SOPC_clock_0_in_begins_xfer = ~d1_reasons_to_wait & ((cpu_data_master_qualified_request_DE2_70_SOPC_clock_0_in));
  //assign DE2_70_SOPC_clock_0_in_readdata_from_sa = DE2_70_SOPC_clock_0_in_readdata so that symbol knows where to group signals which may go to master only, which is an e_assign
  assign DE2_70_SOPC_clock_0_in_readdata_from_sa = DE2_70_SOPC_clock_0_in_readdata;

  assign cpu_data_master_requests_DE2_70_SOPC_clock_0_in = ({cpu_data_master_address_to_slave[24 : 5] , 5'b0} == 25'h1409040) & (cpu_data_master_read | cpu_data_master_write);
  //assign DE2_70_SOPC_clock_0_in_waitrequest_from_sa = DE2_70_SOPC_clock_0_in_waitrequest so that symbol knows where to group signals which may go to master only, which is an e_assign
  assign DE2_70_SOPC_clock_0_in_waitrequest_from_sa = DE2_70_SOPC_clock_0_in_waitrequest;

  //DE2_70_SOPC_clock_0_in_arb_share_counter set values, which is an e_mux
  assign DE2_70_SOPC_clock_0_in_arb_share_set_values = 1;

  //DE2_70_SOPC_clock_0_in_non_bursting_master_requests mux, which is an e_mux
  assign DE2_70_SOPC_clock_0_in_non_bursting_master_requests = cpu_data_master_requests_DE2_70_SOPC_clock_0_in;

  //DE2_70_SOPC_clock_0_in_any_bursting_master_saved_grant mux, which is an e_mux
  assign DE2_70_SOPC_clock_0_in_any_bursting_master_saved_grant = 0;

  //DE2_70_SOPC_clock_0_in_arb_share_counter_next_value assignment, which is an e_assign
  assign DE2_70_SOPC_clock_0_in_arb_share_counter_next_value = DE2_70_SOPC_clock_0_in_firsttransfer ? (DE2_70_SOPC_clock_0_in_arb_share_set_values - 1) : |DE2_70_SOPC_clock_0_in_arb_share_counter ? (DE2_70_SOPC_clock_0_in_arb_share_counter - 1) : 0;

  //DE2_70_SOPC_clock_0_in_allgrants all slave grants, which is an e_mux
  assign DE2_70_SOPC_clock_0_in_allgrants = |DE2_70_SOPC_clock_0_in_grant_vector;

  //DE2_70_SOPC_clock_0_in_end_xfer assignment, which is an e_assign
  assign DE2_70_SOPC_clock_0_in_end_xfer = ~(DE2_70_SOPC_clock_0_in_waits_for_read | DE2_70_SOPC_clock_0_in_waits_for_write);

  //end_xfer_arb_share_counter_term_DE2_70_SOPC_clock_0_in arb share counter enable term, which is an e_assign
  assign end_xfer_arb_share_counter_term_DE2_70_SOPC_clock_0_in = DE2_70_SOPC_clock_0_in_end_xfer & (~DE2_70_SOPC_clock_0_in_any_bursting_master_saved_grant | in_a_read_cycle | in_a_write_cycle);

  //DE2_70_SOPC_clock_0_in_arb_share_counter arbitration counter enable, which is an e_assign
  assign DE2_70_SOPC_clock_0_in_arb_counter_enable = (end_xfer_arb_share_counter_term_DE2_70_SOPC_clock_0_in & DE2_70_SOPC_clock_0_in_allgrants) | (end_xfer_arb_share_counter_term_DE2_70_SOPC_clock_0_in & ~DE2_70_SOPC_clock_0_in_non_bursting_master_requests);

  //DE2_70_SOPC_clock_0_in_arb_share_counter counter, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          DE2_70_SOPC_clock_0_in_arb_share_counter <= 0;
      else if (DE2_70_SOPC_clock_0_in_arb_counter_enable)
          DE2_70_SOPC_clock_0_in_arb_share_counter <= DE2_70_SOPC_clock_0_in_arb_share_counter_next_value;
    end


  //DE2_70_SOPC_clock_0_in_slavearbiterlockenable slave enables arbiterlock, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          DE2_70_SOPC_clock_0_in_slavearbiterlockenable <= 0;
      else if ((|DE2_70_SOPC_clock_0_in_master_qreq_vector & end_xfer_arb_share_counter_term_DE2_70_SOPC_clock_0_in) | (end_xfer_arb_share_counter_term_DE2_70_SOPC_clock_0_in & ~DE2_70_SOPC_clock_0_in_non_bursting_master_requests))
          DE2_70_SOPC_clock_0_in_slavearbiterlockenable <= |DE2_70_SOPC_clock_0_in_arb_share_counter_next_value;
    end


  //cpu/data_master DE2_70_SOPC_clock_0/in arbiterlock, which is an e_assign
  assign cpu_data_master_arbiterlock = DE2_70_SOPC_clock_0_in_slavearbiterlockenable & cpu_data_master_continuerequest;

  //DE2_70_SOPC_clock_0_in_slavearbiterlockenable2 slave enables arbiterlock2, which is an e_assign
  assign DE2_70_SOPC_clock_0_in_slavearbiterlockenable2 = |DE2_70_SOPC_clock_0_in_arb_share_counter_next_value;

  //cpu/data_master DE2_70_SOPC_clock_0/in arbiterlock2, which is an e_assign
  assign cpu_data_master_arbiterlock2 = DE2_70_SOPC_clock_0_in_slavearbiterlockenable2 & cpu_data_master_continuerequest;

  //DE2_70_SOPC_clock_0_in_any_continuerequest at least one master continues requesting, which is an e_assign
  assign DE2_70_SOPC_clock_0_in_any_continuerequest = 1;

  //cpu_data_master_continuerequest continued request, which is an e_assign
  assign cpu_data_master_continuerequest = 1;

  assign cpu_data_master_qualified_request_DE2_70_SOPC_clock_0_in = cpu_data_master_requests_DE2_70_SOPC_clock_0_in & ~((cpu_data_master_read & ((cpu_data_master_latency_counter != 0))));
  //local readdatavalid cpu_data_master_read_data_valid_DE2_70_SOPC_clock_0_in, which is an e_mux
  assign cpu_data_master_read_data_valid_DE2_70_SOPC_clock_0_in = cpu_data_master_granted_DE2_70_SOPC_clock_0_in & cpu_data_master_read & ~DE2_70_SOPC_clock_0_in_waits_for_read;

  //DE2_70_SOPC_clock_0_in_writedata mux, which is an e_mux
  assign DE2_70_SOPC_clock_0_in_writedata = cpu_data_master_writedata;

  //assign DE2_70_SOPC_clock_0_in_endofpacket_from_sa = DE2_70_SOPC_clock_0_in_endofpacket so that symbol knows where to group signals which may go to master only, which is an e_assign
  assign DE2_70_SOPC_clock_0_in_endofpacket_from_sa = DE2_70_SOPC_clock_0_in_endofpacket;

  //master is always granted when requested
  assign cpu_data_master_granted_DE2_70_SOPC_clock_0_in = cpu_data_master_qualified_request_DE2_70_SOPC_clock_0_in;

  //cpu/data_master saved-grant DE2_70_SOPC_clock_0/in, which is an e_assign
  assign cpu_data_master_saved_grant_DE2_70_SOPC_clock_0_in = cpu_data_master_requests_DE2_70_SOPC_clock_0_in;

  //allow new arb cycle for DE2_70_SOPC_clock_0/in, which is an e_assign
  assign DE2_70_SOPC_clock_0_in_allow_new_arb_cycle = 1;

  //placeholder chosen master
  assign DE2_70_SOPC_clock_0_in_grant_vector = 1;

  //placeholder vector of master qualified-requests
  assign DE2_70_SOPC_clock_0_in_master_qreq_vector = 1;

  //DE2_70_SOPC_clock_0_in_reset_n assignment, which is an e_assign
  assign DE2_70_SOPC_clock_0_in_reset_n = reset_n;

  //DE2_70_SOPC_clock_0_in_firsttransfer first transaction, which is an e_assign
  assign DE2_70_SOPC_clock_0_in_firsttransfer = DE2_70_SOPC_clock_0_in_begins_xfer ? DE2_70_SOPC_clock_0_in_unreg_firsttransfer : DE2_70_SOPC_clock_0_in_reg_firsttransfer;

  //DE2_70_SOPC_clock_0_in_unreg_firsttransfer first transaction, which is an e_assign
  assign DE2_70_SOPC_clock_0_in_unreg_firsttransfer = ~(DE2_70_SOPC_clock_0_in_slavearbiterlockenable & DE2_70_SOPC_clock_0_in_any_continuerequest);

  //DE2_70_SOPC_clock_0_in_reg_firsttransfer first transaction, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          DE2_70_SOPC_clock_0_in_reg_firsttransfer <= 1'b1;
      else if (DE2_70_SOPC_clock_0_in_begins_xfer)
          DE2_70_SOPC_clock_0_in_reg_firsttransfer <= DE2_70_SOPC_clock_0_in_unreg_firsttransfer;
    end


  //DE2_70_SOPC_clock_0_in_beginbursttransfer_internal begin burst transfer, which is an e_assign
  assign DE2_70_SOPC_clock_0_in_beginbursttransfer_internal = DE2_70_SOPC_clock_0_in_begins_xfer;

  //DE2_70_SOPC_clock_0_in_read assignment, which is an e_mux
  assign DE2_70_SOPC_clock_0_in_read = cpu_data_master_granted_DE2_70_SOPC_clock_0_in & cpu_data_master_read;

  //DE2_70_SOPC_clock_0_in_write assignment, which is an e_mux
  assign DE2_70_SOPC_clock_0_in_write = cpu_data_master_granted_DE2_70_SOPC_clock_0_in & cpu_data_master_write;

  assign shifted_address_to_DE2_70_SOPC_clock_0_in_from_cpu_data_master = cpu_data_master_address_to_slave;
  //DE2_70_SOPC_clock_0_in_address mux, which is an e_mux
  assign DE2_70_SOPC_clock_0_in_address = shifted_address_to_DE2_70_SOPC_clock_0_in_from_cpu_data_master >> 2;

  //slaveid DE2_70_SOPC_clock_0_in_nativeaddress nativeaddress mux, which is an e_mux
  assign DE2_70_SOPC_clock_0_in_nativeaddress = cpu_data_master_address_to_slave >> 2;

  //d1_DE2_70_SOPC_clock_0_in_end_xfer register, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          d1_DE2_70_SOPC_clock_0_in_end_xfer <= 1;
      else 
        d1_DE2_70_SOPC_clock_0_in_end_xfer <= DE2_70_SOPC_clock_0_in_end_xfer;
    end


  //DE2_70_SOPC_clock_0_in_waits_for_read in a cycle, which is an e_mux
  assign DE2_70_SOPC_clock_0_in_waits_for_read = DE2_70_SOPC_clock_0_in_in_a_read_cycle & DE2_70_SOPC_clock_0_in_waitrequest_from_sa;

  //DE2_70_SOPC_clock_0_in_in_a_read_cycle assignment, which is an e_assign
  assign DE2_70_SOPC_clock_0_in_in_a_read_cycle = cpu_data_master_granted_DE2_70_SOPC_clock_0_in & cpu_data_master_read;

  //in_a_read_cycle assignment, which is an e_mux
  assign in_a_read_cycle = DE2_70_SOPC_clock_0_in_in_a_read_cycle;

  //DE2_70_SOPC_clock_0_in_waits_for_write in a cycle, which is an e_mux
  assign DE2_70_SOPC_clock_0_in_waits_for_write = DE2_70_SOPC_clock_0_in_in_a_write_cycle & DE2_70_SOPC_clock_0_in_waitrequest_from_sa;

  //DE2_70_SOPC_clock_0_in_in_a_write_cycle assignment, which is an e_assign
  assign DE2_70_SOPC_clock_0_in_in_a_write_cycle = cpu_data_master_granted_DE2_70_SOPC_clock_0_in & cpu_data_master_write;

  //in_a_write_cycle assignment, which is an e_mux
  assign in_a_write_cycle = DE2_70_SOPC_clock_0_in_in_a_write_cycle;

  assign wait_for_DE2_70_SOPC_clock_0_in_counter = 0;
  //DE2_70_SOPC_clock_0_in_byteenable byte enable port mux, which is an e_mux
  assign DE2_70_SOPC_clock_0_in_byteenable = (cpu_data_master_granted_DE2_70_SOPC_clock_0_in)? cpu_data_master_byteenable :
    -1;


//synthesis translate_off
//////////////// SIMULATION-ONLY CONTENTS
  //DE2_70_SOPC_clock_0/in enable non-zero assertions, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          enable_nonzero_assertions <= 0;
      else 
        enable_nonzero_assertions <= 1'b1;
    end



//////////////// END SIMULATION-ONLY CONTENTS

//synthesis translate_on

endmodule


// synthesis translate_off
`timescale 1ns / 1ps
// synthesis translate_on

// turn off superfluous verilog processor warnings 
// altera message_level Level1 
// altera message_off 10034 10035 10036 10037 10230 10240 10030 

module DE2_70_SOPC_clock_0_out_arbitrator (
                                            // inputs:
                                             DE2_70_SOPC_clock_0_out_address,
                                             DE2_70_SOPC_clock_0_out_byteenable,
                                             DE2_70_SOPC_clock_0_out_granted_pll_s1,
                                             DE2_70_SOPC_clock_0_out_qualified_request_pll_s1,
                                             DE2_70_SOPC_clock_0_out_read,
                                             DE2_70_SOPC_clock_0_out_read_data_valid_pll_s1,
                                             DE2_70_SOPC_clock_0_out_requests_pll_s1,
                                             DE2_70_SOPC_clock_0_out_write,
                                             DE2_70_SOPC_clock_0_out_writedata,
                                             clk,
                                             d1_pll_s1_end_xfer,
                                             pll_s1_readdata_from_sa,
                                             reset_n,

                                            // outputs:
                                             DE2_70_SOPC_clock_0_out_address_to_slave,
                                             DE2_70_SOPC_clock_0_out_readdata,
                                             DE2_70_SOPC_clock_0_out_reset_n,
                                             DE2_70_SOPC_clock_0_out_waitrequest
                                          )
;

  output  [  3: 0] DE2_70_SOPC_clock_0_out_address_to_slave;
  output  [ 15: 0] DE2_70_SOPC_clock_0_out_readdata;
  output           DE2_70_SOPC_clock_0_out_reset_n;
  output           DE2_70_SOPC_clock_0_out_waitrequest;
  input   [  3: 0] DE2_70_SOPC_clock_0_out_address;
  input   [  1: 0] DE2_70_SOPC_clock_0_out_byteenable;
  input            DE2_70_SOPC_clock_0_out_granted_pll_s1;
  input            DE2_70_SOPC_clock_0_out_qualified_request_pll_s1;
  input            DE2_70_SOPC_clock_0_out_read;
  input            DE2_70_SOPC_clock_0_out_read_data_valid_pll_s1;
  input            DE2_70_SOPC_clock_0_out_requests_pll_s1;
  input            DE2_70_SOPC_clock_0_out_write;
  input   [ 15: 0] DE2_70_SOPC_clock_0_out_writedata;
  input            clk;
  input            d1_pll_s1_end_xfer;
  input   [ 15: 0] pll_s1_readdata_from_sa;
  input            reset_n;

  reg     [  3: 0] DE2_70_SOPC_clock_0_out_address_last_time;
  wire    [  3: 0] DE2_70_SOPC_clock_0_out_address_to_slave;
  reg     [  1: 0] DE2_70_SOPC_clock_0_out_byteenable_last_time;
  reg              DE2_70_SOPC_clock_0_out_read_last_time;
  wire    [ 15: 0] DE2_70_SOPC_clock_0_out_readdata;
  wire             DE2_70_SOPC_clock_0_out_reset_n;
  wire             DE2_70_SOPC_clock_0_out_run;
  wire             DE2_70_SOPC_clock_0_out_waitrequest;
  reg              DE2_70_SOPC_clock_0_out_write_last_time;
  reg     [ 15: 0] DE2_70_SOPC_clock_0_out_writedata_last_time;
  reg              active_and_waiting_last_time;
  wire             r_2;
  //r_2 master_run cascaded wait assignment, which is an e_assign
  assign r_2 = 1 & ((~DE2_70_SOPC_clock_0_out_qualified_request_pll_s1 | ~DE2_70_SOPC_clock_0_out_read | (1 & ~d1_pll_s1_end_xfer & DE2_70_SOPC_clock_0_out_read))) & ((~DE2_70_SOPC_clock_0_out_qualified_request_pll_s1 | ~DE2_70_SOPC_clock_0_out_write | (1 & DE2_70_SOPC_clock_0_out_write)));

  //cascaded wait assignment, which is an e_assign
  assign DE2_70_SOPC_clock_0_out_run = r_2;

  //optimize select-logic by passing only those address bits which matter.
  assign DE2_70_SOPC_clock_0_out_address_to_slave = DE2_70_SOPC_clock_0_out_address;

  //DE2_70_SOPC_clock_0/out readdata mux, which is an e_mux
  assign DE2_70_SOPC_clock_0_out_readdata = pll_s1_readdata_from_sa;

  //actual waitrequest port, which is an e_assign
  assign DE2_70_SOPC_clock_0_out_waitrequest = ~DE2_70_SOPC_clock_0_out_run;

  //DE2_70_SOPC_clock_0_out_reset_n assignment, which is an e_assign
  assign DE2_70_SOPC_clock_0_out_reset_n = reset_n;


//synthesis translate_off
//////////////// SIMULATION-ONLY CONTENTS
  //DE2_70_SOPC_clock_0_out_address check against wait, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          DE2_70_SOPC_clock_0_out_address_last_time <= 0;
      else 
        DE2_70_SOPC_clock_0_out_address_last_time <= DE2_70_SOPC_clock_0_out_address;
    end


  //DE2_70_SOPC_clock_0/out waited last time, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          active_and_waiting_last_time <= 0;
      else 
        active_and_waiting_last_time <= DE2_70_SOPC_clock_0_out_waitrequest & (DE2_70_SOPC_clock_0_out_read | DE2_70_SOPC_clock_0_out_write);
    end


  //DE2_70_SOPC_clock_0_out_address matches last port_name, which is an e_process
  always @(posedge clk)
    begin
      if (active_and_waiting_last_time & (DE2_70_SOPC_clock_0_out_address != DE2_70_SOPC_clock_0_out_address_last_time))
        begin
          $write("%0d ns: DE2_70_SOPC_clock_0_out_address did not heed wait!!!", $time);
          $stop;
        end
    end


  //DE2_70_SOPC_clock_0_out_byteenable check against wait, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          DE2_70_SOPC_clock_0_out_byteenable_last_time <= 0;
      else 
        DE2_70_SOPC_clock_0_out_byteenable_last_time <= DE2_70_SOPC_clock_0_out_byteenable;
    end


  //DE2_70_SOPC_clock_0_out_byteenable matches last port_name, which is an e_process
  always @(posedge clk)
    begin
      if (active_and_waiting_last_time & (DE2_70_SOPC_clock_0_out_byteenable != DE2_70_SOPC_clock_0_out_byteenable_last_time))
        begin
          $write("%0d ns: DE2_70_SOPC_clock_0_out_byteenable did not heed wait!!!", $time);
          $stop;
        end
    end


  //DE2_70_SOPC_clock_0_out_read check against wait, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          DE2_70_SOPC_clock_0_out_read_last_time <= 0;
      else 
        DE2_70_SOPC_clock_0_out_read_last_time <= DE2_70_SOPC_clock_0_out_read;
    end


  //DE2_70_SOPC_clock_0_out_read matches last port_name, which is an e_process
  always @(posedge clk)
    begin
      if (active_and_waiting_last_time & (DE2_70_SOPC_clock_0_out_read != DE2_70_SOPC_clock_0_out_read_last_time))
        begin
          $write("%0d ns: DE2_70_SOPC_clock_0_out_read did not heed wait!!!", $time);
          $stop;
        end
    end


  //DE2_70_SOPC_clock_0_out_write check against wait, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          DE2_70_SOPC_clock_0_out_write_last_time <= 0;
      else 
        DE2_70_SOPC_clock_0_out_write_last_time <= DE2_70_SOPC_clock_0_out_write;
    end


  //DE2_70_SOPC_clock_0_out_write matches last port_name, which is an e_process
  always @(posedge clk)
    begin
      if (active_and_waiting_last_time & (DE2_70_SOPC_clock_0_out_write != DE2_70_SOPC_clock_0_out_write_last_time))
        begin
          $write("%0d ns: DE2_70_SOPC_clock_0_out_write did not heed wait!!!", $time);
          $stop;
        end
    end


  //DE2_70_SOPC_clock_0_out_writedata check against wait, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          DE2_70_SOPC_clock_0_out_writedata_last_time <= 0;
      else 
        DE2_70_SOPC_clock_0_out_writedata_last_time <= DE2_70_SOPC_clock_0_out_writedata;
    end


  //DE2_70_SOPC_clock_0_out_writedata matches last port_name, which is an e_process
  always @(posedge clk)
    begin
      if (active_and_waiting_last_time & (DE2_70_SOPC_clock_0_out_writedata != DE2_70_SOPC_clock_0_out_writedata_last_time) & DE2_70_SOPC_clock_0_out_write)
        begin
          $write("%0d ns: DE2_70_SOPC_clock_0_out_writedata did not heed wait!!!", $time);
          $stop;
        end
    end



//////////////// END SIMULATION-ONLY CONTENTS

//synthesis translate_on

endmodule


// synthesis translate_off
`timescale 1ns / 1ps
// synthesis translate_on

// turn off superfluous verilog processor warnings 
// altera message_level Level1 
// altera message_off 10034 10035 10036 10037 10230 10240 10030 

module DE2_70_SOPC_clock_1_in_arbitrator (
                                           // inputs:
                                            DE2_70_SOPC_clock_1_in_endofpacket,
                                            DE2_70_SOPC_clock_1_in_readdata,
                                            DE2_70_SOPC_clock_1_in_waitrequest,
                                            clk,
                                            clock_crossing_0_m1_address_to_slave,
                                            clock_crossing_0_m1_byteenable,
                                            clock_crossing_0_m1_latency_counter,
                                            clock_crossing_0_m1_nativeaddress,
                                            clock_crossing_0_m1_read,
                                            clock_crossing_0_m1_write,
                                            clock_crossing_0_m1_writedata,
                                            reset_n,

                                           // outputs:
                                            DE2_70_SOPC_clock_1_in_address,
                                            DE2_70_SOPC_clock_1_in_byteenable,
                                            DE2_70_SOPC_clock_1_in_endofpacket_from_sa,
                                            DE2_70_SOPC_clock_1_in_nativeaddress,
                                            DE2_70_SOPC_clock_1_in_read,
                                            DE2_70_SOPC_clock_1_in_readdata_from_sa,
                                            DE2_70_SOPC_clock_1_in_reset_n,
                                            DE2_70_SOPC_clock_1_in_waitrequest_from_sa,
                                            DE2_70_SOPC_clock_1_in_write,
                                            DE2_70_SOPC_clock_1_in_writedata,
                                            clock_crossing_0_m1_granted_DE2_70_SOPC_clock_1_in,
                                            clock_crossing_0_m1_qualified_request_DE2_70_SOPC_clock_1_in,
                                            clock_crossing_0_m1_read_data_valid_DE2_70_SOPC_clock_1_in,
                                            clock_crossing_0_m1_requests_DE2_70_SOPC_clock_1_in,
                                            d1_DE2_70_SOPC_clock_1_in_end_xfer
                                         )
;

  output  [  1: 0] DE2_70_SOPC_clock_1_in_address;
  output  [  1: 0] DE2_70_SOPC_clock_1_in_byteenable;
  output           DE2_70_SOPC_clock_1_in_endofpacket_from_sa;
  output           DE2_70_SOPC_clock_1_in_nativeaddress;
  output           DE2_70_SOPC_clock_1_in_read;
  output  [ 15: 0] DE2_70_SOPC_clock_1_in_readdata_from_sa;
  output           DE2_70_SOPC_clock_1_in_reset_n;
  output           DE2_70_SOPC_clock_1_in_waitrequest_from_sa;
  output           DE2_70_SOPC_clock_1_in_write;
  output  [ 15: 0] DE2_70_SOPC_clock_1_in_writedata;
  output           clock_crossing_0_m1_granted_DE2_70_SOPC_clock_1_in;
  output           clock_crossing_0_m1_qualified_request_DE2_70_SOPC_clock_1_in;
  output           clock_crossing_0_m1_read_data_valid_DE2_70_SOPC_clock_1_in;
  output           clock_crossing_0_m1_requests_DE2_70_SOPC_clock_1_in;
  output           d1_DE2_70_SOPC_clock_1_in_end_xfer;
  input            DE2_70_SOPC_clock_1_in_endofpacket;
  input   [ 15: 0] DE2_70_SOPC_clock_1_in_readdata;
  input            DE2_70_SOPC_clock_1_in_waitrequest;
  input            clk;
  input   [  2: 0] clock_crossing_0_m1_address_to_slave;
  input   [  3: 0] clock_crossing_0_m1_byteenable;
  input            clock_crossing_0_m1_latency_counter;
  input            clock_crossing_0_m1_nativeaddress;
  input            clock_crossing_0_m1_read;
  input            clock_crossing_0_m1_write;
  input   [ 31: 0] clock_crossing_0_m1_writedata;
  input            reset_n;

  wire    [  1: 0] DE2_70_SOPC_clock_1_in_address;
  wire             DE2_70_SOPC_clock_1_in_allgrants;
  wire             DE2_70_SOPC_clock_1_in_allow_new_arb_cycle;
  wire             DE2_70_SOPC_clock_1_in_any_bursting_master_saved_grant;
  wire             DE2_70_SOPC_clock_1_in_any_continuerequest;
  wire             DE2_70_SOPC_clock_1_in_arb_counter_enable;
  reg              DE2_70_SOPC_clock_1_in_arb_share_counter;
  wire             DE2_70_SOPC_clock_1_in_arb_share_counter_next_value;
  wire             DE2_70_SOPC_clock_1_in_arb_share_set_values;
  wire             DE2_70_SOPC_clock_1_in_beginbursttransfer_internal;
  wire             DE2_70_SOPC_clock_1_in_begins_xfer;
  wire    [  1: 0] DE2_70_SOPC_clock_1_in_byteenable;
  wire             DE2_70_SOPC_clock_1_in_end_xfer;
  wire             DE2_70_SOPC_clock_1_in_endofpacket_from_sa;
  wire             DE2_70_SOPC_clock_1_in_firsttransfer;
  wire             DE2_70_SOPC_clock_1_in_grant_vector;
  wire             DE2_70_SOPC_clock_1_in_in_a_read_cycle;
  wire             DE2_70_SOPC_clock_1_in_in_a_write_cycle;
  wire             DE2_70_SOPC_clock_1_in_master_qreq_vector;
  wire             DE2_70_SOPC_clock_1_in_nativeaddress;
  wire             DE2_70_SOPC_clock_1_in_non_bursting_master_requests;
  wire             DE2_70_SOPC_clock_1_in_read;
  wire    [ 15: 0] DE2_70_SOPC_clock_1_in_readdata_from_sa;
  reg              DE2_70_SOPC_clock_1_in_reg_firsttransfer;
  wire             DE2_70_SOPC_clock_1_in_reset_n;
  reg              DE2_70_SOPC_clock_1_in_slavearbiterlockenable;
  wire             DE2_70_SOPC_clock_1_in_slavearbiterlockenable2;
  wire             DE2_70_SOPC_clock_1_in_unreg_firsttransfer;
  wire             DE2_70_SOPC_clock_1_in_waitrequest_from_sa;
  wire             DE2_70_SOPC_clock_1_in_waits_for_read;
  wire             DE2_70_SOPC_clock_1_in_waits_for_write;
  wire             DE2_70_SOPC_clock_1_in_write;
  wire    [ 15: 0] DE2_70_SOPC_clock_1_in_writedata;
  wire             clock_crossing_0_m1_arbiterlock;
  wire             clock_crossing_0_m1_arbiterlock2;
  wire             clock_crossing_0_m1_continuerequest;
  wire             clock_crossing_0_m1_granted_DE2_70_SOPC_clock_1_in;
  wire             clock_crossing_0_m1_qualified_request_DE2_70_SOPC_clock_1_in;
  wire             clock_crossing_0_m1_read_data_valid_DE2_70_SOPC_clock_1_in;
  wire             clock_crossing_0_m1_requests_DE2_70_SOPC_clock_1_in;
  wire             clock_crossing_0_m1_saved_grant_DE2_70_SOPC_clock_1_in;
  reg              d1_DE2_70_SOPC_clock_1_in_end_xfer;
  reg              d1_reasons_to_wait;
  reg              enable_nonzero_assertions;
  wire             end_xfer_arb_share_counter_term_DE2_70_SOPC_clock_1_in;
  wire             in_a_read_cycle;
  wire             in_a_write_cycle;
  wire             wait_for_DE2_70_SOPC_clock_1_in_counter;
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          d1_reasons_to_wait <= 0;
      else 
        d1_reasons_to_wait <= ~DE2_70_SOPC_clock_1_in_end_xfer;
    end


  assign DE2_70_SOPC_clock_1_in_begins_xfer = ~d1_reasons_to_wait & ((clock_crossing_0_m1_qualified_request_DE2_70_SOPC_clock_1_in));
  //assign DE2_70_SOPC_clock_1_in_readdata_from_sa = DE2_70_SOPC_clock_1_in_readdata so that symbol knows where to group signals which may go to master only, which is an e_assign
  assign DE2_70_SOPC_clock_1_in_readdata_from_sa = DE2_70_SOPC_clock_1_in_readdata;

  assign clock_crossing_0_m1_requests_DE2_70_SOPC_clock_1_in = (1) & (clock_crossing_0_m1_read | clock_crossing_0_m1_write);
  //assign DE2_70_SOPC_clock_1_in_waitrequest_from_sa = DE2_70_SOPC_clock_1_in_waitrequest so that symbol knows where to group signals which may go to master only, which is an e_assign
  assign DE2_70_SOPC_clock_1_in_waitrequest_from_sa = DE2_70_SOPC_clock_1_in_waitrequest;

  //DE2_70_SOPC_clock_1_in_arb_share_counter set values, which is an e_mux
  assign DE2_70_SOPC_clock_1_in_arb_share_set_values = 1;

  //DE2_70_SOPC_clock_1_in_non_bursting_master_requests mux, which is an e_mux
  assign DE2_70_SOPC_clock_1_in_non_bursting_master_requests = clock_crossing_0_m1_requests_DE2_70_SOPC_clock_1_in;

  //DE2_70_SOPC_clock_1_in_any_bursting_master_saved_grant mux, which is an e_mux
  assign DE2_70_SOPC_clock_1_in_any_bursting_master_saved_grant = 0;

  //DE2_70_SOPC_clock_1_in_arb_share_counter_next_value assignment, which is an e_assign
  assign DE2_70_SOPC_clock_1_in_arb_share_counter_next_value = DE2_70_SOPC_clock_1_in_firsttransfer ? (DE2_70_SOPC_clock_1_in_arb_share_set_values - 1) : |DE2_70_SOPC_clock_1_in_arb_share_counter ? (DE2_70_SOPC_clock_1_in_arb_share_counter - 1) : 0;

  //DE2_70_SOPC_clock_1_in_allgrants all slave grants, which is an e_mux
  assign DE2_70_SOPC_clock_1_in_allgrants = |DE2_70_SOPC_clock_1_in_grant_vector;

  //DE2_70_SOPC_clock_1_in_end_xfer assignment, which is an e_assign
  assign DE2_70_SOPC_clock_1_in_end_xfer = ~(DE2_70_SOPC_clock_1_in_waits_for_read | DE2_70_SOPC_clock_1_in_waits_for_write);

  //end_xfer_arb_share_counter_term_DE2_70_SOPC_clock_1_in arb share counter enable term, which is an e_assign
  assign end_xfer_arb_share_counter_term_DE2_70_SOPC_clock_1_in = DE2_70_SOPC_clock_1_in_end_xfer & (~DE2_70_SOPC_clock_1_in_any_bursting_master_saved_grant | in_a_read_cycle | in_a_write_cycle);

  //DE2_70_SOPC_clock_1_in_arb_share_counter arbitration counter enable, which is an e_assign
  assign DE2_70_SOPC_clock_1_in_arb_counter_enable = (end_xfer_arb_share_counter_term_DE2_70_SOPC_clock_1_in & DE2_70_SOPC_clock_1_in_allgrants) | (end_xfer_arb_share_counter_term_DE2_70_SOPC_clock_1_in & ~DE2_70_SOPC_clock_1_in_non_bursting_master_requests);

  //DE2_70_SOPC_clock_1_in_arb_share_counter counter, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          DE2_70_SOPC_clock_1_in_arb_share_counter <= 0;
      else if (DE2_70_SOPC_clock_1_in_arb_counter_enable)
          DE2_70_SOPC_clock_1_in_arb_share_counter <= DE2_70_SOPC_clock_1_in_arb_share_counter_next_value;
    end


  //DE2_70_SOPC_clock_1_in_slavearbiterlockenable slave enables arbiterlock, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          DE2_70_SOPC_clock_1_in_slavearbiterlockenable <= 0;
      else if ((|DE2_70_SOPC_clock_1_in_master_qreq_vector & end_xfer_arb_share_counter_term_DE2_70_SOPC_clock_1_in) | (end_xfer_arb_share_counter_term_DE2_70_SOPC_clock_1_in & ~DE2_70_SOPC_clock_1_in_non_bursting_master_requests))
          DE2_70_SOPC_clock_1_in_slavearbiterlockenable <= |DE2_70_SOPC_clock_1_in_arb_share_counter_next_value;
    end


  //clock_crossing_0/m1 DE2_70_SOPC_clock_1/in arbiterlock, which is an e_assign
  assign clock_crossing_0_m1_arbiterlock = DE2_70_SOPC_clock_1_in_slavearbiterlockenable & clock_crossing_0_m1_continuerequest;

  //DE2_70_SOPC_clock_1_in_slavearbiterlockenable2 slave enables arbiterlock2, which is an e_assign
  assign DE2_70_SOPC_clock_1_in_slavearbiterlockenable2 = |DE2_70_SOPC_clock_1_in_arb_share_counter_next_value;

  //clock_crossing_0/m1 DE2_70_SOPC_clock_1/in arbiterlock2, which is an e_assign
  assign clock_crossing_0_m1_arbiterlock2 = DE2_70_SOPC_clock_1_in_slavearbiterlockenable2 & clock_crossing_0_m1_continuerequest;

  //DE2_70_SOPC_clock_1_in_any_continuerequest at least one master continues requesting, which is an e_assign
  assign DE2_70_SOPC_clock_1_in_any_continuerequest = 1;

  //clock_crossing_0_m1_continuerequest continued request, which is an e_assign
  assign clock_crossing_0_m1_continuerequest = 1;

  assign clock_crossing_0_m1_qualified_request_DE2_70_SOPC_clock_1_in = clock_crossing_0_m1_requests_DE2_70_SOPC_clock_1_in & ~((clock_crossing_0_m1_read & ((clock_crossing_0_m1_latency_counter != 0))));
  //local readdatavalid clock_crossing_0_m1_read_data_valid_DE2_70_SOPC_clock_1_in, which is an e_mux
  assign clock_crossing_0_m1_read_data_valid_DE2_70_SOPC_clock_1_in = clock_crossing_0_m1_granted_DE2_70_SOPC_clock_1_in & clock_crossing_0_m1_read & ~DE2_70_SOPC_clock_1_in_waits_for_read;

  //DE2_70_SOPC_clock_1_in_writedata mux, which is an e_mux
  assign DE2_70_SOPC_clock_1_in_writedata = clock_crossing_0_m1_writedata;

  //assign DE2_70_SOPC_clock_1_in_endofpacket_from_sa = DE2_70_SOPC_clock_1_in_endofpacket so that symbol knows where to group signals which may go to master only, which is an e_assign
  assign DE2_70_SOPC_clock_1_in_endofpacket_from_sa = DE2_70_SOPC_clock_1_in_endofpacket;

  //master is always granted when requested
  assign clock_crossing_0_m1_granted_DE2_70_SOPC_clock_1_in = clock_crossing_0_m1_qualified_request_DE2_70_SOPC_clock_1_in;

  //clock_crossing_0/m1 saved-grant DE2_70_SOPC_clock_1/in, which is an e_assign
  assign clock_crossing_0_m1_saved_grant_DE2_70_SOPC_clock_1_in = clock_crossing_0_m1_requests_DE2_70_SOPC_clock_1_in;

  //allow new arb cycle for DE2_70_SOPC_clock_1/in, which is an e_assign
  assign DE2_70_SOPC_clock_1_in_allow_new_arb_cycle = 1;

  //placeholder chosen master
  assign DE2_70_SOPC_clock_1_in_grant_vector = 1;

  //placeholder vector of master qualified-requests
  assign DE2_70_SOPC_clock_1_in_master_qreq_vector = 1;

  //DE2_70_SOPC_clock_1_in_reset_n assignment, which is an e_assign
  assign DE2_70_SOPC_clock_1_in_reset_n = reset_n;

  //DE2_70_SOPC_clock_1_in_firsttransfer first transaction, which is an e_assign
  assign DE2_70_SOPC_clock_1_in_firsttransfer = DE2_70_SOPC_clock_1_in_begins_xfer ? DE2_70_SOPC_clock_1_in_unreg_firsttransfer : DE2_70_SOPC_clock_1_in_reg_firsttransfer;

  //DE2_70_SOPC_clock_1_in_unreg_firsttransfer first transaction, which is an e_assign
  assign DE2_70_SOPC_clock_1_in_unreg_firsttransfer = ~(DE2_70_SOPC_clock_1_in_slavearbiterlockenable & DE2_70_SOPC_clock_1_in_any_continuerequest);

  //DE2_70_SOPC_clock_1_in_reg_firsttransfer first transaction, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          DE2_70_SOPC_clock_1_in_reg_firsttransfer <= 1'b1;
      else if (DE2_70_SOPC_clock_1_in_begins_xfer)
          DE2_70_SOPC_clock_1_in_reg_firsttransfer <= DE2_70_SOPC_clock_1_in_unreg_firsttransfer;
    end


  //DE2_70_SOPC_clock_1_in_beginbursttransfer_internal begin burst transfer, which is an e_assign
  assign DE2_70_SOPC_clock_1_in_beginbursttransfer_internal = DE2_70_SOPC_clock_1_in_begins_xfer;

  //DE2_70_SOPC_clock_1_in_read assignment, which is an e_mux
  assign DE2_70_SOPC_clock_1_in_read = clock_crossing_0_m1_granted_DE2_70_SOPC_clock_1_in & clock_crossing_0_m1_read;

  //DE2_70_SOPC_clock_1_in_write assignment, which is an e_mux
  assign DE2_70_SOPC_clock_1_in_write = clock_crossing_0_m1_granted_DE2_70_SOPC_clock_1_in & clock_crossing_0_m1_write;

  //DE2_70_SOPC_clock_1_in_address mux, which is an e_mux
  assign DE2_70_SOPC_clock_1_in_address = clock_crossing_0_m1_address_to_slave;

  //slaveid DE2_70_SOPC_clock_1_in_nativeaddress nativeaddress mux, which is an e_mux
  assign DE2_70_SOPC_clock_1_in_nativeaddress = clock_crossing_0_m1_nativeaddress;

  //d1_DE2_70_SOPC_clock_1_in_end_xfer register, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          d1_DE2_70_SOPC_clock_1_in_end_xfer <= 1;
      else 
        d1_DE2_70_SOPC_clock_1_in_end_xfer <= DE2_70_SOPC_clock_1_in_end_xfer;
    end


  //DE2_70_SOPC_clock_1_in_waits_for_read in a cycle, which is an e_mux
  assign DE2_70_SOPC_clock_1_in_waits_for_read = DE2_70_SOPC_clock_1_in_in_a_read_cycle & DE2_70_SOPC_clock_1_in_waitrequest_from_sa;

  //DE2_70_SOPC_clock_1_in_in_a_read_cycle assignment, which is an e_assign
  assign DE2_70_SOPC_clock_1_in_in_a_read_cycle = clock_crossing_0_m1_granted_DE2_70_SOPC_clock_1_in & clock_crossing_0_m1_read;

  //in_a_read_cycle assignment, which is an e_mux
  assign in_a_read_cycle = DE2_70_SOPC_clock_1_in_in_a_read_cycle;

  //DE2_70_SOPC_clock_1_in_waits_for_write in a cycle, which is an e_mux
  assign DE2_70_SOPC_clock_1_in_waits_for_write = DE2_70_SOPC_clock_1_in_in_a_write_cycle & DE2_70_SOPC_clock_1_in_waitrequest_from_sa;

  //DE2_70_SOPC_clock_1_in_in_a_write_cycle assignment, which is an e_assign
  assign DE2_70_SOPC_clock_1_in_in_a_write_cycle = clock_crossing_0_m1_granted_DE2_70_SOPC_clock_1_in & clock_crossing_0_m1_write;

  //in_a_write_cycle assignment, which is an e_mux
  assign in_a_write_cycle = DE2_70_SOPC_clock_1_in_in_a_write_cycle;

  assign wait_for_DE2_70_SOPC_clock_1_in_counter = 0;
  //DE2_70_SOPC_clock_1_in_byteenable byte enable port mux, which is an e_mux
  assign DE2_70_SOPC_clock_1_in_byteenable = (clock_crossing_0_m1_granted_DE2_70_SOPC_clock_1_in)? clock_crossing_0_m1_byteenable :
    -1;


//synthesis translate_off
//////////////// SIMULATION-ONLY CONTENTS
  //DE2_70_SOPC_clock_1/in enable non-zero assertions, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          enable_nonzero_assertions <= 0;
      else 
        enable_nonzero_assertions <= 1'b1;
    end



//////////////// END SIMULATION-ONLY CONTENTS

//synthesis translate_on

endmodule


// synthesis translate_off
`timescale 1ns / 1ps
// synthesis translate_on

// turn off superfluous verilog processor warnings 
// altera message_level Level1 
// altera message_off 10034 10035 10036 10037 10230 10240 10030 

module DE2_70_SOPC_clock_1_out_arbitrator (
                                            // inputs:
                                             DE2_70_SOPC_clock_1_out_address,
                                             DE2_70_SOPC_clock_1_out_byteenable,
                                             DE2_70_SOPC_clock_1_out_granted_DM9000A_s1,
                                             DE2_70_SOPC_clock_1_out_qualified_request_DM9000A_s1,
                                             DE2_70_SOPC_clock_1_out_read,
                                             DE2_70_SOPC_clock_1_out_read_data_valid_DM9000A_s1,
                                             DE2_70_SOPC_clock_1_out_requests_DM9000A_s1,
                                             DE2_70_SOPC_clock_1_out_write,
                                             DE2_70_SOPC_clock_1_out_writedata,
                                             DM9000A_s1_readdata_from_sa,
                                             DM9000A_s1_wait_counter_eq_0,
                                             clk,
                                             d1_DM9000A_s1_end_xfer,
                                             reset_n,

                                            // outputs:
                                             DE2_70_SOPC_clock_1_out_address_to_slave,
                                             DE2_70_SOPC_clock_1_out_readdata,
                                             DE2_70_SOPC_clock_1_out_reset_n,
                                             DE2_70_SOPC_clock_1_out_waitrequest
                                          )
;

  output  [  1: 0] DE2_70_SOPC_clock_1_out_address_to_slave;
  output  [ 15: 0] DE2_70_SOPC_clock_1_out_readdata;
  output           DE2_70_SOPC_clock_1_out_reset_n;
  output           DE2_70_SOPC_clock_1_out_waitrequest;
  input   [  1: 0] DE2_70_SOPC_clock_1_out_address;
  input   [  1: 0] DE2_70_SOPC_clock_1_out_byteenable;
  input            DE2_70_SOPC_clock_1_out_granted_DM9000A_s1;
  input            DE2_70_SOPC_clock_1_out_qualified_request_DM9000A_s1;
  input            DE2_70_SOPC_clock_1_out_read;
  input            DE2_70_SOPC_clock_1_out_read_data_valid_DM9000A_s1;
  input            DE2_70_SOPC_clock_1_out_requests_DM9000A_s1;
  input            DE2_70_SOPC_clock_1_out_write;
  input   [ 15: 0] DE2_70_SOPC_clock_1_out_writedata;
  input   [ 15: 0] DM9000A_s1_readdata_from_sa;
  input            DM9000A_s1_wait_counter_eq_0;
  input            clk;
  input            d1_DM9000A_s1_end_xfer;
  input            reset_n;

  reg     [  1: 0] DE2_70_SOPC_clock_1_out_address_last_time;
  wire    [  1: 0] DE2_70_SOPC_clock_1_out_address_to_slave;
  reg     [  1: 0] DE2_70_SOPC_clock_1_out_byteenable_last_time;
  reg              DE2_70_SOPC_clock_1_out_read_last_time;
  wire    [ 15: 0] DE2_70_SOPC_clock_1_out_readdata;
  wire             DE2_70_SOPC_clock_1_out_reset_n;
  wire             DE2_70_SOPC_clock_1_out_run;
  wire             DE2_70_SOPC_clock_1_out_waitrequest;
  reg              DE2_70_SOPC_clock_1_out_write_last_time;
  reg     [ 15: 0] DE2_70_SOPC_clock_1_out_writedata_last_time;
  reg              active_and_waiting_last_time;
  wire             r_0;
  //r_0 master_run cascaded wait assignment, which is an e_assign
  assign r_0 = 1 & ((~DE2_70_SOPC_clock_1_out_qualified_request_DM9000A_s1 | ~DE2_70_SOPC_clock_1_out_read | (1 & ((DM9000A_s1_wait_counter_eq_0 & ~d1_DM9000A_s1_end_xfer)) & DE2_70_SOPC_clock_1_out_read))) & ((~DE2_70_SOPC_clock_1_out_qualified_request_DM9000A_s1 | ~DE2_70_SOPC_clock_1_out_write | (1 & ((DM9000A_s1_wait_counter_eq_0 & ~d1_DM9000A_s1_end_xfer)) & DE2_70_SOPC_clock_1_out_write)));

  //cascaded wait assignment, which is an e_assign
  assign DE2_70_SOPC_clock_1_out_run = r_0;

  //optimize select-logic by passing only those address bits which matter.
  assign DE2_70_SOPC_clock_1_out_address_to_slave = DE2_70_SOPC_clock_1_out_address;

  //DE2_70_SOPC_clock_1/out readdata mux, which is an e_mux
  assign DE2_70_SOPC_clock_1_out_readdata = DM9000A_s1_readdata_from_sa;

  //actual waitrequest port, which is an e_assign
  assign DE2_70_SOPC_clock_1_out_waitrequest = ~DE2_70_SOPC_clock_1_out_run;

  //DE2_70_SOPC_clock_1_out_reset_n assignment, which is an e_assign
  assign DE2_70_SOPC_clock_1_out_reset_n = reset_n;


//synthesis translate_off
//////////////// SIMULATION-ONLY CONTENTS
  //DE2_70_SOPC_clock_1_out_address check against wait, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          DE2_70_SOPC_clock_1_out_address_last_time <= 0;
      else 
        DE2_70_SOPC_clock_1_out_address_last_time <= DE2_70_SOPC_clock_1_out_address;
    end


  //DE2_70_SOPC_clock_1/out waited last time, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          active_and_waiting_last_time <= 0;
      else 
        active_and_waiting_last_time <= DE2_70_SOPC_clock_1_out_waitrequest & (DE2_70_SOPC_clock_1_out_read | DE2_70_SOPC_clock_1_out_write);
    end


  //DE2_70_SOPC_clock_1_out_address matches last port_name, which is an e_process
  always @(posedge clk)
    begin
      if (active_and_waiting_last_time & (DE2_70_SOPC_clock_1_out_address != DE2_70_SOPC_clock_1_out_address_last_time))
        begin
          $write("%0d ns: DE2_70_SOPC_clock_1_out_address did not heed wait!!!", $time);
          $stop;
        end
    end


  //DE2_70_SOPC_clock_1_out_byteenable check against wait, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          DE2_70_SOPC_clock_1_out_byteenable_last_time <= 0;
      else 
        DE2_70_SOPC_clock_1_out_byteenable_last_time <= DE2_70_SOPC_clock_1_out_byteenable;
    end


  //DE2_70_SOPC_clock_1_out_byteenable matches last port_name, which is an e_process
  always @(posedge clk)
    begin
      if (active_and_waiting_last_time & (DE2_70_SOPC_clock_1_out_byteenable != DE2_70_SOPC_clock_1_out_byteenable_last_time))
        begin
          $write("%0d ns: DE2_70_SOPC_clock_1_out_byteenable did not heed wait!!!", $time);
          $stop;
        end
    end


  //DE2_70_SOPC_clock_1_out_read check against wait, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          DE2_70_SOPC_clock_1_out_read_last_time <= 0;
      else 
        DE2_70_SOPC_clock_1_out_read_last_time <= DE2_70_SOPC_clock_1_out_read;
    end


  //DE2_70_SOPC_clock_1_out_read matches last port_name, which is an e_process
  always @(posedge clk)
    begin
      if (active_and_waiting_last_time & (DE2_70_SOPC_clock_1_out_read != DE2_70_SOPC_clock_1_out_read_last_time))
        begin
          $write("%0d ns: DE2_70_SOPC_clock_1_out_read did not heed wait!!!", $time);
          $stop;
        end
    end


  //DE2_70_SOPC_clock_1_out_write check against wait, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          DE2_70_SOPC_clock_1_out_write_last_time <= 0;
      else 
        DE2_70_SOPC_clock_1_out_write_last_time <= DE2_70_SOPC_clock_1_out_write;
    end


  //DE2_70_SOPC_clock_1_out_write matches last port_name, which is an e_process
  always @(posedge clk)
    begin
      if (active_and_waiting_last_time & (DE2_70_SOPC_clock_1_out_write != DE2_70_SOPC_clock_1_out_write_last_time))
        begin
          $write("%0d ns: DE2_70_SOPC_clock_1_out_write did not heed wait!!!", $time);
          $stop;
        end
    end


  //DE2_70_SOPC_clock_1_out_writedata check against wait, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          DE2_70_SOPC_clock_1_out_writedata_last_time <= 0;
      else 
        DE2_70_SOPC_clock_1_out_writedata_last_time <= DE2_70_SOPC_clock_1_out_writedata;
    end


  //DE2_70_SOPC_clock_1_out_writedata matches last port_name, which is an e_process
  always @(posedge clk)
    begin
      if (active_and_waiting_last_time & (DE2_70_SOPC_clock_1_out_writedata != DE2_70_SOPC_clock_1_out_writedata_last_time) & DE2_70_SOPC_clock_1_out_write)
        begin
          $write("%0d ns: DE2_70_SOPC_clock_1_out_writedata did not heed wait!!!", $time);
          $stop;
        end
    end



//////////////// END SIMULATION-ONLY CONTENTS

//synthesis translate_on

endmodule


// synthesis translate_off
`timescale 1ns / 1ps
// synthesis translate_on

// turn off superfluous verilog processor warnings 
// altera message_level Level1 
// altera message_off 10034 10035 10036 10037 10230 10240 10030 

module DE2_70_SOPC_clock_2_in_arbitrator (
                                           // inputs:
                                            DE2_70_SOPC_clock_2_in_endofpacket,
                                            DE2_70_SOPC_clock_2_in_readdata,
                                            DE2_70_SOPC_clock_2_in_waitrequest,
                                            clk,
                                            cpu_data_master_address_to_slave,
                                            cpu_data_master_byteenable,
                                            cpu_data_master_latency_counter,
                                            cpu_data_master_read,
                                            cpu_data_master_write,
                                            cpu_data_master_writedata,
                                            reset_n,

                                           // outputs:
                                            DE2_70_SOPC_clock_2_in_address,
                                            DE2_70_SOPC_clock_2_in_byteenable,
                                            DE2_70_SOPC_clock_2_in_endofpacket_from_sa,
                                            DE2_70_SOPC_clock_2_in_nativeaddress,
                                            DE2_70_SOPC_clock_2_in_read,
                                            DE2_70_SOPC_clock_2_in_readdata_from_sa,
                                            DE2_70_SOPC_clock_2_in_reset_n,
                                            DE2_70_SOPC_clock_2_in_waitrequest_from_sa,
                                            DE2_70_SOPC_clock_2_in_write,
                                            DE2_70_SOPC_clock_2_in_writedata,
                                            cpu_data_master_granted_DE2_70_SOPC_clock_2_in,
                                            cpu_data_master_qualified_request_DE2_70_SOPC_clock_2_in,
                                            cpu_data_master_read_data_valid_DE2_70_SOPC_clock_2_in,
                                            cpu_data_master_requests_DE2_70_SOPC_clock_2_in,
                                            d1_DE2_70_SOPC_clock_2_in_end_xfer
                                         )
;

  output  [  2: 0] DE2_70_SOPC_clock_2_in_address;
  output  [  3: 0] DE2_70_SOPC_clock_2_in_byteenable;
  output           DE2_70_SOPC_clock_2_in_endofpacket_from_sa;
  output           DE2_70_SOPC_clock_2_in_nativeaddress;
  output           DE2_70_SOPC_clock_2_in_read;
  output  [ 31: 0] DE2_70_SOPC_clock_2_in_readdata_from_sa;
  output           DE2_70_SOPC_clock_2_in_reset_n;
  output           DE2_70_SOPC_clock_2_in_waitrequest_from_sa;
  output           DE2_70_SOPC_clock_2_in_write;
  output  [ 31: 0] DE2_70_SOPC_clock_2_in_writedata;
  output           cpu_data_master_granted_DE2_70_SOPC_clock_2_in;
  output           cpu_data_master_qualified_request_DE2_70_SOPC_clock_2_in;
  output           cpu_data_master_read_data_valid_DE2_70_SOPC_clock_2_in;
  output           cpu_data_master_requests_DE2_70_SOPC_clock_2_in;
  output           d1_DE2_70_SOPC_clock_2_in_end_xfer;
  input            DE2_70_SOPC_clock_2_in_endofpacket;
  input   [ 31: 0] DE2_70_SOPC_clock_2_in_readdata;
  input            DE2_70_SOPC_clock_2_in_waitrequest;
  input            clk;
  input   [ 24: 0] cpu_data_master_address_to_slave;
  input   [  3: 0] cpu_data_master_byteenable;
  input   [  2: 0] cpu_data_master_latency_counter;
  input            cpu_data_master_read;
  input            cpu_data_master_write;
  input   [ 31: 0] cpu_data_master_writedata;
  input            reset_n;

  wire    [  2: 0] DE2_70_SOPC_clock_2_in_address;
  wire             DE2_70_SOPC_clock_2_in_allgrants;
  wire             DE2_70_SOPC_clock_2_in_allow_new_arb_cycle;
  wire             DE2_70_SOPC_clock_2_in_any_bursting_master_saved_grant;
  wire             DE2_70_SOPC_clock_2_in_any_continuerequest;
  wire             DE2_70_SOPC_clock_2_in_arb_counter_enable;
  reg              DE2_70_SOPC_clock_2_in_arb_share_counter;
  wire             DE2_70_SOPC_clock_2_in_arb_share_counter_next_value;
  wire             DE2_70_SOPC_clock_2_in_arb_share_set_values;
  wire             DE2_70_SOPC_clock_2_in_beginbursttransfer_internal;
  wire             DE2_70_SOPC_clock_2_in_begins_xfer;
  wire    [  3: 0] DE2_70_SOPC_clock_2_in_byteenable;
  wire             DE2_70_SOPC_clock_2_in_end_xfer;
  wire             DE2_70_SOPC_clock_2_in_endofpacket_from_sa;
  wire             DE2_70_SOPC_clock_2_in_firsttransfer;
  wire             DE2_70_SOPC_clock_2_in_grant_vector;
  wire             DE2_70_SOPC_clock_2_in_in_a_read_cycle;
  wire             DE2_70_SOPC_clock_2_in_in_a_write_cycle;
  wire             DE2_70_SOPC_clock_2_in_master_qreq_vector;
  wire             DE2_70_SOPC_clock_2_in_nativeaddress;
  wire             DE2_70_SOPC_clock_2_in_non_bursting_master_requests;
  wire             DE2_70_SOPC_clock_2_in_read;
  wire    [ 31: 0] DE2_70_SOPC_clock_2_in_readdata_from_sa;
  reg              DE2_70_SOPC_clock_2_in_reg_firsttransfer;
  wire             DE2_70_SOPC_clock_2_in_reset_n;
  reg              DE2_70_SOPC_clock_2_in_slavearbiterlockenable;
  wire             DE2_70_SOPC_clock_2_in_slavearbiterlockenable2;
  wire             DE2_70_SOPC_clock_2_in_unreg_firsttransfer;
  wire             DE2_70_SOPC_clock_2_in_waitrequest_from_sa;
  wire             DE2_70_SOPC_clock_2_in_waits_for_read;
  wire             DE2_70_SOPC_clock_2_in_waits_for_write;
  wire             DE2_70_SOPC_clock_2_in_write;
  wire    [ 31: 0] DE2_70_SOPC_clock_2_in_writedata;
  wire             cpu_data_master_arbiterlock;
  wire             cpu_data_master_arbiterlock2;
  wire             cpu_data_master_continuerequest;
  wire             cpu_data_master_granted_DE2_70_SOPC_clock_2_in;
  wire             cpu_data_master_qualified_request_DE2_70_SOPC_clock_2_in;
  wire             cpu_data_master_read_data_valid_DE2_70_SOPC_clock_2_in;
  wire             cpu_data_master_requests_DE2_70_SOPC_clock_2_in;
  wire             cpu_data_master_saved_grant_DE2_70_SOPC_clock_2_in;
  reg              d1_DE2_70_SOPC_clock_2_in_end_xfer;
  reg              d1_reasons_to_wait;
  reg              enable_nonzero_assertions;
  wire             end_xfer_arb_share_counter_term_DE2_70_SOPC_clock_2_in;
  wire             in_a_read_cycle;
  wire             in_a_write_cycle;
  wire             wait_for_DE2_70_SOPC_clock_2_in_counter;
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          d1_reasons_to_wait <= 0;
      else 
        d1_reasons_to_wait <= ~DE2_70_SOPC_clock_2_in_end_xfer;
    end


  assign DE2_70_SOPC_clock_2_in_begins_xfer = ~d1_reasons_to_wait & ((cpu_data_master_qualified_request_DE2_70_SOPC_clock_2_in));
  //assign DE2_70_SOPC_clock_2_in_readdata_from_sa = DE2_70_SOPC_clock_2_in_readdata so that symbol knows where to group signals which may go to master only, which is an e_assign
  assign DE2_70_SOPC_clock_2_in_readdata_from_sa = DE2_70_SOPC_clock_2_in_readdata;

  assign cpu_data_master_requests_DE2_70_SOPC_clock_2_in = ({cpu_data_master_address_to_slave[24 : 3] , 3'b0} == 25'h0) & (cpu_data_master_read | cpu_data_master_write);
  //assign DE2_70_SOPC_clock_2_in_waitrequest_from_sa = DE2_70_SOPC_clock_2_in_waitrequest so that symbol knows where to group signals which may go to master only, which is an e_assign
  assign DE2_70_SOPC_clock_2_in_waitrequest_from_sa = DE2_70_SOPC_clock_2_in_waitrequest;

  //DE2_70_SOPC_clock_2_in_arb_share_counter set values, which is an e_mux
  assign DE2_70_SOPC_clock_2_in_arb_share_set_values = 1;

  //DE2_70_SOPC_clock_2_in_non_bursting_master_requests mux, which is an e_mux
  assign DE2_70_SOPC_clock_2_in_non_bursting_master_requests = cpu_data_master_requests_DE2_70_SOPC_clock_2_in;

  //DE2_70_SOPC_clock_2_in_any_bursting_master_saved_grant mux, which is an e_mux
  assign DE2_70_SOPC_clock_2_in_any_bursting_master_saved_grant = 0;

  //DE2_70_SOPC_clock_2_in_arb_share_counter_next_value assignment, which is an e_assign
  assign DE2_70_SOPC_clock_2_in_arb_share_counter_next_value = DE2_70_SOPC_clock_2_in_firsttransfer ? (DE2_70_SOPC_clock_2_in_arb_share_set_values - 1) : |DE2_70_SOPC_clock_2_in_arb_share_counter ? (DE2_70_SOPC_clock_2_in_arb_share_counter - 1) : 0;

  //DE2_70_SOPC_clock_2_in_allgrants all slave grants, which is an e_mux
  assign DE2_70_SOPC_clock_2_in_allgrants = |DE2_70_SOPC_clock_2_in_grant_vector;

  //DE2_70_SOPC_clock_2_in_end_xfer assignment, which is an e_assign
  assign DE2_70_SOPC_clock_2_in_end_xfer = ~(DE2_70_SOPC_clock_2_in_waits_for_read | DE2_70_SOPC_clock_2_in_waits_for_write);

  //end_xfer_arb_share_counter_term_DE2_70_SOPC_clock_2_in arb share counter enable term, which is an e_assign
  assign end_xfer_arb_share_counter_term_DE2_70_SOPC_clock_2_in = DE2_70_SOPC_clock_2_in_end_xfer & (~DE2_70_SOPC_clock_2_in_any_bursting_master_saved_grant | in_a_read_cycle | in_a_write_cycle);

  //DE2_70_SOPC_clock_2_in_arb_share_counter arbitration counter enable, which is an e_assign
  assign DE2_70_SOPC_clock_2_in_arb_counter_enable = (end_xfer_arb_share_counter_term_DE2_70_SOPC_clock_2_in & DE2_70_SOPC_clock_2_in_allgrants) | (end_xfer_arb_share_counter_term_DE2_70_SOPC_clock_2_in & ~DE2_70_SOPC_clock_2_in_non_bursting_master_requests);

  //DE2_70_SOPC_clock_2_in_arb_share_counter counter, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          DE2_70_SOPC_clock_2_in_arb_share_counter <= 0;
      else if (DE2_70_SOPC_clock_2_in_arb_counter_enable)
          DE2_70_SOPC_clock_2_in_arb_share_counter <= DE2_70_SOPC_clock_2_in_arb_share_counter_next_value;
    end


  //DE2_70_SOPC_clock_2_in_slavearbiterlockenable slave enables arbiterlock, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          DE2_70_SOPC_clock_2_in_slavearbiterlockenable <= 0;
      else if ((|DE2_70_SOPC_clock_2_in_master_qreq_vector & end_xfer_arb_share_counter_term_DE2_70_SOPC_clock_2_in) | (end_xfer_arb_share_counter_term_DE2_70_SOPC_clock_2_in & ~DE2_70_SOPC_clock_2_in_non_bursting_master_requests))
          DE2_70_SOPC_clock_2_in_slavearbiterlockenable <= |DE2_70_SOPC_clock_2_in_arb_share_counter_next_value;
    end


  //cpu/data_master DE2_70_SOPC_clock_2/in arbiterlock, which is an e_assign
  assign cpu_data_master_arbiterlock = DE2_70_SOPC_clock_2_in_slavearbiterlockenable & cpu_data_master_continuerequest;

  //DE2_70_SOPC_clock_2_in_slavearbiterlockenable2 slave enables arbiterlock2, which is an e_assign
  assign DE2_70_SOPC_clock_2_in_slavearbiterlockenable2 = |DE2_70_SOPC_clock_2_in_arb_share_counter_next_value;

  //cpu/data_master DE2_70_SOPC_clock_2/in arbiterlock2, which is an e_assign
  assign cpu_data_master_arbiterlock2 = DE2_70_SOPC_clock_2_in_slavearbiterlockenable2 & cpu_data_master_continuerequest;

  //DE2_70_SOPC_clock_2_in_any_continuerequest at least one master continues requesting, which is an e_assign
  assign DE2_70_SOPC_clock_2_in_any_continuerequest = 1;

  //cpu_data_master_continuerequest continued request, which is an e_assign
  assign cpu_data_master_continuerequest = 1;

  assign cpu_data_master_qualified_request_DE2_70_SOPC_clock_2_in = cpu_data_master_requests_DE2_70_SOPC_clock_2_in & ~((cpu_data_master_read & ((cpu_data_master_latency_counter != 0))));
  //local readdatavalid cpu_data_master_read_data_valid_DE2_70_SOPC_clock_2_in, which is an e_mux
  assign cpu_data_master_read_data_valid_DE2_70_SOPC_clock_2_in = cpu_data_master_granted_DE2_70_SOPC_clock_2_in & cpu_data_master_read & ~DE2_70_SOPC_clock_2_in_waits_for_read;

  //DE2_70_SOPC_clock_2_in_writedata mux, which is an e_mux
  assign DE2_70_SOPC_clock_2_in_writedata = cpu_data_master_writedata;

  //assign DE2_70_SOPC_clock_2_in_endofpacket_from_sa = DE2_70_SOPC_clock_2_in_endofpacket so that symbol knows where to group signals which may go to master only, which is an e_assign
  assign DE2_70_SOPC_clock_2_in_endofpacket_from_sa = DE2_70_SOPC_clock_2_in_endofpacket;

  //master is always granted when requested
  assign cpu_data_master_granted_DE2_70_SOPC_clock_2_in = cpu_data_master_qualified_request_DE2_70_SOPC_clock_2_in;

  //cpu/data_master saved-grant DE2_70_SOPC_clock_2/in, which is an e_assign
  assign cpu_data_master_saved_grant_DE2_70_SOPC_clock_2_in = cpu_data_master_requests_DE2_70_SOPC_clock_2_in;

  //allow new arb cycle for DE2_70_SOPC_clock_2/in, which is an e_assign
  assign DE2_70_SOPC_clock_2_in_allow_new_arb_cycle = 1;

  //placeholder chosen master
  assign DE2_70_SOPC_clock_2_in_grant_vector = 1;

  //placeholder vector of master qualified-requests
  assign DE2_70_SOPC_clock_2_in_master_qreq_vector = 1;

  //DE2_70_SOPC_clock_2_in_reset_n assignment, which is an e_assign
  assign DE2_70_SOPC_clock_2_in_reset_n = reset_n;

  //DE2_70_SOPC_clock_2_in_firsttransfer first transaction, which is an e_assign
  assign DE2_70_SOPC_clock_2_in_firsttransfer = DE2_70_SOPC_clock_2_in_begins_xfer ? DE2_70_SOPC_clock_2_in_unreg_firsttransfer : DE2_70_SOPC_clock_2_in_reg_firsttransfer;

  //DE2_70_SOPC_clock_2_in_unreg_firsttransfer first transaction, which is an e_assign
  assign DE2_70_SOPC_clock_2_in_unreg_firsttransfer = ~(DE2_70_SOPC_clock_2_in_slavearbiterlockenable & DE2_70_SOPC_clock_2_in_any_continuerequest);

  //DE2_70_SOPC_clock_2_in_reg_firsttransfer first transaction, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          DE2_70_SOPC_clock_2_in_reg_firsttransfer <= 1'b1;
      else if (DE2_70_SOPC_clock_2_in_begins_xfer)
          DE2_70_SOPC_clock_2_in_reg_firsttransfer <= DE2_70_SOPC_clock_2_in_unreg_firsttransfer;
    end


  //DE2_70_SOPC_clock_2_in_beginbursttransfer_internal begin burst transfer, which is an e_assign
  assign DE2_70_SOPC_clock_2_in_beginbursttransfer_internal = DE2_70_SOPC_clock_2_in_begins_xfer;

  //DE2_70_SOPC_clock_2_in_read assignment, which is an e_mux
  assign DE2_70_SOPC_clock_2_in_read = cpu_data_master_granted_DE2_70_SOPC_clock_2_in & cpu_data_master_read;

  //DE2_70_SOPC_clock_2_in_write assignment, which is an e_mux
  assign DE2_70_SOPC_clock_2_in_write = cpu_data_master_granted_DE2_70_SOPC_clock_2_in & cpu_data_master_write;

  //DE2_70_SOPC_clock_2_in_address mux, which is an e_mux
  assign DE2_70_SOPC_clock_2_in_address = cpu_data_master_address_to_slave;

  //slaveid DE2_70_SOPC_clock_2_in_nativeaddress nativeaddress mux, which is an e_mux
  assign DE2_70_SOPC_clock_2_in_nativeaddress = cpu_data_master_address_to_slave >> 2;

  //d1_DE2_70_SOPC_clock_2_in_end_xfer register, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          d1_DE2_70_SOPC_clock_2_in_end_xfer <= 1;
      else 
        d1_DE2_70_SOPC_clock_2_in_end_xfer <= DE2_70_SOPC_clock_2_in_end_xfer;
    end


  //DE2_70_SOPC_clock_2_in_waits_for_read in a cycle, which is an e_mux
  assign DE2_70_SOPC_clock_2_in_waits_for_read = DE2_70_SOPC_clock_2_in_in_a_read_cycle & DE2_70_SOPC_clock_2_in_waitrequest_from_sa;

  //DE2_70_SOPC_clock_2_in_in_a_read_cycle assignment, which is an e_assign
  assign DE2_70_SOPC_clock_2_in_in_a_read_cycle = cpu_data_master_granted_DE2_70_SOPC_clock_2_in & cpu_data_master_read;

  //in_a_read_cycle assignment, which is an e_mux
  assign in_a_read_cycle = DE2_70_SOPC_clock_2_in_in_a_read_cycle;

  //DE2_70_SOPC_clock_2_in_waits_for_write in a cycle, which is an e_mux
  assign DE2_70_SOPC_clock_2_in_waits_for_write = DE2_70_SOPC_clock_2_in_in_a_write_cycle & DE2_70_SOPC_clock_2_in_waitrequest_from_sa;

  //DE2_70_SOPC_clock_2_in_in_a_write_cycle assignment, which is an e_assign
  assign DE2_70_SOPC_clock_2_in_in_a_write_cycle = cpu_data_master_granted_DE2_70_SOPC_clock_2_in & cpu_data_master_write;

  //in_a_write_cycle assignment, which is an e_mux
  assign in_a_write_cycle = DE2_70_SOPC_clock_2_in_in_a_write_cycle;

  assign wait_for_DE2_70_SOPC_clock_2_in_counter = 0;
  //DE2_70_SOPC_clock_2_in_byteenable byte enable port mux, which is an e_mux
  assign DE2_70_SOPC_clock_2_in_byteenable = (cpu_data_master_granted_DE2_70_SOPC_clock_2_in)? cpu_data_master_byteenable :
    -1;


//synthesis translate_off
//////////////// SIMULATION-ONLY CONTENTS
  //DE2_70_SOPC_clock_2/in enable non-zero assertions, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          enable_nonzero_assertions <= 0;
      else 
        enable_nonzero_assertions <= 1'b1;
    end



//////////////// END SIMULATION-ONLY CONTENTS

//synthesis translate_on

endmodule


// synthesis translate_off
`timescale 1ns / 1ps
// synthesis translate_on

// turn off superfluous verilog processor warnings 
// altera message_level Level1 
// altera message_off 10034 10035 10036 10037 10230 10240 10030 

module DE2_70_SOPC_clock_2_out_arbitrator (
                                            // inputs:
                                             DE2_70_SOPC_clock_2_out_address,
                                             DE2_70_SOPC_clock_2_out_byteenable,
                                             DE2_70_SOPC_clock_2_out_granted_clock_crossing_0_s1,
                                             DE2_70_SOPC_clock_2_out_qualified_request_clock_crossing_0_s1,
                                             DE2_70_SOPC_clock_2_out_read,
                                             DE2_70_SOPC_clock_2_out_read_data_valid_clock_crossing_0_s1,
                                             DE2_70_SOPC_clock_2_out_read_data_valid_clock_crossing_0_s1_shift_register,
                                             DE2_70_SOPC_clock_2_out_requests_clock_crossing_0_s1,
                                             DE2_70_SOPC_clock_2_out_write,
                                             DE2_70_SOPC_clock_2_out_writedata,
                                             clk,
                                             clock_crossing_0_s1_endofpacket_from_sa,
                                             clock_crossing_0_s1_readdata_from_sa,
                                             clock_crossing_0_s1_waitrequest_from_sa,
                                             d1_clock_crossing_0_s1_end_xfer,
                                             reset_n,

                                            // outputs:
                                             DE2_70_SOPC_clock_2_out_address_to_slave,
                                             DE2_70_SOPC_clock_2_out_endofpacket,
                                             DE2_70_SOPC_clock_2_out_readdata,
                                             DE2_70_SOPC_clock_2_out_reset_n,
                                             DE2_70_SOPC_clock_2_out_waitrequest
                                          )
;

  output  [  2: 0] DE2_70_SOPC_clock_2_out_address_to_slave;
  output           DE2_70_SOPC_clock_2_out_endofpacket;
  output  [ 31: 0] DE2_70_SOPC_clock_2_out_readdata;
  output           DE2_70_SOPC_clock_2_out_reset_n;
  output           DE2_70_SOPC_clock_2_out_waitrequest;
  input   [  2: 0] DE2_70_SOPC_clock_2_out_address;
  input   [  3: 0] DE2_70_SOPC_clock_2_out_byteenable;
  input            DE2_70_SOPC_clock_2_out_granted_clock_crossing_0_s1;
  input            DE2_70_SOPC_clock_2_out_qualified_request_clock_crossing_0_s1;
  input            DE2_70_SOPC_clock_2_out_read;
  input            DE2_70_SOPC_clock_2_out_read_data_valid_clock_crossing_0_s1;
  input            DE2_70_SOPC_clock_2_out_read_data_valid_clock_crossing_0_s1_shift_register;
  input            DE2_70_SOPC_clock_2_out_requests_clock_crossing_0_s1;
  input            DE2_70_SOPC_clock_2_out_write;
  input   [ 31: 0] DE2_70_SOPC_clock_2_out_writedata;
  input            clk;
  input            clock_crossing_0_s1_endofpacket_from_sa;
  input   [ 31: 0] clock_crossing_0_s1_readdata_from_sa;
  input            clock_crossing_0_s1_waitrequest_from_sa;
  input            d1_clock_crossing_0_s1_end_xfer;
  input            reset_n;

  reg     [  2: 0] DE2_70_SOPC_clock_2_out_address_last_time;
  wire    [  2: 0] DE2_70_SOPC_clock_2_out_address_to_slave;
  reg     [  3: 0] DE2_70_SOPC_clock_2_out_byteenable_last_time;
  wire             DE2_70_SOPC_clock_2_out_endofpacket;
  reg              DE2_70_SOPC_clock_2_out_read_last_time;
  wire    [ 31: 0] DE2_70_SOPC_clock_2_out_readdata;
  wire             DE2_70_SOPC_clock_2_out_reset_n;
  wire             DE2_70_SOPC_clock_2_out_run;
  wire             DE2_70_SOPC_clock_2_out_waitrequest;
  reg              DE2_70_SOPC_clock_2_out_write_last_time;
  reg     [ 31: 0] DE2_70_SOPC_clock_2_out_writedata_last_time;
  reg              active_and_waiting_last_time;
  wire             r_0;
  //r_0 master_run cascaded wait assignment, which is an e_assign
  assign r_0 = 1 & (DE2_70_SOPC_clock_2_out_qualified_request_clock_crossing_0_s1 | DE2_70_SOPC_clock_2_out_read_data_valid_clock_crossing_0_s1 | ~DE2_70_SOPC_clock_2_out_requests_clock_crossing_0_s1) & ((~DE2_70_SOPC_clock_2_out_qualified_request_clock_crossing_0_s1 | ~DE2_70_SOPC_clock_2_out_read | (DE2_70_SOPC_clock_2_out_read_data_valid_clock_crossing_0_s1 & DE2_70_SOPC_clock_2_out_read))) & ((~DE2_70_SOPC_clock_2_out_qualified_request_clock_crossing_0_s1 | ~(DE2_70_SOPC_clock_2_out_read | DE2_70_SOPC_clock_2_out_write) | (1 & ~clock_crossing_0_s1_waitrequest_from_sa & (DE2_70_SOPC_clock_2_out_read | DE2_70_SOPC_clock_2_out_write))));

  //cascaded wait assignment, which is an e_assign
  assign DE2_70_SOPC_clock_2_out_run = r_0;

  //optimize select-logic by passing only those address bits which matter.
  assign DE2_70_SOPC_clock_2_out_address_to_slave = DE2_70_SOPC_clock_2_out_address;

  //DE2_70_SOPC_clock_2/out readdata mux, which is an e_mux
  assign DE2_70_SOPC_clock_2_out_readdata = clock_crossing_0_s1_readdata_from_sa;

  //actual waitrequest port, which is an e_assign
  assign DE2_70_SOPC_clock_2_out_waitrequest = ~DE2_70_SOPC_clock_2_out_run;

  //DE2_70_SOPC_clock_2_out_reset_n assignment, which is an e_assign
  assign DE2_70_SOPC_clock_2_out_reset_n = reset_n;

  //mux DE2_70_SOPC_clock_2_out_endofpacket, which is an e_mux
  assign DE2_70_SOPC_clock_2_out_endofpacket = clock_crossing_0_s1_endofpacket_from_sa;


//synthesis translate_off
//////////////// SIMULATION-ONLY CONTENTS
  //DE2_70_SOPC_clock_2_out_address check against wait, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          DE2_70_SOPC_clock_2_out_address_last_time <= 0;
      else 
        DE2_70_SOPC_clock_2_out_address_last_time <= DE2_70_SOPC_clock_2_out_address;
    end


  //DE2_70_SOPC_clock_2/out waited last time, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          active_and_waiting_last_time <= 0;
      else 
        active_and_waiting_last_time <= DE2_70_SOPC_clock_2_out_waitrequest & (DE2_70_SOPC_clock_2_out_read | DE2_70_SOPC_clock_2_out_write);
    end


  //DE2_70_SOPC_clock_2_out_address matches last port_name, which is an e_process
  always @(posedge clk)
    begin
      if (active_and_waiting_last_time & (DE2_70_SOPC_clock_2_out_address != DE2_70_SOPC_clock_2_out_address_last_time))
        begin
          $write("%0d ns: DE2_70_SOPC_clock_2_out_address did not heed wait!!!", $time);
          $stop;
        end
    end


  //DE2_70_SOPC_clock_2_out_byteenable check against wait, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          DE2_70_SOPC_clock_2_out_byteenable_last_time <= 0;
      else 
        DE2_70_SOPC_clock_2_out_byteenable_last_time <= DE2_70_SOPC_clock_2_out_byteenable;
    end


  //DE2_70_SOPC_clock_2_out_byteenable matches last port_name, which is an e_process
  always @(posedge clk)
    begin
      if (active_and_waiting_last_time & (DE2_70_SOPC_clock_2_out_byteenable != DE2_70_SOPC_clock_2_out_byteenable_last_time))
        begin
          $write("%0d ns: DE2_70_SOPC_clock_2_out_byteenable did not heed wait!!!", $time);
          $stop;
        end
    end


  //DE2_70_SOPC_clock_2_out_read check against wait, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          DE2_70_SOPC_clock_2_out_read_last_time <= 0;
      else 
        DE2_70_SOPC_clock_2_out_read_last_time <= DE2_70_SOPC_clock_2_out_read;
    end


  //DE2_70_SOPC_clock_2_out_read matches last port_name, which is an e_process
  always @(posedge clk)
    begin
      if (active_and_waiting_last_time & (DE2_70_SOPC_clock_2_out_read != DE2_70_SOPC_clock_2_out_read_last_time))
        begin
          $write("%0d ns: DE2_70_SOPC_clock_2_out_read did not heed wait!!!", $time);
          $stop;
        end
    end


  //DE2_70_SOPC_clock_2_out_write check against wait, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          DE2_70_SOPC_clock_2_out_write_last_time <= 0;
      else 
        DE2_70_SOPC_clock_2_out_write_last_time <= DE2_70_SOPC_clock_2_out_write;
    end


  //DE2_70_SOPC_clock_2_out_write matches last port_name, which is an e_process
  always @(posedge clk)
    begin
      if (active_and_waiting_last_time & (DE2_70_SOPC_clock_2_out_write != DE2_70_SOPC_clock_2_out_write_last_time))
        begin
          $write("%0d ns: DE2_70_SOPC_clock_2_out_write did not heed wait!!!", $time);
          $stop;
        end
    end


  //DE2_70_SOPC_clock_2_out_writedata check against wait, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          DE2_70_SOPC_clock_2_out_writedata_last_time <= 0;
      else 
        DE2_70_SOPC_clock_2_out_writedata_last_time <= DE2_70_SOPC_clock_2_out_writedata;
    end


  //DE2_70_SOPC_clock_2_out_writedata matches last port_name, which is an e_process
  always @(posedge clk)
    begin
      if (active_and_waiting_last_time & (DE2_70_SOPC_clock_2_out_writedata != DE2_70_SOPC_clock_2_out_writedata_last_time) & DE2_70_SOPC_clock_2_out_write)
        begin
          $write("%0d ns: DE2_70_SOPC_clock_2_out_writedata did not heed wait!!!", $time);
          $stop;
        end
    end



//////////////// END SIMULATION-ONLY CONTENTS

//synthesis translate_on

endmodule


// synthesis translate_off
`timescale 1ns / 1ps
// synthesis translate_on

// turn off superfluous verilog processor warnings 
// altera message_level Level1 
// altera message_off 10034 10035 10036 10037 10230 10240 10030 

module DM9000A_s1_arbitrator (
                               // inputs:
                                DE2_70_SOPC_clock_1_out_address_to_slave,
                                DE2_70_SOPC_clock_1_out_nativeaddress,
                                DE2_70_SOPC_clock_1_out_read,
                                DE2_70_SOPC_clock_1_out_write,
                                DE2_70_SOPC_clock_1_out_writedata,
                                DM9000A_s1_irq,
                                DM9000A_s1_readdata,
                                clk,
                                reset_n,

                               // outputs:
                                DE2_70_SOPC_clock_1_out_granted_DM9000A_s1,
                                DE2_70_SOPC_clock_1_out_qualified_request_DM9000A_s1,
                                DE2_70_SOPC_clock_1_out_read_data_valid_DM9000A_s1,
                                DE2_70_SOPC_clock_1_out_requests_DM9000A_s1,
                                DM9000A_s1_address,
                                DM9000A_s1_chipselect_n,
                                DM9000A_s1_irq_from_sa,
                                DM9000A_s1_read_n,
                                DM9000A_s1_readdata_from_sa,
                                DM9000A_s1_reset_n,
                                DM9000A_s1_wait_counter_eq_0,
                                DM9000A_s1_write_n,
                                DM9000A_s1_writedata,
                                d1_DM9000A_s1_end_xfer
                             )
;

  output           DE2_70_SOPC_clock_1_out_granted_DM9000A_s1;
  output           DE2_70_SOPC_clock_1_out_qualified_request_DM9000A_s1;
  output           DE2_70_SOPC_clock_1_out_read_data_valid_DM9000A_s1;
  output           DE2_70_SOPC_clock_1_out_requests_DM9000A_s1;
  output           DM9000A_s1_address;
  output           DM9000A_s1_chipselect_n;
  output           DM9000A_s1_irq_from_sa;
  output           DM9000A_s1_read_n;
  output  [ 15: 0] DM9000A_s1_readdata_from_sa;
  output           DM9000A_s1_reset_n;
  output           DM9000A_s1_wait_counter_eq_0;
  output           DM9000A_s1_write_n;
  output  [ 15: 0] DM9000A_s1_writedata;
  output           d1_DM9000A_s1_end_xfer;
  input   [  1: 0] DE2_70_SOPC_clock_1_out_address_to_slave;
  input            DE2_70_SOPC_clock_1_out_nativeaddress;
  input            DE2_70_SOPC_clock_1_out_read;
  input            DE2_70_SOPC_clock_1_out_write;
  input   [ 15: 0] DE2_70_SOPC_clock_1_out_writedata;
  input            DM9000A_s1_irq;
  input   [ 15: 0] DM9000A_s1_readdata;
  input            clk;
  input            reset_n;

  wire             DE2_70_SOPC_clock_1_out_arbiterlock;
  wire             DE2_70_SOPC_clock_1_out_arbiterlock2;
  wire             DE2_70_SOPC_clock_1_out_continuerequest;
  wire             DE2_70_SOPC_clock_1_out_granted_DM9000A_s1;
  wire             DE2_70_SOPC_clock_1_out_qualified_request_DM9000A_s1;
  wire             DE2_70_SOPC_clock_1_out_read_data_valid_DM9000A_s1;
  wire             DE2_70_SOPC_clock_1_out_requests_DM9000A_s1;
  wire             DE2_70_SOPC_clock_1_out_saved_grant_DM9000A_s1;
  wire             DM9000A_s1_address;
  wire             DM9000A_s1_allgrants;
  wire             DM9000A_s1_allow_new_arb_cycle;
  wire             DM9000A_s1_any_bursting_master_saved_grant;
  wire             DM9000A_s1_any_continuerequest;
  wire             DM9000A_s1_arb_counter_enable;
  reg              DM9000A_s1_arb_share_counter;
  wire             DM9000A_s1_arb_share_counter_next_value;
  wire             DM9000A_s1_arb_share_set_values;
  wire             DM9000A_s1_beginbursttransfer_internal;
  wire             DM9000A_s1_begins_xfer;
  wire             DM9000A_s1_chipselect_n;
  wire    [  1: 0] DM9000A_s1_counter_load_value;
  wire             DM9000A_s1_end_xfer;
  wire             DM9000A_s1_firsttransfer;
  wire             DM9000A_s1_grant_vector;
  wire             DM9000A_s1_in_a_read_cycle;
  wire             DM9000A_s1_in_a_write_cycle;
  wire             DM9000A_s1_irq_from_sa;
  wire             DM9000A_s1_master_qreq_vector;
  wire             DM9000A_s1_non_bursting_master_requests;
  wire             DM9000A_s1_read_n;
  wire    [ 15: 0] DM9000A_s1_readdata_from_sa;
  reg              DM9000A_s1_reg_firsttransfer;
  wire             DM9000A_s1_reset_n;
  reg              DM9000A_s1_slavearbiterlockenable;
  wire             DM9000A_s1_slavearbiterlockenable2;
  wire             DM9000A_s1_unreg_firsttransfer;
  reg     [  1: 0] DM9000A_s1_wait_counter;
  wire             DM9000A_s1_wait_counter_eq_0;
  wire             DM9000A_s1_waits_for_read;
  wire             DM9000A_s1_waits_for_write;
  wire             DM9000A_s1_write_n;
  wire    [ 15: 0] DM9000A_s1_writedata;
  reg              d1_DM9000A_s1_end_xfer;
  reg              d1_reasons_to_wait;
  reg              enable_nonzero_assertions;
  wire             end_xfer_arb_share_counter_term_DM9000A_s1;
  wire             in_a_read_cycle;
  wire             in_a_write_cycle;
  wire             wait_for_DM9000A_s1_counter;
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          d1_reasons_to_wait <= 0;
      else 
        d1_reasons_to_wait <= ~DM9000A_s1_end_xfer;
    end


  assign DM9000A_s1_begins_xfer = ~d1_reasons_to_wait & ((DE2_70_SOPC_clock_1_out_qualified_request_DM9000A_s1));
  //assign DM9000A_s1_readdata_from_sa = DM9000A_s1_readdata so that symbol knows where to group signals which may go to master only, which is an e_assign
  assign DM9000A_s1_readdata_from_sa = DM9000A_s1_readdata;

  assign DE2_70_SOPC_clock_1_out_requests_DM9000A_s1 = (1) & (DE2_70_SOPC_clock_1_out_read | DE2_70_SOPC_clock_1_out_write);
  //DM9000A_s1_arb_share_counter set values, which is an e_mux
  assign DM9000A_s1_arb_share_set_values = 1;

  //DM9000A_s1_non_bursting_master_requests mux, which is an e_mux
  assign DM9000A_s1_non_bursting_master_requests = DE2_70_SOPC_clock_1_out_requests_DM9000A_s1;

  //DM9000A_s1_any_bursting_master_saved_grant mux, which is an e_mux
  assign DM9000A_s1_any_bursting_master_saved_grant = 0;

  //DM9000A_s1_arb_share_counter_next_value assignment, which is an e_assign
  assign DM9000A_s1_arb_share_counter_next_value = DM9000A_s1_firsttransfer ? (DM9000A_s1_arb_share_set_values - 1) : |DM9000A_s1_arb_share_counter ? (DM9000A_s1_arb_share_counter - 1) : 0;

  //DM9000A_s1_allgrants all slave grants, which is an e_mux
  assign DM9000A_s1_allgrants = |DM9000A_s1_grant_vector;

  //DM9000A_s1_end_xfer assignment, which is an e_assign
  assign DM9000A_s1_end_xfer = ~(DM9000A_s1_waits_for_read | DM9000A_s1_waits_for_write);

  //end_xfer_arb_share_counter_term_DM9000A_s1 arb share counter enable term, which is an e_assign
  assign end_xfer_arb_share_counter_term_DM9000A_s1 = DM9000A_s1_end_xfer & (~DM9000A_s1_any_bursting_master_saved_grant | in_a_read_cycle | in_a_write_cycle);

  //DM9000A_s1_arb_share_counter arbitration counter enable, which is an e_assign
  assign DM9000A_s1_arb_counter_enable = (end_xfer_arb_share_counter_term_DM9000A_s1 & DM9000A_s1_allgrants) | (end_xfer_arb_share_counter_term_DM9000A_s1 & ~DM9000A_s1_non_bursting_master_requests);

  //DM9000A_s1_arb_share_counter counter, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          DM9000A_s1_arb_share_counter <= 0;
      else if (DM9000A_s1_arb_counter_enable)
          DM9000A_s1_arb_share_counter <= DM9000A_s1_arb_share_counter_next_value;
    end


  //DM9000A_s1_slavearbiterlockenable slave enables arbiterlock, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          DM9000A_s1_slavearbiterlockenable <= 0;
      else if ((|DM9000A_s1_master_qreq_vector & end_xfer_arb_share_counter_term_DM9000A_s1) | (end_xfer_arb_share_counter_term_DM9000A_s1 & ~DM9000A_s1_non_bursting_master_requests))
          DM9000A_s1_slavearbiterlockenable <= |DM9000A_s1_arb_share_counter_next_value;
    end


  //DE2_70_SOPC_clock_1/out DM9000A/s1 arbiterlock, which is an e_assign
  assign DE2_70_SOPC_clock_1_out_arbiterlock = DM9000A_s1_slavearbiterlockenable & DE2_70_SOPC_clock_1_out_continuerequest;

  //DM9000A_s1_slavearbiterlockenable2 slave enables arbiterlock2, which is an e_assign
  assign DM9000A_s1_slavearbiterlockenable2 = |DM9000A_s1_arb_share_counter_next_value;

  //DE2_70_SOPC_clock_1/out DM9000A/s1 arbiterlock2, which is an e_assign
  assign DE2_70_SOPC_clock_1_out_arbiterlock2 = DM9000A_s1_slavearbiterlockenable2 & DE2_70_SOPC_clock_1_out_continuerequest;

  //DM9000A_s1_any_continuerequest at least one master continues requesting, which is an e_assign
  assign DM9000A_s1_any_continuerequest = 1;

  //DE2_70_SOPC_clock_1_out_continuerequest continued request, which is an e_assign
  assign DE2_70_SOPC_clock_1_out_continuerequest = 1;

  assign DE2_70_SOPC_clock_1_out_qualified_request_DM9000A_s1 = DE2_70_SOPC_clock_1_out_requests_DM9000A_s1;
  //DM9000A_s1_writedata mux, which is an e_mux
  assign DM9000A_s1_writedata = DE2_70_SOPC_clock_1_out_writedata;

  //master is always granted when requested
  assign DE2_70_SOPC_clock_1_out_granted_DM9000A_s1 = DE2_70_SOPC_clock_1_out_qualified_request_DM9000A_s1;

  //DE2_70_SOPC_clock_1/out saved-grant DM9000A/s1, which is an e_assign
  assign DE2_70_SOPC_clock_1_out_saved_grant_DM9000A_s1 = DE2_70_SOPC_clock_1_out_requests_DM9000A_s1;

  //allow new arb cycle for DM9000A/s1, which is an e_assign
  assign DM9000A_s1_allow_new_arb_cycle = 1;

  //placeholder chosen master
  assign DM9000A_s1_grant_vector = 1;

  //placeholder vector of master qualified-requests
  assign DM9000A_s1_master_qreq_vector = 1;

  //DM9000A_s1_reset_n assignment, which is an e_assign
  assign DM9000A_s1_reset_n = reset_n;

  assign DM9000A_s1_chipselect_n = ~DE2_70_SOPC_clock_1_out_granted_DM9000A_s1;
  //DM9000A_s1_firsttransfer first transaction, which is an e_assign
  assign DM9000A_s1_firsttransfer = DM9000A_s1_begins_xfer ? DM9000A_s1_unreg_firsttransfer : DM9000A_s1_reg_firsttransfer;

  //DM9000A_s1_unreg_firsttransfer first transaction, which is an e_assign
  assign DM9000A_s1_unreg_firsttransfer = ~(DM9000A_s1_slavearbiterlockenable & DM9000A_s1_any_continuerequest);

  //DM9000A_s1_reg_firsttransfer first transaction, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          DM9000A_s1_reg_firsttransfer <= 1'b1;
      else if (DM9000A_s1_begins_xfer)
          DM9000A_s1_reg_firsttransfer <= DM9000A_s1_unreg_firsttransfer;
    end


  //DM9000A_s1_beginbursttransfer_internal begin burst transfer, which is an e_assign
  assign DM9000A_s1_beginbursttransfer_internal = DM9000A_s1_begins_xfer;

  //~DM9000A_s1_read_n assignment, which is an e_mux
  assign DM9000A_s1_read_n = ~(((DE2_70_SOPC_clock_1_out_granted_DM9000A_s1 & DE2_70_SOPC_clock_1_out_read))& ~DM9000A_s1_begins_xfer);

  //~DM9000A_s1_write_n assignment, which is an e_mux
  assign DM9000A_s1_write_n = ~(((DE2_70_SOPC_clock_1_out_granted_DM9000A_s1 & DE2_70_SOPC_clock_1_out_write)) & ~DM9000A_s1_begins_xfer & (DM9000A_s1_wait_counter >= 1));

  //DM9000A_s1_address mux, which is an e_mux
  assign DM9000A_s1_address = DE2_70_SOPC_clock_1_out_nativeaddress;

  //d1_DM9000A_s1_end_xfer register, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          d1_DM9000A_s1_end_xfer <= 1;
      else 
        d1_DM9000A_s1_end_xfer <= DM9000A_s1_end_xfer;
    end


  //DM9000A_s1_waits_for_read in a cycle, which is an e_mux
  assign DM9000A_s1_waits_for_read = DM9000A_s1_in_a_read_cycle & wait_for_DM9000A_s1_counter;

  //DM9000A_s1_in_a_read_cycle assignment, which is an e_assign
  assign DM9000A_s1_in_a_read_cycle = DE2_70_SOPC_clock_1_out_granted_DM9000A_s1 & DE2_70_SOPC_clock_1_out_read;

  //in_a_read_cycle assignment, which is an e_mux
  assign in_a_read_cycle = DM9000A_s1_in_a_read_cycle;

  //DM9000A_s1_waits_for_write in a cycle, which is an e_mux
  assign DM9000A_s1_waits_for_write = DM9000A_s1_in_a_write_cycle & wait_for_DM9000A_s1_counter;

  //DM9000A_s1_in_a_write_cycle assignment, which is an e_assign
  assign DM9000A_s1_in_a_write_cycle = DE2_70_SOPC_clock_1_out_granted_DM9000A_s1 & DE2_70_SOPC_clock_1_out_write;

  //in_a_write_cycle assignment, which is an e_mux
  assign in_a_write_cycle = DM9000A_s1_in_a_write_cycle;

  assign DM9000A_s1_wait_counter_eq_0 = DM9000A_s1_wait_counter == 0;
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          DM9000A_s1_wait_counter <= 0;
      else 
        DM9000A_s1_wait_counter <= DM9000A_s1_counter_load_value;
    end


  assign DM9000A_s1_counter_load_value = ((DM9000A_s1_in_a_write_cycle & DM9000A_s1_begins_xfer))? 2 :
    ((DM9000A_s1_in_a_read_cycle & DM9000A_s1_begins_xfer))? 1 :
    (~DM9000A_s1_wait_counter_eq_0)? DM9000A_s1_wait_counter - 1 :
    0;

  assign wait_for_DM9000A_s1_counter = DM9000A_s1_begins_xfer | ~DM9000A_s1_wait_counter_eq_0;
  //assign DM9000A_s1_irq_from_sa = DM9000A_s1_irq so that symbol knows where to group signals which may go to master only, which is an e_assign
  assign DM9000A_s1_irq_from_sa = DM9000A_s1_irq;


//synthesis translate_off
//////////////// SIMULATION-ONLY CONTENTS
  //DM9000A/s1 enable non-zero assertions, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          enable_nonzero_assertions <= 0;
      else 
        enable_nonzero_assertions <= 1'b1;
    end



//////////////// END SIMULATION-ONLY CONTENTS

//synthesis translate_on

endmodule


// synthesis translate_off
`timescale 1ns / 1ps
// synthesis translate_on

// turn off superfluous verilog processor warnings 
// altera message_level Level1 
// altera message_off 10034 10035 10036 10037 10230 10240 10030 

module SEG7_s1_arbitrator (
                            // inputs:
                             SEG7_s1_readdata,
                             clk,
                             cpu_data_master_address_to_slave,
                             cpu_data_master_byteenable,
                             cpu_data_master_latency_counter,
                             cpu_data_master_read,
                             cpu_data_master_write,
                             cpu_data_master_writedata,
                             reset_n,

                            // outputs:
                             SEG7_s1_address,
                             SEG7_s1_read,
                             SEG7_s1_readdata_from_sa,
                             SEG7_s1_reset,
                             SEG7_s1_write,
                             SEG7_s1_writedata,
                             cpu_data_master_granted_SEG7_s1,
                             cpu_data_master_qualified_request_SEG7_s1,
                             cpu_data_master_read_data_valid_SEG7_s1,
                             cpu_data_master_requests_SEG7_s1,
                             d1_SEG7_s1_end_xfer
                          )
;

  output  [  2: 0] SEG7_s1_address;
  output           SEG7_s1_read;
  output  [  7: 0] SEG7_s1_readdata_from_sa;
  output           SEG7_s1_reset;
  output           SEG7_s1_write;
  output  [  7: 0] SEG7_s1_writedata;
  output           cpu_data_master_granted_SEG7_s1;
  output           cpu_data_master_qualified_request_SEG7_s1;
  output           cpu_data_master_read_data_valid_SEG7_s1;
  output           cpu_data_master_requests_SEG7_s1;
  output           d1_SEG7_s1_end_xfer;
  input   [  7: 0] SEG7_s1_readdata;
  input            clk;
  input   [ 24: 0] cpu_data_master_address_to_slave;
  input   [  3: 0] cpu_data_master_byteenable;
  input   [  2: 0] cpu_data_master_latency_counter;
  input            cpu_data_master_read;
  input            cpu_data_master_write;
  input   [ 31: 0] cpu_data_master_writedata;
  input            reset_n;

  wire    [  2: 0] SEG7_s1_address;
  wire             SEG7_s1_allgrants;
  wire             SEG7_s1_allow_new_arb_cycle;
  wire             SEG7_s1_any_bursting_master_saved_grant;
  wire             SEG7_s1_any_continuerequest;
  wire             SEG7_s1_arb_counter_enable;
  reg              SEG7_s1_arb_share_counter;
  wire             SEG7_s1_arb_share_counter_next_value;
  wire             SEG7_s1_arb_share_set_values;
  wire             SEG7_s1_beginbursttransfer_internal;
  wire             SEG7_s1_begins_xfer;
  wire             SEG7_s1_end_xfer;
  wire             SEG7_s1_firsttransfer;
  wire             SEG7_s1_grant_vector;
  wire             SEG7_s1_in_a_read_cycle;
  wire             SEG7_s1_in_a_write_cycle;
  wire             SEG7_s1_master_qreq_vector;
  wire             SEG7_s1_non_bursting_master_requests;
  wire             SEG7_s1_pretend_byte_enable;
  wire             SEG7_s1_read;
  wire    [  7: 0] SEG7_s1_readdata_from_sa;
  reg              SEG7_s1_reg_firsttransfer;
  wire             SEG7_s1_reset;
  reg              SEG7_s1_slavearbiterlockenable;
  wire             SEG7_s1_slavearbiterlockenable2;
  wire             SEG7_s1_unreg_firsttransfer;
  wire             SEG7_s1_waits_for_read;
  wire             SEG7_s1_waits_for_write;
  wire             SEG7_s1_write;
  wire    [  7: 0] SEG7_s1_writedata;
  wire             cpu_data_master_arbiterlock;
  wire             cpu_data_master_arbiterlock2;
  wire             cpu_data_master_continuerequest;
  wire             cpu_data_master_granted_SEG7_s1;
  wire             cpu_data_master_qualified_request_SEG7_s1;
  wire             cpu_data_master_read_data_valid_SEG7_s1;
  wire             cpu_data_master_requests_SEG7_s1;
  wire             cpu_data_master_saved_grant_SEG7_s1;
  reg              d1_SEG7_s1_end_xfer;
  reg              d1_reasons_to_wait;
  reg              enable_nonzero_assertions;
  wire             end_xfer_arb_share_counter_term_SEG7_s1;
  wire             in_a_read_cycle;
  wire             in_a_write_cycle;
  wire    [ 24: 0] shifted_address_to_SEG7_s1_from_cpu_data_master;
  wire             wait_for_SEG7_s1_counter;
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          d1_reasons_to_wait <= 0;
      else 
        d1_reasons_to_wait <= ~SEG7_s1_end_xfer;
    end


  assign SEG7_s1_begins_xfer = ~d1_reasons_to_wait & ((cpu_data_master_qualified_request_SEG7_s1));
  //assign SEG7_s1_readdata_from_sa = SEG7_s1_readdata so that symbol knows where to group signals which may go to master only, which is an e_assign
  assign SEG7_s1_readdata_from_sa = SEG7_s1_readdata;

  assign cpu_data_master_requests_SEG7_s1 = ({cpu_data_master_address_to_slave[24 : 5] , 5'b0} == 25'h1409060) & (cpu_data_master_read | cpu_data_master_write);
  //SEG7_s1_arb_share_counter set values, which is an e_mux
  assign SEG7_s1_arb_share_set_values = 1;

  //SEG7_s1_non_bursting_master_requests mux, which is an e_mux
  assign SEG7_s1_non_bursting_master_requests = cpu_data_master_requests_SEG7_s1;

  //SEG7_s1_any_bursting_master_saved_grant mux, which is an e_mux
  assign SEG7_s1_any_bursting_master_saved_grant = 0;

  //SEG7_s1_arb_share_counter_next_value assignment, which is an e_assign
  assign SEG7_s1_arb_share_counter_next_value = SEG7_s1_firsttransfer ? (SEG7_s1_arb_share_set_values - 1) : |SEG7_s1_arb_share_counter ? (SEG7_s1_arb_share_counter - 1) : 0;

  //SEG7_s1_allgrants all slave grants, which is an e_mux
  assign SEG7_s1_allgrants = |SEG7_s1_grant_vector;

  //SEG7_s1_end_xfer assignment, which is an e_assign
  assign SEG7_s1_end_xfer = ~(SEG7_s1_waits_for_read | SEG7_s1_waits_for_write);

  //end_xfer_arb_share_counter_term_SEG7_s1 arb share counter enable term, which is an e_assign
  assign end_xfer_arb_share_counter_term_SEG7_s1 = SEG7_s1_end_xfer & (~SEG7_s1_any_bursting_master_saved_grant | in_a_read_cycle | in_a_write_cycle);

  //SEG7_s1_arb_share_counter arbitration counter enable, which is an e_assign
  assign SEG7_s1_arb_counter_enable = (end_xfer_arb_share_counter_term_SEG7_s1 & SEG7_s1_allgrants) | (end_xfer_arb_share_counter_term_SEG7_s1 & ~SEG7_s1_non_bursting_master_requests);

  //SEG7_s1_arb_share_counter counter, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          SEG7_s1_arb_share_counter <= 0;
      else if (SEG7_s1_arb_counter_enable)
          SEG7_s1_arb_share_counter <= SEG7_s1_arb_share_counter_next_value;
    end


  //SEG7_s1_slavearbiterlockenable slave enables arbiterlock, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          SEG7_s1_slavearbiterlockenable <= 0;
      else if ((|SEG7_s1_master_qreq_vector & end_xfer_arb_share_counter_term_SEG7_s1) | (end_xfer_arb_share_counter_term_SEG7_s1 & ~SEG7_s1_non_bursting_master_requests))
          SEG7_s1_slavearbiterlockenable <= |SEG7_s1_arb_share_counter_next_value;
    end


  //cpu/data_master SEG7/s1 arbiterlock, which is an e_assign
  assign cpu_data_master_arbiterlock = SEG7_s1_slavearbiterlockenable & cpu_data_master_continuerequest;

  //SEG7_s1_slavearbiterlockenable2 slave enables arbiterlock2, which is an e_assign
  assign SEG7_s1_slavearbiterlockenable2 = |SEG7_s1_arb_share_counter_next_value;

  //cpu/data_master SEG7/s1 arbiterlock2, which is an e_assign
  assign cpu_data_master_arbiterlock2 = SEG7_s1_slavearbiterlockenable2 & cpu_data_master_continuerequest;

  //SEG7_s1_any_continuerequest at least one master continues requesting, which is an e_assign
  assign SEG7_s1_any_continuerequest = 1;

  //cpu_data_master_continuerequest continued request, which is an e_assign
  assign cpu_data_master_continuerequest = 1;

  assign cpu_data_master_qualified_request_SEG7_s1 = cpu_data_master_requests_SEG7_s1 & ~((cpu_data_master_read & ((cpu_data_master_latency_counter != 0))));
  //local readdatavalid cpu_data_master_read_data_valid_SEG7_s1, which is an e_mux
  assign cpu_data_master_read_data_valid_SEG7_s1 = cpu_data_master_granted_SEG7_s1 & cpu_data_master_read & ~SEG7_s1_waits_for_read;

  //SEG7_s1_writedata mux, which is an e_mux
  assign SEG7_s1_writedata = cpu_data_master_writedata;

  //master is always granted when requested
  assign cpu_data_master_granted_SEG7_s1 = cpu_data_master_qualified_request_SEG7_s1;

  //cpu/data_master saved-grant SEG7/s1, which is an e_assign
  assign cpu_data_master_saved_grant_SEG7_s1 = cpu_data_master_requests_SEG7_s1;

  //allow new arb cycle for SEG7/s1, which is an e_assign
  assign SEG7_s1_allow_new_arb_cycle = 1;

  //placeholder chosen master
  assign SEG7_s1_grant_vector = 1;

  //placeholder vector of master qualified-requests
  assign SEG7_s1_master_qreq_vector = 1;

  //~SEG7_s1_reset assignment, which is an e_assign
  assign SEG7_s1_reset = ~reset_n;

  //SEG7_s1_firsttransfer first transaction, which is an e_assign
  assign SEG7_s1_firsttransfer = SEG7_s1_begins_xfer ? SEG7_s1_unreg_firsttransfer : SEG7_s1_reg_firsttransfer;

  //SEG7_s1_unreg_firsttransfer first transaction, which is an e_assign
  assign SEG7_s1_unreg_firsttransfer = ~(SEG7_s1_slavearbiterlockenable & SEG7_s1_any_continuerequest);

  //SEG7_s1_reg_firsttransfer first transaction, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          SEG7_s1_reg_firsttransfer <= 1'b1;
      else if (SEG7_s1_begins_xfer)
          SEG7_s1_reg_firsttransfer <= SEG7_s1_unreg_firsttransfer;
    end


  //SEG7_s1_beginbursttransfer_internal begin burst transfer, which is an e_assign
  assign SEG7_s1_beginbursttransfer_internal = SEG7_s1_begins_xfer;

  //SEG7_s1_read assignment, which is an e_mux
  assign SEG7_s1_read = cpu_data_master_granted_SEG7_s1 & cpu_data_master_read;

  //SEG7_s1_write assignment, which is an e_mux
  assign SEG7_s1_write = ((cpu_data_master_granted_SEG7_s1 & cpu_data_master_write)) & SEG7_s1_pretend_byte_enable;

  assign shifted_address_to_SEG7_s1_from_cpu_data_master = cpu_data_master_address_to_slave;
  //SEG7_s1_address mux, which is an e_mux
  assign SEG7_s1_address = shifted_address_to_SEG7_s1_from_cpu_data_master >> 2;

  //d1_SEG7_s1_end_xfer register, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          d1_SEG7_s1_end_xfer <= 1;
      else 
        d1_SEG7_s1_end_xfer <= SEG7_s1_end_xfer;
    end


  //SEG7_s1_waits_for_read in a cycle, which is an e_mux
  assign SEG7_s1_waits_for_read = SEG7_s1_in_a_read_cycle & 0;

  //SEG7_s1_in_a_read_cycle assignment, which is an e_assign
  assign SEG7_s1_in_a_read_cycle = cpu_data_master_granted_SEG7_s1 & cpu_data_master_read;

  //in_a_read_cycle assignment, which is an e_mux
  assign in_a_read_cycle = SEG7_s1_in_a_read_cycle;

  //SEG7_s1_waits_for_write in a cycle, which is an e_mux
  assign SEG7_s1_waits_for_write = SEG7_s1_in_a_write_cycle & 0;

  //SEG7_s1_in_a_write_cycle assignment, which is an e_assign
  assign SEG7_s1_in_a_write_cycle = cpu_data_master_granted_SEG7_s1 & cpu_data_master_write;

  //in_a_write_cycle assignment, which is an e_mux
  assign in_a_write_cycle = SEG7_s1_in_a_write_cycle;

  assign wait_for_SEG7_s1_counter = 0;
  //SEG7_s1_pretend_byte_enable byte enable port mux, which is an e_mux
  assign SEG7_s1_pretend_byte_enable = (cpu_data_master_granted_SEG7_s1)? cpu_data_master_byteenable :
    -1;


//synthesis translate_off
//////////////// SIMULATION-ONLY CONTENTS
  //SEG7/s1 enable non-zero assertions, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          enable_nonzero_assertions <= 0;
      else 
        enable_nonzero_assertions <= 1'b1;
    end



//////////////// END SIMULATION-ONLY CONTENTS

//synthesis translate_on

endmodule


// synthesis translate_off
`timescale 1ns / 1ps
// synthesis translate_on

// turn off superfluous verilog processor warnings 
// altera message_level Level1 
// altera message_off 10034 10035 10036 10037 10230 10240 10030 

module rdv_fifo_for_DE2_70_SOPC_clock_2_out_to_clock_crossing_0_s1_module (
                                                                            // inputs:
                                                                             clear_fifo,
                                                                             clk,
                                                                             data_in,
                                                                             read,
                                                                             reset_n,
                                                                             sync_reset,
                                                                             write,

                                                                            // outputs:
                                                                             data_out,
                                                                             empty,
                                                                             fifo_contains_ones_n,
                                                                             full
                                                                          )
;

  output           data_out;
  output           empty;
  output           fifo_contains_ones_n;
  output           full;
  input            clear_fifo;
  input            clk;
  input            data_in;
  input            read;
  input            reset_n;
  input            sync_reset;
  input            write;

  wire             data_out;
  wire             empty;
  reg              fifo_contains_ones_n;
  wire             full;
  reg              full_0;
  reg              full_1;
  reg              full_10;
  reg              full_11;
  reg              full_12;
  reg              full_13;
  reg              full_14;
  reg              full_15;
  wire             full_16;
  reg              full_2;
  reg              full_3;
  reg              full_4;
  reg              full_5;
  reg              full_6;
  reg              full_7;
  reg              full_8;
  reg              full_9;
  reg     [  5: 0] how_many_ones;
  wire    [  5: 0] one_count_minus_one;
  wire    [  5: 0] one_count_plus_one;
  wire             p0_full_0;
  wire             p0_stage_0;
  wire             p10_full_10;
  wire             p10_stage_10;
  wire             p11_full_11;
  wire             p11_stage_11;
  wire             p12_full_12;
  wire             p12_stage_12;
  wire             p13_full_13;
  wire             p13_stage_13;
  wire             p14_full_14;
  wire             p14_stage_14;
  wire             p15_full_15;
  wire             p15_stage_15;
  wire             p1_full_1;
  wire             p1_stage_1;
  wire             p2_full_2;
  wire             p2_stage_2;
  wire             p3_full_3;
  wire             p3_stage_3;
  wire             p4_full_4;
  wire             p4_stage_4;
  wire             p5_full_5;
  wire             p5_stage_5;
  wire             p6_full_6;
  wire             p6_stage_6;
  wire             p7_full_7;
  wire             p7_stage_7;
  wire             p8_full_8;
  wire             p8_stage_8;
  wire             p9_full_9;
  wire             p9_stage_9;
  reg              stage_0;
  reg              stage_1;
  reg              stage_10;
  reg              stage_11;
  reg              stage_12;
  reg              stage_13;
  reg              stage_14;
  reg              stage_15;
  reg              stage_2;
  reg              stage_3;
  reg              stage_4;
  reg              stage_5;
  reg              stage_6;
  reg              stage_7;
  reg              stage_8;
  reg              stage_9;
  wire    [  5: 0] updated_one_count;
  assign data_out = stage_0;
  assign full = full_15;
  assign empty = !full_0;
  assign full_16 = 0;
  //data_15, which is an e_mux
  assign p15_stage_15 = ((full_16 & ~clear_fifo) == 0)? data_in :
    data_in;

  //data_reg_15, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          stage_15 <= 0;
      else if (clear_fifo | sync_reset | read | (write & !full_15))
          if (sync_reset & full_15 & !((full_16 == 0) & read & write))
              stage_15 <= 0;
          else 
            stage_15 <= p15_stage_15;
    end


  //control_15, which is an e_mux
  assign p15_full_15 = ((read & !write) == 0)? full_14 :
    0;

  //control_reg_15, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          full_15 <= 0;
      else if (clear_fifo | (read ^ write) | (write & !full_0))
          if (clear_fifo)
              full_15 <= 0;
          else 
            full_15 <= p15_full_15;
    end


  //data_14, which is an e_mux
  assign p14_stage_14 = ((full_15 & ~clear_fifo) == 0)? data_in :
    stage_15;

  //data_reg_14, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          stage_14 <= 0;
      else if (clear_fifo | sync_reset | read | (write & !full_14))
          if (sync_reset & full_14 & !((full_15 == 0) & read & write))
              stage_14 <= 0;
          else 
            stage_14 <= p14_stage_14;
    end


  //control_14, which is an e_mux
  assign p14_full_14 = ((read & !write) == 0)? full_13 :
    full_15;

  //control_reg_14, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          full_14 <= 0;
      else if (clear_fifo | (read ^ write) | (write & !full_0))
          if (clear_fifo)
              full_14 <= 0;
          else 
            full_14 <= p14_full_14;
    end


  //data_13, which is an e_mux
  assign p13_stage_13 = ((full_14 & ~clear_fifo) == 0)? data_in :
    stage_14;

  //data_reg_13, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          stage_13 <= 0;
      else if (clear_fifo | sync_reset | read | (write & !full_13))
          if (sync_reset & full_13 & !((full_14 == 0) & read & write))
              stage_13 <= 0;
          else 
            stage_13 <= p13_stage_13;
    end


  //control_13, which is an e_mux
  assign p13_full_13 = ((read & !write) == 0)? full_12 :
    full_14;

  //control_reg_13, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          full_13 <= 0;
      else if (clear_fifo | (read ^ write) | (write & !full_0))
          if (clear_fifo)
              full_13 <= 0;
          else 
            full_13 <= p13_full_13;
    end


  //data_12, which is an e_mux
  assign p12_stage_12 = ((full_13 & ~clear_fifo) == 0)? data_in :
    stage_13;

  //data_reg_12, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          stage_12 <= 0;
      else if (clear_fifo | sync_reset | read | (write & !full_12))
          if (sync_reset & full_12 & !((full_13 == 0) & read & write))
              stage_12 <= 0;
          else 
            stage_12 <= p12_stage_12;
    end


  //control_12, which is an e_mux
  assign p12_full_12 = ((read & !write) == 0)? full_11 :
    full_13;

  //control_reg_12, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          full_12 <= 0;
      else if (clear_fifo | (read ^ write) | (write & !full_0))
          if (clear_fifo)
              full_12 <= 0;
          else 
            full_12 <= p12_full_12;
    end


  //data_11, which is an e_mux
  assign p11_stage_11 = ((full_12 & ~clear_fifo) == 0)? data_in :
    stage_12;

  //data_reg_11, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          stage_11 <= 0;
      else if (clear_fifo | sync_reset | read | (write & !full_11))
          if (sync_reset & full_11 & !((full_12 == 0) & read & write))
              stage_11 <= 0;
          else 
            stage_11 <= p11_stage_11;
    end


  //control_11, which is an e_mux
  assign p11_full_11 = ((read & !write) == 0)? full_10 :
    full_12;

  //control_reg_11, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          full_11 <= 0;
      else if (clear_fifo | (read ^ write) | (write & !full_0))
          if (clear_fifo)
              full_11 <= 0;
          else 
            full_11 <= p11_full_11;
    end


  //data_10, which is an e_mux
  assign p10_stage_10 = ((full_11 & ~clear_fifo) == 0)? data_in :
    stage_11;

  //data_reg_10, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          stage_10 <= 0;
      else if (clear_fifo | sync_reset | read | (write & !full_10))
          if (sync_reset & full_10 & !((full_11 == 0) & read & write))
              stage_10 <= 0;
          else 
            stage_10 <= p10_stage_10;
    end


  //control_10, which is an e_mux
  assign p10_full_10 = ((read & !write) == 0)? full_9 :
    full_11;

  //control_reg_10, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          full_10 <= 0;
      else if (clear_fifo | (read ^ write) | (write & !full_0))
          if (clear_fifo)
              full_10 <= 0;
          else 
            full_10 <= p10_full_10;
    end


  //data_9, which is an e_mux
  assign p9_stage_9 = ((full_10 & ~clear_fifo) == 0)? data_in :
    stage_10;

  //data_reg_9, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          stage_9 <= 0;
      else if (clear_fifo | sync_reset | read | (write & !full_9))
          if (sync_reset & full_9 & !((full_10 == 0) & read & write))
              stage_9 <= 0;
          else 
            stage_9 <= p9_stage_9;
    end


  //control_9, which is an e_mux
  assign p9_full_9 = ((read & !write) == 0)? full_8 :
    full_10;

  //control_reg_9, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          full_9 <= 0;
      else if (clear_fifo | (read ^ write) | (write & !full_0))
          if (clear_fifo)
              full_9 <= 0;
          else 
            full_9 <= p9_full_9;
    end


  //data_8, which is an e_mux
  assign p8_stage_8 = ((full_9 & ~clear_fifo) == 0)? data_in :
    stage_9;

  //data_reg_8, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          stage_8 <= 0;
      else if (clear_fifo | sync_reset | read | (write & !full_8))
          if (sync_reset & full_8 & !((full_9 == 0) & read & write))
              stage_8 <= 0;
          else 
            stage_8 <= p8_stage_8;
    end


  //control_8, which is an e_mux
  assign p8_full_8 = ((read & !write) == 0)? full_7 :
    full_9;

  //control_reg_8, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          full_8 <= 0;
      else if (clear_fifo | (read ^ write) | (write & !full_0))
          if (clear_fifo)
              full_8 <= 0;
          else 
            full_8 <= p8_full_8;
    end


  //data_7, which is an e_mux
  assign p7_stage_7 = ((full_8 & ~clear_fifo) == 0)? data_in :
    stage_8;

  //data_reg_7, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          stage_7 <= 0;
      else if (clear_fifo | sync_reset | read | (write & !full_7))
          if (sync_reset & full_7 & !((full_8 == 0) & read & write))
              stage_7 <= 0;
          else 
            stage_7 <= p7_stage_7;
    end


  //control_7, which is an e_mux
  assign p7_full_7 = ((read & !write) == 0)? full_6 :
    full_8;

  //control_reg_7, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          full_7 <= 0;
      else if (clear_fifo | (read ^ write) | (write & !full_0))
          if (clear_fifo)
              full_7 <= 0;
          else 
            full_7 <= p7_full_7;
    end


  //data_6, which is an e_mux
  assign p6_stage_6 = ((full_7 & ~clear_fifo) == 0)? data_in :
    stage_7;

  //data_reg_6, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          stage_6 <= 0;
      else if (clear_fifo | sync_reset | read | (write & !full_6))
          if (sync_reset & full_6 & !((full_7 == 0) & read & write))
              stage_6 <= 0;
          else 
            stage_6 <= p6_stage_6;
    end


  //control_6, which is an e_mux
  assign p6_full_6 = ((read & !write) == 0)? full_5 :
    full_7;

  //control_reg_6, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          full_6 <= 0;
      else if (clear_fifo | (read ^ write) | (write & !full_0))
          if (clear_fifo)
              full_6 <= 0;
          else 
            full_6 <= p6_full_6;
    end


  //data_5, which is an e_mux
  assign p5_stage_5 = ((full_6 & ~clear_fifo) == 0)? data_in :
    stage_6;

  //data_reg_5, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          stage_5 <= 0;
      else if (clear_fifo | sync_reset | read | (write & !full_5))
          if (sync_reset & full_5 & !((full_6 == 0) & read & write))
              stage_5 <= 0;
          else 
            stage_5 <= p5_stage_5;
    end


  //control_5, which is an e_mux
  assign p5_full_5 = ((read & !write) == 0)? full_4 :
    full_6;

  //control_reg_5, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          full_5 <= 0;
      else if (clear_fifo | (read ^ write) | (write & !full_0))
          if (clear_fifo)
              full_5 <= 0;
          else 
            full_5 <= p5_full_5;
    end


  //data_4, which is an e_mux
  assign p4_stage_4 = ((full_5 & ~clear_fifo) == 0)? data_in :
    stage_5;

  //data_reg_4, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          stage_4 <= 0;
      else if (clear_fifo | sync_reset | read | (write & !full_4))
          if (sync_reset & full_4 & !((full_5 == 0) & read & write))
              stage_4 <= 0;
          else 
            stage_4 <= p4_stage_4;
    end


  //control_4, which is an e_mux
  assign p4_full_4 = ((read & !write) == 0)? full_3 :
    full_5;

  //control_reg_4, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          full_4 <= 0;
      else if (clear_fifo | (read ^ write) | (write & !full_0))
          if (clear_fifo)
              full_4 <= 0;
          else 
            full_4 <= p4_full_4;
    end


  //data_3, which is an e_mux
  assign p3_stage_3 = ((full_4 & ~clear_fifo) == 0)? data_in :
    stage_4;

  //data_reg_3, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          stage_3 <= 0;
      else if (clear_fifo | sync_reset | read | (write & !full_3))
          if (sync_reset & full_3 & !((full_4 == 0) & read & write))
              stage_3 <= 0;
          else 
            stage_3 <= p3_stage_3;
    end


  //control_3, which is an e_mux
  assign p3_full_3 = ((read & !write) == 0)? full_2 :
    full_4;

  //control_reg_3, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          full_3 <= 0;
      else if (clear_fifo | (read ^ write) | (write & !full_0))
          if (clear_fifo)
              full_3 <= 0;
          else 
            full_3 <= p3_full_3;
    end


  //data_2, which is an e_mux
  assign p2_stage_2 = ((full_3 & ~clear_fifo) == 0)? data_in :
    stage_3;

  //data_reg_2, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          stage_2 <= 0;
      else if (clear_fifo | sync_reset | read | (write & !full_2))
          if (sync_reset & full_2 & !((full_3 == 0) & read & write))
              stage_2 <= 0;
          else 
            stage_2 <= p2_stage_2;
    end


  //control_2, which is an e_mux
  assign p2_full_2 = ((read & !write) == 0)? full_1 :
    full_3;

  //control_reg_2, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          full_2 <= 0;
      else if (clear_fifo | (read ^ write) | (write & !full_0))
          if (clear_fifo)
              full_2 <= 0;
          else 
            full_2 <= p2_full_2;
    end


  //data_1, which is an e_mux
  assign p1_stage_1 = ((full_2 & ~clear_fifo) == 0)? data_in :
    stage_2;

  //data_reg_1, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          stage_1 <= 0;
      else if (clear_fifo | sync_reset | read | (write & !full_1))
          if (sync_reset & full_1 & !((full_2 == 0) & read & write))
              stage_1 <= 0;
          else 
            stage_1 <= p1_stage_1;
    end


  //control_1, which is an e_mux
  assign p1_full_1 = ((read & !write) == 0)? full_0 :
    full_2;

  //control_reg_1, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          full_1 <= 0;
      else if (clear_fifo | (read ^ write) | (write & !full_0))
          if (clear_fifo)
              full_1 <= 0;
          else 
            full_1 <= p1_full_1;
    end


  //data_0, which is an e_mux
  assign p0_stage_0 = ((full_1 & ~clear_fifo) == 0)? data_in :
    stage_1;

  //data_reg_0, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          stage_0 <= 0;
      else if (clear_fifo | sync_reset | read | (write & !full_0))
          if (sync_reset & full_0 & !((full_1 == 0) & read & write))
              stage_0 <= 0;
          else 
            stage_0 <= p0_stage_0;
    end


  //control_0, which is an e_mux
  assign p0_full_0 = ((read & !write) == 0)? 1 :
    full_1;

  //control_reg_0, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          full_0 <= 0;
      else if (clear_fifo | (read ^ write) | (write & !full_0))
          if (clear_fifo & ~write)
              full_0 <= 0;
          else 
            full_0 <= p0_full_0;
    end


  assign one_count_plus_one = how_many_ones + 1;
  assign one_count_minus_one = how_many_ones - 1;
  //updated_one_count, which is an e_mux
  assign updated_one_count = ((((clear_fifo | sync_reset) & !write)))? 0 :
    ((((clear_fifo | sync_reset) & write)))? |data_in :
    ((read & (|data_in) & write & (|stage_0)))? how_many_ones :
    ((write & (|data_in)))? one_count_plus_one :
    ((read & (|stage_0)))? one_count_minus_one :
    how_many_ones;

  //counts how many ones in the data pipeline, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          how_many_ones <= 0;
      else if (clear_fifo | sync_reset | read | write)
          how_many_ones <= updated_one_count;
    end


  //this fifo contains ones in the data pipeline, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          fifo_contains_ones_n <= 1;
      else if (clear_fifo | sync_reset | read | write)
          fifo_contains_ones_n <= ~(|updated_one_count);
    end



endmodule


// synthesis translate_off
`timescale 1ns / 1ps
// synthesis translate_on

// turn off superfluous verilog processor warnings 
// altera message_level Level1 
// altera message_off 10034 10035 10036 10037 10230 10240 10030 

module clock_crossing_0_s1_arbitrator (
                                        // inputs:
                                         DE2_70_SOPC_clock_2_out_address_to_slave,
                                         DE2_70_SOPC_clock_2_out_byteenable,
                                         DE2_70_SOPC_clock_2_out_nativeaddress,
                                         DE2_70_SOPC_clock_2_out_read,
                                         DE2_70_SOPC_clock_2_out_write,
                                         DE2_70_SOPC_clock_2_out_writedata,
                                         clk,
                                         clock_crossing_0_s1_endofpacket,
                                         clock_crossing_0_s1_readdata,
                                         clock_crossing_0_s1_readdatavalid,
                                         clock_crossing_0_s1_waitrequest,
                                         reset_n,

                                        // outputs:
                                         DE2_70_SOPC_clock_2_out_granted_clock_crossing_0_s1,
                                         DE2_70_SOPC_clock_2_out_qualified_request_clock_crossing_0_s1,
                                         DE2_70_SOPC_clock_2_out_read_data_valid_clock_crossing_0_s1,
                                         DE2_70_SOPC_clock_2_out_read_data_valid_clock_crossing_0_s1_shift_register,
                                         DE2_70_SOPC_clock_2_out_requests_clock_crossing_0_s1,
                                         clock_crossing_0_s1_address,
                                         clock_crossing_0_s1_byteenable,
                                         clock_crossing_0_s1_endofpacket_from_sa,
                                         clock_crossing_0_s1_nativeaddress,
                                         clock_crossing_0_s1_read,
                                         clock_crossing_0_s1_readdata_from_sa,
                                         clock_crossing_0_s1_reset_n,
                                         clock_crossing_0_s1_waitrequest_from_sa,
                                         clock_crossing_0_s1_write,
                                         clock_crossing_0_s1_writedata,
                                         d1_clock_crossing_0_s1_end_xfer
                                      )
;

  output           DE2_70_SOPC_clock_2_out_granted_clock_crossing_0_s1;
  output           DE2_70_SOPC_clock_2_out_qualified_request_clock_crossing_0_s1;
  output           DE2_70_SOPC_clock_2_out_read_data_valid_clock_crossing_0_s1;
  output           DE2_70_SOPC_clock_2_out_read_data_valid_clock_crossing_0_s1_shift_register;
  output           DE2_70_SOPC_clock_2_out_requests_clock_crossing_0_s1;
  output           clock_crossing_0_s1_address;
  output  [  3: 0] clock_crossing_0_s1_byteenable;
  output           clock_crossing_0_s1_endofpacket_from_sa;
  output           clock_crossing_0_s1_nativeaddress;
  output           clock_crossing_0_s1_read;
  output  [ 31: 0] clock_crossing_0_s1_readdata_from_sa;
  output           clock_crossing_0_s1_reset_n;
  output           clock_crossing_0_s1_waitrequest_from_sa;
  output           clock_crossing_0_s1_write;
  output  [ 31: 0] clock_crossing_0_s1_writedata;
  output           d1_clock_crossing_0_s1_end_xfer;
  input   [  2: 0] DE2_70_SOPC_clock_2_out_address_to_slave;
  input   [  3: 0] DE2_70_SOPC_clock_2_out_byteenable;
  input            DE2_70_SOPC_clock_2_out_nativeaddress;
  input            DE2_70_SOPC_clock_2_out_read;
  input            DE2_70_SOPC_clock_2_out_write;
  input   [ 31: 0] DE2_70_SOPC_clock_2_out_writedata;
  input            clk;
  input            clock_crossing_0_s1_endofpacket;
  input   [ 31: 0] clock_crossing_0_s1_readdata;
  input            clock_crossing_0_s1_readdatavalid;
  input            clock_crossing_0_s1_waitrequest;
  input            reset_n;

  wire             DE2_70_SOPC_clock_2_out_arbiterlock;
  wire             DE2_70_SOPC_clock_2_out_arbiterlock2;
  wire             DE2_70_SOPC_clock_2_out_continuerequest;
  wire             DE2_70_SOPC_clock_2_out_granted_clock_crossing_0_s1;
  wire             DE2_70_SOPC_clock_2_out_qualified_request_clock_crossing_0_s1;
  wire             DE2_70_SOPC_clock_2_out_rdv_fifo_empty_clock_crossing_0_s1;
  wire             DE2_70_SOPC_clock_2_out_rdv_fifo_output_from_clock_crossing_0_s1;
  wire             DE2_70_SOPC_clock_2_out_read_data_valid_clock_crossing_0_s1;
  wire             DE2_70_SOPC_clock_2_out_read_data_valid_clock_crossing_0_s1_shift_register;
  wire             DE2_70_SOPC_clock_2_out_requests_clock_crossing_0_s1;
  wire             DE2_70_SOPC_clock_2_out_saved_grant_clock_crossing_0_s1;
  wire             clock_crossing_0_s1_address;
  wire             clock_crossing_0_s1_allgrants;
  wire             clock_crossing_0_s1_allow_new_arb_cycle;
  wire             clock_crossing_0_s1_any_bursting_master_saved_grant;
  wire             clock_crossing_0_s1_any_continuerequest;
  wire             clock_crossing_0_s1_arb_counter_enable;
  reg              clock_crossing_0_s1_arb_share_counter;
  wire             clock_crossing_0_s1_arb_share_counter_next_value;
  wire             clock_crossing_0_s1_arb_share_set_values;
  wire             clock_crossing_0_s1_beginbursttransfer_internal;
  wire             clock_crossing_0_s1_begins_xfer;
  wire    [  3: 0] clock_crossing_0_s1_byteenable;
  wire             clock_crossing_0_s1_end_xfer;
  wire             clock_crossing_0_s1_endofpacket_from_sa;
  wire             clock_crossing_0_s1_firsttransfer;
  wire             clock_crossing_0_s1_grant_vector;
  wire             clock_crossing_0_s1_in_a_read_cycle;
  wire             clock_crossing_0_s1_in_a_write_cycle;
  wire             clock_crossing_0_s1_master_qreq_vector;
  wire             clock_crossing_0_s1_move_on_to_next_transaction;
  wire             clock_crossing_0_s1_nativeaddress;
  wire             clock_crossing_0_s1_non_bursting_master_requests;
  wire             clock_crossing_0_s1_read;
  wire    [ 31: 0] clock_crossing_0_s1_readdata_from_sa;
  wire             clock_crossing_0_s1_readdatavalid_from_sa;
  reg              clock_crossing_0_s1_reg_firsttransfer;
  wire             clock_crossing_0_s1_reset_n;
  reg              clock_crossing_0_s1_slavearbiterlockenable;
  wire             clock_crossing_0_s1_slavearbiterlockenable2;
  wire             clock_crossing_0_s1_unreg_firsttransfer;
  wire             clock_crossing_0_s1_waitrequest_from_sa;
  wire             clock_crossing_0_s1_waits_for_read;
  wire             clock_crossing_0_s1_waits_for_write;
  wire             clock_crossing_0_s1_write;
  wire    [ 31: 0] clock_crossing_0_s1_writedata;
  reg              d1_clock_crossing_0_s1_end_xfer;
  reg              d1_reasons_to_wait;
  reg              enable_nonzero_assertions;
  wire             end_xfer_arb_share_counter_term_clock_crossing_0_s1;
  wire             in_a_read_cycle;
  wire             in_a_write_cycle;
  wire    [  2: 0] shifted_address_to_clock_crossing_0_s1_from_DE2_70_SOPC_clock_2_out;
  wire             wait_for_clock_crossing_0_s1_counter;
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          d1_reasons_to_wait <= 0;
      else 
        d1_reasons_to_wait <= ~clock_crossing_0_s1_end_xfer;
    end


  assign clock_crossing_0_s1_begins_xfer = ~d1_reasons_to_wait & ((DE2_70_SOPC_clock_2_out_qualified_request_clock_crossing_0_s1));
  //assign clock_crossing_0_s1_readdata_from_sa = clock_crossing_0_s1_readdata so that symbol knows where to group signals which may go to master only, which is an e_assign
  assign clock_crossing_0_s1_readdata_from_sa = clock_crossing_0_s1_readdata;

  assign DE2_70_SOPC_clock_2_out_requests_clock_crossing_0_s1 = (1) & (DE2_70_SOPC_clock_2_out_read | DE2_70_SOPC_clock_2_out_write);
  //assign clock_crossing_0_s1_waitrequest_from_sa = clock_crossing_0_s1_waitrequest so that symbol knows where to group signals which may go to master only, which is an e_assign
  assign clock_crossing_0_s1_waitrequest_from_sa = clock_crossing_0_s1_waitrequest;

  //assign clock_crossing_0_s1_readdatavalid_from_sa = clock_crossing_0_s1_readdatavalid so that symbol knows where to group signals which may go to master only, which is an e_assign
  assign clock_crossing_0_s1_readdatavalid_from_sa = clock_crossing_0_s1_readdatavalid;

  //clock_crossing_0_s1_arb_share_counter set values, which is an e_mux
  assign clock_crossing_0_s1_arb_share_set_values = 1;

  //clock_crossing_0_s1_non_bursting_master_requests mux, which is an e_mux
  assign clock_crossing_0_s1_non_bursting_master_requests = DE2_70_SOPC_clock_2_out_requests_clock_crossing_0_s1;

  //clock_crossing_0_s1_any_bursting_master_saved_grant mux, which is an e_mux
  assign clock_crossing_0_s1_any_bursting_master_saved_grant = 0;

  //clock_crossing_0_s1_arb_share_counter_next_value assignment, which is an e_assign
  assign clock_crossing_0_s1_arb_share_counter_next_value = clock_crossing_0_s1_firsttransfer ? (clock_crossing_0_s1_arb_share_set_values - 1) : |clock_crossing_0_s1_arb_share_counter ? (clock_crossing_0_s1_arb_share_counter - 1) : 0;

  //clock_crossing_0_s1_allgrants all slave grants, which is an e_mux
  assign clock_crossing_0_s1_allgrants = |clock_crossing_0_s1_grant_vector;

  //clock_crossing_0_s1_end_xfer assignment, which is an e_assign
  assign clock_crossing_0_s1_end_xfer = ~(clock_crossing_0_s1_waits_for_read | clock_crossing_0_s1_waits_for_write);

  //end_xfer_arb_share_counter_term_clock_crossing_0_s1 arb share counter enable term, which is an e_assign
  assign end_xfer_arb_share_counter_term_clock_crossing_0_s1 = clock_crossing_0_s1_end_xfer & (~clock_crossing_0_s1_any_bursting_master_saved_grant | in_a_read_cycle | in_a_write_cycle);

  //clock_crossing_0_s1_arb_share_counter arbitration counter enable, which is an e_assign
  assign clock_crossing_0_s1_arb_counter_enable = (end_xfer_arb_share_counter_term_clock_crossing_0_s1 & clock_crossing_0_s1_allgrants) | (end_xfer_arb_share_counter_term_clock_crossing_0_s1 & ~clock_crossing_0_s1_non_bursting_master_requests);

  //clock_crossing_0_s1_arb_share_counter counter, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          clock_crossing_0_s1_arb_share_counter <= 0;
      else if (clock_crossing_0_s1_arb_counter_enable)
          clock_crossing_0_s1_arb_share_counter <= clock_crossing_0_s1_arb_share_counter_next_value;
    end


  //clock_crossing_0_s1_slavearbiterlockenable slave enables arbiterlock, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          clock_crossing_0_s1_slavearbiterlockenable <= 0;
      else if ((|clock_crossing_0_s1_master_qreq_vector & end_xfer_arb_share_counter_term_clock_crossing_0_s1) | (end_xfer_arb_share_counter_term_clock_crossing_0_s1 & ~clock_crossing_0_s1_non_bursting_master_requests))
          clock_crossing_0_s1_slavearbiterlockenable <= |clock_crossing_0_s1_arb_share_counter_next_value;
    end


  //DE2_70_SOPC_clock_2/out clock_crossing_0/s1 arbiterlock, which is an e_assign
  assign DE2_70_SOPC_clock_2_out_arbiterlock = clock_crossing_0_s1_slavearbiterlockenable & DE2_70_SOPC_clock_2_out_continuerequest;

  //clock_crossing_0_s1_slavearbiterlockenable2 slave enables arbiterlock2, which is an e_assign
  assign clock_crossing_0_s1_slavearbiterlockenable2 = |clock_crossing_0_s1_arb_share_counter_next_value;

  //DE2_70_SOPC_clock_2/out clock_crossing_0/s1 arbiterlock2, which is an e_assign
  assign DE2_70_SOPC_clock_2_out_arbiterlock2 = clock_crossing_0_s1_slavearbiterlockenable2 & DE2_70_SOPC_clock_2_out_continuerequest;

  //clock_crossing_0_s1_any_continuerequest at least one master continues requesting, which is an e_assign
  assign clock_crossing_0_s1_any_continuerequest = 1;

  //DE2_70_SOPC_clock_2_out_continuerequest continued request, which is an e_assign
  assign DE2_70_SOPC_clock_2_out_continuerequest = 1;

  assign DE2_70_SOPC_clock_2_out_qualified_request_clock_crossing_0_s1 = DE2_70_SOPC_clock_2_out_requests_clock_crossing_0_s1 & ~((DE2_70_SOPC_clock_2_out_read & ((|DE2_70_SOPC_clock_2_out_read_data_valid_clock_crossing_0_s1_shift_register))));
  //unique name for clock_crossing_0_s1_move_on_to_next_transaction, which is an e_assign
  assign clock_crossing_0_s1_move_on_to_next_transaction = clock_crossing_0_s1_readdatavalid_from_sa;

  //rdv_fifo_for_DE2_70_SOPC_clock_2_out_to_clock_crossing_0_s1, which is an e_fifo_with_registered_outputs
  rdv_fifo_for_DE2_70_SOPC_clock_2_out_to_clock_crossing_0_s1_module rdv_fifo_for_DE2_70_SOPC_clock_2_out_to_clock_crossing_0_s1
    (
      .clear_fifo           (1'b0),
      .clk                  (clk),
      .data_in              (DE2_70_SOPC_clock_2_out_granted_clock_crossing_0_s1),
      .data_out             (DE2_70_SOPC_clock_2_out_rdv_fifo_output_from_clock_crossing_0_s1),
      .empty                (),
      .fifo_contains_ones_n (DE2_70_SOPC_clock_2_out_rdv_fifo_empty_clock_crossing_0_s1),
      .full                 (),
      .read                 (clock_crossing_0_s1_move_on_to_next_transaction),
      .reset_n              (reset_n),
      .sync_reset           (1'b0),
      .write                (in_a_read_cycle & ~clock_crossing_0_s1_waits_for_read)
    );

  assign DE2_70_SOPC_clock_2_out_read_data_valid_clock_crossing_0_s1_shift_register = ~DE2_70_SOPC_clock_2_out_rdv_fifo_empty_clock_crossing_0_s1;
  //local readdatavalid DE2_70_SOPC_clock_2_out_read_data_valid_clock_crossing_0_s1, which is an e_mux
  assign DE2_70_SOPC_clock_2_out_read_data_valid_clock_crossing_0_s1 = clock_crossing_0_s1_readdatavalid_from_sa;

  //clock_crossing_0_s1_writedata mux, which is an e_mux
  assign clock_crossing_0_s1_writedata = DE2_70_SOPC_clock_2_out_writedata;

  //assign clock_crossing_0_s1_endofpacket_from_sa = clock_crossing_0_s1_endofpacket so that symbol knows where to group signals which may go to master only, which is an e_assign
  assign clock_crossing_0_s1_endofpacket_from_sa = clock_crossing_0_s1_endofpacket;

  //master is always granted when requested
  assign DE2_70_SOPC_clock_2_out_granted_clock_crossing_0_s1 = DE2_70_SOPC_clock_2_out_qualified_request_clock_crossing_0_s1;

  //DE2_70_SOPC_clock_2/out saved-grant clock_crossing_0/s1, which is an e_assign
  assign DE2_70_SOPC_clock_2_out_saved_grant_clock_crossing_0_s1 = DE2_70_SOPC_clock_2_out_requests_clock_crossing_0_s1;

  //allow new arb cycle for clock_crossing_0/s1, which is an e_assign
  assign clock_crossing_0_s1_allow_new_arb_cycle = 1;

  //placeholder chosen master
  assign clock_crossing_0_s1_grant_vector = 1;

  //placeholder vector of master qualified-requests
  assign clock_crossing_0_s1_master_qreq_vector = 1;

  //clock_crossing_0_s1_reset_n assignment, which is an e_assign
  assign clock_crossing_0_s1_reset_n = reset_n;

  //clock_crossing_0_s1_firsttransfer first transaction, which is an e_assign
  assign clock_crossing_0_s1_firsttransfer = clock_crossing_0_s1_begins_xfer ? clock_crossing_0_s1_unreg_firsttransfer : clock_crossing_0_s1_reg_firsttransfer;

  //clock_crossing_0_s1_unreg_firsttransfer first transaction, which is an e_assign
  assign clock_crossing_0_s1_unreg_firsttransfer = ~(clock_crossing_0_s1_slavearbiterlockenable & clock_crossing_0_s1_any_continuerequest);

  //clock_crossing_0_s1_reg_firsttransfer first transaction, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          clock_crossing_0_s1_reg_firsttransfer <= 1'b1;
      else if (clock_crossing_0_s1_begins_xfer)
          clock_crossing_0_s1_reg_firsttransfer <= clock_crossing_0_s1_unreg_firsttransfer;
    end


  //clock_crossing_0_s1_beginbursttransfer_internal begin burst transfer, which is an e_assign
  assign clock_crossing_0_s1_beginbursttransfer_internal = clock_crossing_0_s1_begins_xfer;

  //clock_crossing_0_s1_read assignment, which is an e_mux
  assign clock_crossing_0_s1_read = DE2_70_SOPC_clock_2_out_granted_clock_crossing_0_s1 & DE2_70_SOPC_clock_2_out_read;

  //clock_crossing_0_s1_write assignment, which is an e_mux
  assign clock_crossing_0_s1_write = DE2_70_SOPC_clock_2_out_granted_clock_crossing_0_s1 & DE2_70_SOPC_clock_2_out_write;

  assign shifted_address_to_clock_crossing_0_s1_from_DE2_70_SOPC_clock_2_out = DE2_70_SOPC_clock_2_out_address_to_slave;
  //clock_crossing_0_s1_address mux, which is an e_mux
  assign clock_crossing_0_s1_address = shifted_address_to_clock_crossing_0_s1_from_DE2_70_SOPC_clock_2_out >> 2;

  //slaveid clock_crossing_0_s1_nativeaddress nativeaddress mux, which is an e_mux
  assign clock_crossing_0_s1_nativeaddress = DE2_70_SOPC_clock_2_out_nativeaddress;

  //d1_clock_crossing_0_s1_end_xfer register, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          d1_clock_crossing_0_s1_end_xfer <= 1;
      else 
        d1_clock_crossing_0_s1_end_xfer <= clock_crossing_0_s1_end_xfer;
    end


  //clock_crossing_0_s1_waits_for_read in a cycle, which is an e_mux
  assign clock_crossing_0_s1_waits_for_read = clock_crossing_0_s1_in_a_read_cycle & clock_crossing_0_s1_waitrequest_from_sa;

  //clock_crossing_0_s1_in_a_read_cycle assignment, which is an e_assign
  assign clock_crossing_0_s1_in_a_read_cycle = DE2_70_SOPC_clock_2_out_granted_clock_crossing_0_s1 & DE2_70_SOPC_clock_2_out_read;

  //in_a_read_cycle assignment, which is an e_mux
  assign in_a_read_cycle = clock_crossing_0_s1_in_a_read_cycle;

  //clock_crossing_0_s1_waits_for_write in a cycle, which is an e_mux
  assign clock_crossing_0_s1_waits_for_write = clock_crossing_0_s1_in_a_write_cycle & clock_crossing_0_s1_waitrequest_from_sa;

  //clock_crossing_0_s1_in_a_write_cycle assignment, which is an e_assign
  assign clock_crossing_0_s1_in_a_write_cycle = DE2_70_SOPC_clock_2_out_granted_clock_crossing_0_s1 & DE2_70_SOPC_clock_2_out_write;

  //in_a_write_cycle assignment, which is an e_mux
  assign in_a_write_cycle = clock_crossing_0_s1_in_a_write_cycle;

  assign wait_for_clock_crossing_0_s1_counter = 0;
  //clock_crossing_0_s1_byteenable byte enable port mux, which is an e_mux
  assign clock_crossing_0_s1_byteenable = (DE2_70_SOPC_clock_2_out_granted_clock_crossing_0_s1)? DE2_70_SOPC_clock_2_out_byteenable :
    -1;


//synthesis translate_off
//////////////// SIMULATION-ONLY CONTENTS
  //clock_crossing_0/s1 enable non-zero assertions, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          enable_nonzero_assertions <= 0;
      else 
        enable_nonzero_assertions <= 1'b1;
    end



//////////////// END SIMULATION-ONLY CONTENTS

//synthesis translate_on

endmodule


// synthesis translate_off
`timescale 1ns / 1ps
// synthesis translate_on

// turn off superfluous verilog processor warnings 
// altera message_level Level1 
// altera message_off 10034 10035 10036 10037 10230 10240 10030 

module clock_crossing_0_m1_arbitrator (
                                        // inputs:
                                         DE2_70_SOPC_clock_1_in_endofpacket_from_sa,
                                         DE2_70_SOPC_clock_1_in_readdata_from_sa,
                                         DE2_70_SOPC_clock_1_in_waitrequest_from_sa,
                                         clk,
                                         clock_crossing_0_m1_address,
                                         clock_crossing_0_m1_byteenable,
                                         clock_crossing_0_m1_granted_DE2_70_SOPC_clock_1_in,
                                         clock_crossing_0_m1_qualified_request_DE2_70_SOPC_clock_1_in,
                                         clock_crossing_0_m1_read,
                                         clock_crossing_0_m1_read_data_valid_DE2_70_SOPC_clock_1_in,
                                         clock_crossing_0_m1_requests_DE2_70_SOPC_clock_1_in,
                                         clock_crossing_0_m1_write,
                                         clock_crossing_0_m1_writedata,
                                         d1_DE2_70_SOPC_clock_1_in_end_xfer,
                                         reset_n,

                                        // outputs:
                                         clock_crossing_0_m1_address_to_slave,
                                         clock_crossing_0_m1_endofpacket,
                                         clock_crossing_0_m1_latency_counter,
                                         clock_crossing_0_m1_readdata,
                                         clock_crossing_0_m1_readdatavalid,
                                         clock_crossing_0_m1_reset_n,
                                         clock_crossing_0_m1_waitrequest
                                      )
;

  output  [  2: 0] clock_crossing_0_m1_address_to_slave;
  output           clock_crossing_0_m1_endofpacket;
  output           clock_crossing_0_m1_latency_counter;
  output  [ 31: 0] clock_crossing_0_m1_readdata;
  output           clock_crossing_0_m1_readdatavalid;
  output           clock_crossing_0_m1_reset_n;
  output           clock_crossing_0_m1_waitrequest;
  input            DE2_70_SOPC_clock_1_in_endofpacket_from_sa;
  input   [ 15: 0] DE2_70_SOPC_clock_1_in_readdata_from_sa;
  input            DE2_70_SOPC_clock_1_in_waitrequest_from_sa;
  input            clk;
  input   [  2: 0] clock_crossing_0_m1_address;
  input   [  3: 0] clock_crossing_0_m1_byteenable;
  input            clock_crossing_0_m1_granted_DE2_70_SOPC_clock_1_in;
  input            clock_crossing_0_m1_qualified_request_DE2_70_SOPC_clock_1_in;
  input            clock_crossing_0_m1_read;
  input            clock_crossing_0_m1_read_data_valid_DE2_70_SOPC_clock_1_in;
  input            clock_crossing_0_m1_requests_DE2_70_SOPC_clock_1_in;
  input            clock_crossing_0_m1_write;
  input   [ 31: 0] clock_crossing_0_m1_writedata;
  input            d1_DE2_70_SOPC_clock_1_in_end_xfer;
  input            reset_n;

  reg              active_and_waiting_last_time;
  reg     [  2: 0] clock_crossing_0_m1_address_last_time;
  wire    [  2: 0] clock_crossing_0_m1_address_to_slave;
  reg     [  3: 0] clock_crossing_0_m1_byteenable_last_time;
  wire             clock_crossing_0_m1_endofpacket;
  wire             clock_crossing_0_m1_latency_counter;
  reg              clock_crossing_0_m1_read_last_time;
  wire    [ 31: 0] clock_crossing_0_m1_readdata;
  wire             clock_crossing_0_m1_readdatavalid;
  wire             clock_crossing_0_m1_reset_n;
  wire             clock_crossing_0_m1_run;
  wire             clock_crossing_0_m1_waitrequest;
  reg              clock_crossing_0_m1_write_last_time;
  reg     [ 31: 0] clock_crossing_0_m1_writedata_last_time;
  wire             pre_flush_clock_crossing_0_m1_readdatavalid;
  wire             r_0;
  //r_0 master_run cascaded wait assignment, which is an e_assign
  assign r_0 = 1 & (clock_crossing_0_m1_qualified_request_DE2_70_SOPC_clock_1_in | ~clock_crossing_0_m1_requests_DE2_70_SOPC_clock_1_in) & ((~clock_crossing_0_m1_qualified_request_DE2_70_SOPC_clock_1_in | ~(clock_crossing_0_m1_read | clock_crossing_0_m1_write) | (1 & ~DE2_70_SOPC_clock_1_in_waitrequest_from_sa & (clock_crossing_0_m1_read | clock_crossing_0_m1_write)))) & ((~clock_crossing_0_m1_qualified_request_DE2_70_SOPC_clock_1_in | ~(clock_crossing_0_m1_read | clock_crossing_0_m1_write) | (1 & ~DE2_70_SOPC_clock_1_in_waitrequest_from_sa & (clock_crossing_0_m1_read | clock_crossing_0_m1_write))));

  //cascaded wait assignment, which is an e_assign
  assign clock_crossing_0_m1_run = r_0;

  //optimize select-logic by passing only those address bits which matter.
  assign clock_crossing_0_m1_address_to_slave = clock_crossing_0_m1_address[2 : 0];

  //latent slave read data valids which may be flushed, which is an e_mux
  assign pre_flush_clock_crossing_0_m1_readdatavalid = 0;

  //latent slave read data valid which is not flushed, which is an e_mux
  assign clock_crossing_0_m1_readdatavalid = 0 |
    pre_flush_clock_crossing_0_m1_readdatavalid |
    clock_crossing_0_m1_read_data_valid_DE2_70_SOPC_clock_1_in;

  //clock_crossing_0/m1 readdata mux, which is an e_mux
  assign clock_crossing_0_m1_readdata = DE2_70_SOPC_clock_1_in_readdata_from_sa;

  //actual waitrequest port, which is an e_assign
  assign clock_crossing_0_m1_waitrequest = ~clock_crossing_0_m1_run;

  //latent max counter, which is an e_assign
  assign clock_crossing_0_m1_latency_counter = 0;

  //clock_crossing_0_m1_reset_n assignment, which is an e_assign
  assign clock_crossing_0_m1_reset_n = reset_n;

  //mux clock_crossing_0_m1_endofpacket, which is an e_mux
  assign clock_crossing_0_m1_endofpacket = DE2_70_SOPC_clock_1_in_endofpacket_from_sa;


//synthesis translate_off
//////////////// SIMULATION-ONLY CONTENTS
  //clock_crossing_0_m1_address check against wait, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          clock_crossing_0_m1_address_last_time <= 0;
      else 
        clock_crossing_0_m1_address_last_time <= clock_crossing_0_m1_address;
    end


  //clock_crossing_0/m1 waited last time, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          active_and_waiting_last_time <= 0;
      else 
        active_and_waiting_last_time <= clock_crossing_0_m1_waitrequest & (clock_crossing_0_m1_read | clock_crossing_0_m1_write);
    end


  //clock_crossing_0_m1_address matches last port_name, which is an e_process
  always @(posedge clk)
    begin
      if (active_and_waiting_last_time & (clock_crossing_0_m1_address != clock_crossing_0_m1_address_last_time))
        begin
          $write("%0d ns: clock_crossing_0_m1_address did not heed wait!!!", $time);
          $stop;
        end
    end


  //clock_crossing_0_m1_byteenable check against wait, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          clock_crossing_0_m1_byteenable_last_time <= 0;
      else 
        clock_crossing_0_m1_byteenable_last_time <= clock_crossing_0_m1_byteenable;
    end


  //clock_crossing_0_m1_byteenable matches last port_name, which is an e_process
  always @(posedge clk)
    begin
      if (active_and_waiting_last_time & (clock_crossing_0_m1_byteenable != clock_crossing_0_m1_byteenable_last_time))
        begin
          $write("%0d ns: clock_crossing_0_m1_byteenable did not heed wait!!!", $time);
          $stop;
        end
    end


  //clock_crossing_0_m1_read check against wait, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          clock_crossing_0_m1_read_last_time <= 0;
      else 
        clock_crossing_0_m1_read_last_time <= clock_crossing_0_m1_read;
    end


  //clock_crossing_0_m1_read matches last port_name, which is an e_process
  always @(posedge clk)
    begin
      if (active_and_waiting_last_time & (clock_crossing_0_m1_read != clock_crossing_0_m1_read_last_time))
        begin
          $write("%0d ns: clock_crossing_0_m1_read did not heed wait!!!", $time);
          $stop;
        end
    end


  //clock_crossing_0_m1_write check against wait, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          clock_crossing_0_m1_write_last_time <= 0;
      else 
        clock_crossing_0_m1_write_last_time <= clock_crossing_0_m1_write;
    end


  //clock_crossing_0_m1_write matches last port_name, which is an e_process
  always @(posedge clk)
    begin
      if (active_and_waiting_last_time & (clock_crossing_0_m1_write != clock_crossing_0_m1_write_last_time))
        begin
          $write("%0d ns: clock_crossing_0_m1_write did not heed wait!!!", $time);
          $stop;
        end
    end


  //clock_crossing_0_m1_writedata check against wait, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          clock_crossing_0_m1_writedata_last_time <= 0;
      else 
        clock_crossing_0_m1_writedata_last_time <= clock_crossing_0_m1_writedata;
    end


  //clock_crossing_0_m1_writedata matches last port_name, which is an e_process
  always @(posedge clk)
    begin
      if (active_and_waiting_last_time & (clock_crossing_0_m1_writedata != clock_crossing_0_m1_writedata_last_time) & clock_crossing_0_m1_write)
        begin
          $write("%0d ns: clock_crossing_0_m1_writedata did not heed wait!!!", $time);
          $stop;
        end
    end



//////////////// END SIMULATION-ONLY CONTENTS

//synthesis translate_on

endmodule


// synthesis translate_off
`timescale 1ns / 1ps
// synthesis translate_on

// turn off superfluous verilog processor warnings 
// altera message_level Level1 
// altera message_off 10034 10035 10036 10037 10230 10240 10030 

module clock_crossing_0_bridge_arbitrator 
;



endmodule


// synthesis translate_off
`timescale 1ns / 1ps
// synthesis translate_on

// turn off superfluous verilog processor warnings 
// altera message_level Level1 
// altera message_off 10034 10035 10036 10037 10230 10240 10030 

module control_in_pio_s1_arbitrator (
                                      // inputs:
                                       clk,
                                       control_in_pio_s1_readdata,
                                       cpu_data_master_address_to_slave,
                                       cpu_data_master_latency_counter,
                                       cpu_data_master_read,
                                       cpu_data_master_write,
                                       cpu_data_master_writedata,
                                       reset_n,

                                      // outputs:
                                       control_in_pio_s1_address,
                                       control_in_pio_s1_chipselect,
                                       control_in_pio_s1_readdata_from_sa,
                                       control_in_pio_s1_reset_n,
                                       control_in_pio_s1_write_n,
                                       control_in_pio_s1_writedata,
                                       cpu_data_master_granted_control_in_pio_s1,
                                       cpu_data_master_qualified_request_control_in_pio_s1,
                                       cpu_data_master_read_data_valid_control_in_pio_s1,
                                       cpu_data_master_requests_control_in_pio_s1,
                                       d1_control_in_pio_s1_end_xfer
                                    )
;

  output  [  1: 0] control_in_pio_s1_address;
  output           control_in_pio_s1_chipselect;
  output  [ 31: 0] control_in_pio_s1_readdata_from_sa;
  output           control_in_pio_s1_reset_n;
  output           control_in_pio_s1_write_n;
  output  [ 31: 0] control_in_pio_s1_writedata;
  output           cpu_data_master_granted_control_in_pio_s1;
  output           cpu_data_master_qualified_request_control_in_pio_s1;
  output           cpu_data_master_read_data_valid_control_in_pio_s1;
  output           cpu_data_master_requests_control_in_pio_s1;
  output           d1_control_in_pio_s1_end_xfer;
  input            clk;
  input   [ 31: 0] control_in_pio_s1_readdata;
  input   [ 24: 0] cpu_data_master_address_to_slave;
  input   [  2: 0] cpu_data_master_latency_counter;
  input            cpu_data_master_read;
  input            cpu_data_master_write;
  input   [ 31: 0] cpu_data_master_writedata;
  input            reset_n;

  wire    [  1: 0] control_in_pio_s1_address;
  wire             control_in_pio_s1_allgrants;
  wire             control_in_pio_s1_allow_new_arb_cycle;
  wire             control_in_pio_s1_any_bursting_master_saved_grant;
  wire             control_in_pio_s1_any_continuerequest;
  wire             control_in_pio_s1_arb_counter_enable;
  reg              control_in_pio_s1_arb_share_counter;
  wire             control_in_pio_s1_arb_share_counter_next_value;
  wire             control_in_pio_s1_arb_share_set_values;
  wire             control_in_pio_s1_beginbursttransfer_internal;
  wire             control_in_pio_s1_begins_xfer;
  wire             control_in_pio_s1_chipselect;
  wire             control_in_pio_s1_end_xfer;
  wire             control_in_pio_s1_firsttransfer;
  wire             control_in_pio_s1_grant_vector;
  wire             control_in_pio_s1_in_a_read_cycle;
  wire             control_in_pio_s1_in_a_write_cycle;
  wire             control_in_pio_s1_master_qreq_vector;
  wire             control_in_pio_s1_non_bursting_master_requests;
  wire    [ 31: 0] control_in_pio_s1_readdata_from_sa;
  reg              control_in_pio_s1_reg_firsttransfer;
  wire             control_in_pio_s1_reset_n;
  reg              control_in_pio_s1_slavearbiterlockenable;
  wire             control_in_pio_s1_slavearbiterlockenable2;
  wire             control_in_pio_s1_unreg_firsttransfer;
  wire             control_in_pio_s1_waits_for_read;
  wire             control_in_pio_s1_waits_for_write;
  wire             control_in_pio_s1_write_n;
  wire    [ 31: 0] control_in_pio_s1_writedata;
  wire             cpu_data_master_arbiterlock;
  wire             cpu_data_master_arbiterlock2;
  wire             cpu_data_master_continuerequest;
  wire             cpu_data_master_granted_control_in_pio_s1;
  wire             cpu_data_master_qualified_request_control_in_pio_s1;
  wire             cpu_data_master_read_data_valid_control_in_pio_s1;
  wire             cpu_data_master_requests_control_in_pio_s1;
  wire             cpu_data_master_saved_grant_control_in_pio_s1;
  reg              d1_control_in_pio_s1_end_xfer;
  reg              d1_reasons_to_wait;
  reg              enable_nonzero_assertions;
  wire             end_xfer_arb_share_counter_term_control_in_pio_s1;
  wire             in_a_read_cycle;
  wire             in_a_write_cycle;
  wire    [ 24: 0] shifted_address_to_control_in_pio_s1_from_cpu_data_master;
  wire             wait_for_control_in_pio_s1_counter;
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          d1_reasons_to_wait <= 0;
      else 
        d1_reasons_to_wait <= ~control_in_pio_s1_end_xfer;
    end


  assign control_in_pio_s1_begins_xfer = ~d1_reasons_to_wait & ((cpu_data_master_qualified_request_control_in_pio_s1));
  //assign control_in_pio_s1_readdata_from_sa = control_in_pio_s1_readdata so that symbol knows where to group signals which may go to master only, which is an e_assign
  assign control_in_pio_s1_readdata_from_sa = control_in_pio_s1_readdata;

  assign cpu_data_master_requests_control_in_pio_s1 = ({cpu_data_master_address_to_slave[24 : 4] , 4'b0} == 25'h1409100) & (cpu_data_master_read | cpu_data_master_write);
  //control_in_pio_s1_arb_share_counter set values, which is an e_mux
  assign control_in_pio_s1_arb_share_set_values = 1;

  //control_in_pio_s1_non_bursting_master_requests mux, which is an e_mux
  assign control_in_pio_s1_non_bursting_master_requests = cpu_data_master_requests_control_in_pio_s1;

  //control_in_pio_s1_any_bursting_master_saved_grant mux, which is an e_mux
  assign control_in_pio_s1_any_bursting_master_saved_grant = 0;

  //control_in_pio_s1_arb_share_counter_next_value assignment, which is an e_assign
  assign control_in_pio_s1_arb_share_counter_next_value = control_in_pio_s1_firsttransfer ? (control_in_pio_s1_arb_share_set_values - 1) : |control_in_pio_s1_arb_share_counter ? (control_in_pio_s1_arb_share_counter - 1) : 0;

  //control_in_pio_s1_allgrants all slave grants, which is an e_mux
  assign control_in_pio_s1_allgrants = |control_in_pio_s1_grant_vector;

  //control_in_pio_s1_end_xfer assignment, which is an e_assign
  assign control_in_pio_s1_end_xfer = ~(control_in_pio_s1_waits_for_read | control_in_pio_s1_waits_for_write);

  //end_xfer_arb_share_counter_term_control_in_pio_s1 arb share counter enable term, which is an e_assign
  assign end_xfer_arb_share_counter_term_control_in_pio_s1 = control_in_pio_s1_end_xfer & (~control_in_pio_s1_any_bursting_master_saved_grant | in_a_read_cycle | in_a_write_cycle);

  //control_in_pio_s1_arb_share_counter arbitration counter enable, which is an e_assign
  assign control_in_pio_s1_arb_counter_enable = (end_xfer_arb_share_counter_term_control_in_pio_s1 & control_in_pio_s1_allgrants) | (end_xfer_arb_share_counter_term_control_in_pio_s1 & ~control_in_pio_s1_non_bursting_master_requests);

  //control_in_pio_s1_arb_share_counter counter, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          control_in_pio_s1_arb_share_counter <= 0;
      else if (control_in_pio_s1_arb_counter_enable)
          control_in_pio_s1_arb_share_counter <= control_in_pio_s1_arb_share_counter_next_value;
    end


  //control_in_pio_s1_slavearbiterlockenable slave enables arbiterlock, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          control_in_pio_s1_slavearbiterlockenable <= 0;
      else if ((|control_in_pio_s1_master_qreq_vector & end_xfer_arb_share_counter_term_control_in_pio_s1) | (end_xfer_arb_share_counter_term_control_in_pio_s1 & ~control_in_pio_s1_non_bursting_master_requests))
          control_in_pio_s1_slavearbiterlockenable <= |control_in_pio_s1_arb_share_counter_next_value;
    end


  //cpu/data_master control_in_pio/s1 arbiterlock, which is an e_assign
  assign cpu_data_master_arbiterlock = control_in_pio_s1_slavearbiterlockenable & cpu_data_master_continuerequest;

  //control_in_pio_s1_slavearbiterlockenable2 slave enables arbiterlock2, which is an e_assign
  assign control_in_pio_s1_slavearbiterlockenable2 = |control_in_pio_s1_arb_share_counter_next_value;

  //cpu/data_master control_in_pio/s1 arbiterlock2, which is an e_assign
  assign cpu_data_master_arbiterlock2 = control_in_pio_s1_slavearbiterlockenable2 & cpu_data_master_continuerequest;

  //control_in_pio_s1_any_continuerequest at least one master continues requesting, which is an e_assign
  assign control_in_pio_s1_any_continuerequest = 1;

  //cpu_data_master_continuerequest continued request, which is an e_assign
  assign cpu_data_master_continuerequest = 1;

  assign cpu_data_master_qualified_request_control_in_pio_s1 = cpu_data_master_requests_control_in_pio_s1 & ~((cpu_data_master_read & ((cpu_data_master_latency_counter != 0))));
  //local readdatavalid cpu_data_master_read_data_valid_control_in_pio_s1, which is an e_mux
  assign cpu_data_master_read_data_valid_control_in_pio_s1 = cpu_data_master_granted_control_in_pio_s1 & cpu_data_master_read & ~control_in_pio_s1_waits_for_read;

  //control_in_pio_s1_writedata mux, which is an e_mux
  assign control_in_pio_s1_writedata = cpu_data_master_writedata;

  //master is always granted when requested
  assign cpu_data_master_granted_control_in_pio_s1 = cpu_data_master_qualified_request_control_in_pio_s1;

  //cpu/data_master saved-grant control_in_pio/s1, which is an e_assign
  assign cpu_data_master_saved_grant_control_in_pio_s1 = cpu_data_master_requests_control_in_pio_s1;

  //allow new arb cycle for control_in_pio/s1, which is an e_assign
  assign control_in_pio_s1_allow_new_arb_cycle = 1;

  //placeholder chosen master
  assign control_in_pio_s1_grant_vector = 1;

  //placeholder vector of master qualified-requests
  assign control_in_pio_s1_master_qreq_vector = 1;

  //control_in_pio_s1_reset_n assignment, which is an e_assign
  assign control_in_pio_s1_reset_n = reset_n;

  assign control_in_pio_s1_chipselect = cpu_data_master_granted_control_in_pio_s1;
  //control_in_pio_s1_firsttransfer first transaction, which is an e_assign
  assign control_in_pio_s1_firsttransfer = control_in_pio_s1_begins_xfer ? control_in_pio_s1_unreg_firsttransfer : control_in_pio_s1_reg_firsttransfer;

  //control_in_pio_s1_unreg_firsttransfer first transaction, which is an e_assign
  assign control_in_pio_s1_unreg_firsttransfer = ~(control_in_pio_s1_slavearbiterlockenable & control_in_pio_s1_any_continuerequest);

  //control_in_pio_s1_reg_firsttransfer first transaction, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          control_in_pio_s1_reg_firsttransfer <= 1'b1;
      else if (control_in_pio_s1_begins_xfer)
          control_in_pio_s1_reg_firsttransfer <= control_in_pio_s1_unreg_firsttransfer;
    end


  //control_in_pio_s1_beginbursttransfer_internal begin burst transfer, which is an e_assign
  assign control_in_pio_s1_beginbursttransfer_internal = control_in_pio_s1_begins_xfer;

  //~control_in_pio_s1_write_n assignment, which is an e_mux
  assign control_in_pio_s1_write_n = ~(cpu_data_master_granted_control_in_pio_s1 & cpu_data_master_write);

  assign shifted_address_to_control_in_pio_s1_from_cpu_data_master = cpu_data_master_address_to_slave;
  //control_in_pio_s1_address mux, which is an e_mux
  assign control_in_pio_s1_address = shifted_address_to_control_in_pio_s1_from_cpu_data_master >> 2;

  //d1_control_in_pio_s1_end_xfer register, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          d1_control_in_pio_s1_end_xfer <= 1;
      else 
        d1_control_in_pio_s1_end_xfer <= control_in_pio_s1_end_xfer;
    end


  //control_in_pio_s1_waits_for_read in a cycle, which is an e_mux
  assign control_in_pio_s1_waits_for_read = control_in_pio_s1_in_a_read_cycle & control_in_pio_s1_begins_xfer;

  //control_in_pio_s1_in_a_read_cycle assignment, which is an e_assign
  assign control_in_pio_s1_in_a_read_cycle = cpu_data_master_granted_control_in_pio_s1 & cpu_data_master_read;

  //in_a_read_cycle assignment, which is an e_mux
  assign in_a_read_cycle = control_in_pio_s1_in_a_read_cycle;

  //control_in_pio_s1_waits_for_write in a cycle, which is an e_mux
  assign control_in_pio_s1_waits_for_write = control_in_pio_s1_in_a_write_cycle & 0;

  //control_in_pio_s1_in_a_write_cycle assignment, which is an e_assign
  assign control_in_pio_s1_in_a_write_cycle = cpu_data_master_granted_control_in_pio_s1 & cpu_data_master_write;

  //in_a_write_cycle assignment, which is an e_mux
  assign in_a_write_cycle = control_in_pio_s1_in_a_write_cycle;

  assign wait_for_control_in_pio_s1_counter = 0;

//synthesis translate_off
//////////////// SIMULATION-ONLY CONTENTS
  //control_in_pio/s1 enable non-zero assertions, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          enable_nonzero_assertions <= 0;
      else 
        enable_nonzero_assertions <= 1'b1;
    end



//////////////// END SIMULATION-ONLY CONTENTS

//synthesis translate_on

endmodule


// synthesis translate_off
`timescale 1ns / 1ps
// synthesis translate_on

// turn off superfluous verilog processor warnings 
// altera message_level Level1 
// altera message_off 10034 10035 10036 10037 10230 10240 10030 

module control_out_pio_s1_arbitrator (
                                       // inputs:
                                        clk,
                                        control_out_pio_s1_readdata,
                                        cpu_data_master_address_to_slave,
                                        cpu_data_master_latency_counter,
                                        cpu_data_master_read,
                                        cpu_data_master_write,
                                        cpu_data_master_writedata,
                                        reset_n,

                                       // outputs:
                                        control_out_pio_s1_address,
                                        control_out_pio_s1_chipselect,
                                        control_out_pio_s1_readdata_from_sa,
                                        control_out_pio_s1_reset_n,
                                        control_out_pio_s1_write_n,
                                        control_out_pio_s1_writedata,
                                        cpu_data_master_granted_control_out_pio_s1,
                                        cpu_data_master_qualified_request_control_out_pio_s1,
                                        cpu_data_master_read_data_valid_control_out_pio_s1,
                                        cpu_data_master_requests_control_out_pio_s1,
                                        d1_control_out_pio_s1_end_xfer
                                     )
;

  output  [  2: 0] control_out_pio_s1_address;
  output           control_out_pio_s1_chipselect;
  output  [ 31: 0] control_out_pio_s1_readdata_from_sa;
  output           control_out_pio_s1_reset_n;
  output           control_out_pio_s1_write_n;
  output  [ 31: 0] control_out_pio_s1_writedata;
  output           cpu_data_master_granted_control_out_pio_s1;
  output           cpu_data_master_qualified_request_control_out_pio_s1;
  output           cpu_data_master_read_data_valid_control_out_pio_s1;
  output           cpu_data_master_requests_control_out_pio_s1;
  output           d1_control_out_pio_s1_end_xfer;
  input            clk;
  input   [ 31: 0] control_out_pio_s1_readdata;
  input   [ 24: 0] cpu_data_master_address_to_slave;
  input   [  2: 0] cpu_data_master_latency_counter;
  input            cpu_data_master_read;
  input            cpu_data_master_write;
  input   [ 31: 0] cpu_data_master_writedata;
  input            reset_n;

  wire    [  2: 0] control_out_pio_s1_address;
  wire             control_out_pio_s1_allgrants;
  wire             control_out_pio_s1_allow_new_arb_cycle;
  wire             control_out_pio_s1_any_bursting_master_saved_grant;
  wire             control_out_pio_s1_any_continuerequest;
  wire             control_out_pio_s1_arb_counter_enable;
  reg              control_out_pio_s1_arb_share_counter;
  wire             control_out_pio_s1_arb_share_counter_next_value;
  wire             control_out_pio_s1_arb_share_set_values;
  wire             control_out_pio_s1_beginbursttransfer_internal;
  wire             control_out_pio_s1_begins_xfer;
  wire             control_out_pio_s1_chipselect;
  wire             control_out_pio_s1_end_xfer;
  wire             control_out_pio_s1_firsttransfer;
  wire             control_out_pio_s1_grant_vector;
  wire             control_out_pio_s1_in_a_read_cycle;
  wire             control_out_pio_s1_in_a_write_cycle;
  wire             control_out_pio_s1_master_qreq_vector;
  wire             control_out_pio_s1_non_bursting_master_requests;
  wire    [ 31: 0] control_out_pio_s1_readdata_from_sa;
  reg              control_out_pio_s1_reg_firsttransfer;
  wire             control_out_pio_s1_reset_n;
  reg              control_out_pio_s1_slavearbiterlockenable;
  wire             control_out_pio_s1_slavearbiterlockenable2;
  wire             control_out_pio_s1_unreg_firsttransfer;
  wire             control_out_pio_s1_waits_for_read;
  wire             control_out_pio_s1_waits_for_write;
  wire             control_out_pio_s1_write_n;
  wire    [ 31: 0] control_out_pio_s1_writedata;
  wire             cpu_data_master_arbiterlock;
  wire             cpu_data_master_arbiterlock2;
  wire             cpu_data_master_continuerequest;
  wire             cpu_data_master_granted_control_out_pio_s1;
  wire             cpu_data_master_qualified_request_control_out_pio_s1;
  wire             cpu_data_master_read_data_valid_control_out_pio_s1;
  wire             cpu_data_master_requests_control_out_pio_s1;
  wire             cpu_data_master_saved_grant_control_out_pio_s1;
  reg              d1_control_out_pio_s1_end_xfer;
  reg              d1_reasons_to_wait;
  reg              enable_nonzero_assertions;
  wire             end_xfer_arb_share_counter_term_control_out_pio_s1;
  wire             in_a_read_cycle;
  wire             in_a_write_cycle;
  wire    [ 24: 0] shifted_address_to_control_out_pio_s1_from_cpu_data_master;
  wire             wait_for_control_out_pio_s1_counter;
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          d1_reasons_to_wait <= 0;
      else 
        d1_reasons_to_wait <= ~control_out_pio_s1_end_xfer;
    end


  assign control_out_pio_s1_begins_xfer = ~d1_reasons_to_wait & ((cpu_data_master_qualified_request_control_out_pio_s1));
  //assign control_out_pio_s1_readdata_from_sa = control_out_pio_s1_readdata so that symbol knows where to group signals which may go to master only, which is an e_assign
  assign control_out_pio_s1_readdata_from_sa = control_out_pio_s1_readdata;

  assign cpu_data_master_requests_control_out_pio_s1 = ({cpu_data_master_address_to_slave[24 : 5] , 5'b0} == 25'h1409080) & (cpu_data_master_read | cpu_data_master_write);
  //control_out_pio_s1_arb_share_counter set values, which is an e_mux
  assign control_out_pio_s1_arb_share_set_values = 1;

  //control_out_pio_s1_non_bursting_master_requests mux, which is an e_mux
  assign control_out_pio_s1_non_bursting_master_requests = cpu_data_master_requests_control_out_pio_s1;

  //control_out_pio_s1_any_bursting_master_saved_grant mux, which is an e_mux
  assign control_out_pio_s1_any_bursting_master_saved_grant = 0;

  //control_out_pio_s1_arb_share_counter_next_value assignment, which is an e_assign
  assign control_out_pio_s1_arb_share_counter_next_value = control_out_pio_s1_firsttransfer ? (control_out_pio_s1_arb_share_set_values - 1) : |control_out_pio_s1_arb_share_counter ? (control_out_pio_s1_arb_share_counter - 1) : 0;

  //control_out_pio_s1_allgrants all slave grants, which is an e_mux
  assign control_out_pio_s1_allgrants = |control_out_pio_s1_grant_vector;

  //control_out_pio_s1_end_xfer assignment, which is an e_assign
  assign control_out_pio_s1_end_xfer = ~(control_out_pio_s1_waits_for_read | control_out_pio_s1_waits_for_write);

  //end_xfer_arb_share_counter_term_control_out_pio_s1 arb share counter enable term, which is an e_assign
  assign end_xfer_arb_share_counter_term_control_out_pio_s1 = control_out_pio_s1_end_xfer & (~control_out_pio_s1_any_bursting_master_saved_grant | in_a_read_cycle | in_a_write_cycle);

  //control_out_pio_s1_arb_share_counter arbitration counter enable, which is an e_assign
  assign control_out_pio_s1_arb_counter_enable = (end_xfer_arb_share_counter_term_control_out_pio_s1 & control_out_pio_s1_allgrants) | (end_xfer_arb_share_counter_term_control_out_pio_s1 & ~control_out_pio_s1_non_bursting_master_requests);

  //control_out_pio_s1_arb_share_counter counter, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          control_out_pio_s1_arb_share_counter <= 0;
      else if (control_out_pio_s1_arb_counter_enable)
          control_out_pio_s1_arb_share_counter <= control_out_pio_s1_arb_share_counter_next_value;
    end


  //control_out_pio_s1_slavearbiterlockenable slave enables arbiterlock, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          control_out_pio_s1_slavearbiterlockenable <= 0;
      else if ((|control_out_pio_s1_master_qreq_vector & end_xfer_arb_share_counter_term_control_out_pio_s1) | (end_xfer_arb_share_counter_term_control_out_pio_s1 & ~control_out_pio_s1_non_bursting_master_requests))
          control_out_pio_s1_slavearbiterlockenable <= |control_out_pio_s1_arb_share_counter_next_value;
    end


  //cpu/data_master control_out_pio/s1 arbiterlock, which is an e_assign
  assign cpu_data_master_arbiterlock = control_out_pio_s1_slavearbiterlockenable & cpu_data_master_continuerequest;

  //control_out_pio_s1_slavearbiterlockenable2 slave enables arbiterlock2, which is an e_assign
  assign control_out_pio_s1_slavearbiterlockenable2 = |control_out_pio_s1_arb_share_counter_next_value;

  //cpu/data_master control_out_pio/s1 arbiterlock2, which is an e_assign
  assign cpu_data_master_arbiterlock2 = control_out_pio_s1_slavearbiterlockenable2 & cpu_data_master_continuerequest;

  //control_out_pio_s1_any_continuerequest at least one master continues requesting, which is an e_assign
  assign control_out_pio_s1_any_continuerequest = 1;

  //cpu_data_master_continuerequest continued request, which is an e_assign
  assign cpu_data_master_continuerequest = 1;

  assign cpu_data_master_qualified_request_control_out_pio_s1 = cpu_data_master_requests_control_out_pio_s1 & ~((cpu_data_master_read & ((cpu_data_master_latency_counter != 0))));
  //local readdatavalid cpu_data_master_read_data_valid_control_out_pio_s1, which is an e_mux
  assign cpu_data_master_read_data_valid_control_out_pio_s1 = cpu_data_master_granted_control_out_pio_s1 & cpu_data_master_read & ~control_out_pio_s1_waits_for_read;

  //control_out_pio_s1_writedata mux, which is an e_mux
  assign control_out_pio_s1_writedata = cpu_data_master_writedata;

  //master is always granted when requested
  assign cpu_data_master_granted_control_out_pio_s1 = cpu_data_master_qualified_request_control_out_pio_s1;

  //cpu/data_master saved-grant control_out_pio/s1, which is an e_assign
  assign cpu_data_master_saved_grant_control_out_pio_s1 = cpu_data_master_requests_control_out_pio_s1;

  //allow new arb cycle for control_out_pio/s1, which is an e_assign
  assign control_out_pio_s1_allow_new_arb_cycle = 1;

  //placeholder chosen master
  assign control_out_pio_s1_grant_vector = 1;

  //placeholder vector of master qualified-requests
  assign control_out_pio_s1_master_qreq_vector = 1;

  //control_out_pio_s1_reset_n assignment, which is an e_assign
  assign control_out_pio_s1_reset_n = reset_n;

  assign control_out_pio_s1_chipselect = cpu_data_master_granted_control_out_pio_s1;
  //control_out_pio_s1_firsttransfer first transaction, which is an e_assign
  assign control_out_pio_s1_firsttransfer = control_out_pio_s1_begins_xfer ? control_out_pio_s1_unreg_firsttransfer : control_out_pio_s1_reg_firsttransfer;

  //control_out_pio_s1_unreg_firsttransfer first transaction, which is an e_assign
  assign control_out_pio_s1_unreg_firsttransfer = ~(control_out_pio_s1_slavearbiterlockenable & control_out_pio_s1_any_continuerequest);

  //control_out_pio_s1_reg_firsttransfer first transaction, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          control_out_pio_s1_reg_firsttransfer <= 1'b1;
      else if (control_out_pio_s1_begins_xfer)
          control_out_pio_s1_reg_firsttransfer <= control_out_pio_s1_unreg_firsttransfer;
    end


  //control_out_pio_s1_beginbursttransfer_internal begin burst transfer, which is an e_assign
  assign control_out_pio_s1_beginbursttransfer_internal = control_out_pio_s1_begins_xfer;

  //~control_out_pio_s1_write_n assignment, which is an e_mux
  assign control_out_pio_s1_write_n = ~(cpu_data_master_granted_control_out_pio_s1 & cpu_data_master_write);

  assign shifted_address_to_control_out_pio_s1_from_cpu_data_master = cpu_data_master_address_to_slave;
  //control_out_pio_s1_address mux, which is an e_mux
  assign control_out_pio_s1_address = shifted_address_to_control_out_pio_s1_from_cpu_data_master >> 2;

  //d1_control_out_pio_s1_end_xfer register, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          d1_control_out_pio_s1_end_xfer <= 1;
      else 
        d1_control_out_pio_s1_end_xfer <= control_out_pio_s1_end_xfer;
    end


  //control_out_pio_s1_waits_for_read in a cycle, which is an e_mux
  assign control_out_pio_s1_waits_for_read = control_out_pio_s1_in_a_read_cycle & control_out_pio_s1_begins_xfer;

  //control_out_pio_s1_in_a_read_cycle assignment, which is an e_assign
  assign control_out_pio_s1_in_a_read_cycle = cpu_data_master_granted_control_out_pio_s1 & cpu_data_master_read;

  //in_a_read_cycle assignment, which is an e_mux
  assign in_a_read_cycle = control_out_pio_s1_in_a_read_cycle;

  //control_out_pio_s1_waits_for_write in a cycle, which is an e_mux
  assign control_out_pio_s1_waits_for_write = control_out_pio_s1_in_a_write_cycle & 0;

  //control_out_pio_s1_in_a_write_cycle assignment, which is an e_assign
  assign control_out_pio_s1_in_a_write_cycle = cpu_data_master_granted_control_out_pio_s1 & cpu_data_master_write;

  //in_a_write_cycle assignment, which is an e_mux
  assign in_a_write_cycle = control_out_pio_s1_in_a_write_cycle;

  assign wait_for_control_out_pio_s1_counter = 0;

//synthesis translate_off
//////////////// SIMULATION-ONLY CONTENTS
  //control_out_pio/s1 enable non-zero assertions, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          enable_nonzero_assertions <= 0;
      else 
        enable_nonzero_assertions <= 1'b1;
    end



//////////////// END SIMULATION-ONLY CONTENTS

//synthesis translate_on

endmodule


// synthesis translate_off
`timescale 1ns / 1ps
// synthesis translate_on

// turn off superfluous verilog processor warnings 
// altera message_level Level1 
// altera message_off 10034 10035 10036 10037 10230 10240 10030 

module cpu_jtag_debug_module_arbitrator (
                                          // inputs:
                                           clk,
                                           cpu_data_master_address_to_slave,
                                           cpu_data_master_byteenable,
                                           cpu_data_master_debugaccess,
                                           cpu_data_master_latency_counter,
                                           cpu_data_master_read,
                                           cpu_data_master_write,
                                           cpu_data_master_writedata,
                                           cpu_instruction_master_address_to_slave,
                                           cpu_instruction_master_latency_counter,
                                           cpu_instruction_master_read,
                                           cpu_jtag_debug_module_readdata,
                                           cpu_jtag_debug_module_resetrequest,
                                           reset_n,

                                          // outputs:
                                           cpu_data_master_granted_cpu_jtag_debug_module,
                                           cpu_data_master_qualified_request_cpu_jtag_debug_module,
                                           cpu_data_master_read_data_valid_cpu_jtag_debug_module,
                                           cpu_data_master_requests_cpu_jtag_debug_module,
                                           cpu_instruction_master_granted_cpu_jtag_debug_module,
                                           cpu_instruction_master_qualified_request_cpu_jtag_debug_module,
                                           cpu_instruction_master_read_data_valid_cpu_jtag_debug_module,
                                           cpu_instruction_master_requests_cpu_jtag_debug_module,
                                           cpu_jtag_debug_module_address,
                                           cpu_jtag_debug_module_begintransfer,
                                           cpu_jtag_debug_module_byteenable,
                                           cpu_jtag_debug_module_chipselect,
                                           cpu_jtag_debug_module_debugaccess,
                                           cpu_jtag_debug_module_readdata_from_sa,
                                           cpu_jtag_debug_module_reset_n,
                                           cpu_jtag_debug_module_resetrequest_from_sa,
                                           cpu_jtag_debug_module_write,
                                           cpu_jtag_debug_module_writedata,
                                           d1_cpu_jtag_debug_module_end_xfer
                                        )
;

  output           cpu_data_master_granted_cpu_jtag_debug_module;
  output           cpu_data_master_qualified_request_cpu_jtag_debug_module;
  output           cpu_data_master_read_data_valid_cpu_jtag_debug_module;
  output           cpu_data_master_requests_cpu_jtag_debug_module;
  output           cpu_instruction_master_granted_cpu_jtag_debug_module;
  output           cpu_instruction_master_qualified_request_cpu_jtag_debug_module;
  output           cpu_instruction_master_read_data_valid_cpu_jtag_debug_module;
  output           cpu_instruction_master_requests_cpu_jtag_debug_module;
  output  [  8: 0] cpu_jtag_debug_module_address;
  output           cpu_jtag_debug_module_begintransfer;
  output  [  3: 0] cpu_jtag_debug_module_byteenable;
  output           cpu_jtag_debug_module_chipselect;
  output           cpu_jtag_debug_module_debugaccess;
  output  [ 31: 0] cpu_jtag_debug_module_readdata_from_sa;
  output           cpu_jtag_debug_module_reset_n;
  output           cpu_jtag_debug_module_resetrequest_from_sa;
  output           cpu_jtag_debug_module_write;
  output  [ 31: 0] cpu_jtag_debug_module_writedata;
  output           d1_cpu_jtag_debug_module_end_xfer;
  input            clk;
  input   [ 24: 0] cpu_data_master_address_to_slave;
  input   [  3: 0] cpu_data_master_byteenable;
  input            cpu_data_master_debugaccess;
  input   [  2: 0] cpu_data_master_latency_counter;
  input            cpu_data_master_read;
  input            cpu_data_master_write;
  input   [ 31: 0] cpu_data_master_writedata;
  input   [ 24: 0] cpu_instruction_master_address_to_slave;
  input   [  2: 0] cpu_instruction_master_latency_counter;
  input            cpu_instruction_master_read;
  input   [ 31: 0] cpu_jtag_debug_module_readdata;
  input            cpu_jtag_debug_module_resetrequest;
  input            reset_n;

  wire             cpu_data_master_arbiterlock;
  wire             cpu_data_master_arbiterlock2;
  wire             cpu_data_master_continuerequest;
  wire             cpu_data_master_granted_cpu_jtag_debug_module;
  wire             cpu_data_master_qualified_request_cpu_jtag_debug_module;
  wire             cpu_data_master_read_data_valid_cpu_jtag_debug_module;
  wire             cpu_data_master_requests_cpu_jtag_debug_module;
  wire             cpu_data_master_saved_grant_cpu_jtag_debug_module;
  wire             cpu_instruction_master_arbiterlock;
  wire             cpu_instruction_master_arbiterlock2;
  wire             cpu_instruction_master_continuerequest;
  wire             cpu_instruction_master_granted_cpu_jtag_debug_module;
  wire             cpu_instruction_master_qualified_request_cpu_jtag_debug_module;
  wire             cpu_instruction_master_read_data_valid_cpu_jtag_debug_module;
  wire             cpu_instruction_master_requests_cpu_jtag_debug_module;
  wire             cpu_instruction_master_saved_grant_cpu_jtag_debug_module;
  wire    [  8: 0] cpu_jtag_debug_module_address;
  wire             cpu_jtag_debug_module_allgrants;
  wire             cpu_jtag_debug_module_allow_new_arb_cycle;
  wire             cpu_jtag_debug_module_any_bursting_master_saved_grant;
  wire             cpu_jtag_debug_module_any_continuerequest;
  reg     [  1: 0] cpu_jtag_debug_module_arb_addend;
  wire             cpu_jtag_debug_module_arb_counter_enable;
  reg              cpu_jtag_debug_module_arb_share_counter;
  wire             cpu_jtag_debug_module_arb_share_counter_next_value;
  wire             cpu_jtag_debug_module_arb_share_set_values;
  wire    [  1: 0] cpu_jtag_debug_module_arb_winner;
  wire             cpu_jtag_debug_module_arbitration_holdoff_internal;
  wire             cpu_jtag_debug_module_beginbursttransfer_internal;
  wire             cpu_jtag_debug_module_begins_xfer;
  wire             cpu_jtag_debug_module_begintransfer;
  wire    [  3: 0] cpu_jtag_debug_module_byteenable;
  wire             cpu_jtag_debug_module_chipselect;
  wire    [  3: 0] cpu_jtag_debug_module_chosen_master_double_vector;
  wire    [  1: 0] cpu_jtag_debug_module_chosen_master_rot_left;
  wire             cpu_jtag_debug_module_debugaccess;
  wire             cpu_jtag_debug_module_end_xfer;
  wire             cpu_jtag_debug_module_firsttransfer;
  wire    [  1: 0] cpu_jtag_debug_module_grant_vector;
  wire             cpu_jtag_debug_module_in_a_read_cycle;
  wire             cpu_jtag_debug_module_in_a_write_cycle;
  wire    [  1: 0] cpu_jtag_debug_module_master_qreq_vector;
  wire             cpu_jtag_debug_module_non_bursting_master_requests;
  wire    [ 31: 0] cpu_jtag_debug_module_readdata_from_sa;
  reg              cpu_jtag_debug_module_reg_firsttransfer;
  wire             cpu_jtag_debug_module_reset_n;
  wire             cpu_jtag_debug_module_resetrequest_from_sa;
  reg     [  1: 0] cpu_jtag_debug_module_saved_chosen_master_vector;
  reg              cpu_jtag_debug_module_slavearbiterlockenable;
  wire             cpu_jtag_debug_module_slavearbiterlockenable2;
  wire             cpu_jtag_debug_module_unreg_firsttransfer;
  wire             cpu_jtag_debug_module_waits_for_read;
  wire             cpu_jtag_debug_module_waits_for_write;
  wire             cpu_jtag_debug_module_write;
  wire    [ 31: 0] cpu_jtag_debug_module_writedata;
  reg              d1_cpu_jtag_debug_module_end_xfer;
  reg              d1_reasons_to_wait;
  reg              enable_nonzero_assertions;
  wire             end_xfer_arb_share_counter_term_cpu_jtag_debug_module;
  wire             in_a_read_cycle;
  wire             in_a_write_cycle;
  reg              last_cycle_cpu_data_master_granted_slave_cpu_jtag_debug_module;
  reg              last_cycle_cpu_instruction_master_granted_slave_cpu_jtag_debug_module;
  wire    [ 24: 0] shifted_address_to_cpu_jtag_debug_module_from_cpu_data_master;
  wire    [ 24: 0] shifted_address_to_cpu_jtag_debug_module_from_cpu_instruction_master;
  wire             wait_for_cpu_jtag_debug_module_counter;
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          d1_reasons_to_wait <= 0;
      else 
        d1_reasons_to_wait <= ~cpu_jtag_debug_module_end_xfer;
    end


  assign cpu_jtag_debug_module_begins_xfer = ~d1_reasons_to_wait & ((cpu_data_master_qualified_request_cpu_jtag_debug_module | cpu_instruction_master_qualified_request_cpu_jtag_debug_module));
  //assign cpu_jtag_debug_module_readdata_from_sa = cpu_jtag_debug_module_readdata so that symbol knows where to group signals which may go to master only, which is an e_assign
  assign cpu_jtag_debug_module_readdata_from_sa = cpu_jtag_debug_module_readdata;

  assign cpu_data_master_requests_cpu_jtag_debug_module = ({cpu_data_master_address_to_slave[24 : 11] , 11'b0} == 25'h1408800) & (cpu_data_master_read | cpu_data_master_write);
  //cpu_jtag_debug_module_arb_share_counter set values, which is an e_mux
  assign cpu_jtag_debug_module_arb_share_set_values = 1;

  //cpu_jtag_debug_module_non_bursting_master_requests mux, which is an e_mux
  assign cpu_jtag_debug_module_non_bursting_master_requests = cpu_data_master_requests_cpu_jtag_debug_module |
    cpu_instruction_master_requests_cpu_jtag_debug_module |
    cpu_data_master_requests_cpu_jtag_debug_module |
    cpu_instruction_master_requests_cpu_jtag_debug_module;

  //cpu_jtag_debug_module_any_bursting_master_saved_grant mux, which is an e_mux
  assign cpu_jtag_debug_module_any_bursting_master_saved_grant = 0;

  //cpu_jtag_debug_module_arb_share_counter_next_value assignment, which is an e_assign
  assign cpu_jtag_debug_module_arb_share_counter_next_value = cpu_jtag_debug_module_firsttransfer ? (cpu_jtag_debug_module_arb_share_set_values - 1) : |cpu_jtag_debug_module_arb_share_counter ? (cpu_jtag_debug_module_arb_share_counter - 1) : 0;

  //cpu_jtag_debug_module_allgrants all slave grants, which is an e_mux
  assign cpu_jtag_debug_module_allgrants = (|cpu_jtag_debug_module_grant_vector) |
    (|cpu_jtag_debug_module_grant_vector) |
    (|cpu_jtag_debug_module_grant_vector) |
    (|cpu_jtag_debug_module_grant_vector);

  //cpu_jtag_debug_module_end_xfer assignment, which is an e_assign
  assign cpu_jtag_debug_module_end_xfer = ~(cpu_jtag_debug_module_waits_for_read | cpu_jtag_debug_module_waits_for_write);

  //end_xfer_arb_share_counter_term_cpu_jtag_debug_module arb share counter enable term, which is an e_assign
  assign end_xfer_arb_share_counter_term_cpu_jtag_debug_module = cpu_jtag_debug_module_end_xfer & (~cpu_jtag_debug_module_any_bursting_master_saved_grant | in_a_read_cycle | in_a_write_cycle);

  //cpu_jtag_debug_module_arb_share_counter arbitration counter enable, which is an e_assign
  assign cpu_jtag_debug_module_arb_counter_enable = (end_xfer_arb_share_counter_term_cpu_jtag_debug_module & cpu_jtag_debug_module_allgrants) | (end_xfer_arb_share_counter_term_cpu_jtag_debug_module & ~cpu_jtag_debug_module_non_bursting_master_requests);

  //cpu_jtag_debug_module_arb_share_counter counter, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          cpu_jtag_debug_module_arb_share_counter <= 0;
      else if (cpu_jtag_debug_module_arb_counter_enable)
          cpu_jtag_debug_module_arb_share_counter <= cpu_jtag_debug_module_arb_share_counter_next_value;
    end


  //cpu_jtag_debug_module_slavearbiterlockenable slave enables arbiterlock, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          cpu_jtag_debug_module_slavearbiterlockenable <= 0;
      else if ((|cpu_jtag_debug_module_master_qreq_vector & end_xfer_arb_share_counter_term_cpu_jtag_debug_module) | (end_xfer_arb_share_counter_term_cpu_jtag_debug_module & ~cpu_jtag_debug_module_non_bursting_master_requests))
          cpu_jtag_debug_module_slavearbiterlockenable <= |cpu_jtag_debug_module_arb_share_counter_next_value;
    end


  //cpu/data_master cpu/jtag_debug_module arbiterlock, which is an e_assign
  assign cpu_data_master_arbiterlock = cpu_jtag_debug_module_slavearbiterlockenable & cpu_data_master_continuerequest;

  //cpu_jtag_debug_module_slavearbiterlockenable2 slave enables arbiterlock2, which is an e_assign
  assign cpu_jtag_debug_module_slavearbiterlockenable2 = |cpu_jtag_debug_module_arb_share_counter_next_value;

  //cpu/data_master cpu/jtag_debug_module arbiterlock2, which is an e_assign
  assign cpu_data_master_arbiterlock2 = cpu_jtag_debug_module_slavearbiterlockenable2 & cpu_data_master_continuerequest;

  //cpu/instruction_master cpu/jtag_debug_module arbiterlock, which is an e_assign
  assign cpu_instruction_master_arbiterlock = cpu_jtag_debug_module_slavearbiterlockenable & cpu_instruction_master_continuerequest;

  //cpu/instruction_master cpu/jtag_debug_module arbiterlock2, which is an e_assign
  assign cpu_instruction_master_arbiterlock2 = cpu_jtag_debug_module_slavearbiterlockenable2 & cpu_instruction_master_continuerequest;

  //cpu/instruction_master granted cpu/jtag_debug_module last time, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          last_cycle_cpu_instruction_master_granted_slave_cpu_jtag_debug_module <= 0;
      else 
        last_cycle_cpu_instruction_master_granted_slave_cpu_jtag_debug_module <= cpu_instruction_master_saved_grant_cpu_jtag_debug_module ? 1 : (cpu_jtag_debug_module_arbitration_holdoff_internal | ~cpu_instruction_master_requests_cpu_jtag_debug_module) ? 0 : last_cycle_cpu_instruction_master_granted_slave_cpu_jtag_debug_module;
    end


  //cpu_instruction_master_continuerequest continued request, which is an e_mux
  assign cpu_instruction_master_continuerequest = last_cycle_cpu_instruction_master_granted_slave_cpu_jtag_debug_module & cpu_instruction_master_requests_cpu_jtag_debug_module;

  //cpu_jtag_debug_module_any_continuerequest at least one master continues requesting, which is an e_mux
  assign cpu_jtag_debug_module_any_continuerequest = cpu_instruction_master_continuerequest |
    cpu_data_master_continuerequest;

  assign cpu_data_master_qualified_request_cpu_jtag_debug_module = cpu_data_master_requests_cpu_jtag_debug_module & ~((cpu_data_master_read & ((cpu_data_master_latency_counter != 0))) | cpu_instruction_master_arbiterlock);
  //local readdatavalid cpu_data_master_read_data_valid_cpu_jtag_debug_module, which is an e_mux
  assign cpu_data_master_read_data_valid_cpu_jtag_debug_module = cpu_data_master_granted_cpu_jtag_debug_module & cpu_data_master_read & ~cpu_jtag_debug_module_waits_for_read;

  //cpu_jtag_debug_module_writedata mux, which is an e_mux
  assign cpu_jtag_debug_module_writedata = cpu_data_master_writedata;

  assign cpu_instruction_master_requests_cpu_jtag_debug_module = (({cpu_instruction_master_address_to_slave[24 : 11] , 11'b0} == 25'h1408800) & (cpu_instruction_master_read)) & cpu_instruction_master_read;
  //cpu/data_master granted cpu/jtag_debug_module last time, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          last_cycle_cpu_data_master_granted_slave_cpu_jtag_debug_module <= 0;
      else 
        last_cycle_cpu_data_master_granted_slave_cpu_jtag_debug_module <= cpu_data_master_saved_grant_cpu_jtag_debug_module ? 1 : (cpu_jtag_debug_module_arbitration_holdoff_internal | ~cpu_data_master_requests_cpu_jtag_debug_module) ? 0 : last_cycle_cpu_data_master_granted_slave_cpu_jtag_debug_module;
    end


  //cpu_data_master_continuerequest continued request, which is an e_mux
  assign cpu_data_master_continuerequest = last_cycle_cpu_data_master_granted_slave_cpu_jtag_debug_module & cpu_data_master_requests_cpu_jtag_debug_module;

  assign cpu_instruction_master_qualified_request_cpu_jtag_debug_module = cpu_instruction_master_requests_cpu_jtag_debug_module & ~((cpu_instruction_master_read & ((cpu_instruction_master_latency_counter != 0))) | cpu_data_master_arbiterlock);
  //local readdatavalid cpu_instruction_master_read_data_valid_cpu_jtag_debug_module, which is an e_mux
  assign cpu_instruction_master_read_data_valid_cpu_jtag_debug_module = cpu_instruction_master_granted_cpu_jtag_debug_module & cpu_instruction_master_read & ~cpu_jtag_debug_module_waits_for_read;

  //allow new arb cycle for cpu/jtag_debug_module, which is an e_assign
  assign cpu_jtag_debug_module_allow_new_arb_cycle = ~cpu_data_master_arbiterlock & ~cpu_instruction_master_arbiterlock;

  //cpu/instruction_master assignment into master qualified-requests vector for cpu/jtag_debug_module, which is an e_assign
  assign cpu_jtag_debug_module_master_qreq_vector[0] = cpu_instruction_master_qualified_request_cpu_jtag_debug_module;

  //cpu/instruction_master grant cpu/jtag_debug_module, which is an e_assign
  assign cpu_instruction_master_granted_cpu_jtag_debug_module = cpu_jtag_debug_module_grant_vector[0];

  //cpu/instruction_master saved-grant cpu/jtag_debug_module, which is an e_assign
  assign cpu_instruction_master_saved_grant_cpu_jtag_debug_module = cpu_jtag_debug_module_arb_winner[0] && cpu_instruction_master_requests_cpu_jtag_debug_module;

  //cpu/data_master assignment into master qualified-requests vector for cpu/jtag_debug_module, which is an e_assign
  assign cpu_jtag_debug_module_master_qreq_vector[1] = cpu_data_master_qualified_request_cpu_jtag_debug_module;

  //cpu/data_master grant cpu/jtag_debug_module, which is an e_assign
  assign cpu_data_master_granted_cpu_jtag_debug_module = cpu_jtag_debug_module_grant_vector[1];

  //cpu/data_master saved-grant cpu/jtag_debug_module, which is an e_assign
  assign cpu_data_master_saved_grant_cpu_jtag_debug_module = cpu_jtag_debug_module_arb_winner[1] && cpu_data_master_requests_cpu_jtag_debug_module;

  //cpu/jtag_debug_module chosen-master double-vector, which is an e_assign
  assign cpu_jtag_debug_module_chosen_master_double_vector = {cpu_jtag_debug_module_master_qreq_vector, cpu_jtag_debug_module_master_qreq_vector} & ({~cpu_jtag_debug_module_master_qreq_vector, ~cpu_jtag_debug_module_master_qreq_vector} + cpu_jtag_debug_module_arb_addend);

  //stable onehot encoding of arb winner
  assign cpu_jtag_debug_module_arb_winner = (cpu_jtag_debug_module_allow_new_arb_cycle & | cpu_jtag_debug_module_grant_vector) ? cpu_jtag_debug_module_grant_vector : cpu_jtag_debug_module_saved_chosen_master_vector;

  //saved cpu_jtag_debug_module_grant_vector, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          cpu_jtag_debug_module_saved_chosen_master_vector <= 0;
      else if (cpu_jtag_debug_module_allow_new_arb_cycle)
          cpu_jtag_debug_module_saved_chosen_master_vector <= |cpu_jtag_debug_module_grant_vector ? cpu_jtag_debug_module_grant_vector : cpu_jtag_debug_module_saved_chosen_master_vector;
    end


  //onehot encoding of chosen master
  assign cpu_jtag_debug_module_grant_vector = {(cpu_jtag_debug_module_chosen_master_double_vector[1] | cpu_jtag_debug_module_chosen_master_double_vector[3]),
    (cpu_jtag_debug_module_chosen_master_double_vector[0] | cpu_jtag_debug_module_chosen_master_double_vector[2])};

  //cpu/jtag_debug_module chosen master rotated left, which is an e_assign
  assign cpu_jtag_debug_module_chosen_master_rot_left = (cpu_jtag_debug_module_arb_winner << 1) ? (cpu_jtag_debug_module_arb_winner << 1) : 1;

  //cpu/jtag_debug_module's addend for next-master-grant
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          cpu_jtag_debug_module_arb_addend <= 1;
      else if (|cpu_jtag_debug_module_grant_vector)
          cpu_jtag_debug_module_arb_addend <= cpu_jtag_debug_module_end_xfer? cpu_jtag_debug_module_chosen_master_rot_left : cpu_jtag_debug_module_grant_vector;
    end


  assign cpu_jtag_debug_module_begintransfer = cpu_jtag_debug_module_begins_xfer;
  //cpu_jtag_debug_module_reset_n assignment, which is an e_assign
  assign cpu_jtag_debug_module_reset_n = reset_n;

  //assign cpu_jtag_debug_module_resetrequest_from_sa = cpu_jtag_debug_module_resetrequest so that symbol knows where to group signals which may go to master only, which is an e_assign
  assign cpu_jtag_debug_module_resetrequest_from_sa = cpu_jtag_debug_module_resetrequest;

  assign cpu_jtag_debug_module_chipselect = cpu_data_master_granted_cpu_jtag_debug_module | cpu_instruction_master_granted_cpu_jtag_debug_module;
  //cpu_jtag_debug_module_firsttransfer first transaction, which is an e_assign
  assign cpu_jtag_debug_module_firsttransfer = cpu_jtag_debug_module_begins_xfer ? cpu_jtag_debug_module_unreg_firsttransfer : cpu_jtag_debug_module_reg_firsttransfer;

  //cpu_jtag_debug_module_unreg_firsttransfer first transaction, which is an e_assign
  assign cpu_jtag_debug_module_unreg_firsttransfer = ~(cpu_jtag_debug_module_slavearbiterlockenable & cpu_jtag_debug_module_any_continuerequest);

  //cpu_jtag_debug_module_reg_firsttransfer first transaction, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          cpu_jtag_debug_module_reg_firsttransfer <= 1'b1;
      else if (cpu_jtag_debug_module_begins_xfer)
          cpu_jtag_debug_module_reg_firsttransfer <= cpu_jtag_debug_module_unreg_firsttransfer;
    end


  //cpu_jtag_debug_module_beginbursttransfer_internal begin burst transfer, which is an e_assign
  assign cpu_jtag_debug_module_beginbursttransfer_internal = cpu_jtag_debug_module_begins_xfer;

  //cpu_jtag_debug_module_arbitration_holdoff_internal arbitration_holdoff, which is an e_assign
  assign cpu_jtag_debug_module_arbitration_holdoff_internal = cpu_jtag_debug_module_begins_xfer & cpu_jtag_debug_module_firsttransfer;

  //cpu_jtag_debug_module_write assignment, which is an e_mux
  assign cpu_jtag_debug_module_write = cpu_data_master_granted_cpu_jtag_debug_module & cpu_data_master_write;

  assign shifted_address_to_cpu_jtag_debug_module_from_cpu_data_master = cpu_data_master_address_to_slave;
  //cpu_jtag_debug_module_address mux, which is an e_mux
  assign cpu_jtag_debug_module_address = (cpu_data_master_granted_cpu_jtag_debug_module)? (shifted_address_to_cpu_jtag_debug_module_from_cpu_data_master >> 2) :
    (shifted_address_to_cpu_jtag_debug_module_from_cpu_instruction_master >> 2);

  assign shifted_address_to_cpu_jtag_debug_module_from_cpu_instruction_master = cpu_instruction_master_address_to_slave;
  //d1_cpu_jtag_debug_module_end_xfer register, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          d1_cpu_jtag_debug_module_end_xfer <= 1;
      else 
        d1_cpu_jtag_debug_module_end_xfer <= cpu_jtag_debug_module_end_xfer;
    end


  //cpu_jtag_debug_module_waits_for_read in a cycle, which is an e_mux
  assign cpu_jtag_debug_module_waits_for_read = cpu_jtag_debug_module_in_a_read_cycle & cpu_jtag_debug_module_begins_xfer;

  //cpu_jtag_debug_module_in_a_read_cycle assignment, which is an e_assign
  assign cpu_jtag_debug_module_in_a_read_cycle = (cpu_data_master_granted_cpu_jtag_debug_module & cpu_data_master_read) | (cpu_instruction_master_granted_cpu_jtag_debug_module & cpu_instruction_master_read);

  //in_a_read_cycle assignment, which is an e_mux
  assign in_a_read_cycle = cpu_jtag_debug_module_in_a_read_cycle;

  //cpu_jtag_debug_module_waits_for_write in a cycle, which is an e_mux
  assign cpu_jtag_debug_module_waits_for_write = cpu_jtag_debug_module_in_a_write_cycle & 0;

  //cpu_jtag_debug_module_in_a_write_cycle assignment, which is an e_assign
  assign cpu_jtag_debug_module_in_a_write_cycle = cpu_data_master_granted_cpu_jtag_debug_module & cpu_data_master_write;

  //in_a_write_cycle assignment, which is an e_mux
  assign in_a_write_cycle = cpu_jtag_debug_module_in_a_write_cycle;

  assign wait_for_cpu_jtag_debug_module_counter = 0;
  //cpu_jtag_debug_module_byteenable byte enable port mux, which is an e_mux
  assign cpu_jtag_debug_module_byteenable = (cpu_data_master_granted_cpu_jtag_debug_module)? cpu_data_master_byteenable :
    -1;

  //debugaccess mux, which is an e_mux
  assign cpu_jtag_debug_module_debugaccess = (cpu_data_master_granted_cpu_jtag_debug_module)? cpu_data_master_debugaccess :
    0;


//synthesis translate_off
//////////////// SIMULATION-ONLY CONTENTS
  //cpu/jtag_debug_module enable non-zero assertions, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          enable_nonzero_assertions <= 0;
      else 
        enable_nonzero_assertions <= 1'b1;
    end


  //grant signals are active simultaneously, which is an e_process
  always @(posedge clk)
    begin
      if (cpu_data_master_granted_cpu_jtag_debug_module + cpu_instruction_master_granted_cpu_jtag_debug_module > 1)
        begin
          $write("%0d ns: > 1 of grant signals are active simultaneously", $time);
          $stop;
        end
    end


  //saved_grant signals are active simultaneously, which is an e_process
  always @(posedge clk)
    begin
      if (cpu_data_master_saved_grant_cpu_jtag_debug_module + cpu_instruction_master_saved_grant_cpu_jtag_debug_module > 1)
        begin
          $write("%0d ns: > 1 of saved_grant signals are active simultaneously", $time);
          $stop;
        end
    end



//////////////// END SIMULATION-ONLY CONTENTS

//synthesis translate_on

endmodule


// synthesis translate_off
`timescale 1ns / 1ps
// synthesis translate_on

// turn off superfluous verilog processor warnings 
// altera message_level Level1 
// altera message_off 10034 10035 10036 10037 10230 10240 10030 

module DM9000A_s1_irq_from_sa_clock_crossing_cpu_data_master_module (
                                                                      // inputs:
                                                                       clk,
                                                                       data_in,
                                                                       reset_n,

                                                                      // outputs:
                                                                       data_out
                                                                    )
;

  output           data_out;
  input            clk;
  input            data_in;
  input            reset_n;

  reg              data_in_d1 /* synthesis ALTERA_ATTRIBUTE = "{-from \"*\"} CUT=ON ; PRESERVE_REGISTER=ON"  */;
  reg              data_out /* synthesis ALTERA_ATTRIBUTE = "PRESERVE_REGISTER=ON"  */;
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          data_in_d1 <= 0;
      else 
        data_in_d1 <= data_in;
    end


  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          data_out <= 0;
      else 
        data_out <= data_in_d1;
    end



endmodule


// synthesis translate_off
`timescale 1ns / 1ps
// synthesis translate_on

// turn off superfluous verilog processor warnings 
// altera message_level Level1 
// altera message_off 10034 10035 10036 10037 10230 10240 10030 

module cpu_data_master_arbitrator (
                                    // inputs:
                                     DE2_70_SOPC_clock_0_in_readdata_from_sa,
                                     DE2_70_SOPC_clock_0_in_waitrequest_from_sa,
                                     DE2_70_SOPC_clock_2_in_readdata_from_sa,
                                     DE2_70_SOPC_clock_2_in_waitrequest_from_sa,
                                     DM9000A_s1_irq_from_sa,
                                     SEG7_s1_readdata_from_sa,
                                     clk,
                                     control_in_pio_s1_readdata_from_sa,
                                     control_out_pio_s1_readdata_from_sa,
                                     cpu_data_master_address,
                                     cpu_data_master_byteenable,
                                     cpu_data_master_granted_DE2_70_SOPC_clock_0_in,
                                     cpu_data_master_granted_DE2_70_SOPC_clock_2_in,
                                     cpu_data_master_granted_SEG7_s1,
                                     cpu_data_master_granted_control_in_pio_s1,
                                     cpu_data_master_granted_control_out_pio_s1,
                                     cpu_data_master_granted_cpu_jtag_debug_module,
                                     cpu_data_master_granted_data_in_pio_s1,
                                     cpu_data_master_granted_data_out_pio_s1,
                                     cpu_data_master_granted_epcs_flash_controller_0_epcs_control_port,
                                     cpu_data_master_granted_jtag_uart_avalon_jtag_slave,
                                     cpu_data_master_granted_lcd_control_slave,
                                     cpu_data_master_granted_onchip_mem_s1,
                                     cpu_data_master_granted_pio_button_s1,
                                     cpu_data_master_granted_pio_switch_s1,
                                     cpu_data_master_granted_ssram_s1,
                                     cpu_data_master_granted_sysid_control_slave,
                                     cpu_data_master_granted_timer_s1,
                                     cpu_data_master_granted_timer_stamp_s1,
                                     cpu_data_master_qualified_request_DE2_70_SOPC_clock_0_in,
                                     cpu_data_master_qualified_request_DE2_70_SOPC_clock_2_in,
                                     cpu_data_master_qualified_request_SEG7_s1,
                                     cpu_data_master_qualified_request_control_in_pio_s1,
                                     cpu_data_master_qualified_request_control_out_pio_s1,
                                     cpu_data_master_qualified_request_cpu_jtag_debug_module,
                                     cpu_data_master_qualified_request_data_in_pio_s1,
                                     cpu_data_master_qualified_request_data_out_pio_s1,
                                     cpu_data_master_qualified_request_epcs_flash_controller_0_epcs_control_port,
                                     cpu_data_master_qualified_request_jtag_uart_avalon_jtag_slave,
                                     cpu_data_master_qualified_request_lcd_control_slave,
                                     cpu_data_master_qualified_request_onchip_mem_s1,
                                     cpu_data_master_qualified_request_pio_button_s1,
                                     cpu_data_master_qualified_request_pio_switch_s1,
                                     cpu_data_master_qualified_request_ssram_s1,
                                     cpu_data_master_qualified_request_sysid_control_slave,
                                     cpu_data_master_qualified_request_timer_s1,
                                     cpu_data_master_qualified_request_timer_stamp_s1,
                                     cpu_data_master_read,
                                     cpu_data_master_read_data_valid_DE2_70_SOPC_clock_0_in,
                                     cpu_data_master_read_data_valid_DE2_70_SOPC_clock_2_in,
                                     cpu_data_master_read_data_valid_SEG7_s1,
                                     cpu_data_master_read_data_valid_control_in_pio_s1,
                                     cpu_data_master_read_data_valid_control_out_pio_s1,
                                     cpu_data_master_read_data_valid_cpu_jtag_debug_module,
                                     cpu_data_master_read_data_valid_data_in_pio_s1,
                                     cpu_data_master_read_data_valid_data_out_pio_s1,
                                     cpu_data_master_read_data_valid_epcs_flash_controller_0_epcs_control_port,
                                     cpu_data_master_read_data_valid_jtag_uart_avalon_jtag_slave,
                                     cpu_data_master_read_data_valid_lcd_control_slave,
                                     cpu_data_master_read_data_valid_onchip_mem_s1,
                                     cpu_data_master_read_data_valid_pio_button_s1,
                                     cpu_data_master_read_data_valid_pio_switch_s1,
                                     cpu_data_master_read_data_valid_ssram_s1,
                                     cpu_data_master_read_data_valid_sysid_control_slave,
                                     cpu_data_master_read_data_valid_timer_s1,
                                     cpu_data_master_read_data_valid_timer_stamp_s1,
                                     cpu_data_master_requests_DE2_70_SOPC_clock_0_in,
                                     cpu_data_master_requests_DE2_70_SOPC_clock_2_in,
                                     cpu_data_master_requests_SEG7_s1,
                                     cpu_data_master_requests_control_in_pio_s1,
                                     cpu_data_master_requests_control_out_pio_s1,
                                     cpu_data_master_requests_cpu_jtag_debug_module,
                                     cpu_data_master_requests_data_in_pio_s1,
                                     cpu_data_master_requests_data_out_pio_s1,
                                     cpu_data_master_requests_epcs_flash_controller_0_epcs_control_port,
                                     cpu_data_master_requests_jtag_uart_avalon_jtag_slave,
                                     cpu_data_master_requests_lcd_control_slave,
                                     cpu_data_master_requests_onchip_mem_s1,
                                     cpu_data_master_requests_pio_button_s1,
                                     cpu_data_master_requests_pio_switch_s1,
                                     cpu_data_master_requests_ssram_s1,
                                     cpu_data_master_requests_sysid_control_slave,
                                     cpu_data_master_requests_timer_s1,
                                     cpu_data_master_requests_timer_stamp_s1,
                                     cpu_data_master_write,
                                     cpu_data_master_writedata,
                                     cpu_jtag_debug_module_readdata_from_sa,
                                     d1_DE2_70_SOPC_clock_0_in_end_xfer,
                                     d1_DE2_70_SOPC_clock_2_in_end_xfer,
                                     d1_SEG7_s1_end_xfer,
                                     d1_control_in_pio_s1_end_xfer,
                                     d1_control_out_pio_s1_end_xfer,
                                     d1_cpu_jtag_debug_module_end_xfer,
                                     d1_data_in_pio_s1_end_xfer,
                                     d1_data_out_pio_s1_end_xfer,
                                     d1_epcs_flash_controller_0_epcs_control_port_end_xfer,
                                     d1_jtag_uart_avalon_jtag_slave_end_xfer,
                                     d1_lcd_control_slave_end_xfer,
                                     d1_onchip_mem_s1_end_xfer,
                                     d1_pio_button_s1_end_xfer,
                                     d1_pio_switch_s1_end_xfer,
                                     d1_sysid_control_slave_end_xfer,
                                     d1_timer_s1_end_xfer,
                                     d1_timer_stamp_s1_end_xfer,
                                     d1_tristate_bridge_ssram_avalon_slave_end_xfer,
                                     data_in_pio_s1_readdata_from_sa,
                                     data_out_pio_s1_readdata_from_sa,
                                     epcs_flash_controller_0_epcs_control_port_irq_from_sa,
                                     epcs_flash_controller_0_epcs_control_port_readdata_from_sa,
                                     incoming_data_to_and_from_the_ssram,
                                     jtag_uart_avalon_jtag_slave_irq_from_sa,
                                     jtag_uart_avalon_jtag_slave_readdata_from_sa,
                                     jtag_uart_avalon_jtag_slave_waitrequest_from_sa,
                                     lcd_control_slave_readdata_from_sa,
                                     lcd_control_slave_wait_counter_eq_0,
                                     onchip_mem_s1_readdata_from_sa,
                                     pio_button_s1_irq_from_sa,
                                     pio_button_s1_readdata_from_sa,
                                     pio_switch_s1_readdata_from_sa,
                                     pll_c0_out,
                                     pll_c0_out_reset_n,
                                     reset_n,
                                     sysid_control_slave_readdata_from_sa,
                                     timer_s1_irq_from_sa,
                                     timer_s1_readdata_from_sa,
                                     timer_stamp_s1_irq_from_sa,
                                     timer_stamp_s1_readdata_from_sa,

                                    // outputs:
                                     cpu_data_master_address_to_slave,
                                     cpu_data_master_irq,
                                     cpu_data_master_latency_counter,
                                     cpu_data_master_readdata,
                                     cpu_data_master_readdatavalid,
                                     cpu_data_master_waitrequest
                                  )
;

  output  [ 24: 0] cpu_data_master_address_to_slave;
  output  [ 31: 0] cpu_data_master_irq;
  output  [  2: 0] cpu_data_master_latency_counter;
  output  [ 31: 0] cpu_data_master_readdata;
  output           cpu_data_master_readdatavalid;
  output           cpu_data_master_waitrequest;
  input   [ 15: 0] DE2_70_SOPC_clock_0_in_readdata_from_sa;
  input            DE2_70_SOPC_clock_0_in_waitrequest_from_sa;
  input   [ 31: 0] DE2_70_SOPC_clock_2_in_readdata_from_sa;
  input            DE2_70_SOPC_clock_2_in_waitrequest_from_sa;
  input            DM9000A_s1_irq_from_sa;
  input   [  7: 0] SEG7_s1_readdata_from_sa;
  input            clk;
  input   [ 31: 0] control_in_pio_s1_readdata_from_sa;
  input   [ 31: 0] control_out_pio_s1_readdata_from_sa;
  input   [ 24: 0] cpu_data_master_address;
  input   [  3: 0] cpu_data_master_byteenable;
  input            cpu_data_master_granted_DE2_70_SOPC_clock_0_in;
  input            cpu_data_master_granted_DE2_70_SOPC_clock_2_in;
  input            cpu_data_master_granted_SEG7_s1;
  input            cpu_data_master_granted_control_in_pio_s1;
  input            cpu_data_master_granted_control_out_pio_s1;
  input            cpu_data_master_granted_cpu_jtag_debug_module;
  input            cpu_data_master_granted_data_in_pio_s1;
  input            cpu_data_master_granted_data_out_pio_s1;
  input            cpu_data_master_granted_epcs_flash_controller_0_epcs_control_port;
  input            cpu_data_master_granted_jtag_uart_avalon_jtag_slave;
  input            cpu_data_master_granted_lcd_control_slave;
  input            cpu_data_master_granted_onchip_mem_s1;
  input            cpu_data_master_granted_pio_button_s1;
  input            cpu_data_master_granted_pio_switch_s1;
  input            cpu_data_master_granted_ssram_s1;
  input            cpu_data_master_granted_sysid_control_slave;
  input            cpu_data_master_granted_timer_s1;
  input            cpu_data_master_granted_timer_stamp_s1;
  input            cpu_data_master_qualified_request_DE2_70_SOPC_clock_0_in;
  input            cpu_data_master_qualified_request_DE2_70_SOPC_clock_2_in;
  input            cpu_data_master_qualified_request_SEG7_s1;
  input            cpu_data_master_qualified_request_control_in_pio_s1;
  input            cpu_data_master_qualified_request_control_out_pio_s1;
  input            cpu_data_master_qualified_request_cpu_jtag_debug_module;
  input            cpu_data_master_qualified_request_data_in_pio_s1;
  input            cpu_data_master_qualified_request_data_out_pio_s1;
  input            cpu_data_master_qualified_request_epcs_flash_controller_0_epcs_control_port;
  input            cpu_data_master_qualified_request_jtag_uart_avalon_jtag_slave;
  input            cpu_data_master_qualified_request_lcd_control_slave;
  input            cpu_data_master_qualified_request_onchip_mem_s1;
  input            cpu_data_master_qualified_request_pio_button_s1;
  input            cpu_data_master_qualified_request_pio_switch_s1;
  input            cpu_data_master_qualified_request_ssram_s1;
  input            cpu_data_master_qualified_request_sysid_control_slave;
  input            cpu_data_master_qualified_request_timer_s1;
  input            cpu_data_master_qualified_request_timer_stamp_s1;
  input            cpu_data_master_read;
  input            cpu_data_master_read_data_valid_DE2_70_SOPC_clock_0_in;
  input            cpu_data_master_read_data_valid_DE2_70_SOPC_clock_2_in;
  input            cpu_data_master_read_data_valid_SEG7_s1;
  input            cpu_data_master_read_data_valid_control_in_pio_s1;
  input            cpu_data_master_read_data_valid_control_out_pio_s1;
  input            cpu_data_master_read_data_valid_cpu_jtag_debug_module;
  input            cpu_data_master_read_data_valid_data_in_pio_s1;
  input            cpu_data_master_read_data_valid_data_out_pio_s1;
  input            cpu_data_master_read_data_valid_epcs_flash_controller_0_epcs_control_port;
  input            cpu_data_master_read_data_valid_jtag_uart_avalon_jtag_slave;
  input            cpu_data_master_read_data_valid_lcd_control_slave;
  input            cpu_data_master_read_data_valid_onchip_mem_s1;
  input            cpu_data_master_read_data_valid_pio_button_s1;
  input            cpu_data_master_read_data_valid_pio_switch_s1;
  input            cpu_data_master_read_data_valid_ssram_s1;
  input            cpu_data_master_read_data_valid_sysid_control_slave;
  input            cpu_data_master_read_data_valid_timer_s1;
  input            cpu_data_master_read_data_valid_timer_stamp_s1;
  input            cpu_data_master_requests_DE2_70_SOPC_clock_0_in;
  input            cpu_data_master_requests_DE2_70_SOPC_clock_2_in;
  input            cpu_data_master_requests_SEG7_s1;
  input            cpu_data_master_requests_control_in_pio_s1;
  input            cpu_data_master_requests_control_out_pio_s1;
  input            cpu_data_master_requests_cpu_jtag_debug_module;
  input            cpu_data_master_requests_data_in_pio_s1;
  input            cpu_data_master_requests_data_out_pio_s1;
  input            cpu_data_master_requests_epcs_flash_controller_0_epcs_control_port;
  input            cpu_data_master_requests_jtag_uart_avalon_jtag_slave;
  input            cpu_data_master_requests_lcd_control_slave;
  input            cpu_data_master_requests_onchip_mem_s1;
  input            cpu_data_master_requests_pio_button_s1;
  input            cpu_data_master_requests_pio_switch_s1;
  input            cpu_data_master_requests_ssram_s1;
  input            cpu_data_master_requests_sysid_control_slave;
  input            cpu_data_master_requests_timer_s1;
  input            cpu_data_master_requests_timer_stamp_s1;
  input            cpu_data_master_write;
  input   [ 31: 0] cpu_data_master_writedata;
  input   [ 31: 0] cpu_jtag_debug_module_readdata_from_sa;
  input            d1_DE2_70_SOPC_clock_0_in_end_xfer;
  input            d1_DE2_70_SOPC_clock_2_in_end_xfer;
  input            d1_SEG7_s1_end_xfer;
  input            d1_control_in_pio_s1_end_xfer;
  input            d1_control_out_pio_s1_end_xfer;
  input            d1_cpu_jtag_debug_module_end_xfer;
  input            d1_data_in_pio_s1_end_xfer;
  input            d1_data_out_pio_s1_end_xfer;
  input            d1_epcs_flash_controller_0_epcs_control_port_end_xfer;
  input            d1_jtag_uart_avalon_jtag_slave_end_xfer;
  input            d1_lcd_control_slave_end_xfer;
  input            d1_onchip_mem_s1_end_xfer;
  input            d1_pio_button_s1_end_xfer;
  input            d1_pio_switch_s1_end_xfer;
  input            d1_sysid_control_slave_end_xfer;
  input            d1_timer_s1_end_xfer;
  input            d1_timer_stamp_s1_end_xfer;
  input            d1_tristate_bridge_ssram_avalon_slave_end_xfer;
  input   [ 31: 0] data_in_pio_s1_readdata_from_sa;
  input   [ 31: 0] data_out_pio_s1_readdata_from_sa;
  input            epcs_flash_controller_0_epcs_control_port_irq_from_sa;
  input   [ 31: 0] epcs_flash_controller_0_epcs_control_port_readdata_from_sa;
  input   [ 31: 0] incoming_data_to_and_from_the_ssram;
  input            jtag_uart_avalon_jtag_slave_irq_from_sa;
  input   [ 31: 0] jtag_uart_avalon_jtag_slave_readdata_from_sa;
  input            jtag_uart_avalon_jtag_slave_waitrequest_from_sa;
  input   [  7: 0] lcd_control_slave_readdata_from_sa;
  input            lcd_control_slave_wait_counter_eq_0;
  input   [ 31: 0] onchip_mem_s1_readdata_from_sa;
  input            pio_button_s1_irq_from_sa;
  input   [ 31: 0] pio_button_s1_readdata_from_sa;
  input   [ 31: 0] pio_switch_s1_readdata_from_sa;
  input            pll_c0_out;
  input            pll_c0_out_reset_n;
  input            reset_n;
  input   [ 31: 0] sysid_control_slave_readdata_from_sa;
  input            timer_s1_irq_from_sa;
  input   [ 15: 0] timer_s1_readdata_from_sa;
  input            timer_stamp_s1_irq_from_sa;
  input   [ 15: 0] timer_stamp_s1_readdata_from_sa;

  reg              active_and_waiting_last_time;
  reg     [ 24: 0] cpu_data_master_address_last_time;
  wire    [ 24: 0] cpu_data_master_address_to_slave;
  reg     [  3: 0] cpu_data_master_byteenable_last_time;
  wire    [ 31: 0] cpu_data_master_irq;
  wire             cpu_data_master_is_granted_some_slave;
  reg     [  2: 0] cpu_data_master_latency_counter;
  reg              cpu_data_master_read_but_no_slave_selected;
  reg              cpu_data_master_read_last_time;
  wire    [ 31: 0] cpu_data_master_readdata;
  wire             cpu_data_master_readdatavalid;
  wire             cpu_data_master_run;
  wire             cpu_data_master_waitrequest;
  reg              cpu_data_master_write_last_time;
  reg     [ 31: 0] cpu_data_master_writedata_last_time;
  wire    [  2: 0] latency_load_value;
  wire    [  2: 0] p1_cpu_data_master_latency_counter;
  wire             pll_c0_out_DM9000A_s1_irq_from_sa;
  wire             pre_flush_cpu_data_master_readdatavalid;
  wire             r_0;
  wire             r_1;
  wire             r_2;
  wire             r_3;
  //r_0 master_run cascaded wait assignment, which is an e_assign
  assign r_0 = 1 & (cpu_data_master_qualified_request_DE2_70_SOPC_clock_0_in | ~cpu_data_master_requests_DE2_70_SOPC_clock_0_in) & ((~cpu_data_master_qualified_request_DE2_70_SOPC_clock_0_in | ~(cpu_data_master_read | cpu_data_master_write) | (1 & ~DE2_70_SOPC_clock_0_in_waitrequest_from_sa & (cpu_data_master_read | cpu_data_master_write)))) & ((~cpu_data_master_qualified_request_DE2_70_SOPC_clock_0_in | ~(cpu_data_master_read | cpu_data_master_write) | (1 & ~DE2_70_SOPC_clock_0_in_waitrequest_from_sa & (cpu_data_master_read | cpu_data_master_write)))) & 1 & (cpu_data_master_qualified_request_DE2_70_SOPC_clock_2_in | ~cpu_data_master_requests_DE2_70_SOPC_clock_2_in) & ((~cpu_data_master_qualified_request_DE2_70_SOPC_clock_2_in | ~(cpu_data_master_read | cpu_data_master_write) | (1 & ~DE2_70_SOPC_clock_2_in_waitrequest_from_sa & (cpu_data_master_read | cpu_data_master_write)))) & ((~cpu_data_master_qualified_request_DE2_70_SOPC_clock_2_in | ~(cpu_data_master_read | cpu_data_master_write) | (1 & ~DE2_70_SOPC_clock_2_in_waitrequest_from_sa & (cpu_data_master_read | cpu_data_master_write)))) & 1 & (cpu_data_master_qualified_request_SEG7_s1 | ~cpu_data_master_requests_SEG7_s1) & ((~cpu_data_master_qualified_request_SEG7_s1 | ~(cpu_data_master_read | cpu_data_master_write) | (1 & (cpu_data_master_read | cpu_data_master_write)))) & ((~cpu_data_master_qualified_request_SEG7_s1 | ~(cpu_data_master_read | cpu_data_master_write) | (1 & (cpu_data_master_read | cpu_data_master_write)))) & 1 & (cpu_data_master_qualified_request_control_in_pio_s1 | ~cpu_data_master_requests_control_in_pio_s1) & ((~cpu_data_master_qualified_request_control_in_pio_s1 | ~cpu_data_master_read | (1 & ~d1_control_in_pio_s1_end_xfer & cpu_data_master_read))) & ((~cpu_data_master_qualified_request_control_in_pio_s1 | ~cpu_data_master_write | (1 & cpu_data_master_write))) & 1 & (cpu_data_master_qualified_request_control_out_pio_s1 | ~cpu_data_master_requests_control_out_pio_s1) & ((~cpu_data_master_qualified_request_control_out_pio_s1 | ~cpu_data_master_read | (1 & ~d1_control_out_pio_s1_end_xfer & cpu_data_master_read))) & ((~cpu_data_master_qualified_request_control_out_pio_s1 | ~cpu_data_master_write | (1 & cpu_data_master_write)));

  //cascaded wait assignment, which is an e_assign
  assign cpu_data_master_run = r_0 & r_1 & r_2 & r_3;

  //r_1 master_run cascaded wait assignment, which is an e_assign
  assign r_1 = 1 & (cpu_data_master_qualified_request_cpu_jtag_debug_module | ~cpu_data_master_requests_cpu_jtag_debug_module) & (cpu_data_master_granted_cpu_jtag_debug_module | ~cpu_data_master_qualified_request_cpu_jtag_debug_module) & ((~cpu_data_master_qualified_request_cpu_jtag_debug_module | ~cpu_data_master_read | (1 & ~d1_cpu_jtag_debug_module_end_xfer & cpu_data_master_read))) & ((~cpu_data_master_qualified_request_cpu_jtag_debug_module | ~cpu_data_master_write | (1 & cpu_data_master_write))) & 1 & (cpu_data_master_qualified_request_data_in_pio_s1 | ~cpu_data_master_requests_data_in_pio_s1) & ((~cpu_data_master_qualified_request_data_in_pio_s1 | ~cpu_data_master_read | (1 & ~d1_data_in_pio_s1_end_xfer & cpu_data_master_read))) & ((~cpu_data_master_qualified_request_data_in_pio_s1 | ~cpu_data_master_write | (1 & cpu_data_master_write))) & 1 & (cpu_data_master_qualified_request_data_out_pio_s1 | ~cpu_data_master_requests_data_out_pio_s1) & ((~cpu_data_master_qualified_request_data_out_pio_s1 | ~cpu_data_master_read | (1 & ~d1_data_out_pio_s1_end_xfer & cpu_data_master_read))) & ((~cpu_data_master_qualified_request_data_out_pio_s1 | ~cpu_data_master_write | (1 & cpu_data_master_write))) & 1 & (cpu_data_master_qualified_request_epcs_flash_controller_0_epcs_control_port | ~cpu_data_master_requests_epcs_flash_controller_0_epcs_control_port) & (cpu_data_master_granted_epcs_flash_controller_0_epcs_control_port | ~cpu_data_master_qualified_request_epcs_flash_controller_0_epcs_control_port) & ((~cpu_data_master_qualified_request_epcs_flash_controller_0_epcs_control_port | ~(cpu_data_master_read | cpu_data_master_write) | (1 & ~d1_epcs_flash_controller_0_epcs_control_port_end_xfer & (cpu_data_master_read | cpu_data_master_write)))) & ((~cpu_data_master_qualified_request_epcs_flash_controller_0_epcs_control_port | ~(cpu_data_master_read | cpu_data_master_write) | (1 & ~d1_epcs_flash_controller_0_epcs_control_port_end_xfer & (cpu_data_master_read | cpu_data_master_write)))) & 1 & (cpu_data_master_qualified_request_jtag_uart_avalon_jtag_slave | ~cpu_data_master_requests_jtag_uart_avalon_jtag_slave);

  //r_2 master_run cascaded wait assignment, which is an e_assign
  assign r_2 = ((~cpu_data_master_qualified_request_jtag_uart_avalon_jtag_slave | ~(cpu_data_master_read | cpu_data_master_write) | (1 & ~jtag_uart_avalon_jtag_slave_waitrequest_from_sa & (cpu_data_master_read | cpu_data_master_write)))) & ((~cpu_data_master_qualified_request_jtag_uart_avalon_jtag_slave | ~(cpu_data_master_read | cpu_data_master_write) | (1 & ~jtag_uart_avalon_jtag_slave_waitrequest_from_sa & (cpu_data_master_read | cpu_data_master_write)))) & 1 & (cpu_data_master_qualified_request_lcd_control_slave | ~cpu_data_master_requests_lcd_control_slave) & ((~cpu_data_master_qualified_request_lcd_control_slave | ~cpu_data_master_read | (1 & ((lcd_control_slave_wait_counter_eq_0 & ~d1_lcd_control_slave_end_xfer)) & cpu_data_master_read))) & ((~cpu_data_master_qualified_request_lcd_control_slave | ~cpu_data_master_write | (1 & ((lcd_control_slave_wait_counter_eq_0 & ~d1_lcd_control_slave_end_xfer)) & cpu_data_master_write))) & 1 & (cpu_data_master_qualified_request_onchip_mem_s1 | ~cpu_data_master_requests_onchip_mem_s1) & (cpu_data_master_granted_onchip_mem_s1 | ~cpu_data_master_qualified_request_onchip_mem_s1) & ((~cpu_data_master_qualified_request_onchip_mem_s1 | ~(cpu_data_master_read | cpu_data_master_write) | (1 & (cpu_data_master_read | cpu_data_master_write)))) & ((~cpu_data_master_qualified_request_onchip_mem_s1 | ~(cpu_data_master_read | cpu_data_master_write) | (1 & (cpu_data_master_read | cpu_data_master_write)))) & 1 & (cpu_data_master_qualified_request_pio_button_s1 | ~cpu_data_master_requests_pio_button_s1) & ((~cpu_data_master_qualified_request_pio_button_s1 | ~cpu_data_master_read | (1 & ~d1_pio_button_s1_end_xfer & cpu_data_master_read))) & ((~cpu_data_master_qualified_request_pio_button_s1 | ~cpu_data_master_write | (1 & cpu_data_master_write))) & 1 & (cpu_data_master_qualified_request_pio_switch_s1 | ~cpu_data_master_requests_pio_switch_s1) & ((~cpu_data_master_qualified_request_pio_switch_s1 | ~cpu_data_master_read | (1 & ~d1_pio_switch_s1_end_xfer & cpu_data_master_read))) & ((~cpu_data_master_qualified_request_pio_switch_s1 | ~cpu_data_master_write | (1 & cpu_data_master_write))) & 1;

  //r_3 master_run cascaded wait assignment, which is an e_assign
  assign r_3 = (cpu_data_master_qualified_request_sysid_control_slave | ~cpu_data_master_requests_sysid_control_slave) & ((~cpu_data_master_qualified_request_sysid_control_slave | ~cpu_data_master_read | (1 & ~d1_sysid_control_slave_end_xfer & cpu_data_master_read))) & ((~cpu_data_master_qualified_request_sysid_control_slave | ~cpu_data_master_write | (1 & cpu_data_master_write))) & 1 & (cpu_data_master_qualified_request_timer_s1 | ~cpu_data_master_requests_timer_s1) & ((~cpu_data_master_qualified_request_timer_s1 | ~cpu_data_master_read | (1 & ~d1_timer_s1_end_xfer & cpu_data_master_read))) & ((~cpu_data_master_qualified_request_timer_s1 | ~cpu_data_master_write | (1 & cpu_data_master_write))) & 1 & (cpu_data_master_qualified_request_timer_stamp_s1 | ~cpu_data_master_requests_timer_stamp_s1) & ((~cpu_data_master_qualified_request_timer_stamp_s1 | ~cpu_data_master_read | (1 & ~d1_timer_stamp_s1_end_xfer & cpu_data_master_read))) & ((~cpu_data_master_qualified_request_timer_stamp_s1 | ~cpu_data_master_write | (1 & cpu_data_master_write))) & 1 & (cpu_data_master_qualified_request_ssram_s1 | ~cpu_data_master_requests_ssram_s1) & (cpu_data_master_granted_ssram_s1 | ~cpu_data_master_qualified_request_ssram_s1) & ((~cpu_data_master_qualified_request_ssram_s1 | ~(cpu_data_master_read | cpu_data_master_write) | (1 & (cpu_data_master_read | cpu_data_master_write)))) & ((~cpu_data_master_qualified_request_ssram_s1 | ~(cpu_data_master_read | cpu_data_master_write) | (1 & (cpu_data_master_read | cpu_data_master_write))));

  //optimize select-logic by passing only those address bits which matter.
  assign cpu_data_master_address_to_slave = {cpu_data_master_address[24],
    1'b0,
    cpu_data_master_address[22 : 0]};

  //cpu_data_master_read_but_no_slave_selected assignment, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          cpu_data_master_read_but_no_slave_selected <= 0;
      else 
        cpu_data_master_read_but_no_slave_selected <= cpu_data_master_read & cpu_data_master_run & ~cpu_data_master_is_granted_some_slave;
    end


  //some slave is getting selected, which is an e_mux
  assign cpu_data_master_is_granted_some_slave = cpu_data_master_granted_DE2_70_SOPC_clock_0_in |
    cpu_data_master_granted_DE2_70_SOPC_clock_2_in |
    cpu_data_master_granted_SEG7_s1 |
    cpu_data_master_granted_control_in_pio_s1 |
    cpu_data_master_granted_control_out_pio_s1 |
    cpu_data_master_granted_cpu_jtag_debug_module |
    cpu_data_master_granted_data_in_pio_s1 |
    cpu_data_master_granted_data_out_pio_s1 |
    cpu_data_master_granted_epcs_flash_controller_0_epcs_control_port |
    cpu_data_master_granted_jtag_uart_avalon_jtag_slave |
    cpu_data_master_granted_lcd_control_slave |
    cpu_data_master_granted_onchip_mem_s1 |
    cpu_data_master_granted_pio_button_s1 |
    cpu_data_master_granted_pio_switch_s1 |
    cpu_data_master_granted_sysid_control_slave |
    cpu_data_master_granted_timer_s1 |
    cpu_data_master_granted_timer_stamp_s1 |
    cpu_data_master_granted_ssram_s1;

  //latent slave read data valids which may be flushed, which is an e_mux
  assign pre_flush_cpu_data_master_readdatavalid = cpu_data_master_read_data_valid_onchip_mem_s1 |
    cpu_data_master_read_data_valid_ssram_s1;

  //latent slave read data valid which is not flushed, which is an e_mux
  assign cpu_data_master_readdatavalid = cpu_data_master_read_but_no_slave_selected |
    pre_flush_cpu_data_master_readdatavalid |
    cpu_data_master_read_data_valid_DE2_70_SOPC_clock_0_in |
    cpu_data_master_read_but_no_slave_selected |
    pre_flush_cpu_data_master_readdatavalid |
    cpu_data_master_read_data_valid_DE2_70_SOPC_clock_2_in |
    cpu_data_master_read_but_no_slave_selected |
    pre_flush_cpu_data_master_readdatavalid |
    cpu_data_master_read_data_valid_SEG7_s1 |
    cpu_data_master_read_but_no_slave_selected |
    pre_flush_cpu_data_master_readdatavalid |
    cpu_data_master_read_data_valid_control_in_pio_s1 |
    cpu_data_master_read_but_no_slave_selected |
    pre_flush_cpu_data_master_readdatavalid |
    cpu_data_master_read_data_valid_control_out_pio_s1 |
    cpu_data_master_read_but_no_slave_selected |
    pre_flush_cpu_data_master_readdatavalid |
    cpu_data_master_read_data_valid_cpu_jtag_debug_module |
    cpu_data_master_read_but_no_slave_selected |
    pre_flush_cpu_data_master_readdatavalid |
    cpu_data_master_read_data_valid_data_in_pio_s1 |
    cpu_data_master_read_but_no_slave_selected |
    pre_flush_cpu_data_master_readdatavalid |
    cpu_data_master_read_data_valid_data_out_pio_s1 |
    cpu_data_master_read_but_no_slave_selected |
    pre_flush_cpu_data_master_readdatavalid |
    cpu_data_master_read_data_valid_epcs_flash_controller_0_epcs_control_port |
    cpu_data_master_read_but_no_slave_selected |
    pre_flush_cpu_data_master_readdatavalid |
    cpu_data_master_read_data_valid_jtag_uart_avalon_jtag_slave |
    cpu_data_master_read_but_no_slave_selected |
    pre_flush_cpu_data_master_readdatavalid |
    cpu_data_master_read_data_valid_lcd_control_slave |
    cpu_data_master_read_but_no_slave_selected |
    pre_flush_cpu_data_master_readdatavalid |
    cpu_data_master_read_but_no_slave_selected |
    pre_flush_cpu_data_master_readdatavalid |
    cpu_data_master_read_data_valid_pio_button_s1 |
    cpu_data_master_read_but_no_slave_selected |
    pre_flush_cpu_data_master_readdatavalid |
    cpu_data_master_read_data_valid_pio_switch_s1 |
    cpu_data_master_read_but_no_slave_selected |
    pre_flush_cpu_data_master_readdatavalid |
    cpu_data_master_read_data_valid_sysid_control_slave |
    cpu_data_master_read_but_no_slave_selected |
    pre_flush_cpu_data_master_readdatavalid |
    cpu_data_master_read_data_valid_timer_s1 |
    cpu_data_master_read_but_no_slave_selected |
    pre_flush_cpu_data_master_readdatavalid |
    cpu_data_master_read_data_valid_timer_stamp_s1 |
    cpu_data_master_read_but_no_slave_selected |
    pre_flush_cpu_data_master_readdatavalid;

  //cpu/data_master readdata mux, which is an e_mux
  assign cpu_data_master_readdata = ({32 {~(cpu_data_master_qualified_request_DE2_70_SOPC_clock_0_in & cpu_data_master_read)}} | DE2_70_SOPC_clock_0_in_readdata_from_sa) &
    ({32 {~(cpu_data_master_qualified_request_DE2_70_SOPC_clock_2_in & cpu_data_master_read)}} | DE2_70_SOPC_clock_2_in_readdata_from_sa) &
    ({32 {~(cpu_data_master_qualified_request_SEG7_s1 & cpu_data_master_read)}} | SEG7_s1_readdata_from_sa) &
    ({32 {~(cpu_data_master_qualified_request_control_in_pio_s1 & cpu_data_master_read)}} | control_in_pio_s1_readdata_from_sa) &
    ({32 {~(cpu_data_master_qualified_request_control_out_pio_s1 & cpu_data_master_read)}} | control_out_pio_s1_readdata_from_sa) &
    ({32 {~(cpu_data_master_qualified_request_cpu_jtag_debug_module & cpu_data_master_read)}} | cpu_jtag_debug_module_readdata_from_sa) &
    ({32 {~(cpu_data_master_qualified_request_data_in_pio_s1 & cpu_data_master_read)}} | data_in_pio_s1_readdata_from_sa) &
    ({32 {~(cpu_data_master_qualified_request_data_out_pio_s1 & cpu_data_master_read)}} | data_out_pio_s1_readdata_from_sa) &
    ({32 {~(cpu_data_master_qualified_request_epcs_flash_controller_0_epcs_control_port & cpu_data_master_read)}} | epcs_flash_controller_0_epcs_control_port_readdata_from_sa) &
    ({32 {~(cpu_data_master_qualified_request_jtag_uart_avalon_jtag_slave & cpu_data_master_read)}} | jtag_uart_avalon_jtag_slave_readdata_from_sa) &
    ({32 {~(cpu_data_master_qualified_request_lcd_control_slave & cpu_data_master_read)}} | lcd_control_slave_readdata_from_sa) &
    ({32 {~cpu_data_master_read_data_valid_onchip_mem_s1}} | onchip_mem_s1_readdata_from_sa) &
    ({32 {~(cpu_data_master_qualified_request_pio_button_s1 & cpu_data_master_read)}} | pio_button_s1_readdata_from_sa) &
    ({32 {~(cpu_data_master_qualified_request_pio_switch_s1 & cpu_data_master_read)}} | pio_switch_s1_readdata_from_sa) &
    ({32 {~(cpu_data_master_qualified_request_sysid_control_slave & cpu_data_master_read)}} | sysid_control_slave_readdata_from_sa) &
    ({32 {~(cpu_data_master_qualified_request_timer_s1 & cpu_data_master_read)}} | timer_s1_readdata_from_sa) &
    ({32 {~(cpu_data_master_qualified_request_timer_stamp_s1 & cpu_data_master_read)}} | timer_stamp_s1_readdata_from_sa) &
    ({32 {~cpu_data_master_read_data_valid_ssram_s1}} | incoming_data_to_and_from_the_ssram);

  //actual waitrequest port, which is an e_assign
  assign cpu_data_master_waitrequest = ~cpu_data_master_run;

  //latent max counter, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          cpu_data_master_latency_counter <= 0;
      else 
        cpu_data_master_latency_counter <= p1_cpu_data_master_latency_counter;
    end


  //latency counter load mux, which is an e_mux
  assign p1_cpu_data_master_latency_counter = ((cpu_data_master_run & cpu_data_master_read))? latency_load_value :
    (cpu_data_master_latency_counter)? cpu_data_master_latency_counter - 1 :
    0;

  //read latency load values, which is an e_mux
  assign latency_load_value = ({3 {cpu_data_master_requests_onchip_mem_s1}} & 1) |
    ({3 {cpu_data_master_requests_ssram_s1}} & 4);

  //DM9000A_s1_irq_from_sa from clk_25 to pll_c0_out
  DM9000A_s1_irq_from_sa_clock_crossing_cpu_data_master_module DM9000A_s1_irq_from_sa_clock_crossing_cpu_data_master
    (
      .clk      (pll_c0_out),
      .data_in  (DM9000A_s1_irq_from_sa),
      .data_out (pll_c0_out_DM9000A_s1_irq_from_sa),
      .reset_n  (pll_c0_out_reset_n)
    );

  //irq assign, which is an e_assign
  assign cpu_data_master_irq = {1'b0,
    1'b0,
    1'b0,
    1'b0,
    1'b0,
    1'b0,
    1'b0,
    1'b0,
    1'b0,
    1'b0,
    1'b0,
    1'b0,
    1'b0,
    1'b0,
    1'b0,
    1'b0,
    1'b0,
    1'b0,
    1'b0,
    1'b0,
    1'b0,
    1'b0,
    1'b0,
    1'b0,
    1'b0,
    1'b0,
    epcs_flash_controller_0_epcs_control_port_irq_from_sa,
    pio_button_s1_irq_from_sa,
    pll_c0_out_DM9000A_s1_irq_from_sa,
    jtag_uart_avalon_jtag_slave_irq_from_sa,
    timer_stamp_s1_irq_from_sa,
    timer_s1_irq_from_sa};


//synthesis translate_off
//////////////// SIMULATION-ONLY CONTENTS
  //cpu_data_master_address check against wait, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          cpu_data_master_address_last_time <= 0;
      else 
        cpu_data_master_address_last_time <= cpu_data_master_address;
    end


  //cpu/data_master waited last time, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          active_and_waiting_last_time <= 0;
      else 
        active_and_waiting_last_time <= cpu_data_master_waitrequest & (cpu_data_master_read | cpu_data_master_write);
    end


  //cpu_data_master_address matches last port_name, which is an e_process
  always @(posedge clk)
    begin
      if (active_and_waiting_last_time & (cpu_data_master_address != cpu_data_master_address_last_time))
        begin
          $write("%0d ns: cpu_data_master_address did not heed wait!!!", $time);
          $stop;
        end
    end


  //cpu_data_master_byteenable check against wait, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          cpu_data_master_byteenable_last_time <= 0;
      else 
        cpu_data_master_byteenable_last_time <= cpu_data_master_byteenable;
    end


  //cpu_data_master_byteenable matches last port_name, which is an e_process
  always @(posedge clk)
    begin
      if (active_and_waiting_last_time & (cpu_data_master_byteenable != cpu_data_master_byteenable_last_time))
        begin
          $write("%0d ns: cpu_data_master_byteenable did not heed wait!!!", $time);
          $stop;
        end
    end


  //cpu_data_master_read check against wait, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          cpu_data_master_read_last_time <= 0;
      else 
        cpu_data_master_read_last_time <= cpu_data_master_read;
    end


  //cpu_data_master_read matches last port_name, which is an e_process
  always @(posedge clk)
    begin
      if (active_and_waiting_last_time & (cpu_data_master_read != cpu_data_master_read_last_time))
        begin
          $write("%0d ns: cpu_data_master_read did not heed wait!!!", $time);
          $stop;
        end
    end


  //cpu_data_master_write check against wait, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          cpu_data_master_write_last_time <= 0;
      else 
        cpu_data_master_write_last_time <= cpu_data_master_write;
    end


  //cpu_data_master_write matches last port_name, which is an e_process
  always @(posedge clk)
    begin
      if (active_and_waiting_last_time & (cpu_data_master_write != cpu_data_master_write_last_time))
        begin
          $write("%0d ns: cpu_data_master_write did not heed wait!!!", $time);
          $stop;
        end
    end


  //cpu_data_master_writedata check against wait, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          cpu_data_master_writedata_last_time <= 0;
      else 
        cpu_data_master_writedata_last_time <= cpu_data_master_writedata;
    end


  //cpu_data_master_writedata matches last port_name, which is an e_process
  always @(posedge clk)
    begin
      if (active_and_waiting_last_time & (cpu_data_master_writedata != cpu_data_master_writedata_last_time) & cpu_data_master_write)
        begin
          $write("%0d ns: cpu_data_master_writedata did not heed wait!!!", $time);
          $stop;
        end
    end



//////////////// END SIMULATION-ONLY CONTENTS

//synthesis translate_on

endmodule


// synthesis translate_off
`timescale 1ns / 1ps
// synthesis translate_on

// turn off superfluous verilog processor warnings 
// altera message_level Level1 
// altera message_off 10034 10035 10036 10037 10230 10240 10030 

module cpu_instruction_master_arbitrator (
                                           // inputs:
                                            clk,
                                            cpu_instruction_master_address,
                                            cpu_instruction_master_granted_cpu_jtag_debug_module,
                                            cpu_instruction_master_granted_epcs_flash_controller_0_epcs_control_port,
                                            cpu_instruction_master_granted_onchip_mem_s1,
                                            cpu_instruction_master_granted_ssram_s1,
                                            cpu_instruction_master_qualified_request_cpu_jtag_debug_module,
                                            cpu_instruction_master_qualified_request_epcs_flash_controller_0_epcs_control_port,
                                            cpu_instruction_master_qualified_request_onchip_mem_s1,
                                            cpu_instruction_master_qualified_request_ssram_s1,
                                            cpu_instruction_master_read,
                                            cpu_instruction_master_read_data_valid_cpu_jtag_debug_module,
                                            cpu_instruction_master_read_data_valid_epcs_flash_controller_0_epcs_control_port,
                                            cpu_instruction_master_read_data_valid_onchip_mem_s1,
                                            cpu_instruction_master_read_data_valid_ssram_s1,
                                            cpu_instruction_master_requests_cpu_jtag_debug_module,
                                            cpu_instruction_master_requests_epcs_flash_controller_0_epcs_control_port,
                                            cpu_instruction_master_requests_onchip_mem_s1,
                                            cpu_instruction_master_requests_ssram_s1,
                                            cpu_jtag_debug_module_readdata_from_sa,
                                            d1_cpu_jtag_debug_module_end_xfer,
                                            d1_epcs_flash_controller_0_epcs_control_port_end_xfer,
                                            d1_onchip_mem_s1_end_xfer,
                                            d1_tristate_bridge_ssram_avalon_slave_end_xfer,
                                            epcs_flash_controller_0_epcs_control_port_readdata_from_sa,
                                            incoming_data_to_and_from_the_ssram,
                                            onchip_mem_s1_readdata_from_sa,
                                            reset_n,

                                           // outputs:
                                            cpu_instruction_master_address_to_slave,
                                            cpu_instruction_master_latency_counter,
                                            cpu_instruction_master_readdata,
                                            cpu_instruction_master_readdatavalid,
                                            cpu_instruction_master_waitrequest
                                         )
;

  output  [ 24: 0] cpu_instruction_master_address_to_slave;
  output  [  2: 0] cpu_instruction_master_latency_counter;
  output  [ 31: 0] cpu_instruction_master_readdata;
  output           cpu_instruction_master_readdatavalid;
  output           cpu_instruction_master_waitrequest;
  input            clk;
  input   [ 24: 0] cpu_instruction_master_address;
  input            cpu_instruction_master_granted_cpu_jtag_debug_module;
  input            cpu_instruction_master_granted_epcs_flash_controller_0_epcs_control_port;
  input            cpu_instruction_master_granted_onchip_mem_s1;
  input            cpu_instruction_master_granted_ssram_s1;
  input            cpu_instruction_master_qualified_request_cpu_jtag_debug_module;
  input            cpu_instruction_master_qualified_request_epcs_flash_controller_0_epcs_control_port;
  input            cpu_instruction_master_qualified_request_onchip_mem_s1;
  input            cpu_instruction_master_qualified_request_ssram_s1;
  input            cpu_instruction_master_read;
  input            cpu_instruction_master_read_data_valid_cpu_jtag_debug_module;
  input            cpu_instruction_master_read_data_valid_epcs_flash_controller_0_epcs_control_port;
  input            cpu_instruction_master_read_data_valid_onchip_mem_s1;
  input            cpu_instruction_master_read_data_valid_ssram_s1;
  input            cpu_instruction_master_requests_cpu_jtag_debug_module;
  input            cpu_instruction_master_requests_epcs_flash_controller_0_epcs_control_port;
  input            cpu_instruction_master_requests_onchip_mem_s1;
  input            cpu_instruction_master_requests_ssram_s1;
  input   [ 31: 0] cpu_jtag_debug_module_readdata_from_sa;
  input            d1_cpu_jtag_debug_module_end_xfer;
  input            d1_epcs_flash_controller_0_epcs_control_port_end_xfer;
  input            d1_onchip_mem_s1_end_xfer;
  input            d1_tristate_bridge_ssram_avalon_slave_end_xfer;
  input   [ 31: 0] epcs_flash_controller_0_epcs_control_port_readdata_from_sa;
  input   [ 31: 0] incoming_data_to_and_from_the_ssram;
  input   [ 31: 0] onchip_mem_s1_readdata_from_sa;
  input            reset_n;

  reg              active_and_waiting_last_time;
  reg     [ 24: 0] cpu_instruction_master_address_last_time;
  wire    [ 24: 0] cpu_instruction_master_address_to_slave;
  wire             cpu_instruction_master_is_granted_some_slave;
  reg     [  2: 0] cpu_instruction_master_latency_counter;
  reg              cpu_instruction_master_read_but_no_slave_selected;
  reg              cpu_instruction_master_read_last_time;
  wire    [ 31: 0] cpu_instruction_master_readdata;
  wire             cpu_instruction_master_readdatavalid;
  wire             cpu_instruction_master_run;
  wire             cpu_instruction_master_waitrequest;
  wire    [  2: 0] latency_load_value;
  wire    [  2: 0] p1_cpu_instruction_master_latency_counter;
  wire             pre_flush_cpu_instruction_master_readdatavalid;
  wire             r_1;
  wire             r_2;
  wire             r_3;
  //r_1 master_run cascaded wait assignment, which is an e_assign
  assign r_1 = 1 & (cpu_instruction_master_qualified_request_cpu_jtag_debug_module | ~cpu_instruction_master_requests_cpu_jtag_debug_module) & (cpu_instruction_master_granted_cpu_jtag_debug_module | ~cpu_instruction_master_qualified_request_cpu_jtag_debug_module) & ((~cpu_instruction_master_qualified_request_cpu_jtag_debug_module | ~cpu_instruction_master_read | (1 & ~d1_cpu_jtag_debug_module_end_xfer & cpu_instruction_master_read))) & 1 & (cpu_instruction_master_qualified_request_epcs_flash_controller_0_epcs_control_port | ~cpu_instruction_master_requests_epcs_flash_controller_0_epcs_control_port) & (cpu_instruction_master_granted_epcs_flash_controller_0_epcs_control_port | ~cpu_instruction_master_qualified_request_epcs_flash_controller_0_epcs_control_port) & ((~cpu_instruction_master_qualified_request_epcs_flash_controller_0_epcs_control_port | ~(cpu_instruction_master_read) | (1 & ~d1_epcs_flash_controller_0_epcs_control_port_end_xfer & (cpu_instruction_master_read))));

  //cascaded wait assignment, which is an e_assign
  assign cpu_instruction_master_run = r_1 & r_2 & r_3;

  //r_2 master_run cascaded wait assignment, which is an e_assign
  assign r_2 = 1 & (cpu_instruction_master_qualified_request_onchip_mem_s1 | ~cpu_instruction_master_requests_onchip_mem_s1) & (cpu_instruction_master_granted_onchip_mem_s1 | ~cpu_instruction_master_qualified_request_onchip_mem_s1) & ((~cpu_instruction_master_qualified_request_onchip_mem_s1 | ~(cpu_instruction_master_read) | (1 & (cpu_instruction_master_read))));

  //r_3 master_run cascaded wait assignment, which is an e_assign
  assign r_3 = 1 & (cpu_instruction_master_qualified_request_ssram_s1 | ~cpu_instruction_master_requests_ssram_s1) & (cpu_instruction_master_granted_ssram_s1 | ~cpu_instruction_master_qualified_request_ssram_s1) & ((~cpu_instruction_master_qualified_request_ssram_s1 | ~(cpu_instruction_master_read) | (1 & (cpu_instruction_master_read))));

  //optimize select-logic by passing only those address bits which matter.
  assign cpu_instruction_master_address_to_slave = {2'b10,
    cpu_instruction_master_address[22 : 0]};

  //cpu_instruction_master_read_but_no_slave_selected assignment, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          cpu_instruction_master_read_but_no_slave_selected <= 0;
      else 
        cpu_instruction_master_read_but_no_slave_selected <= cpu_instruction_master_read & cpu_instruction_master_run & ~cpu_instruction_master_is_granted_some_slave;
    end


  //some slave is getting selected, which is an e_mux
  assign cpu_instruction_master_is_granted_some_slave = cpu_instruction_master_granted_cpu_jtag_debug_module |
    cpu_instruction_master_granted_epcs_flash_controller_0_epcs_control_port |
    cpu_instruction_master_granted_onchip_mem_s1 |
    cpu_instruction_master_granted_ssram_s1;

  //latent slave read data valids which may be flushed, which is an e_mux
  assign pre_flush_cpu_instruction_master_readdatavalid = cpu_instruction_master_read_data_valid_onchip_mem_s1 |
    cpu_instruction_master_read_data_valid_ssram_s1;

  //latent slave read data valid which is not flushed, which is an e_mux
  assign cpu_instruction_master_readdatavalid = cpu_instruction_master_read_but_no_slave_selected |
    pre_flush_cpu_instruction_master_readdatavalid |
    cpu_instruction_master_read_data_valid_cpu_jtag_debug_module |
    cpu_instruction_master_read_but_no_slave_selected |
    pre_flush_cpu_instruction_master_readdatavalid |
    cpu_instruction_master_read_data_valid_epcs_flash_controller_0_epcs_control_port |
    cpu_instruction_master_read_but_no_slave_selected |
    pre_flush_cpu_instruction_master_readdatavalid |
    cpu_instruction_master_read_but_no_slave_selected |
    pre_flush_cpu_instruction_master_readdatavalid;

  //cpu/instruction_master readdata mux, which is an e_mux
  assign cpu_instruction_master_readdata = ({32 {~(cpu_instruction_master_qualified_request_cpu_jtag_debug_module & cpu_instruction_master_read)}} | cpu_jtag_debug_module_readdata_from_sa) &
    ({32 {~(cpu_instruction_master_qualified_request_epcs_flash_controller_0_epcs_control_port & cpu_instruction_master_read)}} | epcs_flash_controller_0_epcs_control_port_readdata_from_sa) &
    ({32 {~cpu_instruction_master_read_data_valid_onchip_mem_s1}} | onchip_mem_s1_readdata_from_sa) &
    ({32 {~cpu_instruction_master_read_data_valid_ssram_s1}} | incoming_data_to_and_from_the_ssram);

  //actual waitrequest port, which is an e_assign
  assign cpu_instruction_master_waitrequest = ~cpu_instruction_master_run;

  //latent max counter, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          cpu_instruction_master_latency_counter <= 0;
      else 
        cpu_instruction_master_latency_counter <= p1_cpu_instruction_master_latency_counter;
    end


  //latency counter load mux, which is an e_mux
  assign p1_cpu_instruction_master_latency_counter = ((cpu_instruction_master_run & cpu_instruction_master_read))? latency_load_value :
    (cpu_instruction_master_latency_counter)? cpu_instruction_master_latency_counter - 1 :
    0;

  //read latency load values, which is an e_mux
  assign latency_load_value = ({3 {cpu_instruction_master_requests_onchip_mem_s1}} & 1) |
    ({3 {cpu_instruction_master_requests_ssram_s1}} & 4);


//synthesis translate_off
//////////////// SIMULATION-ONLY CONTENTS
  //cpu_instruction_master_address check against wait, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          cpu_instruction_master_address_last_time <= 0;
      else 
        cpu_instruction_master_address_last_time <= cpu_instruction_master_address;
    end


  //cpu/instruction_master waited last time, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          active_and_waiting_last_time <= 0;
      else 
        active_and_waiting_last_time <= cpu_instruction_master_waitrequest & (cpu_instruction_master_read);
    end


  //cpu_instruction_master_address matches last port_name, which is an e_process
  always @(posedge clk)
    begin
      if (active_and_waiting_last_time & (cpu_instruction_master_address != cpu_instruction_master_address_last_time))
        begin
          $write("%0d ns: cpu_instruction_master_address did not heed wait!!!", $time);
          $stop;
        end
    end


  //cpu_instruction_master_read check against wait, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          cpu_instruction_master_read_last_time <= 0;
      else 
        cpu_instruction_master_read_last_time <= cpu_instruction_master_read;
    end


  //cpu_instruction_master_read matches last port_name, which is an e_process
  always @(posedge clk)
    begin
      if (active_and_waiting_last_time & (cpu_instruction_master_read != cpu_instruction_master_read_last_time))
        begin
          $write("%0d ns: cpu_instruction_master_read did not heed wait!!!", $time);
          $stop;
        end
    end



//////////////// END SIMULATION-ONLY CONTENTS

//synthesis translate_on

endmodule


// synthesis translate_off
`timescale 1ns / 1ps
// synthesis translate_on

// turn off superfluous verilog processor warnings 
// altera message_level Level1 
// altera message_off 10034 10035 10036 10037 10230 10240 10030 

module data_in_pio_s1_arbitrator (
                                   // inputs:
                                    clk,
                                    cpu_data_master_address_to_slave,
                                    cpu_data_master_latency_counter,
                                    cpu_data_master_read,
                                    cpu_data_master_write,
                                    cpu_data_master_writedata,
                                    data_in_pio_s1_readdata,
                                    reset_n,

                                   // outputs:
                                    cpu_data_master_granted_data_in_pio_s1,
                                    cpu_data_master_qualified_request_data_in_pio_s1,
                                    cpu_data_master_read_data_valid_data_in_pio_s1,
                                    cpu_data_master_requests_data_in_pio_s1,
                                    d1_data_in_pio_s1_end_xfer,
                                    data_in_pio_s1_address,
                                    data_in_pio_s1_chipselect,
                                    data_in_pio_s1_readdata_from_sa,
                                    data_in_pio_s1_reset_n,
                                    data_in_pio_s1_write_n,
                                    data_in_pio_s1_writedata
                                 )
;

  output           cpu_data_master_granted_data_in_pio_s1;
  output           cpu_data_master_qualified_request_data_in_pio_s1;
  output           cpu_data_master_read_data_valid_data_in_pio_s1;
  output           cpu_data_master_requests_data_in_pio_s1;
  output           d1_data_in_pio_s1_end_xfer;
  output  [  1: 0] data_in_pio_s1_address;
  output           data_in_pio_s1_chipselect;
  output  [ 31: 0] data_in_pio_s1_readdata_from_sa;
  output           data_in_pio_s1_reset_n;
  output           data_in_pio_s1_write_n;
  output  [ 31: 0] data_in_pio_s1_writedata;
  input            clk;
  input   [ 24: 0] cpu_data_master_address_to_slave;
  input   [  2: 0] cpu_data_master_latency_counter;
  input            cpu_data_master_read;
  input            cpu_data_master_write;
  input   [ 31: 0] cpu_data_master_writedata;
  input   [ 31: 0] data_in_pio_s1_readdata;
  input            reset_n;

  wire             cpu_data_master_arbiterlock;
  wire             cpu_data_master_arbiterlock2;
  wire             cpu_data_master_continuerequest;
  wire             cpu_data_master_granted_data_in_pio_s1;
  wire             cpu_data_master_qualified_request_data_in_pio_s1;
  wire             cpu_data_master_read_data_valid_data_in_pio_s1;
  wire             cpu_data_master_requests_data_in_pio_s1;
  wire             cpu_data_master_saved_grant_data_in_pio_s1;
  reg              d1_data_in_pio_s1_end_xfer;
  reg              d1_reasons_to_wait;
  wire    [  1: 0] data_in_pio_s1_address;
  wire             data_in_pio_s1_allgrants;
  wire             data_in_pio_s1_allow_new_arb_cycle;
  wire             data_in_pio_s1_any_bursting_master_saved_grant;
  wire             data_in_pio_s1_any_continuerequest;
  wire             data_in_pio_s1_arb_counter_enable;
  reg              data_in_pio_s1_arb_share_counter;
  wire             data_in_pio_s1_arb_share_counter_next_value;
  wire             data_in_pio_s1_arb_share_set_values;
  wire             data_in_pio_s1_beginbursttransfer_internal;
  wire             data_in_pio_s1_begins_xfer;
  wire             data_in_pio_s1_chipselect;
  wire             data_in_pio_s1_end_xfer;
  wire             data_in_pio_s1_firsttransfer;
  wire             data_in_pio_s1_grant_vector;
  wire             data_in_pio_s1_in_a_read_cycle;
  wire             data_in_pio_s1_in_a_write_cycle;
  wire             data_in_pio_s1_master_qreq_vector;
  wire             data_in_pio_s1_non_bursting_master_requests;
  wire    [ 31: 0] data_in_pio_s1_readdata_from_sa;
  reg              data_in_pio_s1_reg_firsttransfer;
  wire             data_in_pio_s1_reset_n;
  reg              data_in_pio_s1_slavearbiterlockenable;
  wire             data_in_pio_s1_slavearbiterlockenable2;
  wire             data_in_pio_s1_unreg_firsttransfer;
  wire             data_in_pio_s1_waits_for_read;
  wire             data_in_pio_s1_waits_for_write;
  wire             data_in_pio_s1_write_n;
  wire    [ 31: 0] data_in_pio_s1_writedata;
  reg              enable_nonzero_assertions;
  wire             end_xfer_arb_share_counter_term_data_in_pio_s1;
  wire             in_a_read_cycle;
  wire             in_a_write_cycle;
  wire    [ 24: 0] shifted_address_to_data_in_pio_s1_from_cpu_data_master;
  wire             wait_for_data_in_pio_s1_counter;
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          d1_reasons_to_wait <= 0;
      else 
        d1_reasons_to_wait <= ~data_in_pio_s1_end_xfer;
    end


  assign data_in_pio_s1_begins_xfer = ~d1_reasons_to_wait & ((cpu_data_master_qualified_request_data_in_pio_s1));
  //assign data_in_pio_s1_readdata_from_sa = data_in_pio_s1_readdata so that symbol knows where to group signals which may go to master only, which is an e_assign
  assign data_in_pio_s1_readdata_from_sa = data_in_pio_s1_readdata;

  assign cpu_data_master_requests_data_in_pio_s1 = ({cpu_data_master_address_to_slave[24 : 4] , 4'b0} == 25'h14090f0) & (cpu_data_master_read | cpu_data_master_write);
  //data_in_pio_s1_arb_share_counter set values, which is an e_mux
  assign data_in_pio_s1_arb_share_set_values = 1;

  //data_in_pio_s1_non_bursting_master_requests mux, which is an e_mux
  assign data_in_pio_s1_non_bursting_master_requests = cpu_data_master_requests_data_in_pio_s1;

  //data_in_pio_s1_any_bursting_master_saved_grant mux, which is an e_mux
  assign data_in_pio_s1_any_bursting_master_saved_grant = 0;

  //data_in_pio_s1_arb_share_counter_next_value assignment, which is an e_assign
  assign data_in_pio_s1_arb_share_counter_next_value = data_in_pio_s1_firsttransfer ? (data_in_pio_s1_arb_share_set_values - 1) : |data_in_pio_s1_arb_share_counter ? (data_in_pio_s1_arb_share_counter - 1) : 0;

  //data_in_pio_s1_allgrants all slave grants, which is an e_mux
  assign data_in_pio_s1_allgrants = |data_in_pio_s1_grant_vector;

  //data_in_pio_s1_end_xfer assignment, which is an e_assign
  assign data_in_pio_s1_end_xfer = ~(data_in_pio_s1_waits_for_read | data_in_pio_s1_waits_for_write);

  //end_xfer_arb_share_counter_term_data_in_pio_s1 arb share counter enable term, which is an e_assign
  assign end_xfer_arb_share_counter_term_data_in_pio_s1 = data_in_pio_s1_end_xfer & (~data_in_pio_s1_any_bursting_master_saved_grant | in_a_read_cycle | in_a_write_cycle);

  //data_in_pio_s1_arb_share_counter arbitration counter enable, which is an e_assign
  assign data_in_pio_s1_arb_counter_enable = (end_xfer_arb_share_counter_term_data_in_pio_s1 & data_in_pio_s1_allgrants) | (end_xfer_arb_share_counter_term_data_in_pio_s1 & ~data_in_pio_s1_non_bursting_master_requests);

  //data_in_pio_s1_arb_share_counter counter, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          data_in_pio_s1_arb_share_counter <= 0;
      else if (data_in_pio_s1_arb_counter_enable)
          data_in_pio_s1_arb_share_counter <= data_in_pio_s1_arb_share_counter_next_value;
    end


  //data_in_pio_s1_slavearbiterlockenable slave enables arbiterlock, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          data_in_pio_s1_slavearbiterlockenable <= 0;
      else if ((|data_in_pio_s1_master_qreq_vector & end_xfer_arb_share_counter_term_data_in_pio_s1) | (end_xfer_arb_share_counter_term_data_in_pio_s1 & ~data_in_pio_s1_non_bursting_master_requests))
          data_in_pio_s1_slavearbiterlockenable <= |data_in_pio_s1_arb_share_counter_next_value;
    end


  //cpu/data_master data_in_pio/s1 arbiterlock, which is an e_assign
  assign cpu_data_master_arbiterlock = data_in_pio_s1_slavearbiterlockenable & cpu_data_master_continuerequest;

  //data_in_pio_s1_slavearbiterlockenable2 slave enables arbiterlock2, which is an e_assign
  assign data_in_pio_s1_slavearbiterlockenable2 = |data_in_pio_s1_arb_share_counter_next_value;

  //cpu/data_master data_in_pio/s1 arbiterlock2, which is an e_assign
  assign cpu_data_master_arbiterlock2 = data_in_pio_s1_slavearbiterlockenable2 & cpu_data_master_continuerequest;

  //data_in_pio_s1_any_continuerequest at least one master continues requesting, which is an e_assign
  assign data_in_pio_s1_any_continuerequest = 1;

  //cpu_data_master_continuerequest continued request, which is an e_assign
  assign cpu_data_master_continuerequest = 1;

  assign cpu_data_master_qualified_request_data_in_pio_s1 = cpu_data_master_requests_data_in_pio_s1 & ~((cpu_data_master_read & ((cpu_data_master_latency_counter != 0))));
  //local readdatavalid cpu_data_master_read_data_valid_data_in_pio_s1, which is an e_mux
  assign cpu_data_master_read_data_valid_data_in_pio_s1 = cpu_data_master_granted_data_in_pio_s1 & cpu_data_master_read & ~data_in_pio_s1_waits_for_read;

  //data_in_pio_s1_writedata mux, which is an e_mux
  assign data_in_pio_s1_writedata = cpu_data_master_writedata;

  //master is always granted when requested
  assign cpu_data_master_granted_data_in_pio_s1 = cpu_data_master_qualified_request_data_in_pio_s1;

  //cpu/data_master saved-grant data_in_pio/s1, which is an e_assign
  assign cpu_data_master_saved_grant_data_in_pio_s1 = cpu_data_master_requests_data_in_pio_s1;

  //allow new arb cycle for data_in_pio/s1, which is an e_assign
  assign data_in_pio_s1_allow_new_arb_cycle = 1;

  //placeholder chosen master
  assign data_in_pio_s1_grant_vector = 1;

  //placeholder vector of master qualified-requests
  assign data_in_pio_s1_master_qreq_vector = 1;

  //data_in_pio_s1_reset_n assignment, which is an e_assign
  assign data_in_pio_s1_reset_n = reset_n;

  assign data_in_pio_s1_chipselect = cpu_data_master_granted_data_in_pio_s1;
  //data_in_pio_s1_firsttransfer first transaction, which is an e_assign
  assign data_in_pio_s1_firsttransfer = data_in_pio_s1_begins_xfer ? data_in_pio_s1_unreg_firsttransfer : data_in_pio_s1_reg_firsttransfer;

  //data_in_pio_s1_unreg_firsttransfer first transaction, which is an e_assign
  assign data_in_pio_s1_unreg_firsttransfer = ~(data_in_pio_s1_slavearbiterlockenable & data_in_pio_s1_any_continuerequest);

  //data_in_pio_s1_reg_firsttransfer first transaction, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          data_in_pio_s1_reg_firsttransfer <= 1'b1;
      else if (data_in_pio_s1_begins_xfer)
          data_in_pio_s1_reg_firsttransfer <= data_in_pio_s1_unreg_firsttransfer;
    end


  //data_in_pio_s1_beginbursttransfer_internal begin burst transfer, which is an e_assign
  assign data_in_pio_s1_beginbursttransfer_internal = data_in_pio_s1_begins_xfer;

  //~data_in_pio_s1_write_n assignment, which is an e_mux
  assign data_in_pio_s1_write_n = ~(cpu_data_master_granted_data_in_pio_s1 & cpu_data_master_write);

  assign shifted_address_to_data_in_pio_s1_from_cpu_data_master = cpu_data_master_address_to_slave;
  //data_in_pio_s1_address mux, which is an e_mux
  assign data_in_pio_s1_address = shifted_address_to_data_in_pio_s1_from_cpu_data_master >> 2;

  //d1_data_in_pio_s1_end_xfer register, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          d1_data_in_pio_s1_end_xfer <= 1;
      else 
        d1_data_in_pio_s1_end_xfer <= data_in_pio_s1_end_xfer;
    end


  //data_in_pio_s1_waits_for_read in a cycle, which is an e_mux
  assign data_in_pio_s1_waits_for_read = data_in_pio_s1_in_a_read_cycle & data_in_pio_s1_begins_xfer;

  //data_in_pio_s1_in_a_read_cycle assignment, which is an e_assign
  assign data_in_pio_s1_in_a_read_cycle = cpu_data_master_granted_data_in_pio_s1 & cpu_data_master_read;

  //in_a_read_cycle assignment, which is an e_mux
  assign in_a_read_cycle = data_in_pio_s1_in_a_read_cycle;

  //data_in_pio_s1_waits_for_write in a cycle, which is an e_mux
  assign data_in_pio_s1_waits_for_write = data_in_pio_s1_in_a_write_cycle & 0;

  //data_in_pio_s1_in_a_write_cycle assignment, which is an e_assign
  assign data_in_pio_s1_in_a_write_cycle = cpu_data_master_granted_data_in_pio_s1 & cpu_data_master_write;

  //in_a_write_cycle assignment, which is an e_mux
  assign in_a_write_cycle = data_in_pio_s1_in_a_write_cycle;

  assign wait_for_data_in_pio_s1_counter = 0;

//synthesis translate_off
//////////////// SIMULATION-ONLY CONTENTS
  //data_in_pio/s1 enable non-zero assertions, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          enable_nonzero_assertions <= 0;
      else 
        enable_nonzero_assertions <= 1'b1;
    end



//////////////// END SIMULATION-ONLY CONTENTS

//synthesis translate_on

endmodule


// synthesis translate_off
`timescale 1ns / 1ps
// synthesis translate_on

// turn off superfluous verilog processor warnings 
// altera message_level Level1 
// altera message_off 10034 10035 10036 10037 10230 10240 10030 

module data_out_pio_s1_arbitrator (
                                    // inputs:
                                     clk,
                                     cpu_data_master_address_to_slave,
                                     cpu_data_master_latency_counter,
                                     cpu_data_master_read,
                                     cpu_data_master_write,
                                     cpu_data_master_writedata,
                                     data_out_pio_s1_readdata,
                                     reset_n,

                                    // outputs:
                                     cpu_data_master_granted_data_out_pio_s1,
                                     cpu_data_master_qualified_request_data_out_pio_s1,
                                     cpu_data_master_read_data_valid_data_out_pio_s1,
                                     cpu_data_master_requests_data_out_pio_s1,
                                     d1_data_out_pio_s1_end_xfer,
                                     data_out_pio_s1_address,
                                     data_out_pio_s1_chipselect,
                                     data_out_pio_s1_readdata_from_sa,
                                     data_out_pio_s1_reset_n,
                                     data_out_pio_s1_write_n,
                                     data_out_pio_s1_writedata
                                  )
;

  output           cpu_data_master_granted_data_out_pio_s1;
  output           cpu_data_master_qualified_request_data_out_pio_s1;
  output           cpu_data_master_read_data_valid_data_out_pio_s1;
  output           cpu_data_master_requests_data_out_pio_s1;
  output           d1_data_out_pio_s1_end_xfer;
  output  [  2: 0] data_out_pio_s1_address;
  output           data_out_pio_s1_chipselect;
  output  [ 31: 0] data_out_pio_s1_readdata_from_sa;
  output           data_out_pio_s1_reset_n;
  output           data_out_pio_s1_write_n;
  output  [ 31: 0] data_out_pio_s1_writedata;
  input            clk;
  input   [ 24: 0] cpu_data_master_address_to_slave;
  input   [  2: 0] cpu_data_master_latency_counter;
  input            cpu_data_master_read;
  input            cpu_data_master_write;
  input   [ 31: 0] cpu_data_master_writedata;
  input   [ 31: 0] data_out_pio_s1_readdata;
  input            reset_n;

  wire             cpu_data_master_arbiterlock;
  wire             cpu_data_master_arbiterlock2;
  wire             cpu_data_master_continuerequest;
  wire             cpu_data_master_granted_data_out_pio_s1;
  wire             cpu_data_master_qualified_request_data_out_pio_s1;
  wire             cpu_data_master_read_data_valid_data_out_pio_s1;
  wire             cpu_data_master_requests_data_out_pio_s1;
  wire             cpu_data_master_saved_grant_data_out_pio_s1;
  reg              d1_data_out_pio_s1_end_xfer;
  reg              d1_reasons_to_wait;
  wire    [  2: 0] data_out_pio_s1_address;
  wire             data_out_pio_s1_allgrants;
  wire             data_out_pio_s1_allow_new_arb_cycle;
  wire             data_out_pio_s1_any_bursting_master_saved_grant;
  wire             data_out_pio_s1_any_continuerequest;
  wire             data_out_pio_s1_arb_counter_enable;
  reg              data_out_pio_s1_arb_share_counter;
  wire             data_out_pio_s1_arb_share_counter_next_value;
  wire             data_out_pio_s1_arb_share_set_values;
  wire             data_out_pio_s1_beginbursttransfer_internal;
  wire             data_out_pio_s1_begins_xfer;
  wire             data_out_pio_s1_chipselect;
  wire             data_out_pio_s1_end_xfer;
  wire             data_out_pio_s1_firsttransfer;
  wire             data_out_pio_s1_grant_vector;
  wire             data_out_pio_s1_in_a_read_cycle;
  wire             data_out_pio_s1_in_a_write_cycle;
  wire             data_out_pio_s1_master_qreq_vector;
  wire             data_out_pio_s1_non_bursting_master_requests;
  wire    [ 31: 0] data_out_pio_s1_readdata_from_sa;
  reg              data_out_pio_s1_reg_firsttransfer;
  wire             data_out_pio_s1_reset_n;
  reg              data_out_pio_s1_slavearbiterlockenable;
  wire             data_out_pio_s1_slavearbiterlockenable2;
  wire             data_out_pio_s1_unreg_firsttransfer;
  wire             data_out_pio_s1_waits_for_read;
  wire             data_out_pio_s1_waits_for_write;
  wire             data_out_pio_s1_write_n;
  wire    [ 31: 0] data_out_pio_s1_writedata;
  reg              enable_nonzero_assertions;
  wire             end_xfer_arb_share_counter_term_data_out_pio_s1;
  wire             in_a_read_cycle;
  wire             in_a_write_cycle;
  wire    [ 24: 0] shifted_address_to_data_out_pio_s1_from_cpu_data_master;
  wire             wait_for_data_out_pio_s1_counter;
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          d1_reasons_to_wait <= 0;
      else 
        d1_reasons_to_wait <= ~data_out_pio_s1_end_xfer;
    end


  assign data_out_pio_s1_begins_xfer = ~d1_reasons_to_wait & ((cpu_data_master_qualified_request_data_out_pio_s1));
  //assign data_out_pio_s1_readdata_from_sa = data_out_pio_s1_readdata so that symbol knows where to group signals which may go to master only, which is an e_assign
  assign data_out_pio_s1_readdata_from_sa = data_out_pio_s1_readdata;

  assign cpu_data_master_requests_data_out_pio_s1 = ({cpu_data_master_address_to_slave[24 : 5] , 5'b0} == 25'h14090a0) & (cpu_data_master_read | cpu_data_master_write);
  //data_out_pio_s1_arb_share_counter set values, which is an e_mux
  assign data_out_pio_s1_arb_share_set_values = 1;

  //data_out_pio_s1_non_bursting_master_requests mux, which is an e_mux
  assign data_out_pio_s1_non_bursting_master_requests = cpu_data_master_requests_data_out_pio_s1;

  //data_out_pio_s1_any_bursting_master_saved_grant mux, which is an e_mux
  assign data_out_pio_s1_any_bursting_master_saved_grant = 0;

  //data_out_pio_s1_arb_share_counter_next_value assignment, which is an e_assign
  assign data_out_pio_s1_arb_share_counter_next_value = data_out_pio_s1_firsttransfer ? (data_out_pio_s1_arb_share_set_values - 1) : |data_out_pio_s1_arb_share_counter ? (data_out_pio_s1_arb_share_counter - 1) : 0;

  //data_out_pio_s1_allgrants all slave grants, which is an e_mux
  assign data_out_pio_s1_allgrants = |data_out_pio_s1_grant_vector;

  //data_out_pio_s1_end_xfer assignment, which is an e_assign
  assign data_out_pio_s1_end_xfer = ~(data_out_pio_s1_waits_for_read | data_out_pio_s1_waits_for_write);

  //end_xfer_arb_share_counter_term_data_out_pio_s1 arb share counter enable term, which is an e_assign
  assign end_xfer_arb_share_counter_term_data_out_pio_s1 = data_out_pio_s1_end_xfer & (~data_out_pio_s1_any_bursting_master_saved_grant | in_a_read_cycle | in_a_write_cycle);

  //data_out_pio_s1_arb_share_counter arbitration counter enable, which is an e_assign
  assign data_out_pio_s1_arb_counter_enable = (end_xfer_arb_share_counter_term_data_out_pio_s1 & data_out_pio_s1_allgrants) | (end_xfer_arb_share_counter_term_data_out_pio_s1 & ~data_out_pio_s1_non_bursting_master_requests);

  //data_out_pio_s1_arb_share_counter counter, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          data_out_pio_s1_arb_share_counter <= 0;
      else if (data_out_pio_s1_arb_counter_enable)
          data_out_pio_s1_arb_share_counter <= data_out_pio_s1_arb_share_counter_next_value;
    end


  //data_out_pio_s1_slavearbiterlockenable slave enables arbiterlock, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          data_out_pio_s1_slavearbiterlockenable <= 0;
      else if ((|data_out_pio_s1_master_qreq_vector & end_xfer_arb_share_counter_term_data_out_pio_s1) | (end_xfer_arb_share_counter_term_data_out_pio_s1 & ~data_out_pio_s1_non_bursting_master_requests))
          data_out_pio_s1_slavearbiterlockenable <= |data_out_pio_s1_arb_share_counter_next_value;
    end


  //cpu/data_master data_out_pio/s1 arbiterlock, which is an e_assign
  assign cpu_data_master_arbiterlock = data_out_pio_s1_slavearbiterlockenable & cpu_data_master_continuerequest;

  //data_out_pio_s1_slavearbiterlockenable2 slave enables arbiterlock2, which is an e_assign
  assign data_out_pio_s1_slavearbiterlockenable2 = |data_out_pio_s1_arb_share_counter_next_value;

  //cpu/data_master data_out_pio/s1 arbiterlock2, which is an e_assign
  assign cpu_data_master_arbiterlock2 = data_out_pio_s1_slavearbiterlockenable2 & cpu_data_master_continuerequest;

  //data_out_pio_s1_any_continuerequest at least one master continues requesting, which is an e_assign
  assign data_out_pio_s1_any_continuerequest = 1;

  //cpu_data_master_continuerequest continued request, which is an e_assign
  assign cpu_data_master_continuerequest = 1;

  assign cpu_data_master_qualified_request_data_out_pio_s1 = cpu_data_master_requests_data_out_pio_s1 & ~((cpu_data_master_read & ((cpu_data_master_latency_counter != 0))));
  //local readdatavalid cpu_data_master_read_data_valid_data_out_pio_s1, which is an e_mux
  assign cpu_data_master_read_data_valid_data_out_pio_s1 = cpu_data_master_granted_data_out_pio_s1 & cpu_data_master_read & ~data_out_pio_s1_waits_for_read;

  //data_out_pio_s1_writedata mux, which is an e_mux
  assign data_out_pio_s1_writedata = cpu_data_master_writedata;

  //master is always granted when requested
  assign cpu_data_master_granted_data_out_pio_s1 = cpu_data_master_qualified_request_data_out_pio_s1;

  //cpu/data_master saved-grant data_out_pio/s1, which is an e_assign
  assign cpu_data_master_saved_grant_data_out_pio_s1 = cpu_data_master_requests_data_out_pio_s1;

  //allow new arb cycle for data_out_pio/s1, which is an e_assign
  assign data_out_pio_s1_allow_new_arb_cycle = 1;

  //placeholder chosen master
  assign data_out_pio_s1_grant_vector = 1;

  //placeholder vector of master qualified-requests
  assign data_out_pio_s1_master_qreq_vector = 1;

  //data_out_pio_s1_reset_n assignment, which is an e_assign
  assign data_out_pio_s1_reset_n = reset_n;

  assign data_out_pio_s1_chipselect = cpu_data_master_granted_data_out_pio_s1;
  //data_out_pio_s1_firsttransfer first transaction, which is an e_assign
  assign data_out_pio_s1_firsttransfer = data_out_pio_s1_begins_xfer ? data_out_pio_s1_unreg_firsttransfer : data_out_pio_s1_reg_firsttransfer;

  //data_out_pio_s1_unreg_firsttransfer first transaction, which is an e_assign
  assign data_out_pio_s1_unreg_firsttransfer = ~(data_out_pio_s1_slavearbiterlockenable & data_out_pio_s1_any_continuerequest);

  //data_out_pio_s1_reg_firsttransfer first transaction, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          data_out_pio_s1_reg_firsttransfer <= 1'b1;
      else if (data_out_pio_s1_begins_xfer)
          data_out_pio_s1_reg_firsttransfer <= data_out_pio_s1_unreg_firsttransfer;
    end


  //data_out_pio_s1_beginbursttransfer_internal begin burst transfer, which is an e_assign
  assign data_out_pio_s1_beginbursttransfer_internal = data_out_pio_s1_begins_xfer;

  //~data_out_pio_s1_write_n assignment, which is an e_mux
  assign data_out_pio_s1_write_n = ~(cpu_data_master_granted_data_out_pio_s1 & cpu_data_master_write);

  assign shifted_address_to_data_out_pio_s1_from_cpu_data_master = cpu_data_master_address_to_slave;
  //data_out_pio_s1_address mux, which is an e_mux
  assign data_out_pio_s1_address = shifted_address_to_data_out_pio_s1_from_cpu_data_master >> 2;

  //d1_data_out_pio_s1_end_xfer register, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          d1_data_out_pio_s1_end_xfer <= 1;
      else 
        d1_data_out_pio_s1_end_xfer <= data_out_pio_s1_end_xfer;
    end


  //data_out_pio_s1_waits_for_read in a cycle, which is an e_mux
  assign data_out_pio_s1_waits_for_read = data_out_pio_s1_in_a_read_cycle & data_out_pio_s1_begins_xfer;

  //data_out_pio_s1_in_a_read_cycle assignment, which is an e_assign
  assign data_out_pio_s1_in_a_read_cycle = cpu_data_master_granted_data_out_pio_s1 & cpu_data_master_read;

  //in_a_read_cycle assignment, which is an e_mux
  assign in_a_read_cycle = data_out_pio_s1_in_a_read_cycle;

  //data_out_pio_s1_waits_for_write in a cycle, which is an e_mux
  assign data_out_pio_s1_waits_for_write = data_out_pio_s1_in_a_write_cycle & 0;

  //data_out_pio_s1_in_a_write_cycle assignment, which is an e_assign
  assign data_out_pio_s1_in_a_write_cycle = cpu_data_master_granted_data_out_pio_s1 & cpu_data_master_write;

  //in_a_write_cycle assignment, which is an e_mux
  assign in_a_write_cycle = data_out_pio_s1_in_a_write_cycle;

  assign wait_for_data_out_pio_s1_counter = 0;

//synthesis translate_off
//////////////// SIMULATION-ONLY CONTENTS
  //data_out_pio/s1 enable non-zero assertions, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          enable_nonzero_assertions <= 0;
      else 
        enable_nonzero_assertions <= 1'b1;
    end



//////////////// END SIMULATION-ONLY CONTENTS

//synthesis translate_on

endmodule


// synthesis translate_off
`timescale 1ns / 1ps
// synthesis translate_on

// turn off superfluous verilog processor warnings 
// altera message_level Level1 
// altera message_off 10034 10035 10036 10037 10230 10240 10030 

module epcs_flash_controller_0_epcs_control_port_arbitrator (
                                                              // inputs:
                                                               clk,
                                                               cpu_data_master_address_to_slave,
                                                               cpu_data_master_latency_counter,
                                                               cpu_data_master_read,
                                                               cpu_data_master_write,
                                                               cpu_data_master_writedata,
                                                               cpu_instruction_master_address_to_slave,
                                                               cpu_instruction_master_latency_counter,
                                                               cpu_instruction_master_read,
                                                               epcs_flash_controller_0_epcs_control_port_dataavailable,
                                                               epcs_flash_controller_0_epcs_control_port_endofpacket,
                                                               epcs_flash_controller_0_epcs_control_port_irq,
                                                               epcs_flash_controller_0_epcs_control_port_readdata,
                                                               epcs_flash_controller_0_epcs_control_port_readyfordata,
                                                               reset_n,

                                                              // outputs:
                                                               cpu_data_master_granted_epcs_flash_controller_0_epcs_control_port,
                                                               cpu_data_master_qualified_request_epcs_flash_controller_0_epcs_control_port,
                                                               cpu_data_master_read_data_valid_epcs_flash_controller_0_epcs_control_port,
                                                               cpu_data_master_requests_epcs_flash_controller_0_epcs_control_port,
                                                               cpu_instruction_master_granted_epcs_flash_controller_0_epcs_control_port,
                                                               cpu_instruction_master_qualified_request_epcs_flash_controller_0_epcs_control_port,
                                                               cpu_instruction_master_read_data_valid_epcs_flash_controller_0_epcs_control_port,
                                                               cpu_instruction_master_requests_epcs_flash_controller_0_epcs_control_port,
                                                               d1_epcs_flash_controller_0_epcs_control_port_end_xfer,
                                                               epcs_flash_controller_0_epcs_control_port_address,
                                                               epcs_flash_controller_0_epcs_control_port_chipselect,
                                                               epcs_flash_controller_0_epcs_control_port_dataavailable_from_sa,
                                                               epcs_flash_controller_0_epcs_control_port_endofpacket_from_sa,
                                                               epcs_flash_controller_0_epcs_control_port_irq_from_sa,
                                                               epcs_flash_controller_0_epcs_control_port_read_n,
                                                               epcs_flash_controller_0_epcs_control_port_readdata_from_sa,
                                                               epcs_flash_controller_0_epcs_control_port_readyfordata_from_sa,
                                                               epcs_flash_controller_0_epcs_control_port_reset_n,
                                                               epcs_flash_controller_0_epcs_control_port_write_n,
                                                               epcs_flash_controller_0_epcs_control_port_writedata
                                                            )
;

  output           cpu_data_master_granted_epcs_flash_controller_0_epcs_control_port;
  output           cpu_data_master_qualified_request_epcs_flash_controller_0_epcs_control_port;
  output           cpu_data_master_read_data_valid_epcs_flash_controller_0_epcs_control_port;
  output           cpu_data_master_requests_epcs_flash_controller_0_epcs_control_port;
  output           cpu_instruction_master_granted_epcs_flash_controller_0_epcs_control_port;
  output           cpu_instruction_master_qualified_request_epcs_flash_controller_0_epcs_control_port;
  output           cpu_instruction_master_read_data_valid_epcs_flash_controller_0_epcs_control_port;
  output           cpu_instruction_master_requests_epcs_flash_controller_0_epcs_control_port;
  output           d1_epcs_flash_controller_0_epcs_control_port_end_xfer;
  output  [  8: 0] epcs_flash_controller_0_epcs_control_port_address;
  output           epcs_flash_controller_0_epcs_control_port_chipselect;
  output           epcs_flash_controller_0_epcs_control_port_dataavailable_from_sa;
  output           epcs_flash_controller_0_epcs_control_port_endofpacket_from_sa;
  output           epcs_flash_controller_0_epcs_control_port_irq_from_sa;
  output           epcs_flash_controller_0_epcs_control_port_read_n;
  output  [ 31: 0] epcs_flash_controller_0_epcs_control_port_readdata_from_sa;
  output           epcs_flash_controller_0_epcs_control_port_readyfordata_from_sa;
  output           epcs_flash_controller_0_epcs_control_port_reset_n;
  output           epcs_flash_controller_0_epcs_control_port_write_n;
  output  [ 31: 0] epcs_flash_controller_0_epcs_control_port_writedata;
  input            clk;
  input   [ 24: 0] cpu_data_master_address_to_slave;
  input   [  2: 0] cpu_data_master_latency_counter;
  input            cpu_data_master_read;
  input            cpu_data_master_write;
  input   [ 31: 0] cpu_data_master_writedata;
  input   [ 24: 0] cpu_instruction_master_address_to_slave;
  input   [  2: 0] cpu_instruction_master_latency_counter;
  input            cpu_instruction_master_read;
  input            epcs_flash_controller_0_epcs_control_port_dataavailable;
  input            epcs_flash_controller_0_epcs_control_port_endofpacket;
  input            epcs_flash_controller_0_epcs_control_port_irq;
  input   [ 31: 0] epcs_flash_controller_0_epcs_control_port_readdata;
  input            epcs_flash_controller_0_epcs_control_port_readyfordata;
  input            reset_n;

  wire             cpu_data_master_arbiterlock;
  wire             cpu_data_master_arbiterlock2;
  wire             cpu_data_master_continuerequest;
  wire             cpu_data_master_granted_epcs_flash_controller_0_epcs_control_port;
  wire             cpu_data_master_qualified_request_epcs_flash_controller_0_epcs_control_port;
  wire             cpu_data_master_read_data_valid_epcs_flash_controller_0_epcs_control_port;
  wire             cpu_data_master_requests_epcs_flash_controller_0_epcs_control_port;
  wire             cpu_data_master_saved_grant_epcs_flash_controller_0_epcs_control_port;
  wire             cpu_instruction_master_arbiterlock;
  wire             cpu_instruction_master_arbiterlock2;
  wire             cpu_instruction_master_continuerequest;
  wire             cpu_instruction_master_granted_epcs_flash_controller_0_epcs_control_port;
  wire             cpu_instruction_master_qualified_request_epcs_flash_controller_0_epcs_control_port;
  wire             cpu_instruction_master_read_data_valid_epcs_flash_controller_0_epcs_control_port;
  wire             cpu_instruction_master_requests_epcs_flash_controller_0_epcs_control_port;
  wire             cpu_instruction_master_saved_grant_epcs_flash_controller_0_epcs_control_port;
  reg              d1_epcs_flash_controller_0_epcs_control_port_end_xfer;
  reg              d1_reasons_to_wait;
  reg              enable_nonzero_assertions;
  wire             end_xfer_arb_share_counter_term_epcs_flash_controller_0_epcs_control_port;
  wire    [  8: 0] epcs_flash_controller_0_epcs_control_port_address;
  wire             epcs_flash_controller_0_epcs_control_port_allgrants;
  wire             epcs_flash_controller_0_epcs_control_port_allow_new_arb_cycle;
  wire             epcs_flash_controller_0_epcs_control_port_any_bursting_master_saved_grant;
  wire             epcs_flash_controller_0_epcs_control_port_any_continuerequest;
  reg     [  1: 0] epcs_flash_controller_0_epcs_control_port_arb_addend;
  wire             epcs_flash_controller_0_epcs_control_port_arb_counter_enable;
  reg              epcs_flash_controller_0_epcs_control_port_arb_share_counter;
  wire             epcs_flash_controller_0_epcs_control_port_arb_share_counter_next_value;
  wire             epcs_flash_controller_0_epcs_control_port_arb_share_set_values;
  wire    [  1: 0] epcs_flash_controller_0_epcs_control_port_arb_winner;
  wire             epcs_flash_controller_0_epcs_control_port_arbitration_holdoff_internal;
  wire             epcs_flash_controller_0_epcs_control_port_beginbursttransfer_internal;
  wire             epcs_flash_controller_0_epcs_control_port_begins_xfer;
  wire             epcs_flash_controller_0_epcs_control_port_chipselect;
  wire    [  3: 0] epcs_flash_controller_0_epcs_control_port_chosen_master_double_vector;
  wire    [  1: 0] epcs_flash_controller_0_epcs_control_port_chosen_master_rot_left;
  wire             epcs_flash_controller_0_epcs_control_port_dataavailable_from_sa;
  wire             epcs_flash_controller_0_epcs_control_port_end_xfer;
  wire             epcs_flash_controller_0_epcs_control_port_endofpacket_from_sa;
  wire             epcs_flash_controller_0_epcs_control_port_firsttransfer;
  wire    [  1: 0] epcs_flash_controller_0_epcs_control_port_grant_vector;
  wire             epcs_flash_controller_0_epcs_control_port_in_a_read_cycle;
  wire             epcs_flash_controller_0_epcs_control_port_in_a_write_cycle;
  wire             epcs_flash_controller_0_epcs_control_port_irq_from_sa;
  wire    [  1: 0] epcs_flash_controller_0_epcs_control_port_master_qreq_vector;
  wire             epcs_flash_controller_0_epcs_control_port_non_bursting_master_requests;
  wire             epcs_flash_controller_0_epcs_control_port_read_n;
  wire    [ 31: 0] epcs_flash_controller_0_epcs_control_port_readdata_from_sa;
  wire             epcs_flash_controller_0_epcs_control_port_readyfordata_from_sa;
  reg              epcs_flash_controller_0_epcs_control_port_reg_firsttransfer;
  wire             epcs_flash_controller_0_epcs_control_port_reset_n;
  reg     [  1: 0] epcs_flash_controller_0_epcs_control_port_saved_chosen_master_vector;
  reg              epcs_flash_controller_0_epcs_control_port_slavearbiterlockenable;
  wire             epcs_flash_controller_0_epcs_control_port_slavearbiterlockenable2;
  wire             epcs_flash_controller_0_epcs_control_port_unreg_firsttransfer;
  wire             epcs_flash_controller_0_epcs_control_port_waits_for_read;
  wire             epcs_flash_controller_0_epcs_control_port_waits_for_write;
  wire             epcs_flash_controller_0_epcs_control_port_write_n;
  wire    [ 31: 0] epcs_flash_controller_0_epcs_control_port_writedata;
  wire             in_a_read_cycle;
  wire             in_a_write_cycle;
  reg              last_cycle_cpu_data_master_granted_slave_epcs_flash_controller_0_epcs_control_port;
  reg              last_cycle_cpu_instruction_master_granted_slave_epcs_flash_controller_0_epcs_control_port;
  wire    [ 24: 0] shifted_address_to_epcs_flash_controller_0_epcs_control_port_from_cpu_data_master;
  wire    [ 24: 0] shifted_address_to_epcs_flash_controller_0_epcs_control_port_from_cpu_instruction_master;
  wire             wait_for_epcs_flash_controller_0_epcs_control_port_counter;
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          d1_reasons_to_wait <= 0;
      else 
        d1_reasons_to_wait <= ~epcs_flash_controller_0_epcs_control_port_end_xfer;
    end


  assign epcs_flash_controller_0_epcs_control_port_begins_xfer = ~d1_reasons_to_wait & ((cpu_data_master_qualified_request_epcs_flash_controller_0_epcs_control_port | cpu_instruction_master_qualified_request_epcs_flash_controller_0_epcs_control_port));
  //assign epcs_flash_controller_0_epcs_control_port_readdata_from_sa = epcs_flash_controller_0_epcs_control_port_readdata so that symbol knows where to group signals which may go to master only, which is an e_assign
  assign epcs_flash_controller_0_epcs_control_port_readdata_from_sa = epcs_flash_controller_0_epcs_control_port_readdata;

  assign cpu_data_master_requests_epcs_flash_controller_0_epcs_control_port = ({cpu_data_master_address_to_slave[24 : 11] , 11'b0} == 25'h1000800) & (cpu_data_master_read | cpu_data_master_write);
  //assign epcs_flash_controller_0_epcs_control_port_dataavailable_from_sa = epcs_flash_controller_0_epcs_control_port_dataavailable so that symbol knows where to group signals which may go to master only, which is an e_assign
  assign epcs_flash_controller_0_epcs_control_port_dataavailable_from_sa = epcs_flash_controller_0_epcs_control_port_dataavailable;

  //assign epcs_flash_controller_0_epcs_control_port_readyfordata_from_sa = epcs_flash_controller_0_epcs_control_port_readyfordata so that symbol knows where to group signals which may go to master only, which is an e_assign
  assign epcs_flash_controller_0_epcs_control_port_readyfordata_from_sa = epcs_flash_controller_0_epcs_control_port_readyfordata;

  //epcs_flash_controller_0_epcs_control_port_arb_share_counter set values, which is an e_mux
  assign epcs_flash_controller_0_epcs_control_port_arb_share_set_values = 1;

  //epcs_flash_controller_0_epcs_control_port_non_bursting_master_requests mux, which is an e_mux
  assign epcs_flash_controller_0_epcs_control_port_non_bursting_master_requests = cpu_data_master_requests_epcs_flash_controller_0_epcs_control_port |
    cpu_instruction_master_requests_epcs_flash_controller_0_epcs_control_port |
    cpu_data_master_requests_epcs_flash_controller_0_epcs_control_port |
    cpu_instruction_master_requests_epcs_flash_controller_0_epcs_control_port;

  //epcs_flash_controller_0_epcs_control_port_any_bursting_master_saved_grant mux, which is an e_mux
  assign epcs_flash_controller_0_epcs_control_port_any_bursting_master_saved_grant = 0;

  //epcs_flash_controller_0_epcs_control_port_arb_share_counter_next_value assignment, which is an e_assign
  assign epcs_flash_controller_0_epcs_control_port_arb_share_counter_next_value = epcs_flash_controller_0_epcs_control_port_firsttransfer ? (epcs_flash_controller_0_epcs_control_port_arb_share_set_values - 1) : |epcs_flash_controller_0_epcs_control_port_arb_share_counter ? (epcs_flash_controller_0_epcs_control_port_arb_share_counter - 1) : 0;

  //epcs_flash_controller_0_epcs_control_port_allgrants all slave grants, which is an e_mux
  assign epcs_flash_controller_0_epcs_control_port_allgrants = (|epcs_flash_controller_0_epcs_control_port_grant_vector) |
    (|epcs_flash_controller_0_epcs_control_port_grant_vector) |
    (|epcs_flash_controller_0_epcs_control_port_grant_vector) |
    (|epcs_flash_controller_0_epcs_control_port_grant_vector);

  //epcs_flash_controller_0_epcs_control_port_end_xfer assignment, which is an e_assign
  assign epcs_flash_controller_0_epcs_control_port_end_xfer = ~(epcs_flash_controller_0_epcs_control_port_waits_for_read | epcs_flash_controller_0_epcs_control_port_waits_for_write);

  //end_xfer_arb_share_counter_term_epcs_flash_controller_0_epcs_control_port arb share counter enable term, which is an e_assign
  assign end_xfer_arb_share_counter_term_epcs_flash_controller_0_epcs_control_port = epcs_flash_controller_0_epcs_control_port_end_xfer & (~epcs_flash_controller_0_epcs_control_port_any_bursting_master_saved_grant | in_a_read_cycle | in_a_write_cycle);

  //epcs_flash_controller_0_epcs_control_port_arb_share_counter arbitration counter enable, which is an e_assign
  assign epcs_flash_controller_0_epcs_control_port_arb_counter_enable = (end_xfer_arb_share_counter_term_epcs_flash_controller_0_epcs_control_port & epcs_flash_controller_0_epcs_control_port_allgrants) | (end_xfer_arb_share_counter_term_epcs_flash_controller_0_epcs_control_port & ~epcs_flash_controller_0_epcs_control_port_non_bursting_master_requests);

  //epcs_flash_controller_0_epcs_control_port_arb_share_counter counter, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          epcs_flash_controller_0_epcs_control_port_arb_share_counter <= 0;
      else if (epcs_flash_controller_0_epcs_control_port_arb_counter_enable)
          epcs_flash_controller_0_epcs_control_port_arb_share_counter <= epcs_flash_controller_0_epcs_control_port_arb_share_counter_next_value;
    end


  //epcs_flash_controller_0_epcs_control_port_slavearbiterlockenable slave enables arbiterlock, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          epcs_flash_controller_0_epcs_control_port_slavearbiterlockenable <= 0;
      else if ((|epcs_flash_controller_0_epcs_control_port_master_qreq_vector & end_xfer_arb_share_counter_term_epcs_flash_controller_0_epcs_control_port) | (end_xfer_arb_share_counter_term_epcs_flash_controller_0_epcs_control_port & ~epcs_flash_controller_0_epcs_control_port_non_bursting_master_requests))
          epcs_flash_controller_0_epcs_control_port_slavearbiterlockenable <= |epcs_flash_controller_0_epcs_control_port_arb_share_counter_next_value;
    end


  //cpu/data_master epcs_flash_controller_0/epcs_control_port arbiterlock, which is an e_assign
  assign cpu_data_master_arbiterlock = epcs_flash_controller_0_epcs_control_port_slavearbiterlockenable & cpu_data_master_continuerequest;

  //epcs_flash_controller_0_epcs_control_port_slavearbiterlockenable2 slave enables arbiterlock2, which is an e_assign
  assign epcs_flash_controller_0_epcs_control_port_slavearbiterlockenable2 = |epcs_flash_controller_0_epcs_control_port_arb_share_counter_next_value;

  //cpu/data_master epcs_flash_controller_0/epcs_control_port arbiterlock2, which is an e_assign
  assign cpu_data_master_arbiterlock2 = epcs_flash_controller_0_epcs_control_port_slavearbiterlockenable2 & cpu_data_master_continuerequest;

  //cpu/instruction_master epcs_flash_controller_0/epcs_control_port arbiterlock, which is an e_assign
  assign cpu_instruction_master_arbiterlock = epcs_flash_controller_0_epcs_control_port_slavearbiterlockenable & cpu_instruction_master_continuerequest;

  //cpu/instruction_master epcs_flash_controller_0/epcs_control_port arbiterlock2, which is an e_assign
  assign cpu_instruction_master_arbiterlock2 = epcs_flash_controller_0_epcs_control_port_slavearbiterlockenable2 & cpu_instruction_master_continuerequest;

  //cpu/instruction_master granted epcs_flash_controller_0/epcs_control_port last time, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          last_cycle_cpu_instruction_master_granted_slave_epcs_flash_controller_0_epcs_control_port <= 0;
      else 
        last_cycle_cpu_instruction_master_granted_slave_epcs_flash_controller_0_epcs_control_port <= cpu_instruction_master_saved_grant_epcs_flash_controller_0_epcs_control_port ? 1 : (epcs_flash_controller_0_epcs_control_port_arbitration_holdoff_internal | ~cpu_instruction_master_requests_epcs_flash_controller_0_epcs_control_port) ? 0 : last_cycle_cpu_instruction_master_granted_slave_epcs_flash_controller_0_epcs_control_port;
    end


  //cpu_instruction_master_continuerequest continued request, which is an e_mux
  assign cpu_instruction_master_continuerequest = last_cycle_cpu_instruction_master_granted_slave_epcs_flash_controller_0_epcs_control_port & cpu_instruction_master_requests_epcs_flash_controller_0_epcs_control_port;

  //epcs_flash_controller_0_epcs_control_port_any_continuerequest at least one master continues requesting, which is an e_mux
  assign epcs_flash_controller_0_epcs_control_port_any_continuerequest = cpu_instruction_master_continuerequest |
    cpu_data_master_continuerequest;

  assign cpu_data_master_qualified_request_epcs_flash_controller_0_epcs_control_port = cpu_data_master_requests_epcs_flash_controller_0_epcs_control_port & ~((cpu_data_master_read & ((cpu_data_master_latency_counter != 0))) | cpu_instruction_master_arbiterlock);
  //local readdatavalid cpu_data_master_read_data_valid_epcs_flash_controller_0_epcs_control_port, which is an e_mux
  assign cpu_data_master_read_data_valid_epcs_flash_controller_0_epcs_control_port = cpu_data_master_granted_epcs_flash_controller_0_epcs_control_port & cpu_data_master_read & ~epcs_flash_controller_0_epcs_control_port_waits_for_read;

  //epcs_flash_controller_0_epcs_control_port_writedata mux, which is an e_mux
  assign epcs_flash_controller_0_epcs_control_port_writedata = cpu_data_master_writedata;

  //assign epcs_flash_controller_0_epcs_control_port_endofpacket_from_sa = epcs_flash_controller_0_epcs_control_port_endofpacket so that symbol knows where to group signals which may go to master only, which is an e_assign
  assign epcs_flash_controller_0_epcs_control_port_endofpacket_from_sa = epcs_flash_controller_0_epcs_control_port_endofpacket;

  assign cpu_instruction_master_requests_epcs_flash_controller_0_epcs_control_port = (({cpu_instruction_master_address_to_slave[24 : 11] , 11'b0} == 25'h1000800) & (cpu_instruction_master_read)) & cpu_instruction_master_read;
  //cpu/data_master granted epcs_flash_controller_0/epcs_control_port last time, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          last_cycle_cpu_data_master_granted_slave_epcs_flash_controller_0_epcs_control_port <= 0;
      else 
        last_cycle_cpu_data_master_granted_slave_epcs_flash_controller_0_epcs_control_port <= cpu_data_master_saved_grant_epcs_flash_controller_0_epcs_control_port ? 1 : (epcs_flash_controller_0_epcs_control_port_arbitration_holdoff_internal | ~cpu_data_master_requests_epcs_flash_controller_0_epcs_control_port) ? 0 : last_cycle_cpu_data_master_granted_slave_epcs_flash_controller_0_epcs_control_port;
    end


  //cpu_data_master_continuerequest continued request, which is an e_mux
  assign cpu_data_master_continuerequest = last_cycle_cpu_data_master_granted_slave_epcs_flash_controller_0_epcs_control_port & cpu_data_master_requests_epcs_flash_controller_0_epcs_control_port;

  assign cpu_instruction_master_qualified_request_epcs_flash_controller_0_epcs_control_port = cpu_instruction_master_requests_epcs_flash_controller_0_epcs_control_port & ~((cpu_instruction_master_read & ((cpu_instruction_master_latency_counter != 0))) | cpu_data_master_arbiterlock);
  //local readdatavalid cpu_instruction_master_read_data_valid_epcs_flash_controller_0_epcs_control_port, which is an e_mux
  assign cpu_instruction_master_read_data_valid_epcs_flash_controller_0_epcs_control_port = cpu_instruction_master_granted_epcs_flash_controller_0_epcs_control_port & cpu_instruction_master_read & ~epcs_flash_controller_0_epcs_control_port_waits_for_read;

  //allow new arb cycle for epcs_flash_controller_0/epcs_control_port, which is an e_assign
  assign epcs_flash_controller_0_epcs_control_port_allow_new_arb_cycle = ~cpu_data_master_arbiterlock & ~cpu_instruction_master_arbiterlock;

  //cpu/instruction_master assignment into master qualified-requests vector for epcs_flash_controller_0/epcs_control_port, which is an e_assign
  assign epcs_flash_controller_0_epcs_control_port_master_qreq_vector[0] = cpu_instruction_master_qualified_request_epcs_flash_controller_0_epcs_control_port;

  //cpu/instruction_master grant epcs_flash_controller_0/epcs_control_port, which is an e_assign
  assign cpu_instruction_master_granted_epcs_flash_controller_0_epcs_control_port = epcs_flash_controller_0_epcs_control_port_grant_vector[0];

  //cpu/instruction_master saved-grant epcs_flash_controller_0/epcs_control_port, which is an e_assign
  assign cpu_instruction_master_saved_grant_epcs_flash_controller_0_epcs_control_port = epcs_flash_controller_0_epcs_control_port_arb_winner[0] && cpu_instruction_master_requests_epcs_flash_controller_0_epcs_control_port;

  //cpu/data_master assignment into master qualified-requests vector for epcs_flash_controller_0/epcs_control_port, which is an e_assign
  assign epcs_flash_controller_0_epcs_control_port_master_qreq_vector[1] = cpu_data_master_qualified_request_epcs_flash_controller_0_epcs_control_port;

  //cpu/data_master grant epcs_flash_controller_0/epcs_control_port, which is an e_assign
  assign cpu_data_master_granted_epcs_flash_controller_0_epcs_control_port = epcs_flash_controller_0_epcs_control_port_grant_vector[1];

  //cpu/data_master saved-grant epcs_flash_controller_0/epcs_control_port, which is an e_assign
  assign cpu_data_master_saved_grant_epcs_flash_controller_0_epcs_control_port = epcs_flash_controller_0_epcs_control_port_arb_winner[1] && cpu_data_master_requests_epcs_flash_controller_0_epcs_control_port;

  //epcs_flash_controller_0/epcs_control_port chosen-master double-vector, which is an e_assign
  assign epcs_flash_controller_0_epcs_control_port_chosen_master_double_vector = {epcs_flash_controller_0_epcs_control_port_master_qreq_vector, epcs_flash_controller_0_epcs_control_port_master_qreq_vector} & ({~epcs_flash_controller_0_epcs_control_port_master_qreq_vector, ~epcs_flash_controller_0_epcs_control_port_master_qreq_vector} + epcs_flash_controller_0_epcs_control_port_arb_addend);

  //stable onehot encoding of arb winner
  assign epcs_flash_controller_0_epcs_control_port_arb_winner = (epcs_flash_controller_0_epcs_control_port_allow_new_arb_cycle & | epcs_flash_controller_0_epcs_control_port_grant_vector) ? epcs_flash_controller_0_epcs_control_port_grant_vector : epcs_flash_controller_0_epcs_control_port_saved_chosen_master_vector;

  //saved epcs_flash_controller_0_epcs_control_port_grant_vector, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          epcs_flash_controller_0_epcs_control_port_saved_chosen_master_vector <= 0;
      else if (epcs_flash_controller_0_epcs_control_port_allow_new_arb_cycle)
          epcs_flash_controller_0_epcs_control_port_saved_chosen_master_vector <= |epcs_flash_controller_0_epcs_control_port_grant_vector ? epcs_flash_controller_0_epcs_control_port_grant_vector : epcs_flash_controller_0_epcs_control_port_saved_chosen_master_vector;
    end


  //onehot encoding of chosen master
  assign epcs_flash_controller_0_epcs_control_port_grant_vector = {(epcs_flash_controller_0_epcs_control_port_chosen_master_double_vector[1] | epcs_flash_controller_0_epcs_control_port_chosen_master_double_vector[3]),
    (epcs_flash_controller_0_epcs_control_port_chosen_master_double_vector[0] | epcs_flash_controller_0_epcs_control_port_chosen_master_double_vector[2])};

  //epcs_flash_controller_0/epcs_control_port chosen master rotated left, which is an e_assign
  assign epcs_flash_controller_0_epcs_control_port_chosen_master_rot_left = (epcs_flash_controller_0_epcs_control_port_arb_winner << 1) ? (epcs_flash_controller_0_epcs_control_port_arb_winner << 1) : 1;

  //epcs_flash_controller_0/epcs_control_port's addend for next-master-grant
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          epcs_flash_controller_0_epcs_control_port_arb_addend <= 1;
      else if (|epcs_flash_controller_0_epcs_control_port_grant_vector)
          epcs_flash_controller_0_epcs_control_port_arb_addend <= epcs_flash_controller_0_epcs_control_port_end_xfer? epcs_flash_controller_0_epcs_control_port_chosen_master_rot_left : epcs_flash_controller_0_epcs_control_port_grant_vector;
    end


  //epcs_flash_controller_0_epcs_control_port_reset_n assignment, which is an e_assign
  assign epcs_flash_controller_0_epcs_control_port_reset_n = reset_n;

  assign epcs_flash_controller_0_epcs_control_port_chipselect = cpu_data_master_granted_epcs_flash_controller_0_epcs_control_port | cpu_instruction_master_granted_epcs_flash_controller_0_epcs_control_port;
  //epcs_flash_controller_0_epcs_control_port_firsttransfer first transaction, which is an e_assign
  assign epcs_flash_controller_0_epcs_control_port_firsttransfer = epcs_flash_controller_0_epcs_control_port_begins_xfer ? epcs_flash_controller_0_epcs_control_port_unreg_firsttransfer : epcs_flash_controller_0_epcs_control_port_reg_firsttransfer;

  //epcs_flash_controller_0_epcs_control_port_unreg_firsttransfer first transaction, which is an e_assign
  assign epcs_flash_controller_0_epcs_control_port_unreg_firsttransfer = ~(epcs_flash_controller_0_epcs_control_port_slavearbiterlockenable & epcs_flash_controller_0_epcs_control_port_any_continuerequest);

  //epcs_flash_controller_0_epcs_control_port_reg_firsttransfer first transaction, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          epcs_flash_controller_0_epcs_control_port_reg_firsttransfer <= 1'b1;
      else if (epcs_flash_controller_0_epcs_control_port_begins_xfer)
          epcs_flash_controller_0_epcs_control_port_reg_firsttransfer <= epcs_flash_controller_0_epcs_control_port_unreg_firsttransfer;
    end


  //epcs_flash_controller_0_epcs_control_port_beginbursttransfer_internal begin burst transfer, which is an e_assign
  assign epcs_flash_controller_0_epcs_control_port_beginbursttransfer_internal = epcs_flash_controller_0_epcs_control_port_begins_xfer;

  //epcs_flash_controller_0_epcs_control_port_arbitration_holdoff_internal arbitration_holdoff, which is an e_assign
  assign epcs_flash_controller_0_epcs_control_port_arbitration_holdoff_internal = epcs_flash_controller_0_epcs_control_port_begins_xfer & epcs_flash_controller_0_epcs_control_port_firsttransfer;

  //~epcs_flash_controller_0_epcs_control_port_read_n assignment, which is an e_mux
  assign epcs_flash_controller_0_epcs_control_port_read_n = ~((cpu_data_master_granted_epcs_flash_controller_0_epcs_control_port & cpu_data_master_read) | (cpu_instruction_master_granted_epcs_flash_controller_0_epcs_control_port & cpu_instruction_master_read));

  //~epcs_flash_controller_0_epcs_control_port_write_n assignment, which is an e_mux
  assign epcs_flash_controller_0_epcs_control_port_write_n = ~(cpu_data_master_granted_epcs_flash_controller_0_epcs_control_port & cpu_data_master_write);

  assign shifted_address_to_epcs_flash_controller_0_epcs_control_port_from_cpu_data_master = cpu_data_master_address_to_slave;
  //epcs_flash_controller_0_epcs_control_port_address mux, which is an e_mux
  assign epcs_flash_controller_0_epcs_control_port_address = (cpu_data_master_granted_epcs_flash_controller_0_epcs_control_port)? (shifted_address_to_epcs_flash_controller_0_epcs_control_port_from_cpu_data_master >> 2) :
    (shifted_address_to_epcs_flash_controller_0_epcs_control_port_from_cpu_instruction_master >> 2);

  assign shifted_address_to_epcs_flash_controller_0_epcs_control_port_from_cpu_instruction_master = cpu_instruction_master_address_to_slave;
  //d1_epcs_flash_controller_0_epcs_control_port_end_xfer register, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          d1_epcs_flash_controller_0_epcs_control_port_end_xfer <= 1;
      else 
        d1_epcs_flash_controller_0_epcs_control_port_end_xfer <= epcs_flash_controller_0_epcs_control_port_end_xfer;
    end


  //epcs_flash_controller_0_epcs_control_port_waits_for_read in a cycle, which is an e_mux
  assign epcs_flash_controller_0_epcs_control_port_waits_for_read = epcs_flash_controller_0_epcs_control_port_in_a_read_cycle & epcs_flash_controller_0_epcs_control_port_begins_xfer;

  //epcs_flash_controller_0_epcs_control_port_in_a_read_cycle assignment, which is an e_assign
  assign epcs_flash_controller_0_epcs_control_port_in_a_read_cycle = (cpu_data_master_granted_epcs_flash_controller_0_epcs_control_port & cpu_data_master_read) | (cpu_instruction_master_granted_epcs_flash_controller_0_epcs_control_port & cpu_instruction_master_read);

  //in_a_read_cycle assignment, which is an e_mux
  assign in_a_read_cycle = epcs_flash_controller_0_epcs_control_port_in_a_read_cycle;

  //epcs_flash_controller_0_epcs_control_port_waits_for_write in a cycle, which is an e_mux
  assign epcs_flash_controller_0_epcs_control_port_waits_for_write = epcs_flash_controller_0_epcs_control_port_in_a_write_cycle & epcs_flash_controller_0_epcs_control_port_begins_xfer;

  //epcs_flash_controller_0_epcs_control_port_in_a_write_cycle assignment, which is an e_assign
  assign epcs_flash_controller_0_epcs_control_port_in_a_write_cycle = cpu_data_master_granted_epcs_flash_controller_0_epcs_control_port & cpu_data_master_write;

  //in_a_write_cycle assignment, which is an e_mux
  assign in_a_write_cycle = epcs_flash_controller_0_epcs_control_port_in_a_write_cycle;

  assign wait_for_epcs_flash_controller_0_epcs_control_port_counter = 0;
  //assign epcs_flash_controller_0_epcs_control_port_irq_from_sa = epcs_flash_controller_0_epcs_control_port_irq so that symbol knows where to group signals which may go to master only, which is an e_assign
  assign epcs_flash_controller_0_epcs_control_port_irq_from_sa = epcs_flash_controller_0_epcs_control_port_irq;


//synthesis translate_off
//////////////// SIMULATION-ONLY CONTENTS
  //epcs_flash_controller_0/epcs_control_port enable non-zero assertions, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          enable_nonzero_assertions <= 0;
      else 
        enable_nonzero_assertions <= 1'b1;
    end


  //grant signals are active simultaneously, which is an e_process
  always @(posedge clk)
    begin
      if (cpu_data_master_granted_epcs_flash_controller_0_epcs_control_port + cpu_instruction_master_granted_epcs_flash_controller_0_epcs_control_port > 1)
        begin
          $write("%0d ns: > 1 of grant signals are active simultaneously", $time);
          $stop;
        end
    end


  //saved_grant signals are active simultaneously, which is an e_process
  always @(posedge clk)
    begin
      if (cpu_data_master_saved_grant_epcs_flash_controller_0_epcs_control_port + cpu_instruction_master_saved_grant_epcs_flash_controller_0_epcs_control_port > 1)
        begin
          $write("%0d ns: > 1 of saved_grant signals are active simultaneously", $time);
          $stop;
        end
    end



//////////////// END SIMULATION-ONLY CONTENTS

//synthesis translate_on

endmodule


// synthesis translate_off
`timescale 1ns / 1ps
// synthesis translate_on

// turn off superfluous verilog processor warnings 
// altera message_level Level1 
// altera message_off 10034 10035 10036 10037 10230 10240 10030 

module jtag_uart_avalon_jtag_slave_arbitrator (
                                                // inputs:
                                                 clk,
                                                 cpu_data_master_address_to_slave,
                                                 cpu_data_master_latency_counter,
                                                 cpu_data_master_read,
                                                 cpu_data_master_write,
                                                 cpu_data_master_writedata,
                                                 jtag_uart_avalon_jtag_slave_dataavailable,
                                                 jtag_uart_avalon_jtag_slave_irq,
                                                 jtag_uart_avalon_jtag_slave_readdata,
                                                 jtag_uart_avalon_jtag_slave_readyfordata,
                                                 jtag_uart_avalon_jtag_slave_waitrequest,
                                                 reset_n,

                                                // outputs:
                                                 cpu_data_master_granted_jtag_uart_avalon_jtag_slave,
                                                 cpu_data_master_qualified_request_jtag_uart_avalon_jtag_slave,
                                                 cpu_data_master_read_data_valid_jtag_uart_avalon_jtag_slave,
                                                 cpu_data_master_requests_jtag_uart_avalon_jtag_slave,
                                                 d1_jtag_uart_avalon_jtag_slave_end_xfer,
                                                 jtag_uart_avalon_jtag_slave_address,
                                                 jtag_uart_avalon_jtag_slave_chipselect,
                                                 jtag_uart_avalon_jtag_slave_dataavailable_from_sa,
                                                 jtag_uart_avalon_jtag_slave_irq_from_sa,
                                                 jtag_uart_avalon_jtag_slave_read_n,
                                                 jtag_uart_avalon_jtag_slave_readdata_from_sa,
                                                 jtag_uart_avalon_jtag_slave_readyfordata_from_sa,
                                                 jtag_uart_avalon_jtag_slave_reset_n,
                                                 jtag_uart_avalon_jtag_slave_waitrequest_from_sa,
                                                 jtag_uart_avalon_jtag_slave_write_n,
                                                 jtag_uart_avalon_jtag_slave_writedata
                                              )
;

  output           cpu_data_master_granted_jtag_uart_avalon_jtag_slave;
  output           cpu_data_master_qualified_request_jtag_uart_avalon_jtag_slave;
  output           cpu_data_master_read_data_valid_jtag_uart_avalon_jtag_slave;
  output           cpu_data_master_requests_jtag_uart_avalon_jtag_slave;
  output           d1_jtag_uart_avalon_jtag_slave_end_xfer;
  output           jtag_uart_avalon_jtag_slave_address;
  output           jtag_uart_avalon_jtag_slave_chipselect;
  output           jtag_uart_avalon_jtag_slave_dataavailable_from_sa;
  output           jtag_uart_avalon_jtag_slave_irq_from_sa;
  output           jtag_uart_avalon_jtag_slave_read_n;
  output  [ 31: 0] jtag_uart_avalon_jtag_slave_readdata_from_sa;
  output           jtag_uart_avalon_jtag_slave_readyfordata_from_sa;
  output           jtag_uart_avalon_jtag_slave_reset_n;
  output           jtag_uart_avalon_jtag_slave_waitrequest_from_sa;
  output           jtag_uart_avalon_jtag_slave_write_n;
  output  [ 31: 0] jtag_uart_avalon_jtag_slave_writedata;
  input            clk;
  input   [ 24: 0] cpu_data_master_address_to_slave;
  input   [  2: 0] cpu_data_master_latency_counter;
  input            cpu_data_master_read;
  input            cpu_data_master_write;
  input   [ 31: 0] cpu_data_master_writedata;
  input            jtag_uart_avalon_jtag_slave_dataavailable;
  input            jtag_uart_avalon_jtag_slave_irq;
  input   [ 31: 0] jtag_uart_avalon_jtag_slave_readdata;
  input            jtag_uart_avalon_jtag_slave_readyfordata;
  input            jtag_uart_avalon_jtag_slave_waitrequest;
  input            reset_n;

  wire             cpu_data_master_arbiterlock;
  wire             cpu_data_master_arbiterlock2;
  wire             cpu_data_master_continuerequest;
  wire             cpu_data_master_granted_jtag_uart_avalon_jtag_slave;
  wire             cpu_data_master_qualified_request_jtag_uart_avalon_jtag_slave;
  wire             cpu_data_master_read_data_valid_jtag_uart_avalon_jtag_slave;
  wire             cpu_data_master_requests_jtag_uart_avalon_jtag_slave;
  wire             cpu_data_master_saved_grant_jtag_uart_avalon_jtag_slave;
  reg              d1_jtag_uart_avalon_jtag_slave_end_xfer;
  reg              d1_reasons_to_wait;
  reg              enable_nonzero_assertions;
  wire             end_xfer_arb_share_counter_term_jtag_uart_avalon_jtag_slave;
  wire             in_a_read_cycle;
  wire             in_a_write_cycle;
  wire             jtag_uart_avalon_jtag_slave_address;
  wire             jtag_uart_avalon_jtag_slave_allgrants;
  wire             jtag_uart_avalon_jtag_slave_allow_new_arb_cycle;
  wire             jtag_uart_avalon_jtag_slave_any_bursting_master_saved_grant;
  wire             jtag_uart_avalon_jtag_slave_any_continuerequest;
  wire             jtag_uart_avalon_jtag_slave_arb_counter_enable;
  reg              jtag_uart_avalon_jtag_slave_arb_share_counter;
  wire             jtag_uart_avalon_jtag_slave_arb_share_counter_next_value;
  wire             jtag_uart_avalon_jtag_slave_arb_share_set_values;
  wire             jtag_uart_avalon_jtag_slave_beginbursttransfer_internal;
  wire             jtag_uart_avalon_jtag_slave_begins_xfer;
  wire             jtag_uart_avalon_jtag_slave_chipselect;
  wire             jtag_uart_avalon_jtag_slave_dataavailable_from_sa;
  wire             jtag_uart_avalon_jtag_slave_end_xfer;
  wire             jtag_uart_avalon_jtag_slave_firsttransfer;
  wire             jtag_uart_avalon_jtag_slave_grant_vector;
  wire             jtag_uart_avalon_jtag_slave_in_a_read_cycle;
  wire             jtag_uart_avalon_jtag_slave_in_a_write_cycle;
  wire             jtag_uart_avalon_jtag_slave_irq_from_sa;
  wire             jtag_uart_avalon_jtag_slave_master_qreq_vector;
  wire             jtag_uart_avalon_jtag_slave_non_bursting_master_requests;
  wire             jtag_uart_avalon_jtag_slave_read_n;
  wire    [ 31: 0] jtag_uart_avalon_jtag_slave_readdata_from_sa;
  wire             jtag_uart_avalon_jtag_slave_readyfordata_from_sa;
  reg              jtag_uart_avalon_jtag_slave_reg_firsttransfer;
  wire             jtag_uart_avalon_jtag_slave_reset_n;
  reg              jtag_uart_avalon_jtag_slave_slavearbiterlockenable;
  wire             jtag_uart_avalon_jtag_slave_slavearbiterlockenable2;
  wire             jtag_uart_avalon_jtag_slave_unreg_firsttransfer;
  wire             jtag_uart_avalon_jtag_slave_waitrequest_from_sa;
  wire             jtag_uart_avalon_jtag_slave_waits_for_read;
  wire             jtag_uart_avalon_jtag_slave_waits_for_write;
  wire             jtag_uart_avalon_jtag_slave_write_n;
  wire    [ 31: 0] jtag_uart_avalon_jtag_slave_writedata;
  wire    [ 24: 0] shifted_address_to_jtag_uart_avalon_jtag_slave_from_cpu_data_master;
  wire             wait_for_jtag_uart_avalon_jtag_slave_counter;
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          d1_reasons_to_wait <= 0;
      else 
        d1_reasons_to_wait <= ~jtag_uart_avalon_jtag_slave_end_xfer;
    end


  assign jtag_uart_avalon_jtag_slave_begins_xfer = ~d1_reasons_to_wait & ((cpu_data_master_qualified_request_jtag_uart_avalon_jtag_slave));
  //assign jtag_uart_avalon_jtag_slave_readdata_from_sa = jtag_uart_avalon_jtag_slave_readdata so that symbol knows where to group signals which may go to master only, which is an e_assign
  assign jtag_uart_avalon_jtag_slave_readdata_from_sa = jtag_uart_avalon_jtag_slave_readdata;

  assign cpu_data_master_requests_jtag_uart_avalon_jtag_slave = ({cpu_data_master_address_to_slave[24 : 3] , 3'b0} == 25'h1409118) & (cpu_data_master_read | cpu_data_master_write);
  //assign jtag_uart_avalon_jtag_slave_dataavailable_from_sa = jtag_uart_avalon_jtag_slave_dataavailable so that symbol knows where to group signals which may go to master only, which is an e_assign
  assign jtag_uart_avalon_jtag_slave_dataavailable_from_sa = jtag_uart_avalon_jtag_slave_dataavailable;

  //assign jtag_uart_avalon_jtag_slave_readyfordata_from_sa = jtag_uart_avalon_jtag_slave_readyfordata so that symbol knows where to group signals which may go to master only, which is an e_assign
  assign jtag_uart_avalon_jtag_slave_readyfordata_from_sa = jtag_uart_avalon_jtag_slave_readyfordata;

  //assign jtag_uart_avalon_jtag_slave_waitrequest_from_sa = jtag_uart_avalon_jtag_slave_waitrequest so that symbol knows where to group signals which may go to master only, which is an e_assign
  assign jtag_uart_avalon_jtag_slave_waitrequest_from_sa = jtag_uart_avalon_jtag_slave_waitrequest;

  //jtag_uart_avalon_jtag_slave_arb_share_counter set values, which is an e_mux
  assign jtag_uart_avalon_jtag_slave_arb_share_set_values = 1;

  //jtag_uart_avalon_jtag_slave_non_bursting_master_requests mux, which is an e_mux
  assign jtag_uart_avalon_jtag_slave_non_bursting_master_requests = cpu_data_master_requests_jtag_uart_avalon_jtag_slave;

  //jtag_uart_avalon_jtag_slave_any_bursting_master_saved_grant mux, which is an e_mux
  assign jtag_uart_avalon_jtag_slave_any_bursting_master_saved_grant = 0;

  //jtag_uart_avalon_jtag_slave_arb_share_counter_next_value assignment, which is an e_assign
  assign jtag_uart_avalon_jtag_slave_arb_share_counter_next_value = jtag_uart_avalon_jtag_slave_firsttransfer ? (jtag_uart_avalon_jtag_slave_arb_share_set_values - 1) : |jtag_uart_avalon_jtag_slave_arb_share_counter ? (jtag_uart_avalon_jtag_slave_arb_share_counter - 1) : 0;

  //jtag_uart_avalon_jtag_slave_allgrants all slave grants, which is an e_mux
  assign jtag_uart_avalon_jtag_slave_allgrants = |jtag_uart_avalon_jtag_slave_grant_vector;

  //jtag_uart_avalon_jtag_slave_end_xfer assignment, which is an e_assign
  assign jtag_uart_avalon_jtag_slave_end_xfer = ~(jtag_uart_avalon_jtag_slave_waits_for_read | jtag_uart_avalon_jtag_slave_waits_for_write);

  //end_xfer_arb_share_counter_term_jtag_uart_avalon_jtag_slave arb share counter enable term, which is an e_assign
  assign end_xfer_arb_share_counter_term_jtag_uart_avalon_jtag_slave = jtag_uart_avalon_jtag_slave_end_xfer & (~jtag_uart_avalon_jtag_slave_any_bursting_master_saved_grant | in_a_read_cycle | in_a_write_cycle);

  //jtag_uart_avalon_jtag_slave_arb_share_counter arbitration counter enable, which is an e_assign
  assign jtag_uart_avalon_jtag_slave_arb_counter_enable = (end_xfer_arb_share_counter_term_jtag_uart_avalon_jtag_slave & jtag_uart_avalon_jtag_slave_allgrants) | (end_xfer_arb_share_counter_term_jtag_uart_avalon_jtag_slave & ~jtag_uart_avalon_jtag_slave_non_bursting_master_requests);

  //jtag_uart_avalon_jtag_slave_arb_share_counter counter, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          jtag_uart_avalon_jtag_slave_arb_share_counter <= 0;
      else if (jtag_uart_avalon_jtag_slave_arb_counter_enable)
          jtag_uart_avalon_jtag_slave_arb_share_counter <= jtag_uart_avalon_jtag_slave_arb_share_counter_next_value;
    end


  //jtag_uart_avalon_jtag_slave_slavearbiterlockenable slave enables arbiterlock, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          jtag_uart_avalon_jtag_slave_slavearbiterlockenable <= 0;
      else if ((|jtag_uart_avalon_jtag_slave_master_qreq_vector & end_xfer_arb_share_counter_term_jtag_uart_avalon_jtag_slave) | (end_xfer_arb_share_counter_term_jtag_uart_avalon_jtag_slave & ~jtag_uart_avalon_jtag_slave_non_bursting_master_requests))
          jtag_uart_avalon_jtag_slave_slavearbiterlockenable <= |jtag_uart_avalon_jtag_slave_arb_share_counter_next_value;
    end


  //cpu/data_master jtag_uart/avalon_jtag_slave arbiterlock, which is an e_assign
  assign cpu_data_master_arbiterlock = jtag_uart_avalon_jtag_slave_slavearbiterlockenable & cpu_data_master_continuerequest;

  //jtag_uart_avalon_jtag_slave_slavearbiterlockenable2 slave enables arbiterlock2, which is an e_assign
  assign jtag_uart_avalon_jtag_slave_slavearbiterlockenable2 = |jtag_uart_avalon_jtag_slave_arb_share_counter_next_value;

  //cpu/data_master jtag_uart/avalon_jtag_slave arbiterlock2, which is an e_assign
  assign cpu_data_master_arbiterlock2 = jtag_uart_avalon_jtag_slave_slavearbiterlockenable2 & cpu_data_master_continuerequest;

  //jtag_uart_avalon_jtag_slave_any_continuerequest at least one master continues requesting, which is an e_assign
  assign jtag_uart_avalon_jtag_slave_any_continuerequest = 1;

  //cpu_data_master_continuerequest continued request, which is an e_assign
  assign cpu_data_master_continuerequest = 1;

  assign cpu_data_master_qualified_request_jtag_uart_avalon_jtag_slave = cpu_data_master_requests_jtag_uart_avalon_jtag_slave & ~((cpu_data_master_read & ((cpu_data_master_latency_counter != 0))));
  //local readdatavalid cpu_data_master_read_data_valid_jtag_uart_avalon_jtag_slave, which is an e_mux
  assign cpu_data_master_read_data_valid_jtag_uart_avalon_jtag_slave = cpu_data_master_granted_jtag_uart_avalon_jtag_slave & cpu_data_master_read & ~jtag_uart_avalon_jtag_slave_waits_for_read;

  //jtag_uart_avalon_jtag_slave_writedata mux, which is an e_mux
  assign jtag_uart_avalon_jtag_slave_writedata = cpu_data_master_writedata;

  //master is always granted when requested
  assign cpu_data_master_granted_jtag_uart_avalon_jtag_slave = cpu_data_master_qualified_request_jtag_uart_avalon_jtag_slave;

  //cpu/data_master saved-grant jtag_uart/avalon_jtag_slave, which is an e_assign
  assign cpu_data_master_saved_grant_jtag_uart_avalon_jtag_slave = cpu_data_master_requests_jtag_uart_avalon_jtag_slave;

  //allow new arb cycle for jtag_uart/avalon_jtag_slave, which is an e_assign
  assign jtag_uart_avalon_jtag_slave_allow_new_arb_cycle = 1;

  //placeholder chosen master
  assign jtag_uart_avalon_jtag_slave_grant_vector = 1;

  //placeholder vector of master qualified-requests
  assign jtag_uart_avalon_jtag_slave_master_qreq_vector = 1;

  //jtag_uart_avalon_jtag_slave_reset_n assignment, which is an e_assign
  assign jtag_uart_avalon_jtag_slave_reset_n = reset_n;

  assign jtag_uart_avalon_jtag_slave_chipselect = cpu_data_master_granted_jtag_uart_avalon_jtag_slave;
  //jtag_uart_avalon_jtag_slave_firsttransfer first transaction, which is an e_assign
  assign jtag_uart_avalon_jtag_slave_firsttransfer = jtag_uart_avalon_jtag_slave_begins_xfer ? jtag_uart_avalon_jtag_slave_unreg_firsttransfer : jtag_uart_avalon_jtag_slave_reg_firsttransfer;

  //jtag_uart_avalon_jtag_slave_unreg_firsttransfer first transaction, which is an e_assign
  assign jtag_uart_avalon_jtag_slave_unreg_firsttransfer = ~(jtag_uart_avalon_jtag_slave_slavearbiterlockenable & jtag_uart_avalon_jtag_slave_any_continuerequest);

  //jtag_uart_avalon_jtag_slave_reg_firsttransfer first transaction, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          jtag_uart_avalon_jtag_slave_reg_firsttransfer <= 1'b1;
      else if (jtag_uart_avalon_jtag_slave_begins_xfer)
          jtag_uart_avalon_jtag_slave_reg_firsttransfer <= jtag_uart_avalon_jtag_slave_unreg_firsttransfer;
    end


  //jtag_uart_avalon_jtag_slave_beginbursttransfer_internal begin burst transfer, which is an e_assign
  assign jtag_uart_avalon_jtag_slave_beginbursttransfer_internal = jtag_uart_avalon_jtag_slave_begins_xfer;

  //~jtag_uart_avalon_jtag_slave_read_n assignment, which is an e_mux
  assign jtag_uart_avalon_jtag_slave_read_n = ~(cpu_data_master_granted_jtag_uart_avalon_jtag_slave & cpu_data_master_read);

  //~jtag_uart_avalon_jtag_slave_write_n assignment, which is an e_mux
  assign jtag_uart_avalon_jtag_slave_write_n = ~(cpu_data_master_granted_jtag_uart_avalon_jtag_slave & cpu_data_master_write);

  assign shifted_address_to_jtag_uart_avalon_jtag_slave_from_cpu_data_master = cpu_data_master_address_to_slave;
  //jtag_uart_avalon_jtag_slave_address mux, which is an e_mux
  assign jtag_uart_avalon_jtag_slave_address = shifted_address_to_jtag_uart_avalon_jtag_slave_from_cpu_data_master >> 2;

  //d1_jtag_uart_avalon_jtag_slave_end_xfer register, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          d1_jtag_uart_avalon_jtag_slave_end_xfer <= 1;
      else 
        d1_jtag_uart_avalon_jtag_slave_end_xfer <= jtag_uart_avalon_jtag_slave_end_xfer;
    end


  //jtag_uart_avalon_jtag_slave_waits_for_read in a cycle, which is an e_mux
  assign jtag_uart_avalon_jtag_slave_waits_for_read = jtag_uart_avalon_jtag_slave_in_a_read_cycle & jtag_uart_avalon_jtag_slave_waitrequest_from_sa;

  //jtag_uart_avalon_jtag_slave_in_a_read_cycle assignment, which is an e_assign
  assign jtag_uart_avalon_jtag_slave_in_a_read_cycle = cpu_data_master_granted_jtag_uart_avalon_jtag_slave & cpu_data_master_read;

  //in_a_read_cycle assignment, which is an e_mux
  assign in_a_read_cycle = jtag_uart_avalon_jtag_slave_in_a_read_cycle;

  //jtag_uart_avalon_jtag_slave_waits_for_write in a cycle, which is an e_mux
  assign jtag_uart_avalon_jtag_slave_waits_for_write = jtag_uart_avalon_jtag_slave_in_a_write_cycle & jtag_uart_avalon_jtag_slave_waitrequest_from_sa;

  //jtag_uart_avalon_jtag_slave_in_a_write_cycle assignment, which is an e_assign
  assign jtag_uart_avalon_jtag_slave_in_a_write_cycle = cpu_data_master_granted_jtag_uart_avalon_jtag_slave & cpu_data_master_write;

  //in_a_write_cycle assignment, which is an e_mux
  assign in_a_write_cycle = jtag_uart_avalon_jtag_slave_in_a_write_cycle;

  assign wait_for_jtag_uart_avalon_jtag_slave_counter = 0;
  //assign jtag_uart_avalon_jtag_slave_irq_from_sa = jtag_uart_avalon_jtag_slave_irq so that symbol knows where to group signals which may go to master only, which is an e_assign
  assign jtag_uart_avalon_jtag_slave_irq_from_sa = jtag_uart_avalon_jtag_slave_irq;


//synthesis translate_off
//////////////// SIMULATION-ONLY CONTENTS
  //jtag_uart/avalon_jtag_slave enable non-zero assertions, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          enable_nonzero_assertions <= 0;
      else 
        enable_nonzero_assertions <= 1'b1;
    end



//////////////// END SIMULATION-ONLY CONTENTS

//synthesis translate_on

endmodule


// synthesis translate_off
`timescale 1ns / 1ps
// synthesis translate_on

// turn off superfluous verilog processor warnings 
// altera message_level Level1 
// altera message_off 10034 10035 10036 10037 10230 10240 10030 

module lcd_control_slave_arbitrator (
                                      // inputs:
                                       clk,
                                       cpu_data_master_address_to_slave,
                                       cpu_data_master_byteenable,
                                       cpu_data_master_latency_counter,
                                       cpu_data_master_read,
                                       cpu_data_master_write,
                                       cpu_data_master_writedata,
                                       lcd_control_slave_readdata,
                                       reset_n,

                                      // outputs:
                                       cpu_data_master_granted_lcd_control_slave,
                                       cpu_data_master_qualified_request_lcd_control_slave,
                                       cpu_data_master_read_data_valid_lcd_control_slave,
                                       cpu_data_master_requests_lcd_control_slave,
                                       d1_lcd_control_slave_end_xfer,
                                       lcd_control_slave_address,
                                       lcd_control_slave_begintransfer,
                                       lcd_control_slave_read,
                                       lcd_control_slave_readdata_from_sa,
                                       lcd_control_slave_reset_n,
                                       lcd_control_slave_wait_counter_eq_0,
                                       lcd_control_slave_write,
                                       lcd_control_slave_writedata
                                    )
;

  output           cpu_data_master_granted_lcd_control_slave;
  output           cpu_data_master_qualified_request_lcd_control_slave;
  output           cpu_data_master_read_data_valid_lcd_control_slave;
  output           cpu_data_master_requests_lcd_control_slave;
  output           d1_lcd_control_slave_end_xfer;
  output  [  1: 0] lcd_control_slave_address;
  output           lcd_control_slave_begintransfer;
  output           lcd_control_slave_read;
  output  [  7: 0] lcd_control_slave_readdata_from_sa;
  output           lcd_control_slave_reset_n;
  output           lcd_control_slave_wait_counter_eq_0;
  output           lcd_control_slave_write;
  output  [  7: 0] lcd_control_slave_writedata;
  input            clk;
  input   [ 24: 0] cpu_data_master_address_to_slave;
  input   [  3: 0] cpu_data_master_byteenable;
  input   [  2: 0] cpu_data_master_latency_counter;
  input            cpu_data_master_read;
  input            cpu_data_master_write;
  input   [ 31: 0] cpu_data_master_writedata;
  input   [  7: 0] lcd_control_slave_readdata;
  input            reset_n;

  wire             cpu_data_master_arbiterlock;
  wire             cpu_data_master_arbiterlock2;
  wire             cpu_data_master_continuerequest;
  wire             cpu_data_master_granted_lcd_control_slave;
  wire             cpu_data_master_qualified_request_lcd_control_slave;
  wire             cpu_data_master_read_data_valid_lcd_control_slave;
  wire             cpu_data_master_requests_lcd_control_slave;
  wire             cpu_data_master_saved_grant_lcd_control_slave;
  reg              d1_lcd_control_slave_end_xfer;
  reg              d1_reasons_to_wait;
  reg              enable_nonzero_assertions;
  wire             end_xfer_arb_share_counter_term_lcd_control_slave;
  wire             in_a_read_cycle;
  wire             in_a_write_cycle;
  wire    [  1: 0] lcd_control_slave_address;
  wire             lcd_control_slave_allgrants;
  wire             lcd_control_slave_allow_new_arb_cycle;
  wire             lcd_control_slave_any_bursting_master_saved_grant;
  wire             lcd_control_slave_any_continuerequest;
  wire             lcd_control_slave_arb_counter_enable;
  reg              lcd_control_slave_arb_share_counter;
  wire             lcd_control_slave_arb_share_counter_next_value;
  wire             lcd_control_slave_arb_share_set_values;
  wire             lcd_control_slave_beginbursttransfer_internal;
  wire             lcd_control_slave_begins_xfer;
  wire             lcd_control_slave_begintransfer;
  wire    [  6: 0] lcd_control_slave_counter_load_value;
  wire             lcd_control_slave_end_xfer;
  wire             lcd_control_slave_firsttransfer;
  wire             lcd_control_slave_grant_vector;
  wire             lcd_control_slave_in_a_read_cycle;
  wire             lcd_control_slave_in_a_write_cycle;
  wire             lcd_control_slave_master_qreq_vector;
  wire             lcd_control_slave_non_bursting_master_requests;
  wire             lcd_control_slave_pretend_byte_enable;
  wire             lcd_control_slave_read;
  wire    [  7: 0] lcd_control_slave_readdata_from_sa;
  reg              lcd_control_slave_reg_firsttransfer;
  wire             lcd_control_slave_reset_n;
  reg              lcd_control_slave_slavearbiterlockenable;
  wire             lcd_control_slave_slavearbiterlockenable2;
  wire             lcd_control_slave_unreg_firsttransfer;
  reg     [  6: 0] lcd_control_slave_wait_counter;
  wire             lcd_control_slave_wait_counter_eq_0;
  wire             lcd_control_slave_waits_for_read;
  wire             lcd_control_slave_waits_for_write;
  wire             lcd_control_slave_write;
  wire    [  7: 0] lcd_control_slave_writedata;
  wire    [ 24: 0] shifted_address_to_lcd_control_slave_from_cpu_data_master;
  wire             wait_for_lcd_control_slave_counter;
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          d1_reasons_to_wait <= 0;
      else 
        d1_reasons_to_wait <= ~lcd_control_slave_end_xfer;
    end


  assign lcd_control_slave_begins_xfer = ~d1_reasons_to_wait & ((cpu_data_master_qualified_request_lcd_control_slave));
  //assign lcd_control_slave_readdata_from_sa = lcd_control_slave_readdata so that symbol knows where to group signals which may go to master only, which is an e_assign
  assign lcd_control_slave_readdata_from_sa = lcd_control_slave_readdata;

  assign cpu_data_master_requests_lcd_control_slave = ({cpu_data_master_address_to_slave[24 : 4] , 4'b0} == 25'h14090e0) & (cpu_data_master_read | cpu_data_master_write);
  //lcd_control_slave_arb_share_counter set values, which is an e_mux
  assign lcd_control_slave_arb_share_set_values = 1;

  //lcd_control_slave_non_bursting_master_requests mux, which is an e_mux
  assign lcd_control_slave_non_bursting_master_requests = cpu_data_master_requests_lcd_control_slave;

  //lcd_control_slave_any_bursting_master_saved_grant mux, which is an e_mux
  assign lcd_control_slave_any_bursting_master_saved_grant = 0;

  //lcd_control_slave_arb_share_counter_next_value assignment, which is an e_assign
  assign lcd_control_slave_arb_share_counter_next_value = lcd_control_slave_firsttransfer ? (lcd_control_slave_arb_share_set_values - 1) : |lcd_control_slave_arb_share_counter ? (lcd_control_slave_arb_share_counter - 1) : 0;

  //lcd_control_slave_allgrants all slave grants, which is an e_mux
  assign lcd_control_slave_allgrants = |lcd_control_slave_grant_vector;

  //lcd_control_slave_end_xfer assignment, which is an e_assign
  assign lcd_control_slave_end_xfer = ~(lcd_control_slave_waits_for_read | lcd_control_slave_waits_for_write);

  //end_xfer_arb_share_counter_term_lcd_control_slave arb share counter enable term, which is an e_assign
  assign end_xfer_arb_share_counter_term_lcd_control_slave = lcd_control_slave_end_xfer & (~lcd_control_slave_any_bursting_master_saved_grant | in_a_read_cycle | in_a_write_cycle);

  //lcd_control_slave_arb_share_counter arbitration counter enable, which is an e_assign
  assign lcd_control_slave_arb_counter_enable = (end_xfer_arb_share_counter_term_lcd_control_slave & lcd_control_slave_allgrants) | (end_xfer_arb_share_counter_term_lcd_control_slave & ~lcd_control_slave_non_bursting_master_requests);

  //lcd_control_slave_arb_share_counter counter, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          lcd_control_slave_arb_share_counter <= 0;
      else if (lcd_control_slave_arb_counter_enable)
          lcd_control_slave_arb_share_counter <= lcd_control_slave_arb_share_counter_next_value;
    end


  //lcd_control_slave_slavearbiterlockenable slave enables arbiterlock, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          lcd_control_slave_slavearbiterlockenable <= 0;
      else if ((|lcd_control_slave_master_qreq_vector & end_xfer_arb_share_counter_term_lcd_control_slave) | (end_xfer_arb_share_counter_term_lcd_control_slave & ~lcd_control_slave_non_bursting_master_requests))
          lcd_control_slave_slavearbiterlockenable <= |lcd_control_slave_arb_share_counter_next_value;
    end


  //cpu/data_master lcd/control_slave arbiterlock, which is an e_assign
  assign cpu_data_master_arbiterlock = lcd_control_slave_slavearbiterlockenable & cpu_data_master_continuerequest;

  //lcd_control_slave_slavearbiterlockenable2 slave enables arbiterlock2, which is an e_assign
  assign lcd_control_slave_slavearbiterlockenable2 = |lcd_control_slave_arb_share_counter_next_value;

  //cpu/data_master lcd/control_slave arbiterlock2, which is an e_assign
  assign cpu_data_master_arbiterlock2 = lcd_control_slave_slavearbiterlockenable2 & cpu_data_master_continuerequest;

  //lcd_control_slave_any_continuerequest at least one master continues requesting, which is an e_assign
  assign lcd_control_slave_any_continuerequest = 1;

  //cpu_data_master_continuerequest continued request, which is an e_assign
  assign cpu_data_master_continuerequest = 1;

  assign cpu_data_master_qualified_request_lcd_control_slave = cpu_data_master_requests_lcd_control_slave & ~((cpu_data_master_read & ((cpu_data_master_latency_counter != 0))));
  //local readdatavalid cpu_data_master_read_data_valid_lcd_control_slave, which is an e_mux
  assign cpu_data_master_read_data_valid_lcd_control_slave = cpu_data_master_granted_lcd_control_slave & cpu_data_master_read & ~lcd_control_slave_waits_for_read;

  //lcd_control_slave_writedata mux, which is an e_mux
  assign lcd_control_slave_writedata = cpu_data_master_writedata;

  //master is always granted when requested
  assign cpu_data_master_granted_lcd_control_slave = cpu_data_master_qualified_request_lcd_control_slave;

  //cpu/data_master saved-grant lcd/control_slave, which is an e_assign
  assign cpu_data_master_saved_grant_lcd_control_slave = cpu_data_master_requests_lcd_control_slave;

  //allow new arb cycle for lcd/control_slave, which is an e_assign
  assign lcd_control_slave_allow_new_arb_cycle = 1;

  //placeholder chosen master
  assign lcd_control_slave_grant_vector = 1;

  //placeholder vector of master qualified-requests
  assign lcd_control_slave_master_qreq_vector = 1;

  assign lcd_control_slave_begintransfer = lcd_control_slave_begins_xfer;
  //lcd_control_slave_reset_n assignment, which is an e_assign
  assign lcd_control_slave_reset_n = reset_n;

  //lcd_control_slave_firsttransfer first transaction, which is an e_assign
  assign lcd_control_slave_firsttransfer = lcd_control_slave_begins_xfer ? lcd_control_slave_unreg_firsttransfer : lcd_control_slave_reg_firsttransfer;

  //lcd_control_slave_unreg_firsttransfer first transaction, which is an e_assign
  assign lcd_control_slave_unreg_firsttransfer = ~(lcd_control_slave_slavearbiterlockenable & lcd_control_slave_any_continuerequest);

  //lcd_control_slave_reg_firsttransfer first transaction, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          lcd_control_slave_reg_firsttransfer <= 1'b1;
      else if (lcd_control_slave_begins_xfer)
          lcd_control_slave_reg_firsttransfer <= lcd_control_slave_unreg_firsttransfer;
    end


  //lcd_control_slave_beginbursttransfer_internal begin burst transfer, which is an e_assign
  assign lcd_control_slave_beginbursttransfer_internal = lcd_control_slave_begins_xfer;

  //lcd_control_slave_read assignment, which is an e_mux
  assign lcd_control_slave_read = ((cpu_data_master_granted_lcd_control_slave & cpu_data_master_read))& ~lcd_control_slave_begins_xfer & (lcd_control_slave_wait_counter < 25);

  //lcd_control_slave_write assignment, which is an e_mux
  assign lcd_control_slave_write = ((cpu_data_master_granted_lcd_control_slave & cpu_data_master_write)) & ~lcd_control_slave_begins_xfer & (lcd_control_slave_wait_counter >= 25) & (lcd_control_slave_wait_counter < 50) & lcd_control_slave_pretend_byte_enable;

  assign shifted_address_to_lcd_control_slave_from_cpu_data_master = cpu_data_master_address_to_slave;
  //lcd_control_slave_address mux, which is an e_mux
  assign lcd_control_slave_address = shifted_address_to_lcd_control_slave_from_cpu_data_master >> 2;

  //d1_lcd_control_slave_end_xfer register, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          d1_lcd_control_slave_end_xfer <= 1;
      else 
        d1_lcd_control_slave_end_xfer <= lcd_control_slave_end_xfer;
    end


  //lcd_control_slave_waits_for_read in a cycle, which is an e_mux
  assign lcd_control_slave_waits_for_read = lcd_control_slave_in_a_read_cycle & wait_for_lcd_control_slave_counter;

  //lcd_control_slave_in_a_read_cycle assignment, which is an e_assign
  assign lcd_control_slave_in_a_read_cycle = cpu_data_master_granted_lcd_control_slave & cpu_data_master_read;

  //in_a_read_cycle assignment, which is an e_mux
  assign in_a_read_cycle = lcd_control_slave_in_a_read_cycle;

  //lcd_control_slave_waits_for_write in a cycle, which is an e_mux
  assign lcd_control_slave_waits_for_write = lcd_control_slave_in_a_write_cycle & wait_for_lcd_control_slave_counter;

  //lcd_control_slave_in_a_write_cycle assignment, which is an e_assign
  assign lcd_control_slave_in_a_write_cycle = cpu_data_master_granted_lcd_control_slave & cpu_data_master_write;

  //in_a_write_cycle assignment, which is an e_mux
  assign in_a_write_cycle = lcd_control_slave_in_a_write_cycle;

  assign lcd_control_slave_wait_counter_eq_0 = lcd_control_slave_wait_counter == 0;
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          lcd_control_slave_wait_counter <= 0;
      else 
        lcd_control_slave_wait_counter <= lcd_control_slave_counter_load_value;
    end


  assign lcd_control_slave_counter_load_value = ((lcd_control_slave_in_a_read_cycle & lcd_control_slave_begins_xfer))? 48 :
    ((lcd_control_slave_in_a_write_cycle & lcd_control_slave_begins_xfer))? 73 :
    (~lcd_control_slave_wait_counter_eq_0)? lcd_control_slave_wait_counter - 1 :
    0;

  assign wait_for_lcd_control_slave_counter = lcd_control_slave_begins_xfer | ~lcd_control_slave_wait_counter_eq_0;
  //lcd_control_slave_pretend_byte_enable byte enable port mux, which is an e_mux
  assign lcd_control_slave_pretend_byte_enable = (cpu_data_master_granted_lcd_control_slave)? cpu_data_master_byteenable :
    -1;


//synthesis translate_off
//////////////// SIMULATION-ONLY CONTENTS
  //lcd/control_slave enable non-zero assertions, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          enable_nonzero_assertions <= 0;
      else 
        enable_nonzero_assertions <= 1'b1;
    end



//////////////// END SIMULATION-ONLY CONTENTS

//synthesis translate_on

endmodule


// synthesis translate_off
`timescale 1ns / 1ps
// synthesis translate_on

// turn off superfluous verilog processor warnings 
// altera message_level Level1 
// altera message_off 10034 10035 10036 10037 10230 10240 10030 

module onchip_mem_s1_arbitrator (
                                  // inputs:
                                   clk,
                                   cpu_data_master_address_to_slave,
                                   cpu_data_master_byteenable,
                                   cpu_data_master_latency_counter,
                                   cpu_data_master_read,
                                   cpu_data_master_write,
                                   cpu_data_master_writedata,
                                   cpu_instruction_master_address_to_slave,
                                   cpu_instruction_master_latency_counter,
                                   cpu_instruction_master_read,
                                   onchip_mem_s1_readdata,
                                   reset_n,

                                  // outputs:
                                   cpu_data_master_granted_onchip_mem_s1,
                                   cpu_data_master_qualified_request_onchip_mem_s1,
                                   cpu_data_master_read_data_valid_onchip_mem_s1,
                                   cpu_data_master_requests_onchip_mem_s1,
                                   cpu_instruction_master_granted_onchip_mem_s1,
                                   cpu_instruction_master_qualified_request_onchip_mem_s1,
                                   cpu_instruction_master_read_data_valid_onchip_mem_s1,
                                   cpu_instruction_master_requests_onchip_mem_s1,
                                   d1_onchip_mem_s1_end_xfer,
                                   onchip_mem_s1_address,
                                   onchip_mem_s1_byteenable,
                                   onchip_mem_s1_chipselect,
                                   onchip_mem_s1_clken,
                                   onchip_mem_s1_readdata_from_sa,
                                   onchip_mem_s1_reset,
                                   onchip_mem_s1_write,
                                   onchip_mem_s1_writedata
                                )
;

  output           cpu_data_master_granted_onchip_mem_s1;
  output           cpu_data_master_qualified_request_onchip_mem_s1;
  output           cpu_data_master_read_data_valid_onchip_mem_s1;
  output           cpu_data_master_requests_onchip_mem_s1;
  output           cpu_instruction_master_granted_onchip_mem_s1;
  output           cpu_instruction_master_qualified_request_onchip_mem_s1;
  output           cpu_instruction_master_read_data_valid_onchip_mem_s1;
  output           cpu_instruction_master_requests_onchip_mem_s1;
  output           d1_onchip_mem_s1_end_xfer;
  output  [ 11: 0] onchip_mem_s1_address;
  output  [  3: 0] onchip_mem_s1_byteenable;
  output           onchip_mem_s1_chipselect;
  output           onchip_mem_s1_clken;
  output  [ 31: 0] onchip_mem_s1_readdata_from_sa;
  output           onchip_mem_s1_reset;
  output           onchip_mem_s1_write;
  output  [ 31: 0] onchip_mem_s1_writedata;
  input            clk;
  input   [ 24: 0] cpu_data_master_address_to_slave;
  input   [  3: 0] cpu_data_master_byteenable;
  input   [  2: 0] cpu_data_master_latency_counter;
  input            cpu_data_master_read;
  input            cpu_data_master_write;
  input   [ 31: 0] cpu_data_master_writedata;
  input   [ 24: 0] cpu_instruction_master_address_to_slave;
  input   [  2: 0] cpu_instruction_master_latency_counter;
  input            cpu_instruction_master_read;
  input   [ 31: 0] onchip_mem_s1_readdata;
  input            reset_n;

  wire             cpu_data_master_arbiterlock;
  wire             cpu_data_master_arbiterlock2;
  wire             cpu_data_master_continuerequest;
  wire             cpu_data_master_granted_onchip_mem_s1;
  wire             cpu_data_master_qualified_request_onchip_mem_s1;
  wire             cpu_data_master_read_data_valid_onchip_mem_s1;
  reg              cpu_data_master_read_data_valid_onchip_mem_s1_shift_register;
  wire             cpu_data_master_read_data_valid_onchip_mem_s1_shift_register_in;
  wire             cpu_data_master_requests_onchip_mem_s1;
  wire             cpu_data_master_saved_grant_onchip_mem_s1;
  wire             cpu_instruction_master_arbiterlock;
  wire             cpu_instruction_master_arbiterlock2;
  wire             cpu_instruction_master_continuerequest;
  wire             cpu_instruction_master_granted_onchip_mem_s1;
  wire             cpu_instruction_master_qualified_request_onchip_mem_s1;
  wire             cpu_instruction_master_read_data_valid_onchip_mem_s1;
  reg              cpu_instruction_master_read_data_valid_onchip_mem_s1_shift_register;
  wire             cpu_instruction_master_read_data_valid_onchip_mem_s1_shift_register_in;
  wire             cpu_instruction_master_requests_onchip_mem_s1;
  wire             cpu_instruction_master_saved_grant_onchip_mem_s1;
  reg              d1_onchip_mem_s1_end_xfer;
  reg              d1_reasons_to_wait;
  reg              enable_nonzero_assertions;
  wire             end_xfer_arb_share_counter_term_onchip_mem_s1;
  wire             in_a_read_cycle;
  wire             in_a_write_cycle;
  reg              last_cycle_cpu_data_master_granted_slave_onchip_mem_s1;
  reg              last_cycle_cpu_instruction_master_granted_slave_onchip_mem_s1;
  wire    [ 11: 0] onchip_mem_s1_address;
  wire             onchip_mem_s1_allgrants;
  wire             onchip_mem_s1_allow_new_arb_cycle;
  wire             onchip_mem_s1_any_bursting_master_saved_grant;
  wire             onchip_mem_s1_any_continuerequest;
  reg     [  1: 0] onchip_mem_s1_arb_addend;
  wire             onchip_mem_s1_arb_counter_enable;
  reg              onchip_mem_s1_arb_share_counter;
  wire             onchip_mem_s1_arb_share_counter_next_value;
  wire             onchip_mem_s1_arb_share_set_values;
  wire    [  1: 0] onchip_mem_s1_arb_winner;
  wire             onchip_mem_s1_arbitration_holdoff_internal;
  wire             onchip_mem_s1_beginbursttransfer_internal;
  wire             onchip_mem_s1_begins_xfer;
  wire    [  3: 0] onchip_mem_s1_byteenable;
  wire             onchip_mem_s1_chipselect;
  wire    [  3: 0] onchip_mem_s1_chosen_master_double_vector;
  wire    [  1: 0] onchip_mem_s1_chosen_master_rot_left;
  wire             onchip_mem_s1_clken;
  wire             onchip_mem_s1_end_xfer;
  wire             onchip_mem_s1_firsttransfer;
  wire    [  1: 0] onchip_mem_s1_grant_vector;
  wire             onchip_mem_s1_in_a_read_cycle;
  wire             onchip_mem_s1_in_a_write_cycle;
  wire    [  1: 0] onchip_mem_s1_master_qreq_vector;
  wire             onchip_mem_s1_non_bursting_master_requests;
  wire    [ 31: 0] onchip_mem_s1_readdata_from_sa;
  reg              onchip_mem_s1_reg_firsttransfer;
  wire             onchip_mem_s1_reset;
  reg     [  1: 0] onchip_mem_s1_saved_chosen_master_vector;
  reg              onchip_mem_s1_slavearbiterlockenable;
  wire             onchip_mem_s1_slavearbiterlockenable2;
  wire             onchip_mem_s1_unreg_firsttransfer;
  wire             onchip_mem_s1_waits_for_read;
  wire             onchip_mem_s1_waits_for_write;
  wire             onchip_mem_s1_write;
  wire    [ 31: 0] onchip_mem_s1_writedata;
  wire             p1_cpu_data_master_read_data_valid_onchip_mem_s1_shift_register;
  wire             p1_cpu_instruction_master_read_data_valid_onchip_mem_s1_shift_register;
  wire    [ 24: 0] shifted_address_to_onchip_mem_s1_from_cpu_data_master;
  wire    [ 24: 0] shifted_address_to_onchip_mem_s1_from_cpu_instruction_master;
  wire             wait_for_onchip_mem_s1_counter;
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          d1_reasons_to_wait <= 0;
      else 
        d1_reasons_to_wait <= ~onchip_mem_s1_end_xfer;
    end


  assign onchip_mem_s1_begins_xfer = ~d1_reasons_to_wait & ((cpu_data_master_qualified_request_onchip_mem_s1 | cpu_instruction_master_qualified_request_onchip_mem_s1));
  //assign onchip_mem_s1_readdata_from_sa = onchip_mem_s1_readdata so that symbol knows where to group signals which may go to master only, which is an e_assign
  assign onchip_mem_s1_readdata_from_sa = onchip_mem_s1_readdata;

  assign cpu_data_master_requests_onchip_mem_s1 = ({cpu_data_master_address_to_slave[24 : 14] , 14'b0} == 25'h1404000) & (cpu_data_master_read | cpu_data_master_write);
  //onchip_mem_s1_arb_share_counter set values, which is an e_mux
  assign onchip_mem_s1_arb_share_set_values = 1;

  //onchip_mem_s1_non_bursting_master_requests mux, which is an e_mux
  assign onchip_mem_s1_non_bursting_master_requests = cpu_data_master_requests_onchip_mem_s1 |
    cpu_instruction_master_requests_onchip_mem_s1 |
    cpu_data_master_requests_onchip_mem_s1 |
    cpu_instruction_master_requests_onchip_mem_s1;

  //onchip_mem_s1_any_bursting_master_saved_grant mux, which is an e_mux
  assign onchip_mem_s1_any_bursting_master_saved_grant = 0;

  //onchip_mem_s1_arb_share_counter_next_value assignment, which is an e_assign
  assign onchip_mem_s1_arb_share_counter_next_value = onchip_mem_s1_firsttransfer ? (onchip_mem_s1_arb_share_set_values - 1) : |onchip_mem_s1_arb_share_counter ? (onchip_mem_s1_arb_share_counter - 1) : 0;

  //onchip_mem_s1_allgrants all slave grants, which is an e_mux
  assign onchip_mem_s1_allgrants = (|onchip_mem_s1_grant_vector) |
    (|onchip_mem_s1_grant_vector) |
    (|onchip_mem_s1_grant_vector) |
    (|onchip_mem_s1_grant_vector);

  //onchip_mem_s1_end_xfer assignment, which is an e_assign
  assign onchip_mem_s1_end_xfer = ~(onchip_mem_s1_waits_for_read | onchip_mem_s1_waits_for_write);

  //end_xfer_arb_share_counter_term_onchip_mem_s1 arb share counter enable term, which is an e_assign
  assign end_xfer_arb_share_counter_term_onchip_mem_s1 = onchip_mem_s1_end_xfer & (~onchip_mem_s1_any_bursting_master_saved_grant | in_a_read_cycle | in_a_write_cycle);

  //onchip_mem_s1_arb_share_counter arbitration counter enable, which is an e_assign
  assign onchip_mem_s1_arb_counter_enable = (end_xfer_arb_share_counter_term_onchip_mem_s1 & onchip_mem_s1_allgrants) | (end_xfer_arb_share_counter_term_onchip_mem_s1 & ~onchip_mem_s1_non_bursting_master_requests);

  //onchip_mem_s1_arb_share_counter counter, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          onchip_mem_s1_arb_share_counter <= 0;
      else if (onchip_mem_s1_arb_counter_enable)
          onchip_mem_s1_arb_share_counter <= onchip_mem_s1_arb_share_counter_next_value;
    end


  //onchip_mem_s1_slavearbiterlockenable slave enables arbiterlock, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          onchip_mem_s1_slavearbiterlockenable <= 0;
      else if ((|onchip_mem_s1_master_qreq_vector & end_xfer_arb_share_counter_term_onchip_mem_s1) | (end_xfer_arb_share_counter_term_onchip_mem_s1 & ~onchip_mem_s1_non_bursting_master_requests))
          onchip_mem_s1_slavearbiterlockenable <= |onchip_mem_s1_arb_share_counter_next_value;
    end


  //cpu/data_master onchip_mem/s1 arbiterlock, which is an e_assign
  assign cpu_data_master_arbiterlock = onchip_mem_s1_slavearbiterlockenable & cpu_data_master_continuerequest;

  //onchip_mem_s1_slavearbiterlockenable2 slave enables arbiterlock2, which is an e_assign
  assign onchip_mem_s1_slavearbiterlockenable2 = |onchip_mem_s1_arb_share_counter_next_value;

  //cpu/data_master onchip_mem/s1 arbiterlock2, which is an e_assign
  assign cpu_data_master_arbiterlock2 = onchip_mem_s1_slavearbiterlockenable2 & cpu_data_master_continuerequest;

  //cpu/instruction_master onchip_mem/s1 arbiterlock, which is an e_assign
  assign cpu_instruction_master_arbiterlock = onchip_mem_s1_slavearbiterlockenable & cpu_instruction_master_continuerequest;

  //cpu/instruction_master onchip_mem/s1 arbiterlock2, which is an e_assign
  assign cpu_instruction_master_arbiterlock2 = onchip_mem_s1_slavearbiterlockenable2 & cpu_instruction_master_continuerequest;

  //cpu/instruction_master granted onchip_mem/s1 last time, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          last_cycle_cpu_instruction_master_granted_slave_onchip_mem_s1 <= 0;
      else 
        last_cycle_cpu_instruction_master_granted_slave_onchip_mem_s1 <= cpu_instruction_master_saved_grant_onchip_mem_s1 ? 1 : (onchip_mem_s1_arbitration_holdoff_internal | ~cpu_instruction_master_requests_onchip_mem_s1) ? 0 : last_cycle_cpu_instruction_master_granted_slave_onchip_mem_s1;
    end


  //cpu_instruction_master_continuerequest continued request, which is an e_mux
  assign cpu_instruction_master_continuerequest = last_cycle_cpu_instruction_master_granted_slave_onchip_mem_s1 & cpu_instruction_master_requests_onchip_mem_s1;

  //onchip_mem_s1_any_continuerequest at least one master continues requesting, which is an e_mux
  assign onchip_mem_s1_any_continuerequest = cpu_instruction_master_continuerequest |
    cpu_data_master_continuerequest;

  assign cpu_data_master_qualified_request_onchip_mem_s1 = cpu_data_master_requests_onchip_mem_s1 & ~((cpu_data_master_read & ((1 < cpu_data_master_latency_counter))) | cpu_instruction_master_arbiterlock);
  //cpu_data_master_read_data_valid_onchip_mem_s1_shift_register_in mux for readlatency shift register, which is an e_mux
  assign cpu_data_master_read_data_valid_onchip_mem_s1_shift_register_in = cpu_data_master_granted_onchip_mem_s1 & cpu_data_master_read & ~onchip_mem_s1_waits_for_read;

  //shift register p1 cpu_data_master_read_data_valid_onchip_mem_s1_shift_register in if flush, otherwise shift left, which is an e_mux
  assign p1_cpu_data_master_read_data_valid_onchip_mem_s1_shift_register = {cpu_data_master_read_data_valid_onchip_mem_s1_shift_register, cpu_data_master_read_data_valid_onchip_mem_s1_shift_register_in};

  //cpu_data_master_read_data_valid_onchip_mem_s1_shift_register for remembering which master asked for a fixed latency read, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          cpu_data_master_read_data_valid_onchip_mem_s1_shift_register <= 0;
      else 
        cpu_data_master_read_data_valid_onchip_mem_s1_shift_register <= p1_cpu_data_master_read_data_valid_onchip_mem_s1_shift_register;
    end


  //local readdatavalid cpu_data_master_read_data_valid_onchip_mem_s1, which is an e_mux
  assign cpu_data_master_read_data_valid_onchip_mem_s1 = cpu_data_master_read_data_valid_onchip_mem_s1_shift_register;

  //onchip_mem_s1_writedata mux, which is an e_mux
  assign onchip_mem_s1_writedata = cpu_data_master_writedata;

  //mux onchip_mem_s1_clken, which is an e_mux
  assign onchip_mem_s1_clken = 1'b1;

  assign cpu_instruction_master_requests_onchip_mem_s1 = (({cpu_instruction_master_address_to_slave[24 : 14] , 14'b0} == 25'h1404000) & (cpu_instruction_master_read)) & cpu_instruction_master_read;
  //cpu/data_master granted onchip_mem/s1 last time, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          last_cycle_cpu_data_master_granted_slave_onchip_mem_s1 <= 0;
      else 
        last_cycle_cpu_data_master_granted_slave_onchip_mem_s1 <= cpu_data_master_saved_grant_onchip_mem_s1 ? 1 : (onchip_mem_s1_arbitration_holdoff_internal | ~cpu_data_master_requests_onchip_mem_s1) ? 0 : last_cycle_cpu_data_master_granted_slave_onchip_mem_s1;
    end


  //cpu_data_master_continuerequest continued request, which is an e_mux
  assign cpu_data_master_continuerequest = last_cycle_cpu_data_master_granted_slave_onchip_mem_s1 & cpu_data_master_requests_onchip_mem_s1;

  assign cpu_instruction_master_qualified_request_onchip_mem_s1 = cpu_instruction_master_requests_onchip_mem_s1 & ~((cpu_instruction_master_read & ((1 < cpu_instruction_master_latency_counter))) | cpu_data_master_arbiterlock);
  //cpu_instruction_master_read_data_valid_onchip_mem_s1_shift_register_in mux for readlatency shift register, which is an e_mux
  assign cpu_instruction_master_read_data_valid_onchip_mem_s1_shift_register_in = cpu_instruction_master_granted_onchip_mem_s1 & cpu_instruction_master_read & ~onchip_mem_s1_waits_for_read;

  //shift register p1 cpu_instruction_master_read_data_valid_onchip_mem_s1_shift_register in if flush, otherwise shift left, which is an e_mux
  assign p1_cpu_instruction_master_read_data_valid_onchip_mem_s1_shift_register = {cpu_instruction_master_read_data_valid_onchip_mem_s1_shift_register, cpu_instruction_master_read_data_valid_onchip_mem_s1_shift_register_in};

  //cpu_instruction_master_read_data_valid_onchip_mem_s1_shift_register for remembering which master asked for a fixed latency read, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          cpu_instruction_master_read_data_valid_onchip_mem_s1_shift_register <= 0;
      else 
        cpu_instruction_master_read_data_valid_onchip_mem_s1_shift_register <= p1_cpu_instruction_master_read_data_valid_onchip_mem_s1_shift_register;
    end


  //local readdatavalid cpu_instruction_master_read_data_valid_onchip_mem_s1, which is an e_mux
  assign cpu_instruction_master_read_data_valid_onchip_mem_s1 = cpu_instruction_master_read_data_valid_onchip_mem_s1_shift_register;

  //allow new arb cycle for onchip_mem/s1, which is an e_assign
  assign onchip_mem_s1_allow_new_arb_cycle = ~cpu_data_master_arbiterlock & ~cpu_instruction_master_arbiterlock;

  //cpu/instruction_master assignment into master qualified-requests vector for onchip_mem/s1, which is an e_assign
  assign onchip_mem_s1_master_qreq_vector[0] = cpu_instruction_master_qualified_request_onchip_mem_s1;

  //cpu/instruction_master grant onchip_mem/s1, which is an e_assign
  assign cpu_instruction_master_granted_onchip_mem_s1 = onchip_mem_s1_grant_vector[0];

  //cpu/instruction_master saved-grant onchip_mem/s1, which is an e_assign
  assign cpu_instruction_master_saved_grant_onchip_mem_s1 = onchip_mem_s1_arb_winner[0] && cpu_instruction_master_requests_onchip_mem_s1;

  //cpu/data_master assignment into master qualified-requests vector for onchip_mem/s1, which is an e_assign
  assign onchip_mem_s1_master_qreq_vector[1] = cpu_data_master_qualified_request_onchip_mem_s1;

  //cpu/data_master grant onchip_mem/s1, which is an e_assign
  assign cpu_data_master_granted_onchip_mem_s1 = onchip_mem_s1_grant_vector[1];

  //cpu/data_master saved-grant onchip_mem/s1, which is an e_assign
  assign cpu_data_master_saved_grant_onchip_mem_s1 = onchip_mem_s1_arb_winner[1] && cpu_data_master_requests_onchip_mem_s1;

  //onchip_mem/s1 chosen-master double-vector, which is an e_assign
  assign onchip_mem_s1_chosen_master_double_vector = {onchip_mem_s1_master_qreq_vector, onchip_mem_s1_master_qreq_vector} & ({~onchip_mem_s1_master_qreq_vector, ~onchip_mem_s1_master_qreq_vector} + onchip_mem_s1_arb_addend);

  //stable onehot encoding of arb winner
  assign onchip_mem_s1_arb_winner = (onchip_mem_s1_allow_new_arb_cycle & | onchip_mem_s1_grant_vector) ? onchip_mem_s1_grant_vector : onchip_mem_s1_saved_chosen_master_vector;

  //saved onchip_mem_s1_grant_vector, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          onchip_mem_s1_saved_chosen_master_vector <= 0;
      else if (onchip_mem_s1_allow_new_arb_cycle)
          onchip_mem_s1_saved_chosen_master_vector <= |onchip_mem_s1_grant_vector ? onchip_mem_s1_grant_vector : onchip_mem_s1_saved_chosen_master_vector;
    end


  //onehot encoding of chosen master
  assign onchip_mem_s1_grant_vector = {(onchip_mem_s1_chosen_master_double_vector[1] | onchip_mem_s1_chosen_master_double_vector[3]),
    (onchip_mem_s1_chosen_master_double_vector[0] | onchip_mem_s1_chosen_master_double_vector[2])};

  //onchip_mem/s1 chosen master rotated left, which is an e_assign
  assign onchip_mem_s1_chosen_master_rot_left = (onchip_mem_s1_arb_winner << 1) ? (onchip_mem_s1_arb_winner << 1) : 1;

  //onchip_mem/s1's addend for next-master-grant
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          onchip_mem_s1_arb_addend <= 1;
      else if (|onchip_mem_s1_grant_vector)
          onchip_mem_s1_arb_addend <= onchip_mem_s1_end_xfer? onchip_mem_s1_chosen_master_rot_left : onchip_mem_s1_grant_vector;
    end


  //~onchip_mem_s1_reset assignment, which is an e_assign
  assign onchip_mem_s1_reset = ~reset_n;

  assign onchip_mem_s1_chipselect = cpu_data_master_granted_onchip_mem_s1 | cpu_instruction_master_granted_onchip_mem_s1;
  //onchip_mem_s1_firsttransfer first transaction, which is an e_assign
  assign onchip_mem_s1_firsttransfer = onchip_mem_s1_begins_xfer ? onchip_mem_s1_unreg_firsttransfer : onchip_mem_s1_reg_firsttransfer;

  //onchip_mem_s1_unreg_firsttransfer first transaction, which is an e_assign
  assign onchip_mem_s1_unreg_firsttransfer = ~(onchip_mem_s1_slavearbiterlockenable & onchip_mem_s1_any_continuerequest);

  //onchip_mem_s1_reg_firsttransfer first transaction, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          onchip_mem_s1_reg_firsttransfer <= 1'b1;
      else if (onchip_mem_s1_begins_xfer)
          onchip_mem_s1_reg_firsttransfer <= onchip_mem_s1_unreg_firsttransfer;
    end


  //onchip_mem_s1_beginbursttransfer_internal begin burst transfer, which is an e_assign
  assign onchip_mem_s1_beginbursttransfer_internal = onchip_mem_s1_begins_xfer;

  //onchip_mem_s1_arbitration_holdoff_internal arbitration_holdoff, which is an e_assign
  assign onchip_mem_s1_arbitration_holdoff_internal = onchip_mem_s1_begins_xfer & onchip_mem_s1_firsttransfer;

  //onchip_mem_s1_write assignment, which is an e_mux
  assign onchip_mem_s1_write = cpu_data_master_granted_onchip_mem_s1 & cpu_data_master_write;

  assign shifted_address_to_onchip_mem_s1_from_cpu_data_master = cpu_data_master_address_to_slave;
  //onchip_mem_s1_address mux, which is an e_mux
  assign onchip_mem_s1_address = (cpu_data_master_granted_onchip_mem_s1)? (shifted_address_to_onchip_mem_s1_from_cpu_data_master >> 2) :
    (shifted_address_to_onchip_mem_s1_from_cpu_instruction_master >> 2);

  assign shifted_address_to_onchip_mem_s1_from_cpu_instruction_master = cpu_instruction_master_address_to_slave;
  //d1_onchip_mem_s1_end_xfer register, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          d1_onchip_mem_s1_end_xfer <= 1;
      else 
        d1_onchip_mem_s1_end_xfer <= onchip_mem_s1_end_xfer;
    end


  //onchip_mem_s1_waits_for_read in a cycle, which is an e_mux
  assign onchip_mem_s1_waits_for_read = onchip_mem_s1_in_a_read_cycle & 0;

  //onchip_mem_s1_in_a_read_cycle assignment, which is an e_assign
  assign onchip_mem_s1_in_a_read_cycle = (cpu_data_master_granted_onchip_mem_s1 & cpu_data_master_read) | (cpu_instruction_master_granted_onchip_mem_s1 & cpu_instruction_master_read);

  //in_a_read_cycle assignment, which is an e_mux
  assign in_a_read_cycle = onchip_mem_s1_in_a_read_cycle;

  //onchip_mem_s1_waits_for_write in a cycle, which is an e_mux
  assign onchip_mem_s1_waits_for_write = onchip_mem_s1_in_a_write_cycle & 0;

  //onchip_mem_s1_in_a_write_cycle assignment, which is an e_assign
  assign onchip_mem_s1_in_a_write_cycle = cpu_data_master_granted_onchip_mem_s1 & cpu_data_master_write;

  //in_a_write_cycle assignment, which is an e_mux
  assign in_a_write_cycle = onchip_mem_s1_in_a_write_cycle;

  assign wait_for_onchip_mem_s1_counter = 0;
  //onchip_mem_s1_byteenable byte enable port mux, which is an e_mux
  assign onchip_mem_s1_byteenable = (cpu_data_master_granted_onchip_mem_s1)? cpu_data_master_byteenable :
    -1;


//synthesis translate_off
//////////////// SIMULATION-ONLY CONTENTS
  //onchip_mem/s1 enable non-zero assertions, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          enable_nonzero_assertions <= 0;
      else 
        enable_nonzero_assertions <= 1'b1;
    end


  //grant signals are active simultaneously, which is an e_process
  always @(posedge clk)
    begin
      if (cpu_data_master_granted_onchip_mem_s1 + cpu_instruction_master_granted_onchip_mem_s1 > 1)
        begin
          $write("%0d ns: > 1 of grant signals are active simultaneously", $time);
          $stop;
        end
    end


  //saved_grant signals are active simultaneously, which is an e_process
  always @(posedge clk)
    begin
      if (cpu_data_master_saved_grant_onchip_mem_s1 + cpu_instruction_master_saved_grant_onchip_mem_s1 > 1)
        begin
          $write("%0d ns: > 1 of saved_grant signals are active simultaneously", $time);
          $stop;
        end
    end



//////////////// END SIMULATION-ONLY CONTENTS

//synthesis translate_on

endmodule


// synthesis translate_off
`timescale 1ns / 1ps
// synthesis translate_on

// turn off superfluous verilog processor warnings 
// altera message_level Level1 
// altera message_off 10034 10035 10036 10037 10230 10240 10030 

module pio_button_s1_arbitrator (
                                  // inputs:
                                   clk,
                                   cpu_data_master_address_to_slave,
                                   cpu_data_master_latency_counter,
                                   cpu_data_master_read,
                                   cpu_data_master_write,
                                   cpu_data_master_writedata,
                                   pio_button_s1_irq,
                                   pio_button_s1_readdata,
                                   reset_n,

                                  // outputs:
                                   cpu_data_master_granted_pio_button_s1,
                                   cpu_data_master_qualified_request_pio_button_s1,
                                   cpu_data_master_read_data_valid_pio_button_s1,
                                   cpu_data_master_requests_pio_button_s1,
                                   d1_pio_button_s1_end_xfer,
                                   pio_button_s1_address,
                                   pio_button_s1_chipselect,
                                   pio_button_s1_irq_from_sa,
                                   pio_button_s1_readdata_from_sa,
                                   pio_button_s1_reset_n,
                                   pio_button_s1_write_n,
                                   pio_button_s1_writedata
                                )
;

  output           cpu_data_master_granted_pio_button_s1;
  output           cpu_data_master_qualified_request_pio_button_s1;
  output           cpu_data_master_read_data_valid_pio_button_s1;
  output           cpu_data_master_requests_pio_button_s1;
  output           d1_pio_button_s1_end_xfer;
  output  [  1: 0] pio_button_s1_address;
  output           pio_button_s1_chipselect;
  output           pio_button_s1_irq_from_sa;
  output  [ 31: 0] pio_button_s1_readdata_from_sa;
  output           pio_button_s1_reset_n;
  output           pio_button_s1_write_n;
  output  [ 31: 0] pio_button_s1_writedata;
  input            clk;
  input   [ 24: 0] cpu_data_master_address_to_slave;
  input   [  2: 0] cpu_data_master_latency_counter;
  input            cpu_data_master_read;
  input            cpu_data_master_write;
  input   [ 31: 0] cpu_data_master_writedata;
  input            pio_button_s1_irq;
  input   [ 31: 0] pio_button_s1_readdata;
  input            reset_n;

  wire             cpu_data_master_arbiterlock;
  wire             cpu_data_master_arbiterlock2;
  wire             cpu_data_master_continuerequest;
  wire             cpu_data_master_granted_pio_button_s1;
  wire             cpu_data_master_qualified_request_pio_button_s1;
  wire             cpu_data_master_read_data_valid_pio_button_s1;
  wire             cpu_data_master_requests_pio_button_s1;
  wire             cpu_data_master_saved_grant_pio_button_s1;
  reg              d1_pio_button_s1_end_xfer;
  reg              d1_reasons_to_wait;
  reg              enable_nonzero_assertions;
  wire             end_xfer_arb_share_counter_term_pio_button_s1;
  wire             in_a_read_cycle;
  wire             in_a_write_cycle;
  wire    [  1: 0] pio_button_s1_address;
  wire             pio_button_s1_allgrants;
  wire             pio_button_s1_allow_new_arb_cycle;
  wire             pio_button_s1_any_bursting_master_saved_grant;
  wire             pio_button_s1_any_continuerequest;
  wire             pio_button_s1_arb_counter_enable;
  reg              pio_button_s1_arb_share_counter;
  wire             pio_button_s1_arb_share_counter_next_value;
  wire             pio_button_s1_arb_share_set_values;
  wire             pio_button_s1_beginbursttransfer_internal;
  wire             pio_button_s1_begins_xfer;
  wire             pio_button_s1_chipselect;
  wire             pio_button_s1_end_xfer;
  wire             pio_button_s1_firsttransfer;
  wire             pio_button_s1_grant_vector;
  wire             pio_button_s1_in_a_read_cycle;
  wire             pio_button_s1_in_a_write_cycle;
  wire             pio_button_s1_irq_from_sa;
  wire             pio_button_s1_master_qreq_vector;
  wire             pio_button_s1_non_bursting_master_requests;
  wire    [ 31: 0] pio_button_s1_readdata_from_sa;
  reg              pio_button_s1_reg_firsttransfer;
  wire             pio_button_s1_reset_n;
  reg              pio_button_s1_slavearbiterlockenable;
  wire             pio_button_s1_slavearbiterlockenable2;
  wire             pio_button_s1_unreg_firsttransfer;
  wire             pio_button_s1_waits_for_read;
  wire             pio_button_s1_waits_for_write;
  wire             pio_button_s1_write_n;
  wire    [ 31: 0] pio_button_s1_writedata;
  wire    [ 24: 0] shifted_address_to_pio_button_s1_from_cpu_data_master;
  wire             wait_for_pio_button_s1_counter;
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          d1_reasons_to_wait <= 0;
      else 
        d1_reasons_to_wait <= ~pio_button_s1_end_xfer;
    end


  assign pio_button_s1_begins_xfer = ~d1_reasons_to_wait & ((cpu_data_master_qualified_request_pio_button_s1));
  //assign pio_button_s1_readdata_from_sa = pio_button_s1_readdata so that symbol knows where to group signals which may go to master only, which is an e_assign
  assign pio_button_s1_readdata_from_sa = pio_button_s1_readdata;

  assign cpu_data_master_requests_pio_button_s1 = ({cpu_data_master_address_to_slave[24 : 4] , 4'b0} == 25'h14090d0) & (cpu_data_master_read | cpu_data_master_write);
  //pio_button_s1_arb_share_counter set values, which is an e_mux
  assign pio_button_s1_arb_share_set_values = 1;

  //pio_button_s1_non_bursting_master_requests mux, which is an e_mux
  assign pio_button_s1_non_bursting_master_requests = cpu_data_master_requests_pio_button_s1;

  //pio_button_s1_any_bursting_master_saved_grant mux, which is an e_mux
  assign pio_button_s1_any_bursting_master_saved_grant = 0;

  //pio_button_s1_arb_share_counter_next_value assignment, which is an e_assign
  assign pio_button_s1_arb_share_counter_next_value = pio_button_s1_firsttransfer ? (pio_button_s1_arb_share_set_values - 1) : |pio_button_s1_arb_share_counter ? (pio_button_s1_arb_share_counter - 1) : 0;

  //pio_button_s1_allgrants all slave grants, which is an e_mux
  assign pio_button_s1_allgrants = |pio_button_s1_grant_vector;

  //pio_button_s1_end_xfer assignment, which is an e_assign
  assign pio_button_s1_end_xfer = ~(pio_button_s1_waits_for_read | pio_button_s1_waits_for_write);

  //end_xfer_arb_share_counter_term_pio_button_s1 arb share counter enable term, which is an e_assign
  assign end_xfer_arb_share_counter_term_pio_button_s1 = pio_button_s1_end_xfer & (~pio_button_s1_any_bursting_master_saved_grant | in_a_read_cycle | in_a_write_cycle);

  //pio_button_s1_arb_share_counter arbitration counter enable, which is an e_assign
  assign pio_button_s1_arb_counter_enable = (end_xfer_arb_share_counter_term_pio_button_s1 & pio_button_s1_allgrants) | (end_xfer_arb_share_counter_term_pio_button_s1 & ~pio_button_s1_non_bursting_master_requests);

  //pio_button_s1_arb_share_counter counter, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          pio_button_s1_arb_share_counter <= 0;
      else if (pio_button_s1_arb_counter_enable)
          pio_button_s1_arb_share_counter <= pio_button_s1_arb_share_counter_next_value;
    end


  //pio_button_s1_slavearbiterlockenable slave enables arbiterlock, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          pio_button_s1_slavearbiterlockenable <= 0;
      else if ((|pio_button_s1_master_qreq_vector & end_xfer_arb_share_counter_term_pio_button_s1) | (end_xfer_arb_share_counter_term_pio_button_s1 & ~pio_button_s1_non_bursting_master_requests))
          pio_button_s1_slavearbiterlockenable <= |pio_button_s1_arb_share_counter_next_value;
    end


  //cpu/data_master pio_button/s1 arbiterlock, which is an e_assign
  assign cpu_data_master_arbiterlock = pio_button_s1_slavearbiterlockenable & cpu_data_master_continuerequest;

  //pio_button_s1_slavearbiterlockenable2 slave enables arbiterlock2, which is an e_assign
  assign pio_button_s1_slavearbiterlockenable2 = |pio_button_s1_arb_share_counter_next_value;

  //cpu/data_master pio_button/s1 arbiterlock2, which is an e_assign
  assign cpu_data_master_arbiterlock2 = pio_button_s1_slavearbiterlockenable2 & cpu_data_master_continuerequest;

  //pio_button_s1_any_continuerequest at least one master continues requesting, which is an e_assign
  assign pio_button_s1_any_continuerequest = 1;

  //cpu_data_master_continuerequest continued request, which is an e_assign
  assign cpu_data_master_continuerequest = 1;

  assign cpu_data_master_qualified_request_pio_button_s1 = cpu_data_master_requests_pio_button_s1 & ~((cpu_data_master_read & ((cpu_data_master_latency_counter != 0))));
  //local readdatavalid cpu_data_master_read_data_valid_pio_button_s1, which is an e_mux
  assign cpu_data_master_read_data_valid_pio_button_s1 = cpu_data_master_granted_pio_button_s1 & cpu_data_master_read & ~pio_button_s1_waits_for_read;

  //pio_button_s1_writedata mux, which is an e_mux
  assign pio_button_s1_writedata = cpu_data_master_writedata;

  //master is always granted when requested
  assign cpu_data_master_granted_pio_button_s1 = cpu_data_master_qualified_request_pio_button_s1;

  //cpu/data_master saved-grant pio_button/s1, which is an e_assign
  assign cpu_data_master_saved_grant_pio_button_s1 = cpu_data_master_requests_pio_button_s1;

  //allow new arb cycle for pio_button/s1, which is an e_assign
  assign pio_button_s1_allow_new_arb_cycle = 1;

  //placeholder chosen master
  assign pio_button_s1_grant_vector = 1;

  //placeholder vector of master qualified-requests
  assign pio_button_s1_master_qreq_vector = 1;

  //pio_button_s1_reset_n assignment, which is an e_assign
  assign pio_button_s1_reset_n = reset_n;

  assign pio_button_s1_chipselect = cpu_data_master_granted_pio_button_s1;
  //pio_button_s1_firsttransfer first transaction, which is an e_assign
  assign pio_button_s1_firsttransfer = pio_button_s1_begins_xfer ? pio_button_s1_unreg_firsttransfer : pio_button_s1_reg_firsttransfer;

  //pio_button_s1_unreg_firsttransfer first transaction, which is an e_assign
  assign pio_button_s1_unreg_firsttransfer = ~(pio_button_s1_slavearbiterlockenable & pio_button_s1_any_continuerequest);

  //pio_button_s1_reg_firsttransfer first transaction, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          pio_button_s1_reg_firsttransfer <= 1'b1;
      else if (pio_button_s1_begins_xfer)
          pio_button_s1_reg_firsttransfer <= pio_button_s1_unreg_firsttransfer;
    end


  //pio_button_s1_beginbursttransfer_internal begin burst transfer, which is an e_assign
  assign pio_button_s1_beginbursttransfer_internal = pio_button_s1_begins_xfer;

  //~pio_button_s1_write_n assignment, which is an e_mux
  assign pio_button_s1_write_n = ~(cpu_data_master_granted_pio_button_s1 & cpu_data_master_write);

  assign shifted_address_to_pio_button_s1_from_cpu_data_master = cpu_data_master_address_to_slave;
  //pio_button_s1_address mux, which is an e_mux
  assign pio_button_s1_address = shifted_address_to_pio_button_s1_from_cpu_data_master >> 2;

  //d1_pio_button_s1_end_xfer register, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          d1_pio_button_s1_end_xfer <= 1;
      else 
        d1_pio_button_s1_end_xfer <= pio_button_s1_end_xfer;
    end


  //pio_button_s1_waits_for_read in a cycle, which is an e_mux
  assign pio_button_s1_waits_for_read = pio_button_s1_in_a_read_cycle & pio_button_s1_begins_xfer;

  //pio_button_s1_in_a_read_cycle assignment, which is an e_assign
  assign pio_button_s1_in_a_read_cycle = cpu_data_master_granted_pio_button_s1 & cpu_data_master_read;

  //in_a_read_cycle assignment, which is an e_mux
  assign in_a_read_cycle = pio_button_s1_in_a_read_cycle;

  //pio_button_s1_waits_for_write in a cycle, which is an e_mux
  assign pio_button_s1_waits_for_write = pio_button_s1_in_a_write_cycle & 0;

  //pio_button_s1_in_a_write_cycle assignment, which is an e_assign
  assign pio_button_s1_in_a_write_cycle = cpu_data_master_granted_pio_button_s1 & cpu_data_master_write;

  //in_a_write_cycle assignment, which is an e_mux
  assign in_a_write_cycle = pio_button_s1_in_a_write_cycle;

  assign wait_for_pio_button_s1_counter = 0;
  //assign pio_button_s1_irq_from_sa = pio_button_s1_irq so that symbol knows where to group signals which may go to master only, which is an e_assign
  assign pio_button_s1_irq_from_sa = pio_button_s1_irq;


//synthesis translate_off
//////////////// SIMULATION-ONLY CONTENTS
  //pio_button/s1 enable non-zero assertions, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          enable_nonzero_assertions <= 0;
      else 
        enable_nonzero_assertions <= 1'b1;
    end



//////////////// END SIMULATION-ONLY CONTENTS

//synthesis translate_on

endmodule


// synthesis translate_off
`timescale 1ns / 1ps
// synthesis translate_on

// turn off superfluous verilog processor warnings 
// altera message_level Level1 
// altera message_off 10034 10035 10036 10037 10230 10240 10030 

module pio_switch_s1_arbitrator (
                                  // inputs:
                                   clk,
                                   cpu_data_master_address_to_slave,
                                   cpu_data_master_latency_counter,
                                   cpu_data_master_read,
                                   cpu_data_master_write,
                                   pio_switch_s1_readdata,
                                   reset_n,

                                  // outputs:
                                   cpu_data_master_granted_pio_switch_s1,
                                   cpu_data_master_qualified_request_pio_switch_s1,
                                   cpu_data_master_read_data_valid_pio_switch_s1,
                                   cpu_data_master_requests_pio_switch_s1,
                                   d1_pio_switch_s1_end_xfer,
                                   pio_switch_s1_address,
                                   pio_switch_s1_readdata_from_sa,
                                   pio_switch_s1_reset_n
                                )
;

  output           cpu_data_master_granted_pio_switch_s1;
  output           cpu_data_master_qualified_request_pio_switch_s1;
  output           cpu_data_master_read_data_valid_pio_switch_s1;
  output           cpu_data_master_requests_pio_switch_s1;
  output           d1_pio_switch_s1_end_xfer;
  output  [  1: 0] pio_switch_s1_address;
  output  [ 31: 0] pio_switch_s1_readdata_from_sa;
  output           pio_switch_s1_reset_n;
  input            clk;
  input   [ 24: 0] cpu_data_master_address_to_slave;
  input   [  2: 0] cpu_data_master_latency_counter;
  input            cpu_data_master_read;
  input            cpu_data_master_write;
  input   [ 31: 0] pio_switch_s1_readdata;
  input            reset_n;

  wire             cpu_data_master_arbiterlock;
  wire             cpu_data_master_arbiterlock2;
  wire             cpu_data_master_continuerequest;
  wire             cpu_data_master_granted_pio_switch_s1;
  wire             cpu_data_master_qualified_request_pio_switch_s1;
  wire             cpu_data_master_read_data_valid_pio_switch_s1;
  wire             cpu_data_master_requests_pio_switch_s1;
  wire             cpu_data_master_saved_grant_pio_switch_s1;
  reg              d1_pio_switch_s1_end_xfer;
  reg              d1_reasons_to_wait;
  reg              enable_nonzero_assertions;
  wire             end_xfer_arb_share_counter_term_pio_switch_s1;
  wire             in_a_read_cycle;
  wire             in_a_write_cycle;
  wire    [  1: 0] pio_switch_s1_address;
  wire             pio_switch_s1_allgrants;
  wire             pio_switch_s1_allow_new_arb_cycle;
  wire             pio_switch_s1_any_bursting_master_saved_grant;
  wire             pio_switch_s1_any_continuerequest;
  wire             pio_switch_s1_arb_counter_enable;
  reg              pio_switch_s1_arb_share_counter;
  wire             pio_switch_s1_arb_share_counter_next_value;
  wire             pio_switch_s1_arb_share_set_values;
  wire             pio_switch_s1_beginbursttransfer_internal;
  wire             pio_switch_s1_begins_xfer;
  wire             pio_switch_s1_end_xfer;
  wire             pio_switch_s1_firsttransfer;
  wire             pio_switch_s1_grant_vector;
  wire             pio_switch_s1_in_a_read_cycle;
  wire             pio_switch_s1_in_a_write_cycle;
  wire             pio_switch_s1_master_qreq_vector;
  wire             pio_switch_s1_non_bursting_master_requests;
  wire    [ 31: 0] pio_switch_s1_readdata_from_sa;
  reg              pio_switch_s1_reg_firsttransfer;
  wire             pio_switch_s1_reset_n;
  reg              pio_switch_s1_slavearbiterlockenable;
  wire             pio_switch_s1_slavearbiterlockenable2;
  wire             pio_switch_s1_unreg_firsttransfer;
  wire             pio_switch_s1_waits_for_read;
  wire             pio_switch_s1_waits_for_write;
  wire    [ 24: 0] shifted_address_to_pio_switch_s1_from_cpu_data_master;
  wire             wait_for_pio_switch_s1_counter;
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          d1_reasons_to_wait <= 0;
      else 
        d1_reasons_to_wait <= ~pio_switch_s1_end_xfer;
    end


  assign pio_switch_s1_begins_xfer = ~d1_reasons_to_wait & ((cpu_data_master_qualified_request_pio_switch_s1));
  //assign pio_switch_s1_readdata_from_sa = pio_switch_s1_readdata so that symbol knows where to group signals which may go to master only, which is an e_assign
  assign pio_switch_s1_readdata_from_sa = pio_switch_s1_readdata;

  assign cpu_data_master_requests_pio_switch_s1 = (({cpu_data_master_address_to_slave[24 : 4] , 4'b0} == 25'h14090c0) & (cpu_data_master_read | cpu_data_master_write)) & cpu_data_master_read;
  //pio_switch_s1_arb_share_counter set values, which is an e_mux
  assign pio_switch_s1_arb_share_set_values = 1;

  //pio_switch_s1_non_bursting_master_requests mux, which is an e_mux
  assign pio_switch_s1_non_bursting_master_requests = cpu_data_master_requests_pio_switch_s1;

  //pio_switch_s1_any_bursting_master_saved_grant mux, which is an e_mux
  assign pio_switch_s1_any_bursting_master_saved_grant = 0;

  //pio_switch_s1_arb_share_counter_next_value assignment, which is an e_assign
  assign pio_switch_s1_arb_share_counter_next_value = pio_switch_s1_firsttransfer ? (pio_switch_s1_arb_share_set_values - 1) : |pio_switch_s1_arb_share_counter ? (pio_switch_s1_arb_share_counter - 1) : 0;

  //pio_switch_s1_allgrants all slave grants, which is an e_mux
  assign pio_switch_s1_allgrants = |pio_switch_s1_grant_vector;

  //pio_switch_s1_end_xfer assignment, which is an e_assign
  assign pio_switch_s1_end_xfer = ~(pio_switch_s1_waits_for_read | pio_switch_s1_waits_for_write);

  //end_xfer_arb_share_counter_term_pio_switch_s1 arb share counter enable term, which is an e_assign
  assign end_xfer_arb_share_counter_term_pio_switch_s1 = pio_switch_s1_end_xfer & (~pio_switch_s1_any_bursting_master_saved_grant | in_a_read_cycle | in_a_write_cycle);

  //pio_switch_s1_arb_share_counter arbitration counter enable, which is an e_assign
  assign pio_switch_s1_arb_counter_enable = (end_xfer_arb_share_counter_term_pio_switch_s1 & pio_switch_s1_allgrants) | (end_xfer_arb_share_counter_term_pio_switch_s1 & ~pio_switch_s1_non_bursting_master_requests);

  //pio_switch_s1_arb_share_counter counter, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          pio_switch_s1_arb_share_counter <= 0;
      else if (pio_switch_s1_arb_counter_enable)
          pio_switch_s1_arb_share_counter <= pio_switch_s1_arb_share_counter_next_value;
    end


  //pio_switch_s1_slavearbiterlockenable slave enables arbiterlock, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          pio_switch_s1_slavearbiterlockenable <= 0;
      else if ((|pio_switch_s1_master_qreq_vector & end_xfer_arb_share_counter_term_pio_switch_s1) | (end_xfer_arb_share_counter_term_pio_switch_s1 & ~pio_switch_s1_non_bursting_master_requests))
          pio_switch_s1_slavearbiterlockenable <= |pio_switch_s1_arb_share_counter_next_value;
    end


  //cpu/data_master pio_switch/s1 arbiterlock, which is an e_assign
  assign cpu_data_master_arbiterlock = pio_switch_s1_slavearbiterlockenable & cpu_data_master_continuerequest;

  //pio_switch_s1_slavearbiterlockenable2 slave enables arbiterlock2, which is an e_assign
  assign pio_switch_s1_slavearbiterlockenable2 = |pio_switch_s1_arb_share_counter_next_value;

  //cpu/data_master pio_switch/s1 arbiterlock2, which is an e_assign
  assign cpu_data_master_arbiterlock2 = pio_switch_s1_slavearbiterlockenable2 & cpu_data_master_continuerequest;

  //pio_switch_s1_any_continuerequest at least one master continues requesting, which is an e_assign
  assign pio_switch_s1_any_continuerequest = 1;

  //cpu_data_master_continuerequest continued request, which is an e_assign
  assign cpu_data_master_continuerequest = 1;

  assign cpu_data_master_qualified_request_pio_switch_s1 = cpu_data_master_requests_pio_switch_s1 & ~((cpu_data_master_read & ((cpu_data_master_latency_counter != 0))));
  //local readdatavalid cpu_data_master_read_data_valid_pio_switch_s1, which is an e_mux
  assign cpu_data_master_read_data_valid_pio_switch_s1 = cpu_data_master_granted_pio_switch_s1 & cpu_data_master_read & ~pio_switch_s1_waits_for_read;

  //master is always granted when requested
  assign cpu_data_master_granted_pio_switch_s1 = cpu_data_master_qualified_request_pio_switch_s1;

  //cpu/data_master saved-grant pio_switch/s1, which is an e_assign
  assign cpu_data_master_saved_grant_pio_switch_s1 = cpu_data_master_requests_pio_switch_s1;

  //allow new arb cycle for pio_switch/s1, which is an e_assign
  assign pio_switch_s1_allow_new_arb_cycle = 1;

  //placeholder chosen master
  assign pio_switch_s1_grant_vector = 1;

  //placeholder vector of master qualified-requests
  assign pio_switch_s1_master_qreq_vector = 1;

  //pio_switch_s1_reset_n assignment, which is an e_assign
  assign pio_switch_s1_reset_n = reset_n;

  //pio_switch_s1_firsttransfer first transaction, which is an e_assign
  assign pio_switch_s1_firsttransfer = pio_switch_s1_begins_xfer ? pio_switch_s1_unreg_firsttransfer : pio_switch_s1_reg_firsttransfer;

  //pio_switch_s1_unreg_firsttransfer first transaction, which is an e_assign
  assign pio_switch_s1_unreg_firsttransfer = ~(pio_switch_s1_slavearbiterlockenable & pio_switch_s1_any_continuerequest);

  //pio_switch_s1_reg_firsttransfer first transaction, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          pio_switch_s1_reg_firsttransfer <= 1'b1;
      else if (pio_switch_s1_begins_xfer)
          pio_switch_s1_reg_firsttransfer <= pio_switch_s1_unreg_firsttransfer;
    end


  //pio_switch_s1_beginbursttransfer_internal begin burst transfer, which is an e_assign
  assign pio_switch_s1_beginbursttransfer_internal = pio_switch_s1_begins_xfer;

  assign shifted_address_to_pio_switch_s1_from_cpu_data_master = cpu_data_master_address_to_slave;
  //pio_switch_s1_address mux, which is an e_mux
  assign pio_switch_s1_address = shifted_address_to_pio_switch_s1_from_cpu_data_master >> 2;

  //d1_pio_switch_s1_end_xfer register, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          d1_pio_switch_s1_end_xfer <= 1;
      else 
        d1_pio_switch_s1_end_xfer <= pio_switch_s1_end_xfer;
    end


  //pio_switch_s1_waits_for_read in a cycle, which is an e_mux
  assign pio_switch_s1_waits_for_read = pio_switch_s1_in_a_read_cycle & pio_switch_s1_begins_xfer;

  //pio_switch_s1_in_a_read_cycle assignment, which is an e_assign
  assign pio_switch_s1_in_a_read_cycle = cpu_data_master_granted_pio_switch_s1 & cpu_data_master_read;

  //in_a_read_cycle assignment, which is an e_mux
  assign in_a_read_cycle = pio_switch_s1_in_a_read_cycle;

  //pio_switch_s1_waits_for_write in a cycle, which is an e_mux
  assign pio_switch_s1_waits_for_write = pio_switch_s1_in_a_write_cycle & 0;

  //pio_switch_s1_in_a_write_cycle assignment, which is an e_assign
  assign pio_switch_s1_in_a_write_cycle = cpu_data_master_granted_pio_switch_s1 & cpu_data_master_write;

  //in_a_write_cycle assignment, which is an e_mux
  assign in_a_write_cycle = pio_switch_s1_in_a_write_cycle;

  assign wait_for_pio_switch_s1_counter = 0;

//synthesis translate_off
//////////////// SIMULATION-ONLY CONTENTS
  //pio_switch/s1 enable non-zero assertions, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          enable_nonzero_assertions <= 0;
      else 
        enable_nonzero_assertions <= 1'b1;
    end



//////////////// END SIMULATION-ONLY CONTENTS

//synthesis translate_on

endmodule


// synthesis translate_off
`timescale 1ns / 1ps
// synthesis translate_on

// turn off superfluous verilog processor warnings 
// altera message_level Level1 
// altera message_off 10034 10035 10036 10037 10230 10240 10030 

module pll_s1_arbitrator (
                           // inputs:
                            DE2_70_SOPC_clock_0_out_address_to_slave,
                            DE2_70_SOPC_clock_0_out_nativeaddress,
                            DE2_70_SOPC_clock_0_out_read,
                            DE2_70_SOPC_clock_0_out_write,
                            DE2_70_SOPC_clock_0_out_writedata,
                            clk,
                            pll_s1_readdata,
                            pll_s1_resetrequest,
                            reset_n,

                           // outputs:
                            DE2_70_SOPC_clock_0_out_granted_pll_s1,
                            DE2_70_SOPC_clock_0_out_qualified_request_pll_s1,
                            DE2_70_SOPC_clock_0_out_read_data_valid_pll_s1,
                            DE2_70_SOPC_clock_0_out_requests_pll_s1,
                            d1_pll_s1_end_xfer,
                            pll_s1_address,
                            pll_s1_chipselect,
                            pll_s1_read,
                            pll_s1_readdata_from_sa,
                            pll_s1_reset_n,
                            pll_s1_resetrequest_from_sa,
                            pll_s1_write,
                            pll_s1_writedata
                         )
;

  output           DE2_70_SOPC_clock_0_out_granted_pll_s1;
  output           DE2_70_SOPC_clock_0_out_qualified_request_pll_s1;
  output           DE2_70_SOPC_clock_0_out_read_data_valid_pll_s1;
  output           DE2_70_SOPC_clock_0_out_requests_pll_s1;
  output           d1_pll_s1_end_xfer;
  output  [  2: 0] pll_s1_address;
  output           pll_s1_chipselect;
  output           pll_s1_read;
  output  [ 15: 0] pll_s1_readdata_from_sa;
  output           pll_s1_reset_n;
  output           pll_s1_resetrequest_from_sa;
  output           pll_s1_write;
  output  [ 15: 0] pll_s1_writedata;
  input   [  3: 0] DE2_70_SOPC_clock_0_out_address_to_slave;
  input   [  2: 0] DE2_70_SOPC_clock_0_out_nativeaddress;
  input            DE2_70_SOPC_clock_0_out_read;
  input            DE2_70_SOPC_clock_0_out_write;
  input   [ 15: 0] DE2_70_SOPC_clock_0_out_writedata;
  input            clk;
  input   [ 15: 0] pll_s1_readdata;
  input            pll_s1_resetrequest;
  input            reset_n;

  wire             DE2_70_SOPC_clock_0_out_arbiterlock;
  wire             DE2_70_SOPC_clock_0_out_arbiterlock2;
  wire             DE2_70_SOPC_clock_0_out_continuerequest;
  wire             DE2_70_SOPC_clock_0_out_granted_pll_s1;
  wire             DE2_70_SOPC_clock_0_out_qualified_request_pll_s1;
  wire             DE2_70_SOPC_clock_0_out_read_data_valid_pll_s1;
  wire             DE2_70_SOPC_clock_0_out_requests_pll_s1;
  wire             DE2_70_SOPC_clock_0_out_saved_grant_pll_s1;
  reg              d1_pll_s1_end_xfer;
  reg              d1_reasons_to_wait;
  reg              enable_nonzero_assertions;
  wire             end_xfer_arb_share_counter_term_pll_s1;
  wire             in_a_read_cycle;
  wire             in_a_write_cycle;
  wire    [  2: 0] pll_s1_address;
  wire             pll_s1_allgrants;
  wire             pll_s1_allow_new_arb_cycle;
  wire             pll_s1_any_bursting_master_saved_grant;
  wire             pll_s1_any_continuerequest;
  wire             pll_s1_arb_counter_enable;
  reg              pll_s1_arb_share_counter;
  wire             pll_s1_arb_share_counter_next_value;
  wire             pll_s1_arb_share_set_values;
  wire             pll_s1_beginbursttransfer_internal;
  wire             pll_s1_begins_xfer;
  wire             pll_s1_chipselect;
  wire             pll_s1_end_xfer;
  wire             pll_s1_firsttransfer;
  wire             pll_s1_grant_vector;
  wire             pll_s1_in_a_read_cycle;
  wire             pll_s1_in_a_write_cycle;
  wire             pll_s1_master_qreq_vector;
  wire             pll_s1_non_bursting_master_requests;
  wire             pll_s1_read;
  wire    [ 15: 0] pll_s1_readdata_from_sa;
  reg              pll_s1_reg_firsttransfer;
  wire             pll_s1_reset_n;
  wire             pll_s1_resetrequest_from_sa;
  reg              pll_s1_slavearbiterlockenable;
  wire             pll_s1_slavearbiterlockenable2;
  wire             pll_s1_unreg_firsttransfer;
  wire             pll_s1_waits_for_read;
  wire             pll_s1_waits_for_write;
  wire             pll_s1_write;
  wire    [ 15: 0] pll_s1_writedata;
  wire             wait_for_pll_s1_counter;
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          d1_reasons_to_wait <= 0;
      else 
        d1_reasons_to_wait <= ~pll_s1_end_xfer;
    end


  assign pll_s1_begins_xfer = ~d1_reasons_to_wait & ((DE2_70_SOPC_clock_0_out_qualified_request_pll_s1));
  //assign pll_s1_readdata_from_sa = pll_s1_readdata so that symbol knows where to group signals which may go to master only, which is an e_assign
  assign pll_s1_readdata_from_sa = pll_s1_readdata;

  assign DE2_70_SOPC_clock_0_out_requests_pll_s1 = (1) & (DE2_70_SOPC_clock_0_out_read | DE2_70_SOPC_clock_0_out_write);
  //pll_s1_arb_share_counter set values, which is an e_mux
  assign pll_s1_arb_share_set_values = 1;

  //pll_s1_non_bursting_master_requests mux, which is an e_mux
  assign pll_s1_non_bursting_master_requests = DE2_70_SOPC_clock_0_out_requests_pll_s1;

  //pll_s1_any_bursting_master_saved_grant mux, which is an e_mux
  assign pll_s1_any_bursting_master_saved_grant = 0;

  //pll_s1_arb_share_counter_next_value assignment, which is an e_assign
  assign pll_s1_arb_share_counter_next_value = pll_s1_firsttransfer ? (pll_s1_arb_share_set_values - 1) : |pll_s1_arb_share_counter ? (pll_s1_arb_share_counter - 1) : 0;

  //pll_s1_allgrants all slave grants, which is an e_mux
  assign pll_s1_allgrants = |pll_s1_grant_vector;

  //pll_s1_end_xfer assignment, which is an e_assign
  assign pll_s1_end_xfer = ~(pll_s1_waits_for_read | pll_s1_waits_for_write);

  //end_xfer_arb_share_counter_term_pll_s1 arb share counter enable term, which is an e_assign
  assign end_xfer_arb_share_counter_term_pll_s1 = pll_s1_end_xfer & (~pll_s1_any_bursting_master_saved_grant | in_a_read_cycle | in_a_write_cycle);

  //pll_s1_arb_share_counter arbitration counter enable, which is an e_assign
  assign pll_s1_arb_counter_enable = (end_xfer_arb_share_counter_term_pll_s1 & pll_s1_allgrants) | (end_xfer_arb_share_counter_term_pll_s1 & ~pll_s1_non_bursting_master_requests);

  //pll_s1_arb_share_counter counter, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          pll_s1_arb_share_counter <= 0;
      else if (pll_s1_arb_counter_enable)
          pll_s1_arb_share_counter <= pll_s1_arb_share_counter_next_value;
    end


  //pll_s1_slavearbiterlockenable slave enables arbiterlock, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          pll_s1_slavearbiterlockenable <= 0;
      else if ((|pll_s1_master_qreq_vector & end_xfer_arb_share_counter_term_pll_s1) | (end_xfer_arb_share_counter_term_pll_s1 & ~pll_s1_non_bursting_master_requests))
          pll_s1_slavearbiterlockenable <= |pll_s1_arb_share_counter_next_value;
    end


  //DE2_70_SOPC_clock_0/out pll/s1 arbiterlock, which is an e_assign
  assign DE2_70_SOPC_clock_0_out_arbiterlock = pll_s1_slavearbiterlockenable & DE2_70_SOPC_clock_0_out_continuerequest;

  //pll_s1_slavearbiterlockenable2 slave enables arbiterlock2, which is an e_assign
  assign pll_s1_slavearbiterlockenable2 = |pll_s1_arb_share_counter_next_value;

  //DE2_70_SOPC_clock_0/out pll/s1 arbiterlock2, which is an e_assign
  assign DE2_70_SOPC_clock_0_out_arbiterlock2 = pll_s1_slavearbiterlockenable2 & DE2_70_SOPC_clock_0_out_continuerequest;

  //pll_s1_any_continuerequest at least one master continues requesting, which is an e_assign
  assign pll_s1_any_continuerequest = 1;

  //DE2_70_SOPC_clock_0_out_continuerequest continued request, which is an e_assign
  assign DE2_70_SOPC_clock_0_out_continuerequest = 1;

  assign DE2_70_SOPC_clock_0_out_qualified_request_pll_s1 = DE2_70_SOPC_clock_0_out_requests_pll_s1;
  //pll_s1_writedata mux, which is an e_mux
  assign pll_s1_writedata = DE2_70_SOPC_clock_0_out_writedata;

  //master is always granted when requested
  assign DE2_70_SOPC_clock_0_out_granted_pll_s1 = DE2_70_SOPC_clock_0_out_qualified_request_pll_s1;

  //DE2_70_SOPC_clock_0/out saved-grant pll/s1, which is an e_assign
  assign DE2_70_SOPC_clock_0_out_saved_grant_pll_s1 = DE2_70_SOPC_clock_0_out_requests_pll_s1;

  //allow new arb cycle for pll/s1, which is an e_assign
  assign pll_s1_allow_new_arb_cycle = 1;

  //placeholder chosen master
  assign pll_s1_grant_vector = 1;

  //placeholder vector of master qualified-requests
  assign pll_s1_master_qreq_vector = 1;

  //pll_s1_reset_n assignment, which is an e_assign
  assign pll_s1_reset_n = reset_n;

  //assign pll_s1_resetrequest_from_sa = pll_s1_resetrequest so that symbol knows where to group signals which may go to master only, which is an e_assign
  assign pll_s1_resetrequest_from_sa = pll_s1_resetrequest;

  assign pll_s1_chipselect = DE2_70_SOPC_clock_0_out_granted_pll_s1;
  //pll_s1_firsttransfer first transaction, which is an e_assign
  assign pll_s1_firsttransfer = pll_s1_begins_xfer ? pll_s1_unreg_firsttransfer : pll_s1_reg_firsttransfer;

  //pll_s1_unreg_firsttransfer first transaction, which is an e_assign
  assign pll_s1_unreg_firsttransfer = ~(pll_s1_slavearbiterlockenable & pll_s1_any_continuerequest);

  //pll_s1_reg_firsttransfer first transaction, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          pll_s1_reg_firsttransfer <= 1'b1;
      else if (pll_s1_begins_xfer)
          pll_s1_reg_firsttransfer <= pll_s1_unreg_firsttransfer;
    end


  //pll_s1_beginbursttransfer_internal begin burst transfer, which is an e_assign
  assign pll_s1_beginbursttransfer_internal = pll_s1_begins_xfer;

  //pll_s1_read assignment, which is an e_mux
  assign pll_s1_read = DE2_70_SOPC_clock_0_out_granted_pll_s1 & DE2_70_SOPC_clock_0_out_read;

  //pll_s1_write assignment, which is an e_mux
  assign pll_s1_write = DE2_70_SOPC_clock_0_out_granted_pll_s1 & DE2_70_SOPC_clock_0_out_write;

  //pll_s1_address mux, which is an e_mux
  assign pll_s1_address = DE2_70_SOPC_clock_0_out_nativeaddress;

  //d1_pll_s1_end_xfer register, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          d1_pll_s1_end_xfer <= 1;
      else 
        d1_pll_s1_end_xfer <= pll_s1_end_xfer;
    end


  //pll_s1_waits_for_read in a cycle, which is an e_mux
  assign pll_s1_waits_for_read = pll_s1_in_a_read_cycle & pll_s1_begins_xfer;

  //pll_s1_in_a_read_cycle assignment, which is an e_assign
  assign pll_s1_in_a_read_cycle = DE2_70_SOPC_clock_0_out_granted_pll_s1 & DE2_70_SOPC_clock_0_out_read;

  //in_a_read_cycle assignment, which is an e_mux
  assign in_a_read_cycle = pll_s1_in_a_read_cycle;

  //pll_s1_waits_for_write in a cycle, which is an e_mux
  assign pll_s1_waits_for_write = pll_s1_in_a_write_cycle & 0;

  //pll_s1_in_a_write_cycle assignment, which is an e_assign
  assign pll_s1_in_a_write_cycle = DE2_70_SOPC_clock_0_out_granted_pll_s1 & DE2_70_SOPC_clock_0_out_write;

  //in_a_write_cycle assignment, which is an e_mux
  assign in_a_write_cycle = pll_s1_in_a_write_cycle;

  assign wait_for_pll_s1_counter = 0;

//synthesis translate_off
//////////////// SIMULATION-ONLY CONTENTS
  //pll/s1 enable non-zero assertions, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          enable_nonzero_assertions <= 0;
      else 
        enable_nonzero_assertions <= 1'b1;
    end



//////////////// END SIMULATION-ONLY CONTENTS

//synthesis translate_on

endmodule


// synthesis translate_off
`timescale 1ns / 1ps
// synthesis translate_on

// turn off superfluous verilog processor warnings 
// altera message_level Level1 
// altera message_off 10034 10035 10036 10037 10230 10240 10030 

module sysid_control_slave_arbitrator (
                                        // inputs:
                                         clk,
                                         cpu_data_master_address_to_slave,
                                         cpu_data_master_latency_counter,
                                         cpu_data_master_read,
                                         cpu_data_master_write,
                                         reset_n,
                                         sysid_control_slave_readdata,

                                        // outputs:
                                         cpu_data_master_granted_sysid_control_slave,
                                         cpu_data_master_qualified_request_sysid_control_slave,
                                         cpu_data_master_read_data_valid_sysid_control_slave,
                                         cpu_data_master_requests_sysid_control_slave,
                                         d1_sysid_control_slave_end_xfer,
                                         sysid_control_slave_address,
                                         sysid_control_slave_readdata_from_sa,
                                         sysid_control_slave_reset_n
                                      )
;

  output           cpu_data_master_granted_sysid_control_slave;
  output           cpu_data_master_qualified_request_sysid_control_slave;
  output           cpu_data_master_read_data_valid_sysid_control_slave;
  output           cpu_data_master_requests_sysid_control_slave;
  output           d1_sysid_control_slave_end_xfer;
  output           sysid_control_slave_address;
  output  [ 31: 0] sysid_control_slave_readdata_from_sa;
  output           sysid_control_slave_reset_n;
  input            clk;
  input   [ 24: 0] cpu_data_master_address_to_slave;
  input   [  2: 0] cpu_data_master_latency_counter;
  input            cpu_data_master_read;
  input            cpu_data_master_write;
  input            reset_n;
  input   [ 31: 0] sysid_control_slave_readdata;

  wire             cpu_data_master_arbiterlock;
  wire             cpu_data_master_arbiterlock2;
  wire             cpu_data_master_continuerequest;
  wire             cpu_data_master_granted_sysid_control_slave;
  wire             cpu_data_master_qualified_request_sysid_control_slave;
  wire             cpu_data_master_read_data_valid_sysid_control_slave;
  wire             cpu_data_master_requests_sysid_control_slave;
  wire             cpu_data_master_saved_grant_sysid_control_slave;
  reg              d1_reasons_to_wait;
  reg              d1_sysid_control_slave_end_xfer;
  reg              enable_nonzero_assertions;
  wire             end_xfer_arb_share_counter_term_sysid_control_slave;
  wire             in_a_read_cycle;
  wire             in_a_write_cycle;
  wire    [ 24: 0] shifted_address_to_sysid_control_slave_from_cpu_data_master;
  wire             sysid_control_slave_address;
  wire             sysid_control_slave_allgrants;
  wire             sysid_control_slave_allow_new_arb_cycle;
  wire             sysid_control_slave_any_bursting_master_saved_grant;
  wire             sysid_control_slave_any_continuerequest;
  wire             sysid_control_slave_arb_counter_enable;
  reg              sysid_control_slave_arb_share_counter;
  wire             sysid_control_slave_arb_share_counter_next_value;
  wire             sysid_control_slave_arb_share_set_values;
  wire             sysid_control_slave_beginbursttransfer_internal;
  wire             sysid_control_slave_begins_xfer;
  wire             sysid_control_slave_end_xfer;
  wire             sysid_control_slave_firsttransfer;
  wire             sysid_control_slave_grant_vector;
  wire             sysid_control_slave_in_a_read_cycle;
  wire             sysid_control_slave_in_a_write_cycle;
  wire             sysid_control_slave_master_qreq_vector;
  wire             sysid_control_slave_non_bursting_master_requests;
  wire    [ 31: 0] sysid_control_slave_readdata_from_sa;
  reg              sysid_control_slave_reg_firsttransfer;
  wire             sysid_control_slave_reset_n;
  reg              sysid_control_slave_slavearbiterlockenable;
  wire             sysid_control_slave_slavearbiterlockenable2;
  wire             sysid_control_slave_unreg_firsttransfer;
  wire             sysid_control_slave_waits_for_read;
  wire             sysid_control_slave_waits_for_write;
  wire             wait_for_sysid_control_slave_counter;
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          d1_reasons_to_wait <= 0;
      else 
        d1_reasons_to_wait <= ~sysid_control_slave_end_xfer;
    end


  assign sysid_control_slave_begins_xfer = ~d1_reasons_to_wait & ((cpu_data_master_qualified_request_sysid_control_slave));
  //assign sysid_control_slave_readdata_from_sa = sysid_control_slave_readdata so that symbol knows where to group signals which may go to master only, which is an e_assign
  assign sysid_control_slave_readdata_from_sa = sysid_control_slave_readdata;

  assign cpu_data_master_requests_sysid_control_slave = (({cpu_data_master_address_to_slave[24 : 3] , 3'b0} == 25'h1409110) & (cpu_data_master_read | cpu_data_master_write)) & cpu_data_master_read;
  //sysid_control_slave_arb_share_counter set values, which is an e_mux
  assign sysid_control_slave_arb_share_set_values = 1;

  //sysid_control_slave_non_bursting_master_requests mux, which is an e_mux
  assign sysid_control_slave_non_bursting_master_requests = cpu_data_master_requests_sysid_control_slave;

  //sysid_control_slave_any_bursting_master_saved_grant mux, which is an e_mux
  assign sysid_control_slave_any_bursting_master_saved_grant = 0;

  //sysid_control_slave_arb_share_counter_next_value assignment, which is an e_assign
  assign sysid_control_slave_arb_share_counter_next_value = sysid_control_slave_firsttransfer ? (sysid_control_slave_arb_share_set_values - 1) : |sysid_control_slave_arb_share_counter ? (sysid_control_slave_arb_share_counter - 1) : 0;

  //sysid_control_slave_allgrants all slave grants, which is an e_mux
  assign sysid_control_slave_allgrants = |sysid_control_slave_grant_vector;

  //sysid_control_slave_end_xfer assignment, which is an e_assign
  assign sysid_control_slave_end_xfer = ~(sysid_control_slave_waits_for_read | sysid_control_slave_waits_for_write);

  //end_xfer_arb_share_counter_term_sysid_control_slave arb share counter enable term, which is an e_assign
  assign end_xfer_arb_share_counter_term_sysid_control_slave = sysid_control_slave_end_xfer & (~sysid_control_slave_any_bursting_master_saved_grant | in_a_read_cycle | in_a_write_cycle);

  //sysid_control_slave_arb_share_counter arbitration counter enable, which is an e_assign
  assign sysid_control_slave_arb_counter_enable = (end_xfer_arb_share_counter_term_sysid_control_slave & sysid_control_slave_allgrants) | (end_xfer_arb_share_counter_term_sysid_control_slave & ~sysid_control_slave_non_bursting_master_requests);

  //sysid_control_slave_arb_share_counter counter, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          sysid_control_slave_arb_share_counter <= 0;
      else if (sysid_control_slave_arb_counter_enable)
          sysid_control_slave_arb_share_counter <= sysid_control_slave_arb_share_counter_next_value;
    end


  //sysid_control_slave_slavearbiterlockenable slave enables arbiterlock, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          sysid_control_slave_slavearbiterlockenable <= 0;
      else if ((|sysid_control_slave_master_qreq_vector & end_xfer_arb_share_counter_term_sysid_control_slave) | (end_xfer_arb_share_counter_term_sysid_control_slave & ~sysid_control_slave_non_bursting_master_requests))
          sysid_control_slave_slavearbiterlockenable <= |sysid_control_slave_arb_share_counter_next_value;
    end


  //cpu/data_master sysid/control_slave arbiterlock, which is an e_assign
  assign cpu_data_master_arbiterlock = sysid_control_slave_slavearbiterlockenable & cpu_data_master_continuerequest;

  //sysid_control_slave_slavearbiterlockenable2 slave enables arbiterlock2, which is an e_assign
  assign sysid_control_slave_slavearbiterlockenable2 = |sysid_control_slave_arb_share_counter_next_value;

  //cpu/data_master sysid/control_slave arbiterlock2, which is an e_assign
  assign cpu_data_master_arbiterlock2 = sysid_control_slave_slavearbiterlockenable2 & cpu_data_master_continuerequest;

  //sysid_control_slave_any_continuerequest at least one master continues requesting, which is an e_assign
  assign sysid_control_slave_any_continuerequest = 1;

  //cpu_data_master_continuerequest continued request, which is an e_assign
  assign cpu_data_master_continuerequest = 1;

  assign cpu_data_master_qualified_request_sysid_control_slave = cpu_data_master_requests_sysid_control_slave & ~((cpu_data_master_read & ((cpu_data_master_latency_counter != 0))));
  //local readdatavalid cpu_data_master_read_data_valid_sysid_control_slave, which is an e_mux
  assign cpu_data_master_read_data_valid_sysid_control_slave = cpu_data_master_granted_sysid_control_slave & cpu_data_master_read & ~sysid_control_slave_waits_for_read;

  //master is always granted when requested
  assign cpu_data_master_granted_sysid_control_slave = cpu_data_master_qualified_request_sysid_control_slave;

  //cpu/data_master saved-grant sysid/control_slave, which is an e_assign
  assign cpu_data_master_saved_grant_sysid_control_slave = cpu_data_master_requests_sysid_control_slave;

  //allow new arb cycle for sysid/control_slave, which is an e_assign
  assign sysid_control_slave_allow_new_arb_cycle = 1;

  //placeholder chosen master
  assign sysid_control_slave_grant_vector = 1;

  //placeholder vector of master qualified-requests
  assign sysid_control_slave_master_qreq_vector = 1;

  //sysid_control_slave_reset_n assignment, which is an e_assign
  assign sysid_control_slave_reset_n = reset_n;

  //sysid_control_slave_firsttransfer first transaction, which is an e_assign
  assign sysid_control_slave_firsttransfer = sysid_control_slave_begins_xfer ? sysid_control_slave_unreg_firsttransfer : sysid_control_slave_reg_firsttransfer;

  //sysid_control_slave_unreg_firsttransfer first transaction, which is an e_assign
  assign sysid_control_slave_unreg_firsttransfer = ~(sysid_control_slave_slavearbiterlockenable & sysid_control_slave_any_continuerequest);

  //sysid_control_slave_reg_firsttransfer first transaction, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          sysid_control_slave_reg_firsttransfer <= 1'b1;
      else if (sysid_control_slave_begins_xfer)
          sysid_control_slave_reg_firsttransfer <= sysid_control_slave_unreg_firsttransfer;
    end


  //sysid_control_slave_beginbursttransfer_internal begin burst transfer, which is an e_assign
  assign sysid_control_slave_beginbursttransfer_internal = sysid_control_slave_begins_xfer;

  assign shifted_address_to_sysid_control_slave_from_cpu_data_master = cpu_data_master_address_to_slave;
  //sysid_control_slave_address mux, which is an e_mux
  assign sysid_control_slave_address = shifted_address_to_sysid_control_slave_from_cpu_data_master >> 2;

  //d1_sysid_control_slave_end_xfer register, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          d1_sysid_control_slave_end_xfer <= 1;
      else 
        d1_sysid_control_slave_end_xfer <= sysid_control_slave_end_xfer;
    end


  //sysid_control_slave_waits_for_read in a cycle, which is an e_mux
  assign sysid_control_slave_waits_for_read = sysid_control_slave_in_a_read_cycle & sysid_control_slave_begins_xfer;

  //sysid_control_slave_in_a_read_cycle assignment, which is an e_assign
  assign sysid_control_slave_in_a_read_cycle = cpu_data_master_granted_sysid_control_slave & cpu_data_master_read;

  //in_a_read_cycle assignment, which is an e_mux
  assign in_a_read_cycle = sysid_control_slave_in_a_read_cycle;

  //sysid_control_slave_waits_for_write in a cycle, which is an e_mux
  assign sysid_control_slave_waits_for_write = sysid_control_slave_in_a_write_cycle & 0;

  //sysid_control_slave_in_a_write_cycle assignment, which is an e_assign
  assign sysid_control_slave_in_a_write_cycle = cpu_data_master_granted_sysid_control_slave & cpu_data_master_write;

  //in_a_write_cycle assignment, which is an e_mux
  assign in_a_write_cycle = sysid_control_slave_in_a_write_cycle;

  assign wait_for_sysid_control_slave_counter = 0;

//synthesis translate_off
//////////////// SIMULATION-ONLY CONTENTS
  //sysid/control_slave enable non-zero assertions, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          enable_nonzero_assertions <= 0;
      else 
        enable_nonzero_assertions <= 1'b1;
    end



//////////////// END SIMULATION-ONLY CONTENTS

//synthesis translate_on

endmodule


// synthesis translate_off
`timescale 1ns / 1ps
// synthesis translate_on

// turn off superfluous verilog processor warnings 
// altera message_level Level1 
// altera message_off 10034 10035 10036 10037 10230 10240 10030 

module timer_s1_arbitrator (
                             // inputs:
                              clk,
                              cpu_data_master_address_to_slave,
                              cpu_data_master_latency_counter,
                              cpu_data_master_read,
                              cpu_data_master_write,
                              cpu_data_master_writedata,
                              reset_n,
                              timer_s1_irq,
                              timer_s1_readdata,

                             // outputs:
                              cpu_data_master_granted_timer_s1,
                              cpu_data_master_qualified_request_timer_s1,
                              cpu_data_master_read_data_valid_timer_s1,
                              cpu_data_master_requests_timer_s1,
                              d1_timer_s1_end_xfer,
                              timer_s1_address,
                              timer_s1_chipselect,
                              timer_s1_irq_from_sa,
                              timer_s1_readdata_from_sa,
                              timer_s1_reset_n,
                              timer_s1_write_n,
                              timer_s1_writedata
                           )
;

  output           cpu_data_master_granted_timer_s1;
  output           cpu_data_master_qualified_request_timer_s1;
  output           cpu_data_master_read_data_valid_timer_s1;
  output           cpu_data_master_requests_timer_s1;
  output           d1_timer_s1_end_xfer;
  output  [  2: 0] timer_s1_address;
  output           timer_s1_chipselect;
  output           timer_s1_irq_from_sa;
  output  [ 15: 0] timer_s1_readdata_from_sa;
  output           timer_s1_reset_n;
  output           timer_s1_write_n;
  output  [ 15: 0] timer_s1_writedata;
  input            clk;
  input   [ 24: 0] cpu_data_master_address_to_slave;
  input   [  2: 0] cpu_data_master_latency_counter;
  input            cpu_data_master_read;
  input            cpu_data_master_write;
  input   [ 31: 0] cpu_data_master_writedata;
  input            reset_n;
  input            timer_s1_irq;
  input   [ 15: 0] timer_s1_readdata;

  wire             cpu_data_master_arbiterlock;
  wire             cpu_data_master_arbiterlock2;
  wire             cpu_data_master_continuerequest;
  wire             cpu_data_master_granted_timer_s1;
  wire             cpu_data_master_qualified_request_timer_s1;
  wire             cpu_data_master_read_data_valid_timer_s1;
  wire             cpu_data_master_requests_timer_s1;
  wire             cpu_data_master_saved_grant_timer_s1;
  reg              d1_reasons_to_wait;
  reg              d1_timer_s1_end_xfer;
  reg              enable_nonzero_assertions;
  wire             end_xfer_arb_share_counter_term_timer_s1;
  wire             in_a_read_cycle;
  wire             in_a_write_cycle;
  wire    [ 24: 0] shifted_address_to_timer_s1_from_cpu_data_master;
  wire    [  2: 0] timer_s1_address;
  wire             timer_s1_allgrants;
  wire             timer_s1_allow_new_arb_cycle;
  wire             timer_s1_any_bursting_master_saved_grant;
  wire             timer_s1_any_continuerequest;
  wire             timer_s1_arb_counter_enable;
  reg              timer_s1_arb_share_counter;
  wire             timer_s1_arb_share_counter_next_value;
  wire             timer_s1_arb_share_set_values;
  wire             timer_s1_beginbursttransfer_internal;
  wire             timer_s1_begins_xfer;
  wire             timer_s1_chipselect;
  wire             timer_s1_end_xfer;
  wire             timer_s1_firsttransfer;
  wire             timer_s1_grant_vector;
  wire             timer_s1_in_a_read_cycle;
  wire             timer_s1_in_a_write_cycle;
  wire             timer_s1_irq_from_sa;
  wire             timer_s1_master_qreq_vector;
  wire             timer_s1_non_bursting_master_requests;
  wire    [ 15: 0] timer_s1_readdata_from_sa;
  reg              timer_s1_reg_firsttransfer;
  wire             timer_s1_reset_n;
  reg              timer_s1_slavearbiterlockenable;
  wire             timer_s1_slavearbiterlockenable2;
  wire             timer_s1_unreg_firsttransfer;
  wire             timer_s1_waits_for_read;
  wire             timer_s1_waits_for_write;
  wire             timer_s1_write_n;
  wire    [ 15: 0] timer_s1_writedata;
  wire             wait_for_timer_s1_counter;
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          d1_reasons_to_wait <= 0;
      else 
        d1_reasons_to_wait <= ~timer_s1_end_xfer;
    end


  assign timer_s1_begins_xfer = ~d1_reasons_to_wait & ((cpu_data_master_qualified_request_timer_s1));
  //assign timer_s1_readdata_from_sa = timer_s1_readdata so that symbol knows where to group signals which may go to master only, which is an e_assign
  assign timer_s1_readdata_from_sa = timer_s1_readdata;

  assign cpu_data_master_requests_timer_s1 = ({cpu_data_master_address_to_slave[24 : 5] , 5'b0} == 25'h1409000) & (cpu_data_master_read | cpu_data_master_write);
  //timer_s1_arb_share_counter set values, which is an e_mux
  assign timer_s1_arb_share_set_values = 1;

  //timer_s1_non_bursting_master_requests mux, which is an e_mux
  assign timer_s1_non_bursting_master_requests = cpu_data_master_requests_timer_s1;

  //timer_s1_any_bursting_master_saved_grant mux, which is an e_mux
  assign timer_s1_any_bursting_master_saved_grant = 0;

  //timer_s1_arb_share_counter_next_value assignment, which is an e_assign
  assign timer_s1_arb_share_counter_next_value = timer_s1_firsttransfer ? (timer_s1_arb_share_set_values - 1) : |timer_s1_arb_share_counter ? (timer_s1_arb_share_counter - 1) : 0;

  //timer_s1_allgrants all slave grants, which is an e_mux
  assign timer_s1_allgrants = |timer_s1_grant_vector;

  //timer_s1_end_xfer assignment, which is an e_assign
  assign timer_s1_end_xfer = ~(timer_s1_waits_for_read | timer_s1_waits_for_write);

  //end_xfer_arb_share_counter_term_timer_s1 arb share counter enable term, which is an e_assign
  assign end_xfer_arb_share_counter_term_timer_s1 = timer_s1_end_xfer & (~timer_s1_any_bursting_master_saved_grant | in_a_read_cycle | in_a_write_cycle);

  //timer_s1_arb_share_counter arbitration counter enable, which is an e_assign
  assign timer_s1_arb_counter_enable = (end_xfer_arb_share_counter_term_timer_s1 & timer_s1_allgrants) | (end_xfer_arb_share_counter_term_timer_s1 & ~timer_s1_non_bursting_master_requests);

  //timer_s1_arb_share_counter counter, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          timer_s1_arb_share_counter <= 0;
      else if (timer_s1_arb_counter_enable)
          timer_s1_arb_share_counter <= timer_s1_arb_share_counter_next_value;
    end


  //timer_s1_slavearbiterlockenable slave enables arbiterlock, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          timer_s1_slavearbiterlockenable <= 0;
      else if ((|timer_s1_master_qreq_vector & end_xfer_arb_share_counter_term_timer_s1) | (end_xfer_arb_share_counter_term_timer_s1 & ~timer_s1_non_bursting_master_requests))
          timer_s1_slavearbiterlockenable <= |timer_s1_arb_share_counter_next_value;
    end


  //cpu/data_master timer/s1 arbiterlock, which is an e_assign
  assign cpu_data_master_arbiterlock = timer_s1_slavearbiterlockenable & cpu_data_master_continuerequest;

  //timer_s1_slavearbiterlockenable2 slave enables arbiterlock2, which is an e_assign
  assign timer_s1_slavearbiterlockenable2 = |timer_s1_arb_share_counter_next_value;

  //cpu/data_master timer/s1 arbiterlock2, which is an e_assign
  assign cpu_data_master_arbiterlock2 = timer_s1_slavearbiterlockenable2 & cpu_data_master_continuerequest;

  //timer_s1_any_continuerequest at least one master continues requesting, which is an e_assign
  assign timer_s1_any_continuerequest = 1;

  //cpu_data_master_continuerequest continued request, which is an e_assign
  assign cpu_data_master_continuerequest = 1;

  assign cpu_data_master_qualified_request_timer_s1 = cpu_data_master_requests_timer_s1 & ~((cpu_data_master_read & ((cpu_data_master_latency_counter != 0))));
  //local readdatavalid cpu_data_master_read_data_valid_timer_s1, which is an e_mux
  assign cpu_data_master_read_data_valid_timer_s1 = cpu_data_master_granted_timer_s1 & cpu_data_master_read & ~timer_s1_waits_for_read;

  //timer_s1_writedata mux, which is an e_mux
  assign timer_s1_writedata = cpu_data_master_writedata;

  //master is always granted when requested
  assign cpu_data_master_granted_timer_s1 = cpu_data_master_qualified_request_timer_s1;

  //cpu/data_master saved-grant timer/s1, which is an e_assign
  assign cpu_data_master_saved_grant_timer_s1 = cpu_data_master_requests_timer_s1;

  //allow new arb cycle for timer/s1, which is an e_assign
  assign timer_s1_allow_new_arb_cycle = 1;

  //placeholder chosen master
  assign timer_s1_grant_vector = 1;

  //placeholder vector of master qualified-requests
  assign timer_s1_master_qreq_vector = 1;

  //timer_s1_reset_n assignment, which is an e_assign
  assign timer_s1_reset_n = reset_n;

  assign timer_s1_chipselect = cpu_data_master_granted_timer_s1;
  //timer_s1_firsttransfer first transaction, which is an e_assign
  assign timer_s1_firsttransfer = timer_s1_begins_xfer ? timer_s1_unreg_firsttransfer : timer_s1_reg_firsttransfer;

  //timer_s1_unreg_firsttransfer first transaction, which is an e_assign
  assign timer_s1_unreg_firsttransfer = ~(timer_s1_slavearbiterlockenable & timer_s1_any_continuerequest);

  //timer_s1_reg_firsttransfer first transaction, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          timer_s1_reg_firsttransfer <= 1'b1;
      else if (timer_s1_begins_xfer)
          timer_s1_reg_firsttransfer <= timer_s1_unreg_firsttransfer;
    end


  //timer_s1_beginbursttransfer_internal begin burst transfer, which is an e_assign
  assign timer_s1_beginbursttransfer_internal = timer_s1_begins_xfer;

  //~timer_s1_write_n assignment, which is an e_mux
  assign timer_s1_write_n = ~(cpu_data_master_granted_timer_s1 & cpu_data_master_write);

  assign shifted_address_to_timer_s1_from_cpu_data_master = cpu_data_master_address_to_slave;
  //timer_s1_address mux, which is an e_mux
  assign timer_s1_address = shifted_address_to_timer_s1_from_cpu_data_master >> 2;

  //d1_timer_s1_end_xfer register, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          d1_timer_s1_end_xfer <= 1;
      else 
        d1_timer_s1_end_xfer <= timer_s1_end_xfer;
    end


  //timer_s1_waits_for_read in a cycle, which is an e_mux
  assign timer_s1_waits_for_read = timer_s1_in_a_read_cycle & timer_s1_begins_xfer;

  //timer_s1_in_a_read_cycle assignment, which is an e_assign
  assign timer_s1_in_a_read_cycle = cpu_data_master_granted_timer_s1 & cpu_data_master_read;

  //in_a_read_cycle assignment, which is an e_mux
  assign in_a_read_cycle = timer_s1_in_a_read_cycle;

  //timer_s1_waits_for_write in a cycle, which is an e_mux
  assign timer_s1_waits_for_write = timer_s1_in_a_write_cycle & 0;

  //timer_s1_in_a_write_cycle assignment, which is an e_assign
  assign timer_s1_in_a_write_cycle = cpu_data_master_granted_timer_s1 & cpu_data_master_write;

  //in_a_write_cycle assignment, which is an e_mux
  assign in_a_write_cycle = timer_s1_in_a_write_cycle;

  assign wait_for_timer_s1_counter = 0;
  //assign timer_s1_irq_from_sa = timer_s1_irq so that symbol knows where to group signals which may go to master only, which is an e_assign
  assign timer_s1_irq_from_sa = timer_s1_irq;


//synthesis translate_off
//////////////// SIMULATION-ONLY CONTENTS
  //timer/s1 enable non-zero assertions, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          enable_nonzero_assertions <= 0;
      else 
        enable_nonzero_assertions <= 1'b1;
    end



//////////////// END SIMULATION-ONLY CONTENTS

//synthesis translate_on

endmodule


// synthesis translate_off
`timescale 1ns / 1ps
// synthesis translate_on

// turn off superfluous verilog processor warnings 
// altera message_level Level1 
// altera message_off 10034 10035 10036 10037 10230 10240 10030 

module timer_stamp_s1_arbitrator (
                                   // inputs:
                                    clk,
                                    cpu_data_master_address_to_slave,
                                    cpu_data_master_latency_counter,
                                    cpu_data_master_read,
                                    cpu_data_master_write,
                                    cpu_data_master_writedata,
                                    reset_n,
                                    timer_stamp_s1_irq,
                                    timer_stamp_s1_readdata,

                                   // outputs:
                                    cpu_data_master_granted_timer_stamp_s1,
                                    cpu_data_master_qualified_request_timer_stamp_s1,
                                    cpu_data_master_read_data_valid_timer_stamp_s1,
                                    cpu_data_master_requests_timer_stamp_s1,
                                    d1_timer_stamp_s1_end_xfer,
                                    timer_stamp_s1_address,
                                    timer_stamp_s1_chipselect,
                                    timer_stamp_s1_irq_from_sa,
                                    timer_stamp_s1_readdata_from_sa,
                                    timer_stamp_s1_reset_n,
                                    timer_stamp_s1_write_n,
                                    timer_stamp_s1_writedata
                                 )
;

  output           cpu_data_master_granted_timer_stamp_s1;
  output           cpu_data_master_qualified_request_timer_stamp_s1;
  output           cpu_data_master_read_data_valid_timer_stamp_s1;
  output           cpu_data_master_requests_timer_stamp_s1;
  output           d1_timer_stamp_s1_end_xfer;
  output  [  2: 0] timer_stamp_s1_address;
  output           timer_stamp_s1_chipselect;
  output           timer_stamp_s1_irq_from_sa;
  output  [ 15: 0] timer_stamp_s1_readdata_from_sa;
  output           timer_stamp_s1_reset_n;
  output           timer_stamp_s1_write_n;
  output  [ 15: 0] timer_stamp_s1_writedata;
  input            clk;
  input   [ 24: 0] cpu_data_master_address_to_slave;
  input   [  2: 0] cpu_data_master_latency_counter;
  input            cpu_data_master_read;
  input            cpu_data_master_write;
  input   [ 31: 0] cpu_data_master_writedata;
  input            reset_n;
  input            timer_stamp_s1_irq;
  input   [ 15: 0] timer_stamp_s1_readdata;

  wire             cpu_data_master_arbiterlock;
  wire             cpu_data_master_arbiterlock2;
  wire             cpu_data_master_continuerequest;
  wire             cpu_data_master_granted_timer_stamp_s1;
  wire             cpu_data_master_qualified_request_timer_stamp_s1;
  wire             cpu_data_master_read_data_valid_timer_stamp_s1;
  wire             cpu_data_master_requests_timer_stamp_s1;
  wire             cpu_data_master_saved_grant_timer_stamp_s1;
  reg              d1_reasons_to_wait;
  reg              d1_timer_stamp_s1_end_xfer;
  reg              enable_nonzero_assertions;
  wire             end_xfer_arb_share_counter_term_timer_stamp_s1;
  wire             in_a_read_cycle;
  wire             in_a_write_cycle;
  wire    [ 24: 0] shifted_address_to_timer_stamp_s1_from_cpu_data_master;
  wire    [  2: 0] timer_stamp_s1_address;
  wire             timer_stamp_s1_allgrants;
  wire             timer_stamp_s1_allow_new_arb_cycle;
  wire             timer_stamp_s1_any_bursting_master_saved_grant;
  wire             timer_stamp_s1_any_continuerequest;
  wire             timer_stamp_s1_arb_counter_enable;
  reg              timer_stamp_s1_arb_share_counter;
  wire             timer_stamp_s1_arb_share_counter_next_value;
  wire             timer_stamp_s1_arb_share_set_values;
  wire             timer_stamp_s1_beginbursttransfer_internal;
  wire             timer_stamp_s1_begins_xfer;
  wire             timer_stamp_s1_chipselect;
  wire             timer_stamp_s1_end_xfer;
  wire             timer_stamp_s1_firsttransfer;
  wire             timer_stamp_s1_grant_vector;
  wire             timer_stamp_s1_in_a_read_cycle;
  wire             timer_stamp_s1_in_a_write_cycle;
  wire             timer_stamp_s1_irq_from_sa;
  wire             timer_stamp_s1_master_qreq_vector;
  wire             timer_stamp_s1_non_bursting_master_requests;
  wire    [ 15: 0] timer_stamp_s1_readdata_from_sa;
  reg              timer_stamp_s1_reg_firsttransfer;
  wire             timer_stamp_s1_reset_n;
  reg              timer_stamp_s1_slavearbiterlockenable;
  wire             timer_stamp_s1_slavearbiterlockenable2;
  wire             timer_stamp_s1_unreg_firsttransfer;
  wire             timer_stamp_s1_waits_for_read;
  wire             timer_stamp_s1_waits_for_write;
  wire             timer_stamp_s1_write_n;
  wire    [ 15: 0] timer_stamp_s1_writedata;
  wire             wait_for_timer_stamp_s1_counter;
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          d1_reasons_to_wait <= 0;
      else 
        d1_reasons_to_wait <= ~timer_stamp_s1_end_xfer;
    end


  assign timer_stamp_s1_begins_xfer = ~d1_reasons_to_wait & ((cpu_data_master_qualified_request_timer_stamp_s1));
  //assign timer_stamp_s1_readdata_from_sa = timer_stamp_s1_readdata so that symbol knows where to group signals which may go to master only, which is an e_assign
  assign timer_stamp_s1_readdata_from_sa = timer_stamp_s1_readdata;

  assign cpu_data_master_requests_timer_stamp_s1 = ({cpu_data_master_address_to_slave[24 : 5] , 5'b0} == 25'h1409020) & (cpu_data_master_read | cpu_data_master_write);
  //timer_stamp_s1_arb_share_counter set values, which is an e_mux
  assign timer_stamp_s1_arb_share_set_values = 1;

  //timer_stamp_s1_non_bursting_master_requests mux, which is an e_mux
  assign timer_stamp_s1_non_bursting_master_requests = cpu_data_master_requests_timer_stamp_s1;

  //timer_stamp_s1_any_bursting_master_saved_grant mux, which is an e_mux
  assign timer_stamp_s1_any_bursting_master_saved_grant = 0;

  //timer_stamp_s1_arb_share_counter_next_value assignment, which is an e_assign
  assign timer_stamp_s1_arb_share_counter_next_value = timer_stamp_s1_firsttransfer ? (timer_stamp_s1_arb_share_set_values - 1) : |timer_stamp_s1_arb_share_counter ? (timer_stamp_s1_arb_share_counter - 1) : 0;

  //timer_stamp_s1_allgrants all slave grants, which is an e_mux
  assign timer_stamp_s1_allgrants = |timer_stamp_s1_grant_vector;

  //timer_stamp_s1_end_xfer assignment, which is an e_assign
  assign timer_stamp_s1_end_xfer = ~(timer_stamp_s1_waits_for_read | timer_stamp_s1_waits_for_write);

  //end_xfer_arb_share_counter_term_timer_stamp_s1 arb share counter enable term, which is an e_assign
  assign end_xfer_arb_share_counter_term_timer_stamp_s1 = timer_stamp_s1_end_xfer & (~timer_stamp_s1_any_bursting_master_saved_grant | in_a_read_cycle | in_a_write_cycle);

  //timer_stamp_s1_arb_share_counter arbitration counter enable, which is an e_assign
  assign timer_stamp_s1_arb_counter_enable = (end_xfer_arb_share_counter_term_timer_stamp_s1 & timer_stamp_s1_allgrants) | (end_xfer_arb_share_counter_term_timer_stamp_s1 & ~timer_stamp_s1_non_bursting_master_requests);

  //timer_stamp_s1_arb_share_counter counter, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          timer_stamp_s1_arb_share_counter <= 0;
      else if (timer_stamp_s1_arb_counter_enable)
          timer_stamp_s1_arb_share_counter <= timer_stamp_s1_arb_share_counter_next_value;
    end


  //timer_stamp_s1_slavearbiterlockenable slave enables arbiterlock, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          timer_stamp_s1_slavearbiterlockenable <= 0;
      else if ((|timer_stamp_s1_master_qreq_vector & end_xfer_arb_share_counter_term_timer_stamp_s1) | (end_xfer_arb_share_counter_term_timer_stamp_s1 & ~timer_stamp_s1_non_bursting_master_requests))
          timer_stamp_s1_slavearbiterlockenable <= |timer_stamp_s1_arb_share_counter_next_value;
    end


  //cpu/data_master timer_stamp/s1 arbiterlock, which is an e_assign
  assign cpu_data_master_arbiterlock = timer_stamp_s1_slavearbiterlockenable & cpu_data_master_continuerequest;

  //timer_stamp_s1_slavearbiterlockenable2 slave enables arbiterlock2, which is an e_assign
  assign timer_stamp_s1_slavearbiterlockenable2 = |timer_stamp_s1_arb_share_counter_next_value;

  //cpu/data_master timer_stamp/s1 arbiterlock2, which is an e_assign
  assign cpu_data_master_arbiterlock2 = timer_stamp_s1_slavearbiterlockenable2 & cpu_data_master_continuerequest;

  //timer_stamp_s1_any_continuerequest at least one master continues requesting, which is an e_assign
  assign timer_stamp_s1_any_continuerequest = 1;

  //cpu_data_master_continuerequest continued request, which is an e_assign
  assign cpu_data_master_continuerequest = 1;

  assign cpu_data_master_qualified_request_timer_stamp_s1 = cpu_data_master_requests_timer_stamp_s1 & ~((cpu_data_master_read & ((cpu_data_master_latency_counter != 0))));
  //local readdatavalid cpu_data_master_read_data_valid_timer_stamp_s1, which is an e_mux
  assign cpu_data_master_read_data_valid_timer_stamp_s1 = cpu_data_master_granted_timer_stamp_s1 & cpu_data_master_read & ~timer_stamp_s1_waits_for_read;

  //timer_stamp_s1_writedata mux, which is an e_mux
  assign timer_stamp_s1_writedata = cpu_data_master_writedata;

  //master is always granted when requested
  assign cpu_data_master_granted_timer_stamp_s1 = cpu_data_master_qualified_request_timer_stamp_s1;

  //cpu/data_master saved-grant timer_stamp/s1, which is an e_assign
  assign cpu_data_master_saved_grant_timer_stamp_s1 = cpu_data_master_requests_timer_stamp_s1;

  //allow new arb cycle for timer_stamp/s1, which is an e_assign
  assign timer_stamp_s1_allow_new_arb_cycle = 1;

  //placeholder chosen master
  assign timer_stamp_s1_grant_vector = 1;

  //placeholder vector of master qualified-requests
  assign timer_stamp_s1_master_qreq_vector = 1;

  //timer_stamp_s1_reset_n assignment, which is an e_assign
  assign timer_stamp_s1_reset_n = reset_n;

  assign timer_stamp_s1_chipselect = cpu_data_master_granted_timer_stamp_s1;
  //timer_stamp_s1_firsttransfer first transaction, which is an e_assign
  assign timer_stamp_s1_firsttransfer = timer_stamp_s1_begins_xfer ? timer_stamp_s1_unreg_firsttransfer : timer_stamp_s1_reg_firsttransfer;

  //timer_stamp_s1_unreg_firsttransfer first transaction, which is an e_assign
  assign timer_stamp_s1_unreg_firsttransfer = ~(timer_stamp_s1_slavearbiterlockenable & timer_stamp_s1_any_continuerequest);

  //timer_stamp_s1_reg_firsttransfer first transaction, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          timer_stamp_s1_reg_firsttransfer <= 1'b1;
      else if (timer_stamp_s1_begins_xfer)
          timer_stamp_s1_reg_firsttransfer <= timer_stamp_s1_unreg_firsttransfer;
    end


  //timer_stamp_s1_beginbursttransfer_internal begin burst transfer, which is an e_assign
  assign timer_stamp_s1_beginbursttransfer_internal = timer_stamp_s1_begins_xfer;

  //~timer_stamp_s1_write_n assignment, which is an e_mux
  assign timer_stamp_s1_write_n = ~(cpu_data_master_granted_timer_stamp_s1 & cpu_data_master_write);

  assign shifted_address_to_timer_stamp_s1_from_cpu_data_master = cpu_data_master_address_to_slave;
  //timer_stamp_s1_address mux, which is an e_mux
  assign timer_stamp_s1_address = shifted_address_to_timer_stamp_s1_from_cpu_data_master >> 2;

  //d1_timer_stamp_s1_end_xfer register, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          d1_timer_stamp_s1_end_xfer <= 1;
      else 
        d1_timer_stamp_s1_end_xfer <= timer_stamp_s1_end_xfer;
    end


  //timer_stamp_s1_waits_for_read in a cycle, which is an e_mux
  assign timer_stamp_s1_waits_for_read = timer_stamp_s1_in_a_read_cycle & timer_stamp_s1_begins_xfer;

  //timer_stamp_s1_in_a_read_cycle assignment, which is an e_assign
  assign timer_stamp_s1_in_a_read_cycle = cpu_data_master_granted_timer_stamp_s1 & cpu_data_master_read;

  //in_a_read_cycle assignment, which is an e_mux
  assign in_a_read_cycle = timer_stamp_s1_in_a_read_cycle;

  //timer_stamp_s1_waits_for_write in a cycle, which is an e_mux
  assign timer_stamp_s1_waits_for_write = timer_stamp_s1_in_a_write_cycle & 0;

  //timer_stamp_s1_in_a_write_cycle assignment, which is an e_assign
  assign timer_stamp_s1_in_a_write_cycle = cpu_data_master_granted_timer_stamp_s1 & cpu_data_master_write;

  //in_a_write_cycle assignment, which is an e_mux
  assign in_a_write_cycle = timer_stamp_s1_in_a_write_cycle;

  assign wait_for_timer_stamp_s1_counter = 0;
  //assign timer_stamp_s1_irq_from_sa = timer_stamp_s1_irq so that symbol knows where to group signals which may go to master only, which is an e_assign
  assign timer_stamp_s1_irq_from_sa = timer_stamp_s1_irq;


//synthesis translate_off
//////////////// SIMULATION-ONLY CONTENTS
  //timer_stamp/s1 enable non-zero assertions, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          enable_nonzero_assertions <= 0;
      else 
        enable_nonzero_assertions <= 1'b1;
    end



//////////////// END SIMULATION-ONLY CONTENTS

//synthesis translate_on

endmodule


// synthesis translate_off
`timescale 1ns / 1ps
// synthesis translate_on

// turn off superfluous verilog processor warnings 
// altera message_level Level1 
// altera message_off 10034 10035 10036 10037 10230 10240 10030 

module tristate_bridge_ssram_avalon_slave_arbitrator (
                                                       // inputs:
                                                        clk,
                                                        cpu_data_master_address_to_slave,
                                                        cpu_data_master_byteenable,
                                                        cpu_data_master_latency_counter,
                                                        cpu_data_master_read,
                                                        cpu_data_master_write,
                                                        cpu_data_master_writedata,
                                                        cpu_instruction_master_address_to_slave,
                                                        cpu_instruction_master_latency_counter,
                                                        cpu_instruction_master_read,
                                                        reset_n,

                                                       // outputs:
                                                        address_to_the_ssram,
                                                        adsc_n_to_the_ssram,
                                                        bw_n_to_the_ssram,
                                                        bwe_n_to_the_ssram,
                                                        chipenable1_n_to_the_ssram,
                                                        cpu_data_master_granted_ssram_s1,
                                                        cpu_data_master_qualified_request_ssram_s1,
                                                        cpu_data_master_read_data_valid_ssram_s1,
                                                        cpu_data_master_requests_ssram_s1,
                                                        cpu_instruction_master_granted_ssram_s1,
                                                        cpu_instruction_master_qualified_request_ssram_s1,
                                                        cpu_instruction_master_read_data_valid_ssram_s1,
                                                        cpu_instruction_master_requests_ssram_s1,
                                                        d1_tristate_bridge_ssram_avalon_slave_end_xfer,
                                                        data_to_and_from_the_ssram,
                                                        incoming_data_to_and_from_the_ssram,
                                                        outputenable_n_to_the_ssram,
                                                        reset_n_to_the_ssram
                                                     )
;

  output  [ 20: 0] address_to_the_ssram;
  output           adsc_n_to_the_ssram;
  output  [  3: 0] bw_n_to_the_ssram;
  output           bwe_n_to_the_ssram;
  output           chipenable1_n_to_the_ssram;
  output           cpu_data_master_granted_ssram_s1;
  output           cpu_data_master_qualified_request_ssram_s1;
  output           cpu_data_master_read_data_valid_ssram_s1;
  output           cpu_data_master_requests_ssram_s1;
  output           cpu_instruction_master_granted_ssram_s1;
  output           cpu_instruction_master_qualified_request_ssram_s1;
  output           cpu_instruction_master_read_data_valid_ssram_s1;
  output           cpu_instruction_master_requests_ssram_s1;
  output           d1_tristate_bridge_ssram_avalon_slave_end_xfer;
  inout   [ 31: 0] data_to_and_from_the_ssram;
  output  [ 31: 0] incoming_data_to_and_from_the_ssram;
  output           outputenable_n_to_the_ssram;
  output           reset_n_to_the_ssram;
  input            clk;
  input   [ 24: 0] cpu_data_master_address_to_slave;
  input   [  3: 0] cpu_data_master_byteenable;
  input   [  2: 0] cpu_data_master_latency_counter;
  input            cpu_data_master_read;
  input            cpu_data_master_write;
  input   [ 31: 0] cpu_data_master_writedata;
  input   [ 24: 0] cpu_instruction_master_address_to_slave;
  input   [  2: 0] cpu_instruction_master_latency_counter;
  input            cpu_instruction_master_read;
  input            reset_n;

  reg     [ 20: 0] address_to_the_ssram /* synthesis ALTERA_ATTRIBUTE = "FAST_OUTPUT_REGISTER=ON"  */;
  reg              adsc_n_to_the_ssram /* synthesis ALTERA_ATTRIBUTE = "FAST_OUTPUT_REGISTER=ON"  */;
  reg     [  3: 0] bw_n_to_the_ssram /* synthesis ALTERA_ATTRIBUTE = "FAST_OUTPUT_REGISTER=ON"  */;
  reg              bwe_n_to_the_ssram /* synthesis ALTERA_ATTRIBUTE = "FAST_OUTPUT_REGISTER=ON"  */;
  reg              chipenable1_n_to_the_ssram /* synthesis ALTERA_ATTRIBUTE = "FAST_OUTPUT_REGISTER=ON"  */;
  wire             cpu_data_master_arbiterlock;
  wire             cpu_data_master_arbiterlock2;
  wire             cpu_data_master_continuerequest;
  wire             cpu_data_master_granted_ssram_s1;
  wire             cpu_data_master_qualified_request_ssram_s1;
  wire             cpu_data_master_read_data_valid_ssram_s1;
  reg     [  3: 0] cpu_data_master_read_data_valid_ssram_s1_shift_register;
  wire             cpu_data_master_read_data_valid_ssram_s1_shift_register_in;
  wire             cpu_data_master_requests_ssram_s1;
  wire             cpu_data_master_saved_grant_ssram_s1;
  wire             cpu_instruction_master_arbiterlock;
  wire             cpu_instruction_master_arbiterlock2;
  wire             cpu_instruction_master_continuerequest;
  wire             cpu_instruction_master_granted_ssram_s1;
  wire             cpu_instruction_master_qualified_request_ssram_s1;
  wire             cpu_instruction_master_read_data_valid_ssram_s1;
  reg     [  3: 0] cpu_instruction_master_read_data_valid_ssram_s1_shift_register;
  wire             cpu_instruction_master_read_data_valid_ssram_s1_shift_register_in;
  wire             cpu_instruction_master_requests_ssram_s1;
  wire             cpu_instruction_master_saved_grant_ssram_s1;
  reg              d1_in_a_write_cycle /* synthesis ALTERA_ATTRIBUTE = "FAST_OUTPUT_ENABLE_REGISTER=ON"  */;
  reg     [ 31: 0] d1_outgoing_data_to_and_from_the_ssram /* synthesis ALTERA_ATTRIBUTE = "FAST_OUTPUT_REGISTER=ON"  */;
  reg              d1_reasons_to_wait;
  reg              d1_tristate_bridge_ssram_avalon_slave_end_xfer;
  wire    [ 31: 0] data_to_and_from_the_ssram;
  reg              enable_nonzero_assertions;
  wire             end_xfer_arb_share_counter_term_tristate_bridge_ssram_avalon_slave;
  wire             in_a_read_cycle;
  wire             in_a_write_cycle;
  reg     [ 31: 0] incoming_data_to_and_from_the_ssram /* synthesis ALTERA_ATTRIBUTE = "FAST_INPUT_REGISTER=ON"  */;
  reg              last_cycle_cpu_data_master_granted_slave_ssram_s1;
  reg              last_cycle_cpu_instruction_master_granted_slave_ssram_s1;
  wire    [ 31: 0] outgoing_data_to_and_from_the_ssram;
  reg              outputenable_n_to_the_ssram /* synthesis ALTERA_ATTRIBUTE = "FAST_OUTPUT_REGISTER=ON"  */;
  wire    [ 20: 0] p1_address_to_the_ssram;
  wire             p1_adsc_n_to_the_ssram;
  wire    [  3: 0] p1_bw_n_to_the_ssram;
  wire             p1_bwe_n_to_the_ssram;
  wire             p1_chipenable1_n_to_the_ssram;
  wire    [  3: 0] p1_cpu_data_master_read_data_valid_ssram_s1_shift_register;
  wire    [  3: 0] p1_cpu_instruction_master_read_data_valid_ssram_s1_shift_register;
  wire             p1_outputenable_n_to_the_ssram;
  wire             p1_reset_n_to_the_ssram;
  reg              reset_n_to_the_ssram /* synthesis ALTERA_ATTRIBUTE = "FAST_OUTPUT_REGISTER=ON"  */;
  wire             ssram_s1_in_a_read_cycle;
  wire             ssram_s1_in_a_write_cycle;
  wire             ssram_s1_waits_for_read;
  wire             ssram_s1_waits_for_write;
  wire             ssram_s1_with_write_latency;
  wire             time_to_write;
  wire             tristate_bridge_ssram_avalon_slave_allgrants;
  wire             tristate_bridge_ssram_avalon_slave_allow_new_arb_cycle;
  wire             tristate_bridge_ssram_avalon_slave_any_bursting_master_saved_grant;
  wire             tristate_bridge_ssram_avalon_slave_any_continuerequest;
  reg     [  1: 0] tristate_bridge_ssram_avalon_slave_arb_addend;
  wire             tristate_bridge_ssram_avalon_slave_arb_counter_enable;
  reg              tristate_bridge_ssram_avalon_slave_arb_share_counter;
  wire             tristate_bridge_ssram_avalon_slave_arb_share_counter_next_value;
  wire             tristate_bridge_ssram_avalon_slave_arb_share_set_values;
  wire    [  1: 0] tristate_bridge_ssram_avalon_slave_arb_winner;
  wire             tristate_bridge_ssram_avalon_slave_arbitration_holdoff_internal;
  wire             tristate_bridge_ssram_avalon_slave_beginbursttransfer_internal;
  wire             tristate_bridge_ssram_avalon_slave_begins_xfer;
  wire    [  3: 0] tristate_bridge_ssram_avalon_slave_chosen_master_double_vector;
  wire    [  1: 0] tristate_bridge_ssram_avalon_slave_chosen_master_rot_left;
  wire             tristate_bridge_ssram_avalon_slave_end_xfer;
  wire             tristate_bridge_ssram_avalon_slave_firsttransfer;
  wire    [  1: 0] tristate_bridge_ssram_avalon_slave_grant_vector;
  wire    [  1: 0] tristate_bridge_ssram_avalon_slave_master_qreq_vector;
  wire             tristate_bridge_ssram_avalon_slave_non_bursting_master_requests;
  wire             tristate_bridge_ssram_avalon_slave_read_pending;
  reg              tristate_bridge_ssram_avalon_slave_reg_firsttransfer;
  reg     [  1: 0] tristate_bridge_ssram_avalon_slave_saved_chosen_master_vector;
  reg              tristate_bridge_ssram_avalon_slave_slavearbiterlockenable;
  wire             tristate_bridge_ssram_avalon_slave_slavearbiterlockenable2;
  wire             tristate_bridge_ssram_avalon_slave_unreg_firsttransfer;
  wire             tristate_bridge_ssram_avalon_slave_write_pending;
  wire             wait_for_ssram_s1_counter;
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          d1_reasons_to_wait <= 0;
      else 
        d1_reasons_to_wait <= ~tristate_bridge_ssram_avalon_slave_end_xfer;
    end


  assign tristate_bridge_ssram_avalon_slave_begins_xfer = ~d1_reasons_to_wait & ((cpu_data_master_qualified_request_ssram_s1 | cpu_instruction_master_qualified_request_ssram_s1));
  assign cpu_data_master_requests_ssram_s1 = ({cpu_data_master_address_to_slave[24 : 21] , 21'b0} == 25'h1200000) & (cpu_data_master_read | cpu_data_master_write);
  //~chipenable1_n_to_the_ssram of type chipselect to ~p1_chipenable1_n_to_the_ssram, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          chipenable1_n_to_the_ssram <= ~0;
      else 
        chipenable1_n_to_the_ssram <= p1_chipenable1_n_to_the_ssram;
    end


  assign tristate_bridge_ssram_avalon_slave_write_pending = 0;
  //tristate_bridge_ssram/avalon_slave read pending calc, which is an e_assign
  assign tristate_bridge_ssram_avalon_slave_read_pending = (|cpu_data_master_read_data_valid_ssram_s1_shift_register[1 : 0]) | (|cpu_instruction_master_read_data_valid_ssram_s1_shift_register[1 : 0]);

  //tristate_bridge_ssram_avalon_slave_arb_share_counter set values, which is an e_mux
  assign tristate_bridge_ssram_avalon_slave_arb_share_set_values = 1;

  //tristate_bridge_ssram_avalon_slave_non_bursting_master_requests mux, which is an e_mux
  assign tristate_bridge_ssram_avalon_slave_non_bursting_master_requests = cpu_data_master_requests_ssram_s1 |
    cpu_instruction_master_requests_ssram_s1 |
    cpu_data_master_requests_ssram_s1 |
    cpu_instruction_master_requests_ssram_s1;

  //tristate_bridge_ssram_avalon_slave_any_bursting_master_saved_grant mux, which is an e_mux
  assign tristate_bridge_ssram_avalon_slave_any_bursting_master_saved_grant = 0;

  //tristate_bridge_ssram_avalon_slave_arb_share_counter_next_value assignment, which is an e_assign
  assign tristate_bridge_ssram_avalon_slave_arb_share_counter_next_value = tristate_bridge_ssram_avalon_slave_firsttransfer ? (tristate_bridge_ssram_avalon_slave_arb_share_set_values - 1) : |tristate_bridge_ssram_avalon_slave_arb_share_counter ? (tristate_bridge_ssram_avalon_slave_arb_share_counter - 1) : 0;

  //tristate_bridge_ssram_avalon_slave_allgrants all slave grants, which is an e_mux
  assign tristate_bridge_ssram_avalon_slave_allgrants = (|tristate_bridge_ssram_avalon_slave_grant_vector) |
    (|tristate_bridge_ssram_avalon_slave_grant_vector) |
    (|tristate_bridge_ssram_avalon_slave_grant_vector) |
    (|tristate_bridge_ssram_avalon_slave_grant_vector);

  //tristate_bridge_ssram_avalon_slave_end_xfer assignment, which is an e_assign
  assign tristate_bridge_ssram_avalon_slave_end_xfer = ~(ssram_s1_waits_for_read | ssram_s1_waits_for_write);

  //end_xfer_arb_share_counter_term_tristate_bridge_ssram_avalon_slave arb share counter enable term, which is an e_assign
  assign end_xfer_arb_share_counter_term_tristate_bridge_ssram_avalon_slave = tristate_bridge_ssram_avalon_slave_end_xfer & (~tristate_bridge_ssram_avalon_slave_any_bursting_master_saved_grant | in_a_read_cycle | in_a_write_cycle);

  //tristate_bridge_ssram_avalon_slave_arb_share_counter arbitration counter enable, which is an e_assign
  assign tristate_bridge_ssram_avalon_slave_arb_counter_enable = (end_xfer_arb_share_counter_term_tristate_bridge_ssram_avalon_slave & tristate_bridge_ssram_avalon_slave_allgrants) | (end_xfer_arb_share_counter_term_tristate_bridge_ssram_avalon_slave & ~tristate_bridge_ssram_avalon_slave_non_bursting_master_requests);

  //tristate_bridge_ssram_avalon_slave_arb_share_counter counter, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          tristate_bridge_ssram_avalon_slave_arb_share_counter <= 0;
      else if (tristate_bridge_ssram_avalon_slave_arb_counter_enable)
          tristate_bridge_ssram_avalon_slave_arb_share_counter <= tristate_bridge_ssram_avalon_slave_arb_share_counter_next_value;
    end


  //tristate_bridge_ssram_avalon_slave_slavearbiterlockenable slave enables arbiterlock, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          tristate_bridge_ssram_avalon_slave_slavearbiterlockenable <= 0;
      else if ((|tristate_bridge_ssram_avalon_slave_master_qreq_vector & end_xfer_arb_share_counter_term_tristate_bridge_ssram_avalon_slave) | (end_xfer_arb_share_counter_term_tristate_bridge_ssram_avalon_slave & ~tristate_bridge_ssram_avalon_slave_non_bursting_master_requests))
          tristate_bridge_ssram_avalon_slave_slavearbiterlockenable <= |tristate_bridge_ssram_avalon_slave_arb_share_counter_next_value;
    end


  //cpu/data_master tristate_bridge_ssram/avalon_slave arbiterlock, which is an e_assign
  assign cpu_data_master_arbiterlock = tristate_bridge_ssram_avalon_slave_slavearbiterlockenable & cpu_data_master_continuerequest;

  //tristate_bridge_ssram_avalon_slave_slavearbiterlockenable2 slave enables arbiterlock2, which is an e_assign
  assign tristate_bridge_ssram_avalon_slave_slavearbiterlockenable2 = |tristate_bridge_ssram_avalon_slave_arb_share_counter_next_value;

  //cpu/data_master tristate_bridge_ssram/avalon_slave arbiterlock2, which is an e_assign
  assign cpu_data_master_arbiterlock2 = tristate_bridge_ssram_avalon_slave_slavearbiterlockenable2 & cpu_data_master_continuerequest;

  //cpu/instruction_master tristate_bridge_ssram/avalon_slave arbiterlock, which is an e_assign
  assign cpu_instruction_master_arbiterlock = tristate_bridge_ssram_avalon_slave_slavearbiterlockenable & cpu_instruction_master_continuerequest;

  //cpu/instruction_master tristate_bridge_ssram/avalon_slave arbiterlock2, which is an e_assign
  assign cpu_instruction_master_arbiterlock2 = tristate_bridge_ssram_avalon_slave_slavearbiterlockenable2 & cpu_instruction_master_continuerequest;

  //cpu/instruction_master granted ssram/s1 last time, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          last_cycle_cpu_instruction_master_granted_slave_ssram_s1 <= 0;
      else 
        last_cycle_cpu_instruction_master_granted_slave_ssram_s1 <= cpu_instruction_master_saved_grant_ssram_s1 ? 1 : (tristate_bridge_ssram_avalon_slave_arbitration_holdoff_internal | ~cpu_instruction_master_requests_ssram_s1) ? 0 : last_cycle_cpu_instruction_master_granted_slave_ssram_s1;
    end


  //cpu_instruction_master_continuerequest continued request, which is an e_mux
  assign cpu_instruction_master_continuerequest = last_cycle_cpu_instruction_master_granted_slave_ssram_s1 & cpu_instruction_master_requests_ssram_s1;

  //tristate_bridge_ssram_avalon_slave_any_continuerequest at least one master continues requesting, which is an e_mux
  assign tristate_bridge_ssram_avalon_slave_any_continuerequest = cpu_instruction_master_continuerequest |
    cpu_data_master_continuerequest;

  assign cpu_data_master_qualified_request_ssram_s1 = cpu_data_master_requests_ssram_s1 & ~((cpu_data_master_read & (tristate_bridge_ssram_avalon_slave_write_pending | (tristate_bridge_ssram_avalon_slave_read_pending & !((((|cpu_data_master_read_data_valid_ssram_s1_shift_register[1 : 0]) | (|cpu_instruction_master_read_data_valid_ssram_s1_shift_register[1 : 0]))))) | (4 < cpu_data_master_latency_counter))) | ((tristate_bridge_ssram_avalon_slave_read_pending) & cpu_data_master_write) | cpu_instruction_master_arbiterlock);
  //cpu_data_master_read_data_valid_ssram_s1_shift_register_in mux for readlatency shift register, which is an e_mux
  assign cpu_data_master_read_data_valid_ssram_s1_shift_register_in = cpu_data_master_granted_ssram_s1 & cpu_data_master_read & ~ssram_s1_waits_for_read;

  //shift register p1 cpu_data_master_read_data_valid_ssram_s1_shift_register in if flush, otherwise shift left, which is an e_mux
  assign p1_cpu_data_master_read_data_valid_ssram_s1_shift_register = {cpu_data_master_read_data_valid_ssram_s1_shift_register, cpu_data_master_read_data_valid_ssram_s1_shift_register_in};

  //cpu_data_master_read_data_valid_ssram_s1_shift_register for remembering which master asked for a fixed latency read, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          cpu_data_master_read_data_valid_ssram_s1_shift_register <= 0;
      else 
        cpu_data_master_read_data_valid_ssram_s1_shift_register <= p1_cpu_data_master_read_data_valid_ssram_s1_shift_register;
    end


  //local readdatavalid cpu_data_master_read_data_valid_ssram_s1, which is an e_mux
  assign cpu_data_master_read_data_valid_ssram_s1 = cpu_data_master_read_data_valid_ssram_s1_shift_register[3];

  //data_to_and_from_the_ssram register, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          incoming_data_to_and_from_the_ssram <= 0;
      else 
        incoming_data_to_and_from_the_ssram <= data_to_and_from_the_ssram;
    end


  //ssram_s1_with_write_latency assignment, which is an e_assign
  assign ssram_s1_with_write_latency = in_a_write_cycle & (cpu_data_master_qualified_request_ssram_s1 | cpu_instruction_master_qualified_request_ssram_s1);

  //time to write the data, which is an e_mux
  assign time_to_write = (ssram_s1_with_write_latency)? 1 :
    0;

  //d1_outgoing_data_to_and_from_the_ssram register, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          d1_outgoing_data_to_and_from_the_ssram <= 0;
      else 
        d1_outgoing_data_to_and_from_the_ssram <= outgoing_data_to_and_from_the_ssram;
    end


  //write cycle delayed by 1, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          d1_in_a_write_cycle <= 0;
      else 
        d1_in_a_write_cycle <= time_to_write;
    end


  //d1_outgoing_data_to_and_from_the_ssram tristate driver, which is an e_assign
  assign data_to_and_from_the_ssram = (d1_in_a_write_cycle)? d1_outgoing_data_to_and_from_the_ssram:{32{1'bz}};

  //outgoing_data_to_and_from_the_ssram mux, which is an e_mux
  assign outgoing_data_to_and_from_the_ssram = cpu_data_master_writedata;

  assign cpu_instruction_master_requests_ssram_s1 = (({cpu_instruction_master_address_to_slave[24 : 21] , 21'b0} == 25'h1200000) & (cpu_instruction_master_read)) & cpu_instruction_master_read;
  //cpu/data_master granted ssram/s1 last time, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          last_cycle_cpu_data_master_granted_slave_ssram_s1 <= 0;
      else 
        last_cycle_cpu_data_master_granted_slave_ssram_s1 <= cpu_data_master_saved_grant_ssram_s1 ? 1 : (tristate_bridge_ssram_avalon_slave_arbitration_holdoff_internal | ~cpu_data_master_requests_ssram_s1) ? 0 : last_cycle_cpu_data_master_granted_slave_ssram_s1;
    end


  //cpu_data_master_continuerequest continued request, which is an e_mux
  assign cpu_data_master_continuerequest = last_cycle_cpu_data_master_granted_slave_ssram_s1 & cpu_data_master_requests_ssram_s1;

  assign cpu_instruction_master_qualified_request_ssram_s1 = cpu_instruction_master_requests_ssram_s1 & ~((cpu_instruction_master_read & (tristate_bridge_ssram_avalon_slave_write_pending | (tristate_bridge_ssram_avalon_slave_read_pending & !((((|cpu_data_master_read_data_valid_ssram_s1_shift_register[1 : 0]) | (|cpu_instruction_master_read_data_valid_ssram_s1_shift_register[1 : 0]))))) | (4 < cpu_instruction_master_latency_counter))) | cpu_data_master_arbiterlock);
  //cpu_instruction_master_read_data_valid_ssram_s1_shift_register_in mux for readlatency shift register, which is an e_mux
  assign cpu_instruction_master_read_data_valid_ssram_s1_shift_register_in = cpu_instruction_master_granted_ssram_s1 & cpu_instruction_master_read & ~ssram_s1_waits_for_read;

  //shift register p1 cpu_instruction_master_read_data_valid_ssram_s1_shift_register in if flush, otherwise shift left, which is an e_mux
  assign p1_cpu_instruction_master_read_data_valid_ssram_s1_shift_register = {cpu_instruction_master_read_data_valid_ssram_s1_shift_register, cpu_instruction_master_read_data_valid_ssram_s1_shift_register_in};

  //cpu_instruction_master_read_data_valid_ssram_s1_shift_register for remembering which master asked for a fixed latency read, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          cpu_instruction_master_read_data_valid_ssram_s1_shift_register <= 0;
      else 
        cpu_instruction_master_read_data_valid_ssram_s1_shift_register <= p1_cpu_instruction_master_read_data_valid_ssram_s1_shift_register;
    end


  //local readdatavalid cpu_instruction_master_read_data_valid_ssram_s1, which is an e_mux
  assign cpu_instruction_master_read_data_valid_ssram_s1 = cpu_instruction_master_read_data_valid_ssram_s1_shift_register[3];

  //allow new arb cycle for tristate_bridge_ssram/avalon_slave, which is an e_assign
  assign tristate_bridge_ssram_avalon_slave_allow_new_arb_cycle = ~cpu_data_master_arbiterlock & ~cpu_instruction_master_arbiterlock;

  //cpu/instruction_master assignment into master qualified-requests vector for ssram/s1, which is an e_assign
  assign tristate_bridge_ssram_avalon_slave_master_qreq_vector[0] = cpu_instruction_master_qualified_request_ssram_s1;

  //cpu/instruction_master grant ssram/s1, which is an e_assign
  assign cpu_instruction_master_granted_ssram_s1 = tristate_bridge_ssram_avalon_slave_grant_vector[0];

  //cpu/instruction_master saved-grant ssram/s1, which is an e_assign
  assign cpu_instruction_master_saved_grant_ssram_s1 = tristate_bridge_ssram_avalon_slave_arb_winner[0] && cpu_instruction_master_requests_ssram_s1;

  //cpu/data_master assignment into master qualified-requests vector for ssram/s1, which is an e_assign
  assign tristate_bridge_ssram_avalon_slave_master_qreq_vector[1] = cpu_data_master_qualified_request_ssram_s1;

  //cpu/data_master grant ssram/s1, which is an e_assign
  assign cpu_data_master_granted_ssram_s1 = tristate_bridge_ssram_avalon_slave_grant_vector[1];

  //cpu/data_master saved-grant ssram/s1, which is an e_assign
  assign cpu_data_master_saved_grant_ssram_s1 = tristate_bridge_ssram_avalon_slave_arb_winner[1] && cpu_data_master_requests_ssram_s1;

  //tristate_bridge_ssram/avalon_slave chosen-master double-vector, which is an e_assign
  assign tristate_bridge_ssram_avalon_slave_chosen_master_double_vector = {tristate_bridge_ssram_avalon_slave_master_qreq_vector, tristate_bridge_ssram_avalon_slave_master_qreq_vector} & ({~tristate_bridge_ssram_avalon_slave_master_qreq_vector, ~tristate_bridge_ssram_avalon_slave_master_qreq_vector} + tristate_bridge_ssram_avalon_slave_arb_addend);

  //stable onehot encoding of arb winner
  assign tristate_bridge_ssram_avalon_slave_arb_winner = (tristate_bridge_ssram_avalon_slave_allow_new_arb_cycle & | tristate_bridge_ssram_avalon_slave_grant_vector) ? tristate_bridge_ssram_avalon_slave_grant_vector : tristate_bridge_ssram_avalon_slave_saved_chosen_master_vector;

  //saved tristate_bridge_ssram_avalon_slave_grant_vector, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          tristate_bridge_ssram_avalon_slave_saved_chosen_master_vector <= 0;
      else if (tristate_bridge_ssram_avalon_slave_allow_new_arb_cycle)
          tristate_bridge_ssram_avalon_slave_saved_chosen_master_vector <= |tristate_bridge_ssram_avalon_slave_grant_vector ? tristate_bridge_ssram_avalon_slave_grant_vector : tristate_bridge_ssram_avalon_slave_saved_chosen_master_vector;
    end


  //onehot encoding of chosen master
  assign tristate_bridge_ssram_avalon_slave_grant_vector = {(tristate_bridge_ssram_avalon_slave_chosen_master_double_vector[1] | tristate_bridge_ssram_avalon_slave_chosen_master_double_vector[3]),
    (tristate_bridge_ssram_avalon_slave_chosen_master_double_vector[0] | tristate_bridge_ssram_avalon_slave_chosen_master_double_vector[2])};

  //tristate_bridge_ssram/avalon_slave chosen master rotated left, which is an e_assign
  assign tristate_bridge_ssram_avalon_slave_chosen_master_rot_left = (tristate_bridge_ssram_avalon_slave_arb_winner << 1) ? (tristate_bridge_ssram_avalon_slave_arb_winner << 1) : 1;

  //tristate_bridge_ssram/avalon_slave's addend for next-master-grant
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          tristate_bridge_ssram_avalon_slave_arb_addend <= 1;
      else if (|tristate_bridge_ssram_avalon_slave_grant_vector)
          tristate_bridge_ssram_avalon_slave_arb_addend <= tristate_bridge_ssram_avalon_slave_end_xfer? tristate_bridge_ssram_avalon_slave_chosen_master_rot_left : tristate_bridge_ssram_avalon_slave_grant_vector;
    end


  //~adsc_n_to_the_ssram of type begintransfer to ~p1_adsc_n_to_the_ssram, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          adsc_n_to_the_ssram <= ~0;
      else 
        adsc_n_to_the_ssram <= p1_adsc_n_to_the_ssram;
    end


  assign p1_adsc_n_to_the_ssram = ~tristate_bridge_ssram_avalon_slave_begins_xfer;
  //~outputenable_n_to_the_ssram of type outputenable to ~p1_outputenable_n_to_the_ssram, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          outputenable_n_to_the_ssram <= ~0;
      else 
        outputenable_n_to_the_ssram <= p1_outputenable_n_to_the_ssram;
    end


  //~p1_outputenable_n_to_the_ssram assignment, which is an e_mux
  assign p1_outputenable_n_to_the_ssram = ~((|cpu_data_master_read_data_valid_ssram_s1_shift_register[1 : 0]) | (|cpu_instruction_master_read_data_valid_ssram_s1_shift_register[1 : 0]) | ssram_s1_in_a_read_cycle);

  //reset_n_to_the_ssram of type reset_n to p1_reset_n_to_the_ssram, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          reset_n_to_the_ssram <= 0;
      else 
        reset_n_to_the_ssram <= p1_reset_n_to_the_ssram;
    end


  //p1_reset_n_to_the_ssram assignment, which is an e_assign
  assign p1_reset_n_to_the_ssram = reset_n;

  assign p1_chipenable1_n_to_the_ssram = ~(cpu_data_master_granted_ssram_s1 | cpu_instruction_master_granted_ssram_s1 | (|cpu_data_master_read_data_valid_ssram_s1_shift_register[1 : 0]) | (|cpu_instruction_master_read_data_valid_ssram_s1_shift_register[1 : 0]));
  //tristate_bridge_ssram_avalon_slave_firsttransfer first transaction, which is an e_assign
  assign tristate_bridge_ssram_avalon_slave_firsttransfer = tristate_bridge_ssram_avalon_slave_begins_xfer ? tristate_bridge_ssram_avalon_slave_unreg_firsttransfer : tristate_bridge_ssram_avalon_slave_reg_firsttransfer;

  //tristate_bridge_ssram_avalon_slave_unreg_firsttransfer first transaction, which is an e_assign
  assign tristate_bridge_ssram_avalon_slave_unreg_firsttransfer = ~(tristate_bridge_ssram_avalon_slave_slavearbiterlockenable & tristate_bridge_ssram_avalon_slave_any_continuerequest);

  //tristate_bridge_ssram_avalon_slave_reg_firsttransfer first transaction, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          tristate_bridge_ssram_avalon_slave_reg_firsttransfer <= 1'b1;
      else if (tristate_bridge_ssram_avalon_slave_begins_xfer)
          tristate_bridge_ssram_avalon_slave_reg_firsttransfer <= tristate_bridge_ssram_avalon_slave_unreg_firsttransfer;
    end


  //tristate_bridge_ssram_avalon_slave_beginbursttransfer_internal begin burst transfer, which is an e_assign
  assign tristate_bridge_ssram_avalon_slave_beginbursttransfer_internal = tristate_bridge_ssram_avalon_slave_begins_xfer;

  //tristate_bridge_ssram_avalon_slave_arbitration_holdoff_internal arbitration_holdoff, which is an e_assign
  assign tristate_bridge_ssram_avalon_slave_arbitration_holdoff_internal = tristate_bridge_ssram_avalon_slave_begins_xfer & tristate_bridge_ssram_avalon_slave_firsttransfer;

  //~bwe_n_to_the_ssram of type write to ~p1_bwe_n_to_the_ssram, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          bwe_n_to_the_ssram <= ~0;
      else 
        bwe_n_to_the_ssram <= p1_bwe_n_to_the_ssram;
    end


  //~bw_n_to_the_ssram of type byteenable to ~p1_bw_n_to_the_ssram, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          bw_n_to_the_ssram <= ~0;
      else 
        bw_n_to_the_ssram <= p1_bw_n_to_the_ssram;
    end


  //~p1_bwe_n_to_the_ssram assignment, which is an e_mux
  assign p1_bwe_n_to_the_ssram = ~(cpu_data_master_granted_ssram_s1 & cpu_data_master_write);

  //address_to_the_ssram of type address to p1_address_to_the_ssram, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          address_to_the_ssram <= 0;
      else 
        address_to_the_ssram <= p1_address_to_the_ssram;
    end


  //p1_address_to_the_ssram mux, which is an e_mux
  assign p1_address_to_the_ssram = (cpu_data_master_granted_ssram_s1)? cpu_data_master_address_to_slave :
    cpu_instruction_master_address_to_slave;

  //d1_tristate_bridge_ssram_avalon_slave_end_xfer register, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          d1_tristate_bridge_ssram_avalon_slave_end_xfer <= 1;
      else 
        d1_tristate_bridge_ssram_avalon_slave_end_xfer <= tristate_bridge_ssram_avalon_slave_end_xfer;
    end


  //ssram_s1_waits_for_read in a cycle, which is an e_mux
  assign ssram_s1_waits_for_read = ssram_s1_in_a_read_cycle & 0;

  //ssram_s1_in_a_read_cycle assignment, which is an e_assign
  assign ssram_s1_in_a_read_cycle = (cpu_data_master_granted_ssram_s1 & cpu_data_master_read) | (cpu_instruction_master_granted_ssram_s1 & cpu_instruction_master_read);

  //in_a_read_cycle assignment, which is an e_mux
  assign in_a_read_cycle = ssram_s1_in_a_read_cycle;

  //ssram_s1_waits_for_write in a cycle, which is an e_mux
  assign ssram_s1_waits_for_write = ssram_s1_in_a_write_cycle & 0;

  //ssram_s1_in_a_write_cycle assignment, which is an e_assign
  assign ssram_s1_in_a_write_cycle = cpu_data_master_granted_ssram_s1 & cpu_data_master_write;

  //in_a_write_cycle assignment, which is an e_mux
  assign in_a_write_cycle = ssram_s1_in_a_write_cycle;

  assign wait_for_ssram_s1_counter = 0;
  //~p1_bw_n_to_the_ssram byte enable port mux, which is an e_mux
  assign p1_bw_n_to_the_ssram = ~((cpu_data_master_granted_ssram_s1)? cpu_data_master_byteenable :
    -1);


//synthesis translate_off
//////////////// SIMULATION-ONLY CONTENTS
  //ssram/s1 enable non-zero assertions, which is an e_register
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          enable_nonzero_assertions <= 0;
      else 
        enable_nonzero_assertions <= 1'b1;
    end


  //grant signals are active simultaneously, which is an e_process
  always @(posedge clk)
    begin
      if (cpu_data_master_granted_ssram_s1 + cpu_instruction_master_granted_ssram_s1 > 1)
        begin
          $write("%0d ns: > 1 of grant signals are active simultaneously", $time);
          $stop;
        end
    end


  //saved_grant signals are active simultaneously, which is an e_process
  always @(posedge clk)
    begin
      if (cpu_data_master_saved_grant_ssram_s1 + cpu_instruction_master_saved_grant_ssram_s1 > 1)
        begin
          $write("%0d ns: > 1 of saved_grant signals are active simultaneously", $time);
          $stop;
        end
    end



//////////////// END SIMULATION-ONLY CONTENTS

//synthesis translate_on

endmodule


// synthesis translate_off
`timescale 1ns / 1ps
// synthesis translate_on

// turn off superfluous verilog processor warnings 
// altera message_level Level1 
// altera message_off 10034 10035 10036 10037 10230 10240 10030 

module tristate_bridge_ssram_bridge_arbitrator 
;



endmodule


// synthesis translate_off
`timescale 1ns / 1ps
// synthesis translate_on

// turn off superfluous verilog processor warnings 
// altera message_level Level1 
// altera message_off 10034 10035 10036 10037 10230 10240 10030 

module DE2_70_SOPC_reset_pll_c0_out_domain_synch_module (
                                                          // inputs:
                                                           clk,
                                                           data_in,
                                                           reset_n,

                                                          // outputs:
                                                           data_out
                                                        )
;

  output           data_out;
  input            clk;
  input            data_in;
  input            reset_n;

  reg              data_in_d1 /* synthesis ALTERA_ATTRIBUTE = "{-from \"*\"} CUT=ON ; PRESERVE_REGISTER=ON ; SUPPRESS_DA_RULE_INTERNAL=R101"  */;
  reg              data_out /* synthesis ALTERA_ATTRIBUTE = "PRESERVE_REGISTER=ON ; SUPPRESS_DA_RULE_INTERNAL=R101"  */;
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          data_in_d1 <= 0;
      else 
        data_in_d1 <= data_in;
    end


  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          data_out <= 0;
      else 
        data_out <= data_in_d1;
    end



endmodule


// synthesis translate_off
`timescale 1ns / 1ps
// synthesis translate_on

// turn off superfluous verilog processor warnings 
// altera message_level Level1 
// altera message_off 10034 10035 10036 10037 10230 10240 10030 

module DE2_70_SOPC_reset_clk_25_domain_synch_module (
                                                      // inputs:
                                                       clk,
                                                       data_in,
                                                       reset_n,

                                                      // outputs:
                                                       data_out
                                                    )
;

  output           data_out;
  input            clk;
  input            data_in;
  input            reset_n;

  reg              data_in_d1 /* synthesis ALTERA_ATTRIBUTE = "{-from \"*\"} CUT=ON ; PRESERVE_REGISTER=ON ; SUPPRESS_DA_RULE_INTERNAL=R101"  */;
  reg              data_out /* synthesis ALTERA_ATTRIBUTE = "PRESERVE_REGISTER=ON ; SUPPRESS_DA_RULE_INTERNAL=R101"  */;
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          data_in_d1 <= 0;
      else 
        data_in_d1 <= data_in;
    end


  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          data_out <= 0;
      else 
        data_out <= data_in_d1;
    end



endmodule


// synthesis translate_off
`timescale 1ns / 1ps
// synthesis translate_on

// turn off superfluous verilog processor warnings 
// altera message_level Level1 
// altera message_off 10034 10035 10036 10037 10230 10240 10030 

module DE2_70_SOPC_reset_clk_50_domain_synch_module (
                                                      // inputs:
                                                       clk,
                                                       data_in,
                                                       reset_n,

                                                      // outputs:
                                                       data_out
                                                    )
;

  output           data_out;
  input            clk;
  input            data_in;
  input            reset_n;

  reg              data_in_d1 /* synthesis ALTERA_ATTRIBUTE = "{-from \"*\"} CUT=ON ; PRESERVE_REGISTER=ON ; SUPPRESS_DA_RULE_INTERNAL=R101"  */;
  reg              data_out /* synthesis ALTERA_ATTRIBUTE = "PRESERVE_REGISTER=ON ; SUPPRESS_DA_RULE_INTERNAL=R101"  */;
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          data_in_d1 <= 0;
      else 
        data_in_d1 <= data_in;
    end


  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
          data_out <= 0;
      else 
        data_out <= data_in_d1;
    end



endmodule


// synthesis translate_off
`timescale 1ns / 1ps
// synthesis translate_on

// turn off superfluous verilog processor warnings 
// altera message_level Level1 
// altera message_off 10034 10035 10036 10037 10230 10240 10030 

module DE2_70_SOPC (
                     // 1) global signals:
                      clk_25,
                      clk_50,
                      pll_c0_out,
                      pll_c1_out,
                      pll_c2_audio,
                      reset_n,

                     // the_DM9000A
                      avs_s1_export_ENET_CLK_from_the_DM9000A,
                      avs_s1_export_ENET_CMD_from_the_DM9000A,
                      avs_s1_export_ENET_CS_N_from_the_DM9000A,
                      avs_s1_export_ENET_DATA_to_and_from_the_DM9000A,
                      avs_s1_export_ENET_INT_to_the_DM9000A,
                      avs_s1_export_ENET_RD_N_from_the_DM9000A,
                      avs_s1_export_ENET_RST_N_from_the_DM9000A,
                      avs_s1_export_ENET_WR_N_from_the_DM9000A,

                     // the_SEG7
                      avs_s1_export_seg7_from_the_SEG7,

                     // the_control_in_pio
                      in_port_to_the_control_in_pio,

                     // the_control_out_pio
                      out_port_from_the_control_out_pio,

                     // the_data_in_pio
                      in_port_to_the_data_in_pio,

                     // the_data_out_pio
                      out_port_from_the_data_out_pio,

                     // the_lcd
                      LCD_E_from_the_lcd,
                      LCD_RS_from_the_lcd,
                      LCD_RW_from_the_lcd,
                      LCD_data_to_and_from_the_lcd,

                     // the_pio_button
                      in_port_to_the_pio_button,

                     // the_pio_switch
                      in_port_to_the_pio_switch,

                     // the_tristate_bridge_ssram_avalon_slave
                      address_to_the_ssram,
                      adsc_n_to_the_ssram,
                      bw_n_to_the_ssram,
                      bwe_n_to_the_ssram,
                      chipenable1_n_to_the_ssram,
                      data_to_and_from_the_ssram,
                      outputenable_n_to_the_ssram,
                      reset_n_to_the_ssram
                   )
;

  output           LCD_E_from_the_lcd;
  output           LCD_RS_from_the_lcd;
  output           LCD_RW_from_the_lcd;
  inout   [  7: 0] LCD_data_to_and_from_the_lcd;
  output  [ 20: 0] address_to_the_ssram;
  output           adsc_n_to_the_ssram;
  output           avs_s1_export_ENET_CLK_from_the_DM9000A;
  output           avs_s1_export_ENET_CMD_from_the_DM9000A;
  output           avs_s1_export_ENET_CS_N_from_the_DM9000A;
  inout   [ 15: 0] avs_s1_export_ENET_DATA_to_and_from_the_DM9000A;
  output           avs_s1_export_ENET_RD_N_from_the_DM9000A;
  output           avs_s1_export_ENET_RST_N_from_the_DM9000A;
  output           avs_s1_export_ENET_WR_N_from_the_DM9000A;
  output  [ 63: 0] avs_s1_export_seg7_from_the_SEG7;
  output  [  3: 0] bw_n_to_the_ssram;
  output           bwe_n_to_the_ssram;
  output           chipenable1_n_to_the_ssram;
  inout   [ 31: 0] data_to_and_from_the_ssram;
  output  [ 31: 0] out_port_from_the_control_out_pio;
  output  [ 31: 0] out_port_from_the_data_out_pio;
  output           outputenable_n_to_the_ssram;
  output           pll_c0_out;
  output           pll_c1_out;
  output           pll_c2_audio;
  output           reset_n_to_the_ssram;
  input            avs_s1_export_ENET_INT_to_the_DM9000A;
  input            clk_25;
  input            clk_50;
  input   [ 31: 0] in_port_to_the_control_in_pio;
  input   [ 31: 0] in_port_to_the_data_in_pio;
  input   [  3: 0] in_port_to_the_pio_button;
  input   [ 17: 0] in_port_to_the_pio_switch;
  input            reset_n;

  wire    [  3: 0] DE2_70_SOPC_clock_0_in_address;
  wire    [  1: 0] DE2_70_SOPC_clock_0_in_byteenable;
  wire             DE2_70_SOPC_clock_0_in_endofpacket;
  wire             DE2_70_SOPC_clock_0_in_endofpacket_from_sa;
  wire    [  2: 0] DE2_70_SOPC_clock_0_in_nativeaddress;
  wire             DE2_70_SOPC_clock_0_in_read;
  wire    [ 15: 0] DE2_70_SOPC_clock_0_in_readdata;
  wire    [ 15: 0] DE2_70_SOPC_clock_0_in_readdata_from_sa;
  wire             DE2_70_SOPC_clock_0_in_reset_n;
  wire             DE2_70_SOPC_clock_0_in_waitrequest;
  wire             DE2_70_SOPC_clock_0_in_waitrequest_from_sa;
  wire             DE2_70_SOPC_clock_0_in_write;
  wire    [ 15: 0] DE2_70_SOPC_clock_0_in_writedata;
  wire    [  3: 0] DE2_70_SOPC_clock_0_out_address;
  wire    [  3: 0] DE2_70_SOPC_clock_0_out_address_to_slave;
  wire    [  1: 0] DE2_70_SOPC_clock_0_out_byteenable;
  wire             DE2_70_SOPC_clock_0_out_endofpacket;
  wire             DE2_70_SOPC_clock_0_out_granted_pll_s1;
  wire    [  2: 0] DE2_70_SOPC_clock_0_out_nativeaddress;
  wire             DE2_70_SOPC_clock_0_out_qualified_request_pll_s1;
  wire             DE2_70_SOPC_clock_0_out_read;
  wire             DE2_70_SOPC_clock_0_out_read_data_valid_pll_s1;
  wire    [ 15: 0] DE2_70_SOPC_clock_0_out_readdata;
  wire             DE2_70_SOPC_clock_0_out_requests_pll_s1;
  wire             DE2_70_SOPC_clock_0_out_reset_n;
  wire             DE2_70_SOPC_clock_0_out_waitrequest;
  wire             DE2_70_SOPC_clock_0_out_write;
  wire    [ 15: 0] DE2_70_SOPC_clock_0_out_writedata;
  wire    [  1: 0] DE2_70_SOPC_clock_1_in_address;
  wire    [  1: 0] DE2_70_SOPC_clock_1_in_byteenable;
  wire             DE2_70_SOPC_clock_1_in_endofpacket;
  wire             DE2_70_SOPC_clock_1_in_endofpacket_from_sa;
  wire             DE2_70_SOPC_clock_1_in_nativeaddress;
  wire             DE2_70_SOPC_clock_1_in_read;
  wire    [ 15: 0] DE2_70_SOPC_clock_1_in_readdata;
  wire    [ 15: 0] DE2_70_SOPC_clock_1_in_readdata_from_sa;
  wire             DE2_70_SOPC_clock_1_in_reset_n;
  wire             DE2_70_SOPC_clock_1_in_waitrequest;
  wire             DE2_70_SOPC_clock_1_in_waitrequest_from_sa;
  wire             DE2_70_SOPC_clock_1_in_write;
  wire    [ 15: 0] DE2_70_SOPC_clock_1_in_writedata;
  wire    [  1: 0] DE2_70_SOPC_clock_1_out_address;
  wire    [  1: 0] DE2_70_SOPC_clock_1_out_address_to_slave;
  wire    [  1: 0] DE2_70_SOPC_clock_1_out_byteenable;
  wire             DE2_70_SOPC_clock_1_out_endofpacket;
  wire             DE2_70_SOPC_clock_1_out_granted_DM9000A_s1;
  wire             DE2_70_SOPC_clock_1_out_nativeaddress;
  wire             DE2_70_SOPC_clock_1_out_qualified_request_DM9000A_s1;
  wire             DE2_70_SOPC_clock_1_out_read;
  wire             DE2_70_SOPC_clock_1_out_read_data_valid_DM9000A_s1;
  wire    [ 15: 0] DE2_70_SOPC_clock_1_out_readdata;
  wire             DE2_70_SOPC_clock_1_out_requests_DM9000A_s1;
  wire             DE2_70_SOPC_clock_1_out_reset_n;
  wire             DE2_70_SOPC_clock_1_out_waitrequest;
  wire             DE2_70_SOPC_clock_1_out_write;
  wire    [ 15: 0] DE2_70_SOPC_clock_1_out_writedata;
  wire    [  2: 0] DE2_70_SOPC_clock_2_in_address;
  wire    [  3: 0] DE2_70_SOPC_clock_2_in_byteenable;
  wire             DE2_70_SOPC_clock_2_in_endofpacket;
  wire             DE2_70_SOPC_clock_2_in_endofpacket_from_sa;
  wire             DE2_70_SOPC_clock_2_in_nativeaddress;
  wire             DE2_70_SOPC_clock_2_in_read;
  wire    [ 31: 0] DE2_70_SOPC_clock_2_in_readdata;
  wire    [ 31: 0] DE2_70_SOPC_clock_2_in_readdata_from_sa;
  wire             DE2_70_SOPC_clock_2_in_reset_n;
  wire             DE2_70_SOPC_clock_2_in_waitrequest;
  wire             DE2_70_SOPC_clock_2_in_waitrequest_from_sa;
  wire             DE2_70_SOPC_clock_2_in_write;
  wire    [ 31: 0] DE2_70_SOPC_clock_2_in_writedata;
  wire    [  2: 0] DE2_70_SOPC_clock_2_out_address;
  wire    [  2: 0] DE2_70_SOPC_clock_2_out_address_to_slave;
  wire    [  3: 0] DE2_70_SOPC_clock_2_out_byteenable;
  wire             DE2_70_SOPC_clock_2_out_endofpacket;
  wire             DE2_70_SOPC_clock_2_out_granted_clock_crossing_0_s1;
  wire             DE2_70_SOPC_clock_2_out_nativeaddress;
  wire             DE2_70_SOPC_clock_2_out_qualified_request_clock_crossing_0_s1;
  wire             DE2_70_SOPC_clock_2_out_read;
  wire             DE2_70_SOPC_clock_2_out_read_data_valid_clock_crossing_0_s1;
  wire             DE2_70_SOPC_clock_2_out_read_data_valid_clock_crossing_0_s1_shift_register;
  wire    [ 31: 0] DE2_70_SOPC_clock_2_out_readdata;
  wire             DE2_70_SOPC_clock_2_out_requests_clock_crossing_0_s1;
  wire             DE2_70_SOPC_clock_2_out_reset_n;
  wire             DE2_70_SOPC_clock_2_out_waitrequest;
  wire             DE2_70_SOPC_clock_2_out_write;
  wire    [ 31: 0] DE2_70_SOPC_clock_2_out_writedata;
  wire             DM9000A_s1_address;
  wire             DM9000A_s1_chipselect_n;
  wire             DM9000A_s1_irq;
  wire             DM9000A_s1_irq_from_sa;
  wire             DM9000A_s1_read_n;
  wire    [ 15: 0] DM9000A_s1_readdata;
  wire    [ 15: 0] DM9000A_s1_readdata_from_sa;
  wire             DM9000A_s1_reset_n;
  wire             DM9000A_s1_wait_counter_eq_0;
  wire             DM9000A_s1_write_n;
  wire    [ 15: 0] DM9000A_s1_writedata;
  wire             LCD_E_from_the_lcd;
  wire             LCD_RS_from_the_lcd;
  wire             LCD_RW_from_the_lcd;
  wire    [  7: 0] LCD_data_to_and_from_the_lcd;
  wire    [  2: 0] SEG7_s1_address;
  wire             SEG7_s1_read;
  wire    [  7: 0] SEG7_s1_readdata;
  wire    [  7: 0] SEG7_s1_readdata_from_sa;
  wire             SEG7_s1_reset;
  wire             SEG7_s1_write;
  wire    [  7: 0] SEG7_s1_writedata;
  wire    [ 20: 0] address_to_the_ssram;
  wire             adsc_n_to_the_ssram;
  wire             avs_s1_export_ENET_CLK_from_the_DM9000A;
  wire             avs_s1_export_ENET_CMD_from_the_DM9000A;
  wire             avs_s1_export_ENET_CS_N_from_the_DM9000A;
  wire    [ 15: 0] avs_s1_export_ENET_DATA_to_and_from_the_DM9000A;
  wire             avs_s1_export_ENET_RD_N_from_the_DM9000A;
  wire             avs_s1_export_ENET_RST_N_from_the_DM9000A;
  wire             avs_s1_export_ENET_WR_N_from_the_DM9000A;
  wire    [ 63: 0] avs_s1_export_seg7_from_the_SEG7;
  wire    [  3: 0] bw_n_to_the_ssram;
  wire             bwe_n_to_the_ssram;
  wire             chipenable1_n_to_the_ssram;
  wire             clk_25_reset_n;
  wire             clk_50_reset_n;
  wire    [  2: 0] clock_crossing_0_m1_address;
  wire    [  2: 0] clock_crossing_0_m1_address_to_slave;
  wire    [  3: 0] clock_crossing_0_m1_byteenable;
  wire             clock_crossing_0_m1_endofpacket;
  wire             clock_crossing_0_m1_granted_DE2_70_SOPC_clock_1_in;
  wire             clock_crossing_0_m1_latency_counter;
  wire             clock_crossing_0_m1_nativeaddress;
  wire             clock_crossing_0_m1_qualified_request_DE2_70_SOPC_clock_1_in;
  wire             clock_crossing_0_m1_read;
  wire             clock_crossing_0_m1_read_data_valid_DE2_70_SOPC_clock_1_in;
  wire    [ 31: 0] clock_crossing_0_m1_readdata;
  wire             clock_crossing_0_m1_readdatavalid;
  wire             clock_crossing_0_m1_requests_DE2_70_SOPC_clock_1_in;
  wire             clock_crossing_0_m1_reset_n;
  wire             clock_crossing_0_m1_waitrequest;
  wire             clock_crossing_0_m1_write;
  wire    [ 31: 0] clock_crossing_0_m1_writedata;
  wire             clock_crossing_0_s1_address;
  wire    [  3: 0] clock_crossing_0_s1_byteenable;
  wire             clock_crossing_0_s1_endofpacket;
  wire             clock_crossing_0_s1_endofpacket_from_sa;
  wire             clock_crossing_0_s1_nativeaddress;
  wire             clock_crossing_0_s1_read;
  wire    [ 31: 0] clock_crossing_0_s1_readdata;
  wire    [ 31: 0] clock_crossing_0_s1_readdata_from_sa;
  wire             clock_crossing_0_s1_readdatavalid;
  wire             clock_crossing_0_s1_reset_n;
  wire             clock_crossing_0_s1_waitrequest;
  wire             clock_crossing_0_s1_waitrequest_from_sa;
  wire             clock_crossing_0_s1_write;
  wire    [ 31: 0] clock_crossing_0_s1_writedata;
  wire    [  1: 0] control_in_pio_s1_address;
  wire             control_in_pio_s1_chipselect;
  wire    [ 31: 0] control_in_pio_s1_readdata;
  wire    [ 31: 0] control_in_pio_s1_readdata_from_sa;
  wire             control_in_pio_s1_reset_n;
  wire             control_in_pio_s1_write_n;
  wire    [ 31: 0] control_in_pio_s1_writedata;
  wire    [  2: 0] control_out_pio_s1_address;
  wire             control_out_pio_s1_chipselect;
  wire    [ 31: 0] control_out_pio_s1_readdata;
  wire    [ 31: 0] control_out_pio_s1_readdata_from_sa;
  wire             control_out_pio_s1_reset_n;
  wire             control_out_pio_s1_write_n;
  wire    [ 31: 0] control_out_pio_s1_writedata;
  wire    [ 24: 0] cpu_data_master_address;
  wire    [ 24: 0] cpu_data_master_address_to_slave;
  wire    [  3: 0] cpu_data_master_byteenable;
  wire             cpu_data_master_debugaccess;
  wire             cpu_data_master_granted_DE2_70_SOPC_clock_0_in;
  wire             cpu_data_master_granted_DE2_70_SOPC_clock_2_in;
  wire             cpu_data_master_granted_SEG7_s1;
  wire             cpu_data_master_granted_control_in_pio_s1;
  wire             cpu_data_master_granted_control_out_pio_s1;
  wire             cpu_data_master_granted_cpu_jtag_debug_module;
  wire             cpu_data_master_granted_data_in_pio_s1;
  wire             cpu_data_master_granted_data_out_pio_s1;
  wire             cpu_data_master_granted_epcs_flash_controller_0_epcs_control_port;
  wire             cpu_data_master_granted_jtag_uart_avalon_jtag_slave;
  wire             cpu_data_master_granted_lcd_control_slave;
  wire             cpu_data_master_granted_onchip_mem_s1;
  wire             cpu_data_master_granted_pio_button_s1;
  wire             cpu_data_master_granted_pio_switch_s1;
  wire             cpu_data_master_granted_ssram_s1;
  wire             cpu_data_master_granted_sysid_control_slave;
  wire             cpu_data_master_granted_timer_s1;
  wire             cpu_data_master_granted_timer_stamp_s1;
  wire    [ 31: 0] cpu_data_master_irq;
  wire    [  2: 0] cpu_data_master_latency_counter;
  wire             cpu_data_master_qualified_request_DE2_70_SOPC_clock_0_in;
  wire             cpu_data_master_qualified_request_DE2_70_SOPC_clock_2_in;
  wire             cpu_data_master_qualified_request_SEG7_s1;
  wire             cpu_data_master_qualified_request_control_in_pio_s1;
  wire             cpu_data_master_qualified_request_control_out_pio_s1;
  wire             cpu_data_master_qualified_request_cpu_jtag_debug_module;
  wire             cpu_data_master_qualified_request_data_in_pio_s1;
  wire             cpu_data_master_qualified_request_data_out_pio_s1;
  wire             cpu_data_master_qualified_request_epcs_flash_controller_0_epcs_control_port;
  wire             cpu_data_master_qualified_request_jtag_uart_avalon_jtag_slave;
  wire             cpu_data_master_qualified_request_lcd_control_slave;
  wire             cpu_data_master_qualified_request_onchip_mem_s1;
  wire             cpu_data_master_qualified_request_pio_button_s1;
  wire             cpu_data_master_qualified_request_pio_switch_s1;
  wire             cpu_data_master_qualified_request_ssram_s1;
  wire             cpu_data_master_qualified_request_sysid_control_slave;
  wire             cpu_data_master_qualified_request_timer_s1;
  wire             cpu_data_master_qualified_request_timer_stamp_s1;
  wire             cpu_data_master_read;
  wire             cpu_data_master_read_data_valid_DE2_70_SOPC_clock_0_in;
  wire             cpu_data_master_read_data_valid_DE2_70_SOPC_clock_2_in;
  wire             cpu_data_master_read_data_valid_SEG7_s1;
  wire             cpu_data_master_read_data_valid_control_in_pio_s1;
  wire             cpu_data_master_read_data_valid_control_out_pio_s1;
  wire             cpu_data_master_read_data_valid_cpu_jtag_debug_module;
  wire             cpu_data_master_read_data_valid_data_in_pio_s1;
  wire             cpu_data_master_read_data_valid_data_out_pio_s1;
  wire             cpu_data_master_read_data_valid_epcs_flash_controller_0_epcs_control_port;
  wire             cpu_data_master_read_data_valid_jtag_uart_avalon_jtag_slave;
  wire             cpu_data_master_read_data_valid_lcd_control_slave;
  wire             cpu_data_master_read_data_valid_onchip_mem_s1;
  wire             cpu_data_master_read_data_valid_pio_button_s1;
  wire             cpu_data_master_read_data_valid_pio_switch_s1;
  wire             cpu_data_master_read_data_valid_ssram_s1;
  wire             cpu_data_master_read_data_valid_sysid_control_slave;
  wire             cpu_data_master_read_data_valid_timer_s1;
  wire             cpu_data_master_read_data_valid_timer_stamp_s1;
  wire    [ 31: 0] cpu_data_master_readdata;
  wire             cpu_data_master_readdatavalid;
  wire             cpu_data_master_requests_DE2_70_SOPC_clock_0_in;
  wire             cpu_data_master_requests_DE2_70_SOPC_clock_2_in;
  wire             cpu_data_master_requests_SEG7_s1;
  wire             cpu_data_master_requests_control_in_pio_s1;
  wire             cpu_data_master_requests_control_out_pio_s1;
  wire             cpu_data_master_requests_cpu_jtag_debug_module;
  wire             cpu_data_master_requests_data_in_pio_s1;
  wire             cpu_data_master_requests_data_out_pio_s1;
  wire             cpu_data_master_requests_epcs_flash_controller_0_epcs_control_port;
  wire             cpu_data_master_requests_jtag_uart_avalon_jtag_slave;
  wire             cpu_data_master_requests_lcd_control_slave;
  wire             cpu_data_master_requests_onchip_mem_s1;
  wire             cpu_data_master_requests_pio_button_s1;
  wire             cpu_data_master_requests_pio_switch_s1;
  wire             cpu_data_master_requests_ssram_s1;
  wire             cpu_data_master_requests_sysid_control_slave;
  wire             cpu_data_master_requests_timer_s1;
  wire             cpu_data_master_requests_timer_stamp_s1;
  wire             cpu_data_master_waitrequest;
  wire             cpu_data_master_write;
  wire    [ 31: 0] cpu_data_master_writedata;
  wire    [ 24: 0] cpu_instruction_master_address;
  wire    [ 24: 0] cpu_instruction_master_address_to_slave;
  wire             cpu_instruction_master_granted_cpu_jtag_debug_module;
  wire             cpu_instruction_master_granted_epcs_flash_controller_0_epcs_control_port;
  wire             cpu_instruction_master_granted_onchip_mem_s1;
  wire             cpu_instruction_master_granted_ssram_s1;
  wire    [  2: 0] cpu_instruction_master_latency_counter;
  wire             cpu_instruction_master_qualified_request_cpu_jtag_debug_module;
  wire             cpu_instruction_master_qualified_request_epcs_flash_controller_0_epcs_control_port;
  wire             cpu_instruction_master_qualified_request_onchip_mem_s1;
  wire             cpu_instruction_master_qualified_request_ssram_s1;
  wire             cpu_instruction_master_read;
  wire             cpu_instruction_master_read_data_valid_cpu_jtag_debug_module;
  wire             cpu_instruction_master_read_data_valid_epcs_flash_controller_0_epcs_control_port;
  wire             cpu_instruction_master_read_data_valid_onchip_mem_s1;
  wire             cpu_instruction_master_read_data_valid_ssram_s1;
  wire    [ 31: 0] cpu_instruction_master_readdata;
  wire             cpu_instruction_master_readdatavalid;
  wire             cpu_instruction_master_requests_cpu_jtag_debug_module;
  wire             cpu_instruction_master_requests_epcs_flash_controller_0_epcs_control_port;
  wire             cpu_instruction_master_requests_onchip_mem_s1;
  wire             cpu_instruction_master_requests_ssram_s1;
  wire             cpu_instruction_master_waitrequest;
  wire    [  8: 0] cpu_jtag_debug_module_address;
  wire             cpu_jtag_debug_module_begintransfer;
  wire    [  3: 0] cpu_jtag_debug_module_byteenable;
  wire             cpu_jtag_debug_module_chipselect;
  wire             cpu_jtag_debug_module_debugaccess;
  wire    [ 31: 0] cpu_jtag_debug_module_readdata;
  wire    [ 31: 0] cpu_jtag_debug_module_readdata_from_sa;
  wire             cpu_jtag_debug_module_reset_n;
  wire             cpu_jtag_debug_module_resetrequest;
  wire             cpu_jtag_debug_module_resetrequest_from_sa;
  wire             cpu_jtag_debug_module_write;
  wire    [ 31: 0] cpu_jtag_debug_module_writedata;
  wire             d1_DE2_70_SOPC_clock_0_in_end_xfer;
  wire             d1_DE2_70_SOPC_clock_1_in_end_xfer;
  wire             d1_DE2_70_SOPC_clock_2_in_end_xfer;
  wire             d1_DM9000A_s1_end_xfer;
  wire             d1_SEG7_s1_end_xfer;
  wire             d1_clock_crossing_0_s1_end_xfer;
  wire             d1_control_in_pio_s1_end_xfer;
  wire             d1_control_out_pio_s1_end_xfer;
  wire             d1_cpu_jtag_debug_module_end_xfer;
  wire             d1_data_in_pio_s1_end_xfer;
  wire             d1_data_out_pio_s1_end_xfer;
  wire             d1_epcs_flash_controller_0_epcs_control_port_end_xfer;
  wire             d1_jtag_uart_avalon_jtag_slave_end_xfer;
  wire             d1_lcd_control_slave_end_xfer;
  wire             d1_onchip_mem_s1_end_xfer;
  wire             d1_pio_button_s1_end_xfer;
  wire             d1_pio_switch_s1_end_xfer;
  wire             d1_pll_s1_end_xfer;
  wire             d1_sysid_control_slave_end_xfer;
  wire             d1_timer_s1_end_xfer;
  wire             d1_timer_stamp_s1_end_xfer;
  wire             d1_tristate_bridge_ssram_avalon_slave_end_xfer;
  wire    [  1: 0] data_in_pio_s1_address;
  wire             data_in_pio_s1_chipselect;
  wire    [ 31: 0] data_in_pio_s1_readdata;
  wire    [ 31: 0] data_in_pio_s1_readdata_from_sa;
  wire             data_in_pio_s1_reset_n;
  wire             data_in_pio_s1_write_n;
  wire    [ 31: 0] data_in_pio_s1_writedata;
  wire    [  2: 0] data_out_pio_s1_address;
  wire             data_out_pio_s1_chipselect;
  wire    [ 31: 0] data_out_pio_s1_readdata;
  wire    [ 31: 0] data_out_pio_s1_readdata_from_sa;
  wire             data_out_pio_s1_reset_n;
  wire             data_out_pio_s1_write_n;
  wire    [ 31: 0] data_out_pio_s1_writedata;
  wire    [ 31: 0] data_to_and_from_the_ssram;
  wire    [  8: 0] epcs_flash_controller_0_epcs_control_port_address;
  wire             epcs_flash_controller_0_epcs_control_port_chipselect;
  wire             epcs_flash_controller_0_epcs_control_port_dataavailable;
  wire             epcs_flash_controller_0_epcs_control_port_dataavailable_from_sa;
  wire             epcs_flash_controller_0_epcs_control_port_endofpacket;
  wire             epcs_flash_controller_0_epcs_control_port_endofpacket_from_sa;
  wire             epcs_flash_controller_0_epcs_control_port_irq;
  wire             epcs_flash_controller_0_epcs_control_port_irq_from_sa;
  wire             epcs_flash_controller_0_epcs_control_port_read_n;
  wire    [ 31: 0] epcs_flash_controller_0_epcs_control_port_readdata;
  wire    [ 31: 0] epcs_flash_controller_0_epcs_control_port_readdata_from_sa;
  wire             epcs_flash_controller_0_epcs_control_port_readyfordata;
  wire             epcs_flash_controller_0_epcs_control_port_readyfordata_from_sa;
  wire             epcs_flash_controller_0_epcs_control_port_reset_n;
  wire             epcs_flash_controller_0_epcs_control_port_write_n;
  wire    [ 31: 0] epcs_flash_controller_0_epcs_control_port_writedata;
  wire    [ 31: 0] incoming_data_to_and_from_the_ssram;
  wire             jtag_uart_avalon_jtag_slave_address;
  wire             jtag_uart_avalon_jtag_slave_chipselect;
  wire             jtag_uart_avalon_jtag_slave_dataavailable;
  wire             jtag_uart_avalon_jtag_slave_dataavailable_from_sa;
  wire             jtag_uart_avalon_jtag_slave_irq;
  wire             jtag_uart_avalon_jtag_slave_irq_from_sa;
  wire             jtag_uart_avalon_jtag_slave_read_n;
  wire    [ 31: 0] jtag_uart_avalon_jtag_slave_readdata;
  wire    [ 31: 0] jtag_uart_avalon_jtag_slave_readdata_from_sa;
  wire             jtag_uart_avalon_jtag_slave_readyfordata;
  wire             jtag_uart_avalon_jtag_slave_readyfordata_from_sa;
  wire             jtag_uart_avalon_jtag_slave_reset_n;
  wire             jtag_uart_avalon_jtag_slave_waitrequest;
  wire             jtag_uart_avalon_jtag_slave_waitrequest_from_sa;
  wire             jtag_uart_avalon_jtag_slave_write_n;
  wire    [ 31: 0] jtag_uart_avalon_jtag_slave_writedata;
  wire    [  1: 0] lcd_control_slave_address;
  wire             lcd_control_slave_begintransfer;
  wire             lcd_control_slave_read;
  wire    [  7: 0] lcd_control_slave_readdata;
  wire    [  7: 0] lcd_control_slave_readdata_from_sa;
  wire             lcd_control_slave_reset_n;
  wire             lcd_control_slave_wait_counter_eq_0;
  wire             lcd_control_slave_write;
  wire    [  7: 0] lcd_control_slave_writedata;
  wire    [ 11: 0] onchip_mem_s1_address;
  wire    [  3: 0] onchip_mem_s1_byteenable;
  wire             onchip_mem_s1_chipselect;
  wire             onchip_mem_s1_clken;
  wire    [ 31: 0] onchip_mem_s1_readdata;
  wire    [ 31: 0] onchip_mem_s1_readdata_from_sa;
  wire             onchip_mem_s1_reset;
  wire             onchip_mem_s1_write;
  wire    [ 31: 0] onchip_mem_s1_writedata;
  wire             out_clk_pll_c0;
  wire             out_clk_pll_c1;
  wire             out_clk_pll_c2;
  wire    [ 31: 0] out_port_from_the_control_out_pio;
  wire    [ 31: 0] out_port_from_the_data_out_pio;
  wire             outputenable_n_to_the_ssram;
  wire    [  1: 0] pio_button_s1_address;
  wire             pio_button_s1_chipselect;
  wire             pio_button_s1_irq;
  wire             pio_button_s1_irq_from_sa;
  wire    [ 31: 0] pio_button_s1_readdata;
  wire    [ 31: 0] pio_button_s1_readdata_from_sa;
  wire             pio_button_s1_reset_n;
  wire             pio_button_s1_write_n;
  wire    [ 31: 0] pio_button_s1_writedata;
  wire    [  1: 0] pio_switch_s1_address;
  wire    [ 31: 0] pio_switch_s1_readdata;
  wire    [ 31: 0] pio_switch_s1_readdata_from_sa;
  wire             pio_switch_s1_reset_n;
  wire             pll_c0_out;
  wire             pll_c0_out_reset_n;
  wire             pll_c1_out;
  wire             pll_c2_audio;
  wire    [  2: 0] pll_s1_address;
  wire             pll_s1_chipselect;
  wire             pll_s1_read;
  wire    [ 15: 0] pll_s1_readdata;
  wire    [ 15: 0] pll_s1_readdata_from_sa;
  wire             pll_s1_reset_n;
  wire             pll_s1_resetrequest;
  wire             pll_s1_resetrequest_from_sa;
  wire             pll_s1_write;
  wire    [ 15: 0] pll_s1_writedata;
  wire             reset_n_sources;
  wire             reset_n_to_the_ssram;
  wire             sysid_control_slave_address;
  wire             sysid_control_slave_clock;
  wire    [ 31: 0] sysid_control_slave_readdata;
  wire    [ 31: 0] sysid_control_slave_readdata_from_sa;
  wire             sysid_control_slave_reset_n;
  wire    [  2: 0] timer_s1_address;
  wire             timer_s1_chipselect;
  wire             timer_s1_irq;
  wire             timer_s1_irq_from_sa;
  wire    [ 15: 0] timer_s1_readdata;
  wire    [ 15: 0] timer_s1_readdata_from_sa;
  wire             timer_s1_reset_n;
  wire             timer_s1_write_n;
  wire    [ 15: 0] timer_s1_writedata;
  wire    [  2: 0] timer_stamp_s1_address;
  wire             timer_stamp_s1_chipselect;
  wire             timer_stamp_s1_irq;
  wire             timer_stamp_s1_irq_from_sa;
  wire    [ 15: 0] timer_stamp_s1_readdata;
  wire    [ 15: 0] timer_stamp_s1_readdata_from_sa;
  wire             timer_stamp_s1_reset_n;
  wire             timer_stamp_s1_write_n;
  wire    [ 15: 0] timer_stamp_s1_writedata;
  DE2_70_SOPC_clock_0_in_arbitrator the_DE2_70_SOPC_clock_0_in
    (
      .DE2_70_SOPC_clock_0_in_address                           (DE2_70_SOPC_clock_0_in_address),
      .DE2_70_SOPC_clock_0_in_byteenable                        (DE2_70_SOPC_clock_0_in_byteenable),
      .DE2_70_SOPC_clock_0_in_endofpacket                       (DE2_70_SOPC_clock_0_in_endofpacket),
      .DE2_70_SOPC_clock_0_in_endofpacket_from_sa               (DE2_70_SOPC_clock_0_in_endofpacket_from_sa),
      .DE2_70_SOPC_clock_0_in_nativeaddress                     (DE2_70_SOPC_clock_0_in_nativeaddress),
      .DE2_70_SOPC_clock_0_in_read                              (DE2_70_SOPC_clock_0_in_read),
      .DE2_70_SOPC_clock_0_in_readdata                          (DE2_70_SOPC_clock_0_in_readdata),
      .DE2_70_SOPC_clock_0_in_readdata_from_sa                  (DE2_70_SOPC_clock_0_in_readdata_from_sa),
      .DE2_70_SOPC_clock_0_in_reset_n                           (DE2_70_SOPC_clock_0_in_reset_n),
      .DE2_70_SOPC_clock_0_in_waitrequest                       (DE2_70_SOPC_clock_0_in_waitrequest),
      .DE2_70_SOPC_clock_0_in_waitrequest_from_sa               (DE2_70_SOPC_clock_0_in_waitrequest_from_sa),
      .DE2_70_SOPC_clock_0_in_write                             (DE2_70_SOPC_clock_0_in_write),
      .DE2_70_SOPC_clock_0_in_writedata                         (DE2_70_SOPC_clock_0_in_writedata),
      .clk                                                      (pll_c0_out),
      .cpu_data_master_address_to_slave                         (cpu_data_master_address_to_slave),
      .cpu_data_master_byteenable                               (cpu_data_master_byteenable),
      .cpu_data_master_granted_DE2_70_SOPC_clock_0_in           (cpu_data_master_granted_DE2_70_SOPC_clock_0_in),
      .cpu_data_master_latency_counter                          (cpu_data_master_latency_counter),
      .cpu_data_master_qualified_request_DE2_70_SOPC_clock_0_in (cpu_data_master_qualified_request_DE2_70_SOPC_clock_0_in),
      .cpu_data_master_read                                     (cpu_data_master_read),
      .cpu_data_master_read_data_valid_DE2_70_SOPC_clock_0_in   (cpu_data_master_read_data_valid_DE2_70_SOPC_clock_0_in),
      .cpu_data_master_requests_DE2_70_SOPC_clock_0_in          (cpu_data_master_requests_DE2_70_SOPC_clock_0_in),
      .cpu_data_master_write                                    (cpu_data_master_write),
      .cpu_data_master_writedata                                (cpu_data_master_writedata),
      .d1_DE2_70_SOPC_clock_0_in_end_xfer                       (d1_DE2_70_SOPC_clock_0_in_end_xfer),
      .reset_n                                                  (pll_c0_out_reset_n)
    );

  DE2_70_SOPC_clock_0_out_arbitrator the_DE2_70_SOPC_clock_0_out
    (
      .DE2_70_SOPC_clock_0_out_address                  (DE2_70_SOPC_clock_0_out_address),
      .DE2_70_SOPC_clock_0_out_address_to_slave         (DE2_70_SOPC_clock_0_out_address_to_slave),
      .DE2_70_SOPC_clock_0_out_byteenable               (DE2_70_SOPC_clock_0_out_byteenable),
      .DE2_70_SOPC_clock_0_out_granted_pll_s1           (DE2_70_SOPC_clock_0_out_granted_pll_s1),
      .DE2_70_SOPC_clock_0_out_qualified_request_pll_s1 (DE2_70_SOPC_clock_0_out_qualified_request_pll_s1),
      .DE2_70_SOPC_clock_0_out_read                     (DE2_70_SOPC_clock_0_out_read),
      .DE2_70_SOPC_clock_0_out_read_data_valid_pll_s1   (DE2_70_SOPC_clock_0_out_read_data_valid_pll_s1),
      .DE2_70_SOPC_clock_0_out_readdata                 (DE2_70_SOPC_clock_0_out_readdata),
      .DE2_70_SOPC_clock_0_out_requests_pll_s1          (DE2_70_SOPC_clock_0_out_requests_pll_s1),
      .DE2_70_SOPC_clock_0_out_reset_n                  (DE2_70_SOPC_clock_0_out_reset_n),
      .DE2_70_SOPC_clock_0_out_waitrequest              (DE2_70_SOPC_clock_0_out_waitrequest),
      .DE2_70_SOPC_clock_0_out_write                    (DE2_70_SOPC_clock_0_out_write),
      .DE2_70_SOPC_clock_0_out_writedata                (DE2_70_SOPC_clock_0_out_writedata),
      .clk                                              (clk_50),
      .d1_pll_s1_end_xfer                               (d1_pll_s1_end_xfer),
      .pll_s1_readdata_from_sa                          (pll_s1_readdata_from_sa),
      .reset_n                                          (clk_50_reset_n)
    );

  DE2_70_SOPC_clock_0 the_DE2_70_SOPC_clock_0
    (
      .master_address       (DE2_70_SOPC_clock_0_out_address),
      .master_byteenable    (DE2_70_SOPC_clock_0_out_byteenable),
      .master_clk           (clk_50),
      .master_endofpacket   (DE2_70_SOPC_clock_0_out_endofpacket),
      .master_nativeaddress (DE2_70_SOPC_clock_0_out_nativeaddress),
      .master_read          (DE2_70_SOPC_clock_0_out_read),
      .master_readdata      (DE2_70_SOPC_clock_0_out_readdata),
      .master_reset_n       (DE2_70_SOPC_clock_0_out_reset_n),
      .master_waitrequest   (DE2_70_SOPC_clock_0_out_waitrequest),
      .master_write         (DE2_70_SOPC_clock_0_out_write),
      .master_writedata     (DE2_70_SOPC_clock_0_out_writedata),
      .slave_address        (DE2_70_SOPC_clock_0_in_address),
      .slave_byteenable     (DE2_70_SOPC_clock_0_in_byteenable),
      .slave_clk            (pll_c0_out),
      .slave_endofpacket    (DE2_70_SOPC_clock_0_in_endofpacket),
      .slave_nativeaddress  (DE2_70_SOPC_clock_0_in_nativeaddress),
      .slave_read           (DE2_70_SOPC_clock_0_in_read),
      .slave_readdata       (DE2_70_SOPC_clock_0_in_readdata),
      .slave_reset_n        (DE2_70_SOPC_clock_0_in_reset_n),
      .slave_waitrequest    (DE2_70_SOPC_clock_0_in_waitrequest),
      .slave_write          (DE2_70_SOPC_clock_0_in_write),
      .slave_writedata      (DE2_70_SOPC_clock_0_in_writedata)
    );

  DE2_70_SOPC_clock_1_in_arbitrator the_DE2_70_SOPC_clock_1_in
    (
      .DE2_70_SOPC_clock_1_in_address                               (DE2_70_SOPC_clock_1_in_address),
      .DE2_70_SOPC_clock_1_in_byteenable                            (DE2_70_SOPC_clock_1_in_byteenable),
      .DE2_70_SOPC_clock_1_in_endofpacket                           (DE2_70_SOPC_clock_1_in_endofpacket),
      .DE2_70_SOPC_clock_1_in_endofpacket_from_sa                   (DE2_70_SOPC_clock_1_in_endofpacket_from_sa),
      .DE2_70_SOPC_clock_1_in_nativeaddress                         (DE2_70_SOPC_clock_1_in_nativeaddress),
      .DE2_70_SOPC_clock_1_in_read                                  (DE2_70_SOPC_clock_1_in_read),
      .DE2_70_SOPC_clock_1_in_readdata                              (DE2_70_SOPC_clock_1_in_readdata),
      .DE2_70_SOPC_clock_1_in_readdata_from_sa                      (DE2_70_SOPC_clock_1_in_readdata_from_sa),
      .DE2_70_SOPC_clock_1_in_reset_n                               (DE2_70_SOPC_clock_1_in_reset_n),
      .DE2_70_SOPC_clock_1_in_waitrequest                           (DE2_70_SOPC_clock_1_in_waitrequest),
      .DE2_70_SOPC_clock_1_in_waitrequest_from_sa                   (DE2_70_SOPC_clock_1_in_waitrequest_from_sa),
      .DE2_70_SOPC_clock_1_in_write                                 (DE2_70_SOPC_clock_1_in_write),
      .DE2_70_SOPC_clock_1_in_writedata                             (DE2_70_SOPC_clock_1_in_writedata),
      .clk                                                          (pll_c0_out),
      .clock_crossing_0_m1_address_to_slave                         (clock_crossing_0_m1_address_to_slave),
      .clock_crossing_0_m1_byteenable                               (clock_crossing_0_m1_byteenable),
      .clock_crossing_0_m1_granted_DE2_70_SOPC_clock_1_in           (clock_crossing_0_m1_granted_DE2_70_SOPC_clock_1_in),
      .clock_crossing_0_m1_latency_counter                          (clock_crossing_0_m1_latency_counter),
      .clock_crossing_0_m1_nativeaddress                            (clock_crossing_0_m1_nativeaddress),
      .clock_crossing_0_m1_qualified_request_DE2_70_SOPC_clock_1_in (clock_crossing_0_m1_qualified_request_DE2_70_SOPC_clock_1_in),
      .clock_crossing_0_m1_read                                     (clock_crossing_0_m1_read),
      .clock_crossing_0_m1_read_data_valid_DE2_70_SOPC_clock_1_in   (clock_crossing_0_m1_read_data_valid_DE2_70_SOPC_clock_1_in),
      .clock_crossing_0_m1_requests_DE2_70_SOPC_clock_1_in          (clock_crossing_0_m1_requests_DE2_70_SOPC_clock_1_in),
      .clock_crossing_0_m1_write                                    (clock_crossing_0_m1_write),
      .clock_crossing_0_m1_writedata                                (clock_crossing_0_m1_writedata),
      .d1_DE2_70_SOPC_clock_1_in_end_xfer                           (d1_DE2_70_SOPC_clock_1_in_end_xfer),
      .reset_n                                                      (pll_c0_out_reset_n)
    );

  DE2_70_SOPC_clock_1_out_arbitrator the_DE2_70_SOPC_clock_1_out
    (
      .DE2_70_SOPC_clock_1_out_address                      (DE2_70_SOPC_clock_1_out_address),
      .DE2_70_SOPC_clock_1_out_address_to_slave             (DE2_70_SOPC_clock_1_out_address_to_slave),
      .DE2_70_SOPC_clock_1_out_byteenable                   (DE2_70_SOPC_clock_1_out_byteenable),
      .DE2_70_SOPC_clock_1_out_granted_DM9000A_s1           (DE2_70_SOPC_clock_1_out_granted_DM9000A_s1),
      .DE2_70_SOPC_clock_1_out_qualified_request_DM9000A_s1 (DE2_70_SOPC_clock_1_out_qualified_request_DM9000A_s1),
      .DE2_70_SOPC_clock_1_out_read                         (DE2_70_SOPC_clock_1_out_read),
      .DE2_70_SOPC_clock_1_out_read_data_valid_DM9000A_s1   (DE2_70_SOPC_clock_1_out_read_data_valid_DM9000A_s1),
      .DE2_70_SOPC_clock_1_out_readdata                     (DE2_70_SOPC_clock_1_out_readdata),
      .DE2_70_SOPC_clock_1_out_requests_DM9000A_s1          (DE2_70_SOPC_clock_1_out_requests_DM9000A_s1),
      .DE2_70_SOPC_clock_1_out_reset_n                      (DE2_70_SOPC_clock_1_out_reset_n),
      .DE2_70_SOPC_clock_1_out_waitrequest                  (DE2_70_SOPC_clock_1_out_waitrequest),
      .DE2_70_SOPC_clock_1_out_write                        (DE2_70_SOPC_clock_1_out_write),
      .DE2_70_SOPC_clock_1_out_writedata                    (DE2_70_SOPC_clock_1_out_writedata),
      .DM9000A_s1_readdata_from_sa                          (DM9000A_s1_readdata_from_sa),
      .DM9000A_s1_wait_counter_eq_0                         (DM9000A_s1_wait_counter_eq_0),
      .clk                                                  (clk_25),
      .d1_DM9000A_s1_end_xfer                               (d1_DM9000A_s1_end_xfer),
      .reset_n                                              (clk_25_reset_n)
    );

  DE2_70_SOPC_clock_1 the_DE2_70_SOPC_clock_1
    (
      .master_address       (DE2_70_SOPC_clock_1_out_address),
      .master_byteenable    (DE2_70_SOPC_clock_1_out_byteenable),
      .master_clk           (clk_25),
      .master_endofpacket   (DE2_70_SOPC_clock_1_out_endofpacket),
      .master_nativeaddress (DE2_70_SOPC_clock_1_out_nativeaddress),
      .master_read          (DE2_70_SOPC_clock_1_out_read),
      .master_readdata      (DE2_70_SOPC_clock_1_out_readdata),
      .master_reset_n       (DE2_70_SOPC_clock_1_out_reset_n),
      .master_waitrequest   (DE2_70_SOPC_clock_1_out_waitrequest),
      .master_write         (DE2_70_SOPC_clock_1_out_write),
      .master_writedata     (DE2_70_SOPC_clock_1_out_writedata),
      .slave_address        (DE2_70_SOPC_clock_1_in_address),
      .slave_byteenable     (DE2_70_SOPC_clock_1_in_byteenable),
      .slave_clk            (pll_c0_out),
      .slave_endofpacket    (DE2_70_SOPC_clock_1_in_endofpacket),
      .slave_nativeaddress  (DE2_70_SOPC_clock_1_in_nativeaddress),
      .slave_read           (DE2_70_SOPC_clock_1_in_read),
      .slave_readdata       (DE2_70_SOPC_clock_1_in_readdata),
      .slave_reset_n        (DE2_70_SOPC_clock_1_in_reset_n),
      .slave_waitrequest    (DE2_70_SOPC_clock_1_in_waitrequest),
      .slave_write          (DE2_70_SOPC_clock_1_in_write),
      .slave_writedata      (DE2_70_SOPC_clock_1_in_writedata)
    );

  DE2_70_SOPC_clock_2_in_arbitrator the_DE2_70_SOPC_clock_2_in
    (
      .DE2_70_SOPC_clock_2_in_address                           (DE2_70_SOPC_clock_2_in_address),
      .DE2_70_SOPC_clock_2_in_byteenable                        (DE2_70_SOPC_clock_2_in_byteenable),
      .DE2_70_SOPC_clock_2_in_endofpacket                       (DE2_70_SOPC_clock_2_in_endofpacket),
      .DE2_70_SOPC_clock_2_in_endofpacket_from_sa               (DE2_70_SOPC_clock_2_in_endofpacket_from_sa),
      .DE2_70_SOPC_clock_2_in_nativeaddress                     (DE2_70_SOPC_clock_2_in_nativeaddress),
      .DE2_70_SOPC_clock_2_in_read                              (DE2_70_SOPC_clock_2_in_read),
      .DE2_70_SOPC_clock_2_in_readdata                          (DE2_70_SOPC_clock_2_in_readdata),
      .DE2_70_SOPC_clock_2_in_readdata_from_sa                  (DE2_70_SOPC_clock_2_in_readdata_from_sa),
      .DE2_70_SOPC_clock_2_in_reset_n                           (DE2_70_SOPC_clock_2_in_reset_n),
      .DE2_70_SOPC_clock_2_in_waitrequest                       (DE2_70_SOPC_clock_2_in_waitrequest),
      .DE2_70_SOPC_clock_2_in_waitrequest_from_sa               (DE2_70_SOPC_clock_2_in_waitrequest_from_sa),
      .DE2_70_SOPC_clock_2_in_write                             (DE2_70_SOPC_clock_2_in_write),
      .DE2_70_SOPC_clock_2_in_writedata                         (DE2_70_SOPC_clock_2_in_writedata),
      .clk                                                      (pll_c0_out),
      .cpu_data_master_address_to_slave                         (cpu_data_master_address_to_slave),
      .cpu_data_master_byteenable                               (cpu_data_master_byteenable),
      .cpu_data_master_granted_DE2_70_SOPC_clock_2_in           (cpu_data_master_granted_DE2_70_SOPC_clock_2_in),
      .cpu_data_master_latency_counter                          (cpu_data_master_latency_counter),
      .cpu_data_master_qualified_request_DE2_70_SOPC_clock_2_in (cpu_data_master_qualified_request_DE2_70_SOPC_clock_2_in),
      .cpu_data_master_read                                     (cpu_data_master_read),
      .cpu_data_master_read_data_valid_DE2_70_SOPC_clock_2_in   (cpu_data_master_read_data_valid_DE2_70_SOPC_clock_2_in),
      .cpu_data_master_requests_DE2_70_SOPC_clock_2_in          (cpu_data_master_requests_DE2_70_SOPC_clock_2_in),
      .cpu_data_master_write                                    (cpu_data_master_write),
      .cpu_data_master_writedata                                (cpu_data_master_writedata),
      .d1_DE2_70_SOPC_clock_2_in_end_xfer                       (d1_DE2_70_SOPC_clock_2_in_end_xfer),
      .reset_n                                                  (pll_c0_out_reset_n)
    );

  DE2_70_SOPC_clock_2_out_arbitrator the_DE2_70_SOPC_clock_2_out
    (
      .DE2_70_SOPC_clock_2_out_address                                            (DE2_70_SOPC_clock_2_out_address),
      .DE2_70_SOPC_clock_2_out_address_to_slave                                   (DE2_70_SOPC_clock_2_out_address_to_slave),
      .DE2_70_SOPC_clock_2_out_byteenable                                         (DE2_70_SOPC_clock_2_out_byteenable),
      .DE2_70_SOPC_clock_2_out_endofpacket                                        (DE2_70_SOPC_clock_2_out_endofpacket),
      .DE2_70_SOPC_clock_2_out_granted_clock_crossing_0_s1                        (DE2_70_SOPC_clock_2_out_granted_clock_crossing_0_s1),
      .DE2_70_SOPC_clock_2_out_qualified_request_clock_crossing_0_s1              (DE2_70_SOPC_clock_2_out_qualified_request_clock_crossing_0_s1),
      .DE2_70_SOPC_clock_2_out_read                                               (DE2_70_SOPC_clock_2_out_read),
      .DE2_70_SOPC_clock_2_out_read_data_valid_clock_crossing_0_s1                (DE2_70_SOPC_clock_2_out_read_data_valid_clock_crossing_0_s1),
      .DE2_70_SOPC_clock_2_out_read_data_valid_clock_crossing_0_s1_shift_register (DE2_70_SOPC_clock_2_out_read_data_valid_clock_crossing_0_s1_shift_register),
      .DE2_70_SOPC_clock_2_out_readdata                                           (DE2_70_SOPC_clock_2_out_readdata),
      .DE2_70_SOPC_clock_2_out_requests_clock_crossing_0_s1                       (DE2_70_SOPC_clock_2_out_requests_clock_crossing_0_s1),
      .DE2_70_SOPC_clock_2_out_reset_n                                            (DE2_70_SOPC_clock_2_out_reset_n),
      .DE2_70_SOPC_clock_2_out_waitrequest                                        (DE2_70_SOPC_clock_2_out_waitrequest),
      .DE2_70_SOPC_clock_2_out_write                                              (DE2_70_SOPC_clock_2_out_write),
      .DE2_70_SOPC_clock_2_out_writedata                                          (DE2_70_SOPC_clock_2_out_writedata),
      .clk                                                                        (clk_25),
      .clock_crossing_0_s1_endofpacket_from_sa                                    (clock_crossing_0_s1_endofpacket_from_sa),
      .clock_crossing_0_s1_readdata_from_sa                                       (clock_crossing_0_s1_readdata_from_sa),
      .clock_crossing_0_s1_waitrequest_from_sa                                    (clock_crossing_0_s1_waitrequest_from_sa),
      .d1_clock_crossing_0_s1_end_xfer                                            (d1_clock_crossing_0_s1_end_xfer),
      .reset_n                                                                    (clk_25_reset_n)
    );

  DE2_70_SOPC_clock_2 the_DE2_70_SOPC_clock_2
    (
      .master_address       (DE2_70_SOPC_clock_2_out_address),
      .master_byteenable    (DE2_70_SOPC_clock_2_out_byteenable),
      .master_clk           (clk_25),
      .master_endofpacket   (DE2_70_SOPC_clock_2_out_endofpacket),
      .master_nativeaddress (DE2_70_SOPC_clock_2_out_nativeaddress),
      .master_read          (DE2_70_SOPC_clock_2_out_read),
      .master_readdata      (DE2_70_SOPC_clock_2_out_readdata),
      .master_reset_n       (DE2_70_SOPC_clock_2_out_reset_n),
      .master_waitrequest   (DE2_70_SOPC_clock_2_out_waitrequest),
      .master_write         (DE2_70_SOPC_clock_2_out_write),
      .master_writedata     (DE2_70_SOPC_clock_2_out_writedata),
      .slave_address        (DE2_70_SOPC_clock_2_in_address),
      .slave_byteenable     (DE2_70_SOPC_clock_2_in_byteenable),
      .slave_clk            (pll_c0_out),
      .slave_endofpacket    (DE2_70_SOPC_clock_2_in_endofpacket),
      .slave_nativeaddress  (DE2_70_SOPC_clock_2_in_nativeaddress),
      .slave_read           (DE2_70_SOPC_clock_2_in_read),
      .slave_readdata       (DE2_70_SOPC_clock_2_in_readdata),
      .slave_reset_n        (DE2_70_SOPC_clock_2_in_reset_n),
      .slave_waitrequest    (DE2_70_SOPC_clock_2_in_waitrequest),
      .slave_write          (DE2_70_SOPC_clock_2_in_write),
      .slave_writedata      (DE2_70_SOPC_clock_2_in_writedata)
    );

  DM9000A_s1_arbitrator the_DM9000A_s1
    (
      .DE2_70_SOPC_clock_1_out_address_to_slave             (DE2_70_SOPC_clock_1_out_address_to_slave),
      .DE2_70_SOPC_clock_1_out_granted_DM9000A_s1           (DE2_70_SOPC_clock_1_out_granted_DM9000A_s1),
      .DE2_70_SOPC_clock_1_out_nativeaddress                (DE2_70_SOPC_clock_1_out_nativeaddress),
      .DE2_70_SOPC_clock_1_out_qualified_request_DM9000A_s1 (DE2_70_SOPC_clock_1_out_qualified_request_DM9000A_s1),
      .DE2_70_SOPC_clock_1_out_read                         (DE2_70_SOPC_clock_1_out_read),
      .DE2_70_SOPC_clock_1_out_read_data_valid_DM9000A_s1   (DE2_70_SOPC_clock_1_out_read_data_valid_DM9000A_s1),
      .DE2_70_SOPC_clock_1_out_requests_DM9000A_s1          (DE2_70_SOPC_clock_1_out_requests_DM9000A_s1),
      .DE2_70_SOPC_clock_1_out_write                        (DE2_70_SOPC_clock_1_out_write),
      .DE2_70_SOPC_clock_1_out_writedata                    (DE2_70_SOPC_clock_1_out_writedata),
      .DM9000A_s1_address                                   (DM9000A_s1_address),
      .DM9000A_s1_chipselect_n                              (DM9000A_s1_chipselect_n),
      .DM9000A_s1_irq                                       (DM9000A_s1_irq),
      .DM9000A_s1_irq_from_sa                               (DM9000A_s1_irq_from_sa),
      .DM9000A_s1_read_n                                    (DM9000A_s1_read_n),
      .DM9000A_s1_readdata                                  (DM9000A_s1_readdata),
      .DM9000A_s1_readdata_from_sa                          (DM9000A_s1_readdata_from_sa),
      .DM9000A_s1_reset_n                                   (DM9000A_s1_reset_n),
      .DM9000A_s1_wait_counter_eq_0                         (DM9000A_s1_wait_counter_eq_0),
      .DM9000A_s1_write_n                                   (DM9000A_s1_write_n),
      .DM9000A_s1_writedata                                 (DM9000A_s1_writedata),
      .clk                                                  (clk_25),
      .d1_DM9000A_s1_end_xfer                               (d1_DM9000A_s1_end_xfer),
      .reset_n                                              (clk_25_reset_n)
    );

  DM9000A the_DM9000A
    (
      .avs_s1_address_iCMD       (DM9000A_s1_address),
      .avs_s1_chipselect_n_iCS_N (DM9000A_s1_chipselect_n),
      .avs_s1_clk_iCLK           (clk_25),
      .avs_s1_export_ENET_CLK    (avs_s1_export_ENET_CLK_from_the_DM9000A),
      .avs_s1_export_ENET_CMD    (avs_s1_export_ENET_CMD_from_the_DM9000A),
      .avs_s1_export_ENET_CS_N   (avs_s1_export_ENET_CS_N_from_the_DM9000A),
      .avs_s1_export_ENET_DATA   (avs_s1_export_ENET_DATA_to_and_from_the_DM9000A),
      .avs_s1_export_ENET_INT    (avs_s1_export_ENET_INT_to_the_DM9000A),
      .avs_s1_export_ENET_RD_N   (avs_s1_export_ENET_RD_N_from_the_DM9000A),
      .avs_s1_export_ENET_RST_N  (avs_s1_export_ENET_RST_N_from_the_DM9000A),
      .avs_s1_export_ENET_WR_N   (avs_s1_export_ENET_WR_N_from_the_DM9000A),
      .avs_s1_irq_oINT           (DM9000A_s1_irq),
      .avs_s1_read_n_iRD_N       (DM9000A_s1_read_n),
      .avs_s1_readdata_oDATA     (DM9000A_s1_readdata),
      .avs_s1_reset_n_iRST_N     (DM9000A_s1_reset_n),
      .avs_s1_write_n_iWR_N      (DM9000A_s1_write_n),
      .avs_s1_writedata_iDATA    (DM9000A_s1_writedata)
    );

  SEG7_s1_arbitrator the_SEG7_s1
    (
      .SEG7_s1_address                           (SEG7_s1_address),
      .SEG7_s1_read                              (SEG7_s1_read),
      .SEG7_s1_readdata                          (SEG7_s1_readdata),
      .SEG7_s1_readdata_from_sa                  (SEG7_s1_readdata_from_sa),
      .SEG7_s1_reset                             (SEG7_s1_reset),
      .SEG7_s1_write                             (SEG7_s1_write),
      .SEG7_s1_writedata                         (SEG7_s1_writedata),
      .clk                                       (pll_c0_out),
      .cpu_data_master_address_to_slave          (cpu_data_master_address_to_slave),
      .cpu_data_master_byteenable                (cpu_data_master_byteenable),
      .cpu_data_master_granted_SEG7_s1           (cpu_data_master_granted_SEG7_s1),
      .cpu_data_master_latency_counter           (cpu_data_master_latency_counter),
      .cpu_data_master_qualified_request_SEG7_s1 (cpu_data_master_qualified_request_SEG7_s1),
      .cpu_data_master_read                      (cpu_data_master_read),
      .cpu_data_master_read_data_valid_SEG7_s1   (cpu_data_master_read_data_valid_SEG7_s1),
      .cpu_data_master_requests_SEG7_s1          (cpu_data_master_requests_SEG7_s1),
      .cpu_data_master_write                     (cpu_data_master_write),
      .cpu_data_master_writedata                 (cpu_data_master_writedata),
      .d1_SEG7_s1_end_xfer                       (d1_SEG7_s1_end_xfer),
      .reset_n                                   (pll_c0_out_reset_n)
    );

  SEG7 the_SEG7
    (
      .avs_s1_address     (SEG7_s1_address),
      .avs_s1_clk         (pll_c0_out),
      .avs_s1_export_seg7 (avs_s1_export_seg7_from_the_SEG7),
      .avs_s1_read        (SEG7_s1_read),
      .avs_s1_readdata    (SEG7_s1_readdata),
      .avs_s1_reset       (SEG7_s1_reset),
      .avs_s1_write       (SEG7_s1_write),
      .avs_s1_writedata   (SEG7_s1_writedata)
    );

  clock_crossing_0_s1_arbitrator the_clock_crossing_0_s1
    (
      .DE2_70_SOPC_clock_2_out_address_to_slave                                   (DE2_70_SOPC_clock_2_out_address_to_slave),
      .DE2_70_SOPC_clock_2_out_byteenable                                         (DE2_70_SOPC_clock_2_out_byteenable),
      .DE2_70_SOPC_clock_2_out_granted_clock_crossing_0_s1                        (DE2_70_SOPC_clock_2_out_granted_clock_crossing_0_s1),
      .DE2_70_SOPC_clock_2_out_nativeaddress                                      (DE2_70_SOPC_clock_2_out_nativeaddress),
      .DE2_70_SOPC_clock_2_out_qualified_request_clock_crossing_0_s1              (DE2_70_SOPC_clock_2_out_qualified_request_clock_crossing_0_s1),
      .DE2_70_SOPC_clock_2_out_read                                               (DE2_70_SOPC_clock_2_out_read),
      .DE2_70_SOPC_clock_2_out_read_data_valid_clock_crossing_0_s1                (DE2_70_SOPC_clock_2_out_read_data_valid_clock_crossing_0_s1),
      .DE2_70_SOPC_clock_2_out_read_data_valid_clock_crossing_0_s1_shift_register (DE2_70_SOPC_clock_2_out_read_data_valid_clock_crossing_0_s1_shift_register),
      .DE2_70_SOPC_clock_2_out_requests_clock_crossing_0_s1                       (DE2_70_SOPC_clock_2_out_requests_clock_crossing_0_s1),
      .DE2_70_SOPC_clock_2_out_write                                              (DE2_70_SOPC_clock_2_out_write),
      .DE2_70_SOPC_clock_2_out_writedata                                          (DE2_70_SOPC_clock_2_out_writedata),
      .clk                                                                        (clk_25),
      .clock_crossing_0_s1_address                                                (clock_crossing_0_s1_address),
      .clock_crossing_0_s1_byteenable                                             (clock_crossing_0_s1_byteenable),
      .clock_crossing_0_s1_endofpacket                                            (clock_crossing_0_s1_endofpacket),
      .clock_crossing_0_s1_endofpacket_from_sa                                    (clock_crossing_0_s1_endofpacket_from_sa),
      .clock_crossing_0_s1_nativeaddress                                          (clock_crossing_0_s1_nativeaddress),
      .clock_crossing_0_s1_read                                                   (clock_crossing_0_s1_read),
      .clock_crossing_0_s1_readdata                                               (clock_crossing_0_s1_readdata),
      .clock_crossing_0_s1_readdata_from_sa                                       (clock_crossing_0_s1_readdata_from_sa),
      .clock_crossing_0_s1_readdatavalid                                          (clock_crossing_0_s1_readdatavalid),
      .clock_crossing_0_s1_reset_n                                                (clock_crossing_0_s1_reset_n),
      .clock_crossing_0_s1_waitrequest                                            (clock_crossing_0_s1_waitrequest),
      .clock_crossing_0_s1_waitrequest_from_sa                                    (clock_crossing_0_s1_waitrequest_from_sa),
      .clock_crossing_0_s1_write                                                  (clock_crossing_0_s1_write),
      .clock_crossing_0_s1_writedata                                              (clock_crossing_0_s1_writedata),
      .d1_clock_crossing_0_s1_end_xfer                                            (d1_clock_crossing_0_s1_end_xfer),
      .reset_n                                                                    (clk_25_reset_n)
    );

  clock_crossing_0_m1_arbitrator the_clock_crossing_0_m1
    (
      .DE2_70_SOPC_clock_1_in_endofpacket_from_sa                   (DE2_70_SOPC_clock_1_in_endofpacket_from_sa),
      .DE2_70_SOPC_clock_1_in_readdata_from_sa                      (DE2_70_SOPC_clock_1_in_readdata_from_sa),
      .DE2_70_SOPC_clock_1_in_waitrequest_from_sa                   (DE2_70_SOPC_clock_1_in_waitrequest_from_sa),
      .clk                                                          (pll_c0_out),
      .clock_crossing_0_m1_address                                  (clock_crossing_0_m1_address),
      .clock_crossing_0_m1_address_to_slave                         (clock_crossing_0_m1_address_to_slave),
      .clock_crossing_0_m1_byteenable                               (clock_crossing_0_m1_byteenable),
      .clock_crossing_0_m1_endofpacket                              (clock_crossing_0_m1_endofpacket),
      .clock_crossing_0_m1_granted_DE2_70_SOPC_clock_1_in           (clock_crossing_0_m1_granted_DE2_70_SOPC_clock_1_in),
      .clock_crossing_0_m1_latency_counter                          (clock_crossing_0_m1_latency_counter),
      .clock_crossing_0_m1_qualified_request_DE2_70_SOPC_clock_1_in (clock_crossing_0_m1_qualified_request_DE2_70_SOPC_clock_1_in),
      .clock_crossing_0_m1_read                                     (clock_crossing_0_m1_read),
      .clock_crossing_0_m1_read_data_valid_DE2_70_SOPC_clock_1_in   (clock_crossing_0_m1_read_data_valid_DE2_70_SOPC_clock_1_in),
      .clock_crossing_0_m1_readdata                                 (clock_crossing_0_m1_readdata),
      .clock_crossing_0_m1_readdatavalid                            (clock_crossing_0_m1_readdatavalid),
      .clock_crossing_0_m1_requests_DE2_70_SOPC_clock_1_in          (clock_crossing_0_m1_requests_DE2_70_SOPC_clock_1_in),
      .clock_crossing_0_m1_reset_n                                  (clock_crossing_0_m1_reset_n),
      .clock_crossing_0_m1_waitrequest                              (clock_crossing_0_m1_waitrequest),
      .clock_crossing_0_m1_write                                    (clock_crossing_0_m1_write),
      .clock_crossing_0_m1_writedata                                (clock_crossing_0_m1_writedata),
      .d1_DE2_70_SOPC_clock_1_in_end_xfer                           (d1_DE2_70_SOPC_clock_1_in_end_xfer),
      .reset_n                                                      (pll_c0_out_reset_n)
    );

  clock_crossing_0 the_clock_crossing_0
    (
      .master_address       (clock_crossing_0_m1_address),
      .master_byteenable    (clock_crossing_0_m1_byteenable),
      .master_clk           (pll_c0_out),
      .master_endofpacket   (clock_crossing_0_m1_endofpacket),
      .master_nativeaddress (clock_crossing_0_m1_nativeaddress),
      .master_read          (clock_crossing_0_m1_read),
      .master_readdata      (clock_crossing_0_m1_readdata),
      .master_readdatavalid (clock_crossing_0_m1_readdatavalid),
      .master_reset_n       (clock_crossing_0_m1_reset_n),
      .master_waitrequest   (clock_crossing_0_m1_waitrequest),
      .master_write         (clock_crossing_0_m1_write),
      .master_writedata     (clock_crossing_0_m1_writedata),
      .slave_address        (clock_crossing_0_s1_address),
      .slave_byteenable     (clock_crossing_0_s1_byteenable),
      .slave_clk            (clk_25),
      .slave_endofpacket    (clock_crossing_0_s1_endofpacket),
      .slave_nativeaddress  (clock_crossing_0_s1_nativeaddress),
      .slave_read           (clock_crossing_0_s1_read),
      .slave_readdata       (clock_crossing_0_s1_readdata),
      .slave_readdatavalid  (clock_crossing_0_s1_readdatavalid),
      .slave_reset_n        (clock_crossing_0_s1_reset_n),
      .slave_waitrequest    (clock_crossing_0_s1_waitrequest),
      .slave_write          (clock_crossing_0_s1_write),
      .slave_writedata      (clock_crossing_0_s1_writedata)
    );

  control_in_pio_s1_arbitrator the_control_in_pio_s1
    (
      .clk                                                 (pll_c0_out),
      .control_in_pio_s1_address                           (control_in_pio_s1_address),
      .control_in_pio_s1_chipselect                        (control_in_pio_s1_chipselect),
      .control_in_pio_s1_readdata                          (control_in_pio_s1_readdata),
      .control_in_pio_s1_readdata_from_sa                  (control_in_pio_s1_readdata_from_sa),
      .control_in_pio_s1_reset_n                           (control_in_pio_s1_reset_n),
      .control_in_pio_s1_write_n                           (control_in_pio_s1_write_n),
      .control_in_pio_s1_writedata                         (control_in_pio_s1_writedata),
      .cpu_data_master_address_to_slave                    (cpu_data_master_address_to_slave),
      .cpu_data_master_granted_control_in_pio_s1           (cpu_data_master_granted_control_in_pio_s1),
      .cpu_data_master_latency_counter                     (cpu_data_master_latency_counter),
      .cpu_data_master_qualified_request_control_in_pio_s1 (cpu_data_master_qualified_request_control_in_pio_s1),
      .cpu_data_master_read                                (cpu_data_master_read),
      .cpu_data_master_read_data_valid_control_in_pio_s1   (cpu_data_master_read_data_valid_control_in_pio_s1),
      .cpu_data_master_requests_control_in_pio_s1          (cpu_data_master_requests_control_in_pio_s1),
      .cpu_data_master_write                               (cpu_data_master_write),
      .cpu_data_master_writedata                           (cpu_data_master_writedata),
      .d1_control_in_pio_s1_end_xfer                       (d1_control_in_pio_s1_end_xfer),
      .reset_n                                             (pll_c0_out_reset_n)
    );

  control_in_pio the_control_in_pio
    (
      .address    (control_in_pio_s1_address),
      .chipselect (control_in_pio_s1_chipselect),
      .clk        (pll_c0_out),
      .in_port    (in_port_to_the_control_in_pio),
      .readdata   (control_in_pio_s1_readdata),
      .reset_n    (control_in_pio_s1_reset_n),
      .write_n    (control_in_pio_s1_write_n),
      .writedata  (control_in_pio_s1_writedata)
    );

  control_out_pio_s1_arbitrator the_control_out_pio_s1
    (
      .clk                                                  (pll_c0_out),
      .control_out_pio_s1_address                           (control_out_pio_s1_address),
      .control_out_pio_s1_chipselect                        (control_out_pio_s1_chipselect),
      .control_out_pio_s1_readdata                          (control_out_pio_s1_readdata),
      .control_out_pio_s1_readdata_from_sa                  (control_out_pio_s1_readdata_from_sa),
      .control_out_pio_s1_reset_n                           (control_out_pio_s1_reset_n),
      .control_out_pio_s1_write_n                           (control_out_pio_s1_write_n),
      .control_out_pio_s1_writedata                         (control_out_pio_s1_writedata),
      .cpu_data_master_address_to_slave                     (cpu_data_master_address_to_slave),
      .cpu_data_master_granted_control_out_pio_s1           (cpu_data_master_granted_control_out_pio_s1),
      .cpu_data_master_latency_counter                      (cpu_data_master_latency_counter),
      .cpu_data_master_qualified_request_control_out_pio_s1 (cpu_data_master_qualified_request_control_out_pio_s1),
      .cpu_data_master_read                                 (cpu_data_master_read),
      .cpu_data_master_read_data_valid_control_out_pio_s1   (cpu_data_master_read_data_valid_control_out_pio_s1),
      .cpu_data_master_requests_control_out_pio_s1          (cpu_data_master_requests_control_out_pio_s1),
      .cpu_data_master_write                                (cpu_data_master_write),
      .cpu_data_master_writedata                            (cpu_data_master_writedata),
      .d1_control_out_pio_s1_end_xfer                       (d1_control_out_pio_s1_end_xfer),
      .reset_n                                              (pll_c0_out_reset_n)
    );

  control_out_pio the_control_out_pio
    (
      .address    (control_out_pio_s1_address),
      .chipselect (control_out_pio_s1_chipselect),
      .clk        (pll_c0_out),
      .out_port   (out_port_from_the_control_out_pio),
      .readdata   (control_out_pio_s1_readdata),
      .reset_n    (control_out_pio_s1_reset_n),
      .write_n    (control_out_pio_s1_write_n),
      .writedata  (control_out_pio_s1_writedata)
    );

  cpu_jtag_debug_module_arbitrator the_cpu_jtag_debug_module
    (
      .clk                                                            (pll_c0_out),
      .cpu_data_master_address_to_slave                               (cpu_data_master_address_to_slave),
      .cpu_data_master_byteenable                                     (cpu_data_master_byteenable),
      .cpu_data_master_debugaccess                                    (cpu_data_master_debugaccess),
      .cpu_data_master_granted_cpu_jtag_debug_module                  (cpu_data_master_granted_cpu_jtag_debug_module),
      .cpu_data_master_latency_counter                                (cpu_data_master_latency_counter),
      .cpu_data_master_qualified_request_cpu_jtag_debug_module        (cpu_data_master_qualified_request_cpu_jtag_debug_module),
      .cpu_data_master_read                                           (cpu_data_master_read),
      .cpu_data_master_read_data_valid_cpu_jtag_debug_module          (cpu_data_master_read_data_valid_cpu_jtag_debug_module),
      .cpu_data_master_requests_cpu_jtag_debug_module                 (cpu_data_master_requests_cpu_jtag_debug_module),
      .cpu_data_master_write                                          (cpu_data_master_write),
      .cpu_data_master_writedata                                      (cpu_data_master_writedata),
      .cpu_instruction_master_address_to_slave                        (cpu_instruction_master_address_to_slave),
      .cpu_instruction_master_granted_cpu_jtag_debug_module           (cpu_instruction_master_granted_cpu_jtag_debug_module),
      .cpu_instruction_master_latency_counter                         (cpu_instruction_master_latency_counter),
      .cpu_instruction_master_qualified_request_cpu_jtag_debug_module (cpu_instruction_master_qualified_request_cpu_jtag_debug_module),
      .cpu_instruction_master_read                                    (cpu_instruction_master_read),
      .cpu_instruction_master_read_data_valid_cpu_jtag_debug_module   (cpu_instruction_master_read_data_valid_cpu_jtag_debug_module),
      .cpu_instruction_master_requests_cpu_jtag_debug_module          (cpu_instruction_master_requests_cpu_jtag_debug_module),
      .cpu_jtag_debug_module_address                                  (cpu_jtag_debug_module_address),
      .cpu_jtag_debug_module_begintransfer                            (cpu_jtag_debug_module_begintransfer),
      .cpu_jtag_debug_module_byteenable                               (cpu_jtag_debug_module_byteenable),
      .cpu_jtag_debug_module_chipselect                               (cpu_jtag_debug_module_chipselect),
      .cpu_jtag_debug_module_debugaccess                              (cpu_jtag_debug_module_debugaccess),
      .cpu_jtag_debug_module_readdata                                 (cpu_jtag_debug_module_readdata),
      .cpu_jtag_debug_module_readdata_from_sa                         (cpu_jtag_debug_module_readdata_from_sa),
      .cpu_jtag_debug_module_reset_n                                  (cpu_jtag_debug_module_reset_n),
      .cpu_jtag_debug_module_resetrequest                             (cpu_jtag_debug_module_resetrequest),
      .cpu_jtag_debug_module_resetrequest_from_sa                     (cpu_jtag_debug_module_resetrequest_from_sa),
      .cpu_jtag_debug_module_write                                    (cpu_jtag_debug_module_write),
      .cpu_jtag_debug_module_writedata                                (cpu_jtag_debug_module_writedata),
      .d1_cpu_jtag_debug_module_end_xfer                              (d1_cpu_jtag_debug_module_end_xfer),
      .reset_n                                                        (pll_c0_out_reset_n)
    );

  cpu_data_master_arbitrator the_cpu_data_master
    (
      .DE2_70_SOPC_clock_0_in_readdata_from_sa                                     (DE2_70_SOPC_clock_0_in_readdata_from_sa),
      .DE2_70_SOPC_clock_0_in_waitrequest_from_sa                                  (DE2_70_SOPC_clock_0_in_waitrequest_from_sa),
      .DE2_70_SOPC_clock_2_in_readdata_from_sa                                     (DE2_70_SOPC_clock_2_in_readdata_from_sa),
      .DE2_70_SOPC_clock_2_in_waitrequest_from_sa                                  (DE2_70_SOPC_clock_2_in_waitrequest_from_sa),
      .DM9000A_s1_irq_from_sa                                                      (DM9000A_s1_irq_from_sa),
      .SEG7_s1_readdata_from_sa                                                    (SEG7_s1_readdata_from_sa),
      .clk                                                                         (pll_c0_out),
      .control_in_pio_s1_readdata_from_sa                                          (control_in_pio_s1_readdata_from_sa),
      .control_out_pio_s1_readdata_from_sa                                         (control_out_pio_s1_readdata_from_sa),
      .cpu_data_master_address                                                     (cpu_data_master_address),
      .cpu_data_master_address_to_slave                                            (cpu_data_master_address_to_slave),
      .cpu_data_master_byteenable                                                  (cpu_data_master_byteenable),
      .cpu_data_master_granted_DE2_70_SOPC_clock_0_in                              (cpu_data_master_granted_DE2_70_SOPC_clock_0_in),
      .cpu_data_master_granted_DE2_70_SOPC_clock_2_in                              (cpu_data_master_granted_DE2_70_SOPC_clock_2_in),
      .cpu_data_master_granted_SEG7_s1                                             (cpu_data_master_granted_SEG7_s1),
      .cpu_data_master_granted_control_in_pio_s1                                   (cpu_data_master_granted_control_in_pio_s1),
      .cpu_data_master_granted_control_out_pio_s1                                  (cpu_data_master_granted_control_out_pio_s1),
      .cpu_data_master_granted_cpu_jtag_debug_module                               (cpu_data_master_granted_cpu_jtag_debug_module),
      .cpu_data_master_granted_data_in_pio_s1                                      (cpu_data_master_granted_data_in_pio_s1),
      .cpu_data_master_granted_data_out_pio_s1                                     (cpu_data_master_granted_data_out_pio_s1),
      .cpu_data_master_granted_epcs_flash_controller_0_epcs_control_port           (cpu_data_master_granted_epcs_flash_controller_0_epcs_control_port),
      .cpu_data_master_granted_jtag_uart_avalon_jtag_slave                         (cpu_data_master_granted_jtag_uart_avalon_jtag_slave),
      .cpu_data_master_granted_lcd_control_slave                                   (cpu_data_master_granted_lcd_control_slave),
      .cpu_data_master_granted_onchip_mem_s1                                       (cpu_data_master_granted_onchip_mem_s1),
      .cpu_data_master_granted_pio_button_s1                                       (cpu_data_master_granted_pio_button_s1),
      .cpu_data_master_granted_pio_switch_s1                                       (cpu_data_master_granted_pio_switch_s1),
      .cpu_data_master_granted_ssram_s1                                            (cpu_data_master_granted_ssram_s1),
      .cpu_data_master_granted_sysid_control_slave                                 (cpu_data_master_granted_sysid_control_slave),
      .cpu_data_master_granted_timer_s1                                            (cpu_data_master_granted_timer_s1),
      .cpu_data_master_granted_timer_stamp_s1                                      (cpu_data_master_granted_timer_stamp_s1),
      .cpu_data_master_irq                                                         (cpu_data_master_irq),
      .cpu_data_master_latency_counter                                             (cpu_data_master_latency_counter),
      .cpu_data_master_qualified_request_DE2_70_SOPC_clock_0_in                    (cpu_data_master_qualified_request_DE2_70_SOPC_clock_0_in),
      .cpu_data_master_qualified_request_DE2_70_SOPC_clock_2_in                    (cpu_data_master_qualified_request_DE2_70_SOPC_clock_2_in),
      .cpu_data_master_qualified_request_SEG7_s1                                   (cpu_data_master_qualified_request_SEG7_s1),
      .cpu_data_master_qualified_request_control_in_pio_s1                         (cpu_data_master_qualified_request_control_in_pio_s1),
      .cpu_data_master_qualified_request_control_out_pio_s1                        (cpu_data_master_qualified_request_control_out_pio_s1),
      .cpu_data_master_qualified_request_cpu_jtag_debug_module                     (cpu_data_master_qualified_request_cpu_jtag_debug_module),
      .cpu_data_master_qualified_request_data_in_pio_s1                            (cpu_data_master_qualified_request_data_in_pio_s1),
      .cpu_data_master_qualified_request_data_out_pio_s1                           (cpu_data_master_qualified_request_data_out_pio_s1),
      .cpu_data_master_qualified_request_epcs_flash_controller_0_epcs_control_port (cpu_data_master_qualified_request_epcs_flash_controller_0_epcs_control_port),
      .cpu_data_master_qualified_request_jtag_uart_avalon_jtag_slave               (cpu_data_master_qualified_request_jtag_uart_avalon_jtag_slave),
      .cpu_data_master_qualified_request_lcd_control_slave                         (cpu_data_master_qualified_request_lcd_control_slave),
      .cpu_data_master_qualified_request_onchip_mem_s1                             (cpu_data_master_qualified_request_onchip_mem_s1),
      .cpu_data_master_qualified_request_pio_button_s1                             (cpu_data_master_qualified_request_pio_button_s1),
      .cpu_data_master_qualified_request_pio_switch_s1                             (cpu_data_master_qualified_request_pio_switch_s1),
      .cpu_data_master_qualified_request_ssram_s1                                  (cpu_data_master_qualified_request_ssram_s1),
      .cpu_data_master_qualified_request_sysid_control_slave                       (cpu_data_master_qualified_request_sysid_control_slave),
      .cpu_data_master_qualified_request_timer_s1                                  (cpu_data_master_qualified_request_timer_s1),
      .cpu_data_master_qualified_request_timer_stamp_s1                            (cpu_data_master_qualified_request_timer_stamp_s1),
      .cpu_data_master_read                                                        (cpu_data_master_read),
      .cpu_data_master_read_data_valid_DE2_70_SOPC_clock_0_in                      (cpu_data_master_read_data_valid_DE2_70_SOPC_clock_0_in),
      .cpu_data_master_read_data_valid_DE2_70_SOPC_clock_2_in                      (cpu_data_master_read_data_valid_DE2_70_SOPC_clock_2_in),
      .cpu_data_master_read_data_valid_SEG7_s1                                     (cpu_data_master_read_data_valid_SEG7_s1),
      .cpu_data_master_read_data_valid_control_in_pio_s1                           (cpu_data_master_read_data_valid_control_in_pio_s1),
      .cpu_data_master_read_data_valid_control_out_pio_s1                          (cpu_data_master_read_data_valid_control_out_pio_s1),
      .cpu_data_master_read_data_valid_cpu_jtag_debug_module                       (cpu_data_master_read_data_valid_cpu_jtag_debug_module),
      .cpu_data_master_read_data_valid_data_in_pio_s1                              (cpu_data_master_read_data_valid_data_in_pio_s1),
      .cpu_data_master_read_data_valid_data_out_pio_s1                             (cpu_data_master_read_data_valid_data_out_pio_s1),
      .cpu_data_master_read_data_valid_epcs_flash_controller_0_epcs_control_port   (cpu_data_master_read_data_valid_epcs_flash_controller_0_epcs_control_port),
      .cpu_data_master_read_data_valid_jtag_uart_avalon_jtag_slave                 (cpu_data_master_read_data_valid_jtag_uart_avalon_jtag_slave),
      .cpu_data_master_read_data_valid_lcd_control_slave                           (cpu_data_master_read_data_valid_lcd_control_slave),
      .cpu_data_master_read_data_valid_onchip_mem_s1                               (cpu_data_master_read_data_valid_onchip_mem_s1),
      .cpu_data_master_read_data_valid_pio_button_s1                               (cpu_data_master_read_data_valid_pio_button_s1),
      .cpu_data_master_read_data_valid_pio_switch_s1                               (cpu_data_master_read_data_valid_pio_switch_s1),
      .cpu_data_master_read_data_valid_ssram_s1                                    (cpu_data_master_read_data_valid_ssram_s1),
      .cpu_data_master_read_data_valid_sysid_control_slave                         (cpu_data_master_read_data_valid_sysid_control_slave),
      .cpu_data_master_read_data_valid_timer_s1                                    (cpu_data_master_read_data_valid_timer_s1),
      .cpu_data_master_read_data_valid_timer_stamp_s1                              (cpu_data_master_read_data_valid_timer_stamp_s1),
      .cpu_data_master_readdata                                                    (cpu_data_master_readdata),
      .cpu_data_master_readdatavalid                                               (cpu_data_master_readdatavalid),
      .cpu_data_master_requests_DE2_70_SOPC_clock_0_in                             (cpu_data_master_requests_DE2_70_SOPC_clock_0_in),
      .cpu_data_master_requests_DE2_70_SOPC_clock_2_in                             (cpu_data_master_requests_DE2_70_SOPC_clock_2_in),
      .cpu_data_master_requests_SEG7_s1                                            (cpu_data_master_requests_SEG7_s1),
      .cpu_data_master_requests_control_in_pio_s1                                  (cpu_data_master_requests_control_in_pio_s1),
      .cpu_data_master_requests_control_out_pio_s1                                 (cpu_data_master_requests_control_out_pio_s1),
      .cpu_data_master_requests_cpu_jtag_debug_module                              (cpu_data_master_requests_cpu_jtag_debug_module),
      .cpu_data_master_requests_data_in_pio_s1                                     (cpu_data_master_requests_data_in_pio_s1),
      .cpu_data_master_requests_data_out_pio_s1                                    (cpu_data_master_requests_data_out_pio_s1),
      .cpu_data_master_requests_epcs_flash_controller_0_epcs_control_port          (cpu_data_master_requests_epcs_flash_controller_0_epcs_control_port),
      .cpu_data_master_requests_jtag_uart_avalon_jtag_slave                        (cpu_data_master_requests_jtag_uart_avalon_jtag_slave),
      .cpu_data_master_requests_lcd_control_slave                                  (cpu_data_master_requests_lcd_control_slave),
      .cpu_data_master_requests_onchip_mem_s1                                      (cpu_data_master_requests_onchip_mem_s1),
      .cpu_data_master_requests_pio_button_s1                                      (cpu_data_master_requests_pio_button_s1),
      .cpu_data_master_requests_pio_switch_s1                                      (cpu_data_master_requests_pio_switch_s1),
      .cpu_data_master_requests_ssram_s1                                           (cpu_data_master_requests_ssram_s1),
      .cpu_data_master_requests_sysid_control_slave                                (cpu_data_master_requests_sysid_control_slave),
      .cpu_data_master_requests_timer_s1                                           (cpu_data_master_requests_timer_s1),
      .cpu_data_master_requests_timer_stamp_s1                                     (cpu_data_master_requests_timer_stamp_s1),
      .cpu_data_master_waitrequest                                                 (cpu_data_master_waitrequest),
      .cpu_data_master_write                                                       (cpu_data_master_write),
      .cpu_data_master_writedata                                                   (cpu_data_master_writedata),
      .cpu_jtag_debug_module_readdata_from_sa                                      (cpu_jtag_debug_module_readdata_from_sa),
      .d1_DE2_70_SOPC_clock_0_in_end_xfer                                          (d1_DE2_70_SOPC_clock_0_in_end_xfer),
      .d1_DE2_70_SOPC_clock_2_in_end_xfer                                          (d1_DE2_70_SOPC_clock_2_in_end_xfer),
      .d1_SEG7_s1_end_xfer                                                         (d1_SEG7_s1_end_xfer),
      .d1_control_in_pio_s1_end_xfer                                               (d1_control_in_pio_s1_end_xfer),
      .d1_control_out_pio_s1_end_xfer                                              (d1_control_out_pio_s1_end_xfer),
      .d1_cpu_jtag_debug_module_end_xfer                                           (d1_cpu_jtag_debug_module_end_xfer),
      .d1_data_in_pio_s1_end_xfer                                                  (d1_data_in_pio_s1_end_xfer),
      .d1_data_out_pio_s1_end_xfer                                                 (d1_data_out_pio_s1_end_xfer),
      .d1_epcs_flash_controller_0_epcs_control_port_end_xfer                       (d1_epcs_flash_controller_0_epcs_control_port_end_xfer),
      .d1_jtag_uart_avalon_jtag_slave_end_xfer                                     (d1_jtag_uart_avalon_jtag_slave_end_xfer),
      .d1_lcd_control_slave_end_xfer                                               (d1_lcd_control_slave_end_xfer),
      .d1_onchip_mem_s1_end_xfer                                                   (d1_onchip_mem_s1_end_xfer),
      .d1_pio_button_s1_end_xfer                                                   (d1_pio_button_s1_end_xfer),
      .d1_pio_switch_s1_end_xfer                                                   (d1_pio_switch_s1_end_xfer),
      .d1_sysid_control_slave_end_xfer                                             (d1_sysid_control_slave_end_xfer),
      .d1_timer_s1_end_xfer                                                        (d1_timer_s1_end_xfer),
      .d1_timer_stamp_s1_end_xfer                                                  (d1_timer_stamp_s1_end_xfer),
      .d1_tristate_bridge_ssram_avalon_slave_end_xfer                              (d1_tristate_bridge_ssram_avalon_slave_end_xfer),
      .data_in_pio_s1_readdata_from_sa                                             (data_in_pio_s1_readdata_from_sa),
      .data_out_pio_s1_readdata_from_sa                                            (data_out_pio_s1_readdata_from_sa),
      .epcs_flash_controller_0_epcs_control_port_irq_from_sa                       (epcs_flash_controller_0_epcs_control_port_irq_from_sa),
      .epcs_flash_controller_0_epcs_control_port_readdata_from_sa                  (epcs_flash_controller_0_epcs_control_port_readdata_from_sa),
      .incoming_data_to_and_from_the_ssram                                         (incoming_data_to_and_from_the_ssram),
      .jtag_uart_avalon_jtag_slave_irq_from_sa                                     (jtag_uart_avalon_jtag_slave_irq_from_sa),
      .jtag_uart_avalon_jtag_slave_readdata_from_sa                                (jtag_uart_avalon_jtag_slave_readdata_from_sa),
      .jtag_uart_avalon_jtag_slave_waitrequest_from_sa                             (jtag_uart_avalon_jtag_slave_waitrequest_from_sa),
      .lcd_control_slave_readdata_from_sa                                          (lcd_control_slave_readdata_from_sa),
      .lcd_control_slave_wait_counter_eq_0                                         (lcd_control_slave_wait_counter_eq_0),
      .onchip_mem_s1_readdata_from_sa                                              (onchip_mem_s1_readdata_from_sa),
      .pio_button_s1_irq_from_sa                                                   (pio_button_s1_irq_from_sa),
      .pio_button_s1_readdata_from_sa                                              (pio_button_s1_readdata_from_sa),
      .pio_switch_s1_readdata_from_sa                                              (pio_switch_s1_readdata_from_sa),
      .pll_c0_out                                                                  (pll_c0_out),
      .pll_c0_out_reset_n                                                          (pll_c0_out_reset_n),
      .reset_n                                                                     (pll_c0_out_reset_n),
      .sysid_control_slave_readdata_from_sa                                        (sysid_control_slave_readdata_from_sa),
      .timer_s1_irq_from_sa                                                        (timer_s1_irq_from_sa),
      .timer_s1_readdata_from_sa                                                   (timer_s1_readdata_from_sa),
      .timer_stamp_s1_irq_from_sa                                                  (timer_stamp_s1_irq_from_sa),
      .timer_stamp_s1_readdata_from_sa                                             (timer_stamp_s1_readdata_from_sa)
    );

  cpu_instruction_master_arbitrator the_cpu_instruction_master
    (
      .clk                                                                                (pll_c0_out),
      .cpu_instruction_master_address                                                     (cpu_instruction_master_address),
      .cpu_instruction_master_address_to_slave                                            (cpu_instruction_master_address_to_slave),
      .cpu_instruction_master_granted_cpu_jtag_debug_module                               (cpu_instruction_master_granted_cpu_jtag_debug_module),
      .cpu_instruction_master_granted_epcs_flash_controller_0_epcs_control_port           (cpu_instruction_master_granted_epcs_flash_controller_0_epcs_control_port),
      .cpu_instruction_master_granted_onchip_mem_s1                                       (cpu_instruction_master_granted_onchip_mem_s1),
      .cpu_instruction_master_granted_ssram_s1                                            (cpu_instruction_master_granted_ssram_s1),
      .cpu_instruction_master_latency_counter                                             (cpu_instruction_master_latency_counter),
      .cpu_instruction_master_qualified_request_cpu_jtag_debug_module                     (cpu_instruction_master_qualified_request_cpu_jtag_debug_module),
      .cpu_instruction_master_qualified_request_epcs_flash_controller_0_epcs_control_port (cpu_instruction_master_qualified_request_epcs_flash_controller_0_epcs_control_port),
      .cpu_instruction_master_qualified_request_onchip_mem_s1                             (cpu_instruction_master_qualified_request_onchip_mem_s1),
      .cpu_instruction_master_qualified_request_ssram_s1                                  (cpu_instruction_master_qualified_request_ssram_s1),
      .cpu_instruction_master_read                                                        (cpu_instruction_master_read),
      .cpu_instruction_master_read_data_valid_cpu_jtag_debug_module                       (cpu_instruction_master_read_data_valid_cpu_jtag_debug_module),
      .cpu_instruction_master_read_data_valid_epcs_flash_controller_0_epcs_control_port   (cpu_instruction_master_read_data_valid_epcs_flash_controller_0_epcs_control_port),
      .cpu_instruction_master_read_data_valid_onchip_mem_s1                               (cpu_instruction_master_read_data_valid_onchip_mem_s1),
      .cpu_instruction_master_read_data_valid_ssram_s1                                    (cpu_instruction_master_read_data_valid_ssram_s1),
      .cpu_instruction_master_readdata                                                    (cpu_instruction_master_readdata),
      .cpu_instruction_master_readdatavalid                                               (cpu_instruction_master_readdatavalid),
      .cpu_instruction_master_requests_cpu_jtag_debug_module                              (cpu_instruction_master_requests_cpu_jtag_debug_module),
      .cpu_instruction_master_requests_epcs_flash_controller_0_epcs_control_port          (cpu_instruction_master_requests_epcs_flash_controller_0_epcs_control_port),
      .cpu_instruction_master_requests_onchip_mem_s1                                      (cpu_instruction_master_requests_onchip_mem_s1),
      .cpu_instruction_master_requests_ssram_s1                                           (cpu_instruction_master_requests_ssram_s1),
      .cpu_instruction_master_waitrequest                                                 (cpu_instruction_master_waitrequest),
      .cpu_jtag_debug_module_readdata_from_sa                                             (cpu_jtag_debug_module_readdata_from_sa),
      .d1_cpu_jtag_debug_module_end_xfer                                                  (d1_cpu_jtag_debug_module_end_xfer),
      .d1_epcs_flash_controller_0_epcs_control_port_end_xfer                              (d1_epcs_flash_controller_0_epcs_control_port_end_xfer),
      .d1_onchip_mem_s1_end_xfer                                                          (d1_onchip_mem_s1_end_xfer),
      .d1_tristate_bridge_ssram_avalon_slave_end_xfer                                     (d1_tristate_bridge_ssram_avalon_slave_end_xfer),
      .epcs_flash_controller_0_epcs_control_port_readdata_from_sa                         (epcs_flash_controller_0_epcs_control_port_readdata_from_sa),
      .incoming_data_to_and_from_the_ssram                                                (incoming_data_to_and_from_the_ssram),
      .onchip_mem_s1_readdata_from_sa                                                     (onchip_mem_s1_readdata_from_sa),
      .reset_n                                                                            (pll_c0_out_reset_n)
    );

  cpu the_cpu
    (
      .clk                                   (pll_c0_out),
      .d_address                             (cpu_data_master_address),
      .d_byteenable                          (cpu_data_master_byteenable),
      .d_irq                                 (cpu_data_master_irq),
      .d_read                                (cpu_data_master_read),
      .d_readdata                            (cpu_data_master_readdata),
      .d_readdatavalid                       (cpu_data_master_readdatavalid),
      .d_waitrequest                         (cpu_data_master_waitrequest),
      .d_write                               (cpu_data_master_write),
      .d_writedata                           (cpu_data_master_writedata),
      .i_address                             (cpu_instruction_master_address),
      .i_read                                (cpu_instruction_master_read),
      .i_readdata                            (cpu_instruction_master_readdata),
      .i_readdatavalid                       (cpu_instruction_master_readdatavalid),
      .i_waitrequest                         (cpu_instruction_master_waitrequest),
      .jtag_debug_module_address             (cpu_jtag_debug_module_address),
      .jtag_debug_module_begintransfer       (cpu_jtag_debug_module_begintransfer),
      .jtag_debug_module_byteenable          (cpu_jtag_debug_module_byteenable),
      .jtag_debug_module_debugaccess         (cpu_jtag_debug_module_debugaccess),
      .jtag_debug_module_debugaccess_to_roms (cpu_data_master_debugaccess),
      .jtag_debug_module_readdata            (cpu_jtag_debug_module_readdata),
      .jtag_debug_module_resetrequest        (cpu_jtag_debug_module_resetrequest),
      .jtag_debug_module_select              (cpu_jtag_debug_module_chipselect),
      .jtag_debug_module_write               (cpu_jtag_debug_module_write),
      .jtag_debug_module_writedata           (cpu_jtag_debug_module_writedata),
      .reset_n                               (cpu_jtag_debug_module_reset_n)
    );

  data_in_pio_s1_arbitrator the_data_in_pio_s1
    (
      .clk                                              (pll_c0_out),
      .cpu_data_master_address_to_slave                 (cpu_data_master_address_to_slave),
      .cpu_data_master_granted_data_in_pio_s1           (cpu_data_master_granted_data_in_pio_s1),
      .cpu_data_master_latency_counter                  (cpu_data_master_latency_counter),
      .cpu_data_master_qualified_request_data_in_pio_s1 (cpu_data_master_qualified_request_data_in_pio_s1),
      .cpu_data_master_read                             (cpu_data_master_read),
      .cpu_data_master_read_data_valid_data_in_pio_s1   (cpu_data_master_read_data_valid_data_in_pio_s1),
      .cpu_data_master_requests_data_in_pio_s1          (cpu_data_master_requests_data_in_pio_s1),
      .cpu_data_master_write                            (cpu_data_master_write),
      .cpu_data_master_writedata                        (cpu_data_master_writedata),
      .d1_data_in_pio_s1_end_xfer                       (d1_data_in_pio_s1_end_xfer),
      .data_in_pio_s1_address                           (data_in_pio_s1_address),
      .data_in_pio_s1_chipselect                        (data_in_pio_s1_chipselect),
      .data_in_pio_s1_readdata                          (data_in_pio_s1_readdata),
      .data_in_pio_s1_readdata_from_sa                  (data_in_pio_s1_readdata_from_sa),
      .data_in_pio_s1_reset_n                           (data_in_pio_s1_reset_n),
      .data_in_pio_s1_write_n                           (data_in_pio_s1_write_n),
      .data_in_pio_s1_writedata                         (data_in_pio_s1_writedata),
      .reset_n                                          (pll_c0_out_reset_n)
    );

  data_in_pio the_data_in_pio
    (
      .address    (data_in_pio_s1_address),
      .chipselect (data_in_pio_s1_chipselect),
      .clk        (pll_c0_out),
      .in_port    (in_port_to_the_data_in_pio),
      .readdata   (data_in_pio_s1_readdata),
      .reset_n    (data_in_pio_s1_reset_n),
      .write_n    (data_in_pio_s1_write_n),
      .writedata  (data_in_pio_s1_writedata)
    );

  data_out_pio_s1_arbitrator the_data_out_pio_s1
    (
      .clk                                               (pll_c0_out),
      .cpu_data_master_address_to_slave                  (cpu_data_master_address_to_slave),
      .cpu_data_master_granted_data_out_pio_s1           (cpu_data_master_granted_data_out_pio_s1),
      .cpu_data_master_latency_counter                   (cpu_data_master_latency_counter),
      .cpu_data_master_qualified_request_data_out_pio_s1 (cpu_data_master_qualified_request_data_out_pio_s1),
      .cpu_data_master_read                              (cpu_data_master_read),
      .cpu_data_master_read_data_valid_data_out_pio_s1   (cpu_data_master_read_data_valid_data_out_pio_s1),
      .cpu_data_master_requests_data_out_pio_s1          (cpu_data_master_requests_data_out_pio_s1),
      .cpu_data_master_write                             (cpu_data_master_write),
      .cpu_data_master_writedata                         (cpu_data_master_writedata),
      .d1_data_out_pio_s1_end_xfer                       (d1_data_out_pio_s1_end_xfer),
      .data_out_pio_s1_address                           (data_out_pio_s1_address),
      .data_out_pio_s1_chipselect                        (data_out_pio_s1_chipselect),
      .data_out_pio_s1_readdata                          (data_out_pio_s1_readdata),
      .data_out_pio_s1_readdata_from_sa                  (data_out_pio_s1_readdata_from_sa),
      .data_out_pio_s1_reset_n                           (data_out_pio_s1_reset_n),
      .data_out_pio_s1_write_n                           (data_out_pio_s1_write_n),
      .data_out_pio_s1_writedata                         (data_out_pio_s1_writedata),
      .reset_n                                           (pll_c0_out_reset_n)
    );

  data_out_pio the_data_out_pio
    (
      .address    (data_out_pio_s1_address),
      .chipselect (data_out_pio_s1_chipselect),
      .clk        (pll_c0_out),
      .out_port   (out_port_from_the_data_out_pio),
      .readdata   (data_out_pio_s1_readdata),
      .reset_n    (data_out_pio_s1_reset_n),
      .write_n    (data_out_pio_s1_write_n),
      .writedata  (data_out_pio_s1_writedata)
    );

  epcs_flash_controller_0_epcs_control_port_arbitrator the_epcs_flash_controller_0_epcs_control_port
    (
      .clk                                                                                (pll_c0_out),
      .cpu_data_master_address_to_slave                                                   (cpu_data_master_address_to_slave),
      .cpu_data_master_granted_epcs_flash_controller_0_epcs_control_port                  (cpu_data_master_granted_epcs_flash_controller_0_epcs_control_port),
      .cpu_data_master_latency_counter                                                    (cpu_data_master_latency_counter),
      .cpu_data_master_qualified_request_epcs_flash_controller_0_epcs_control_port        (cpu_data_master_qualified_request_epcs_flash_controller_0_epcs_control_port),
      .cpu_data_master_read                                                               (cpu_data_master_read),
      .cpu_data_master_read_data_valid_epcs_flash_controller_0_epcs_control_port          (cpu_data_master_read_data_valid_epcs_flash_controller_0_epcs_control_port),
      .cpu_data_master_requests_epcs_flash_controller_0_epcs_control_port                 (cpu_data_master_requests_epcs_flash_controller_0_epcs_control_port),
      .cpu_data_master_write                                                              (cpu_data_master_write),
      .cpu_data_master_writedata                                                          (cpu_data_master_writedata),
      .cpu_instruction_master_address_to_slave                                            (cpu_instruction_master_address_to_slave),
      .cpu_instruction_master_granted_epcs_flash_controller_0_epcs_control_port           (cpu_instruction_master_granted_epcs_flash_controller_0_epcs_control_port),
      .cpu_instruction_master_latency_counter                                             (cpu_instruction_master_latency_counter),
      .cpu_instruction_master_qualified_request_epcs_flash_controller_0_epcs_control_port (cpu_instruction_master_qualified_request_epcs_flash_controller_0_epcs_control_port),
      .cpu_instruction_master_read                                                        (cpu_instruction_master_read),
      .cpu_instruction_master_read_data_valid_epcs_flash_controller_0_epcs_control_port   (cpu_instruction_master_read_data_valid_epcs_flash_controller_0_epcs_control_port),
      .cpu_instruction_master_requests_epcs_flash_controller_0_epcs_control_port          (cpu_instruction_master_requests_epcs_flash_controller_0_epcs_control_port),
      .d1_epcs_flash_controller_0_epcs_control_port_end_xfer                              (d1_epcs_flash_controller_0_epcs_control_port_end_xfer),
      .epcs_flash_controller_0_epcs_control_port_address                                  (epcs_flash_controller_0_epcs_control_port_address),
      .epcs_flash_controller_0_epcs_control_port_chipselect                               (epcs_flash_controller_0_epcs_control_port_chipselect),
      .epcs_flash_controller_0_epcs_control_port_dataavailable                            (epcs_flash_controller_0_epcs_control_port_dataavailable),
      .epcs_flash_controller_0_epcs_control_port_dataavailable_from_sa                    (epcs_flash_controller_0_epcs_control_port_dataavailable_from_sa),
      .epcs_flash_controller_0_epcs_control_port_endofpacket                              (epcs_flash_controller_0_epcs_control_port_endofpacket),
      .epcs_flash_controller_0_epcs_control_port_endofpacket_from_sa                      (epcs_flash_controller_0_epcs_control_port_endofpacket_from_sa),
      .epcs_flash_controller_0_epcs_control_port_irq                                      (epcs_flash_controller_0_epcs_control_port_irq),
      .epcs_flash_controller_0_epcs_control_port_irq_from_sa                              (epcs_flash_controller_0_epcs_control_port_irq_from_sa),
      .epcs_flash_controller_0_epcs_control_port_read_n                                   (epcs_flash_controller_0_epcs_control_port_read_n),
      .epcs_flash_controller_0_epcs_control_port_readdata                                 (epcs_flash_controller_0_epcs_control_port_readdata),
      .epcs_flash_controller_0_epcs_control_port_readdata_from_sa                         (epcs_flash_controller_0_epcs_control_port_readdata_from_sa),
      .epcs_flash_controller_0_epcs_control_port_readyfordata                             (epcs_flash_controller_0_epcs_control_port_readyfordata),
      .epcs_flash_controller_0_epcs_control_port_readyfordata_from_sa                     (epcs_flash_controller_0_epcs_control_port_readyfordata_from_sa),
      .epcs_flash_controller_0_epcs_control_port_reset_n                                  (epcs_flash_controller_0_epcs_control_port_reset_n),
      .epcs_flash_controller_0_epcs_control_port_write_n                                  (epcs_flash_controller_0_epcs_control_port_write_n),
      .epcs_flash_controller_0_epcs_control_port_writedata                                (epcs_flash_controller_0_epcs_control_port_writedata),
      .reset_n                                                                            (pll_c0_out_reset_n)
    );

  epcs_flash_controller_0 the_epcs_flash_controller_0
    (
      .address       (epcs_flash_controller_0_epcs_control_port_address),
      .chipselect    (epcs_flash_controller_0_epcs_control_port_chipselect),
      .clk           (pll_c0_out),
      .dataavailable (epcs_flash_controller_0_epcs_control_port_dataavailable),
      .endofpacket   (epcs_flash_controller_0_epcs_control_port_endofpacket),
      .irq           (epcs_flash_controller_0_epcs_control_port_irq),
      .read_n        (epcs_flash_controller_0_epcs_control_port_read_n),
      .readdata      (epcs_flash_controller_0_epcs_control_port_readdata),
      .readyfordata  (epcs_flash_controller_0_epcs_control_port_readyfordata),
      .reset_n       (epcs_flash_controller_0_epcs_control_port_reset_n),
      .write_n       (epcs_flash_controller_0_epcs_control_port_write_n),
      .writedata     (epcs_flash_controller_0_epcs_control_port_writedata)
    );

  jtag_uart_avalon_jtag_slave_arbitrator the_jtag_uart_avalon_jtag_slave
    (
      .clk                                                           (pll_c0_out),
      .cpu_data_master_address_to_slave                              (cpu_data_master_address_to_slave),
      .cpu_data_master_granted_jtag_uart_avalon_jtag_slave           (cpu_data_master_granted_jtag_uart_avalon_jtag_slave),
      .cpu_data_master_latency_counter                               (cpu_data_master_latency_counter),
      .cpu_data_master_qualified_request_jtag_uart_avalon_jtag_slave (cpu_data_master_qualified_request_jtag_uart_avalon_jtag_slave),
      .cpu_data_master_read                                          (cpu_data_master_read),
      .cpu_data_master_read_data_valid_jtag_uart_avalon_jtag_slave   (cpu_data_master_read_data_valid_jtag_uart_avalon_jtag_slave),
      .cpu_data_master_requests_jtag_uart_avalon_jtag_slave          (cpu_data_master_requests_jtag_uart_avalon_jtag_slave),
      .cpu_data_master_write                                         (cpu_data_master_write),
      .cpu_data_master_writedata                                     (cpu_data_master_writedata),
      .d1_jtag_uart_avalon_jtag_slave_end_xfer                       (d1_jtag_uart_avalon_jtag_slave_end_xfer),
      .jtag_uart_avalon_jtag_slave_address                           (jtag_uart_avalon_jtag_slave_address),
      .jtag_uart_avalon_jtag_slave_chipselect                        (jtag_uart_avalon_jtag_slave_chipselect),
      .jtag_uart_avalon_jtag_slave_dataavailable                     (jtag_uart_avalon_jtag_slave_dataavailable),
      .jtag_uart_avalon_jtag_slave_dataavailable_from_sa             (jtag_uart_avalon_jtag_slave_dataavailable_from_sa),
      .jtag_uart_avalon_jtag_slave_irq                               (jtag_uart_avalon_jtag_slave_irq),
      .jtag_uart_avalon_jtag_slave_irq_from_sa                       (jtag_uart_avalon_jtag_slave_irq_from_sa),
      .jtag_uart_avalon_jtag_slave_read_n                            (jtag_uart_avalon_jtag_slave_read_n),
      .jtag_uart_avalon_jtag_slave_readdata                          (jtag_uart_avalon_jtag_slave_readdata),
      .jtag_uart_avalon_jtag_slave_readdata_from_sa                  (jtag_uart_avalon_jtag_slave_readdata_from_sa),
      .jtag_uart_avalon_jtag_slave_readyfordata                      (jtag_uart_avalon_jtag_slave_readyfordata),
      .jtag_uart_avalon_jtag_slave_readyfordata_from_sa              (jtag_uart_avalon_jtag_slave_readyfordata_from_sa),
      .jtag_uart_avalon_jtag_slave_reset_n                           (jtag_uart_avalon_jtag_slave_reset_n),
      .jtag_uart_avalon_jtag_slave_waitrequest                       (jtag_uart_avalon_jtag_slave_waitrequest),
      .jtag_uart_avalon_jtag_slave_waitrequest_from_sa               (jtag_uart_avalon_jtag_slave_waitrequest_from_sa),
      .jtag_uart_avalon_jtag_slave_write_n                           (jtag_uart_avalon_jtag_slave_write_n),
      .jtag_uart_avalon_jtag_slave_writedata                         (jtag_uart_avalon_jtag_slave_writedata),
      .reset_n                                                       (pll_c0_out_reset_n)
    );

  jtag_uart the_jtag_uart
    (
      .av_address     (jtag_uart_avalon_jtag_slave_address),
      .av_chipselect  (jtag_uart_avalon_jtag_slave_chipselect),
      .av_irq         (jtag_uart_avalon_jtag_slave_irq),
      .av_read_n      (jtag_uart_avalon_jtag_slave_read_n),
      .av_readdata    (jtag_uart_avalon_jtag_slave_readdata),
      .av_waitrequest (jtag_uart_avalon_jtag_slave_waitrequest),
      .av_write_n     (jtag_uart_avalon_jtag_slave_write_n),
      .av_writedata   (jtag_uart_avalon_jtag_slave_writedata),
      .clk            (pll_c0_out),
      .dataavailable  (jtag_uart_avalon_jtag_slave_dataavailable),
      .readyfordata   (jtag_uart_avalon_jtag_slave_readyfordata),
      .rst_n          (jtag_uart_avalon_jtag_slave_reset_n)
    );

  lcd_control_slave_arbitrator the_lcd_control_slave
    (
      .clk                                                 (pll_c0_out),
      .cpu_data_master_address_to_slave                    (cpu_data_master_address_to_slave),
      .cpu_data_master_byteenable                          (cpu_data_master_byteenable),
      .cpu_data_master_granted_lcd_control_slave           (cpu_data_master_granted_lcd_control_slave),
      .cpu_data_master_latency_counter                     (cpu_data_master_latency_counter),
      .cpu_data_master_qualified_request_lcd_control_slave (cpu_data_master_qualified_request_lcd_control_slave),
      .cpu_data_master_read                                (cpu_data_master_read),
      .cpu_data_master_read_data_valid_lcd_control_slave   (cpu_data_master_read_data_valid_lcd_control_slave),
      .cpu_data_master_requests_lcd_control_slave          (cpu_data_master_requests_lcd_control_slave),
      .cpu_data_master_write                               (cpu_data_master_write),
      .cpu_data_master_writedata                           (cpu_data_master_writedata),
      .d1_lcd_control_slave_end_xfer                       (d1_lcd_control_slave_end_xfer),
      .lcd_control_slave_address                           (lcd_control_slave_address),
      .lcd_control_slave_begintransfer                     (lcd_control_slave_begintransfer),
      .lcd_control_slave_read                              (lcd_control_slave_read),
      .lcd_control_slave_readdata                          (lcd_control_slave_readdata),
      .lcd_control_slave_readdata_from_sa                  (lcd_control_slave_readdata_from_sa),
      .lcd_control_slave_reset_n                           (lcd_control_slave_reset_n),
      .lcd_control_slave_wait_counter_eq_0                 (lcd_control_slave_wait_counter_eq_0),
      .lcd_control_slave_write                             (lcd_control_slave_write),
      .lcd_control_slave_writedata                         (lcd_control_slave_writedata),
      .reset_n                                             (pll_c0_out_reset_n)
    );

  lcd the_lcd
    (
      .LCD_E         (LCD_E_from_the_lcd),
      .LCD_RS        (LCD_RS_from_the_lcd),
      .LCD_RW        (LCD_RW_from_the_lcd),
      .LCD_data      (LCD_data_to_and_from_the_lcd),
      .address       (lcd_control_slave_address),
      .begintransfer (lcd_control_slave_begintransfer),
      .clk           (pll_c0_out),
      .read          (lcd_control_slave_read),
      .readdata      (lcd_control_slave_readdata),
      .reset_n       (lcd_control_slave_reset_n),
      .write         (lcd_control_slave_write),
      .writedata     (lcd_control_slave_writedata)
    );

  onchip_mem_s1_arbitrator the_onchip_mem_s1
    (
      .clk                                                    (pll_c0_out),
      .cpu_data_master_address_to_slave                       (cpu_data_master_address_to_slave),
      .cpu_data_master_byteenable                             (cpu_data_master_byteenable),
      .cpu_data_master_granted_onchip_mem_s1                  (cpu_data_master_granted_onchip_mem_s1),
      .cpu_data_master_latency_counter                        (cpu_data_master_latency_counter),
      .cpu_data_master_qualified_request_onchip_mem_s1        (cpu_data_master_qualified_request_onchip_mem_s1),
      .cpu_data_master_read                                   (cpu_data_master_read),
      .cpu_data_master_read_data_valid_onchip_mem_s1          (cpu_data_master_read_data_valid_onchip_mem_s1),
      .cpu_data_master_requests_onchip_mem_s1                 (cpu_data_master_requests_onchip_mem_s1),
      .cpu_data_master_write                                  (cpu_data_master_write),
      .cpu_data_master_writedata                              (cpu_data_master_writedata),
      .cpu_instruction_master_address_to_slave                (cpu_instruction_master_address_to_slave),
      .cpu_instruction_master_granted_onchip_mem_s1           (cpu_instruction_master_granted_onchip_mem_s1),
      .cpu_instruction_master_latency_counter                 (cpu_instruction_master_latency_counter),
      .cpu_instruction_master_qualified_request_onchip_mem_s1 (cpu_instruction_master_qualified_request_onchip_mem_s1),
      .cpu_instruction_master_read                            (cpu_instruction_master_read),
      .cpu_instruction_master_read_data_valid_onchip_mem_s1   (cpu_instruction_master_read_data_valid_onchip_mem_s1),
      .cpu_instruction_master_requests_onchip_mem_s1          (cpu_instruction_master_requests_onchip_mem_s1),
      .d1_onchip_mem_s1_end_xfer                              (d1_onchip_mem_s1_end_xfer),
      .onchip_mem_s1_address                                  (onchip_mem_s1_address),
      .onchip_mem_s1_byteenable                               (onchip_mem_s1_byteenable),
      .onchip_mem_s1_chipselect                               (onchip_mem_s1_chipselect),
      .onchip_mem_s1_clken                                    (onchip_mem_s1_clken),
      .onchip_mem_s1_readdata                                 (onchip_mem_s1_readdata),
      .onchip_mem_s1_readdata_from_sa                         (onchip_mem_s1_readdata_from_sa),
      .onchip_mem_s1_reset                                    (onchip_mem_s1_reset),
      .onchip_mem_s1_write                                    (onchip_mem_s1_write),
      .onchip_mem_s1_writedata                                (onchip_mem_s1_writedata),
      .reset_n                                                (pll_c0_out_reset_n)
    );

  onchip_mem the_onchip_mem
    (
      .address    (onchip_mem_s1_address),
      .byteenable (onchip_mem_s1_byteenable),
      .chipselect (onchip_mem_s1_chipselect),
      .clk        (pll_c0_out),
      .clken      (onchip_mem_s1_clken),
      .readdata   (onchip_mem_s1_readdata),
      .reset      (onchip_mem_s1_reset),
      .write      (onchip_mem_s1_write),
      .writedata  (onchip_mem_s1_writedata)
    );

  pio_button_s1_arbitrator the_pio_button_s1
    (
      .clk                                             (pll_c0_out),
      .cpu_data_master_address_to_slave                (cpu_data_master_address_to_slave),
      .cpu_data_master_granted_pio_button_s1           (cpu_data_master_granted_pio_button_s1),
      .cpu_data_master_latency_counter                 (cpu_data_master_latency_counter),
      .cpu_data_master_qualified_request_pio_button_s1 (cpu_data_master_qualified_request_pio_button_s1),
      .cpu_data_master_read                            (cpu_data_master_read),
      .cpu_data_master_read_data_valid_pio_button_s1   (cpu_data_master_read_data_valid_pio_button_s1),
      .cpu_data_master_requests_pio_button_s1          (cpu_data_master_requests_pio_button_s1),
      .cpu_data_master_write                           (cpu_data_master_write),
      .cpu_data_master_writedata                       (cpu_data_master_writedata),
      .d1_pio_button_s1_end_xfer                       (d1_pio_button_s1_end_xfer),
      .pio_button_s1_address                           (pio_button_s1_address),
      .pio_button_s1_chipselect                        (pio_button_s1_chipselect),
      .pio_button_s1_irq                               (pio_button_s1_irq),
      .pio_button_s1_irq_from_sa                       (pio_button_s1_irq_from_sa),
      .pio_button_s1_readdata                          (pio_button_s1_readdata),
      .pio_button_s1_readdata_from_sa                  (pio_button_s1_readdata_from_sa),
      .pio_button_s1_reset_n                           (pio_button_s1_reset_n),
      .pio_button_s1_write_n                           (pio_button_s1_write_n),
      .pio_button_s1_writedata                         (pio_button_s1_writedata),
      .reset_n                                         (pll_c0_out_reset_n)
    );

  pio_button the_pio_button
    (
      .address    (pio_button_s1_address),
      .chipselect (pio_button_s1_chipselect),
      .clk        (pll_c0_out),
      .in_port    (in_port_to_the_pio_button),
      .irq        (pio_button_s1_irq),
      .readdata   (pio_button_s1_readdata),
      .reset_n    (pio_button_s1_reset_n),
      .write_n    (pio_button_s1_write_n),
      .writedata  (pio_button_s1_writedata)
    );

  pio_switch_s1_arbitrator the_pio_switch_s1
    (
      .clk                                             (pll_c0_out),
      .cpu_data_master_address_to_slave                (cpu_data_master_address_to_slave),
      .cpu_data_master_granted_pio_switch_s1           (cpu_data_master_granted_pio_switch_s1),
      .cpu_data_master_latency_counter                 (cpu_data_master_latency_counter),
      .cpu_data_master_qualified_request_pio_switch_s1 (cpu_data_master_qualified_request_pio_switch_s1),
      .cpu_data_master_read                            (cpu_data_master_read),
      .cpu_data_master_read_data_valid_pio_switch_s1   (cpu_data_master_read_data_valid_pio_switch_s1),
      .cpu_data_master_requests_pio_switch_s1          (cpu_data_master_requests_pio_switch_s1),
      .cpu_data_master_write                           (cpu_data_master_write),
      .d1_pio_switch_s1_end_xfer                       (d1_pio_switch_s1_end_xfer),
      .pio_switch_s1_address                           (pio_switch_s1_address),
      .pio_switch_s1_readdata                          (pio_switch_s1_readdata),
      .pio_switch_s1_readdata_from_sa                  (pio_switch_s1_readdata_from_sa),
      .pio_switch_s1_reset_n                           (pio_switch_s1_reset_n),
      .reset_n                                         (pll_c0_out_reset_n)
    );

  pio_switch the_pio_switch
    (
      .address  (pio_switch_s1_address),
      .clk      (pll_c0_out),
      .in_port  (in_port_to_the_pio_switch),
      .readdata (pio_switch_s1_readdata),
      .reset_n  (pio_switch_s1_reset_n)
    );

  pll_s1_arbitrator the_pll_s1
    (
      .DE2_70_SOPC_clock_0_out_address_to_slave         (DE2_70_SOPC_clock_0_out_address_to_slave),
      .DE2_70_SOPC_clock_0_out_granted_pll_s1           (DE2_70_SOPC_clock_0_out_granted_pll_s1),
      .DE2_70_SOPC_clock_0_out_nativeaddress            (DE2_70_SOPC_clock_0_out_nativeaddress),
      .DE2_70_SOPC_clock_0_out_qualified_request_pll_s1 (DE2_70_SOPC_clock_0_out_qualified_request_pll_s1),
      .DE2_70_SOPC_clock_0_out_read                     (DE2_70_SOPC_clock_0_out_read),
      .DE2_70_SOPC_clock_0_out_read_data_valid_pll_s1   (DE2_70_SOPC_clock_0_out_read_data_valid_pll_s1),
      .DE2_70_SOPC_clock_0_out_requests_pll_s1          (DE2_70_SOPC_clock_0_out_requests_pll_s1),
      .DE2_70_SOPC_clock_0_out_write                    (DE2_70_SOPC_clock_0_out_write),
      .DE2_70_SOPC_clock_0_out_writedata                (DE2_70_SOPC_clock_0_out_writedata),
      .clk                                              (clk_50),
      .d1_pll_s1_end_xfer                               (d1_pll_s1_end_xfer),
      .pll_s1_address                                   (pll_s1_address),
      .pll_s1_chipselect                                (pll_s1_chipselect),
      .pll_s1_read                                      (pll_s1_read),
      .pll_s1_readdata                                  (pll_s1_readdata),
      .pll_s1_readdata_from_sa                          (pll_s1_readdata_from_sa),
      .pll_s1_reset_n                                   (pll_s1_reset_n),
      .pll_s1_resetrequest                              (pll_s1_resetrequest),
      .pll_s1_resetrequest_from_sa                      (pll_s1_resetrequest_from_sa),
      .pll_s1_write                                     (pll_s1_write),
      .pll_s1_writedata                                 (pll_s1_writedata),
      .reset_n                                          (clk_50_reset_n)
    );

  //pll_c0_out out_clk assignment, which is an e_assign
  assign pll_c0_out = out_clk_pll_c0;

  //pll_c1_out out_clk assignment, which is an e_assign
  assign pll_c1_out = out_clk_pll_c1;

  //pll_c2_audio out_clk assignment, which is an e_assign
  assign pll_c2_audio = out_clk_pll_c2;

  pll the_pll
    (
      .address      (pll_s1_address),
      .c0           (out_clk_pll_c0),
      .c1           (out_clk_pll_c1),
      .c2           (out_clk_pll_c2),
      .chipselect   (pll_s1_chipselect),
      .clk          (clk_50),
      .read         (pll_s1_read),
      .readdata     (pll_s1_readdata),
      .reset_n      (pll_s1_reset_n),
      .resetrequest (pll_s1_resetrequest),
      .write        (pll_s1_write),
      .writedata    (pll_s1_writedata)
    );

  sysid_control_slave_arbitrator the_sysid_control_slave
    (
      .clk                                                   (pll_c0_out),
      .cpu_data_master_address_to_slave                      (cpu_data_master_address_to_slave),
      .cpu_data_master_granted_sysid_control_slave           (cpu_data_master_granted_sysid_control_slave),
      .cpu_data_master_latency_counter                       (cpu_data_master_latency_counter),
      .cpu_data_master_qualified_request_sysid_control_slave (cpu_data_master_qualified_request_sysid_control_slave),
      .cpu_data_master_read                                  (cpu_data_master_read),
      .cpu_data_master_read_data_valid_sysid_control_slave   (cpu_data_master_read_data_valid_sysid_control_slave),
      .cpu_data_master_requests_sysid_control_slave          (cpu_data_master_requests_sysid_control_slave),
      .cpu_data_master_write                                 (cpu_data_master_write),
      .d1_sysid_control_slave_end_xfer                       (d1_sysid_control_slave_end_xfer),
      .reset_n                                               (pll_c0_out_reset_n),
      .sysid_control_slave_address                           (sysid_control_slave_address),
      .sysid_control_slave_readdata                          (sysid_control_slave_readdata),
      .sysid_control_slave_readdata_from_sa                  (sysid_control_slave_readdata_from_sa),
      .sysid_control_slave_reset_n                           (sysid_control_slave_reset_n)
    );

  sysid the_sysid
    (
      .address  (sysid_control_slave_address),
      .clock    (sysid_control_slave_clock),
      .readdata (sysid_control_slave_readdata),
      .reset_n  (sysid_control_slave_reset_n)
    );

  timer_s1_arbitrator the_timer_s1
    (
      .clk                                        (pll_c0_out),
      .cpu_data_master_address_to_slave           (cpu_data_master_address_to_slave),
      .cpu_data_master_granted_timer_s1           (cpu_data_master_granted_timer_s1),
      .cpu_data_master_latency_counter            (cpu_data_master_latency_counter),
      .cpu_data_master_qualified_request_timer_s1 (cpu_data_master_qualified_request_timer_s1),
      .cpu_data_master_read                       (cpu_data_master_read),
      .cpu_data_master_read_data_valid_timer_s1   (cpu_data_master_read_data_valid_timer_s1),
      .cpu_data_master_requests_timer_s1          (cpu_data_master_requests_timer_s1),
      .cpu_data_master_write                      (cpu_data_master_write),
      .cpu_data_master_writedata                  (cpu_data_master_writedata),
      .d1_timer_s1_end_xfer                       (d1_timer_s1_end_xfer),
      .reset_n                                    (pll_c0_out_reset_n),
      .timer_s1_address                           (timer_s1_address),
      .timer_s1_chipselect                        (timer_s1_chipselect),
      .timer_s1_irq                               (timer_s1_irq),
      .timer_s1_irq_from_sa                       (timer_s1_irq_from_sa),
      .timer_s1_readdata                          (timer_s1_readdata),
      .timer_s1_readdata_from_sa                  (timer_s1_readdata_from_sa),
      .timer_s1_reset_n                           (timer_s1_reset_n),
      .timer_s1_write_n                           (timer_s1_write_n),
      .timer_s1_writedata                         (timer_s1_writedata)
    );

  timer the_timer
    (
      .address    (timer_s1_address),
      .chipselect (timer_s1_chipselect),
      .clk        (pll_c0_out),
      .irq        (timer_s1_irq),
      .readdata   (timer_s1_readdata),
      .reset_n    (timer_s1_reset_n),
      .write_n    (timer_s1_write_n),
      .writedata  (timer_s1_writedata)
    );

  timer_stamp_s1_arbitrator the_timer_stamp_s1
    (
      .clk                                              (pll_c0_out),
      .cpu_data_master_address_to_slave                 (cpu_data_master_address_to_slave),
      .cpu_data_master_granted_timer_stamp_s1           (cpu_data_master_granted_timer_stamp_s1),
      .cpu_data_master_latency_counter                  (cpu_data_master_latency_counter),
      .cpu_data_master_qualified_request_timer_stamp_s1 (cpu_data_master_qualified_request_timer_stamp_s1),
      .cpu_data_master_read                             (cpu_data_master_read),
      .cpu_data_master_read_data_valid_timer_stamp_s1   (cpu_data_master_read_data_valid_timer_stamp_s1),
      .cpu_data_master_requests_timer_stamp_s1          (cpu_data_master_requests_timer_stamp_s1),
      .cpu_data_master_write                            (cpu_data_master_write),
      .cpu_data_master_writedata                        (cpu_data_master_writedata),
      .d1_timer_stamp_s1_end_xfer                       (d1_timer_stamp_s1_end_xfer),
      .reset_n                                          (pll_c0_out_reset_n),
      .timer_stamp_s1_address                           (timer_stamp_s1_address),
      .timer_stamp_s1_chipselect                        (timer_stamp_s1_chipselect),
      .timer_stamp_s1_irq                               (timer_stamp_s1_irq),
      .timer_stamp_s1_irq_from_sa                       (timer_stamp_s1_irq_from_sa),
      .timer_stamp_s1_readdata                          (timer_stamp_s1_readdata),
      .timer_stamp_s1_readdata_from_sa                  (timer_stamp_s1_readdata_from_sa),
      .timer_stamp_s1_reset_n                           (timer_stamp_s1_reset_n),
      .timer_stamp_s1_write_n                           (timer_stamp_s1_write_n),
      .timer_stamp_s1_writedata                         (timer_stamp_s1_writedata)
    );

  timer_stamp the_timer_stamp
    (
      .address    (timer_stamp_s1_address),
      .chipselect (timer_stamp_s1_chipselect),
      .clk        (pll_c0_out),
      .irq        (timer_stamp_s1_irq),
      .readdata   (timer_stamp_s1_readdata),
      .reset_n    (timer_stamp_s1_reset_n),
      .write_n    (timer_stamp_s1_write_n),
      .writedata  (timer_stamp_s1_writedata)
    );

  tristate_bridge_ssram_avalon_slave_arbitrator the_tristate_bridge_ssram_avalon_slave
    (
      .address_to_the_ssram                              (address_to_the_ssram),
      .adsc_n_to_the_ssram                               (adsc_n_to_the_ssram),
      .bw_n_to_the_ssram                                 (bw_n_to_the_ssram),
      .bwe_n_to_the_ssram                                (bwe_n_to_the_ssram),
      .chipenable1_n_to_the_ssram                        (chipenable1_n_to_the_ssram),
      .clk                                               (pll_c0_out),
      .cpu_data_master_address_to_slave                  (cpu_data_master_address_to_slave),
      .cpu_data_master_byteenable                        (cpu_data_master_byteenable),
      .cpu_data_master_granted_ssram_s1                  (cpu_data_master_granted_ssram_s1),
      .cpu_data_master_latency_counter                   (cpu_data_master_latency_counter),
      .cpu_data_master_qualified_request_ssram_s1        (cpu_data_master_qualified_request_ssram_s1),
      .cpu_data_master_read                              (cpu_data_master_read),
      .cpu_data_master_read_data_valid_ssram_s1          (cpu_data_master_read_data_valid_ssram_s1),
      .cpu_data_master_requests_ssram_s1                 (cpu_data_master_requests_ssram_s1),
      .cpu_data_master_write                             (cpu_data_master_write),
      .cpu_data_master_writedata                         (cpu_data_master_writedata),
      .cpu_instruction_master_address_to_slave           (cpu_instruction_master_address_to_slave),
      .cpu_instruction_master_granted_ssram_s1           (cpu_instruction_master_granted_ssram_s1),
      .cpu_instruction_master_latency_counter            (cpu_instruction_master_latency_counter),
      .cpu_instruction_master_qualified_request_ssram_s1 (cpu_instruction_master_qualified_request_ssram_s1),
      .cpu_instruction_master_read                       (cpu_instruction_master_read),
      .cpu_instruction_master_read_data_valid_ssram_s1   (cpu_instruction_master_read_data_valid_ssram_s1),
      .cpu_instruction_master_requests_ssram_s1          (cpu_instruction_master_requests_ssram_s1),
      .d1_tristate_bridge_ssram_avalon_slave_end_xfer    (d1_tristate_bridge_ssram_avalon_slave_end_xfer),
      .data_to_and_from_the_ssram                        (data_to_and_from_the_ssram),
      .incoming_data_to_and_from_the_ssram               (incoming_data_to_and_from_the_ssram),
      .outputenable_n_to_the_ssram                       (outputenable_n_to_the_ssram),
      .reset_n                                           (pll_c0_out_reset_n),
      .reset_n_to_the_ssram                              (reset_n_to_the_ssram)
    );

  //reset is asserted asynchronously and deasserted synchronously
  DE2_70_SOPC_reset_pll_c0_out_domain_synch_module DE2_70_SOPC_reset_pll_c0_out_domain_synch
    (
      .clk      (pll_c0_out),
      .data_in  (1'b1),
      .data_out (pll_c0_out_reset_n),
      .reset_n  (reset_n_sources)
    );

  //reset sources mux, which is an e_mux
  assign reset_n_sources = ~(~reset_n |
    0 |
    0 |
    cpu_jtag_debug_module_resetrequest_from_sa |
    cpu_jtag_debug_module_resetrequest_from_sa |
    0 |
    pll_s1_resetrequest_from_sa |
    pll_s1_resetrequest_from_sa);

  //reset is asserted asynchronously and deasserted synchronously
  DE2_70_SOPC_reset_clk_25_domain_synch_module DE2_70_SOPC_reset_clk_25_domain_synch
    (
      .clk      (clk_25),
      .data_in  (1'b1),
      .data_out (clk_25_reset_n),
      .reset_n  (reset_n_sources)
    );

  //reset is asserted asynchronously and deasserted synchronously
  DE2_70_SOPC_reset_clk_50_domain_synch_module DE2_70_SOPC_reset_clk_50_domain_synch
    (
      .clk      (clk_50),
      .data_in  (1'b1),
      .data_out (clk_50_reset_n),
      .reset_n  (reset_n_sources)
    );

  //DE2_70_SOPC_clock_0_out_endofpacket of type endofpacket does not connect to anything so wire it to default (0)
  assign DE2_70_SOPC_clock_0_out_endofpacket = 0;

  //DE2_70_SOPC_clock_1_out_endofpacket of type endofpacket does not connect to anything so wire it to default (0)
  assign DE2_70_SOPC_clock_1_out_endofpacket = 0;

  //sysid_control_slave_clock of type clock does not connect to anything so wire it to default (0)
  assign sysid_control_slave_clock = 0;


endmodule


// synthesis translate_off
`timescale 1ns / 1ps
// synthesis translate_on

// turn off superfluous verilog processor warnings 
// altera message_level Level1 
// altera message_off 10034 10035 10036 10037 10230 10240 10030 

module ssram_lane0_module (
                            // inputs:
                             clk,
                             data,
                             rdaddress,
                             rdclken,
                             reset_n,
                             wraddress,
                             wrclock,
                             wren,

                            // outputs:
                             q
                          )
;

  output  [  7: 0] q;
  input            clk;
  input   [  7: 0] data;
  input   [ 18: 0] rdaddress;
  input            rdclken;
  input            reset_n;
  input   [ 18: 0] wraddress;
  input            wrclock;
  input            wren;

  reg     [ 18: 0] d1_rdaddress;
  reg     [  7: 0] mem_array [524287: 0];
  wire    [  7: 0] q;
  reg     [ 18: 0] read_address;

//synthesis translate_off
//////////////// SIMULATION-ONLY CONTENTS
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
        begin
          d1_rdaddress <= 0;
          read_address <= 0;
        end
      else if (rdclken)
        begin
          d1_rdaddress <= rdaddress;
          read_address <= d1_rdaddress;
        end
    end


  // Data read is synchronized through latent_rdaddress.
  assign q = mem_array[read_address];

initial
    $readmemh("ssram_lane0.dat", mem_array);
  always @(posedge wrclock)
    begin
      // Write data
      if (wren)
          mem_array[wraddress] <= data;
    end



//////////////// END SIMULATION-ONLY CONTENTS

//synthesis translate_on
//synthesis read_comments_as_HDL on
//  always @(rdaddress)
//    begin
//      read_address = rdaddress;
//    end
//
//
//  lpm_ram_dp lpm_ram_dp_component
//    (
//      .data (data),
//      .q (q),
//      .rdaddress (read_address),
//      .rdclken (rdclken),
//      .rdclock (clk),
//      .wraddress (wraddress),
//      .wrclock (wrclock),
//      .wren (wren)
//    );
//
//  defparam lpm_ram_dp_component.lpm_file = "ssram_lane0.mif",
//           lpm_ram_dp_component.lpm_hint = "USE_EAB=ON",
//           lpm_ram_dp_component.lpm_indata = "REGISTERED",
//           lpm_ram_dp_component.lpm_outdata = "REGISTERED",
//           lpm_ram_dp_component.lpm_rdaddress_control = "REGISTERED",
//           lpm_ram_dp_component.lpm_width = 8,
//           lpm_ram_dp_component.lpm_widthad = 19,
//           lpm_ram_dp_component.lpm_wraddress_control = "REGISTERED",
//           lpm_ram_dp_component.suppress_memory_conversion_warnings = "ON";
//
//synthesis read_comments_as_HDL off

endmodule


// synthesis translate_off
`timescale 1ns / 1ps
// synthesis translate_on

// turn off superfluous verilog processor warnings 
// altera message_level Level1 
// altera message_off 10034 10035 10036 10037 10230 10240 10030 

module ssram_lane1_module (
                            // inputs:
                             clk,
                             data,
                             rdaddress,
                             rdclken,
                             reset_n,
                             wraddress,
                             wrclock,
                             wren,

                            // outputs:
                             q
                          )
;

  output  [  7: 0] q;
  input            clk;
  input   [  7: 0] data;
  input   [ 18: 0] rdaddress;
  input            rdclken;
  input            reset_n;
  input   [ 18: 0] wraddress;
  input            wrclock;
  input            wren;

  reg     [ 18: 0] d1_rdaddress;
  reg     [  7: 0] mem_array [524287: 0];
  wire    [  7: 0] q;
  reg     [ 18: 0] read_address;

//synthesis translate_off
//////////////// SIMULATION-ONLY CONTENTS
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
        begin
          d1_rdaddress <= 0;
          read_address <= 0;
        end
      else if (rdclken)
        begin
          d1_rdaddress <= rdaddress;
          read_address <= d1_rdaddress;
        end
    end


  // Data read is synchronized through latent_rdaddress.
  assign q = mem_array[read_address];

initial
    $readmemh("ssram_lane1.dat", mem_array);
  always @(posedge wrclock)
    begin
      // Write data
      if (wren)
          mem_array[wraddress] <= data;
    end



//////////////// END SIMULATION-ONLY CONTENTS

//synthesis translate_on
//synthesis read_comments_as_HDL on
//  always @(rdaddress)
//    begin
//      read_address = rdaddress;
//    end
//
//
//  lpm_ram_dp lpm_ram_dp_component
//    (
//      .data (data),
//      .q (q),
//      .rdaddress (read_address),
//      .rdclken (rdclken),
//      .rdclock (clk),
//      .wraddress (wraddress),
//      .wrclock (wrclock),
//      .wren (wren)
//    );
//
//  defparam lpm_ram_dp_component.lpm_file = "ssram_lane1.mif",
//           lpm_ram_dp_component.lpm_hint = "USE_EAB=ON",
//           lpm_ram_dp_component.lpm_indata = "REGISTERED",
//           lpm_ram_dp_component.lpm_outdata = "REGISTERED",
//           lpm_ram_dp_component.lpm_rdaddress_control = "REGISTERED",
//           lpm_ram_dp_component.lpm_width = 8,
//           lpm_ram_dp_component.lpm_widthad = 19,
//           lpm_ram_dp_component.lpm_wraddress_control = "REGISTERED",
//           lpm_ram_dp_component.suppress_memory_conversion_warnings = "ON";
//
//synthesis read_comments_as_HDL off

endmodule


// synthesis translate_off
`timescale 1ns / 1ps
// synthesis translate_on

// turn off superfluous verilog processor warnings 
// altera message_level Level1 
// altera message_off 10034 10035 10036 10037 10230 10240 10030 

module ssram_lane2_module (
                            // inputs:
                             clk,
                             data,
                             rdaddress,
                             rdclken,
                             reset_n,
                             wraddress,
                             wrclock,
                             wren,

                            // outputs:
                             q
                          )
;

  output  [  7: 0] q;
  input            clk;
  input   [  7: 0] data;
  input   [ 18: 0] rdaddress;
  input            rdclken;
  input            reset_n;
  input   [ 18: 0] wraddress;
  input            wrclock;
  input            wren;

  reg     [ 18: 0] d1_rdaddress;
  reg     [  7: 0] mem_array [524287: 0];
  wire    [  7: 0] q;
  reg     [ 18: 0] read_address;

//synthesis translate_off
//////////////// SIMULATION-ONLY CONTENTS
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
        begin
          d1_rdaddress <= 0;
          read_address <= 0;
        end
      else if (rdclken)
        begin
          d1_rdaddress <= rdaddress;
          read_address <= d1_rdaddress;
        end
    end


  // Data read is synchronized through latent_rdaddress.
  assign q = mem_array[read_address];

initial
    $readmemh("ssram_lane2.dat", mem_array);
  always @(posedge wrclock)
    begin
      // Write data
      if (wren)
          mem_array[wraddress] <= data;
    end



//////////////// END SIMULATION-ONLY CONTENTS

//synthesis translate_on
//synthesis read_comments_as_HDL on
//  always @(rdaddress)
//    begin
//      read_address = rdaddress;
//    end
//
//
//  lpm_ram_dp lpm_ram_dp_component
//    (
//      .data (data),
//      .q (q),
//      .rdaddress (read_address),
//      .rdclken (rdclken),
//      .rdclock (clk),
//      .wraddress (wraddress),
//      .wrclock (wrclock),
//      .wren (wren)
//    );
//
//  defparam lpm_ram_dp_component.lpm_file = "ssram_lane2.mif",
//           lpm_ram_dp_component.lpm_hint = "USE_EAB=ON",
//           lpm_ram_dp_component.lpm_indata = "REGISTERED",
//           lpm_ram_dp_component.lpm_outdata = "REGISTERED",
//           lpm_ram_dp_component.lpm_rdaddress_control = "REGISTERED",
//           lpm_ram_dp_component.lpm_width = 8,
//           lpm_ram_dp_component.lpm_widthad = 19,
//           lpm_ram_dp_component.lpm_wraddress_control = "REGISTERED",
//           lpm_ram_dp_component.suppress_memory_conversion_warnings = "ON";
//
//synthesis read_comments_as_HDL off

endmodule


// synthesis translate_off
`timescale 1ns / 1ps
// synthesis translate_on

// turn off superfluous verilog processor warnings 
// altera message_level Level1 
// altera message_off 10034 10035 10036 10037 10230 10240 10030 

module ssram_lane3_module (
                            // inputs:
                             clk,
                             data,
                             rdaddress,
                             rdclken,
                             reset_n,
                             wraddress,
                             wrclock,
                             wren,

                            // outputs:
                             q
                          )
;

  output  [  7: 0] q;
  input            clk;
  input   [  7: 0] data;
  input   [ 18: 0] rdaddress;
  input            rdclken;
  input            reset_n;
  input   [ 18: 0] wraddress;
  input            wrclock;
  input            wren;

  reg     [ 18: 0] d1_rdaddress;
  reg     [  7: 0] mem_array [524287: 0];
  wire    [  7: 0] q;
  reg     [ 18: 0] read_address;

//synthesis translate_off
//////////////// SIMULATION-ONLY CONTENTS
  always @(posedge clk or negedge reset_n)
    begin
      if (reset_n == 0)
        begin
          d1_rdaddress <= 0;
          read_address <= 0;
        end
      else if (rdclken)
        begin
          d1_rdaddress <= rdaddress;
          read_address <= d1_rdaddress;
        end
    end


  // Data read is synchronized through latent_rdaddress.
  assign q = mem_array[read_address];

initial
    $readmemh("ssram_lane3.dat", mem_array);
  always @(posedge wrclock)
    begin
      // Write data
      if (wren)
          mem_array[wraddress] <= data;
    end



//////////////// END SIMULATION-ONLY CONTENTS

//synthesis translate_on
//synthesis read_comments_as_HDL on
//  always @(rdaddress)
//    begin
//      read_address = rdaddress;
//    end
//
//
//  lpm_ram_dp lpm_ram_dp_component
//    (
//      .data (data),
//      .q (q),
//      .rdaddress (read_address),
//      .rdclken (rdclken),
//      .rdclock (clk),
//      .wraddress (wraddress),
//      .wrclock (wrclock),
//      .wren (wren)
//    );
//
//  defparam lpm_ram_dp_component.lpm_file = "ssram_lane3.mif",
//           lpm_ram_dp_component.lpm_hint = "USE_EAB=ON",
//           lpm_ram_dp_component.lpm_indata = "REGISTERED",
//           lpm_ram_dp_component.lpm_outdata = "REGISTERED",
//           lpm_ram_dp_component.lpm_rdaddress_control = "REGISTERED",
//           lpm_ram_dp_component.lpm_width = 8,
//           lpm_ram_dp_component.lpm_widthad = 19,
//           lpm_ram_dp_component.lpm_wraddress_control = "REGISTERED",
//           lpm_ram_dp_component.suppress_memory_conversion_warnings = "ON";
//
//synthesis read_comments_as_HDL off

endmodule


// synthesis translate_off
`timescale 1ns / 1ps
// synthesis translate_on

// turn off superfluous verilog processor warnings 
// altera message_level Level1 
// altera message_off 10034 10035 10036 10037 10230 10240 10030 

module ssram (
               // inputs:
                address,
                adsc_n,
                bw_n,
                bwe_n,
                chipenable1_n,
                clk,
                outputenable_n,
                reset_n,

               // outputs:
                data
             )
;

  inout   [ 31: 0] data;
  input   [ 18: 0] address;
  input            adsc_n;
  input   [  3: 0] bw_n;
  input            bwe_n;
  input            chipenable1_n;
  input            clk;
  input            outputenable_n;
  input            reset_n;

  wire    [ 31: 0] data;
  wire    [  7: 0] data_0;
  wire    [  7: 0] data_1;
  wire    [  7: 0] data_2;
  wire    [  7: 0] data_3;
  wire    [ 31: 0] logic_vector_gasket;
  wire    [  7: 0] q_0;
  wire    [  7: 0] q_1;
  wire    [  7: 0] q_2;
  wire    [  7: 0] q_3;
  //s1, which is an e_ptf_slave

//synthesis translate_off
//////////////// SIMULATION-ONLY CONTENTS
  assign logic_vector_gasket = data;
  assign data_0 = logic_vector_gasket[7 : 0];
  //ssram_lane0, which is an e_ram
  ssram_lane0_module ssram_lane0
    (
      .clk       (clk),
      .data      (data_0),
      .q         (q_0),
      .rdaddress (address),
      .rdclken   (1'b1),
      .reset_n   (reset_n),
      .wraddress (address),
      .wrclock   (clk),
      .wren      (~chipenable1_n & ~bwe_n & ~bw_n[0])
    );

  assign data_1 = logic_vector_gasket[15 : 8];
  //ssram_lane1, which is an e_ram
  ssram_lane1_module ssram_lane1
    (
      .clk       (clk),
      .data      (data_1),
      .q         (q_1),
      .rdaddress (address),
      .rdclken   (1'b1),
      .reset_n   (reset_n),
      .wraddress (address),
      .wrclock   (clk),
      .wren      (~chipenable1_n & ~bwe_n & ~bw_n[1])
    );

  assign data_2 = logic_vector_gasket[23 : 16];
  //ssram_lane2, which is an e_ram
  ssram_lane2_module ssram_lane2
    (
      .clk       (clk),
      .data      (data_2),
      .q         (q_2),
      .rdaddress (address),
      .rdclken   (1'b1),
      .reset_n   (reset_n),
      .wraddress (address),
      .wrclock   (clk),
      .wren      (~chipenable1_n & ~bwe_n & ~bw_n[2])
    );

  assign data_3 = logic_vector_gasket[31 : 24];
  //ssram_lane3, which is an e_ram
  ssram_lane3_module ssram_lane3
    (
      .clk       (clk),
      .data      (data_3),
      .q         (q_3),
      .rdaddress (address),
      .rdclken   (1'b1),
      .reset_n   (reset_n),
      .wraddress (address),
      .wrclock   (clk),
      .wren      (~chipenable1_n & ~bwe_n & ~bw_n[3])
    );

  assign data = (~chipenable1_n & ~outputenable_n)? {q_3,
    q_2,
    q_1,
    q_0}: {32{1'bz}};


//////////////// END SIMULATION-ONLY CONTENTS

//synthesis translate_on

endmodule


//synthesis translate_off



// <ALTERA_NOTE> CODE INSERTED BETWEEN HERE

// AND HERE WILL BE PRESERVED </ALTERA_NOTE>


// If user logic components use Altsync_Ram with convert_hex2ver.dll,
// set USE_convert_hex2ver in the user comments section above

// `ifdef USE_convert_hex2ver
// `else
// `define NO_PLI 1
// `endif

`include "c:/altera/11.0sp1/quartus/eda/sim_lib/altera_mf.v"
`include "c:/altera/11.0sp1/quartus/eda/sim_lib/220model.v"
`include "c:/altera/11.0sp1/quartus/eda/sim_lib/sgate.v"
`include "SEG7.v"
`include "DM9000A.v"
`include "pll.v"
`include "altpllpll.v"
`include "pio_button.v"
`include "DE2_70_SOPC_clock_0.v"
`include "sysid.v"
`include "clock_crossing_0.v"
`include "timer.v"
`include "jtag_uart.v"
`include "pio_switch.v"
`include "lcd.v"
`include "data_in_pio.v"
`include "cpu_test_bench.v"
`include "cpu_mult_cell.v"
`include "cpu_oci_test_bench.v"
`include "cpu_jtag_debug_module_tck.v"
`include "cpu_jtag_debug_module_sysclk.v"
`include "cpu_jtag_debug_module_wrapper.v"
`include "cpu.v"
`include "DE2_70_SOPC_clock_1.v"
`include "data_out_pio.v"
`include "epcs_flash_controller_0.v"
`include "onchip_mem.v"
`include "control_out_pio.v"
`include "control_in_pio.v"
`include "DE2_70_SOPC_clock_2.v"
`include "timer_stamp.v"

`timescale 1ns / 1ps

module test_bench 
;


  wire             DE2_70_SOPC_clock_0_in_endofpacket_from_sa;
  wire             DE2_70_SOPC_clock_0_out_endofpacket;
  wire             DE2_70_SOPC_clock_1_out_endofpacket;
  wire             DE2_70_SOPC_clock_2_in_endofpacket_from_sa;
  wire             LCD_E_from_the_lcd;
  wire             LCD_RS_from_the_lcd;
  wire             LCD_RW_from_the_lcd;
  wire    [  7: 0] LCD_data_to_and_from_the_lcd;
  wire    [ 20: 0] address_to_the_ssram;
  wire             adsc_n_to_the_ssram;
  wire             avs_s1_export_ENET_CLK_from_the_DM9000A;
  wire             avs_s1_export_ENET_CMD_from_the_DM9000A;
  wire             avs_s1_export_ENET_CS_N_from_the_DM9000A;
  wire    [ 15: 0] avs_s1_export_ENET_DATA_to_and_from_the_DM9000A;
  wire             avs_s1_export_ENET_INT_to_the_DM9000A;
  wire             avs_s1_export_ENET_RD_N_from_the_DM9000A;
  wire             avs_s1_export_ENET_RST_N_from_the_DM9000A;
  wire             avs_s1_export_ENET_WR_N_from_the_DM9000A;
  wire    [ 63: 0] avs_s1_export_seg7_from_the_SEG7;
  wire    [  3: 0] bw_n_to_the_ssram;
  wire             bwe_n_to_the_ssram;
  wire             chipenable1_n_to_the_ssram;
  wire             clk;
  reg              clk_25;
  reg              clk_50;
  wire    [ 31: 0] data_to_and_from_the_ssram;
  wire             epcs_flash_controller_0_epcs_control_port_dataavailable_from_sa;
  wire             epcs_flash_controller_0_epcs_control_port_endofpacket_from_sa;
  wire             epcs_flash_controller_0_epcs_control_port_readyfordata_from_sa;
  wire    [ 31: 0] in_port_to_the_control_in_pio;
  wire    [ 31: 0] in_port_to_the_data_in_pio;
  wire    [  3: 0] in_port_to_the_pio_button;
  wire    [ 17: 0] in_port_to_the_pio_switch;
  wire             jtag_uart_avalon_jtag_slave_dataavailable_from_sa;
  wire             jtag_uart_avalon_jtag_slave_readyfordata_from_sa;
  wire    [ 31: 0] out_port_from_the_control_out_pio;
  wire    [ 31: 0] out_port_from_the_data_out_pio;
  wire             outputenable_n_to_the_ssram;
  wire             pll_c0_out;
  wire             pll_c1_out;
  wire             pll_c2_audio;
  reg              reset_n;
  wire             reset_n_to_the_ssram;
  wire             sysid_control_slave_clock;


// <ALTERA_NOTE> CODE INSERTED BETWEEN HERE
//  add your signals and additional architecture here
// AND HERE WILL BE PRESERVED </ALTERA_NOTE>

  //Set us up the Dut
  DE2_70_SOPC DUT
    (
      .LCD_E_from_the_lcd                              (LCD_E_from_the_lcd),
      .LCD_RS_from_the_lcd                             (LCD_RS_from_the_lcd),
      .LCD_RW_from_the_lcd                             (LCD_RW_from_the_lcd),
      .LCD_data_to_and_from_the_lcd                    (LCD_data_to_and_from_the_lcd),
      .address_to_the_ssram                            (address_to_the_ssram),
      .adsc_n_to_the_ssram                             (adsc_n_to_the_ssram),
      .avs_s1_export_ENET_CLK_from_the_DM9000A         (avs_s1_export_ENET_CLK_from_the_DM9000A),
      .avs_s1_export_ENET_CMD_from_the_DM9000A         (avs_s1_export_ENET_CMD_from_the_DM9000A),
      .avs_s1_export_ENET_CS_N_from_the_DM9000A        (avs_s1_export_ENET_CS_N_from_the_DM9000A),
      .avs_s1_export_ENET_DATA_to_and_from_the_DM9000A (avs_s1_export_ENET_DATA_to_and_from_the_DM9000A),
      .avs_s1_export_ENET_INT_to_the_DM9000A           (avs_s1_export_ENET_INT_to_the_DM9000A),
      .avs_s1_export_ENET_RD_N_from_the_DM9000A        (avs_s1_export_ENET_RD_N_from_the_DM9000A),
      .avs_s1_export_ENET_RST_N_from_the_DM9000A       (avs_s1_export_ENET_RST_N_from_the_DM9000A),
      .avs_s1_export_ENET_WR_N_from_the_DM9000A        (avs_s1_export_ENET_WR_N_from_the_DM9000A),
      .avs_s1_export_seg7_from_the_SEG7                (avs_s1_export_seg7_from_the_SEG7),
      .bw_n_to_the_ssram                               (bw_n_to_the_ssram),
      .bwe_n_to_the_ssram                              (bwe_n_to_the_ssram),
      .chipenable1_n_to_the_ssram                      (chipenable1_n_to_the_ssram),
      .clk_25                                          (clk_25),
      .clk_50                                          (clk_50),
      .data_to_and_from_the_ssram                      (data_to_and_from_the_ssram),
      .in_port_to_the_control_in_pio                   (in_port_to_the_control_in_pio),
      .in_port_to_the_data_in_pio                      (in_port_to_the_data_in_pio),
      .in_port_to_the_pio_button                       (in_port_to_the_pio_button),
      .in_port_to_the_pio_switch                       (in_port_to_the_pio_switch),
      .out_port_from_the_control_out_pio               (out_port_from_the_control_out_pio),
      .out_port_from_the_data_out_pio                  (out_port_from_the_data_out_pio),
      .outputenable_n_to_the_ssram                     (outputenable_n_to_the_ssram),
      .pll_c0_out                                      (pll_c0_out),
      .pll_c1_out                                      (pll_c1_out),
      .pll_c2_audio                                    (pll_c2_audio),
      .reset_n                                         (reset_n),
      .reset_n_to_the_ssram                            (reset_n_to_the_ssram)
    );

  //default value specified in MODULE data_in_pio ptf port section
  assign in_port_to_the_data_in_pio = 0;

  ssram the_ssram
    (
      .address        (address_to_the_ssram[20 : 2]),
      .adsc_n         (adsc_n_to_the_ssram),
      .bw_n           (bw_n_to_the_ssram),
      .bwe_n          (bwe_n_to_the_ssram),
      .chipenable1_n  (chipenable1_n_to_the_ssram),
      .clk            (pll_c0_out),
      .data           (data_to_and_from_the_ssram),
      .outputenable_n (outputenable_n_to_the_ssram),
      .reset_n        (reset_n_to_the_ssram)
    );

  initial
    clk_25 = 1'b0;
  always
    #20 clk_25 <= ~clk_25;
  
  initial
    clk_50 = 1'b0;
  always
    #10 clk_50 <= ~clk_50;
  
  initial 
    begin
      reset_n <= 0;
      #200 reset_n <= 1;
    end

endmodule


//synthesis translate_on