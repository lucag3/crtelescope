//
module  code_mux(
ctr      , // Mux first input
sel         , // Select input
cmd_code   , // Mux output
clk           // CLK
);
//-----------Input Ports---------------
input [4:0] ctr; 
input sel, clk ;
//-----------Output Ports---------------
output [4:0] cmd_code;
//------------Internal Variables--------
reg  [4:0] cmd_code;
reg  [4:0] trg_acc ;
//-------------Code Start-----------------
always@(posedge clk) begin
	if(sel == 0) begin 
	cmd_code = ctr;
	trg_acc = 2'h3;
	end
	else  if (sel == 1) begin
	trg_acc = 2'h3;
	cmd_code = trg_acc;
	end
	end
endmodule //End Of Module mux
