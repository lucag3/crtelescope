// AUDIO.v

// This file was auto-generated as part of a SOPC Builder generate operation.
// If you edit it your changes will probably be lost.

`timescale 1 ps / 1 ps
module AUDIO (
		input  wire        avs_s1_clk,           //       s1_clock.clk
		input  wire        avs_s1_reset,         // s1_clock_reset.reset
		input  wire [3:0]  avs_s1_address,       //             s1.address
		input  wire        avs_s1_read,          //               .read
		output wire [31:0] avs_s1_readdata,      //               .readdata
		input  wire        avs_s1_write,         //               .write
		input  wire [31:0] avs_s1_writedata,     //               .writedata
		input  wire        avs_s1_export_BCLK,   //      s1_export.export
		input  wire        avs_s1_export_DACLRC, //               .export
		output wire        avs_s1_export_DACDAT, //               .export
		input  wire        avs_s1_export_ADCLRC, //               .export
		input  wire        avs_s1_export_ADCDAT  //               .export
	);

	AUDIO_IF audio (
		.avs_s1_clk           (avs_s1_clk),           //       s1_clock.clk
		.avs_s1_reset         (avs_s1_reset),         // s1_clock_reset.reset
		.avs_s1_address       (avs_s1_address),       //             s1.address
		.avs_s1_read          (avs_s1_read),          //               .read
		.avs_s1_readdata      (avs_s1_readdata),      //               .readdata
		.avs_s1_write         (avs_s1_write),         //               .write
		.avs_s1_writedata     (avs_s1_writedata),     //               .writedata
		.avs_s1_export_BCLK   (avs_s1_export_BCLK),   //      s1_export.export
		.avs_s1_export_DACLRC (avs_s1_export_DACLRC), //               .export
		.avs_s1_export_DACDAT (avs_s1_export_DACDAT), //               .export
		.avs_s1_export_ADCLRC (avs_s1_export_ADCLRC), //               .export
		.avs_s1_export_ADCDAT (avs_s1_export_ADCDAT)  //               .export
	);

endmodule
