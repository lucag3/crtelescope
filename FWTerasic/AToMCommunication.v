module AToMCommunication(
chip_addr,
cmd_code,
data_0,
data_1,
data_2,
data_3,
data_4,
data_5,
data_6,
data_7,
eve_cou,
clk,
atom_data,
reset,
data_out,
cmd,
CalTrig,
enstrobe,
reset_code);

//list of definitions
`define CMD_SIZE 271 //the sequence start5s always with 3 0s!

//list of inputs
input [4:0] chip_addr;
input [4:0] cmd_code;
input [31:0] data_0;
input [31:0] data_1;
input [31:0] data_2;
input [31:0] data_3;
input [31:0] data_4;
input [31:0] data_5;
input [31:0] data_6;
input [31:0] data_7;
input [4:0] eve_cou;
input clk;
input atom_data;
input reset;
//list of outputs
output [31:0] data_out;
output cmd;
output CalTrig;
output enstrobe;
output reset_code;

wire reset_code;
assign reset_code = end_cnt & valid_code;

reg cmd;
reg [8:0] cnt;
wire end_cnt;
assign end_cnt = (cnt == `CMD_SIZE);
reg valid_code;
reg enstrobe;
reg CalTrig;

// serial command stream
reg [`CMD_SIZE-1:0] cmd_reg;  

//format
always @(cmd_code or reset) begin
	if(reset) begin
		cmd_reg = `CMD_SIZE'b0;
		valid_code = 0;
		CalTrig = 0;
		enstrobe = 0;
	end
	else
		case(cmd_code)
		1: //Clear Readout 
		begin 
			cmd_reg = {261'b0, cmd_code, 5'b10000};
			valid_code = 1;
			enstrobe = 0;
		end
		2: //Sync
		begin 
			cmd_reg = {261'b0, cmd_code, 5'b10000};
			valid_code = 1;
			enstrobe = 0;
		end
		3: //L1 Trig Acc 
		begin 
			cmd_reg = {256'b0, eve_cou, cmd_code, 5'b10000};
			valid_code = 1;
			enstrobe = 0;
		end
		4: //Read Event 
		begin 
			cmd_reg = {261'b0, cmd_code, 5'b10000};
			valid_code = 1;
			enstrobe = 1;
		end
		5: //Cal Strobe 
		begin 
			cmd_reg = {261'b0, cmd_code, 5'b10000};
			valid_code = 1;
			CalTrig = 1;
			enstrobe = 0;
		end
		5'b11010: // 0x1a Write control path selection
		begin 
			cmd_reg = {224'b0, data_0, 5'b00000, cmd_code, 5'b10000};
			valid_code = 1;
			enstrobe = 0;
		end
		5'b11011: //Write control register 
		begin 
			cmd_reg = {224'b0, data_0, chip_addr, cmd_code, 5'b10000};
			valid_code = 1;
			enstrobe = 0;
		end
	   5'b11100: //Write cal and channel mask 
		begin 
			cmd_reg = {data_7, data_6, data_5, data_4, data_3, data_2, data_1, data_0, chip_addr, cmd_code, 5'b10000};
			enstrobe = 0;
			valid_code = 1;
		end	
		5'b11101: //Read registers  
		begin 
			cmd_reg = {256'b0, chip_addr, cmd_code, 5'b10000};
			valid_code = 1;
			enstrobe = 1;
		end
		5'b11110: //Master reset  
		begin 
			cmd_reg = {256'b0, chip_addr, cmd_code, 5'b10000};
			valid_code = 1;
			enstrobe = 0;
		end
		default: 
		begin
			cmd_reg = `CMD_SIZE'b0;
			valid_code = 0;
			CalTrig = 0;
			enstrobe = 0;
		end
	endcase
end
	
always @(posedge clk) begin
	if(reset_code | reset)
		cnt = 0;
	else if (valid_code) begin
		cmd = cmd_reg[cnt];
		cnt = cnt+1;
	end
	else cmd = 0;
	
end
endmodule



