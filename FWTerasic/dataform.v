`timescale 1ns/1ns

`define    SIZOUT  32 // output data register size

module dataform(enstrobe,clk,din,reset,strobe,dout,enfifo,dtack,start);
    input  enstrobe;
    input clk;
    input din;
    input reset;
    output strobe;
    output enfifo;
    output [`SIZOUT-1:0] dout;
    output dtack;
	 output start;

    reg [`SIZOUT-1:0] dout;  // output data register
    reg [`SIZOUT-1:0] pdat;  // register for parallel data bus
    wire enfifo;              // enable for data loading to FIFO 
	 reg dtack;               // data acknoledgment
	 reg strobe;              // data strobe, enable start bit search
	 reg bisstrobe;           // data bisstrobe, enable enablefifo together with strobe
	 
//define the start of the sequence as soon as a bit=1 is found in the data
//stream after setting up the strobe
//start is used to reset the counter and must last one clk tick
    reg [1:0] str_reg;
    wire      start; // read-out sequence start bit

    always @(posedge din or posedge reset) begin
      if(reset) begin
	     str_reg[0] = 0;
      end
		else  begin
	     str_reg[0] = strobe;
		 end
     end
		  
    always @(posedge clk) begin
      str_reg[1] <= str_reg[0];
    end
	 
    assign start = str_reg[0] & ~str_reg[1];

// create a divide-by-n clk for output data bus registration
// as the start bit is set up, start a counter to n to generate enfifo

    reg [4:0] cnt;
    reg [4:0] cntcheck;

//    always @(posedge clk or posedge start) begin
//			if(start) begin
//				cnt=0;
//				enfifoprompt=0;
//   			enfifo=0;
//			end
//			else begin
//				if (bisstrobe==0) begin cnt = 0; end
//			else begin cnt = cnt+1; 
//			     enfifoprompt = (cnt==`SIZOUT-1) & strobe & bisstrobe;
//			     enfifo <= enfifoprompt;
//			     end
//			end
//		end

    always @(posedge clk or posedge start) begin
			if(start) begin
				cnt = 0;
				cntcheck = 1;
			 end
     	   else if(bisstrobe) begin
				cnt = cnt+1;
				cntcheck = cntcheck + 1;
			end
		end

	assign enfifo = (cnt==`SIZOUT-1) & strobe & bisstrobe & (cntcheck == 0);

		
		
//	couenfifo coufifo (.clk(clk),.start(start),.dtack(dtack),.bisstrobe(bisstrobe),.enfifo(enfifo));



	
	
		
// shift register for serial in --> parallel out (with last bit assigned 
// to MSB)
    always @(posedge clk or posedge reset) begin
      if (reset) begin
	      pdat = `SIZOUT'h0;
	   end
      else begin
	      pdat = {din, pdat[`SIZOUT-1:1]};
		end	
	end	

// register dout every n clk ticks
    always @(posedge enfifo or posedge reset) begin
      if (reset) begin
	      dout = `SIZOUT'hffffffff;
		end
      else begin
	      dout = pdat;
		end	
 	end
// AToM data acknowledge occurs when 16 trailing zeros are appended to the
// data stream
    always @(posedge clk or posedge reset) begin
      if(reset) begin
        dtack = 0;
		 end
      else begin
   	  dtack = (dout[`SIZOUT-1:0] == `SIZOUT'h00000000) & strobe;
		 end
    end
// set the strobe up if the issued command is to access the control register
// strobe is turned off as AToM acknowledges data transmission via enstrobe
    wire set_strobe;
	 assign set_strobe = enstrobe;
    always @(posedge set_strobe or posedge reset or posedge dtack) begin
      if(reset | dtack) begin
        strobe = 0;
		 end
      else begin
        strobe = 1;
		 end
	  end	  
// set the bisstrobe up if the start is issued
// strobe is turned off as AToM acknowledges data transmission 
		 
    always @(posedge start or posedge reset or posedge dtack) begin
      if(reset | dtack) begin
        bisstrobe = 0;
		 end
      else if(set_strobe) begin
        bisstrobe = 1;
		 end
		 else begin
		   bisstrobe = 0;
		 end
     end
endmodule
