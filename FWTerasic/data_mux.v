//
module  data_mux(
data_a      , // Mux first input
data_b      , // Mux Second input
sel         , // Select input
atom_data   , // Mux output
clk           // CLK
);
//-----------Input Ports---------------
input data_a, data_b, sel, clk ;
//-----------Output Ports---------------
output atom_data;
//------------Internal Variables--------
reg  atom_data;
//-------------Code Start-----------------
always@ (posedge clk) begin
	if(sel) atom_data = data_a;
	else atom_data = data_b;
	end
endmodule //End Of Module mux
