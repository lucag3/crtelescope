 
#include "basic_io.h"
//#include "test.h"
#include "LCD.h"
#include "DM9000A.C"
#include "SEG7.h"
#include "system.h"
#define PAYLOAD_LEN 1024
#define HEADER_LEN 42

unsigned int aaa,rx_len,i,packet_num;
unsigned char RXT[2000];


void ethernet_interrupts()
{

    printf("\r\n Receving Packet");
	iow(IMR, PAR_set);
    aaa=ReceivePacket (RXT,&rx_len);
    //printf(" done");
    if(!aaa)
    {
    	packet_num++;
      printf("\n\nReceive Packet Length = %d",rx_len);
      for(i=0;i<rx_len;i++)
      {
        if(i%8==0)
        printf("\n");
        printf("0x%2X,",RXT[i]);
      }

    }
    printf("\n");
    SEG7_Hex(packet_num, 0);
    iow(IMR, INTR_set);
    //outport(SEG7_DISPLAY_BASE,packet_num);
}


int main(void)
{
  unsigned char HEAD[] = {0xff,0xff,0xff,0xff,0xff,0xff,//header
                          0x01,0x60,0x6E,0x11,0x02,0x0F,
                          0x08,0x00,0x45,0x00,0x04,0x1c,
                          0xaa,0xbb,0x00,0x00,0xff,
                          0x11,0xf9,0x12,0x0a,0x00,
                          0x00,0x02,0x0a,0x00,0x00,0x01,
                          0x80,0x01,0x04,0x08,0x04,0x08,0x00,0x00};
  unsigned char TXT[HEADER_LEN+PAYLOAD_LEN];
  alt_u32 tx_pck_num=0,i;

  for(i=0;i<HEADER_LEN;i++)
	  TXT[i]=HEAD[i];
  for(i=HEADER_LEN;i<PAYLOAD_LEN+HEADER_LEN;i++)
	  TXT[i]=i;
  printf("\r\n LCD test ");
  LCD_Test();

  printf("done!");
  printf("\r\n DM9000A init");
  DM9000_init();
  printf(" done!(iorq=%d)", DM9000A_IRQ);
  alt_irq_register( DM9000A_IRQ, NULL, (void*)ethernet_interrupts );
  packet_num=0;
  printf("\n");
  while (1)
  {
    msleep(1000);
	printf("\n packets sent=%d received=%d",tx_pck_num++,packet_num);
   //TransmitPacket(TXT,PAYLOAD_LEN+HEADER_LEN);
  }

  printf("\r\nGood Bye");
  return 0;
}


//-------------------------------------------------------------------------


