/*
 * de2-70_NIC_utilities.h
 *
 *  Created on: Aug 31, 2012
 *      Author: massimo
 */
#include "basic_io.h"
#include "system.h"
#include "altera_avalon_pio_regs.h"
#include "includes.h"


#define PAYLOAD_LEN 1024
#define HEADER_LEN 42
#define MAX_PAYLOAD_LEN 1024
#define PACKET_TAIL_BYTES 4
#define UDP_PAYLOAD_OFFSET 42


#define MAC_LEN 6
#define IP_LEN 4

#define ETHERNET_DEST_OFFSET 0
#define ETHERNET_DEST_END 5
#define ETHERNET_DEST_LENGTH MAC_LEN
#define ETHERNET_SOURCE_OFFSET 6
#define ETHERNET_SOURCE_END 11
#define ETHERNET_SOURCE_LENGTH MAC_LEN
#define ETHERNET_TYPE_OFFSET 12
#define ETHERNET_TYPE_LENGTH 2
#define ETHERNET_TYPE_END 13

#define IP_HEADER_OFFSET 14
#define IP_HEADER_END 33
#define IP_HEADER_LENGTH 20
#define IP_VERSION_LENGTH_OFFSET 14
#define IP_VERSION_LENGTH_END 14
#define IP_VERSION_LENGTH_LENGTH 1
#define IP_DSF_OFFSET 15
#define IP_DSF_END 15
#define IP_DSF_LENGTH 1
#define IP_TOTLENNGTH_OFFSET 16
#define IP_TOTLENNGTH_END 17
#define IP_TOTLENNGTH_LENGTH 2
#define IP_ID_OFFSET 18
#define IP_ID_END 19
#define IP_ID_LENGTH 1
#define IP_FLAGS_FRAGOFF_OFFSET 20
#define IP_FLAGS_FRAGOFF_END 21
#define IP_FLAGS_FRAGOFF_LENGTH 2
#define IP_TTL_OFFSET 22
#define IP_TTL_END 22
#define IP_TTL_LENGTH 1
#define IP_UPPER_PROTOCOL_OFFSET 23
#define IP_UPPER_PROTOCOL_END 23
#define IP_UPPER_PROTOCOL_LENGTH 1
#define IP_HEAD_CHECKSUM_OFFSET 24
#define IP_HEAD_CHECKSUM_END 25
#define IP_HEAD_CHECKSUM_LENGTH 2
#define IP_SOURCE_IP_OFFSET 26
#define IP_SOURCE_IP_END 29
#define IP_SOURCE_IP_LENGTH IP_LEN
#define IP_DEST_IP_OFFSET 30
#define IP_DEST_IP_END 33
#define IP_DEST_IP_LENGTH IP_LEN








#define UDP_HEADER_OFFSET 34
#define UDP_HEADER_END 41
#define UDP_HEADER_LENGTH 8
#define UDP_SOURCE_PORT_OFFSET 34
#define UDP_SOURCE_PORT_END 35
#define UDP_SOURCE_PORT_LEGTH 2
#define UDP_DEST_PORT_OFFSET 36
#define UDP_DEST_PORT_END 37
#define UDP_DEST_PORT_LEGTH 2
#define UDP_LENGTH_OFFSET 38
#define UDP_LENGTH_END 39
#define UDP_LEGTH_LEGTH 2
#define UDP_CHECKSUM_PORT_OFFSET 40
#define UDP_CHECKSUM_PORT_END 41
#define UDP_CHECKSUM_PORT_LEGTH 2

#define UDP 0x11


#define ARP_OPERATION_MSB 20
#define ARP_OPERATION_LSB 21
#define ARP_REQUEST 0x0001
#define ARP_REPLY 0x0002
#define ARP_HARDWARE_TYPE_OFFSET 14
#define ARP_HARDWARE_TYPE_END 15
#define ARP_HARDWARE_TYPE_LENGTH 2
#define ARP_PROTOCOL_TYPE_OFFSET 16
#define ARP_PROTOCOL_TYPE_END 17
#define ARP_PROTOCOL_TYPE_LENGTH 2
#define ARP_HARDWARE_SIZE_OFFSET 18
#define ARP_HARDWARE_SIZE_END 18
#define ARP_HARDWARE_SIZE_LENGTH 1
#define ARP_PROTOCOL_SIZE_OFFSET 19
#define ARP_PROTOCOL_SIZE_END 19
#define ARP_PROTOCOL_SIZE_LENGTH 1
#define ARP_SENDER_MAC_OFFSET 22
#define ARP_SENDER_MAC_END 27
#define ARP_SENDER_MAC_LENGTH 6
#define ARP_SENDER_IP_OFFSET 28
#define ARP_SENDER_IP_END 31
#define ARP_SENDER_IP_LENGTH 4
#define ARP_TARGET_MAC_OFFSET 32
#define ARP_TARGET_MAC_END 37
#define ARP_TARGET_MAC_LENGTH 6
#define ARP_TARGET_IP_OFFSET 38
#define ARP_TARGET_IP_END 41
#define ARP_TARGET_IP_LENGTH 4



//board specific definitions
#define PROGRAM_NET_PARAM_SW 1
#define SEND_DEBUG_PACKETS_SW 2
#define SEND_ARP_PACKETS_SW 4
#define MESSAGE_PORT 8888

void get_mac(unsigned char* packet,unsigned int field_start,unsigned int field_end, unsigned char* mac);
void get_ip(unsigned char* packet, unsigned int field_start,unsigned int field_end, unsigned char* ip);
int compare_macs(unsigned char* mac_a,unsigned char* mac_b);
int compare_ips(unsigned char* ip_a,unsigned char* ip_b);
void ip_checksum_calculator(unsigned char* complete_header);
unsigned int extract_data_from_header(unsigned char* packet_header,
									  unsigned int field_start,
									  unsigned int field_end);
void update_packet_templates(unsigned char* arp_request_packet,unsigned char* arp_reply_packet,unsigned char* udp_std_header,
							 unsigned char* dev_board_mac,unsigned char* dev_board_ip,
							 unsigned char* host_pc_mac, unsigned char* host_pc_ip,
							 unsigned short source_port,unsigned short dest_port,
							 unsigned short udp_payload_len);
void get_mac_address_from_keyboard(unsigned char* dev_board_mac);
void get_ip_address_from_keyboard(char* head_string,unsigned char* ip_add);
void get_uint_from_keyboard(char* head_string,unsigned int *data);
void print_packet(const unsigned char * packet, unsigned int rx_len_copy);
int sendto_pchost(OS_EVENT* mutex,unsigned char *header, unsigned char* host_pc_ip, unsigned short dest_port
					,unsigned char* data_ptr, unsigned long data_len);
