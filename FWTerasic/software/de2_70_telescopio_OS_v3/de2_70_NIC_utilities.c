/*
 * de2-70_NIC_utilities.c
 *
 *  Created on: Aug 31, 2012
 *      Author: massimo
 */

#include  "de2_70_NIC_utilities.h"
#include "sys/alt_flash.h"
#include "sys/alt_flash_dev.h"
#include <altera_avalon_epcs_flash_controller.h>
extern struct arp_record arp_table[];
extern unsigned char ARP_REPLY_PACKET[];
extern unsigned char ARP_REQUEST_PACKET[];
extern unsigned char HEAD[];



void ip_checksum_calculator(unsigned char* complete_header){
	unsigned int sum=0,i,j;
	unsigned short checksum;
	unsigned short* us_ptr;
	us_ptr=complete_header;
	for(i=IP_HEADER_OFFSET;i<=IP_HEADER_END;i+=2){
		if(i<IP_HEAD_CHECKSUM_OFFSET || i>IP_HEAD_CHECKSUM_END)
			sum+=(complete_header[i]*256+complete_header[i+1]);
	}
	checksum=sum+(sum>>16);
	checksum=~checksum;
	for(i=IP_HEAD_CHECKSUM_END,j=0;i>=IP_HEAD_CHECKSUM_OFFSET;i--,j++)
		complete_header[i]=(unsigned char)((checksum>>j*8)&0xff);
}
unsigned int extract_data_from_header(unsigned char* packet_header,
									  unsigned int field_start,
									  unsigned int field_end){
	unsigned int i,j,temp_uint=0;
	for(i=field_end,j=0;i>=field_start;i--,j++)
		temp_uint+=((unsigned int)packet_header[i]<<(8*j));
	if(j>3)
		printf("\nWarning, requested to fecth data bigger than 4 bytes\n");
	return(temp_uint);
}
void get_mac(unsigned char* packet,unsigned int field_start,unsigned int field_end, unsigned char* mac){
	unsigned int i,j;
		for(i=field_start,j=0;i<=field_end && j<MAC_LEN;i++,j++)
			mac[j]=packet[i];
}
void get_ip(unsigned char* packet, unsigned int field_start,unsigned int field_end, unsigned char* ip){
	unsigned int i,j;
		for(i=field_start,j=0;i<=field_end && j<IP_LEN;i++,j++)
			ip[j]=packet[i];

}
//compare macs() return 1 if macs are equal
//0 otherwise
int compare_macs(unsigned char* mac_a,unsigned char* mac_b){
	int i,result;
	for(i=0;i<MAC_LEN && mac_a[i]==mac_b[i];i++)
		if(i==MAC_LEN)
			result=EQUAL;
		else
			result=NOTEQUAL;
	return(result);
}
//compare macs() return 1 if macs are equal
//0 otherwise
int compare_ips(unsigned char* ip_a,unsigned char* ip_b){
	int i,result=0,diff=0;
	for(i=0;i<IP_LEN;i++)
		if(ip_a[i]!=ip_b[i]) diff++;
	if(diff)
		result=NOTEQUAL;
	else
		result=EQUAL;
	return(result);
}
void update_packet_templates(unsigned char* arp_request_packet,unsigned char* arp_reply_packet,unsigned char* udp_std_header,
							 unsigned char* dev_board_mac,unsigned char* dev_board_ip,
							 unsigned char* host_pc_mac, unsigned char* host_pc_ip,
							 unsigned short source_port,unsigned short dest_port,
							 unsigned short udp_payload_len){
	int i,j;
	unsigned short temp_us;
//se uno dei puntatori ==NULL il campo corrispondente non viene aggiornato
//per i campi unsigned short il valore 0 significa "non modificato"
//Sezione di aggiornamento dei template ARP
	//Arp Request
	if(arp_request_packet){
		if(dev_board_mac){
			for(i=ETHERNET_SOURCE_END,j=0;i>=ETHERNET_SOURCE_OFFSET;i--,j++)
				arp_request_packet[i]=dev_board_mac[ETHERNET_SOURCE_LENGTH-1-j];
			for(i=ARP_SENDER_MAC_END,j=0;i>=ARP_SENDER_MAC_OFFSET;i--,j++)
				arp_request_packet[i]=dev_board_mac[ARP_SENDER_MAC_LENGTH-1-j];
		}
		if(dev_board_ip){
			for(i=ARP_SENDER_IP_END,j=0;i>=ARP_SENDER_IP_OFFSET;i--,j++)
				arp_request_packet[i]=dev_board_ip[ARP_SENDER_IP_LENGTH-1-j];
				}
		if(host_pc_ip){
				for(i=ARP_TARGET_IP_END,j=0;i>=ARP_TARGET_IP_OFFSET;i--,j++)
					arp_request_packet[i]=host_pc_ip[ARP_TARGET_IP_LENGTH-1-j];
					}
		}
	//Arp Reply
	if(arp_reply_packet){
			if(dev_board_mac){
				for(i=ETHERNET_SOURCE_END,j=0;i>=ETHERNET_SOURCE_OFFSET;i--,j++)
					arp_reply_packet[i]=dev_board_mac[ETHERNET_SOURCE_LENGTH-1-j];
				for(i=ARP_SENDER_MAC_END,j=0;i>=ARP_SENDER_MAC_OFFSET;i--,j++)
					arp_reply_packet[i]=dev_board_mac[ARP_SENDER_MAC_LENGTH-1-j];
			}
			if(dev_board_ip){
				for(i=ARP_SENDER_IP_END,j=0;i>=ARP_SENDER_IP_OFFSET;i--,j++)
					arp_reply_packet[i]=dev_board_ip[ARP_SENDER_IP_LENGTH-1-j];
					}
			if(host_pc_mac){
				for(i=ETHERNET_DEST_END,j=0;i>=ETHERNET_DEST_OFFSET;i--,j++)
					arp_reply_packet[i]=host_pc_mac[ETHERNET_DEST_LENGTH-1-j];
				for(i=ARP_TARGET_MAC_END,j=0;i>=ARP_TARGET_MAC_OFFSET;i--,j++)
					arp_reply_packet[i]=host_pc_mac[ARP_SENDER_MAC_LENGTH-1-j];
				}
			if(host_pc_ip){
				for(i=ARP_TARGET_IP_END,j=0;i>=ARP_TARGET_IP_OFFSET;i--,j++)
					arp_reply_packet[i]=host_pc_ip[ARP_TARGET_IP_LENGTH-1-j];
					}
		}
	//IP-UDP HEADER
	if(udp_std_header){
		if(dev_board_mac){
			for(i=ETHERNET_SOURCE_END,j=0;i>=ETHERNET_SOURCE_OFFSET;i--,j++)
				udp_std_header[i]=dev_board_mac[ETHERNET_SOURCE_LENGTH-1-j];
			}
		if(dev_board_ip){
			for(i=IP_SOURCE_IP_END,j=0;i>=IP_SOURCE_IP_OFFSET;i--,j++)
				udp_std_header[i]=dev_board_ip[IP_SOURCE_IP_LENGTH-1-j];
			}
		if(host_pc_mac){
			for(i=ETHERNET_DEST_END,j=0;i>=ETHERNET_DEST_OFFSET;i--,j++)
				udp_std_header[i]=host_pc_mac[ETHERNET_DEST_LENGTH-1-j];
			}
		if(host_pc_ip){
			for(i=IP_DEST_IP_END,j=0;i>=IP_DEST_IP_OFFSET;i--,j++)
				udp_std_header[i]=host_pc_ip[IP_DEST_IP_LENGTH-1-j];
				}
		if(source_port){
				for(i=UDP_SOURCE_PORT_END,j=0;i>=UDP_SOURCE_PORT_OFFSET;i--,j++)
					udp_std_header[i]=(unsigned char)((source_port>>j*8)&0xff);
				}
		if(dest_port){
				for(i=UDP_DEST_PORT_END,j=0;i>=UDP_DEST_PORT_OFFSET;i--,j++)
					udp_std_header[i]=(unsigned char)((dest_port>>j*8)&0xff);
				}
		if(udp_payload_len){
			temp_us=udp_payload_len+UDP_HEADER_LENGTH+IP_HEADER_LENGTH;
			for(i=IP_TOTLENNGTH_END,j=0;i>=IP_TOTLENNGTH_OFFSET;i--,j++)
							udp_std_header[i]=(unsigned char)((temp_us>>j*8)&0xff);
			temp_us=udp_payload_len+UDP_HEADER_LENGTH;
			for(i=UDP_LENGTH_END,j=0;i>=UDP_LENGTH_OFFSET;i--,j++)
							udp_std_header[i]=(unsigned char)((temp_us>>j*8)&0xff);
			}
		ip_checksum_calculator(udp_std_header);

	}
}
#define EPCS_END_OFFSET 0x1FFFF0
#define MAC_OFFSET_IN_EPCS (EPCS_END_OFFSET-IP_LEN-IP_LEN-MAC_LEN)
#define DEVB_IP_OFFSET_IN_EPCS (EPCS_END_OFFSET-IP_LEN)
#define HOST_IP_OFFSET_IN_EPCS (EPCS_END_OFFSET-IP_LEN-IP_LEN)

int store_addresses_to_epcs(const char *devname,const char* mac, const char* board_ip, const char* host_ip){
	alt_flash_fd* flash_handle;
	char memory_content[IP_LEN+IP_LEN+MAC_LEN];
	int i;
	int error=-1;
	for(i=0;i<MAC_LEN;i++)
		memory_content[i]=mac[i];
	for(i=0;i<IP_LEN;i++)
			memory_content[i+MAC_LEN]=host_ip[i];
	for(i=0;i<IP_LEN;i++)
				memory_content[i+MAC_LEN+IP_LEN]=board_ip[i];

	flash_handle = alt_flash_open_dev(devname);
	      if (flash_handle)
	      {
	      alt_epcs_flash_write(flash_handle, MAC_OFFSET_IN_EPCS,memory_content, IP_LEN+IP_LEN+MAC_LEN);
	      error = 0;
	      alt_flash_close_dev(flash_handle);
	      }
	 return(error);
}

int store_mac_to_epcs(const char *devname,const char* mac){
	alt_flash_fd* flash_handle;
	int error=-1;
	flash_handle = alt_flash_open_dev(devname);
	      if (flash_handle)
	      {
	      alt_epcs_flash_write(flash_handle, MAC_OFFSET_IN_EPCS,mac, MAC_LEN);
	      error = 0;
	      alt_flash_close_dev(flash_handle);
	      }
	 return(error);
}
int retrieve_mac_from_epcs(const char *devname,char* mac){
	alt_flash_fd* flash_handle;
	int error=-1;
	flash_handle = alt_flash_open_dev(devname);
	      if (flash_handle)
	      {
	      alt_epcs_flash_read(flash_handle, MAC_OFFSET_IN_EPCS,mac, MAC_LEN);
	      error = 0;
	      alt_flash_close_dev(flash_handle);
	      }
	 return(error);
}

int store_host_ip_to_epcs(const char *devname,const char* ip){
	alt_flash_fd* flash_handle;
	int error=-1;
	flash_handle = alt_flash_open_dev(devname);
	      if (flash_handle)
	      {
	      alt_epcs_flash_write(flash_handle, HOST_IP_OFFSET_IN_EPCS,ip, IP_LEN);
	      error = 0;
	      alt_flash_close_dev(flash_handle);
	      }
	 return(error);
}
int retrieve_host_ip_from_epcs(const char *devname,char* ip){
	alt_flash_fd* flash_handle;
	int error=-1;
	flash_handle = alt_flash_open_dev(devname);
	      if (flash_handle)
	      {
	      alt_epcs_flash_read(flash_handle, HOST_IP_OFFSET_IN_EPCS,ip,IP_LEN);
	      error = 0;
	      alt_flash_close_dev(flash_handle);
	      }
	 return(error);
}

int store_devb_ip_to_epcs(const char *devname,const char* ip){
	alt_flash_fd* flash_handle;
	int error=-1;
	flash_handle = alt_flash_open_dev(devname);
	      if (flash_handle)
	      {
	      alt_epcs_flash_write(flash_handle, DEVB_IP_OFFSET_IN_EPCS,ip, IP_LEN);
	      error = 0;
	      alt_flash_close_dev(flash_handle);
	      }
	 return(error);
}
int retrieve_devb_ip_from_epcs(const char *devname,char* ip){
	alt_flash_fd* flash_handle;
	int error=-1;
	flash_handle = alt_flash_open_dev(devname);
	      if (flash_handle)
	      {
	      alt_epcs_flash_read(flash_handle, DEVB_IP_OFFSET_IN_EPCS,ip, IP_LEN);
	      error = 0;
	      alt_flash_close_dev(flash_handle);
	      }
	 return(error);
}


#define UPDATE_PERIOD_DEFAULT 400
#define UPDATE_PERIOD_DELTA 50
#define BLINKING_PERIOD 400
#define DEBOUNCE_DELAY 400

void get_mac_address_from_keyboard(unsigned char* dev_board_mac){
	FILE* lcd;
	unsigned char enter_button_edge,up_button_edge,down_button_edge;
	unsigned char up_button,down_button,delta;
	int i,update_period;
	enter_button_edge=0;
	while(!enter_button_edge){

	lcd=fopen("/dev/lcd","w");
	fprintf(lcd,"Dev.Board MAC:\n");
	fprintf(lcd,"  :%02X:%02X:%02X:%02X%02X\n",dev_board_mac[1],dev_board_mac[2],
												   dev_board_mac[3],dev_board_mac[4],dev_board_mac[5]);
	fclose(lcd);
	msleep(BLINKING_PERIOD);
	lcd=fopen("/dev/lcd","w");
	fprintf(lcd,"Dev.Board MAC:\n");
	fprintf(lcd,"%02X:%02X:%02X:%02X:%02X%02X\n",dev_board_mac[0],dev_board_mac[1],dev_board_mac[2],
												   dev_board_mac[3],dev_board_mac[4],dev_board_mac[5]);
	fclose(lcd);
	msleep(BLINKING_PERIOD);
	enter_button_edge=IORD_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE)&0x2;
	up_button_edge=IORD_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE)&0x8;
	down_button_edge=IORD_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE)&0x4;
	if(up_button_edge){
		delta=1;
		i=0;
		dev_board_mac[0]+=delta;
		msleep(DEBOUNCE_DELAY);
		update_period=UPDATE_PERIOD_DEFAULT;
		while(!(up_button=(IORD_ALTERA_AVALON_PIO_DATA(PIO_BUTTON_BASE)&0x8))){
			dev_board_mac[0]+=delta;
			msleep(update_period);
			lcd=fopen("/dev/lcd","w");
			fprintf(lcd,"Dev.Board MAC:\n");
			fprintf(lcd,"%02X:%02X:%02X:%02X:%02X%02X\n",dev_board_mac[0],dev_board_mac[1],dev_board_mac[2],
														   dev_board_mac[3],dev_board_mac[4],dev_board_mac[5]);
			fclose(lcd);
			i++;
			if(i>3 && update_period>UPDATE_PERIOD_DELTA){
				update_period-=UPDATE_PERIOD_DELTA;
				i=0;}

		}
		msleep(DEBOUNCE_DELAY);
		IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE, 0x8);
		}
	if(down_button_edge){
			delta=1;
			i=0;
			dev_board_mac[0]-=delta;
			msleep(DEBOUNCE_DELAY);
			update_period=UPDATE_PERIOD_DEFAULT;
			while(!(up_button=(IORD_ALTERA_AVALON_PIO_DATA(PIO_BUTTON_BASE)&0x4))){
				dev_board_mac[0]-=delta;
				msleep(update_period);
				lcd=fopen("/dev/lcd","w");
				fprintf(lcd,"Dev.Board MAC:\n");
				fprintf(lcd,"%02X:%02X:%02X:%02X:%02X%02X\n",dev_board_mac[0],dev_board_mac[1],dev_board_mac[2],
															   dev_board_mac[3],dev_board_mac[4],dev_board_mac[5]);
				fclose(lcd);
				i++;
				if(i>3 && update_period>UPDATE_PERIOD_DELTA){
					update_period-=UPDATE_PERIOD_DELTA;
					i=0;}

			}
			msleep(DEBOUNCE_DELAY);
			IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE, 0x4);
			}
	}
	IORD_ALTERA_AVALON_PIO_DATA(PIO_BUTTON_BASE);
	/* Write to the edge capture register to reset it. */
	IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE, 0xf);
	enter_button_edge=0;
		while(!enter_button_edge){

		lcd=fopen("/dev/lcd","w");
		fprintf(lcd,"Dev.Board MAC:\n");
		fprintf(lcd,"%02X:  :%02X:%02X:%02X%02X\n",dev_board_mac[0],dev_board_mac[2],
													   dev_board_mac[3],dev_board_mac[4],dev_board_mac[5]);
		fclose(lcd);
		msleep(BLINKING_PERIOD);
		lcd=fopen("/dev/lcd","w");
		fprintf(lcd,"Dev.Board MAC:\n");
		fprintf(lcd,"%02X:%02X:%02X:%02X:%02X%02X\n",dev_board_mac[0],dev_board_mac[1],dev_board_mac[2],
													   dev_board_mac[3],dev_board_mac[4],dev_board_mac[5]);
		fclose(lcd);
		msleep(BLINKING_PERIOD);
		enter_button_edge=IORD_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE)&0x2;
		up_button_edge=IORD_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE)&0x8;
		down_button_edge=IORD_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE)&0x4;
		if(up_button_edge){
			delta=1;
			i=0;
			dev_board_mac[1]+=delta;
			msleep(DEBOUNCE_DELAY);
			update_period=UPDATE_PERIOD_DEFAULT;
			while(!(up_button=(IORD_ALTERA_AVALON_PIO_DATA(PIO_BUTTON_BASE)&0x8))){
				dev_board_mac[1]+=delta;
				msleep(update_period);
				lcd=fopen("/dev/lcd","w");
				fprintf(lcd,"Dev.Board MAC:\n");
				fprintf(lcd,"%02X:%02X:%02X:%02X:%02X%02X\n",dev_board_mac[0],dev_board_mac[1],dev_board_mac[2],
															   dev_board_mac[3],dev_board_mac[4],dev_board_mac[5]);
				fclose(lcd);
				i++;
				if(i>3 && update_period>UPDATE_PERIOD_DELTA){
					update_period-=UPDATE_PERIOD_DELTA;
					i=0;}

			}
			msleep(DEBOUNCE_DELAY);
			IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE, 0x8);
			}
		if(down_button_edge){
				delta=1;
				i=0;
				dev_board_mac[1]-=delta;
				msleep(DEBOUNCE_DELAY);
				update_period=UPDATE_PERIOD_DEFAULT;
				while(!(up_button=(IORD_ALTERA_AVALON_PIO_DATA(PIO_BUTTON_BASE)&0x4))){
					dev_board_mac[1]-=delta;
					msleep(update_period);
					lcd=fopen("/dev/lcd","w");
					fprintf(lcd,"Dev.Board MAC:\n");
					fprintf(lcd,"%02X:%02X:%02X:%02X:%02X%02X\n",dev_board_mac[0],dev_board_mac[1],dev_board_mac[2],
																   dev_board_mac[3],dev_board_mac[4],dev_board_mac[5]);
					fclose(lcd);
					i++;
					if(i>3 && update_period>UPDATE_PERIOD_DELTA){
						update_period-=UPDATE_PERIOD_DELTA;
						i=0;}

				}
				msleep(DEBOUNCE_DELAY);
				IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE, 0x4);
				}
		}
		IORD_ALTERA_AVALON_PIO_DATA(PIO_BUTTON_BASE);
		/* Write to the edge capture register to reset it. */
		IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE, 0xf);
		enter_button_edge=0;
				while(!enter_button_edge){

				lcd=fopen("/dev/lcd","w");
				fprintf(lcd,"Dev.Board MAC:\n");
				fprintf(lcd,"%02X:%02X:  :%02X:%02X%02X\n",dev_board_mac[0],dev_board_mac[1],
															   dev_board_mac[3],dev_board_mac[4],dev_board_mac[5]);
				fclose(lcd);
				msleep(BLINKING_PERIOD);
				lcd=fopen("/dev/lcd","w");
				fprintf(lcd,"Dev.Board MAC:\n");
				fprintf(lcd,"%02X:%02X:%02X:%02X:%02X%02X\n",dev_board_mac[0],dev_board_mac[1],dev_board_mac[2],
															   dev_board_mac[3],dev_board_mac[4],dev_board_mac[5]);
				fclose(lcd);
				msleep(BLINKING_PERIOD);
				enter_button_edge=IORD_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE)&0x2;
				up_button_edge=IORD_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE)&0x8;
				down_button_edge=IORD_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE)&0x4;
				if(up_button_edge){
					delta=1;
					i=0;
					dev_board_mac[2]+=delta;
					msleep(DEBOUNCE_DELAY);
					update_period=UPDATE_PERIOD_DEFAULT;
					while(!(up_button=(IORD_ALTERA_AVALON_PIO_DATA(PIO_BUTTON_BASE)&0x8))){
						dev_board_mac[2]+=delta;
						msleep(update_period);
						lcd=fopen("/dev/lcd","w");
						fprintf(lcd,"Dev.Board MAC:\n");
						fprintf(lcd,"%02X:%02X:%02X:%02X:%02X%02X\n",dev_board_mac[0],dev_board_mac[1],dev_board_mac[2],
																	   dev_board_mac[3],dev_board_mac[4],dev_board_mac[5]);
						fclose(lcd);
						i++;
						if(i>3 && update_period>UPDATE_PERIOD_DELTA){
							update_period-=UPDATE_PERIOD_DELTA;
							i=0;}

					}
					msleep(DEBOUNCE_DELAY);
					IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE, 0x8);
					}
				if(down_button_edge){
						delta=1;
						i=0;
						dev_board_mac[2]-=delta;
						msleep(DEBOUNCE_DELAY);
						update_period=UPDATE_PERIOD_DEFAULT;
						while(!(up_button=(IORD_ALTERA_AVALON_PIO_DATA(PIO_BUTTON_BASE)&0x4))){
							dev_board_mac[2]-=delta;
							msleep(update_period);
							lcd=fopen("/dev/lcd","w");
							fprintf(lcd,"Dev.Board MAC:\n");
							fprintf(lcd,"%02X:%02X:%02X:%02X:%02X%02X\n",dev_board_mac[0],dev_board_mac[1],dev_board_mac[2],
																		   dev_board_mac[3],dev_board_mac[4],dev_board_mac[5]);
							fclose(lcd);
							i++;
							if(i>3 && update_period>UPDATE_PERIOD_DELTA){
								update_period-=UPDATE_PERIOD_DELTA;
								i=0;}

						}
						msleep(DEBOUNCE_DELAY);
						IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE, 0x4);
						}
				}
				IORD_ALTERA_AVALON_PIO_DATA(PIO_BUTTON_BASE);
				/* Write to the edge capture register to reset it. */
				IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE, 0xf);
	enter_button_edge=0;
			while(!enter_button_edge){

			lcd=fopen("/dev/lcd","w");
			fprintf(lcd,"Dev.Board MAC:\n");
			fprintf(lcd,"%02X:%02X:%02X:  :%02X%02X\n",dev_board_mac[0],dev_board_mac[1],
														   dev_board_mac[2],dev_board_mac[4],dev_board_mac[5]);
			fclose(lcd);
			msleep(BLINKING_PERIOD);
			lcd=fopen("/dev/lcd","w");
			fprintf(lcd,"Dev.Board MAC:\n");
			fprintf(lcd,"%02X:%02X:%02X:%02X:%02X%02X\n",dev_board_mac[0],dev_board_mac[1],dev_board_mac[2],
														   dev_board_mac[3],dev_board_mac[4],dev_board_mac[5]);
			fclose(lcd);
			msleep(BLINKING_PERIOD);
			enter_button_edge=IORD_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE)&0x2;
			up_button_edge=IORD_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE)&0x8;
			down_button_edge=IORD_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE)&0x4;
			if(up_button_edge){
				delta=1;
				i=0;
				dev_board_mac[3]+=delta;
				msleep(DEBOUNCE_DELAY);
				update_period=UPDATE_PERIOD_DEFAULT;
				while(!(up_button=(IORD_ALTERA_AVALON_PIO_DATA(PIO_BUTTON_BASE)&0x8))){
					dev_board_mac[3]+=delta;
					msleep(update_period);
					lcd=fopen("/dev/lcd","w");
					fprintf(lcd,"Dev.Board MAC:\n");
					fprintf(lcd,"%02X:%02X:%02X:%02X:%02X%02X\n",dev_board_mac[0],dev_board_mac[1],dev_board_mac[2],
																   dev_board_mac[3],dev_board_mac[4],dev_board_mac[5]);
					fclose(lcd);
					i++;
					if(i>3 && update_period>UPDATE_PERIOD_DELTA){
						update_period-=UPDATE_PERIOD_DELTA;
						i=0;}

				}
				msleep(DEBOUNCE_DELAY);
				IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE, 0x8);
				}
			if(down_button_edge){
					delta=1;
					i=0;
					dev_board_mac[3]-=delta;
					msleep(DEBOUNCE_DELAY);
					update_period=UPDATE_PERIOD_DEFAULT;
					while(!(up_button=(IORD_ALTERA_AVALON_PIO_DATA(PIO_BUTTON_BASE)&0x4))){
						dev_board_mac[3]-=delta;
						msleep(update_period);
						lcd=fopen("/dev/lcd","w");
						fprintf(lcd,"Dev.Board MAC:\n");
						fprintf(lcd,"%02X:%02X:%02X:%02X:%02X%02X\n",dev_board_mac[0],dev_board_mac[1],dev_board_mac[2],
																	   dev_board_mac[3],dev_board_mac[4],dev_board_mac[5]);
						fclose(lcd);
						i++;
						if(i>3 && update_period>UPDATE_PERIOD_DELTA){
							update_period-=UPDATE_PERIOD_DELTA;
							i=0;}

					}
					msleep(DEBOUNCE_DELAY);
					IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE, 0x4);
					}
			}
			IORD_ALTERA_AVALON_PIO_DATA(PIO_BUTTON_BASE);
			/* Write to the edge capture register to reset it. */
			IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE, 0xf);
			enter_button_edge=0;
					while(!enter_button_edge){

					lcd=fopen("/dev/lcd","w");
					fprintf(lcd,"Dev.Board MAC:\n");
					fprintf(lcd,"%02X:%02X:%02X:%02X:  %02X\n",dev_board_mac[0],dev_board_mac[1],
																   dev_board_mac[2],dev_board_mac[3],dev_board_mac[5]);
					fclose(lcd);
					msleep(BLINKING_PERIOD);
					lcd=fopen("/dev/lcd","w");
					fprintf(lcd,"Dev.Board MAC:\n");
					fprintf(lcd,"%02X:%02X:%02X:%02X:%02X%02X\n",dev_board_mac[0],dev_board_mac[1],dev_board_mac[2],
																   dev_board_mac[3],dev_board_mac[4],dev_board_mac[5]);
					fclose(lcd);
					msleep(BLINKING_PERIOD);
					enter_button_edge=IORD_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE)&0x2;
					up_button_edge=IORD_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE)&0x8;
					down_button_edge=IORD_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE)&0x4;
					if(up_button_edge){
						delta=1;
						i=0;
						dev_board_mac[4]+=delta;
						msleep(DEBOUNCE_DELAY);
						update_period=UPDATE_PERIOD_DEFAULT;
						while(!(up_button=(IORD_ALTERA_AVALON_PIO_DATA(PIO_BUTTON_BASE)&0x8))){
							dev_board_mac[4]+=delta;
							msleep(update_period);
							lcd=fopen("/dev/lcd","w");
							fprintf(lcd,"Dev.Board MAC:\n");
							fprintf(lcd,"%02X:%02X:%02X:%02X:%02X%02X\n",dev_board_mac[0],dev_board_mac[1],dev_board_mac[2],
																		   dev_board_mac[3],dev_board_mac[4],dev_board_mac[5]);
							fclose(lcd);
							i++;
							if(i>3 && update_period>UPDATE_PERIOD_DELTA){
								update_period-=UPDATE_PERIOD_DELTA;
								i=0;}

						}
						msleep(DEBOUNCE_DELAY);
						IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE, 0x8);
						}
					if(down_button_edge){
							delta=1;
							i=0;
							dev_board_mac[4]-=delta;
							msleep(DEBOUNCE_DELAY);
							update_period=UPDATE_PERIOD_DEFAULT;
							while(!(up_button=(IORD_ALTERA_AVALON_PIO_DATA(PIO_BUTTON_BASE)&0x4))){
								dev_board_mac[4]-=delta;
								msleep(update_period);
								lcd=fopen("/dev/lcd","w");
								fprintf(lcd,"Dev.Board MAC:\n");
								fprintf(lcd,"%02X:%02X:%02X:%02X:%02X%02X\n",dev_board_mac[0],dev_board_mac[1],dev_board_mac[2],
																			   dev_board_mac[3],dev_board_mac[4],dev_board_mac[5]);
								fclose(lcd);
								i++;
								if(i>3 && update_period>UPDATE_PERIOD_DELTA){
									update_period-=UPDATE_PERIOD_DELTA;
									i=0;}

							}
							msleep(DEBOUNCE_DELAY);
							IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE, 0x4);
							}
					}
					IORD_ALTERA_AVALON_PIO_DATA(PIO_BUTTON_BASE);
					/* Write to the edge capture register to reset it. */
					IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE, 0xf);
			enter_button_edge=0;
					while(!enter_button_edge){

					lcd=fopen("/dev/lcd","w");
					fprintf(lcd,"Dev.Board MAC:\n");
					fprintf(lcd,"%02X:%02X:%02X:%02X:%02X  \n",dev_board_mac[0],dev_board_mac[1],dev_board_mac[2],
																   dev_board_mac[3],dev_board_mac[4]);
					fclose(lcd);
					msleep(BLINKING_PERIOD);
					lcd=fopen("/dev/lcd","w");
					fprintf(lcd,"Dev.Board MAC:\n");
					fprintf(lcd,"%02X:%02X:%02X:%02X:%02X%02X\n",dev_board_mac[0],dev_board_mac[1],dev_board_mac[2],
																   dev_board_mac[3],dev_board_mac[4],dev_board_mac[5]);
					fclose(lcd);
					msleep(BLINKING_PERIOD);
					enter_button_edge=IORD_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE)&0x2;
					up_button_edge=IORD_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE)&0x8;
					down_button_edge=IORD_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE)&0x4;
					if(up_button_edge){
						delta=1;
						i=0;
						dev_board_mac[5]+=delta;
						msleep(DEBOUNCE_DELAY);
						update_period=UPDATE_PERIOD_DEFAULT;
						while(!(up_button=(IORD_ALTERA_AVALON_PIO_DATA(PIO_BUTTON_BASE)&0x8))){
							dev_board_mac[5]+=delta;
							msleep(update_period);
							lcd=fopen("/dev/lcd","w");
							fprintf(lcd,"Dev.Board MAC:\n");
							fprintf(lcd,"%02X:%02X:%02X:%02X:%02X%02X\n",dev_board_mac[0],dev_board_mac[1],dev_board_mac[2],
																		   dev_board_mac[3],dev_board_mac[4],dev_board_mac[5]);
							fclose(lcd);
							i++;
							if(i>3 && update_period>UPDATE_PERIOD_DELTA){
								update_period-=UPDATE_PERIOD_DELTA;
								i=0;}

						}
						msleep(DEBOUNCE_DELAY);
						IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE, 0x8);
						}
					if(down_button_edge){
							delta=1;
							i=0;
							dev_board_mac[5]-=delta;
							msleep(DEBOUNCE_DELAY);
							update_period=UPDATE_PERIOD_DEFAULT;
							while(!(up_button=(IORD_ALTERA_AVALON_PIO_DATA(PIO_BUTTON_BASE)&0x4))){
								dev_board_mac[5]-=delta;
								msleep(update_period);
								lcd=fopen("/dev/lcd","w");
								fprintf(lcd,"Dev.Board MAC:\n");
								fprintf(lcd,"%02X:%02X:%02X:%02X:%02X%02X\n",dev_board_mac[0],dev_board_mac[1],dev_board_mac[2],
																			   dev_board_mac[3],dev_board_mac[4],dev_board_mac[5]);
								fclose(lcd);
								i++;
								if(i>3 && update_period>UPDATE_PERIOD_DELTA){
									update_period-=UPDATE_PERIOD_DELTA;
									i=0;}

							}
							msleep(DEBOUNCE_DELAY);
							IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE, 0x4);
							}
					}
					IORD_ALTERA_AVALON_PIO_DATA(PIO_BUTTON_BASE);
					/* Write to the edge capture register to reset it. */
					IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE, 0xf);
}


void get_ip_address_from_keyboard(char* head_string,unsigned char* ip_add){
	FILE* lcd;
	unsigned char enter_button_edge,up_button_edge,down_button_edge;
	unsigned char up_button,down_button,delta;
	int i,update_period;
	enter_button_edge=0;
			while(!enter_button_edge){

			lcd=fopen("/dev/lcd","w");
			fprintf(lcd,"%s\n",head_string);
			fprintf(lcd,"   .%3d.%3d.%3d\n",ip_add[1],ip_add[2],ip_add[3]);
			fclose(lcd);
			msleep(BLINKING_PERIOD);
			lcd=fopen("/dev/lcd","w");
			fprintf(lcd,"%s\n",head_string);
			fprintf(lcd,"%3d.%3d.%3d.%3d\n",ip_add[0],ip_add[1],ip_add[2],ip_add[3]);
			fclose(lcd);
			msleep(BLINKING_PERIOD);
			enter_button_edge=IORD_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE)&0x2;
			up_button_edge=IORD_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE)&0x8;
			down_button_edge=IORD_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE)&0x4;
			if(up_button_edge){
				delta=1;
				i=0;
				ip_add[0]+=delta;
				msleep(DEBOUNCE_DELAY);
				update_period=UPDATE_PERIOD_DEFAULT;
				while(!(up_button=(IORD_ALTERA_AVALON_PIO_DATA(PIO_BUTTON_BASE)&0x8))){
					ip_add[0]+=delta;
					msleep(update_period);
					lcd=fopen("/dev/lcd","w");
					fprintf(lcd,"%s\n",head_string);
					fprintf(lcd,"%3d.%3d.%3d.%3d\n",ip_add[0],ip_add[1],ip_add[2],ip_add[3]);
					fclose(lcd);
					i++;
					if(i>3 && update_period>UPDATE_PERIOD_DELTA){
						update_period-=UPDATE_PERIOD_DELTA;
						i=0;}

				}
				msleep(DEBOUNCE_DELAY);
				IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE, 0x8);
				}
			if(down_button_edge){
					delta=1;
					i=0;
					ip_add[0]-=delta;
					msleep(DEBOUNCE_DELAY);
					update_period=UPDATE_PERIOD_DEFAULT;
					while(!(up_button=(IORD_ALTERA_AVALON_PIO_DATA(PIO_BUTTON_BASE)&0x4))){
						ip_add[0]-=delta;
						msleep(update_period);
						lcd=fopen("/dev/lcd","w");
						fprintf(lcd,"%s\n",head_string);
						fprintf(lcd,"%3d.%3d.%3d.%3d\n",ip_add[0],ip_add[1],ip_add[2],ip_add[3]);
						fclose(lcd);
						i++;
						if(i>3 && update_period>UPDATE_PERIOD_DELTA){
							update_period-=UPDATE_PERIOD_DELTA;
							i=0;}

					}
					msleep(DEBOUNCE_DELAY);
					IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE, 0x4);
					}
			}
			IORD_ALTERA_AVALON_PIO_DATA(PIO_BUTTON_BASE);
			/* Write to the edge capture register to reset it. */
			IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE, 0xf);
			enter_button_edge=0;
						while(!enter_button_edge){

						lcd=fopen("/dev/lcd","w");
						fprintf(lcd,"%s\n",head_string);
						fprintf(lcd,"%3d.   .%3d.%3d\n",ip_add[0],ip_add[2],ip_add[3]);
						fclose(lcd);
						msleep(BLINKING_PERIOD);
						lcd=fopen("/dev/lcd","w");
						fprintf(lcd,"%s\n",head_string);
						fprintf(lcd,"%3d.%3d.%3d.%3d\n",ip_add[0],ip_add[1],ip_add[2],ip_add[3]);
						fclose(lcd);
						msleep(BLINKING_PERIOD);
						enter_button_edge=IORD_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE)&0x2;
						up_button_edge=IORD_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE)&0x8;
						down_button_edge=IORD_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE)&0x4;
						if(up_button_edge){
							delta=1;
							i=0;
							ip_add[1]+=delta;
							msleep(DEBOUNCE_DELAY);
							update_period=UPDATE_PERIOD_DEFAULT;
							while(!(up_button=(IORD_ALTERA_AVALON_PIO_DATA(PIO_BUTTON_BASE)&0x8))){
								ip_add[1]+=delta;
								msleep(update_period);
								lcd=fopen("/dev/lcd","w");
								fprintf(lcd,"%s\n",head_string);
								fprintf(lcd,"%3d.%3d.%3d.%3d\n",ip_add[0],ip_add[1],ip_add[2],ip_add[3]);
								fclose(lcd);
								i++;
								if(i>3 && update_period>UPDATE_PERIOD_DELTA){
									update_period-=UPDATE_PERIOD_DELTA;
									i=0;}

							}
							msleep(DEBOUNCE_DELAY);
							IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE, 0x8);
							}
						if(down_button_edge){
								delta=1;
								i=0;
								ip_add[1]-=delta;
								msleep(DEBOUNCE_DELAY);
								update_period=UPDATE_PERIOD_DEFAULT;
								while(!(up_button=(IORD_ALTERA_AVALON_PIO_DATA(PIO_BUTTON_BASE)&0x4))){
									ip_add[1]-=delta;
									msleep(update_period);
									lcd=fopen("/dev/lcd","w");
									fprintf(lcd,"%s\n",head_string);
									fprintf(lcd,"%3d.%3d.%3d.%3d\n",ip_add[0],ip_add[1],ip_add[2],ip_add[3]);
									fclose(lcd);
									i++;
									if(i>3 && update_period>UPDATE_PERIOD_DELTA){
										update_period-=UPDATE_PERIOD_DELTA;
										i=0;}

								}
								msleep(DEBOUNCE_DELAY);
								IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE, 0x4);
								}
						}
						IORD_ALTERA_AVALON_PIO_DATA(PIO_BUTTON_BASE);
						/* Write to the edge capture register to reset it. */
						IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE, 0xf);
						enter_button_edge=0;
									while(!enter_button_edge){

									lcd=fopen("/dev/lcd","w");
									fprintf(lcd,"%s\n",head_string);
									fprintf(lcd,"%3d.%3d.   .%3d\n",ip_add[0],ip_add[1],ip_add[3]);
									fclose(lcd);
									msleep(BLINKING_PERIOD);
									lcd=fopen("/dev/lcd","w");
									fprintf(lcd,"%s\n",head_string);
									fprintf(lcd,"%3d.%3d.%3d.%3d\n",ip_add[0],ip_add[1],ip_add[2],ip_add[3]);
									fclose(lcd);
									msleep(BLINKING_PERIOD);
									enter_button_edge=IORD_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE)&0x2;
									up_button_edge=IORD_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE)&0x8;
									down_button_edge=IORD_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE)&0x4;
									if(up_button_edge){
										delta=1;
										i=0;
										ip_add[2]+=delta;
										msleep(DEBOUNCE_DELAY);
										update_period=UPDATE_PERIOD_DEFAULT;
										while(!(up_button=(IORD_ALTERA_AVALON_PIO_DATA(PIO_BUTTON_BASE)&0x8))){
											ip_add[2]+=delta;
											msleep(update_period);
											lcd=fopen("/dev/lcd","w");
											fprintf(lcd,"%s\n",head_string);
											fprintf(lcd,"%3d.%3d.%3d.%3d\n",ip_add[0],ip_add[1],ip_add[2],ip_add[3]);
											fclose(lcd);
											i++;
											if(i>3 && update_period>UPDATE_PERIOD_DELTA){
												update_period-=UPDATE_PERIOD_DELTA;
												i=0;}

										}
										msleep(DEBOUNCE_DELAY);
										IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE, 0x8);
										}
									if(down_button_edge){
											delta=1;
											i=0;
											ip_add[2]-=delta;
											msleep(DEBOUNCE_DELAY);
											update_period=UPDATE_PERIOD_DEFAULT;
											while(!(up_button=(IORD_ALTERA_AVALON_PIO_DATA(PIO_BUTTON_BASE)&0x4))){
												ip_add[2]-=delta;
												msleep(update_period);
												lcd=fopen("/dev/lcd","w");
												fprintf(lcd,"%s\n",head_string);
												fprintf(lcd,"%3d.%3d.%3d.%3d\n",ip_add[0],ip_add[1],ip_add[2],ip_add[3]);
												fclose(lcd);
												i++;
												if(i>3 && update_period>UPDATE_PERIOD_DELTA){
													update_period-=UPDATE_PERIOD_DELTA;
													i=0;}

											}
											msleep(DEBOUNCE_DELAY);
											IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE, 0x4);
											}
									}
									IORD_ALTERA_AVALON_PIO_DATA(PIO_BUTTON_BASE);
									/* Write to the edge capture register to reset it. */
									IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE, 0xf);
									enter_button_edge=0;
												while(!enter_button_edge){

												lcd=fopen("/dev/lcd","w");
												fprintf(lcd,"%s\n",head_string);
												fprintf(lcd,"%3d.%3d.%3d.   \n",ip_add[0],ip_add[1],ip_add[2]);

												fclose(lcd);
												msleep(BLINKING_PERIOD);
												lcd=fopen("/dev/lcd","w");
												fprintf(lcd,"%s\n",head_string);
												fprintf(lcd,"%3d.%3d.%3d.%3d\n",ip_add[0],ip_add[1],ip_add[2],ip_add[3]);
												fclose(lcd);
												msleep(BLINKING_PERIOD);
												enter_button_edge=IORD_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE)&0x2;
												up_button_edge=IORD_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE)&0x8;
												down_button_edge=IORD_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE)&0x4;
												if(up_button_edge){
													delta=1;
													i=0;
													ip_add[3]+=delta;
													msleep(DEBOUNCE_DELAY);
													update_period=UPDATE_PERIOD_DEFAULT;
													while(!(up_button=(IORD_ALTERA_AVALON_PIO_DATA(PIO_BUTTON_BASE)&0x8))){
														ip_add[3]+=delta;
														msleep(update_period);
														lcd=fopen("/dev/lcd","w");
														fprintf(lcd,"%s\n",head_string);
														fprintf(lcd,"%3d.%3d.%3d.%3d\n",ip_add[0],ip_add[1],ip_add[2],ip_add[3]);
														fclose(lcd);
														i++;
														if(i>3 && update_period>UPDATE_PERIOD_DELTA){
															update_period-=UPDATE_PERIOD_DELTA;
															i=0;}

													}
													msleep(DEBOUNCE_DELAY);
													IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE, 0x8);
													}
												if(down_button_edge){
														delta=1;
														i=0;
														ip_add[3]-=delta;
														msleep(DEBOUNCE_DELAY);
														update_period=UPDATE_PERIOD_DEFAULT;
														while(!(up_button=(IORD_ALTERA_AVALON_PIO_DATA(PIO_BUTTON_BASE)&0x4))){
															ip_add[3]-=delta;
															msleep(update_period);
															lcd=fopen("/dev/lcd","w");
															fprintf(lcd,"%s\n",head_string);
															fprintf(lcd,"%3d.%3d.%3d.%3d\n",ip_add[0],ip_add[1],ip_add[2],ip_add[3]);
															fclose(lcd);
															i++;
															if(i>3 && update_period>UPDATE_PERIOD_DELTA){
																update_period-=UPDATE_PERIOD_DELTA;
																i=0;}

														}
														msleep(DEBOUNCE_DELAY);
														IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE, 0x4);
														}
												}
												IORD_ALTERA_AVALON_PIO_DATA(PIO_BUTTON_BASE);
												/* Write to the edge capture register to reset it. */
												IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE, 0xf);
}
void get_uint_from_keyboard(char* head_string,unsigned int *data){
	FILE* lcd;
	unsigned char enter_button_edge,up_button_edge,down_button_edge;
	unsigned char up_button,down_button,delta;
	int i,update_period;
	enter_button_edge=0;
			while(!enter_button_edge){

			lcd=fopen("/dev/lcd","w");
			fprintf(lcd,"%s\n",head_string);
			fprintf(lcd,"    \n");
			fclose(lcd);
			msleep(BLINKING_PERIOD);
			lcd=fopen("/dev/lcd","w");
			fprintf(lcd,"%s\n",head_string);
			fprintf(lcd,"%4d\n",*data);
			fclose(lcd);
			msleep(BLINKING_PERIOD);
			enter_button_edge=IORD_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE)&0x2;
			up_button_edge=IORD_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE)&0x8;
			down_button_edge=IORD_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE)&0x4;
			if(up_button_edge){
				delta=1;
				i=0;
				(*data)+=delta;
				msleep(DEBOUNCE_DELAY);
				update_period=UPDATE_PERIOD_DEFAULT;
				while(!(up_button=(IORD_ALTERA_AVALON_PIO_DATA(PIO_BUTTON_BASE)&0x8))){
					(*data)+=delta;
					msleep(update_period);
					lcd=fopen("/dev/lcd","w");
					fprintf(lcd,"%s\n",head_string);
					fprintf(lcd,"%4d\n",*data);
					fclose(lcd);
					i++;
					if(i>3 && update_period>UPDATE_PERIOD_DELTA){
						update_period-=UPDATE_PERIOD_DELTA;
						i=0;}

				}
				msleep(DEBOUNCE_DELAY);
				IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE, 0x8);
				}
			if(down_button_edge){
					delta=1;
					i=0;
					(*data)-=delta;
					msleep(DEBOUNCE_DELAY);
					update_period=UPDATE_PERIOD_DEFAULT;
					while(!(up_button=(IORD_ALTERA_AVALON_PIO_DATA(PIO_BUTTON_BASE)&0x4))){
						(*data)-=delta;
						msleep(update_period);
						lcd=fopen("/dev/lcd","w");
						fprintf(lcd,"%s\n",head_string);
						fprintf(lcd,"%4d\n",*data);
						fclose(lcd);
						i++;
						if(i>3 && update_period>UPDATE_PERIOD_DELTA){
							update_period-=UPDATE_PERIOD_DELTA;
							i=0;}

					}
					msleep(DEBOUNCE_DELAY);
					IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE, 0x4);
					}
			}
			IORD_ALTERA_AVALON_PIO_DATA(PIO_BUTTON_BASE);
			/* Write to the edge capture register to reset it. */
			IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE, 0xf);
}

void print_packet(const unsigned char * packet, unsigned int rx_len_copy){
	unsigned int i;
	      for(i=0;i<rx_len_copy-PACKET_TAIL_BYTES;i++)
	      {
	        if(i%8==0)
	        printf(" ");
	        if(i%16==0)
	        printf("\n");
	        printf("%02Xh,",packet[i]);
	      }
}
int sendto_pchost(OS_EVENT* mutex,unsigned char *header, unsigned char* host_pc_ip, unsigned short dest_port
					,unsigned char* data_ptr, unsigned long data_len){
	unsigned short temp_len,j,i,k,attempts=0;
	unsigned char *dump_buffer_ptr;
	unsigned char mac[MAC_LEN];
	int temp_int;
	INT8U error;
	temp_int=arp_table_look_for(arp_table,host_pc_ip,mac);
	if(temp_int==ARP_RECORD_NOT_PRESENT){
		while(attempts<MAX_ARP_ATTEMPTS && temp_int==ARP_RECORD_NOT_PRESENT){
			//arp send request
			send_arp_request(mutex,ARP_REQUEST_PACKET,host_pc_ip);
			msleep(ARP_REQUEST_LOOP_DELAY_MS);
			temp_int=arp_table_look_for(arp_table,host_pc_ip,mac);
			attempts++;
		}
		if(attempts>=MAX_ARP_ATTEMPTS)
			return(MAC_ADDRESS_UNRESOLVED);

	}

		//mac address resolved
			dump_buffer_ptr=malloc((MAX_PAYLOAD_LEN+HEADER_LEN)*sizeof(unsigned char));
			if(dump_buffer_ptr==NULL){
				printf("\nERROR allocating transmit buffer\r\n");
				return(-1);
			}
			OSMutexPend(mutex,0,&error);
				 if(error!=OS_NO_ERR){
					printf("\nsend_to_pchost:error Pending NIC access mutex\n");
					return(-1);
				 }
			if(data_len/MAX_PAYLOAD_LEN)
				temp_len=MAX_PAYLOAD_LEN;
			else
				temp_len=(unsigned short)data_len;
			update_packet_templates(NULL,NULL,header,NULL,NULL,mac,host_pc_ip,0,dest_port,temp_len);
			for(j=0;j<HEADER_LEN;j++)
				 dump_buffer_ptr[j]=header[j];
			for(i=0;i<data_len/MAX_PAYLOAD_LEN;i++){
				for(j=HEADER_LEN,k=0;j<temp_len+HEADER_LEN;j++,k++)
						 dump_buffer_ptr[j]=data_ptr[k+(i*MAX_PAYLOAD_LEN)];
				TransmitPacket(dump_buffer_ptr,temp_len+HEADER_LEN);
				}
			if(data_len%MAX_PAYLOAD_LEN){
				temp_len=data_len%MAX_PAYLOAD_LEN;
				update_packet_templates(NULL,NULL,header,NULL,NULL,NULL,NULL,0,0,temp_len);
				for(j=0;j<HEADER_LEN;j++)
						 dump_buffer_ptr[j]=header[j];
				for(j=HEADER_LEN,k=0;j<temp_len+HEADER_LEN;j++,k++)
								 dump_buffer_ptr[j]=data_ptr[k+(i*temp_len)];
				TransmitPacket(dump_buffer_ptr,temp_len+HEADER_LEN);
			}
			error=OSMutexPost(mutex);
				 if(error!=OS_NO_ERR){
					printf("\nsend_to_pchost:error Posting NIC access mutex\n");
					return(-1);
				 }
		free(dump_buffer_ptr);


}
//arp_table_look_for return (-1) if the ip has not been found in the table and, otherwise,the record index
int arp_table_look_for(struct arp_record* arp_record_ptr,unsigned char * ip, unsigned char *mac){
	int i=0,j=0;
	while(i<ARP_TABLE_LENGTH && (compare_ips(ip,arp_record_ptr[i].ip)==NOTEQUAL) && arp_record_ptr[i].status==ACTIVE) i++;
	//active records fill the table continuosly, the first inactive record sets the table end
	if(i==ARP_TABLE_LENGTH || arp_record_ptr[i].status==INACTIVE)
		return (ARP_RECORD_NOT_PRESENT);
	else{
		//ip has been found in the table
		for(j=0;j<MAC_LEN;j++)
			mac[j]=arp_record_ptr[i].mac[j];
		//return the index in the table where ip has been found
		return(i);
	}
}
//arp_table_add_record (-1) if the table is full, otherwise,the record index
int arp_table_add_record(struct arp_record* arp_record_ptr,unsigned char * ip, unsigned char *mac){
	int i=0,j=0;
	int temp_int;
	//check if ip already present first
	do{
		temp_int=compare_ips(ip,arp_record_ptr[i].ip);
		i++;
	}
	while(arp_record_ptr[i].status==ACTIVE && i<ARP_TABLE_LENGTH && temp_int==NOTEQUAL);
	if(temp_int==EQUAL){
		//update the record with the new mac for the ip found
		for(j=0;j<IP_LEN;j++)
			arp_record_ptr[i].ip[j]=ip[j];
		for(j=0;j<MAC_LEN;j++)
			arp_record_ptr[i].mac[j]=mac[j];
		return(ARP_RECORD_UPDATED);
	}
	//seek the first inactive record
	i=0;j=0;
	while(arp_record_ptr[i].status==ACTIVE && i<ARP_TABLE_LENGTH) i++;
	if(i==ARP_TABLE_LENGTH)
		return(ARP_TABLE_FULL);
	else{
		for(j=0;j<IP_LEN;j++)
			arp_record_ptr[i].ip[j]=ip[j];
		for(j=0;j<MAC_LEN;j++)
			arp_record_ptr[i].mac[j]=mac[j];
		arp_record_ptr[i].status=ACTIVE;
		//return the index where record has been added
		return(i);
	}
}
void arp_table_init(struct arp_record* arp_record_ptr){
	int i;
	for(i=0;i<ARP_TABLE_LENGTH;i++)
		arp_record_ptr[i].status=INACTIVE;
}

int send_arp_request(OS_EVENT* mutex,unsigned char* arp_request_packet,unsigned char* wanted_ip){
	INT8U error;
	OSMutexPend(mutex,0,&error);
	 if(error!=OS_NO_ERR){
		printf("\nsend_to_pchost:error Pending NIC access mutex\n");
		return(-1);
	 }
	 //update_packet_templates(arp_request_packet,NULL,NULL,NULL,NULL,NULL, wanted_ip,0,0,0);
	 print_packet(arp_request_packet,ARP_REQUEST_PACKET_LEN);
	 TransmitPacket(arp_request_packet,ARP_REQUEST_PACKET_LEN);
	 error=OSMutexPost(mutex);
		 if(error!=OS_NO_ERR){
			printf("\nsend_to_pchost:error Posting NIC access mutex\n");
			return(-1);
		 }
	return(0);
}
void arp_print_table(struct arp_record *arp_table){
	int i,j;
	printf("\n");
	while(arp_table[i].status==ACTIVE && i<ARP_TABLE_LENGTH){
		printf("record %02d:");
		printf("IP:");
		for(i=0;i<IP_LEN;i++)
			printf("%03d.",arp_table[i].ip[j]);
		printf("MAC:");
		for(i=0;i<MAC_LEN;i++)
			printf("%02x:",arp_table[i].mac[j]);
		i++;
	}
}
