/*************************************************************************
* Copyright (c) 2004 Altera Corporation, San Jose, California, USA.      *
* All rights reserved. All use of this software and documentation is     *
* subject to the License Agreement located at the end of this file below.*
**************************************************************************
* Description:                                                           *
* The following is a simple hello world program running MicroC/OS-II.The * 
* purpose of the design is to be a very simple application that just     *
* demonstrates MicroC/OS-II running on NIOS II.The design doesn't account*
* for issues such as checking system call return codes. etc.             *
*                                                                        *
* Requirements:                                                          *
*   -Supported Example Hardware Platforms                                *
*     Standard                                                           *
*     Full Featured                                                      *
*     Low Cost                                                           *
*   -Supported Development Boards                                        *
*     Nios II Development Board, Stratix II Edition                      *
*     Nios Development Board, Stratix Professional Edition               *
*     Nios Development Board, Stratix Edition                            *
*     Nios Development Board, Cyclone Edition                            *
*   -System Library Settings                                             *
*     RTOS Type - MicroC/OS-II                                           *
*     Periodic System Timer                                              *
*   -Know Issues                                                         *
*     If this design is run on the ISS, terminal output will take several*
*     minutes per iteration.                                             *
**************************************************************************/


#include <stdio.h>
#include "includes.h"



#include "DM9000A.h"
#include <math.h>
#include "system.h"
#include <string.h>
#include "de2_70_NIC_utilities.h"
#include "altera_avalon_pio_regs.h"
#include <sys/alt_irq.h>
#include  "de2_70_NIC_utilities.h"

    void write_register(unsigned int data, unsigned int reg_addr) {

        IOWR(CONTROL_OUT_PIO_BASE,0,0x200); //reset della macchina a stati
        IOWR(CONTROL_OUT_PIO_BASE,0,0x200);
        IOWR(CONTROL_OUT_PIO_BASE,0,0x0);

        //set data on NIOS output data register
        IOWR(DATA_OUT_PIO_BASE,0,data);
        //toggle addreess with NIOS output control register
        IOWR(CONTROL_OUT_PIO_BASE,0,reg_addr);
        IOWR(CONTROL_OUT_PIO_BASE,0,reg_addr);
        IOWR(CONTROL_OUT_PIO_BASE,0,0x0);  
        IOWR(CONTROL_OUT_PIO_BASE,0,0x0);  
  }

    int read_fifo(unsigned int addr, unsigned int *data){

    	unsigned int inctrl;
    	int shift, idx = 0;
    	//control that FIFO is not empty
    	inctrl = IORD(CONTROL_IN_PIO_BASE,0);
    	shift = (addr-1)*2 + 1;
    	inctrl &= 1<<shift;
    	while(inctrl == 0) { //this means that the FIFO is NOT empty
    	//Issue a ReadCLK to the FIFO with outctrl bus (the signal is internally clipped)
    	//and enable the FIFO mux maintaining fixed the outctrl bus
        IOWR(CONTROL_OUT_PIO_BASE,0,addr);
//        IOWR(CONTROL_OUT_PIO_BASE,0,addr);
//        IOWR(CONTROL_OUT_PIO_BASE,0,addr);
//        IOWR(CONTROL_OUT_PIO_BASE,0,addr);
//        IOWR(CONTROL_OUT_PIO_BASE,0,addr);
        usleep(1);
        //read indata bus that contains the fifo output
        data[idx] = IORD(DATA_IN_PIO_BASE,0);
        printf("data = %08x idx = %d \n",data[idx], idx);
    	idx++;
    	//Reset the outctrl bus to be ready for a new readout
    	IOWR(CONTROL_OUT_PIO_BASE,0,0);
    	//control that FIFO is not empty
    	inctrl = IORD(CONTROL_IN_PIO_BASE,0);
    	shift = (addr-1)*2 + 1;
    	inctrl &= 1<<shift;
    	}
    	return idx;
    }

    int read_inctrl(unsigned int *data){

    	data[0] = IORD(CONTROL_IN_PIO_BASE,0);

    	return 1;

    }



unsigned int aaa,rx_len,i,packet_num;
unsigned char RXT[2000];
//BEGIN OS structures definitions and declarations
#define MESS_Q_LENGTH 200
#define ARP_Q_LENGTH 200
#define MAX_MSG_STR_LENGTH 300

OS_EVENT* MessQ;
OS_EVENT* ARPQ;
void* MessList[MESS_Q_LENGTH];
void* ARPMessList[ARP_Q_LENGTH];
struct msg {
	unsigned char msg_str[MAX_MSG_STR_LENGTH];
	unsigned int msg_len;

} msg;
struct arp_msg {
	unsigned short rcvd_op_code;
	unsigned char sender_ip[IP_LEN];
	unsigned char sender_mac[MAC_LEN];

} arp_msg;

struct msg msg_array[MESS_Q_LENGTH];
struct arp_msg arp_msg_array[ARP_Q_LENGTH];
struct arp_record arp_table[ARP_TABLE_LENGTH];
OS_MEM *msg_array_partition;
OS_MEM *arp_msg_array_partition;
OS_EVENT *Eth_NIC_Mutex;
//END OS structures definitions and declarations
extern unsigned char dev_board_mac_add[]={0x01,0x60,0x6E,0x11,0x02,0xFF};
extern unsigned char host_pc_mac_add[]={0x00,0x1d,0xba,0x86,0xa4,0x2d};
extern const unsigned char broadcast_mac_add[]={0xff,0xff,0xff,0xff,0xff,0xff};
extern unsigned char dev_board_ip_add[]={10,0,0,2};
extern unsigned char host_pc_ip_add[]={10,0,0,1};
extern unsigned int host_physics_data_port=2222,host_cmd_port=4444,payload_len=1024;


unsigned char HEAD[] = {  0xff,0xff,0xff,0xff,0xff,0xff,//destination mac address
                          0x01,0x60,0x6E,0x11,0x02,0x0F,//source MAC adddress
                          0x08,0x00,//type (Etnernet=0x0800)
                          0x45,//versio (IPV4 and IPlength=20 bytes)
                          0x00,
                          0x04,0x1c,//total length (IPpayload+Header)
                          0xaa,0xbb,//identification
                          0x00,0x00,0xff,0x11,//F�ags,Offset,TTl;prtotocol(UDP=0x11)
                          0xf9,0x12,//IP checksum
                          0x0a,0x00,0x00,0x02,//IP SOURCE
                          0x0a,0x00,0x00,0x01,//IP DESTINATION
                          0x80,0x01,//UDP source Port
                          0x04,0x08,//UDP dest Port
                          0x04,0x08,//length UDP+PAYLOAD
                          0x00,0x00//checksum
                          };

unsigned char ARP_REPLY_PACKET[]={
		    0x00 , 0x1d  , 0xba , 0x86 , 0xa4 , 0x2d , //destination mac address
		    0x01, 0x60 , 0x6E , 0x11 , 0x02 , 0x0F ,//source MAC adddress
		    0x08 , 0x06 , //type (ARP=0x0806)
		    0x00 , 0x01,//hardware type (Ethernet=1)
		    0x08 , 0x00 ,//protocol type(IP=0x0800)
		    0x06 , 0x04 ,//hardware(MAC) size, protocol size(IP)
		    0x00 , 0x02 , //operation(2 byte 1=REQUESt, 2=REPLY
		    0x01, 0x60 , 0x6E , 0x11 , 0x02 , 0x0F , //sender MAC ADDRESS
		    0x0a , 0x00  , 0x00 , 0x02  , //sender IP address
		    0x00 , 0x1d  , 0xba , 0x86 , 0xa4 , 0x2d ,//target MAC address
		    0x0a , 0x00 , 0x00 , 0x01,//target IP address
  	  	  };
unsigned char ARP_REQUEST_PACKET[]={
		    0xff , 0xff  ,0xff , 0xff , 0xff , 0xff , //destination mac address
		    0x01, 0x60 , 0x6E , 0x11 , 0x22 , 0xFF ,//source MAC adddress
		    0x08 , 0x06 , //type (ARP=0x0806)
		    0x00 , 0x01,//hardware type (Ethernet=1)
		    0x08 , 0x00 ,//protocol type(IP=0x0800)
		    0x06 , 0x04 ,//hardware(MAC) size, protocol size(IP)
		    0x00 , 0x01 , //operation(2 byte 1=REQUESt, 2=REPLY
		    0x01, 0x60 , 0x6E , 0x11 , 0x22 , 0xFF , //sender MAC ADDRESS
		    0x0a , 0x00  , 0x00 , 0x02  , //sender IP address
		    0x00 , 0x1d  , 0xba , 0x86 , 0xa4 , 0x2d ,//target MAC address
		    0x0a , 0x00 , 0x00 , 0x01,//target IP address
  	  	  };

//BEGIN command manipulations utilities

#define MAX_CMD_PARAMETERS 5
#define CMD_STR__ELEMENT_MAX_LENGTH 20
struct command{
	char cmd_type;
	char cmd_str[CMD_STR__ELEMENT_MAX_LENGTH];
	char cmd_parameter[MAX_CMD_PARAMETERS][CMD_STR__ELEMENT_MAX_LENGTH];
	int cmd_param_number;
};
int serial_string_parser(char* input_str, struct command * cmd_ptr){
	int i=0,j=0,k=0;
	(*cmd_ptr).cmd_type=input_str[0];
	while(input_str[i]!='\0'&& input_str[i]!=' '&& i< CMD_STR__ELEMENT_MAX_LENGTH)i++;
			j=i++;
	while(input_str[i]!='\0'&& input_str[i]!=' '&& i< CMD_STR__ELEMENT_MAX_LENGTH)i++;

			strncpy((*cmd_ptr).cmd_str,input_str+j+1,i-j-1);
			(*cmd_ptr).cmd_str[i-j-1]='\0';
	while(input_str[i]!='\0')
		{
		j=i++;
		while(input_str[i]!='\0'&& input_str[i]!=' '&& i< (CMD_STR__ELEMENT_MAX_LENGTH+1)*MAX_CMD_PARAMETERS+2)i++;
			strncpy(&((*cmd_ptr).cmd_parameter[k]),input_str+j+1,i-j-1);
			(*cmd_ptr).cmd_parameter[k][i-j-1]='\0';
			k++;
		}
	(*cmd_ptr).cmd_param_number=k;
	return i;

}
void print_command(struct command incoming_cmd){
unsigned int i;
	//printf("\nINCOMING CMD: \ttype=\t%c",incoming_cmd.cmd_type);
	//printf("\nINCOMING CMD: \tcommand=\t%s",incoming_cmd.cmd_str);
		//for(i=0;i<incoming_cmd.cmd_param_number;i++)
			//printf("\nINCOMING CMD: \tparam%d=\t%s",i,&(incoming_cmd.cmd_parameter[i]));
}

//END command manipulations utilities



void ethernet_interrupts()
{

unsigned short packet_type=0,arp_operation=0,dest_port;
unsigned int rx_len_copy,i,j;
unsigned char transport_layer_protocol;
unsigned char temp_ip[4];
unsigned char temp_mac[6];
char* BROADCAST_MAC={0xff,0xff,0xff,0xff,0xff,0xff};
int isme;
struct msg *msg_ptr;
struct arp_msg *arp_msg_ptr;
INT8U error;
OSIntEnter();
    aaa=MyReceivePacket (RXT,&rx_len);
    if(!aaa)
    {

      rx_len_copy=rx_len;
      packet_num++;
      packet_type=extract_data_from_header(RXT,ETHERNET_TYPE_OFFSET,ETHERNET_TYPE_END);
      arp_operation=extract_data_from_header(RXT,ARP_OPERATION_MSB,ARP_OPERATION_LSB);
//      get_mac(RXT,ETHERNET_DEST_OFFSET,ETHERNET_DEST_END, temp_mac);
//      if((compare_macs(temp_mac,dev_board_mac_add)==EQUAL) || 
//    		  (compare_macs(temp_mac, BROADCAST_MAC)==EQUAL)){
      if (packet_type==0x0806){
    	  //0x0806 indentifies ARP packet
    	  get_ip(RXT, ARP_TARGET_IP_OFFSET,ARP_TARGET_IP_END, temp_ip);
    	  isme=compare_ips(dev_board_ip_add,temp_ip);

    	  if(isme){
    		  arp_msg_ptr=(struct arp_msg*)OSMemGet(arp_msg_array_partition,&error);
    		  if(error==OS_NO_ERR){
    			  get_mac(RXT, ARP_SENDER_MAC_OFFSET,ARP_SENDER_MAC_END, (*arp_msg_ptr).sender_mac);
    			  get_ip(RXT, ARP_SENDER_IP_OFFSET,ARP_SENDER_IP_END, (*arp_msg_ptr).sender_ip);
    			  (*arp_msg_ptr).rcvd_op_code=arp_operation;
    			  error=OSQPost(ARPQ,(void*)arp_msg_ptr);
				  switch(error){
					  case OS_NO_ERR:{
						  //printf("\r\nARP Queue Posted!");
						  break;
						  }
					  case OS_Q_FULL:{
						  printf("\r\nARP Queue is full!!");
						  break;
						  }
    			  	}
    		  	  }


    	  	  }
    	  else
    		  printf("(ARP Not for me. Hang On!)\n");
      	  }

      else if(packet_type==0x0800){
    	  	  transport_layer_protocol=extract_data_from_header(RXT,IP_UPPER_PROTOCOL_OFFSET,IP_UPPER_PROTOCOL_END);
    	  	  if(transport_layer_protocol==UDP){
    	  		dest_port=extract_data_from_header(RXT,UDP_DEST_PORT_OFFSET,UDP_DEST_PORT_END);
    	  		get_ip(RXT, IP_DEST_IP_OFFSET,IP_DEST_IP_END, temp_ip);
    	  		isme=compare_ips(dev_board_ip_add,temp_ip);
    	  		if(dest_port==MESSAGE_PORT && (isme==1)){
    	  			msg_ptr=(struct msg*)OSMemGet(msg_array_partition,&error);
    	  			if(error==OS_NO_ERR){
					  for(i=UDP_PAYLOAD_OFFSET,j=0;j<MAX_MSG_STR_LENGTH && i<rx_len_copy-PACKET_TAIL_BYTES;i++,j++)
						  (*msg_ptr).msg_str[j]=RXT[i];
						  (*msg_ptr).msg_len=j;
						  (*msg_ptr).msg_str[j]='\0';
						  error=OSQPost(MessQ,(void*)msg_ptr);
						  switch(error){
							  case OS_NO_ERR:{
								  //printf("\r\nMessage Posted!");
								  //usleep(50000);
								  break;
								  }
							  case OS_Q_FULL:{
								  printf("\r\nMessage Queue is full!!");
								  break;
								  }
						  }
    	  			}
    	  		}
    	  	  }
//    	  	else{
//    	  	    	  printf("\r\nUFEP(UnidentiFied Ethernet Packet)");
//    	  	    	  print_packet(RXT, rx_len_copy);
//    	  	      	  }
      	  	}
//      else
//    	  printf("\r\nUFP(UnidentiFied Packet)");


    }

//    else
//    	printf("\nInterrupt generated but no data fetched ");
//    		  }
    OSIntExit();
}





/* Definition of Task Stacks */
#define   TASK_STACKSIZE       2048
OS_STK    task1_stk[TASK_STACKSIZE];
OS_STK    task2_stk[TASK_STACKSIZE];
OS_STK    arp_task_stk[TASK_STACKSIZE];

/* Definition of Task Priorities */
#define ETH_NIC_MUTEX_PIP  	4
#define TASK1_PRIORITY      7
#define TASK2_PRIORITY      5
#define ARP_TASK_PRIORITY	6



/* Prints "Hello World" and sleeps for three seconds */
#define DATA_LEN 4096
int task1(void* pdata)
{

	  FILE* lcd;
	  double arg,freq=10;
	  unsigned char *DATA;
	  unsigned short *us_ptr;
	  unsigned int pio_switches;
	  alt_u32 tx_pck_num=0,i;
	  INT8U error;

	  DATA=malloc(DATA_LEN*sizeof(unsigned char));
	  us_ptr=(unsigned short*)DATA;
	  if(DATA==NULL){
		  printf("Error allocating DATA buffer\n");
		  return(-1);
	  }

	  printf("\r\n DM9000A init");
	  DM9000_init();
	  printf(" done!(iorq=%d)", DM9000A_IRQ);
	  alt_irq_register( DM9000A_IRQ, NULL, (void*)ethernet_interrupts );
	  packet_num=0;
	  printf("\n");
	  while (1)
	  {
	    msleep(500);
	    for(i=0;i<DATA_LEN/2;i++)
	    	  	    	  		  us_ptr[i]=200*sin((double)i/800);
	    pio_switches=IORD_ALTERA_AVALON_PIO_DATA(PIO_SWITCH_BASE);
	    if(pio_switches & SEND_DEBUG_PACKETS_SW)
			{
	    	printf("Sending data\n");
	    	sendto_pchost(Eth_NIC_Mutex,HEAD,host_pc_ip_add,host_physics_data_port,DATA, DATA_LEN);
			}

	  }
}
char *buffer;
unsigned int data[4096];
void task2(void* pdata)
{
  struct msg *msg_ptr;
  struct command incoming_cmd;
  INT32U numeric_param[MAX_CMD_PARAMETERS];
  INT8U error;
  int temp_int;
  FILE* lcd;
  int size;

  int ndata,idata;
  unsigned int i,msg_counter=0;

  printf("\nCreated task2\n");

  while (1)
  { 
	  lcd=fopen("/dev/lcd","w");

	  msg_ptr=(struct msg *)OSQPend(MessQ,0,&error);
	  if(error==OS_NO_ERR){

		  if((*msg_ptr).msg_str[0]=='!'){
			  serial_string_parser((*msg_ptr).msg_str, &incoming_cmd);
		  	  print_command(incoming_cmd);
		  	if(strcmp(incoming_cmd.cmd_str,"WRITE_ATOM_REG")==0){
		  		if(incoming_cmd.cmd_param_number==2){
		  			numeric_param[0]=strtoul(incoming_cmd.cmd_parameter[0],NULL,16);
		  			numeric_param[1]=strtoul(incoming_cmd.cmd_parameter[1],NULL,16);
		  			//printf("\r\nReceived %s with parameters 0x%08X 0x%08X",incoming_cmd.cmd_str,
		  			//											 numeric_param[0],numeric_param[1]);
		  			//call related function
		  			 write_register(numeric_param[0], numeric_param[1]);
		  			 temp_int=sendto_pchost(Eth_NIC_Mutex,HEAD, host_pc_ip_add, host_cmd_port,
		  					 	 	 "WRITE_ATOM_REG_DONE",sizeof("WRITE_ATOM_REG_DONE"));
		  			 if(temp_int<0)
		  				 printf("\nError sending data (E:%d)\n",temp_int);

		  		}
		  	} //endif strcmp WRITE_ATOM_REG
		  	else if(strcmp(incoming_cmd.cmd_str,"READ_ATOM_DATA")==0){
		  		if(incoming_cmd.cmd_param_number==1){
		  			numeric_param[0]=strtoul(incoming_cmd.cmd_parameter[0],NULL,16);
		  			printf("\r\nReceived %s with parameter 0x%08X\n",incoming_cmd.cmd_str,
		  														 numeric_param[0]);
		  			//call related function
		  			if(numeric_param[0] == 0) {
		  				ndata = read_inctrl(data);
		  			}
		  			else if(numeric_param[0]>0 && numeric_param[0]<5) {
		  				ndata = read_fifo(numeric_param[0],data);
		  			}
		  			//fill the buffer for UDP packet
		  			char tempbuf[4096]={0};

		  			printf("ndata = %d\n",ndata);
		  			if (ndata == 0) {
		  				size = 14 * sizeof(char);
		  				sprintf(tempbuf,"FIFO %d empty", numeric_param[0]);
		  			}
		  			else {
		  				size = ndata * 8 * sizeof(char);
		  				for(idata = 0; idata<ndata; idata++) {
		  					sprintf(tempbuf+8*idata,"%08x",data[idata]);
		  				}
		  			}
		  			//Send data
	  				buffer = (char*) malloc(size);
	  				memcpy(buffer,tempbuf,size);

		  			temp_int=sendto_pchost(Eth_NIC_Mutex,HEAD, host_pc_ip_add,host_cmd_port,buffer,size);
			  		free(buffer);
		  			if(temp_int<0)
		  				printf("\nError sending data (E:%d)\n",temp_int);
		  			else
		  				printf("\nData Sent\n");
		  			//Send end of command
		  			temp_int=sendto_pchost(Eth_NIC_Mutex,HEAD, host_pc_ip_add, host_cmd_port,
		  					 	 	 "READ_ATOM_DATA_DONE",sizeof("READ_ATOM_DATA_DONE"));
		  			 if(temp_int<0)
		  				 printf("\nError sending data (E:%d)\n",temp_int);


		  		}
		  	} //endif strcmp READ_ATOM_DATA


		  }
		  else{
			  printf("\r\nMessage (%d):\r\n",msg_counter++);
			  for(i=0;i<(*msg_ptr).msg_len;i++){
				  printf("%c",(*msg_ptr).msg_str[i]);
				  fprintf(lcd,"%c",(*msg_ptr).msg_str[i]);
			  	  }
		  	  }
		  fprintf(lcd,"\n");
		  fclose(lcd);
		  error=OSMemPut(msg_array_partition,(void*)msg_ptr);
		  if(error!=OS_NO_ERR)
			  printf("\nERROR (%d) releasing MSGS memory block",error);
	  }
	  else
		  printf("\r\nError Pending Messsage from CMD_MSG Queue:\r\n");
  }
}

void arp_task(void* pdata)
{
	struct arp_msg *msg_ptr;
	int temp_int;
	INT8U error;
	printf("\narp_task started");
	while(1){
		 msg_ptr=(struct arp_msg *)OSQPend(ARPQ,0,&error);
		 if(error==OS_NO_ERR){
			 if((*msg_ptr).rcvd_op_code==ARP_REQUEST){
				 printf("\nARP: ");
				 printf("SENDER MAC ");
				 for(i=0;i<MAC_LEN;i++)
					 printf("%02X: ",(*msg_ptr).sender_mac[i]);
				 printf("\nSENDER IP ");
				 for(i=0;i<IP_LEN;i++)
				 	 printf("%3d ",(*msg_ptr).sender_ip[i]);

			 	 OSMutexPend(Eth_NIC_Mutex,0,&error);
			 	 update_packet_templates(NULL,ARP_REPLY_PACKET,NULL,NULL,NULL,(*msg_ptr).sender_mac,(*msg_ptr).sender_ip, 0,0,0);
					 if(error!=OS_NO_ERR){
						printf("\nARP:error Pending NIC access mutex\n");
						return(-1);
					 }
				 TransmitPacket(ARP_REPLY_PACKET,sizeof(ARP_REPLY_PACKET));
				 error=OSMutexPost(Eth_NIC_Mutex);
					 if(error!=OS_NO_ERR){
						printf("\nARP:error Posting NIC access mutex\n");
						return(-1);
					 }
			 	 printf("(Replied)\n");
			 	 }
			 else
				 if((*msg_ptr).rcvd_op_code==ARP_REPLY){
					 printf("\nARP: ");
					 for(i=0;i<IP_LEN;i++)
					 		printf("%3d ",(*msg_ptr).sender_ip[i]);
					 printf(" is at ");
					 for(i=0;i<MAC_LEN;i++)
					 	printf("%02X: ",(*msg_ptr).sender_mac[i]);
					 printf("\n");
					 temp_int=arp_table_add_record(arp_table,(*msg_ptr).sender_ip, (*msg_ptr).sender_mac);
					 if(temp_int>=0)
						 printf("added a new record at ARP_TABLE[%d]\n",temp_int);
					 else
						 if(temp_int=ARP_RECORD_UPDATED)
							 printf("record was already in ARP_TABLE (updated)\n");
					 else
						 printf("ARP_TABLE is full\n");
					 arp_print_table(arp_table);
				 }
			 error=OSMemPut(arp_msg_array_partition,(void*)msg_ptr);
			 if(error!=OS_NO_ERR)
				 printf("\nERROR (%d) releasing ARP memory block",error);
			 else
				 printf("\nARP memory block released",error);
		 }
		 else
		 		  printf("\r\nError Pending Messsage from ARP Queue:\r\n");
	}
}
/* The main function creates two task and starts multi-tasking */
int main(void)
{
	FILE* lcd;
	unsigned int pio_switches;
	INT8U error;
	printf("\nHello!\n");
	//initialize OS and message queue
	OSInit();
	MessQ=OSQCreate(MessList,MESS_Q_LENGTH);
	if(MessQ==NULL){
		printf("\r\nMessage Queue has not been created!");
		return(-1);
	}
	msg_array_partition=OSMemCreate(msg_array,MESS_Q_LENGTH,sizeof(msg),&error);
	if(error!=OS_NO_ERR){
		printf("\nerror allocating memory partition for messages");
		return(-1);
	}
	//initialize ARP Queue
	ARPQ=OSQCreate(ARPMessList,ARP_Q_LENGTH);
		if(ARPQ==NULL){
			printf("\r\nMessage Queue has not been created!");
			return(-1);
		}
		arp_msg_array_partition=OSMemCreate(arp_msg_array,ARP_Q_LENGTH,sizeof(arp_msg),&error);
		if(error!=OS_NO_ERR){
			printf("\nerror allocating memory partition for ARP");
			return(-1);
		}
	//create a mutex for access to the NIC DM9000A
	Eth_NIC_Mutex=OSMutexCreate(ETH_NIC_MUTEX_PIP,&error);
	if(error!=OS_NO_ERR){
		printf("\nerror creating NIC access Mutex");
		return(-1);
	}
	arp_table_init(arp_table);
	  IORD_ALTERA_AVALON_PIO_DATA(PIO_BUTTON_BASE);
	  /* Write to the edge capture register to reset it. */
	  IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PIO_BUTTON_BASE, 0xf);
	  pio_switches=IORD_ALTERA_AVALON_PIO_DATA(PIO_SWITCH_BASE);
	  if(pio_switches & PROGRAM_NET_PARAM_SW){
	  		  get_mac_address_from_keyboard(dev_board_mac_add);
	  		  get_ip_address_from_keyboard("Dev.Board IP:",dev_board_ip_add);
	  		  get_ip_address_from_keyboard("Host PC IP:",host_pc_ip_add);
	  		 if(store_addresses_to_epcs("/dev/epcs_flash_controller_0",dev_board_mac_add, dev_board_ip_add, host_pc_ip_add)<0)printf("Error writing addresses in EPCS\n");
//	  		  if(store_mac_to_epcs("/dev/epcs_flash_controller_0",dev_board_mac_add)<0) printf("Error writing Brd MAC in EPCS\n");
//	  		if(store_host_ip_to_epcs("/dev/epcs_flash_controller_0",host_pc_ip_add)<0) printf("Error writing Host IP in EPCS\n");
//	  		 if(store_devb_ip_to_epcs("/dev/epcs_flash_controller_0",dev_board_ip_add)<0) printf("Error writing Brd IP in EPCS\n");


	  //		  get_uint_from_keyboard("Host PC Data Port:",&host_physics_data_port);
	  //		  get_uint_from_keyboard("Host PC CMD:",&host_cmd_port);
	  		  }
	  	  else{
	  		 if( retrieve_mac_from_epcs("/dev/epcs_flash_controller_0",dev_board_mac_add)<0) printf("Error reading Brd MAC from EPCS\n");
	  		  if(retrieve_devb_ip_from_epcs("/dev/epcs_flash_controller_0",dev_board_ip_add)<0) printf("Error reading Brd IP from EPCS\n");
	  		  if(retrieve_host_ip_from_epcs("/dev/epcs_flash_controller_0",host_pc_ip_add)<0) printf("Error reading Host IP from EPCS\n");
	  	  }
	  printf("Host-PC IP: %3d.%3d.%3d.%3d\nDev-Brd IP: %3d.%3d.%3d.%3d\n",host_pc_ip_add[0],host_pc_ip_add[1],host_pc_ip_add[2],host_pc_ip_add[3],dev_board_ip_add[0],dev_board_ip_add[1],dev_board_ip_add[2],dev_board_ip_add[3]);
	  printf("Brd MAC: %02X:%02X:%02X:%02X:%02X:%02X\n",dev_board_mac_add[0],dev_board_mac_add[1],dev_board_mac_add[2],dev_board_mac_add[3],dev_board_mac_add[4],dev_board_mac_add[5]);
	  lcd=fopen("/dev/lcd","w");
	  	  fprintf(lcd,"Host-PC IP: %3d.%3d.%3d.%3d\nDev-Brd IP: %3d.%3d.%3d.%3d\n",host_pc_ip_add[0],host_pc_ip_add[1],host_pc_ip_add[2],host_pc_ip_add[3],dev_board_ip_add[0],dev_board_ip_add[1],dev_board_ip_add[2],dev_board_ip_add[3]);
	  	  fclose(lcd);
	  update_packet_templates(ARP_REQUEST_PACKET,ARP_REPLY_PACKET,HEAD,
							  dev_board_mac_add,dev_board_ip_add,
							  host_pc_mac_add,  host_pc_ip_add, 0,host_physics_data_port,payload_len);



  OSTaskCreateExt(task1,
                  NULL,
                  (void *)&task1_stk[TASK_STACKSIZE-1],
                  TASK1_PRIORITY,
                  TASK1_PRIORITY,
                  task1_stk,
                  TASK_STACKSIZE,
                  NULL,
                  0);
              

  OSTaskCreateExt(task2,
                  NULL,
                  (void *)&task2_stk[TASK_STACKSIZE-1],
                  TASK2_PRIORITY,
                  TASK2_PRIORITY,
                  task2_stk,
                  TASK_STACKSIZE,
                  NULL,
                  0);
  OSTaskCreateExt(arp_task,
                    NULL,
                    (void *)&arp_task_stk[TASK_STACKSIZE-1],
                    ARP_TASK_PRIORITY,
                    ARP_TASK_PRIORITY,
                    arp_task_stk,
                    TASK_STACKSIZE,
                    NULL,
                    0);
  OSStart();
  return 0;
}

/******************************************************************************
 *
******************************************************************************/
