/*
 * "Hello World" example.
 *
 * This example prints 'Hello from Nios II' to the STDOUT stream. It runs on
 * the Nios II 'standard', 'full_featured', 'fast', and 'low_cost' example
 * designs. It runs with or without the MicroC/OS-II RTOS and requires a STDOUT
 * device in your system's hardware.
 * The memory footprint of this hosted application is ~69 kbytes by default
 * using the standard reference design.
 *
 * For a reduced footprint version of this template, and an explanation of how
 * to reduce the memory footprint for a given application, see the
 * "small_hello_world" template.
 *
 */


#include <stdio.h>
#include "altera_avalon_pio_regs.h"
#include "system.h"
#include "time.h"

alt_8 givemedigit(int );

int main()
{
FILE *lcd_ptr;
lcd_ptr = fopen("/dev/lcd","w");
//printf("Hello Brodo from Nios II!\n");
//read the switch status
int switch_data = IORD(PIO_SWITCH_BASE,0);
int board_address = switch_data&0x7;
// operate the seg7 digits
// the two left-most digits are suited for the board number
IOWR(SEG7_BASE+0x18, 0, givemedigit(board_address%10));	// writes the seventh digit
IOWR(SEG7_BASE+0x1C, 0, givemedigit(board_address/10));  // writes the eighth digit
//write in the lcd
//fprintf(lcd_ptr,"Stato switch #%d\n",switch_data);
//write on the outdata bus a number to check the output through green LEDs
//int i = 0;
//while(i<16){
//IOWR(DATA_OUT_PIO_BASE,0,i);
//i++;
//usleep(1000000);
//}
// code to write and read the TestFIFO
//1- prepare the data in the output register
//2- give the wrreq with the out ctrl register (bit 0)
// now the data is in the FIFO
//3- give the rdreq with the out ctrl register (bit 1)
//4- take the indata bis and check the content
//1!
//IOWR(DATA_OUT_PIO_BASE,0,0x01234567);
//2!
//IOWR(CONTROL_OUT_PIO_BASE,0,0x1);
//IOWR(CONTROL_OUT_PIO_BASE,0,0x0);
//3!
//IOWR(CONTROL_OUT_PIO_BASE,0,0x2);
//IOWR(CONTROL_OUT_PIO_BASE,0,0x0);
//4!
//int indata = IORD(DATA_IN_PIO_BASE,0);

//check the event couter...
//1- reset the counter (bit 29 of out control bus)
//2- issue a trigger (bit 30 of out control bus)
//3- read event counter status

//1- set outdata value
//2- enable address on outcontrol
//3- read data from register


IOWR(CONTROL_OUT_PIO_BASE,0,0x200); //reset della macchina a stati
IOWR(CONTROL_OUT_PIO_BASE,0,0x200); //reset della macchina a stati
IOWR(CONTROL_OUT_PIO_BASE,0,0x0);


IOWR(DATA_OUT_PIO_BASE,0,0x0);
//2
IOWR(CONTROL_OUT_PIO_BASE,0,0x8);
IOWR(CONTROL_OUT_PIO_BASE,0,0x8);
IOWR(CONTROL_OUT_PIO_BASE,0,0x0);
IOWR(CONTROL_OUT_PIO_BASE,0,0x0);

usleep(1000);
/*

IOWR(DATA_OUT_PIO_BASE,0,0xF1E); //master reset
//2
IOWR(CONTROL_OUT_PIO_BASE,0,0x48);
IOWR(CONTROL_OUT_PIO_BASE,0,0x48);
IOWR(CONTROL_OUT_PIO_BASE,0,0x0);
IOWR(CONTROL_OUT_PIO_BASE,0,0x0);

usleep(1000);


IOWR(DATA_OUT_PIO_BASE,0,0xAB108814);
//2
IOWR(CONTROL_OUT_PIO_BASE,0,0x8);
IOWR(CONTROL_OUT_PIO_BASE,0,0x8);
IOWR(CONTROL_OUT_PIO_BASE,0,0x0);
IOWR(CONTROL_OUT_PIO_BASE,0,0x0);

IOWR(DATA_OUT_PIO_BASE,0,0xF1B);
//2
IOWR(CONTROL_OUT_PIO_BASE,0,0x48);
IOWR(CONTROL_OUT_PIO_BASE,0,0x48);
IOWR(CONTROL_OUT_PIO_BASE,0,0x0);
IOWR(CONTROL_OUT_PIO_BASE,0,0x0);

*/

//3
int data = IORD(DATA_IN_PIO_BASE,0);

printf("%08x\n",data);
fprintf(lcd_ptr,"%x\n",data);


usleep(10);
data = IORD(DATA_IN_PIO_BASE,0);

printf("%08x\n",data);
fprintf(lcd_ptr,"%x\n",data);

fclose(lcd_ptr);
return 0;
}


alt_8 givemedigit(int datain){
	switch (datain){
	case 0: return 0x3F;
	case 1: return 0x06;
	case 2: return 0x5B;
	case 3: return 0x4F;
	case 4: return 0x66;
	case 5: return 0x6D;
	case 6: return 0x7D;
	case 7: return 0x07;
	case 8: return 0x7F;
	case 9: return 0x67;
	default: return 0xFF;
	}
}

