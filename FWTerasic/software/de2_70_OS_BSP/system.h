/*
 * system.h - SOPC Builder system and BSP software package information
 *
 * Machine generated for CPU 'cpu' in SOPC Builder design 'DE2_70_SOPC'
 * SOPC Builder design path: ../../DE2_70_SOPC.sopcinfo
 *
 * Generated: Fri Nov 16 13:10:31 CET 2012
 */

/*
 * DO NOT MODIFY THIS FILE
 *
 * Changing this file will have subtle consequences
 * which will almost certainly lead to a nonfunctioning
 * system. If you do modify this file, be aware that your
 * changes will be overwritten and lost when this file
 * is generated again.
 *
 * DO NOT MODIFY THIS FILE
 */

/*
 * License Agreement
 *
 * Copyright (c) 2008
 * Altera Corporation, San Jose, California, USA.
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * This agreement shall be governed in all respects by the laws of the State
 * of California and by the laws of the United States of America.
 */

#ifndef __SYSTEM_H_
#define __SYSTEM_H_

/* Include definitions from linker script generator */
#include "linker.h"


/*
 * CPU configuration
 *
 */

#define ALT_CPU_ARCHITECTURE "altera_nios2"
#define ALT_CPU_BIG_ENDIAN 0
#define ALT_CPU_BREAK_ADDR 0x1408820
#define ALT_CPU_CPU_FREQ 100000000u
#define ALT_CPU_CPU_ID_SIZE 1
#define ALT_CPU_CPU_ID_VALUE 0x0
#define ALT_CPU_CPU_IMPLEMENTATION "fast"
#define ALT_CPU_DATA_ADDR_WIDTH 0x19
#define ALT_CPU_DCACHE_LINE_SIZE 32
#define ALT_CPU_DCACHE_LINE_SIZE_LOG2 5
#define ALT_CPU_DCACHE_SIZE 2048
#define ALT_CPU_EXCEPTION_ADDR 0x1200020
#define ALT_CPU_FLUSHDA_SUPPORTED
#define ALT_CPU_FREQ 100000000
#define ALT_CPU_HARDWARE_DIVIDE_PRESENT 0
#define ALT_CPU_HARDWARE_MULTIPLY_PRESENT 1
#define ALT_CPU_HARDWARE_MULX_PRESENT 0
#define ALT_CPU_HAS_DEBUG_CORE 1
#define ALT_CPU_HAS_DEBUG_STUB
#define ALT_CPU_HAS_JMPI_INSTRUCTION
#define ALT_CPU_ICACHE_LINE_SIZE 32
#define ALT_CPU_ICACHE_LINE_SIZE_LOG2 5
#define ALT_CPU_ICACHE_SIZE 4096
#define ALT_CPU_INITDA_SUPPORTED
#define ALT_CPU_INST_ADDR_WIDTH 0x19
#define ALT_CPU_NAME "cpu"
#define ALT_CPU_NUM_OF_SHADOW_REG_SETS 0
#define ALT_CPU_RESET_ADDR 0x1000800


/*
 * CPU configuration (with legacy prefix - don't use these anymore)
 *
 */

#define NIOS2_BIG_ENDIAN 0
#define NIOS2_BREAK_ADDR 0x1408820
#define NIOS2_CPU_FREQ 100000000u
#define NIOS2_CPU_ID_SIZE 1
#define NIOS2_CPU_ID_VALUE 0x0
#define NIOS2_CPU_IMPLEMENTATION "fast"
#define NIOS2_DATA_ADDR_WIDTH 0x19
#define NIOS2_DCACHE_LINE_SIZE 32
#define NIOS2_DCACHE_LINE_SIZE_LOG2 5
#define NIOS2_DCACHE_SIZE 2048
#define NIOS2_EXCEPTION_ADDR 0x1200020
#define NIOS2_FLUSHDA_SUPPORTED
#define NIOS2_HARDWARE_DIVIDE_PRESENT 0
#define NIOS2_HARDWARE_MULTIPLY_PRESENT 1
#define NIOS2_HARDWARE_MULX_PRESENT 0
#define NIOS2_HAS_DEBUG_CORE 1
#define NIOS2_HAS_DEBUG_STUB
#define NIOS2_HAS_JMPI_INSTRUCTION
#define NIOS2_ICACHE_LINE_SIZE 32
#define NIOS2_ICACHE_LINE_SIZE_LOG2 5
#define NIOS2_ICACHE_SIZE 4096
#define NIOS2_INITDA_SUPPORTED
#define NIOS2_INST_ADDR_WIDTH 0x19
#define NIOS2_NUM_OF_SHADOW_REG_SETS 0
#define NIOS2_RESET_ADDR 0x1000800


/*
 * DM9000A configuration
 *
 */

#define ALT_MODULE_CLASS_DM9000A DM9000A_IF
#define DM9000A_BASE 0x0
#define DM9000A_IRQ 3
#define DM9000A_IRQ_INTERRUPT_CONTROLLER_ID 0
#define DM9000A_NAME "/dev/DM9000A"
#define DM9000A_SPAN 8
#define DM9000A_TYPE "DM9000A_IF"


/*
 * Define for each module class mastered by the CPU
 *
 */

#define __ALTERA_AVALON_CY7C1380_SSRAM
#define __ALTERA_AVALON_EPCS_FLASH_CONTROLLER
#define __ALTERA_AVALON_JTAG_UART
#define __ALTERA_AVALON_LCD_16207
#define __ALTERA_AVALON_ONCHIP_MEMORY2
#define __ALTERA_AVALON_PIO
#define __ALTERA_AVALON_PLL
#define __ALTERA_AVALON_SYSID
#define __ALTERA_AVALON_TIMER
#define __ALTERA_NIOS2
#define __DM9000A_IF
#define __SEG7_IF


/*
 * SEG7 configuration
 *
 */

#define ALT_MODULE_CLASS_SEG7 SEG7_IF
#define SEG7_BASE 0x1409060
#define SEG7_IRQ -1
#define SEG7_IRQ_INTERRUPT_CONTROLLER_ID -1
#define SEG7_NAME "/dev/SEG7"
#define SEG7_SPAN 32
#define SEG7_TYPE "SEG7_IF"


/*
 * System configuration
 *
 */

#define ALT_DEVICE_FAMILY "CYCLONEII"
#define ALT_ENHANCED_INTERRUPT_API_PRESENT
#define ALT_IRQ_BASE NULL
#define ALT_LOG_PORT "/dev/null"
#define ALT_LOG_PORT_BASE 0x0
#define ALT_LOG_PORT_DEV null
#define ALT_LOG_PORT_TYPE ""
#define ALT_NUM_EXTERNAL_INTERRUPT_CONTROLLERS 0
#define ALT_NUM_INTERNAL_INTERRUPT_CONTROLLERS 1
#define ALT_NUM_INTERRUPT_CONTROLLERS 1
#define ALT_STDERR "/dev/jtag_uart"
#define ALT_STDERR_BASE 0x1409118
#define ALT_STDERR_DEV jtag_uart
#define ALT_STDERR_IS_JTAG_UART
#define ALT_STDERR_PRESENT
#define ALT_STDERR_TYPE "altera_avalon_jtag_uart"
#define ALT_STDIN "/dev/jtag_uart"
#define ALT_STDIN_BASE 0x1409118
#define ALT_STDIN_DEV jtag_uart
#define ALT_STDIN_IS_JTAG_UART
#define ALT_STDIN_PRESENT
#define ALT_STDIN_TYPE "altera_avalon_jtag_uart"
#define ALT_STDOUT "/dev/jtag_uart"
#define ALT_STDOUT_BASE 0x1409118
#define ALT_STDOUT_DEV jtag_uart
#define ALT_STDOUT_IS_JTAG_UART
#define ALT_STDOUT_PRESENT
#define ALT_STDOUT_TYPE "altera_avalon_jtag_uart"
#define ALT_SYSTEM_NAME "DE2_70_SOPC"


/*
 * control_in_pio configuration
 *
 */

#define ALT_MODULE_CLASS_control_in_pio altera_avalon_pio
#define CONTROL_IN_PIO_BASE 0x1409100
#define CONTROL_IN_PIO_BIT_CLEARING_EDGE_REGISTER 1
#define CONTROL_IN_PIO_BIT_MODIFYING_OUTPUT_REGISTER 0
#define CONTROL_IN_PIO_CAPTURE 1
#define CONTROL_IN_PIO_DATA_WIDTH 32
#define CONTROL_IN_PIO_DO_TEST_BENCH_WIRING 0
#define CONTROL_IN_PIO_DRIVEN_SIM_VALUE 0x0
#define CONTROL_IN_PIO_EDGE_TYPE "RISING"
#define CONTROL_IN_PIO_FREQ 100000000u
#define CONTROL_IN_PIO_HAS_IN 1
#define CONTROL_IN_PIO_HAS_OUT 0
#define CONTROL_IN_PIO_HAS_TRI 0
#define CONTROL_IN_PIO_IRQ -1
#define CONTROL_IN_PIO_IRQ_INTERRUPT_CONTROLLER_ID -1
#define CONTROL_IN_PIO_IRQ_TYPE "NONE"
#define CONTROL_IN_PIO_NAME "/dev/control_in_pio"
#define CONTROL_IN_PIO_RESET_VALUE 0x0
#define CONTROL_IN_PIO_SPAN 16
#define CONTROL_IN_PIO_TYPE "altera_avalon_pio"


/*
 * control_out_pio configuration
 *
 */

#define ALT_MODULE_CLASS_control_out_pio altera_avalon_pio
#define CONTROL_OUT_PIO_BASE 0x1409080
#define CONTROL_OUT_PIO_BIT_CLEARING_EDGE_REGISTER 0
#define CONTROL_OUT_PIO_BIT_MODIFYING_OUTPUT_REGISTER 1
#define CONTROL_OUT_PIO_CAPTURE 0
#define CONTROL_OUT_PIO_DATA_WIDTH 32
#define CONTROL_OUT_PIO_DO_TEST_BENCH_WIRING 0
#define CONTROL_OUT_PIO_DRIVEN_SIM_VALUE 0x0
#define CONTROL_OUT_PIO_EDGE_TYPE "NONE"
#define CONTROL_OUT_PIO_FREQ 100000000u
#define CONTROL_OUT_PIO_HAS_IN 0
#define CONTROL_OUT_PIO_HAS_OUT 1
#define CONTROL_OUT_PIO_HAS_TRI 0
#define CONTROL_OUT_PIO_IRQ -1
#define CONTROL_OUT_PIO_IRQ_INTERRUPT_CONTROLLER_ID -1
#define CONTROL_OUT_PIO_IRQ_TYPE "NONE"
#define CONTROL_OUT_PIO_NAME "/dev/control_out_pio"
#define CONTROL_OUT_PIO_RESET_VALUE 0x0
#define CONTROL_OUT_PIO_SPAN 32
#define CONTROL_OUT_PIO_TYPE "altera_avalon_pio"


/*
 * data_in_pio configuration
 *
 */

#define ALT_MODULE_CLASS_data_in_pio altera_avalon_pio
#define DATA_IN_PIO_BASE 0x14090f0
#define DATA_IN_PIO_BIT_CLEARING_EDGE_REGISTER 1
#define DATA_IN_PIO_BIT_MODIFYING_OUTPUT_REGISTER 0
#define DATA_IN_PIO_CAPTURE 1
#define DATA_IN_PIO_DATA_WIDTH 32
#define DATA_IN_PIO_DO_TEST_BENCH_WIRING 1
#define DATA_IN_PIO_DRIVEN_SIM_VALUE 0x0
#define DATA_IN_PIO_EDGE_TYPE "RISING"
#define DATA_IN_PIO_FREQ 100000000u
#define DATA_IN_PIO_HAS_IN 1
#define DATA_IN_PIO_HAS_OUT 0
#define DATA_IN_PIO_HAS_TRI 0
#define DATA_IN_PIO_IRQ -1
#define DATA_IN_PIO_IRQ_INTERRUPT_CONTROLLER_ID -1
#define DATA_IN_PIO_IRQ_TYPE "NONE"
#define DATA_IN_PIO_NAME "/dev/data_in_pio"
#define DATA_IN_PIO_RESET_VALUE 0x0
#define DATA_IN_PIO_SPAN 16
#define DATA_IN_PIO_TYPE "altera_avalon_pio"


/*
 * data_out_pio configuration
 *
 */

#define ALT_MODULE_CLASS_data_out_pio altera_avalon_pio
#define DATA_OUT_PIO_BASE 0x14090a0
#define DATA_OUT_PIO_BIT_CLEARING_EDGE_REGISTER 0
#define DATA_OUT_PIO_BIT_MODIFYING_OUTPUT_REGISTER 1
#define DATA_OUT_PIO_CAPTURE 0
#define DATA_OUT_PIO_DATA_WIDTH 32
#define DATA_OUT_PIO_DO_TEST_BENCH_WIRING 0
#define DATA_OUT_PIO_DRIVEN_SIM_VALUE 0x0
#define DATA_OUT_PIO_EDGE_TYPE "NONE"
#define DATA_OUT_PIO_FREQ 100000000u
#define DATA_OUT_PIO_HAS_IN 0
#define DATA_OUT_PIO_HAS_OUT 1
#define DATA_OUT_PIO_HAS_TRI 0
#define DATA_OUT_PIO_IRQ -1
#define DATA_OUT_PIO_IRQ_INTERRUPT_CONTROLLER_ID -1
#define DATA_OUT_PIO_IRQ_TYPE "NONE"
#define DATA_OUT_PIO_NAME "/dev/data_out_pio"
#define DATA_OUT_PIO_RESET_VALUE 0x0
#define DATA_OUT_PIO_SPAN 32
#define DATA_OUT_PIO_TYPE "altera_avalon_pio"


/*
 * epcs_flash_controller_0 configuration
 *
 */

#define ALT_MODULE_CLASS_epcs_flash_controller_0 altera_avalon_epcs_flash_controller
#define EPCS_FLASH_CONTROLLER_0_BASE 0x1000800
#define EPCS_FLASH_CONTROLLER_0_IRQ 5
#define EPCS_FLASH_CONTROLLER_0_IRQ_INTERRUPT_CONTROLLER_ID 0
#define EPCS_FLASH_CONTROLLER_0_NAME "/dev/epcs_flash_controller_0"
#define EPCS_FLASH_CONTROLLER_0_REGISTER_OFFSET 512
#define EPCS_FLASH_CONTROLLER_0_SPAN 2048
#define EPCS_FLASH_CONTROLLER_0_TYPE "altera_avalon_epcs_flash_controller"


/*
 * hal configuration
 *
 */

#define ALT_MAX_FD 32
#define ALT_SYS_CLK TIMER
#define ALT_TIMESTAMP_CLK none


/*
 * jtag_uart configuration
 *
 */

#define ALT_MODULE_CLASS_jtag_uart altera_avalon_jtag_uart
#define JTAG_UART_BASE 0x1409118
#define JTAG_UART_IRQ 2
#define JTAG_UART_IRQ_INTERRUPT_CONTROLLER_ID 0
#define JTAG_UART_NAME "/dev/jtag_uart"
#define JTAG_UART_READ_DEPTH 64
#define JTAG_UART_READ_THRESHOLD 8
#define JTAG_UART_SPAN 8
#define JTAG_UART_TYPE "altera_avalon_jtag_uart"
#define JTAG_UART_WRITE_DEPTH 64
#define JTAG_UART_WRITE_THRESHOLD 8


/*
 * lcd configuration
 *
 */

#define ALT_MODULE_CLASS_lcd altera_avalon_lcd_16207
#define LCD_BASE 0x14090e0
#define LCD_IRQ -1
#define LCD_IRQ_INTERRUPT_CONTROLLER_ID -1
#define LCD_NAME "/dev/lcd"
#define LCD_SPAN 16
#define LCD_TYPE "altera_avalon_lcd_16207"


/*
 * onchip_mem configuration
 *
 */

#define ALT_MODULE_CLASS_onchip_mem altera_avalon_onchip_memory2
#define ONCHIP_MEM_ALLOW_IN_SYSTEM_MEMORY_CONTENT_EDITOR 0
#define ONCHIP_MEM_ALLOW_MRAM_SIM_CONTENTS_ONLY_FILE 0
#define ONCHIP_MEM_BASE 0x1404000
#define ONCHIP_MEM_CONTENTS_INFO ""
#define ONCHIP_MEM_DUAL_PORT 0
#define ONCHIP_MEM_GUI_RAM_BLOCK_TYPE "Automatic"
#define ONCHIP_MEM_INIT_CONTENTS_FILE "onchip_mem"
#define ONCHIP_MEM_INIT_MEM_CONTENT 1
#define ONCHIP_MEM_INSTANCE_ID "NONE"
#define ONCHIP_MEM_IRQ -1
#define ONCHIP_MEM_IRQ_INTERRUPT_CONTROLLER_ID -1
#define ONCHIP_MEM_NAME "/dev/onchip_mem"
#define ONCHIP_MEM_NON_DEFAULT_INIT_FILE_ENABLED 0
#define ONCHIP_MEM_RAM_BLOCK_TYPE "Auto"
#define ONCHIP_MEM_READ_DURING_WRITE_MODE "DONT_CARE"
#define ONCHIP_MEM_SINGLE_CLOCK_OP 0
#define ONCHIP_MEM_SIZE_MULTIPLE 1
#define ONCHIP_MEM_SIZE_VALUE 12288u
#define ONCHIP_MEM_SPAN 12288
#define ONCHIP_MEM_TYPE "altera_avalon_onchip_memory2"
#define ONCHIP_MEM_WRITABLE 1


/*
 * pio_button configuration
 *
 */

#define ALT_MODULE_CLASS_pio_button altera_avalon_pio
#define PIO_BUTTON_BASE 0x14090d0
#define PIO_BUTTON_BIT_CLEARING_EDGE_REGISTER 0
#define PIO_BUTTON_BIT_MODIFYING_OUTPUT_REGISTER 0
#define PIO_BUTTON_CAPTURE 1
#define PIO_BUTTON_DATA_WIDTH 4
#define PIO_BUTTON_DO_TEST_BENCH_WIRING 0
#define PIO_BUTTON_DRIVEN_SIM_VALUE 0x0
#define PIO_BUTTON_EDGE_TYPE "RISING"
#define PIO_BUTTON_FREQ 100000000u
#define PIO_BUTTON_HAS_IN 1
#define PIO_BUTTON_HAS_OUT 0
#define PIO_BUTTON_HAS_TRI 0
#define PIO_BUTTON_IRQ 4
#define PIO_BUTTON_IRQ_INTERRUPT_CONTROLLER_ID 0
#define PIO_BUTTON_IRQ_TYPE "EDGE"
#define PIO_BUTTON_NAME "/dev/pio_button"
#define PIO_BUTTON_RESET_VALUE 0x0
#define PIO_BUTTON_SPAN 16
#define PIO_BUTTON_TYPE "altera_avalon_pio"


/*
 * pio_switch configuration
 *
 */

#define ALT_MODULE_CLASS_pio_switch altera_avalon_pio
#define PIO_SWITCH_BASE 0x14090c0
#define PIO_SWITCH_BIT_CLEARING_EDGE_REGISTER 0
#define PIO_SWITCH_BIT_MODIFYING_OUTPUT_REGISTER 0
#define PIO_SWITCH_CAPTURE 0
#define PIO_SWITCH_DATA_WIDTH 18
#define PIO_SWITCH_DO_TEST_BENCH_WIRING 0
#define PIO_SWITCH_DRIVEN_SIM_VALUE 0x0
#define PIO_SWITCH_EDGE_TYPE "NONE"
#define PIO_SWITCH_FREQ 100000000u
#define PIO_SWITCH_HAS_IN 1
#define PIO_SWITCH_HAS_OUT 0
#define PIO_SWITCH_HAS_TRI 0
#define PIO_SWITCH_IRQ -1
#define PIO_SWITCH_IRQ_INTERRUPT_CONTROLLER_ID -1
#define PIO_SWITCH_IRQ_TYPE "NONE"
#define PIO_SWITCH_NAME "/dev/pio_switch"
#define PIO_SWITCH_RESET_VALUE 0x0
#define PIO_SWITCH_SPAN 16
#define PIO_SWITCH_TYPE "altera_avalon_pio"


/*
 * pll configuration
 *
 */

#define ALT_MODULE_CLASS_pll altera_avalon_pll
#define PLL_ARESET "None"
#define PLL_BASE 0x1409040
#define PLL_CONFIGUPDATE "None"
#define PLL_IRQ -1
#define PLL_IRQ_INTERRUPT_CONTROLLER_ID -1
#define PLL_LOCKED "None"
#define PLL_NAME "/dev/pll"
#define PLL_PFDENA "None"
#define PLL_PHASECOUNTERSELECT "None"
#define PLL_PHASEDONE "None"
#define PLL_PHASESTEP "None"
#define PLL_PHASEUPDOWN "None"
#define PLL_PLLENA "None"
#define PLL_SCANACLR "None"
#define PLL_SCANCLK "None"
#define PLL_SCANCLKENA "None"
#define PLL_SCANDATA "None"
#define PLL_SCANDATAOUT "None"
#define PLL_SCANDONE "None"
#define PLL_SCANREAD "None"
#define PLL_SCANWRITE "None"
#define PLL_SPAN 32
#define PLL_TYPE "altera_avalon_pll"


/*
 * ssram configuration
 *
 */

#define ALT_MODULE_CLASS_ssram altera_avalon_cy7c1380_ssram
#define SSRAM_BASE 0x1200000
#define SSRAM_IRQ -1
#define SSRAM_IRQ_INTERRUPT_CONTROLLER_ID -1
#define SSRAM_NAME "/dev/ssram"
#define SSRAM_SPAN 2097152
#define SSRAM_SRAM_MEMORY_SIZE 2
#define SSRAM_SRAM_MEMORY_UNITS 1048576
#define SSRAM_SSRAM_DATA_WIDTH 32
#define SSRAM_SSRAM_READ_LATENCY 2
#define SSRAM_TYPE "altera_avalon_cy7c1380_ssram"


/*
 * sysid configuration
 *
 */

#define ALT_MODULE_CLASS_sysid altera_avalon_sysid
#define SYSID_BASE 0x1409110
#define SYSID_ID 0u
#define SYSID_IRQ -1
#define SYSID_IRQ_INTERRUPT_CONTROLLER_ID -1
#define SYSID_NAME "/dev/sysid"
#define SYSID_SPAN 8
#define SYSID_TIMESTAMP 1353067690u
#define SYSID_TYPE "altera_avalon_sysid"


/*
 * timer configuration
 *
 */

#define ALT_MODULE_CLASS_timer altera_avalon_timer
#define TIMER_ALWAYS_RUN 0
#define TIMER_BASE 0x1409000
#define TIMER_COUNTER_SIZE 32
#define TIMER_FIXED_PERIOD 0
#define TIMER_FREQ 100000000u
#define TIMER_IRQ 0
#define TIMER_IRQ_INTERRUPT_CONTROLLER_ID 0
#define TIMER_LOAD_VALUE 99999ull
#define TIMER_MULT 0.0010
#define TIMER_NAME "/dev/timer"
#define TIMER_PERIOD 1.0
#define TIMER_PERIOD_UNITS "ms"
#define TIMER_RESET_OUTPUT 0
#define TIMER_SNAPSHOT 1
#define TIMER_SPAN 32
#define TIMER_TICKS_PER_SEC 1000u
#define TIMER_TIMEOUT_PULSE_OUTPUT 0
#define TIMER_TYPE "altera_avalon_timer"


/*
 * timer_stamp configuration
 *
 */

#define ALT_MODULE_CLASS_timer_stamp altera_avalon_timer
#define TIMER_STAMP_ALWAYS_RUN 0
#define TIMER_STAMP_BASE 0x1409020
#define TIMER_STAMP_COUNTER_SIZE 32
#define TIMER_STAMP_FIXED_PERIOD 0
#define TIMER_STAMP_FREQ 100000000u
#define TIMER_STAMP_IRQ 1
#define TIMER_STAMP_IRQ_INTERRUPT_CONTROLLER_ID 0
#define TIMER_STAMP_LOAD_VALUE 99999ull
#define TIMER_STAMP_MULT 0.0010
#define TIMER_STAMP_NAME "/dev/timer_stamp"
#define TIMER_STAMP_PERIOD 1.0
#define TIMER_STAMP_PERIOD_UNITS "ms"
#define TIMER_STAMP_RESET_OUTPUT 0
#define TIMER_STAMP_SNAPSHOT 1
#define TIMER_STAMP_SPAN 32
#define TIMER_STAMP_TICKS_PER_SEC 1000u
#define TIMER_STAMP_TIMEOUT_PULSE_OUTPUT 0
#define TIMER_STAMP_TYPE "altera_avalon_timer"


/*
 * ucosii configuration
 *
 */

#define OS_ARG_CHK_EN 1
#define OS_CPU_HOOKS_EN 1
#define OS_DEBUG_EN 1
#define OS_EVENT_NAME_SIZE 32
#define OS_FLAGS_NBITS 16
#define OS_FLAG_ACCEPT_EN 1
#define OS_FLAG_DEL_EN 1
#define OS_FLAG_EN 1
#define OS_FLAG_NAME_SIZE 32
#define OS_FLAG_QUERY_EN 1
#define OS_FLAG_WAIT_CLR_EN 1
#define OS_LOWEST_PRIO 20
#define OS_MAX_EVENTS 60
#define OS_MAX_FLAGS 20
#define OS_MAX_MEM_PART 60
#define OS_MAX_QS 20
#define OS_MAX_TASKS 10
#define OS_MBOX_ACCEPT_EN 1
#define OS_MBOX_DEL_EN 1
#define OS_MBOX_EN 1
#define OS_MBOX_POST_EN 1
#define OS_MBOX_POST_OPT_EN 1
#define OS_MBOX_QUERY_EN 1
#define OS_MEM_EN 1
#define OS_MEM_NAME_SIZE 32
#define OS_MEM_QUERY_EN 1
#define OS_MUTEX_ACCEPT_EN 1
#define OS_MUTEX_DEL_EN 1
#define OS_MUTEX_EN 1
#define OS_MUTEX_QUERY_EN 1
#define OS_Q_ACCEPT_EN 1
#define OS_Q_DEL_EN 1
#define OS_Q_EN 1
#define OS_Q_FLUSH_EN 1
#define OS_Q_POST_EN 1
#define OS_Q_POST_FRONT_EN 1
#define OS_Q_POST_OPT_EN 1
#define OS_Q_QUERY_EN 1
#define OS_SCHED_LOCK_EN 1
#define OS_SEM_ACCEPT_EN 1
#define OS_SEM_DEL_EN 1
#define OS_SEM_EN 1
#define OS_SEM_QUERY_EN 1
#define OS_SEM_SET_EN 1
#define OS_TASK_CHANGE_PRIO_EN 1
#define OS_TASK_CREATE_EN 1
#define OS_TASK_CREATE_EXT_EN 1
#define OS_TASK_DEL_EN 1
#define OS_TASK_IDLE_STK_SIZE 512
#define OS_TASK_NAME_SIZE 32
#define OS_TASK_PROFILE_EN 1
#define OS_TASK_QUERY_EN 1
#define OS_TASK_STAT_EN 1
#define OS_TASK_STAT_STK_CHK_EN 1
#define OS_TASK_STAT_STK_SIZE 512
#define OS_TASK_SUSPEND_EN 1
#define OS_TASK_SW_HOOK_EN 1
#define OS_TASK_TMR_PRIO 0
#define OS_TASK_TMR_STK_SIZE 512
#define OS_THREAD_SAFE_NEWLIB 1
#define OS_TICKS_PER_SEC TIMER_TICKS_PER_SEC
#define OS_TICK_STEP_EN 1
#define OS_TIME_DLY_HMSM_EN 1
#define OS_TIME_DLY_RESUME_EN 1
#define OS_TIME_GET_SET_EN 1
#define OS_TIME_TICK_HOOK_EN 1
#define OS_TMR_CFG_MAX 16
#define OS_TMR_CFG_NAME_SIZE 16
#define OS_TMR_CFG_TICKS_PER_SEC 10
#define OS_TMR_CFG_WHEEL_SIZE 2
#define OS_TMR_EN 0

#endif /* __SYSTEM_H_ */
