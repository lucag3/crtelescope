module reg32(reset,clk,enfifo,pdat,dout);
    input reset;
	 input clk;
    input enfifo;
	 input [31:0] pdat;
    output [31:0] dout;
    	 
	 reg [31:0] dout;
		 
	 // register dout every n clk ticks
    always @(posedge clk or posedge reset) begin
      if (reset) begin
	      dout = 32'hffffffff;
		end
      else if(enfifo) begin
	      dout = pdat;
		end	
	end
endmodule
		