//
module  code_mux32(
ctr      , // Mux first input
sel         , // Select input
cmd_code   , // Mux output
clk           // CLK
);
//-----------Input Ports---------------
input [31:0] ctr; 
input sel, clk ;
//-----------Output Ports---------------
output [31:0] cmd_code;
//------------Internal Variables--------
reg  [31:0] cmd_code;
//-------------Code Start-----------------
always@(posedge clk) begin
	if(sel == 0) cmd_code = ctr;
	else if(sel == 1) cmd_code = 32'b00000000000000000000000000000011;
	end
endmodule //End Of Module mux
