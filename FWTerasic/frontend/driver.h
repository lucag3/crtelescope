//list of address

// p-side gpio0
#define data0_p_0 0x008
#define data1_p_0 0x010
#define data2_p_0 0x018
#define data3_p_0 0x020
#define data4_p_0 0x028
#define data5_p_0 0x030
#define data6_p_0 0x038
#define data7_p_0 0x040
#define ctrl_p_0  0x048

// n-side gpio0
#define data0_n_0 0x050
#define data1_n_0 0x058
#define data2_n_0 0x060
#define data3_n_0 0x068
#define data4_n_0 0x070
#define data5_n_0 0x078
#define data6_n_0 0x080
#define data7_n_0 0x088
#define ctrl_n_0  0x090

// p-side gpio1
#define data0_p_1 0x098
#define data1_p_1 0x0A0
#define data2_p_1 0x0A8
#define data3_p_1 0x0B0
#define data4_p_1 0x0B8
#define data5_p_1 0x0C0
#define data6_p_1 0x0C8
#define data7_p_1 0x0D0
#define ctrl_p_1  0x0D8

// n-side gpio1
#define data0_n_1 0x0E0
#define data1_n_1 0x0E8
#define data2_n_1 0x0F0
#define data3_n_1 0x0F8
#define data4_n_1 0x100
#define data5_n_1 0x108
#define data6_n_1 0x110
#define data7_n_1 0x118
#define ctrl_n_1  0x120

// CLK_THROUGH_MODE
#define ClkThrough 0x2B108824


#define ClearReadout 0x01
#define Sync         0x02
#define TrgAcc       0x03
#define ReadEvent    0x04
#define CalStrobe    0x05
#define WCtrlPath    0x1A
#define WCtrlReg     0x1B
#define WCalMask     0x1C
#define ReadReg      0x1D
#define MasterRes    0x1E


