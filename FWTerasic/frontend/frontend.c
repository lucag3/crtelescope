#include <stdio.h>

/* UDP client in the internet domain */
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "driver.h"

int sock, n;
unsigned int length;
struct sockaddr_in server, from;
struct hostent *hp;
char buffer[256];
int chip, side; 
/*side association:
0 = p side gpio0
1 = n side gpio0
2 = p side gpio1
3 = n side gpio1
 */
void error(const char *);

void MasterReset();

void CLKThrough();

void Test();

int main(int argc, char *argv[])
{
       
    if (argc != 3) { printf("Usage: server port\n");
        exit(1);
    }
    sock= socket(AF_INET, SOCK_DGRAM, 0);
    if (sock < 0) error("socket");
    
    server.sin_family = AF_INET;
    hp = gethostbyname(argv[1]);
    if (hp==0) error("Unknown host");
    
    bcopy((char *)hp->h_addr,
          (char *)&server.sin_addr,
          hp->h_length);
    server.sin_port = htons(atoi(argv[2]));
    length=sizeof(struct sockaddr_in);
    //
    int option = 10;
    while(option>0) {
      printf("Options:\n");
      printf("[1] Master reset\n");
      printf("[2] CLK Through Mode\n");
      printf("[3] Test\n");
      printf("[-1] exit\n");
      scanf("%d",&option);
      bzero(buffer,256);
      if(option>-1) {
	printf("Which side?\n 0 = p side gpio0\n 1 = n side gpio0\n 2 = p side gpio1\n 3 = n side gpio1\n");
	scanf("%d",&side);
	printf("Which chip? (f for broadcast)\n");
	scanf("%x",&chip);
      }
      switch(option) {
      case 1: MasterReset();
	break;
      case 2: CLKThrough();
	break;
      case 3: Test();
	break;
      default: break;
      } 
    }
    close(sock);
    return 0;
}

void error(const char *msg)
{
    perror(msg);
    exit(0);
}

 void MasterReset() {
   //set the command word to master reset
   switch(side) {
   case 0: sprintf(buffer,"! WRITE_ATOM_REG %x, %x", MasterRes, (chip<<8) | ctrl_p_0);
     break;   
   case 1: sprintf(buffer,"! WRITE_ATOM_REG %x, %x", MasterRes, (chip<<8) | ctrl_n_0);
     break;   
   case 2: sprintf(buffer,"! WRITE_ATOM_REG %x, %x", MasterRes, (chip<<8) | ctrl_p_1);
     break;   
   case 3: sprintf(buffer,"! WRITE_ATOM_REG %x, %x", MasterRes, (chip<<8)| ctrl_n_1);
     break;
   }

   n=sendto(sock,buffer,
	    strlen(buffer),0,(const struct sockaddr *)&server,length);
   
 }

 void CLKThrough() {

   //first of all issue a master reset
   MasterReset();
   //then set the data0 word
   switch(side) {
   case 0: sprintf(buffer,"! WRITE_ATOM_REG %x, %x", ClkThrough, data0_p_0);
     break;   
   case 1: sprintf(buffer,"! WRITE_ATOM_REG %x, %x", ClkThrough, data0_n_0);
     break;   
   case 2: sprintf(buffer,"! WRITE_ATOM_REG %x, %x", ClkThrough, data0_p_1);
     break;   
   case 3: sprintf(buffer,"! WRITE_ATOM_REG %x, %x", ClkThrough, data0_n_1);
     break;
   }

   n=sendto(sock,buffer,
	    strlen(buffer),0,(const struct sockaddr *)&server,length);

   //finally issue the command
   switch(side) {
   case 0: sprintf(buffer,"! WRITE_ATOM_REG %x, %x", WCtrlReg, (chip<<8) | ctrl_p_0);
     break;   
   case 1: sprintf(buffer,"! WRITE_ATOM_REG %x, %x", WCtrlReg, (chip<<8) | ctrl_n_0);
     break;   
   case 2: sprintf(buffer,"! WRITE_ATOM_REG %x, %x", WCtrlReg, (chip<<8) | ctrl_p_1);
     break;   
   case 3: sprintf(buffer,"! WRITE_ATOM_REG %x, %x", WCtrlReg, (chip<<8)| ctrl_n_1);
     break;
   }

   n=sendto(sock,buffer,
	    strlen(buffer),0,(const struct sockaddr *)&server,length);
 }

void Test() {
  int i;
    for(i = 0; i<8;i++) {
      bzero(buffer,256);
      sprintf(buffer,"! WRITE_ATOM_REG %d 0",i);
      //      fgets(buffer,255,stdin);
      n=sendto(sock,buffer,
	       strlen(buffer),0,(const struct sockaddr *)&server,length);
      if (n < 0) error("Sendto");
      sleep(1);
    }
}

