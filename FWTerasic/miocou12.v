module miocou12(q,d,clk,clk_en,load,carry);

output[11:0] q;
output carry;
input[11:0] d;
input load, clk, clk_en;
reg[11:0] q;
always @(posedge clk or posedge load)
	if(load)
		q = d;
	else if(clk_en)
		q = q + 1;

wire carry;
assign carry = (q == 12'b111111111111);
endmodule