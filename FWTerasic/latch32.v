module latch32(
   // Outputs
   dout,
   // Inputs
   din, le
   );
   input [31:0] din;
   output [31:0] dout;
   input  le; // latch enable
   reg [31:0] dout;
   always @(le)
     if (le == 1'b1)
       dout = din;
   
endmodule // latch