#!/bin/sh
#
# This file was automatically generated.
#
# It can be overwritten by nios2-flash-programmer-generate or nios2-flash-programmer-gui.
#

#
# Converting SOF File: C:\DAQ\ProgettoTelescopioTerasic\DE2_70_NET.sof to: "..\flash/DE2_70_NET_epcs_flash_controller_0.flash"
#
nios2eds/bin/sof2flash --input="C:/DAQ/ProgettoTelescopioTerasic/DE2_70_NET.sof" --output="../flash/DE2_70_NET_epcs_flash_controller_0.flash" --epcs 

#
# Programming File: "..\flash/DE2_70_NET_epcs_flash_controller_0.flash" To Device: epcs_flash_controller_0
#
nios2eds/bin/nios2-flash-programmer "../flash/DE2_70_NET_epcs_flash_controller_0.flash" --base=0x1000800 --epcs --sidp=0x1409110 --id=0x0 --accept-bad-sysid --device=1 --instance=0 '--cable=USB-Blaster on localhost [USB-0]' --program 

#
# Converting ELF File: C:\DAQ\ProgettoTelescopioTerasic\software\de2_70_telescopio_OS_v3\de2_70_telescopio_OS.elf to: "..\flash/de2_70_telescopio_OS_epcs_flash_controller_0.flash"
#
nios2eds/bin/elf2flash --input="C:/DAQ/ProgettoTelescopioTerasic/software/de2_70_telescopio_OS_v3/de2_70_telescopio_OS.elf" --output="../flash/de2_70_telescopio_OS_epcs_flash_controller_0.flash" --epcs --after="../flash/DE2_70_NET_epcs_flash_controller_0.flash" 

#
# Programming File: "..\flash/de2_70_telescopio_OS_epcs_flash_controller_0.flash" To Device: epcs_flash_controller_0
#
nios2eds/bin/nios2-flash-programmer "../flash/de2_70_telescopio_OS_epcs_flash_controller_0.flash" --base=0x1000800 --epcs --sidp=0x1409110 --id=0x0 --accept-bad-sysid --device=1 --instance=0 '--cable=USB-Blaster on localhost [USB-0]' --program 

